#!/bin/bash
set -e

echo "Memory!"
free -g

echo "Running OtherTest group!"
mvn test -D checkstyle.skip=true -D groups="test.OtherTest" -D spring.profiles.active=test,docker -o

# upload the codecov
echo "Running code coverage"
curl -s https://codecov.io/bash >> codecov
bash codecov
