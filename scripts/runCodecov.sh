#!/bin/bash
set -e

cp /app/* /data/

# get the all tests code coverage reports
cd /data/

# upload the codecov
echo "Running code coverage"
curl -s https://codecov.io/bash >> codecov
bash codecov
