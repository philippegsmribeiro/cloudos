#!/bin/bash
set -e

echo "Memory!"
free -g

echo "Running NormalTest group!"

mvn test -D checkstyle.skip=true -D excludedGroups="test.SlowTest,test.HighMemoryTest,test.OtherTest" -D spring.profiles.active=test,docker -o

# upload the codecov
echo "Running code coverage"
curl -s https://codecov.io/bash >> codecov
bash codecov
