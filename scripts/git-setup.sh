#!/bin/bash
pushd ../.git/hooks/
ln -s ../../scripts/git-pre-push-hook.sh pre-push
chmod +x pre-push
popd
echo "git pre-push hook installed!"
