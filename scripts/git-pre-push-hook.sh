#!/bin/bash
export MAVEN_OPTS="-client
  -XX:+TieredCompilation
  -XX:TieredStopAtLevel=1
  -Xverify:none"
echo
echo "Checking code against checkstyle settings and storing in target/checkstyle-results.xml"
echo
mvn -q checkstyle:check -Dcheckstyle.failOnViolation=true