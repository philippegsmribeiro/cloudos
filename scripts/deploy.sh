#!/bin/bash
echo "Deploying!"

echo "1. Generating jar!"
mvn package deploy -D maven.test.skip=true -P master -D checkstyle.skip=true -D org.xml.sax.driver="com.sun.org.apache.xerces.internal.parsers.SAXParser"
cp ./target/*.jar ./target/cloudos-latest.jar 

# todo: remove the pwd from here
echo "2. Uploading jar!"
sshpass -p 'kPP*6$*^AwHXeq9=' scp -rp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ./target/cloudos-latest.jar cloudos@cloudos.ddns.net:/cloudos/codeship/cloudos.jar

echo "3. Restarting the server!"
sshpass -p 'kPP*6$*^AwHXeq9=' ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null cloudos@cloudos.ddns.net 'sh /cloudos/redeploy.sh'