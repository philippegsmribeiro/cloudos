#!/bin/bash
set -e

echo "Memory!"
free -g

echo "Running HighMemory group!"
mvn test -D checkstyle.skip=true -D groups="test.HighMemoryTest" -D spring.profiles.active=test,docker -o
#mvn test -Dcheckstyle.skip=true -Dtest="ApplicationTests" -Dspring.profiles.active=test,docker

# upload the codecov
echo "Running code coverage"
curl -s https://codecov.io/bash >> codecov
bash codecov
