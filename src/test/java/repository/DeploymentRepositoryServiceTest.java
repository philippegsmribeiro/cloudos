package repository;

import static org.junit.Assert.assertTrue;

import cloudos.repositories.DeploymentRepositoryService;

import java.io.File;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

/** Created by philipperibeiro on 12/25/16. */
public class DeploymentRepositoryServiceTest extends CloudOSTest {

  @Autowired private DeploymentRepositoryService deploymentService;

  private String resource;

  private String key;

  @Before
  public void setUp() throws Exception {
    String directory = System.getProperty("user.dir") + File.separator + "data";
    this.key = "six-1.10.0-py2.py3-none-any.whl";
    this.resource = directory + File.separator + key;
  }

  @Test
  public void testUploadRepository() throws Exception {
    assertTrue(this.deploymentService.uploadDeployment(this.resource));
  }

  @Test
  public void testDownloadRepository() throws Exception {
    assertTrue(this.deploymentService.downloadDeployment(this.key, false));
  }
}
