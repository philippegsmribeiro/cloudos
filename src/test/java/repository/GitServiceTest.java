package repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import cloudos.repositories.GitService;

import java.io.File;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.eclipse.jgit.api.Git;
import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import test.CloudOSTest;
import test.SlowTest;

/** Created by philipperibeiro on 12/23/16. */
@Category(SlowTest.class)
public class GitServiceTest extends CloudOSTest {

  @Autowired private GitService gitService;

  @Value("${cloudos.build.endpoint.repository}")
  private String repositoryEndpoint;

  @Value("${cloudos.build.github.username}")
  private String username;

  @Value("${cloudos.build.github.password}")
  private String password;

  private List<String> directories = new ArrayList<>();

  // @Configuration
  // static class ContextConfiguration {
  // }

  @Test
  public void testCloneRepository() throws Exception {
    String uri = "https://github.com/ansible/ansible-examples.git";
    String directory = this.repositoryEndpoint + File.separator + "ansible-examples";
    File file = new File(directory);
    if (file.exists()) {
      deleteDir(file);
    }
    this.directories.add(directory);
    assertNotNull(this.gitService.cloneRepository(uri, directory, true, false));
  }

  @Test
  public void testCreateRepository() throws Exception {
    String directory = this.repositoryEndpoint + File.separator + "create-examples1";
    File file = new File(directory);
    if (file.exists()) {
      deleteDir(file);
    }
    this.directories.add(directory);
    assertNotNull(this.gitService.createRepository(directory));
    assertTrue(new File(directory).exists());
  }

  @Test
  public void testAddFileRepository() throws Exception {
    String directory = this.repositoryEndpoint + File.separator + "create-examples2";
    File file = new File(directory);
    if (file.exists()) {
      deleteDir(file);
    }
    this.directories.add(directory);
    Git git = this.gitService.createRepository(directory);
    assertNotNull(git);
    assertTrue(new File(directory).exists());

    // Create a file
    String pathname = directory + File.separator + "hello.txt";
    assertTrue(
        this.gitService.addFileToRepository(git, new ArrayList<>(Arrays.asList(pathname))));

    // commit a message
    assertTrue(this.gitService.commitRepository(git, "this is a test message"));
  }

  @Test
  public void testRemoveFileRepository() throws Exception {
    String directory = this.repositoryEndpoint + File.separator + "create-examples2";
    File file = new File(directory);
    if (file.exists()) {
      deleteDir(file);
    }
    this.directories.add(directory);
    Git git = this.gitService.createRepository(directory);
    assertNotNull(git);
    assertTrue(new File(directory).exists());

    // Create a file
    String pathname = directory + File.separator + "hello.txt";
    assertTrue(
        this.gitService.removeFileFromRepository(
            git, new ArrayList<>(Arrays.asList(pathname))));
  }

  @Test
  public void testCloneAuthenticatedRepository() throws Exception {
    String uri = "https://github.com/mlcloudos/testrepo.git";
    String directory = this.repositoryEndpoint + File.separator + "testrepo";
    File file = new File(directory);
    if (file.exists()) {
      deleteDir(file);
    }
    this.directories.add(directory);
    Git git =
        this.gitService.cloneRepository(uri, this.username, this.password, directory, false, true);
    assertNotNull(git);
    assertTrue(new File(directory).exists());
  }

  @After
  public void tearDown() throws Exception {
    for (String directory : this.directories) {
      deleteDir(new File(directory));
    }
  }

  private void deleteDir(File dir) {
    try {
      Files.walk(dir.toPath(), FileVisitOption.FOLLOW_LINKS)
          .sorted(Comparator.reverseOrder())
          .map(Path::toFile)
          .peek(System.out::println)
          .forEach(File::delete);
      dir.delete();
    } catch (Exception e2) {
      // TODO: handle exception
      e2.printStackTrace();
    }
  }
}
