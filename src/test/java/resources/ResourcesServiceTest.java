package resources;

import cloudos.Providers;
import cloudos.alerts.AlertException;
import cloudos.exceptions.NotImplementedException;
import cloudos.instances.InstanceService;
import cloudos.models.ClientStatus;
import cloudos.models.Instance;
import cloudos.models.InstanceStatus;
import cloudos.models.QInstance;
import cloudos.models.ResourceFetchRequest;
import cloudos.provider.ProviderMachineType;
import cloudos.provider.ProviderRegion;
import cloudos.provider.ProvidersService;
import cloudos.resources.ResourcesService;
import cloudos.utils.datafactory.DataFactory;
import com.querydsl.core.BooleanBuilder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import test.CloudOSTest;

/**
 * Unit tests for ResourcesService.
 *
 * @author Alex Calagua
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ResourcesServiceTest extends CloudOSTest {

  @Autowired
  ResourcesService resourcesService;

  @Autowired
  private InstanceService instanceService;

  @Autowired
  private ProvidersService providersService;

  private static List<Instance> listResourcesCache;

  private Random random;

  @BeforeClass
  public static void init() {
    listResourcesCache = new ArrayList<Instance>();
  }
  
  @After
  public void finishTest() {
    this.clearResource(listResourcesCache);
  }
  
  @Before
  public void prepareTest() throws AlertException, NotImplementedException {
    BooleanBuilder builder = new BooleanBuilder();
    QInstance instance = new QInstance("instance");
    builder.and(instance.name.contains("test"));
    Iterable<Instance> pageResource = instanceService.findAll(builder);
    this.clearResource(pageResource);
    this.initializeDataTest();
    this.random = new Random();
  }

  @Test
  public void testBListingByName() {
    ResourceFetchRequest fetchRequest = new ResourceFetchRequest();
    Instance instanceFilter = listResourcesCache.get(random.nextInt(listResourcesCache.size()));
    fetchRequest.setName(instanceFilter.getName());
    Page<Instance> pageResources = resourcesService.listResourcesByFilter(fetchRequest);
    for (Instance instance : pageResources.getContent()) {
      Assert.assertTrue(instance.getName().contains(instanceFilter.getName()));
    }
  }

  @Test
  public void testCListingTerminated() {
    ResourceFetchRequest fetchRequest = new ResourceFetchRequest();
    Instance instanceFilter = listResourcesCache.stream()
        .filter(instance -> InstanceStatus.TERMINATED.equals(instance.getStatus())).findAny()
        .orElse(null);
    fetchRequest.setInstanceStatus(Arrays.asList(instanceFilter.getStatus()));
    Page<Instance> pageResources = resourcesService.listResourcesByFilter(fetchRequest);
    for (Instance instance : pageResources.getContent()) {
      Assert.assertTrue(instance.getStatus().equals(instanceFilter.getStatus()));
    }
  }

  @Test
  public void testDListingRunning() {
    ResourceFetchRequest fetchRequest = new ResourceFetchRequest();
    List<InstanceStatus> status =
        Arrays.asList(InstanceStatus.RUNNING, InstanceStatus.PROVISIONING);
    fetchRequest.setInstanceStatus(status);
    Page<Instance> pageResources = resourcesService.listResourcesByFilter(fetchRequest);
    for (Instance instance : pageResources.getContent()) {
      Assert.assertFalse(instance.getDeleted());
      Assert.assertTrue(status.contains(instance.getStatus()));
    }
  }

  @Test
  public void testEListingStopped() {
    ResourceFetchRequest fetchRequest = new ResourceFetchRequest();
    List<InstanceStatus> status = Arrays.asList(InstanceStatus.STOPPED, InstanceStatus.STOPPING);
    fetchRequest.setInstanceStatus(status);
    Page<Instance> pageResources = resourcesService.listResourcesByFilter(fetchRequest);
    for (Instance instance : pageResources.getContent()) {
      Assert.assertFalse(instance.getDeleted());
      Assert.assertTrue(status.contains(instance.getStatus()));
    }
  }

  @Test
  public void testFPageListInstance() {
    ResourceFetchRequest fetchRequest = new ResourceFetchRequest();
    fetchRequest.setName("test");
    Page<Instance> pageResources = resourcesService.listResourcesByFilter(fetchRequest);

    Assert.assertTrue(pageResources.getTotalPages() == 2);
    Assert.assertTrue(pageResources.getNumber() == 0);
    Assert.assertTrue(pageResources.getNumberOfElements() == 10);

    ResourceFetchRequest fetchRequestNextPage = new ResourceFetchRequest();
    fetchRequestNextPage.setName("test");
    fetchRequestNextPage.setPage(1);
    Page<Instance> pageResourcesNextPage =
        resourcesService.listResourcesByFilter(fetchRequestNextPage);

    Assert.assertTrue(pageResourcesNextPage.getTotalPages() == 2);
    Assert.assertTrue(pageResourcesNextPage.getNumber() == 1);
    Assert.assertTrue(pageResourcesNextPage.getNumberOfElements() == 8);
  }
  
  @Test
  public void testGListingByname() {
    ResourceFetchRequest fetchRequest = new ResourceFetchRequest();
    String name = "test";
    fetchRequest.setName(name);
    List<Instance> listResources = resourcesService.listResources(fetchRequest);
    for (Instance instance : listResources) {
      // Assert.assertTrue(instance.getDeleted());
      Assert.assertTrue(instance.getName().contains(name));
    }
  }

  /**
   * Method that clean test data
   */
  private void clearResource(Iterable<Instance> listResource) {
    for (Instance instance : listResource) {
      instanceService.delete(instance.getId());
    }
    listResourcesCache = new ArrayList<>();
  }


  /**
   * Method that mock the data to create a list of instances.
   */
  private void initializeDataTest() {
    DataFactory df = new DataFactory();
    Date minDate = df.getDate(2018, 1, 1);
    Date maxDate = new Date();
    Map<Providers, List<ProviderRegion>> regions = providersService.retrieveRegions();
    Map<Providers, List<ProviderMachineType>> machineTypes = providersService.retrieveMachineTypes();
    List<Providers> providers = providersService.retrieveSupportedProviders();
    listResourcesCache = new ArrayList<Instance>();
    for (int n = 0; n < 18; n = n + 1) {
      Providers provider = df.getItem(providers);
      Instance testData = Instance.builder().provider(provider)
          .providerId(df.getAlphaNumericRandom()).creationDate(df.getDateBetween(minDate, maxDate))
          .includedDate(df.getDate(minDate, 1, 10)).region(df.getRegion(provider, regions.get(provider)))
          .machineType(df.getMachineType(provider, machineTypes.get(provider)))
          .name(df.getInstanceName("test", DataFactory.INSTANCE, DataFactory.HYPHEN))
          .status(df.getItem(InstanceStatus.values()))
          .clientStatus(df.getItem(ClientStatus.values())).cost(df.getDoubleBetween(10.0, 1000.5))
          .deleted(false).build();
      instanceService.save(testData);
      listResourcesCache.add(testData);
    }
  }

}
