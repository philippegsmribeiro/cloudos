package machinelearning.autoscaler;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.datavec.api.util.ClassPathResource;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.primitives.Pair;

import cloudos.machinelearning.autoscaler.AutoscalerResourcesIterator;
import cloudos.machinelearning.autoscaler.ResourceCategory;
import test.CloudOSTest;

public class AutoscalerResourcesIteratorTest extends CloudOSTest {

  private String filename;
  private AutoscalerResourcesIterator iterator;

  @Before
  public void setUp() throws Exception {
    String path = "machinelearning/autoscaler/regression.csv";
    this.filename = new ClassPathResource(path)
                                      .getFile()
                                      .getPath();
  }

  @Test
  @Ignore
  public void testAutoscalerResourcesIterator() throws Exception {
    int batchSize = 64; // mini-batch size
    int exampleLength = 22; // time series length
    double splitRatio = 0.9; // 90% for training, 10% for testing
    int epochs = 1; // training epochs

    logger.debug("Creating dataset iterator for file {}...", this.filename);
    iterator = new AutoscalerResourcesIterator(this.filename, batchSize, exampleLength,
        splitRatio, ResourceCategory.CPU_USAGE);

    logger.debug("Autoscaler iterator: {}", iterator);
    logger.debug("Loading test dataset...");
    List<Pair<INDArray, INDArray>> test = iterator.getTestDataSet();

    for (int i = 0; i < epochs; i++) {
      DataSet dataSet;
      while (iterator.hasNext()) {
        dataSet = iterator.next();
        logger.debug("Autoscaler Dataset: {}", dataSet);
      }
    }

    assertNotNull(test);
    assertThat(test.size(), greaterThan(0));
  }
}
