package machinelearning.autoscaler;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import cloudos.machinelearning.MetricbeatFeatureExtractor;
import cloudos.machinelearning.autoscaler.MetricbeatVector;
import cloudos.models.Metricbeat;

import java.io.File;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import test.CloudOSTest;

/** Created by philipperibeiro on 7/13/17. */
public class MetricbeatVectorTest extends CloudOSTest {

  private String filename;

  private MetricbeatFeatureExtractor featureExtractor;

  @Before
  public void setUp() throws Exception {
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource("machinelearning/metricbeat").getFile());
    this.filename = file.getAbsolutePath();
    assertNotNull(this.filename);

    this.featureExtractor = new MetricbeatFeatureExtractor();
  }

  @Test
  public void testExtractVector() throws Exception {
    assertNotNull(this.filename);
    assertNotNull(this.featureExtractor);
    List<Metricbeat> metricbeatList = this.featureExtractor.readMetricbeats(this.filename);
    assertNotNull(metricbeatList);
    for (Metricbeat metricbeat : metricbeatList) {
      MetricbeatVector vector = new MetricbeatVector(metricbeat);
      assertNotNull(vector);
    }
  }

  @Test
  public void testReadStream() throws Exception {
    assertNotNull(this.filename);
    String parent = new File(this.filename).getParent();
    File file = new File(parent, "metricbeatvector");
    // delete old file
    if (file.exists()) {
      assertTrue(file.delete());
    }
    String destination = file.getAbsolutePath();
    logger.debug("Destination: {}", destination);
    this.featureExtractor.readStream(this.filename, destination);
    assertTrue(file.exists());
  }
}
