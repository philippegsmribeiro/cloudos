package machinelearning.autoscaler;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.springframework.beans.factory.annotation.Autowired;

import cloudos.machinelearning.autoscaler.AmazonAutoscalerManager;
import test.CloudOSTest;

public class AmazonAutoscalerManagerTest extends CloudOSTest {

  @Autowired
  AmazonAutoscalerManager autoscalerManager;

  @Test
  public void testAnalyze() throws Exception {
    Map<String, List<Pair<String, INDArray>>> vectorMap = new HashMap<>();
    double[][] matrix = {
        {0.635, 0, 0, 0, 0, 0, 0},
        {0.656, 0, 0, 0, 0, 0, 0},
        {0.725, 0, 0, 0, 0, 0, 0},
        {0.755, 0, 0, 0, 0, 0, 0},
        {0.335, 0, 0, 0, 0, 0, 0},
        {0.324, 0, 0, 0, 0, 0, 0},
        {0.224, 0, 0, 0, 0, 0, 0},
        {0.424, 0, 0, 0, 0, 0, 0},
        {0.244, 0, 0, 0, 0, 0, 0},
    };
    INDArray indArray = Nd4j.create(matrix);
    Pair<String, INDArray> pair = new ImmutablePair<>("i-x142142", indArray);
    List<Pair<String, INDArray>> list = Collections.singletonList(pair);
    vectorMap.put("us-west-1", list);
    // check no error is thrown
    Assertions.assertThatCode(() -> {
      logger.debug("#########################");
      logger.debug(vectorMap);
      logger.debug(autoscalerManager);
      autoscalerManager.analyze(vectorMap);
    }).doesNotThrowAnyException();
  }
}
