package machinelearning.autoscaler;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import cloudos.Providers;
import cloudos.insights.InsightsService;
import cloudos.instances.CloudManagerService;
import cloudos.instances.InstanceService;
import cloudos.machinelearning.InstanceUtil;
import cloudos.machinelearning.ModelsRepository;
import cloudos.machinelearning.autoscaler.Autoscaler;
import cloudos.machinelearning.autoscaler.AutoscalerVector;
import cloudos.machinelearning.autoscaler.DatapointVector;
import cloudos.models.Instance;
import cloudos.models.cloudgroup.CloudGroupService;
import cloudos.policies.CloudPolicyService;
import cloudos.provider.ProvidersService;
import cloudos.queue.QueueManagerService;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.assertj.core.api.Assertions;
import org.datavec.api.util.ClassPathResource;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.string.NDArrayStrings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import test.CloudOSTest;

/** Created by philipperibeiro on 7/6/17. */
public class AutoscaleTest extends CloudOSTest {

  @Value("${cloudos.ml.autoscaler}")
  private String autoscalerPath;

  private static Autoscaler autoscaler;

  private static boolean initialized = false;

    /* The Machine Type repository will used to fetch information about the instance */
  @Autowired
  ProvidersService providersService;

  @Autowired
  ModelsRepository modelsRepository;

  @Autowired
  InstanceService instanceService;

  @Autowired
  CloudManagerService cloudManagerService;

  @Autowired
  QueueManagerService queueManagerService;

  @Autowired
  CloudGroupService cloudGroupService;
  
  @Autowired
  private InsightsService insightsService;
  
  @Autowired
  CloudPolicyService cloudPolicyService;

  @Before
  public void setUp() throws Exception {
    if (!initialized) {
      autoscaler = new Autoscaler(this.providersService,
                                  this.modelsRepository,
                                  this.instanceService,
                                  this.cloudManagerService,
                                  this.autoscalerPath,
                                  this.queueManagerService,
                                  this.cloudGroupService,
                                  this.insightsService,
                                  this.cloudPolicyService);
      initialized = true;
    }
  }

  @Test
  public void testInstanceUtil() throws Exception {
    InstanceUtil instanceUtil = InstanceUtil.getInstance();
    // assert it found a valid instance type
    assertNotNull(instanceUtil.getInstanceEntry("c1.medium"));
    // assert it didn't find a invalid instance type
    assertNull(instanceUtil.getInstanceEntry("doesnot.exist"));
  }

  @Test
  public void testAutoscalerVector() throws Exception {
    List<DatapointVector> vectorList = Arrays.asList(
        new DatapointVector(1503163026000L,"b270", Arrays.asList(3.16666465068162E-4,0.0,
            0.0,0.0,0.0, 0.0,0.0)),
        new DatapointVector(1503163028000L,"b270", Arrays.asList(0.0,0.0,0.0,0.0,0.0,
            3.3805866666666666E-4,0.0)),
        new DatapointVector(1503163027000L,"b270", Arrays.asList(0.0,0.0,0.0,0.0,
            0.0023753854166666667,0.0,0.0)),
        new DatapointVector(1503163029000L,"b270", Arrays.asList(0.0,0.0,0.0,0.0,0.0,0.0,
            0.004415648))
    );

    INDArray matrix = AutoscalerVector.getMatrix(vectorList);
    assertNotNull(matrix);
    logger.debug(new NDArrayStrings(6).format(matrix));
  }

  @Test
  @Ignore
  public void testMultiAutoscaler() throws Exception {
    Map<String, Autoscaler> autoscalerMap = new HashMap<>();

    List<String> regions = Arrays.asList("us-west-1", "us-west-2", "us-east-1");
    regions.forEach(region -> {
      logger.debug("Processing region {}", region);
      autoscalerMap.put(region, new Autoscaler(this.providersService,
                                               this.modelsRepository,
                                               this.instanceService,
                                               this.cloudManagerService,
                                               this.autoscalerPath,
                                               this.queueManagerService,
                                               this.cloudGroupService,
                                               this.insightsService,
                                               this.cloudPolicyService));
    });

    // check if everything was created correct
    assertNotEquals(autoscalerMap.size(), 0);
    autoscalerMap.forEach((region, autoscaler) -> assertNotNull(autoscaler));
  }

  @Test
  @Ignore
  public void testTrain() throws Exception {

    final String filenameTrain =
        new ClassPathResource("/machinelearning/autoscaler/train_autoscale_classifier.csv")
            .getFile()
            .getPath();

    final String filenameTest =
        new ClassPathResource("/machinelearning/autoscaler/test_autoscale_classifier.csv")
            .getFile()
            .getPath();

    final String regressionTrain =
        new ClassPathResource("/machinelearning/autoscaler/train_autoscale_regression.csv")
            .getFile()
            .getPath();

    final String regressionTest =
        new ClassPathResource("/machinelearning/autoscaler/test_autoscale_regression.csv")
            .getFile()
            .getPath();
    // set the path
    autoscaler.withAutoscalerPath(autoscalerPath);

    autoscaler.train(false, filenameTrain, filenameTest, regressionTrain, regressionTest);
  }

  @Test
  @Ignore
  public void testPredict() throws Exception {
    double[][] data = {
      {0.341, 0, 0, 0, 0, 0, 0},
      {0.955, 0, 0, 0, 0, 0, 0},
      {0.925, 0, 0, 0, 0, 0, 0},
      {0.755, 0, 0, 0, 0, 0, 0},
      {0.635, 0, 0, 0, 0, 0, 0},
      {0.523, 0, 0, 0, 0, 0, 0},
      {0.415, 0, 0, 0, 0, 0, 0},
      {0.355, 0, 0, 0, 0, 0, 0},
      {0.235, 0, 0, 0, 0, 0, 0},
      {0.135, 0, 0, 0, 0, 0, 0},
      {0.835, 0, 0, 0, 0, 0, 0},
      {0.924, 0, 0, 0, 0, 0, 0},
      {0.724, 0, 0, 0, 0, 0, 0},
      {0.524, 0, 0, 0, 0, 0, 0},
      {0.324, 0, 0, 0, 0, 0, 0},
      {0.644, 0, 0, 0, 0, 0, 0},
    };
    List<Instance> instances = instanceService.findAllActiveAndNotStoppedInstancesByProvider(
        Providers.AMAZON_AWS
    );
    assertNotNull(instances);

    if (instances.size() > 0) {
      for (double[] row : data) {
        INDArray rowVector = Nd4j.create(row);
        Pair<String, INDArray> vector = new ImmutablePair<>(instances.get(0).getProviderId(), rowVector);
        Assertions.assertThatCode(
            () -> {
              autoscaler.predict("us-west-1", vector);
            })
            .doesNotThrowAnyException();
      }
    }
  }

  @Test
  @Ignore
  public void testMultiTaskScalerModel() throws Exception {
    final String filenameTrain =
        new ClassPathResource("/machinelearning/autoscaler/regression.csv")
            .getFile()
            .getPath();

    autoscaler.multiTaskScalerModel(false, filenameTrain);
  }
}
