package machinelearning;

import cloudos.machinelearning.nlp.PrepareWordVector;
import cloudos.machinelearning.nlp.SentimentRNNService;

import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;

import test.CloudOSTest;

/** Created by philipperibeiro on 3/4/17. */
@Ignore
public class SentimentRNNServiceTest extends CloudOSTest {

  private SentimentRNNService sentimentRNNService;

  @Value("${cloudos.ml.data.folder}")
  private String dataFolder;

  @Value("${cloudos.ml.data.newsdata}")
  private String dataUrl;

  @Value("${cloudos.ml.data.word.glove}")
  private String wordVectorsPath;

  @Value("{cloudos.ml.data.sentiment.output}")
  private String output;

  private PrepareWordVector prepareWordVector;

  // @Configuration
  // static class ContextConfiguration {
  // }

  @Before
  public void setUp() throws Exception {
    this.prepareWordVector = new PrepareWordVector();
    this.sentimentRNNService =
        new SentimentRNNService(this.dataFolder, this.dataUrl, this.wordVectorsPath, this.output);
  }

  @Test
  public void testSentimentRNN() throws Exception {
    // word vector paths
    this.prepareWordVector.prepare(this.dataUrl);
    MultiLayerConfiguration conf = this.sentimentRNNService.getMultiLayerConfiguration();
    this.sentimentRNNService.trainNeuralNetwork(conf, false);
  }
}
