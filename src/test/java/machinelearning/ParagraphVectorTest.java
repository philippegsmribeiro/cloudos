package machinelearning;

import cloudos.machinelearning.nlp.ParagraphVectorsClassifier;
import cloudos.machinelearning.nlp.ParagraphVectorsInference;
import cloudos.machinelearning.nlp.ParagraphVectorsText;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;

import test.CloudOSTest;

/** Created by philipperibeiro on 3/4/17. */
@Ignore
public class ParagraphVectorTest extends CloudOSTest {

  @Value("${cloudos.ml.data.classification}")
  private String dataFolder;

  private ParagraphVectorsInference paragraphVectorsInference;

  private ParagraphVectorsClassifier paragraphVectorsClassifier;

  private ParagraphVectorsText paragraphVectorsText;

  // @Configuration
  // static class ContextConfiguration {
  // }

  @Before
  public void setUp() throws Exception {
    //this.paragraphVectorsInference = new ParagraphVectorsInference();
    this.paragraphVectorsClassifier = new ParagraphVectorsClassifier();
    //this.paragraphVectorsText = new ParagraphVectorsText();
  }

  @Test
  public void testParagraphVector() throws Exception {

    // build the vectors
    //this.paragraphVectorsText.model(this.dataFolder);

    // build the intereence
    //this.paragraphVectorsInference.inferer(this.dataFolder);

    // build the classifier
    this.paragraphVectorsClassifier.classify(this.dataFolder);
  }
}
