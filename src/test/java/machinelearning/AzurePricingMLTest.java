package machinelearning;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import cloudos.machinelearning.AzurePricingML;
import cloudos.machinelearning.MLUtils;
import cloudos.microsoft.AzureInstance;
import cloudos.microsoft.AzureInstanceRepository;
import cloudos.microsoft.AzurePricingRepository;

import java.util.Arrays;
import java.util.List;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.ml.feature.LabeledPoint;
import org.apache.spark.ml.linalg.Vector;
import org.apache.spark.ml.linalg.Vectors;
import org.apache.spark.ml.regression.GeneralizedLinearRegressionModel;
import org.apache.spark.ml.regression.GeneralizedLinearRegressionTrainingSummary;
import org.apache.spark.ml.regression.LinearRegressionModel;
import org.apache.spark.ml.regression.LinearRegressionTrainingSummary;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import test.CloudOSTest;

/** Created by philipperibeiro on 11/27/16. */
@Ignore
public class AzurePricingMLTest extends CloudOSTest {

  private static AzurePricingML azurePricingML;

  private static JavaRDD<LabeledPoint> points;

  private static Dataset<Row> training;

  @Value("${spring.data.mongodb.host}")
  private String host;

  @Value("${spring.data.mongodb.port}")
  private Integer port;

  @Value("${spring.data.mongodb.database}")
  private String database;

  @Value("${spark.master.uri}")
  private String sparkMaster;

  @Value("${spring.data.mongodb.username}")
  private String username;

  @Value("${spring.data.mongodb.password}")
  private String password;

  @Autowired private AzureInstanceRepository instanceRepository;

  @Autowired private AzurePricingRepository pricingRepository;

  private static boolean setUpfinished = false;

  // @Configuration
  // @TestPropertySource(value="classpath:application-test.properties")
  // static class ContextConfiguration {
  // }

  @Before
  public void setUp() {
    if (setUpfinished) {
      return;
    }

    setUpfinished = true;
  }

  @Test
  public void testGetSample() {
    List<AzureInstance> instances = azurePricingML.getSample(0, 10);
    assertNotNull(instances);
    assertNotEquals(instances.size(), 0);
  }

  @Test
  @Ignore
  public void testGetInstancePrice() {
    // List<Pair<String, Double>> pricing = azurePricingML.getInstancePrice("A4 VM");
    // assertNotNull(pricing);
    // assertNotEquals(pricing.size(), 0);
  }

  @Test
  @Ignore
  public void testConvertJavaRDDToDataset() {
    assertNotNull(training);
    assertNotEquals(training.count(), 0);
  }

  @Test
  @Ignore
  @SuppressWarnings("Duplicates")
  public void testLinearModeling() {
    LinearRegressionModel lrModel = azurePricingML.linearModeling(training);
    assertNotNull(training);
    logger.debug("----------- Linear Model Pricing ------------");
    // Print the coefficients and intercept for linear regression.
    logger.debug("Coefficients: " + lrModel.coefficients() + " Intercept: " + lrModel.intercept());
    logger.debug("Equation: " + MLUtils.getFunction(lrModel.coefficients().toArray(), 5));
    // Summarize the model over the training set and print out some metrics.
    LinearRegressionTrainingSummary trainingSummary = lrModel.summary();
    logger.debug("numIterations: " + trainingSummary.totalIterations());
    logger.debug("objectiveHistory: " + Vectors.dense(trainingSummary.objectiveHistory()));
    trainingSummary.residuals().show();
    logger.debug("RMSE: " + trainingSummary.rootMeanSquaredError());
    logger.debug("R2: " + trainingSummary.r2());
    logger.debug("----------------------------------------------");

    logger.debug("----------- Sample Pricing -------------------");
    List<LabeledPoint> sample = points.collect();
    // Let's just check 10 prices:
    for (int i = 0; i < sample.size() / 2; i++) {
      LabeledPoint point = sample.get(i);
      Vector feature = point.features();
      double label = point.label();
      if (label != 0) {
        double dotproduct = MLUtils.dot(feature, lrModel.coefficients());
        logger.debug(
            String.format(
                "Feature Vector: %s --- Label: %s ----- Predictor: %s",
                feature, label, dotproduct));
      }
    }
    logger.debug("----------------------------------------------");
  }

  @Ignore
  @Test
  @SuppressWarnings("Duplicates")
  public void testGeneralizedModeling() {
    GeneralizedLinearRegressionModel model = azurePricingML.generalizedModeling(training);
    // Print the coefficients and intercept for generalized linear regression model

    logger.debug("-------------------------------------");
    logger.debug("Coefficients: " + model.coefficients());
    logger.debug("Equation: " + MLUtils.getFunction(model.coefficients().toArray(), 5));
    logger.debug("Intercept: " + model.intercept());

    // Summarize the model over the training set and print out some metrics
    GeneralizedLinearRegressionTrainingSummary summary = model.summary();
    logger.debug(
        "Coefficient Standard Errors: " + Arrays.toString(summary.coefficientStandardErrors()));
    logger.debug("T Values: " + Arrays.toString(summary.tValues()));
    logger.debug("P Values: " + Arrays.toString(summary.pValues()));
    logger.debug("Dispersion: " + summary.dispersion());
    logger.debug("Null Deviance: " + summary.nullDeviance());
    logger.debug("Residual Degree Of Freedom Null: " + summary.residualDegreeOfFreedomNull());
    logger.debug("Deviance: " + summary.deviance());
    logger.debug("Residual Degree Of Freedom: " + summary.residualDegreeOfFreedom());
    logger.debug("AIC: " + summary.aic());
    logger.debug("Deviance Residuals: ");
    summary.residuals().show();
    logger.debug("-------------------------------------");

    logger.debug("----------- Sample Pricing -------------------");
    List<LabeledPoint> sample = points.collect();
    // Let's just check 10 prices:
    for (int i = 0; i < sample.size() / 2; i++) {
      LabeledPoint point = sample.get(i);
      Vector feature = point.features();
      double label = point.label();
      if (label != 0) {
        double dotproduct = MLUtils.dot(feature, model.coefficients());
        logger.debug(
            String.format(
                "Feature Vector: %s --- Label: %s ----- Predictor: %s",
                feature, label, dotproduct));
      }
    }
    logger.debug("----------------------------------------------");
  }
}
