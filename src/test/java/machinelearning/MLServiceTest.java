package machinelearning;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.hamcrest.number.OrderingComparison;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DuplicateKeyException;

import aima.core.util.datastructure.LabeledGraph;
import cloudos.Providers;
import cloudos.exceptions.NotImplementedException;
import cloudos.instances.InstanceService;
import cloudos.machinelearning.MLService;
import cloudos.machinelearning.models.CloudGraph;
import cloudos.machinelearning.models.CloudGraphCost;
import cloudos.machinelearning.models.EquivalentRegion;
import cloudos.models.Instance;
import cloudos.models.InstanceStatus;
import cloudos.optimizer.models.CloudEquivalenceRequest;
import cloudos.pricing.PricingInfo;
import cloudos.pricing.PricingInfoRepository;
import cloudos.provider.ProviderMachineType;
import cloudos.provider.ProvidersService;
import cloudos.utils.PropertySplitter;
import test.CloudOSTest;

public class MLServiceTest extends CloudOSTest {

  @Autowired
  PropertySplitter propertySplitter;

  @Autowired
  MLService mlService;

  @Autowired
  EquivalentRegion equivalentRegion;

  @Autowired
  ProvidersService providersService;

  @Autowired
  PricingInfoRepository pricingInfoRepository;

  @Autowired
  private InstanceService instanceService;

  @Value("#{PropertySplitter.map('${cloudos.ml.equivalence.google}')}")
  private Map<String, String> googleEquivalenceMap;

  @Value("#{PropertySplitter.map('${cloudos.ml.equivalence.aws}')}")
  private Map<String, String> amazonEquivalenceMap;

  @Before
  public void setup() throws IOException {
    try {
      importJSON("provider_machine_type", "provider_machine_type.json");
    } catch (DuplicateKeyException e) {
      // data already in database
      logger.debug("not imported");
    }
  }

  @Test
  public void testPropertySplitter() {
    // check the google map
    assertNotNull(googleEquivalenceMap);
    logger.debug(googleEquivalenceMap);

    // check the amazon map
    assertNotNull(amazonEquivalenceMap);
    logger.debug(amazonEquivalenceMap);
  }

  @Test
  public void testGetEquivalentRegions() {
    // check the google map
    assertNotNull(equivalentRegion.getAmazonEquivalenceMap());
    logger.debug(equivalentRegion.getAmazonEquivalenceMap());

    // check the amazon map
    assertNotNull(equivalentRegion.getGoogleEquivalenceMap());
    logger.debug(equivalentRegion.getGoogleEquivalenceMap());
  }

  @Test
  @Ignore
  public void testGetPriceDifference() throws NotImplementedException {
    // test the case the two instances are the same
    ProviderMachineType first = this.providersService.retrieveMachineType(Providers.AMAZON_AWS, "t2.nano", "us-east-1");
    ProviderMachineType second = first;

    // base case
    double costDifference = this.mlService.getPriceDifference(first, second);
    assertEquals(0.0, costDifference, 0.001);

    second = this.providersService.retrieveMachineType(Providers.AMAZON_AWS,"t1.micro", "us-east-1");
    costDifference = this.mlService.getPriceDifference(first, second);
    assertEquals(costDifference, 0.0, 0.001);

  }

  @Test
  @Ignore
  public void testFindMachineTypeBound() {
    // initialize

    // find one instance by name
    Instance amazonInstance = Instance.builder()
        .spot(false)
        .provider(Providers.AMAZON_AWS)
        .region("ap-northeast-1")
        .machineType("t2.nano")
        .status(InstanceStatus.TERMINATED)
        .providerId("b7582")
        .deleted(true)
        .resourceGroup("securityGroup")
        .imageType("ami-859bbfe2")
        .imageProject("Linux")
        .build();

    amazonInstance = this.instanceService.save(amazonInstance);

    logger.debug("Finding the equivalent instance for t2.nano: {}", amazonInstance);
    ProviderMachineType machineType = this.mlService.findMachineTypeBound(
        amazonInstance.getProviderId(), true, true
    );
    // the smallest instance should be equal
    assertNotNull(machineType);
    assertEquals(machineType.getKey(), amazonInstance.getMachineType());

    logger.debug("Finding the equivalent instance for t2.nano: {}", amazonInstance);
     machineType = this.mlService.findMachineTypeBound(
        amazonInstance.getProviderId(), false, true
    );
    assertNotNull(machineType);
    assertNotEquals(machineType.getKey(), amazonInstance.getMachineType());

    // delete the instance afterwards
    this.instanceService.delete(amazonInstance.getId());
  }
  
  @Test
  public void testGetMachineType() {
    // initialize
    
    // find one instance by name
    Instance amazonInstance = Instance.builder()
        .spot(false)
        .provider(Providers.AMAZON_AWS)
        .region("ap-northeast-1")
        .machineType("t2.nano")
        .status(InstanceStatus.TERMINATED)
        .providerId("b7582")
        .deleted(true)
        .resourceGroup("securityGroup")
        .imageType("ami-859bbfe2")
        .imageProject("Linux")
        .build();
    
    amazonInstance = this.instanceService.save(amazonInstance);
    
    logger.debug("Finding the equivalent instance for t2.nano: {}", amazonInstance);
    ProviderMachineType machineType = this.mlService.getMachineType(amazonInstance.getProviderId());
    // the smallest instance should be equal
    assertNotNull(machineType);
    assertEquals(machineType.getKey(), amazonInstance.getMachineType());
    
    // delete the instance afterwards
    this.instanceService.delete(amazonInstance.getId());
  }

  @Test
  public void testMachineTypeComparator() throws NotImplementedException {

    List<ProviderMachineType> machineTypeList =
        this.providersService.retrieveMachineTypes(Providers.AMAZON_AWS);

    assertNotNull(machineTypeList);
    // now use the comparator
    Collections.sort(machineTypeList, mlService.memoryComparator);
    if (machineTypeList.size() > 1) {
      assertThat(machineTypeList.get(1).getMemory(),
          OrderingComparison.greaterThanOrEqualTo(machineTypeList.get(0).getMemory()));
    }
  }

  @Test
  public void testCloudCostComparison() {
    CloudEquivalenceRequest equivalence = CloudEquivalenceRequest.builder()
                                                  .region("us-east1")
                                                  .fromProvider(Providers.GOOGLE_COMPUTE_ENGINE)
                                                  .toProvider(Providers.AMAZON_AWS).build();
    // there should be no running instances
    CloudGraphCost graphCost = this.mlService.cloudCostComparison(equivalence);
    assertNull(graphCost);
  }

  @Test
  public void testGetEquivalentCloud() {
    LabeledGraph<Instance, Instance> labeledGraph = new LabeledGraph<>();
      // create 5 instances and add to the graph
      for (int i = 1; i <= 5; i++) {
        Instance instance = Instance.builder()
            .spot(false)
            .provider(Providers.GOOGLE_COMPUTE_ENGINE)
            .region("us-east4")
            .machineType("f1-micro")
            .imageType("Linux")
            .imageProject("ubuntu-os-cloud")
            .status(InstanceStatus.RUNNING)
            .name("b1717")
            .deleted(true)
            .cost(0.0092).build();

        // add the instance to the graph
        labeledGraph.addVertex(instance);
      }

    CloudGraph graph = CloudGraph.builder()
                            .region("us-east4")
                            .provider(Providers.GOOGLE_COMPUTE_ENGINE)
                            .graph(labeledGraph).build();

    CloudGraph equivalent = this.mlService.getEquivalentCloud(graph, Providers.AMAZON_AWS);
    assertNotNull(equivalent);
    assertNotNull(equivalent.getGraph());
    assertEquals(equivalent.getRegion(), "us-east-1");
    assertEquals(equivalent.getProvider(), Providers.AMAZON_AWS);
  }

  @Test
  public void testGetEquivalentInstance() {
    // test for a Google instance
    Instance googleInstance = Instance.builder()
                                    .spot(false)
                                    .provider(Providers.GOOGLE_COMPUTE_ENGINE)
                                    .region("us-east1")
                                    .machineType("f1-micro")
                                    .imageType("Linux")
                                    .imageProject("ubuntu-os-cloud")
                                    .status(InstanceStatus.RUNNING)
                                    .name("b1717")
                                    .deleted(true).build();

    Instance equivalent1 = this.mlService.getEquivalentInstance(googleInstance, "us-east-1",
        Providers.AMAZON_AWS);

    if (equivalent1 == null) {
      return;
    }

    // check the status
    assertNotNull(equivalent1);
    assertEquals(equivalent1.getCost(), 0.02, 0.0);
    assertEquals(equivalent1.getMachineType(), "t1.micro");
    assertEquals(equivalent1.getRegion(), "us-east-1");
    assertEquals(equivalent1.getProvider(), Providers.AMAZON_AWS);

    // test for an Amazon AWS instance
    Instance amazonInstance = Instance.builder()
                                    .spot(false)
                                    .provider(Providers.AMAZON_AWS)
                                    .region("ap-northeast-1")
                                    .machineType("t2.nano")
                                    .status(InstanceStatus.TERMINATED)
                                    .name("b7582")
                                    .deleted(true)
                                    .resourceGroup("securityGroup")
                                    .imageType("ami-859bbfe2")
                                    .imageProject("Linux").build();

    Instance equivalent2 = this.mlService.getEquivalentInstance(amazonInstance, "asia-northeast1",
        Providers.GOOGLE_COMPUTE_ENGINE);
    assertNotNull(equivalent2);
    assertEquals(equivalent2.getCost(), 0.0092, 0.0);
    assertEquals(equivalent2.getMachineType(), "f1-micro");
    assertEquals(equivalent2.getProvider(), Providers.GOOGLE_COMPUTE_ENGINE);

    // test for an invalid instance
    assertNull(this.mlService.getEquivalentInstance(equivalent1, "us-east-1", Providers.ALIBABA_ALIYUN));

  }

  @Test
  public void testGetEquivalentRegion() {
    // test google regions
    String region1 = this.equivalentRegion.getEquivalentRegion("us-east1",
        Providers.GOOGLE_COMPUTE_ENGINE);
    assertNotNull(region1);
    assertEquals(region1, "us-east-2");

    // test invalid region
    assertNull(this.equivalentRegion.getEquivalentRegion("invalidregion",
        Providers.GOOGLE_COMPUTE_ENGINE));

    // test aws regions
    String region2 = this.equivalentRegion.getEquivalentRegion("us-west-1",
        Providers.AMAZON_AWS);
    assertNotNull(region2);
    assertEquals(region2, "us-central1");

    // test invalid region
    assertNull(this.equivalentRegion.getEquivalentRegion("invalidregion",
        Providers.AMAZON_AWS));

    // test an invalid region
    assertNull(this.equivalentRegion.getEquivalentRegion("invalidregion",
        Providers.MICROSOFT_AZURE));
  }

  @Test
  public void testAmazonPricing() {
    PricingInfo info = this.pricingInfoRepository.findByPricingVersionAndName(
        "59f909e93891143f44110bf2", "c4.2xlarge-ca-central-1-Windows"
    );
    if (info == null) {
      return;
    }

    // test with a valid region
    double price = this.mlService.getAmazonPricing(info);
    assertNotEquals(price, Double.MIN_VALUE, 0.0);
  }

  @Test
  public void testGetGooglePricing() {
    PricingInfo info = this.pricingInfoRepository.findByPricingVersionAndName(
        "59f90a443891143f4412bd0a", "CP-COMPUTEENGINE-VMIMAGE-F1-MICRO"
    );

    if (info == null) {
      return;
    }

    // test with a valid region
    double price = this.mlService.getGooglePricing(info, "asia-southeast");
    assertNotEquals(price, Double.MIN_VALUE, 0.0);

    // test with an invalid region
    price = this.mlService.getGooglePricing(info, "invalidregion");
    assertEquals(price, Double.MIN_VALUE, 0.0);
  }

  @Test
  public void testComputeUnit() throws NotImplementedException {
    // test for an Google Engine instance type
    String machineType1 = "n1-highmem-8";
    assertEquals(this.mlService.computeUnit(Providers.GOOGLE_COMPUTE_ENGINE, machineType1, "us-east1"), 68.0, 0.01);
    ProviderMachineType machineType3 = this.providersService.retrieveMachineType(Providers.GOOGLE_COMPUTE_ENGINE, machineType1, "us-east1");
    assertEquals(this.mlService.computeUnit(machineType3), 68.0, 0.01);

    // test for an AWS instance type
    String machineType2 = "m4.4xlarge";
    assertEquals(this.mlService.computeUnit(Providers.AMAZON_AWS, machineType2, "us-east-1"), 96.0, 0.01);
    ProviderMachineType machineType4 = this.providersService.retrieveMachineType(Providers.AMAZON_AWS, machineType2, "us-east-1");
    assertEquals(this.mlService.computeUnit(machineType4), 96.0, 0.01);

    // negative test
    assertEquals(this.mlService.computeUnit(Providers.AMAZON_AWS, "invalidtype", "us-east-1"), 0.0, 0.0);
    assertEquals(this.mlService.computeUnit(Providers.AMAZON_AWS, machineType2, "invalidRegion"), 0.0, 0.0);
  }

  @Test
  public void testGetCloudGraphCost() {
    LabeledGraph<Instance, Instance> labeledGraph = new LabeledGraph<>();
    Instance instance1 = new Instance();
    instance1.setCost(1.0);
    instance1.setSpot(false);

    Instance instance2 = new Instance();
    instance2.setSpot(true);
    instance2.setCost(1.0);

    Instance instance3 = new Instance();
    instance3.setSpot(true);
    instance3.setCost(1.0);

    labeledGraph.addVertex(instance1);
    labeledGraph.addVertex(instance2);
    labeledGraph.addVertex(instance3);

    CloudGraph graph = CloudGraph.builder()
                            .region("us-west-1")
                            .provider(Providers.AMAZON_AWS)
                            .graph(labeledGraph).build();

    CloudGraphCost cloudGraphCost = this.mlService.getCloudGraphCost(graph);
    assertNotNull(cloudGraphCost);
    assertEquals(cloudGraphCost.getProvider(), Providers.AMAZON_AWS);
    assertEquals(cloudGraphCost.getReservedInstances(), 0);
    assertEquals(cloudGraphCost.getNumberInstances(), 3);
    assertEquals(cloudGraphCost.getRegion(), "us-west-1");
    assertEquals(cloudGraphCost.getSpotInstances(), 2);
    assertEquals(cloudGraphCost.getOndemandinstances(), 1);
    assertEquals(cloudGraphCost.getCost(), 3.0, 0.01);
  }
}
