package machinelearning;

import static org.junit.Assert.assertTrue;

import cloudos.amazon.AWSCredentialService;
import cloudos.amazon.S3;
import cloudos.machinelearning.Models;
import cloudos.machinelearning.ModelsRepository;
import cloudos.utils.FileUtils;

import com.amazonaws.regions.Regions;

import java.nio.file.Paths;

import org.assertj.core.api.Assertions;
import org.datavec.api.util.ClassPathResource;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import test.CloudOSTest;

public class ModelsRepositoryTest extends CloudOSTest {

  @Autowired ModelsRepository modelsRepository;

  @Autowired AWSCredentialService awsCredentialService;

  /* Define the home directory for the ml products for CloudOS */
  @Value("${cloudos.ml.home}")
  String home;

  private S3 s3;

  @Before
  public void setUp() throws Exception {
    this.s3 = new S3(Regions.US_EAST_1.getName(), awsCredentialService.getAWSCredentials());
  }

  @Test
  public void testInit() throws Exception {
    // init method should have been called in the constructor ...
    String repository = this.modelsRepository.getRepository();
    assertTrue(this.s3.bucketExists(repository));
  }

  @Test
  @Ignore
  public void testUpload() throws Exception {
    final String filename =
        new ClassPathResource("/machinelearning/autoscaler/passengers_raw.csv")
            .getFile()
            .getPath();
    Assertions.assertThatCode(
            () -> {
              this.modelsRepository.upload(Models.AUTOSCALER, Models.Task.DATA, filename);
            })
        .doesNotThrowAnyException();

    // test the extended version
    final String anotherfile = new ClassPathResource("/machinelearning/spotbidder/bidder.zip")
            .getFile()
            .getPath();
    Assertions.assertThatCode(
        () -> {
          this.modelsRepository.upload(Models.SPOTBIDDER, Models.Task.DATA, anotherfile);
        })
        .doesNotThrowAnyException();

  }

  @Test
  public void testDownload() throws Exception {
    Assertions.assertThatCode(
            () -> {
              this.modelsRepository.download(
                  Models.AUTOSCALER, Models.Task.PRODUCTION, "decider.zip");
            })
        .doesNotThrowAnyException();
    String path =
        Paths.get(
                home,
                Models.AUTOSCALER.toString(),
                Models.Task.PRODUCTION.toString(),
                "decider.zip")
            .toString();
    logger.debug(path);
    assertTrue(FileUtils.exits(path));

    Assertions.assertThatCode(
        () -> {
          this.modelsRepository.download(
              Models.SPOTBIDDER, Models.Task.AWS, "eu-west-1", "bidder.zip");
        })
        .doesNotThrowAnyException();

    String anotherPath =
        Paths.get(
            home,
            Models.SPOTBIDDER.toString(),
            Models.Task.AWS.toString(),
            "eu-west-1",
            "bidder.zip")
            .toString();
    logger.debug(path);
    assertTrue(FileUtils.exits(anotherPath));
  }
}
