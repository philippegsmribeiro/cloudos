package machinelearning;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import cloudos.amazon.AmazonInstance;
import cloudos.amazon.AmazonInstanceRepository;
import cloudos.machinelearning.AmazonML;
import cloudos.machinelearning.MLUtils;
import cloudos.pricing.AmazonPricingManagement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

@Ignore
public class AmazonMLTest extends CloudOSTest {

  @Autowired
  private AmazonInstanceRepository instanceRepository;
  @Autowired
  AmazonPricingManagement pricingManagement;


  private AmazonML amazonML;

  @Before
  public void setUp() throws Exception {
    this.amazonML = new AmazonML(instanceRepository, pricingManagement);
  }

  @Test
  public void testModel() {
    List<AmazonInstance> instances = this.amazonML.getSample(0, 100);
    Map<Double, List<Double>> points = this.amazonML.featureExtractor(instances);
    OLSMultipleLinearRegression regression = this.amazonML.model(points);

    /* check if model worked */
    assertNotNull(regression);
    double[] beta = regression.estimateRegressionParameters();
    double[] residuals = regression.estimateResiduals();
    double[][] parametersVariance = regression.estimateRegressionParametersVariance();
    double regressandVariance = regression.estimateRegressandVariance();
    double rSquared = regression.calculateRSquared();
    double sigma = regression.estimateRegressionStandardError();

    assertNotNull(beta);
    assertNotNull(residuals);
    assertNotNull(regressandVariance);
    assertNotNull(rSquared);
    assertNotNull(sigma);

    System.out.println("----------------------------------------------------------------");
    System.out.println(String.format("Beta: %s", Arrays.toString(beta)));
    System.out.println(String.format("Residuals: %s", Arrays.toString(residuals)));
    System.out.println(String.format("regressandVariance: %s", regressandVariance));
    System.out.println(String.format("rSquared: %s", rSquared));
    System.out.println(String.format("sigma: %s", sigma));
    System.out.println("parametersVariance: ");
    MLUtils.printMatrix(parametersVariance);
    System.out.println("Function: " + MLUtils.getFunction(beta, 8));
    System.out.println("----------------------------------------------------------------");
  }

  @Test
  public void regression() {
    final OLSMultipleLinearRegression regression = new OLSMultipleLinearRegression();
    double[] y = {4, 8, 13, 18};
    double[][] x2 = {{1, 1, 1}, {1, 2, 4}, {1, 3, 9}, {1, 4, 16},};

    regression.newSampleData(y, x2);
    regression.setNoIntercept(true);
    regression.newSampleData(y, x2);
    double[] beta = regression.estimateRegressionParameters();
    logger.debug(String.format("Beta: %s", Arrays.toString(beta)));
    logger.debug("Function: " + MLUtils.getFunction(beta, 3));
  }

  @Test
  public void testFeatureExtractor() {
    List<AmazonInstance> instances = this.amazonML.getSample(0, 10);
    Map<Double, List<Double>> map = this.amazonML.featureExtractor(instances);
    /* Check if feature extractor worked */
    assertNotNull(map);
    assertNotEquals(map.size(), 0);

    for (Map.Entry<Double, List<Double>> entry : map.entrySet()) {
      logger.debug(String.format("y = %s / x = %s", entry.getKey(), entry.getValue()));
    }
  }

  @Test
  public void testModeling() {
    List<Vector2D> points = new ArrayList<Vector2D>();
    points.add(new Vector2D(4, 0.376));
    points.add(new Vector2D(2, 0.186));
    points.add(new Vector2D(64, 10.94));
    points.add(new Vector2D(16, 1.164));
    points.add(new Vector2D(16, 2.626));
    points.add(new Vector2D(8, 0.3206));
    points.add(new Vector2D(4, 0.646));
    points.add(new Vector2D(4, 0.393));
    SimpleRegression simpleRegression = this.amazonML.modeling(points);

    // querying for model parameters
    logger.debug("-----------------------------------------------");
    logger.debug("slope = " + simpleRegression.getSlope());
    logger.debug("intercept = " + simpleRegression.getIntercept());

    // trying to run model for unknown data
    logger.debug("prediction for 12 vcpu's = " + simpleRegression.predict(12));
    logger.debug("-----------------------------------------------");
  }

  @Test
  public void testGetInstancePrice() {
    // Let's test only for "OnDemand" now.
    // assertEquals(this.amazonML.getInstancePrice(null, null).size(), 0);
    // assertEquals(this.amazonML.getInstancePrice("", "").size(), 0);
    // assertEquals(this.amazonML.getInstancePrice("", "OnDemand").size(), 0);
    // assertEquals(this.amazonML.getInstancePrice("5Z8AXGNWU49BJJSB", "").size(), 0);
    // assertNotEquals(this.amazonML.getInstancePrice("5Z8AXGNWU49BJJSB", "OnDemand").size(), 0);
  }
}
