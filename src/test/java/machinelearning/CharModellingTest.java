package machinelearning;

import cloudos.machinelearning.nlp.CharModelling;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;

import test.CloudOSTest;

/** Created by philipperibeiro on 3/4/17. */
@Ignore
public class CharModellingTest extends CloudOSTest {

  private CharModelling charModelling;

  @Value("${cloudos.ml.data.folder}")
  private String dataFolder;

  @Value("${cloudos.ml.data.sentiment.url}")
  private String dataUrl;

  @Value("${cloudos.ml.data.word.vectors}")
  private String wordVectorsPath;

  // @Configuration
  // static class ContextConfiguration {
  // }

  @Before
  public void setUp() throws Exception {
    this.charModelling = new CharModelling();
  }

  @Test
  public void testCharModelling() throws Exception {
    this.charModelling.model();
  }
}
