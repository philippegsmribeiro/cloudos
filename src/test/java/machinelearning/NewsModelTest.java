package machinelearning;

import cloudos.machinelearning.nlp.PrepareWordVector;
import cloudos.machinelearning.nlp.TestNews;
import cloudos.machinelearning.nlp.TrainNews;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;

import test.CloudOSTest;

/** Created by philipperibeiro on 3/4/17. */
@Ignore
public class NewsModelTest extends CloudOSTest {

  private TrainNews trainNews;

  private TestNews testNews;

  private PrepareWordVector prepareWordVector;

  @Value("${cloudos.ml.data.newsdata}")
  private String dataFolder;

  // @Configuration
  // static class ContextConfiguration {
  // }

  @Before
  public void setUp() throws Exception {
    this.trainNews = new TrainNews();
    this.testNews = new TestNews();
    this.prepareWordVector = new PrepareWordVector();
  }

  @Test
  public void testNewsModel() throws Exception {

    // word vector paths
    // this.prepareWordVector.prepare(this.dataFolder);

    // train the news model first
    // this.trainNews.train(this.dataFolder);
    // test the news model

    this.testNews.test(this.dataFolder);
  }
}
