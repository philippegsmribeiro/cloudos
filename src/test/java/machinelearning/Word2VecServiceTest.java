package machinelearning;

import cloudos.machinelearning.nlp.Word2VecService;

import java.io.File;
import java.util.Collection;

import org.deeplearning4j.models.word2vec.Word2Vec;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;

import test.CloudOSTest;

/** Created by philipperibeiro on 2/28/17. */
@Ignore
public class Word2VecServiceTest extends CloudOSTest {

  private Word2VecService word2VecService;

  @Value("${cloudos.ml.data.folder}")
  private String dataFolder;

  @Value("${cloudos.ml.data.sentiment.url}")
  private String dataUrl;

  @Value("${cloudos.ml.data.word.vectors}")
  private String wordVectorsPath;

  // @Configuration
  // static class ContextConfiguration {
  // }

  @Before
  public void setUp() throws Exception {
    this.word2VecService = new Word2VecService();
  }

  @Test
  public void testClosestWords() throws Exception {
    String rawSentence = this.dataFolder + File.separator + "raw_sentences.txt";
    logger.debug("Training Word2Vec with file data = " + rawSentence);
    Word2Vec word2Vec = this.word2VecService.build(rawSentence);

    Collection<String> list = word2Vec.wordsNearest("day", 10);
    logger.debug("10 Words closest to 'day': " + list);

    list = word2Vec.wordsNearest("exception", 10);
    logger.debug("10 Words closest to 'exception': " + list);
  }

  @Test
  public void testLogFile() throws Exception {
    String apacheLogs = this.dataFolder + File.separator + "error_log";
    logger.debug("Training Word2Vec with file data = " + apacheLogs);
    Word2Vec word2Vec = this.word2VecService.build(apacheLogs);

    Collection<String> list = word2Vec.wordsNearest("reset", 10);
    logger.debug("10 Words closest to 'reset': " + list);

    list = word2Vec.wordsNearest("GET", 10);
    logger.debug("10 Words closest to 'GET': " + list);
  }
}
