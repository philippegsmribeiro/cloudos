package machinelearning;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;

import com.fasterxml.jackson.core.type.TypeReference;

import cloudos.Providers;
import cloudos.instances.InstanceService;
import cloudos.machinelearning.DatapointFeatureExtrator;
import cloudos.machinelearning.autoscaler.DatapointVector;
import cloudos.models.ClientStatus;
import cloudos.models.CloudosDatapoint;
import cloudos.models.CloudosDatapointsRepository;
import cloudos.models.Instance;
import cloudos.models.InstanceStatus;
import cloudos.utils.FileUtils;
import scala.Tuple2;
import test.CloudOSTest;

/** Created by philipperibeiro on 7/21/17. */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DatapointFeatureExtratorTest extends CloudOSTest {

  @Autowired
  CloudosDatapointsRepository datapointsRepository;

  @Autowired
  DatapointFeatureExtrator datapointFeatureExtrator;

  @Autowired
  InstanceService instanceService;

  @Value("${cloudos.ml.home}")
  String path;

  static String destination =  null;
  static String merged = null;

  static List<CloudosDatapoint> datapointList;

  @Before
  public void setUp() throws Exception {
    if (datapointList == null || datapointList.size() == 0) {
      destination =  path + File.separator + String.format("datapoints_%s.csv", generator.generate(10));
      merged = path + File.separator + String.format("merged_datapoints_%s.csv", generator.generate(10));

      datapointList =
          datapointsRepository.findByProvider(Providers.AMAZON_AWS, new PageRequest(0, 100));
      if (CollectionUtils.isEmpty(datapointList)) {
        // read from the file with data
        byte[] readAllLines =
            Files.readAllBytes(Paths.get(this.getClass().getResource("datapoints.json").toURI()));
        List<CloudosDatapoint> values =
            mapper.readValue(readAllLines, new TypeReference<List<CloudosDatapoint>>() {});
        datapointsRepository.save(values);
        datapointList =
            datapointsRepository.findByProvider(Providers.AMAZON_AWS, new PageRequest(0, 100));
      }
    }
  }

  @Test
  public void test1DatapointVector() throws Exception {
    CloudosDatapoint datapoint = datapointList.get(0);
    DatapointVector vector = new DatapointVector(datapoint);

    assertNotNull(vector.getName());
    assertNotNull(vector.getTimestamp());
    assertNotNull(vector.getVector());
    logger.debug(vector);
  }

  @Test
  public void test2ExtractFromList() throws Exception {
    if (destination == null || datapointList == null) {
      return;
    }
    this.datapointFeatureExtrator.extract(datapointList, destination);
    File file = new File(destination);
    assertTrue(file.exists());
  }

  @Test
  public void test3Merge() throws Exception {
    this.datapointFeatureExtrator.merge(destination, merged);
    File file = new File(merged);
    assertTrue(file.exists());

    this.datapointFeatureExtrator.labels(merged, path);
    FileUtils.delete(file);
    FileUtils.delete(new File(destination));
  }

  @Test
  @Ignore
  public void testNormalize() throws Exception {
    List<CloudosDatapoint> datapoints =
        this.datapointsRepository.findByProvider(Providers.AMAZON_AWS, new PageRequest(0, 10));

    if (datapoints == null) {
      return;
    }

    if (datapoints.size() > 0) {

      CloudosDatapoint datapoint = datapoints.get(0);

   // create fake instance just for testing
      Instance instance = new Instance();
      instance.setProvider(Providers.AMAZON_AWS);
      instance.setClientInstalled(true);
      instance.setClientStatus(ClientStatus.INSTALLED);
      instance.setDeleted(false);
      instance.setRegion("us-west-1");
      instance.setMachineType("t1.micro");
      instance.setProviderId(datapoint.getCloudId());
      instance.setStatus(InstanceStatus.RUNNING);
      instance = instanceService.save(instance);

      DatapointVector awsVector = this.datapointFeatureExtrator.normalize(datapoint);
      // delete it
      instanceService.delete(instance.getId());
      if (awsVector == null) {
        return;
      }
      // now test for Google
      datapoints = this.datapointsRepository.findByProvider(Providers.GOOGLE_COMPUTE_ENGINE,
          new PageRequest(0, 10));

      if (datapoints.size() > 0) {

        datapoint = datapoints.get(0);

        // create fake instance just for testing
        instance = new Instance();
        instance.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
        instance.setClientInstalled(true);
        instance.setClientStatus(ClientStatus.INSTALLED);
        instance.setDeleted(false);
        instance.setRegion("us-west1");
        instance.setMachineType("t1.micro");
        instance.setProviderId(datapoint.getCloudId());
        instance.setStatus(InstanceStatus.RUNNING);
        instance = instanceService.save(instance);

        // delete it
        DatapointVector googleVector = this.datapointFeatureExtrator.normalize(datapoint);
        instanceService.delete(instance.getId());
        if (googleVector == null) {
          return;
        }
      }
    }
  }

  @Test
  @Ignore
  public void test4Extract() throws Exception {
    Map<Tuple2<Long, String>, DatapointVector> result =
        this.datapointFeatureExtrator.extract(datapointList);

    assertNotNull(result);
    result.forEach((key, value) -> {
      logger.debug("Key {} has values {}", key, value);
    });
  }
}
