package machinelearning;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import aima.core.search.csp.Assignment;
import aima.core.search.csp.CSP;
import aima.core.search.csp.CSPStateListener;
import aima.core.search.csp.Constraint;
import aima.core.search.csp.Domain;
import aima.core.search.csp.ImprovedBacktrackingStrategy;
import aima.core.search.csp.Variable;

import cloudos.amazon.AmazonInstance;
import cloudos.amazon.AmazonInstanceRepository;
import cloudos.machinelearning.CapacityConstraint;
import cloudos.machinelearning.CloudCSP;
import cloudos.machinelearning.FeatureConstraint;
import cloudos.machinelearning.InstanceVariable;
import cloudos.machinelearning.MLUtils;
import cloudos.pricing.AmazonPricingManagement;
import cloudos.pricing.PricingValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import test.CloudOSTest;

@Ignore
public class CloudConstraintTest extends CloudOSTest {

  @Autowired private MongoTemplate mongoTemplate;

  @Autowired private AmazonInstanceRepository instanceRepository;

  @Autowired
  AmazonPricingManagement pricingManagement;

  private static boolean initialized = false;
  private static final int SAMPLE_SIZE = 10;

  /* I'm defining the Domain to be all the possible instances in the cloud provider */
  private static Domain domains;

  @Before
  @SuppressWarnings({"unchecked"})
  public void setUp() throws Exception {
    if (!initialized) {
      List<String> instanceTypes =
          mongoTemplate.getCollection("aws_instances").distinct("attributes.instanceType");

      domains = new Domain(instanceTypes);
      initialized = true;
    }
  }

  @Test
  public void testCapacityConstraint() {
    /**
     * In the Scenario 1, all the instances are the same. We will just attempt to optimize based on
     * one of the Instance's attributes.
     */
    logger.debug(
        "###################### Scenario 1: Capacity Constraint ################################");
    List<Variable> variables = new ArrayList<>();
    final String instanceType = "m1.large";
    AmazonInstance instance = this.instanceRepository.findOneByAttributesInstanceType(instanceType);

    for (int i = 1; i <= SAMPLE_SIZE; i++) {
      String name = String.format("Instance_%s", i);
      Variable variable = new InstanceVariable(name, instance, instanceType);
      variables.add(variable);
      logger.debug(String.format("Created variable '%s'", variable));
    }

    List<Constraint> constraint =
        Arrays.asList(
            new CapacityConstraint(variables.get(0), variables.get(9)),
            new CapacityConstraint(variables.get(1), variables.get(8)),
            new CapacityConstraint(variables.get(2), variables.get(7)),
            new CapacityConstraint(variables.get(3), variables.get(6)),
            new CapacityConstraint(variables.get(4), variables.get(5)));
    // set the repository before trying to attempt accessing anything
    ((CapacityConstraint) constraint.get(0)).setAmazonInstanceRepository(this.instanceRepository);

    CloudCSP csp = new CloudCSP(variables, domains, constraint);

    ImprovedBacktrackingStrategy ibs = new ImprovedBacktrackingStrategy();
    ibs.addCSPStateListener(
        new CSPStateListener() {

          @Override
          public void stateChanged(Assignment assignment, CSP csp) {
            logger.debug("Assignment evolved: " + assignment);
          }

          @Override
          public void stateChanged(CSP csp) {
            logger.debug("CSP evolved: " + csp);
          }
        });

    long start = System.currentTimeMillis();
    Assignment sol = ibs.solve(csp);
    long end = System.currentTimeMillis();
    long time = end - start;
    logger.debug("*****************************************************");
    logger.debug(String.format("########## Solution Improved Backtracking: %s ############", sol));
    logger.debug(
        String.format(
            "########## Time to solve: (%d in minutes), (%d in seconds) ############",
            TimeUnit.MILLISECONDS.toMinutes(time),
            TimeUnit.MILLISECONDS.toSeconds(time)
                - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time))));
    logger.debug("*****************************************************");

    logger.debug("###################################################################");
  }

  @Test
  public void testFeatureVectorConstraint() {
    logger.debug(
        "###################### Scenario 2: Feature Vector Constraint ################################");
    List<Variable> variables = new ArrayList<>();
    List<Constraint> constraints = new ArrayList<>();

    final String instanceType = "m1.large";
    AmazonInstance instance = this.instanceRepository.findOneByAttributesInstanceType(instanceType);
    List<Double> featureVector =
        MLUtils.elementWiseMultiplication(instance.getFeatureVector(), SAMPLE_SIZE);

    logger.debug(String.format("Instance feature vector: %s", instance.getFeatureVector()));
    logger.debug(String.format("Scaled Feature Vector: %s", featureVector));
    PricingValue pricingForInstance =
        pricingManagement.getPricingForInstance(instance.getInstance());
    assertNotNull(pricingForInstance);
    assertNotEquals(pricingForInstance.getValue(), 0);

    Double price = pricingForInstance.getValue();
    logger.debug(String.format("Instance Price: %s", price));

    for (int i = 1; i <= SAMPLE_SIZE; i++) {
      String name = String.format("Instance_%s", i);
      Variable variable = new InstanceVariable(name, instance, instanceType);
      variables.add(variable);
      constraints.add(new FeatureConstraint(variable, price));

      logger.debug(String.format("Created variable '%s'", variable));
    }

    // set the repository before trying to attempt accessing anything
    ((FeatureConstraint) constraints.get(0)).setAmazonInstanceRepository(this.instanceRepository);
    ((FeatureConstraint) constraints.get(0))
        .setPricingManagement(pricingManagement);
    CloudCSP csp = new CloudCSP(variables, domains, constraints);

    ImprovedBacktrackingStrategy ibs = new ImprovedBacktrackingStrategy();
    ibs.addCSPStateListener(
        new CSPStateListener() {

          @Override
          public void stateChanged(Assignment assignment, CSP csp) {
            logger.debug("Assignment evolved: " + assignment);
          }

          @Override
          public void stateChanged(CSP csp) {
            logger.debug("CSP evolved: " + csp);
          }
        });

    long start = System.currentTimeMillis();
    Assignment sol = ibs.solve(csp);
    long end = System.currentTimeMillis();
    long time = end - start;
    logger.debug("*****************************************************");
    logger.debug(String.format("########## Solution Improved Backtracking: %s ############", sol));
    logger.debug(
        String.format(
            "########## Time to solve: (%d in minutes), (%d in seconds) ############",
            TimeUnit.MILLISECONDS.toMinutes(time),
            TimeUnit.MILLISECONDS.toSeconds(time)
                - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time))));
    logger.debug("*****************************************************");

    logger.debug("###################################################################");
  }

  @Test
  public void testFeatureVectorConstraintWithKeep() {
    logger.debug(
        "###################### Scenario 2: Feature Vector Constraint ################################");
    List<Variable> variables = new ArrayList<>();
    List<Constraint> constraints = new ArrayList<>();

    final String instanceType = "m1.large";
    AmazonInstance instance = this.instanceRepository.findOneByAttributesInstanceType(instanceType);
    List<Double> featureVector =
        MLUtils.elementWiseMultiplication(instance.getFeatureVector(), SAMPLE_SIZE);

    logger.debug(String.format("Instance feature vector: %s", instance.getFeatureVector()));
    logger.debug(String.format("Scaled Feature Vector: %s", featureVector));
    PricingValue priceValue = pricingManagement.getPricingForInstance(instance.getInstance());
    assertNotNull(priceValue);
    assertNotEquals(priceValue.getValue(), 0);

    Double price = priceValue.getValue();
    logger.debug(String.format("Instance Price: %s", price));

    for (int i = 1; i <= SAMPLE_SIZE; i++) {
      String name = String.format("Instance_%s", i);
      Variable variable = new InstanceVariable(name, instance, instanceType);
      variables.add(variable);
      if (i % 4 == 0) {
        ((InstanceVariable) variable).setKeep(true);
      }
      constraints.add(new FeatureConstraint(variable, price));

      logger.debug(String.format("Created variable '%s'", variable));
    }

    // set the repository before trying to attempt accessing anything
    ((FeatureConstraint) constraints.get(0)).setAmazonInstanceRepository(this.instanceRepository);
    ((FeatureConstraint) constraints.get(0))
        .setPricingManagement(pricingManagement);
    CloudCSP csp = new CloudCSP(variables, domains, constraints);

    ImprovedBacktrackingStrategy ibs = new ImprovedBacktrackingStrategy();
    ibs.addCSPStateListener(
        new CSPStateListener() {

          @Override
          public void stateChanged(Assignment assignment, CSP csp) {
            logger.debug("Assignment evolved: " + assignment);
          }

          @Override
          public void stateChanged(CSP csp) {
            logger.debug("CSP evolved: " + csp);
          }
        });

    long start = System.currentTimeMillis();
    Assignment sol = ibs.solve(csp);
    long end = System.currentTimeMillis();
    long time = end - start;
    logger.debug("*****************************************************");
    logger.debug(String.format("########## Solution Improved Backtracking: %s ############", sol));
    logger.debug(
        String.format(
            "########## Time to solve: (%d in minutes), (%d in seconds) ############",
            TimeUnit.MILLISECONDS.toMinutes(time),
            TimeUnit.MILLISECONDS.toSeconds(time)
                - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time))));
    logger.debug("*****************************************************");

    logger.debug("###################################################################");
  }
}
