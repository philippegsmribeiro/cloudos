package machinelearning.optimizer;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cloudos.machinelearning.optimization.OfferingTypeOptimizer;
import test.CloudOSTest;

public class OfferingTypeOptimizerTest extends CloudOSTest {
  
  @Autowired
  OfferingTypeOptimizer offeringTypeOptimizer;
  
  @Test
  @Ignore
  public void testTrain() throws Exception {
    offeringTypeOptimizer.train();
  }
}
