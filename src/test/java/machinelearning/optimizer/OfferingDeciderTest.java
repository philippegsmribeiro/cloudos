package machinelearning.optimizer;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import cloudos.machinelearning.optimization.OfferingDecider;
import test.CloudOSTest;

public class OfferingDeciderTest extends CloudOSTest {
  
  private OfferingDecider offeringDecider;
  
  @Before
  public void setUp() throws Exception {
    offeringDecider = new OfferingDecider();
  }
  
  @Test
  @Ignore
  public void testDecider() throws Exception {
    offeringDecider.train();
  }
}
