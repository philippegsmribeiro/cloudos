package machinelearning;

import static org.junit.Assert.assertTrue;

import cloudos.machinelearning.MetricbeatFeatureExtractor;

import java.io.File;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;

import test.CloudOSTest;

/** Created by philipperibeiro on 7/11/17. */
public class MetricbeatFeatureExtractorTest extends CloudOSTest {

  static MetricbeatFeatureExtractor featureExtractor;

  @Value("${cloudos.ml.data.folder}/autoscaler")
  private String path;

  @Value("${cloudos.ml.data.folder}/autoscaler/vector.csv")
  private String destination;

  @Value("${cloudos.ml.data.folder}/autoscaler/merged.csv")
  private String merged;

  @Value("${cloudos.ml.data.folder}/autoscaler/sorted.csv")
  private String sorted;

  @BeforeClass
  public static void setUp() throws Exception {
    featureExtractor = new MetricbeatFeatureExtractor();
  }

  @Test
  @Ignore
  public void testReadStream() throws Exception {
    File file = new File(destination);
    // delete old file
    if (file.exists()) {
      assertTrue(file.delete());
    }
    String destination = file.getAbsolutePath();
    logger.debug("Destination: {}", destination);
    featureExtractor.readStream(path, destination);
    assertTrue(file.exists());
  }

  @Test
  @Ignore
  public void testStreamMerge() throws Exception {
    featureExtractor.mergeFiles(merged, sorted);
  }

  @AfterClass
  public static void tearDown() throws Exception {
    featureExtractor.stop();
  }
}
