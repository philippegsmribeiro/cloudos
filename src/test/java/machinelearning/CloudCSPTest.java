package machinelearning;

import aima.core.search.csp.Assignment;
import aima.core.search.csp.CSP;
import aima.core.search.csp.CSPStateListener;
import aima.core.search.csp.Constraint;
import aima.core.search.csp.Domain;
import aima.core.search.csp.ImprovedBacktrackingStrategy;
import aima.core.search.csp.Variable;

import cloudos.machinelearning.BacktrackingStrategy;
import cloudos.machinelearning.CloudCSP;
import cloudos.machinelearning.InstanceVariable;
import cloudos.machinelearning.NotEqualConstraint;
import cloudos.machinelearning.TreeCSPSolver;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import test.CloudOSTest;

@Ignore
public class CloudCSPTest extends CloudOSTest {

  private static final Variable NSW = new Variable("NSW");
  private static final Variable NT = new Variable("NT");
  private static final Variable Q = new Variable("Q");
  private static final Variable SA = new Variable("SA");
  private static final Variable T = new Variable("T");
  private static final Variable V = new Variable("V");
  private static final Variable WA = new Variable("WA");
  private static final String RED = "RED";
  private static final String GREEN = "GREEN";
  private static final String BLUE = "BLUE";

  private static boolean initialized = false;
  private static CloudCSP csp;

  @Before
  public void setUp() throws Exception {
    if (!initialized) {
      List<Variable> variables = Arrays.asList(NSW, WA, NT, Q, SA, V, T);
      Domain colors = new Domain(new Object[] {RED, GREEN, BLUE});
      List<Constraint> constraint =
          Arrays.asList(
              new NotEqualConstraint(WA, NT),
              new NotEqualConstraint(WA, SA),
              new NotEqualConstraint(NT, SA),
              new NotEqualConstraint(NT, Q),
              new NotEqualConstraint(SA, Q),
              new NotEqualConstraint(SA, NSW),
              new NotEqualConstraint(SA, V),
              new NotEqualConstraint(Q, NSW),
              new NotEqualConstraint(NSW, V));
      initialized = false;
      csp = new CloudCSP(variables, colors, constraint);
    }
  }

  @Test
  public void testInstanceVariable() {
    Variable BW = new InstanceVariable("BW");
    Variable BY = new InstanceVariable("BY");
    Variable BE = new InstanceVariable("BE");
    List<Variable> variables = Arrays.asList(BW, BY, BE);
    Domain colors = new Domain(new Object[] {RED, GREEN, BLUE});
    List<Constraint> constraint =
        Arrays.asList(
            new NotEqualConstraint(BW, BY),
            new NotEqualConstraint(BW, BE),
            new NotEqualConstraint(BE, BY));
    CloudCSP simpleCsp = new CloudCSP(variables, colors, constraint);
    ImprovedBacktrackingStrategy ibs = new ImprovedBacktrackingStrategy();
    ibs.addCSPStateListener(
        new CSPStateListener() {

          @Override
          public void stateChanged(Assignment assignment, CSP csp) {
            logger.debug("Assignment evolved: " + assignment);
          }

          @Override
          public void stateChanged(CSP csp) {
            logger.debug("CSP evolved: " + csp);
          }
        });

    double start = System.currentTimeMillis();
    Assignment sol = ibs.solve(simpleCsp);
    double end = System.currentTimeMillis();
    logger.debug("##################################################");
    logger.debug(
        String.format(
            "########## Solution Improved Backtracking with Instance Variable: %s ############",
            sol));
    logger.debug(String.format("########## Time to solve = %s ############", (end - start)));
    logger.debug("##################################################");
  }

  @Test
  @Ignore
  public void testTreeBacktracking() {
    TreeCSPSolver tcs = new TreeCSPSolver();
    tcs.addCSPStateListener(
        new CSPStateListener() {

          @Override
          public void stateChanged(Assignment assignment, CSP csp) {
            logger.debug("Assignment evolved: " + assignment);
          }

          @Override
          public void stateChanged(CSP csp) {
            logger.debug("CSP evolved: " + csp);
          }
        });

    double start = System.currentTimeMillis();
    Assignment sol = tcs.solve(csp);
    double end = System.currentTimeMillis();
    logger.debug("##################################################");
    logger.debug(String.format("########## Solution Tree Backtracking: %s ############", sol));
    logger.debug(String.format("########## Time to solve = %s ############", (end - start)));
    logger.debug("##################################################");
  }

  @Test
  public void testImprovedBacktracking() {
    ImprovedBacktrackingStrategy ibs = new ImprovedBacktrackingStrategy();
    ibs.addCSPStateListener(
        new CSPStateListener() {

          @Override
          public void stateChanged(Assignment assignment, CSP csp) {
            logger.debug("Assignment evolved: " + assignment);
          }

          @Override
          public void stateChanged(CSP csp) {
            logger.debug("CSP evolved: " + csp);
          }
        });

    double start = System.currentTimeMillis();
    Assignment sol = ibs.solve(csp);
    double end = System.currentTimeMillis();
    logger.debug("##################################################");
    logger.debug(String.format("########## Solution Improved Backtracking: %s ############", sol));
    logger.debug(String.format("########## Time to solve = %s ############", (end - start)));
    logger.debug("##################################################");
  }

  @Test
  public void testBacktrackingSimple() {
    BacktrackingStrategy bts = new BacktrackingStrategy();
    bts.addCSPStateListener(
        new CSPStateListener() {

          @Override
          public void stateChanged(Assignment assignment, CSP csp) {
            logger.debug("Assignment evolved: " + assignment);
          }

          @Override
          public void stateChanged(CSP csp) {
            logger.debug("CSP evolved: " + csp);
          }
        });

    double start = System.currentTimeMillis();
    Assignment sol = bts.solve(csp);
    double end = System.currentTimeMillis();
    logger.debug("##################################################");
    logger.debug(String.format("########## Solution Backtracking: %s ############", sol));
    logger.debug(String.format("########## Time to solve = %s ############", (end - start)));
    logger.debug("##################################################");
  }
}
