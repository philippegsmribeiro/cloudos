package machinelearning;

import cloudos.machinelearning.nlp.GloVeService;

import java.io.File;
import java.util.Collection;

import org.deeplearning4j.models.glove.Glove;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;

import test.CloudOSTest;

/** Created by philipperibeiro on 3/2/17. */
@Ignore
public class GloVeServiceTest extends CloudOSTest {

  private GloVeService gloVeService;

  @Value("${cloudos.ml.data.folder}")
  private String dataFolder;

  // @Configuration
  // static class ContextConfiguration {
  // }

  @Before
  public void setUp() throws Exception {
    this.gloVeService = new GloVeService();
  }

  @Test
  public void testClosestWords() throws Exception {
    String rawSentence = this.dataFolder + File.separator + "raw_sentences.txt";
    logger.debug("Training Word2Vec with file data = " + rawSentence);
    Glove glove = this.gloVeService.build(rawSentence);

    Collection<String> list = glove.wordsNearest("day", 10);
    logger.debug("10 Words closest to 'day': " + list);

    double simD = glove.similarity("day", "night");
    logger.debug("Day/night similarity: " + simD);

    list = glove.wordsNearest("exception", 10);
    logger.debug("10 Words closest to 'exception': " + list);
  }

  @Test
  public void testLogFile() throws Exception {
    String apacheLogs = this.dataFolder + File.separator + "error_log";
    logger.debug("Training Word2Vec with file data = " + apacheLogs);
    Glove glove = this.gloVeService.build(apacheLogs);

    Collection<String> list = glove.wordsNearest("reset", 10);
    logger.debug("10 Words closest to 'reset': " + list);

    double simD = glove.similarity("reset", "stopped");
    logger.debug("reset/stopped similarity: " + simD);

    list = glove.wordsNearest("GET", 10);
    logger.debug("10 Words closest to 'GET': " + list);
  }
}
