package insights;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cloudos.Providers;
import cloudos.insights.InsightsMessage;
import cloudos.insights.InsightsRepository;
import cloudos.insights.InsightsService;
import cloudos.insights.ResizingRecommendationService;
import cloudos.instances.InstanceService;
import cloudos.models.Instance;
import cloudos.models.InstanceStatus;
import cloudos.models.Metricbeat;
import cloudos.notifications.Notification;
import cloudos.notifications.NotificationRepository;
import cloudos.notifications.NotificationsService;
import cloudos.provider.ProviderMachineType;
import cloudos.provider.ProviderMachineTypeRepositoryTest;
import cloudos.provider.ProvidersService;
import cloudos.queue.QueueListenerForTest;
import cloudos.queue.message.QueueMessageType;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.reflect.Whitebox;
import org.springframework.beans.factory.annotation.Autowired;
import test.CloudOSTest;

public class ResizingRecommendationServiceTest extends CloudOSTest {

  @Autowired
  private InsightsService insightsService;

  @Autowired
  private ResizingRecommendationService resizingRecommendationService;

  @Autowired
  private QueueListenerForTest queueListenerForTest;

  @Autowired
  private NotificationRepository notificationRepository;

  @Mock
  private InsightsRepository insightsRepository;

  @Autowired
  private InstanceService instanceService;

  @Autowired
  private ProvidersService providersService;

  private static String name = "emcahhgq";

  private static String machineType = "t2.medium";

  @Before
  public void setup() throws Exception {
    if (this.providersService.retrieveImages(Providers.AMAZON_AWS).size() == 0) {
      importJSON("provider_machine_type", "provider_machine_type.json");
    }
    List<Instance> instances = this.instanceService.findAllActiveInstances();
    if (instances == null || instances.size() == 0) {
      Instance instance = Instance.builder()
                            .name(name)
                            .provider(Providers.AMAZON_AWS)
                            .machineType(machineType)
                            .region("ap-northeast-2")
                            .status(InstanceStatus.RUNNING)
                            .cost(1.23)
                            .spot(false)
                            .build();
      this.instanceService.save(instance);
    }
  }

  @Test
  public void testIsFourhoursOld() throws Exception {
    insightsService.setReadOnlyMode(true);
    ResizingRecommendationService service = Mockito.mock(ResizingRecommendationService.class);
    // set the date to be now
    Date now = new Date();
    // it must be less than four hours
    assertFalse(Whitebox.invokeMethod(service, "isFourhoursOld", now));

    DateTime date = new DateTime(now);
    date = date.minusHours(5);
    now = date.toDate();
    // should be more than four hours old
    assertTrue(Whitebox.invokeMethod(service, "isFourhoursOld", now));
  }

  @Test
  public void testHashMessage() throws Exception {
    insightsService.setReadOnlyMode(true);
    ResizingRecommendationService service = Mockito.mock(ResizingRecommendationService.class);
    String message = "This is a message";
    String hash = Whitebox.invokeMethod(service, "hashMessage", message);
    assertNotNull(hash);
  }

  @Test
  @Ignore
  public void testSaveInsightMessage() throws Exception {
    insightsService.setReadOnlyMode(true);
    ResizingRecommendationService service = Mockito.mock(ResizingRecommendationService.class);

    String message = "This is a message";
    String hash = Whitebox.invokeMethod(service, "hashMessage", message);

    // save into the collection
    Whitebox.invokeMethod(service, "saveInsightMessage", message, hash);
    InsightsMessage insightsMessage = insightsRepository.findByHash(hash);
    // check it was created
    assertNotNull(insightsMessage);
    assertEquals(message, insightsMessage.getMessage());
    assertEquals(hash, insightsMessage.getHash());
  }

  @Test
  public void testProcessSystemMetricset() {
    insightsService.setReadOnlyMode(true);
    ResizingRecommendationService service = Mockito.mock(ResizingRecommendationService.class);
    String jsonHit = "{\"@timestamp\":\"2018-02-12T20:33:24.373Z\",\"beat\":{\"hostname\":\"ip-172-31-27-74\",\"name\":\"twymktek\",\"version\":\"5.5.0\"},\"metricset\":{\"module\":\"system\",\"name\":\"process\",\"rtt\":22009},\"system\":{\"process\":{\"cpu\":{\"start_time\":\"2018-01-23T05:48:47.000Z\",\"total\":{\"pct\":0.000000}},\"fd\":{\"limit\":{\"hard\":4096,\"soft\":1024},\"open\":0},\"memory\":{\"rss\":{\"bytes\":0,\"pct\":0.000000},\"share\":0,\"size\":0},\"name\":\"netns\",\"pgid\":0,\"pid\":13,\"ppid\":2,\"state\":\"sleeping\",\"username\":\"root\"}},\"type\":\"metricsets\"}";

    Metricbeat metricbeat = gson.fromJson(jsonHit, Metricbeat.class);
    logger.debug("Metricbeat: {}", metricbeat);
    doNothing().when(service).processSystemMetricset(isA(Metricbeat.class));

    // make one call
    service.processSystemMetricset(metricbeat);

    verify(service, times(1)).processSystemMetricset(metricbeat);

    service.analyzeProcess(metricbeat);
    verify(service, times(1)).analyzeProcess(metricbeat);
  }

  @Test
  public void testMemoryMetricset() {
    try {
      insightsService.setReadOnlyMode(true);
      queueListenerForTest.purge(QueueMessageType.MEMORY_INSIGHT);
      
      String jsonHit = "{\"@timestamp\":\"2018-02-21T11:39:14.393Z\",\"beat\":{\"hostname\":\"ip-172-31-38-212\",\"name\":\"emcahhgq\",\"version\":\"5.5.0\"},\"metricset\":{\"module\":\"system\",\"name\":\"memory\",\"rtt\":75},\"system\":{\"memory\":{\"actual\":{\"free\":376672256,\"used\":{\"bytes\":136544256,\"pct\":0.266100}},\"free\":89554944,\"swap\":{\"free\":0,\"total\":0,\"used\":{\"bytes\":0,\"pct\":0.000000}},\"total\":513216512,\"used\":{\"bytes\":423661568,\"pct\":0.825500}}},\"type\":\"metricsets\"}";
      Metricbeat metricbeat = gson.fromJson(jsonHit, Metricbeat.class);
      logger.debug("Metricbeat: {}", metricbeat);
      // make one call
      resizingRecommendationService.processSystemMetricset(metricbeat);
      
      
      // delete all notifications
      notificationRepository.deleteAll();
      
      // read the queue
      logger.debug("Reading queue!");
      Integer messages = null;
      for (int i = 0; i < 60; i++) {
        logger.debug("reading message queue: try {}", i);
        messages = queueListenerForTest.read(QueueMessageType.MEMORY_INSIGHT);
        if(messages != null && messages != 0) {
          logger.debug("Message(s) read: try {}, messages: {}", i, messages);
          break;
        }
        Thread.sleep(1000);
      }
      
      // find the created notification
      Thread.sleep(5000);
      logger.debug("Assert the notification was created!");
      List<Notification> findAll = notificationRepository.findAll();
      findAll.forEach(n -> logger.debug(n.getTitle() + " | " + n.getMessage()));
      assertTrue(findAll.size() == 1); 
      assertTrue(findAll.get(0).getTitle().equals(NotificationsService.MEMORY_INSIGHT_TITLE));
      
    } catch (Exception e) {
      logger.error("Error", e);
      fail(e.getLocalizedMessage());
    }
  }
}
