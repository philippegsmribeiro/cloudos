package insights;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cloudos.insights.InsightsService;
import cloudos.insights.InsightsSettings;
import cloudos.insights.InsightsSettingsRepository;
import test.CloudOSTest;

public class InsightsServiceTest extends CloudOSTest {

  @Autowired
  InsightsService insightsService;
  
  @Autowired
  InsightsSettingsRepository insightsSettingsRepository;
  
  @Test
  public void insightsStringTest() {
    InsightsSettings insightsSettings = insightsSettingsRepository.findAll().get(0);
    assertNotNull(insightsSettings.getResizingRecommendationPolicy());
    assertEquals(insightsSettings.getResizingRecommendationPolicy().getCpuUpperBound(), 0.7, 0.01);
    assertEquals(insightsSettings.getResizingRecommendationPolicy().getMemoryUpperBound(), 0.7,
        0.01);
    assertEquals(insightsSettings.getResizingRecommendationPolicy().getMemoryLowerBound(), 0.2,
        0.01);
    assertEquals(insightsSettings.getResizingRecommendationPolicy().getCpuLowerBound(), 0.2,
        0.01);
  }
  
  @Test
  public void insightsTest() {
    assertTrue(insightsService.isReadOnlyMode());
    assertNotNull(insightsService.getResizingRecommendationPolicy());
  }
}
