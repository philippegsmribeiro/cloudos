package insights;

import static org.hamcrest.CoreMatchers.either;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import cloudos.insights.InsightsController;
import cloudos.insights.ResizingRecommendationPolicy;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import test.CloudOSTest;

public class InsightsControllerTest extends CloudOSTest {

  @Autowired
  InsightsController insightsController;

  private MockMvc mvc;

  @Before
  public void setUp() throws Exception {
    this.mvc = MockMvcBuilders.standaloneSetup(insightsController).build();
    // clean the repository before start testing.
  }

  @Test
  public void testController() throws Exception {
    this.mvc.perform(get(InsightsController.INSIGHTS + "/")).andExpect(status().is4xxClientError());
  }
  
  @Test
  public void testIsReadOnly() throws Exception {
    this.mvc.perform(get(InsightsController.INSIGHTS + "/readOnly")).andExpect(status().is2xxSuccessful()).andExpect(content().string(either(is("true")).or(is("false"))));
  }
  
  @Test
  public void testSettingIsReadOnlyTrue() throws Exception {
    this.mvc.perform(post(InsightsController.INSIGHTS + "/readOnly/" + true)).andExpect(status().is2xxSuccessful()).andExpect(content().string(is("true")));
    this.mvc.perform(get(InsightsController.INSIGHTS + "/readOnly")).andExpect(status().is2xxSuccessful()).andExpect(content().string(is("true")));
  }
  
  @Test
  public void testSettingIsReadOnlyFalse() throws Exception {
    this.mvc.perform(post(InsightsController.INSIGHTS + "/readOnly/" + false)).andExpect(status().is2xxSuccessful()).andExpect(content().string(is("false")));
    this.mvc.perform(get(InsightsController.INSIGHTS + "/readOnly")).andExpect(status().is2xxSuccessful()).andExpect(content().string(is("false")));
  }
  
  @Test
  public void getResizingRecommendationPolicy() throws Exception {
    String policyJson = this.mvc.perform(get(InsightsController.INSIGHTS + "/policy/resizingRecommendation")).andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsString();
    assertNotNull(policyJson);
    ResizingRecommendationPolicy policy = mapper.readValue(policyJson, ResizingRecommendationPolicy.class);
    assertNotNull(policy);
    assertNotNull(policy.getCpuLowerBound());
    assertNotNull(policy.getCpuUpperBound());
    assertNotNull(policy.getFileSystemUpperBound());
    assertNotNull(policy.getMemoryLowerBound());
    assertNotNull(policy.getMemoryUpperBound());
    assertNotNull(policy.getProcessCpuUpperBound());
    assertNotNull(policy.getProcessMemoryUpperBound());
  }  
  
  @Test
  public void setResizingRecommendationPolicy() throws Exception {
     double cpuUpperBound = 0.912;
     double cpuLowerBound = 0.112;
    
     double memoryUpperBound = 0.911;
     double memoryLowerBound = 0.111;
    
     double processCpuUpperBound = 0.910;
     double processMemoryUpperBound = 0.110;

     double fileSystemUpperBound = 0.890;
    
    ResizingRecommendationPolicy policy = ResizingRecommendationPolicy.builder()
                                                                      .cpuLowerBound(cpuLowerBound)
                                                                      .cpuUpperBound(cpuUpperBound)
                                                                      .fileSystemUpperBound(fileSystemUpperBound)
                                                                      .memoryLowerBound(memoryLowerBound)
                                                                      .memoryUpperBound(memoryUpperBound)
                                                                      .processCpuUpperBound(processCpuUpperBound)
                                                                      .processMemoryUpperBound(processMemoryUpperBound)
                                                                      .build();
    
    
    String policyJson = this.mvc.perform(post(InsightsController.INSIGHTS + "/policy/resizingRecommendation").content(mapper.writeValueAsString(policy)).contentType(contentType)).andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsString();
    assertNotNull(policyJson);
    ResizingRecommendationPolicy policyUpdated = mapper.readValue(policyJson, ResizingRecommendationPolicy.class);
    assertNotNull(policyUpdated);
    assertEquals(policyUpdated.getCpuLowerBound(), cpuLowerBound, 0);
    assertEquals(policyUpdated.getCpuUpperBound(), cpuUpperBound, 0);
    assertEquals(policyUpdated.getFileSystemUpperBound(), fileSystemUpperBound, 0);
    assertEquals(policyUpdated.getMemoryLowerBound(), memoryLowerBound, 0);
    assertEquals(policyUpdated.getMemoryUpperBound(), memoryUpperBound, 0);
    assertEquals(policyUpdated.getProcessCpuUpperBound(), processCpuUpperBound, 0);
    assertEquals(policyUpdated.getProcessMemoryUpperBound(), processMemoryUpperBound, 0);
  }  
  
  @Test
  public void setResizingRecommendationPolicyInvalid() throws Exception {
     double test = 0.1;
    ResizingRecommendationPolicy policy = ResizingRecommendationPolicy.builder()
                                                                      .cpuLowerBound(test)
                                                                      .cpuUpperBound(test)
                                                                      .fileSystemUpperBound(test)
                                                                      .memoryLowerBound(test)
                                                                      .memoryUpperBound(test)
                                                                      .processCpuUpperBound(test)
                                                                      .processMemoryUpperBound(null)
                                                                      .build();
    
    
    String policyJson = this.mvc.perform(post(InsightsController.INSIGHTS + "/policy/resizingRecommendation").content(mapper.writeValueAsString(policy))).andExpect(status().is4xxClientError()).andReturn().getResponse().getContentAsString();
    logger.debug(policyJson);
  }  
  

  @Value("${insights.defaultLowerBound:0.2}")
  double lowerBound = 0.2;
  @Value("${insights.defaultUpperBound:0.7}")
  double upperBound = 0.7;

  @After
  public void cleanup() throws Exception {
    this.mvc.perform(post(InsightsController.INSIGHTS + "/readOnly/" + true));
    ResizingRecommendationPolicy policy = ResizingRecommendationPolicy.builder()
                                                                      .cpuLowerBound(lowerBound)
                                                                      .cpuUpperBound(upperBound)
                                                                      .fileSystemUpperBound(upperBound)
                                                                      .memoryLowerBound(lowerBound)
                                                                      .memoryUpperBound(upperBound)
                                                                      .processCpuUpperBound(upperBound)
                                                                      .processMemoryUpperBound(upperBound)
                                                                      .build();
   
    this.mvc.perform(post(InsightsController.INSIGHTS + "/policy/resizingRecommendation").content(mapper.writeValueAsString(policy)).contentType(contentType)).andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsString();
  }
}
