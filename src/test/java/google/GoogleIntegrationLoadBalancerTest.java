package google;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import cloudos.google.GoogleKeyGenerator;
import cloudos.google.integration.GoogleComputeIntegration;

import com.google.api.services.compute.model.Address;
import com.google.api.services.compute.model.Backend;
import com.google.api.services.compute.model.BackendService;
import com.google.api.services.compute.model.Firewall;
import com.google.api.services.compute.model.Firewall.Allowed;
import com.google.api.services.compute.model.ForwardingRule;
import com.google.api.services.compute.model.HttpHealthCheck;
import com.google.api.services.compute.model.HttpsHealthCheck;
import com.google.api.services.compute.model.InstanceGroup;
import com.google.api.services.compute.model.Network;
import com.google.api.services.compute.model.TargetPool;
import com.google.cloud.compute.ImageId;
import com.google.cloud.compute.Instance;
import com.google.cloud.compute.Operation;
import com.google.cloud.compute.Operation.OperationError;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

@Ignore
public class GoogleIntegrationLoadBalancerTest extends CloudOSTest {

  private static final int TIMEOUT = 1000 * 60 * 5;

  @Autowired
  private GoogleComputeIntegration googleIntegration;
  @Autowired
  private GoogleKeyGenerator googleKeyGenerator;
  private String imageCentOSI = "centos-7-v20161027";
  private String imageCentOSP = "centos-cloud";
  private String instanceName = generator.generate(4);
  private String machineTypeLow = "f1-micro";

  // machine1
  private String region = "us-west1";
  private String zone = "us-west1-a";

  private void cleanupAddress(String region, String name) {
    try {
      if (googleIntegration.getAddress(region, name) != null) {
        logger.debug("deleting address: " + name);
        googleIntegration.deleteAddress(region, name);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void cleanupBackendService(String name) {
    try {
      if (googleIntegration.getBackendService(name) != null) {
        logger.debug("deleting backendservice: " + name);
        googleIntegration.deleteBackendService(name);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void cleanupFirewall(String name) {
    try {
      if (googleIntegration.getFirewall(name) != null) {
        logger.debug("deleting firewall: " + name);
        googleIntegration.deleteFirewall(name);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void cleanupForwardingRule(String region, String name) {
    try {
      if (googleIntegration.getForwardingRule(region, name) != null) {
        logger.debug("deleting ForwardingRule: " + name);
        googleIntegration.deleteForwardingRule(region, name);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void cleanupHttpHealthCheck(String name) {
    try {
      if (googleIntegration.getHttpHealthcheck(name) != null) {
        logger.debug("deleting HttpHealthCheck: " + name);
        googleIntegration.deleteHttpHealthCheck(name);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  private void cleanupHttpsHealthCheck(String name) {
    try {
      if (googleIntegration.getHttpsHealthcheck(name) != null) {
        logger.debug("deleting HttpsHealthCheck: " + name);
        googleIntegration.deleteHttpsHealthCheck(name);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Aux method to delete a test instance.
   *
   * @param zone
   * @param instanceName
   * @param wait
   */
  private void cleanupInstance(String zone, String instanceName, boolean wait) {
    logger.debug("deleting instance " + instanceName);
    Operation operation = googleIntegration.deleteInstance(zone, instanceName);
    if (wait) {
      List<OperationError> error = null;
      try {
        error = googleIntegration.blockUntilComplete(operation, TIMEOUT);
      } catch (Exception e) {
        logger.error("Error deleting instance!", e);
      }
      assertThat(error, nullValue());
    }
  }

  private void cleanupInstanceGroup(String zone, String groupName) {
    try {
      if (googleIntegration.getInstanceGroup(zone, groupName) != null) {
        logger.debug("deleting InstanceGroup: " + groupName);
        googleIntegration.deleteInstanceGroup(zone, groupName);
      }
    } catch (Exception e2) {
      e2.printStackTrace();
    }
  }

  private void cleanupTargetPool(String region, String name) {
    try {
      if (googleIntegration.getTargetPool(region, name) != null) {
        logger.debug("deleting TargetPool: " + name);
        googleIntegration.deleteTargetPool(region, name);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void cleanupVpc(String vpcName) {
    try {
      if (googleIntegration.getVpc(vpcName) != null) {
        logger.debug("deleting Vpc: " + vpcName);
        googleIntegration.deleteVpc(vpcName);
      }
    } catch (Exception e2) {
      e2.printStackTrace();
    }
  }

  private Instance createInstance(String zone, String instanceName) throws Exception {
    logger.debug("creating instance " + instanceName);
    Operation operation = googleIntegration.createInstance(zone, instanceName, machineTypeLow,
        ImageId.of(imageCentOSP, imageCentOSI), googleKeyGenerator.getSshPubKey(),
        GoogleKeyGenerator.keyName, null, false, "sdf");
    List<OperationError> error = googleIntegration.blockUntilComplete(operation, TIMEOUT);
    assertThat(error, nullValue());
    return googleIntegration.getInstance(zone, instanceName);
  }

  private InstanceGroup createInstanceGroup(String groupName) throws Exception {
    InstanceGroup createInstanceGroup =
        googleIntegration.createInstanceGroup(zone, groupName, "test description with instances");
    assertNotNull(createInstanceGroup);
    assertEquals(createInstanceGroup.getName(), groupName);
    return createInstanceGroup;
  }

  /**
   * Simple test to delete all instance groups if needed. Ignored as default.
   *
   * @throws IOException
   */
  @Test
  @Ignore
  public void deleteAllGroups() throws IOException {
    // delete all
    Iterable<InstanceGroup> listResourceGroup =
        googleIntegration.listInstanceGroup(zone).getItems();
    if (listResourceGroup != null) {
      for (InstanceGroup instanceGroup : listResourceGroup) {
        cleanupInstanceGroup(zone, instanceGroup.getName());
      }
    }
  }

  /**
   * Basic test
   *
   * @throws IOException
   */
  @Test
  public void testAddress() throws IOException {
    String name = generator.generate(4);

    try {
      // CREATE
      Address address = new Address();
      address.setName(name);
      address.setRegion(region);
      Address created = googleIntegration.createAddress(address);
      assertNotNull(created);
      assertEquals(created.getName(), name);

      // LIST
      Iterable<Address> list = googleIntegration.listAddress(region).getItems();
      if (list != null) {
        for (Address item : list) {
          logger.debug("Address: {}", item);
        }
      }

      // DESCRIBE
      Address get = googleIntegration.getAddress(region, name);
      assertEquals(get.getId(), created.getId());

      // DELETE
      googleIntegration.deleteAddress(region, name);

    } catch (Exception e) {
      logger.error("error testing", e);
      fail(e.getLocalizedMessage());
    } finally {
      // clean up
      cleanupAddress(region, name);
    }
  }

  /**
   * Basic test
   *
   * @throws IOException
   */
  @Test
  public void testBackendService() throws IOException {
    String name = generator.generate(4);
    String healtCheckName = "hc" + name;

    try {
      // 1.2 health check
      HttpHealthCheck httpHealthCheck = new HttpHealthCheck();
      httpHealthCheck.setName(healtCheckName);
      httpHealthCheck = googleIntegration.createHttpHealthCheck(httpHealthCheck);
      logger.debug("HealthCheck created!");

      // CREATE
      BackendService backendService = new BackendService();
      backendService.setName(name);
      backendService.setRegion(region);
      // backendService.setProtocol("tcp");
      backendService.setHealthChecks(Arrays.asList(httpHealthCheck.getSelfLink()));

      BackendService created = googleIntegration.createBackendService(backendService);
      assertNotNull(created);
      assertEquals(created.getName(), name);

      // LIST
      Iterable<BackendService> list = googleIntegration.listBackendService().getItems();
      if (list != null) {
        for (BackendService item : list) {
          logger.debug("BackendService: {}", item);
        }
      }

      // DESCRIBE
      BackendService get = googleIntegration.getBackendService(name);
      assertEquals(get.getId(), created.getId());

      // DELETE
      googleIntegration.deleteBackendService(name);

    } catch (Exception e) {
      logger.error("error testing", e);
      fail(e.getLocalizedMessage());
    } finally {
      cleanupHttpHealthCheck(healtCheckName);
      cleanupBackendService(name);
    }
  }

  /**
   * Basic test
   *
   * @throws IOException
   */
  @Test
  public void testFirewall() throws IOException {
    String name = generator.generate(4);

    try {
      // CREATE
      Firewall firewallRules = new Firewall();
      firewallRules.setName(name);
      firewallRules.setTargetTags(Arrays.asList("http-tag"));
      Allowed allowed80 = new Allowed();
      allowed80.setIPProtocol("tcp");
      allowed80.setPorts(Arrays.asList("80"));
      firewallRules.setAllowed(Arrays.asList(allowed80));
      Firewall created = googleIntegration.createFirewall(firewallRules);
      assertNotNull(created);
      assertEquals(created.getName(), name);

      // LIST
      Iterable<Firewall> list = googleIntegration.listFirewall().getItems();
      if (list != null) {
        for (Firewall item : list) {
          logger.debug("Firewall: {}", item);
        }
      }

      // DESCRIBE
      Firewall get = googleIntegration.getFirewall(name);
      assertEquals(get.getId(), created.getId());

      // DELETE
      googleIntegration.deleteFirewall(name);

    } catch (Exception e) {
      logger.error("error testing", e);
      fail(e.getLocalizedMessage());
    } finally {
      // clean up
      cleanupFirewall(name);
    }
  }

  /**
   * Basic test
   *
   * @throws IOException
   */
  @Test
  public void testForwardingRule() throws IOException {
    String name = generator.generate(4);
    String targetPoolName = "tp"+name;
    try {
      TargetPool targetPool = new TargetPool();
      targetPool.setName(targetPoolName);
      targetPool.setRegion(region);
      targetPool = googleIntegration.createTargetPool(targetPool);

      // CREATE
      ForwardingRule tp = new ForwardingRule();
      tp.setName(name);
      tp.setRegion(region);
      tp.setTarget(targetPool.getSelfLink());
      ForwardingRule created = googleIntegration.createForwardingRule(tp);
      assertNotNull(created);
      assertEquals(created.getName(), name);

      // LIST
      Iterable<ForwardingRule> list = googleIntegration.listForwardingRule(region).getItems();
      if (list != null) {
        for (ForwardingRule item : list) {
          logger.debug("ForwardingRule: {}", item);
        }
      }

      // DESCRIBE
      ForwardingRule get = googleIntegration.getForwardingRule(region, name);
      assertEquals(get.getId(), created.getId());

      // DELETE
      googleIntegration.deleteForwardingRule(region, name);

    } catch (Exception e) {
      logger.error("error testing", e);
      fail(e.getLocalizedMessage());
    } finally {
      // clean up
      cleanupForwardingRule(region, name);
      cleanupTargetPool(region, targetPoolName);
    }
  }

  /**
   * Basic test
   *
   * @throws IOException
   */
  @Test
  public void testHttpHealthCheck() throws IOException {
    String name = generator.generate(4);

    try {
      // CREATE
      HttpHealthCheck httpHealthCheck = new HttpHealthCheck();
      httpHealthCheck.setName(name);
      HttpHealthCheck created = googleIntegration.createHttpHealthCheck(httpHealthCheck);
      assertNotNull(created);
      assertEquals(created.getName(), name);

      // LIST
      Iterable<HttpHealthCheck> list = googleIntegration.listHttpHealthCheck().getItems();
      if (list != null) {
        for (HttpHealthCheck item : list) {
          logger.debug("HttpHealthCheck: {}", item);
        }
      }

      // DESCRIBE
      HttpHealthCheck get = googleIntegration.getHttpHealthcheck(name);
      assertEquals(get.getId(), created.getId());

      // DELETE
      googleIntegration.deleteHttpHealthCheck(name);

    } catch (Exception e) {
      logger.error("error testing", e);
      fail(e.getLocalizedMessage());
    } finally {
      // clean up
      cleanupHttpHealthCheck(name);
    }
  }

  /**
   * Basic test
   *
   * @throws IOException
   */
  @Test
  public void testHttpsHealthCheck() throws IOException {
    String name = generator.generate(4);

    try {
      // CREATE
      HttpsHealthCheck httpHealthCheck = new HttpsHealthCheck();
      httpHealthCheck.setName(name);
      HttpsHealthCheck created = googleIntegration.createHttpsHealthCheck(httpHealthCheck);
      assertNotNull(created);
      assertEquals(created.getName(), name);

      // LIST
      Iterable<HttpsHealthCheck> list = googleIntegration.listHttpsHealthCheck().getItems();
      if (list != null) {
        for (HttpsHealthCheck item : list) {
          logger.debug("HttpsHealthCheck: {}", item);
        }
      }

      // DESCRIBE
      HttpsHealthCheck get = googleIntegration.getHttpsHealthcheck(name);
      assertEquals(get.getId(), created.getId());

      // DELETE
      googleIntegration.deleteHttpsHealthCheck(name);

    } catch (Exception e) {
      logger.error("error testing", e);
      fail(e.getLocalizedMessage());
    } finally {
      // clean up
      cleanupHttpsHealthCheck(name);
    }
  }

  /**
   * Basic test of instance groups.
   *
   * @throws IOException
   */
  @Test
  public void testInstanceGroups() throws IOException {
    String groupName = generator.generate(4);
    String description = "test description";

    try {
      // CREATE
      InstanceGroup instanceGroup = this.createInstanceGroup(groupName);

      try {
        // try to create again with the same name
        googleIntegration.createInstanceGroup(zone, groupName, description);
        fail("It was supposed to throw an exception!");
      } catch (IOException e) {
        logger.debug("Handled error: {}!", e.getLocalizedMessage());
      }

      // LIST
      Iterable<InstanceGroup> listResourceGroup =
          googleIntegration.listInstanceGroup(zone).getItems();
      if (listResourceGroup != null) {
        for (InstanceGroup group : listResourceGroup) {
          logger.debug("InstanceGroup: {}", group);
        }
      }

      // DESCRIBE
      InstanceGroup instanceGroupGet = googleIntegration.getInstanceGroup(zone, groupName);
      assertEquals(instanceGroupGet.getId(), instanceGroup.getId());

      // DELETE
      googleIntegration.deleteInstanceGroup(zone, groupName);

    } catch (Exception e) {
      logger.error("error testing", e);
      fail("test fail!");
    } finally {
      // clean up
      cleanupInstanceGroup(zone, groupName);
    }

  }

  /**
   * Test adding/removing instances to an instance group.
   */
  @Test
  public void testInstanceGroupsInstances() {

    String groupName = generator.generate(4);
    String iName = instanceName + "ig";
    try {
      // create group
      InstanceGroup createInstanceGroup =
          googleIntegration.createInstanceGroup(zone, groupName, "test description with instances");
      assertNotNull(createInstanceGroup);
      assertEquals(createInstanceGroup.getName(), groupName);

      // create instance1
      Instance instance = createInstance(zone, iName);

      // add instance to the group
      InstanceGroup instanceGroupWithInstances = googleIntegration.addInstanceToInstanceGroup(zone,
          groupName, Arrays.asList(instance.getInstanceId()));

      //
      assertEquals(createInstanceGroup.getId(), instanceGroupWithInstances.getId());
      assertEquals(new Integer(1), instanceGroupWithInstances.getSize());

      // remove instance from the group
      InstanceGroup removeInstanceToInstanceGroup = googleIntegration
          .removeInstanceToInstanceGroup(zone, groupName, Arrays.asList(instance.getInstanceId()));

      //
      assertEquals(createInstanceGroup.getId(), removeInstanceToInstanceGroup.getId());
      assertEquals(new Integer(0), removeInstanceToInstanceGroup.getSize());
    } catch (Exception e) {
      logger.error("error testing", e);
      fail("test fail!" + e.getLocalizedMessage());
    } finally {
      // clean up
      cleanupInstance(zone, iName, false);
      cleanupInstanceGroup(zone, groupName);
    }
  }

  @Test
  public void testLoadBalancerContentBase() {
    String tag = generator.generate(3);
    String iName1 = "www-" + tag;
    String iName2 = "www-video-" + tag;
    String firewallName = "www-firewall" + tag;
    String ipName = "lb-ip-1" + tag;
    String ipName2 = "lb-ipv6-1" + tag;
    String groupName1 = "www-group" + tag;
    String groupName2 = "www-video-group" + tag;
    String healtCheckName = "my-tcp-health-check" + tag;
    try {

      // 1. create instances
      Instance instance1 = this.createInstance(zone, iName1);
      Instance instance2 = this.createInstance(zone, iName2);

      // 2. create firewall rule
      // gcloud compute firewall-rules create www-firewall \
      // --target-tags http-tag --allow tcp:80
      Firewall firewall = new Firewall();
      firewall.setName(firewallName);
      firewall.setTargetTags(Arrays.asList("http-tag"));
      Allowed allowed80 = new Allowed();
      allowed80.setIPProtocol("tcp");
      allowed80.setPorts(Arrays.asList("80"));
      firewall.setAllowed(Arrays.asList(allowed80));
      googleIntegration.createFirewall(firewall);

      // 3. ips
      // gcloud compute addresses create lb-ip-1 \
      // --ip-version=IPV4 \
      // --global
      // gcloud compute addresses create lb-ipv6-1 \
      // --ip-version=IPV6 \
      // --global
      Address address = new Address();
      address.setName(ipName);
      address.setRegion(region);
      address.setDescription("IPV4");
      googleIntegration.createAddress(address);
      // googleIntegration.createIpAddress(region, ipName2, "IPV6");

      this.createInstanceGroup(groupName1);
      // add instance to the group
      googleIntegration.addInstanceToInstanceGroup(zone, groupName1,
          Arrays.asList(instance1.getInstanceId()));
      logger.debug("InstanceGroup 1 - instances inserted");


      this.createInstanceGroup(groupName2);
      // add instance to the group
      googleIntegration.addInstanceToInstanceGroup(zone, groupName2,
          Arrays.asList(instance2.getInstanceId()));
      logger.debug("InstanceGroup 2 - instances inserted");

      // Create a health check
      // gcloud compute health-checks create http http-basic-check
      HttpHealthCheck httpHealthCheck = new HttpHealthCheck();
      httpHealthCheck.setName(healtCheckName);
      httpHealthCheck.setPort(80);
      googleIntegration.createHttpHealthCheck(httpHealthCheck);
      logger.debug("HealthCheck created!");



    } catch (Exception e) {
      e.printStackTrace();
      fail("test fail!");
    } finally {
      // cleanup
      this.cleanupInstance(zone, iName1, false);
      this.cleanupInstance(zone, iName2, false);
      this.cleanupFirewall(firewallName);
      this.cleanupAddress(region, ipName);
      this.cleanupAddress(region, ipName2);
      this.cleanupInstanceGroup(zone, groupName1);
      this.cleanupInstanceGroup(zone, groupName2);
      this.cleanupHttpHealthCheck(healtCheckName);
    }
  }

  @Test
  public void testLoadBalancerHttp() {
    String tag = generator.generate(3);
    String healtCheckName = "test-healthcheck-" + tag;
    String groupName = "test-groupname-" + tag;
    String lbName = "test-loadbalancer-" + tag;
    String backendServiceLb = "test-backend-" + tag;


    try {
      // 1. backend

      // 1.1 instance group
      InstanceGroup createInstanceGroup = this.createInstanceGroup(groupName);
      logger.debug("Instance Group created!");
      Backend backendGroup = new Backend();
      backendGroup.setGroup(createInstanceGroup.getSelfLink());

      // 1.2 health check
      HttpHealthCheck httpHealthCheck = new HttpHealthCheck();
      httpHealthCheck.setName(healtCheckName);
      httpHealthCheck = googleIntegration.createHttpHealthCheck(httpHealthCheck);
      logger.debug("HealthCheck created!");

      // bundle up
      BackendService backendService = new BackendService();
      backendService.setName(backendServiceLb);
      backendService.setRegion(region);
      // backendService.setProtocol("tcp");
      backendService.setHealthChecks(Arrays.asList(httpHealthCheck.getSelfLink()));
      backendService.setBackends(Arrays.asList(backendGroup));
      // backendService.set
      googleIntegration.createBackendService(backendService);
      logger.debug("Backend Service created!");


      // 2. host and path rules


      // 3. front end config



    } catch (Exception e) {
      logger.error("error!", e);
      fail(e.getLocalizedMessage());
    } finally {
      this.cleanupInstanceGroup(zone, groupName);
      this.cleanupHttpHealthCheck(healtCheckName);
      this.cleanupBackendService(backendServiceLb);
    }
  }

  @Test
  public void testLoadBalancerInternal() throws Exception {

    String tag = generator.generate(3);
    String vpcName = "my-custom-network" + tag;
    String subnetName = "my-custom-subnet";
    String region = "us-west1";
    String ipRange = "10.128.0.0/20";

    String firewallName = "allow-all-10-128-0-0-20" + tag;
    String firewallName2 = "allow-tcp22-tcp3389-icmp" + tag;

    String iName1 = "instance1" + tag;
    String iName2 = "instance2" + tag;
    String iName3 = "instance3" + tag;
    String iName4 = "instance4" + tag;

    String groupName = "group1-" + tag;
    String groupName2 = "group2-" + tag;

    String healtCheckName = "my-tcp-health-check" + tag;
    String backendServiceLb = "my-int-lb" + tag;
    try {
      // 1. create a vpc
      Network network = new Network();
      network.setName(vpcName);
      network.setIPv4Range(ipRange);
      network.setDescription(region);
      Network execute = googleIntegration.insertVpc(network);
      logger.debug("VPC created!");

      // 2. create the firewall rules

      // 2.1 Create firewall rule that allows all traffic within the subnet
      Firewall firewall = null;
      firewall = new Firewall();
      firewall.setName(firewallName);
      firewall.setNetwork(execute.getSelfLink());
      firewall.setSourceRanges(Arrays.asList(ipRange));
      Allowed allowedSsh = new Allowed();
      allowedSsh.setIPProtocol("tcp");
      Allowed allowedRdp = new Allowed();
      allowedRdp.setIPProtocol("tcp");
      Allowed allowedIcmp = new Allowed();
      allowedIcmp.setIPProtocol("icmp");
      firewall.setAllowed(Arrays.asList(allowedSsh, allowedRdp, allowedIcmp));
      googleIntegration.createFirewall(firewall);
      logger.debug("Firewall 1 created!");

      // 2.2 Create firewall rule that allows SSH, RDP, and ICMP from anywhere
      Firewall firewall2 = null;
      firewall2 = new Firewall();
      firewall2.setName(firewallName2);
      firewall.setNetwork(vpcName);
      Allowed allowedSsh2 = new Allowed();
      allowedSsh2.setIPProtocol("tcp");
      allowedSsh2.setPorts(Arrays.asList("22"));
      Allowed allowedRdp2 = new Allowed();
      allowedRdp2.setIPProtocol("tcp");
      allowedRdp2.setPorts(Arrays.asList("3389"));
      Allowed allowedIcmp2 = new Allowed();
      allowedIcmp2.setIPProtocol("icmp");
      firewall2.setAllowed(Arrays.asList(allowedSsh2, allowedRdp2, allowedIcmp2));
      googleIntegration.createFirewall(firewall2);
      logger.debug("Firewall 2 created!");

      // 3. create instances and instance groups
      // create group
      createInstanceGroup(groupName);
      logger.debug("InstanceGroup 1 created!");

      // create instance1
      Instance instance1 = this.createInstance(zone, iName1);
      Instance instance2 = this.createInstance(zone, iName2);

      // add instance to the group
      googleIntegration.addInstanceToInstanceGroup(zone, groupName,
          Arrays.asList(instance1.getInstanceId(), instance2.getInstanceId()));
      logger.debug("InstanceGroup 1 - instances inserted");


      createInstanceGroup(groupName2);
      logger.debug("InstanceGroup 2 created!");

      // create instance
      Instance instance3 = this.createInstance(zone, iName3);
      Instance instance4 = this.createInstance(zone, iName4);

      // add instance to the group
      googleIntegration.addInstanceToInstanceGroup(zone, groupName2,
          Arrays.asList(instance3.getInstanceId(), instance4.getInstanceId()));
      logger.debug("InstanceGroup 2 - instances inserted");

      // 4. configure the load balancer

      // 4.1 Create a health check
      // gcloud compute health-checks create tcp my-tcp-health-check --port 80
      HttpHealthCheck httpHealthCheck = new HttpHealthCheck();
      httpHealthCheck.setName(healtCheckName);
      httpHealthCheck.setPort(80);
      googleIntegration.createHttpHealthCheck(httpHealthCheck);
      logger.debug("HealthCheck created!");

      // 4.2 Create a backend service
      // gcloud compute backend-services create my-int-lb \
      // --load-balancing-scheme internal \
      // --region us-central1 \
      // --health-checks my-tcp-health-check \
      // --protocol tcp
      BackendService backendService = new BackendService();
      backendService.setName(backendServiceLb);
      backendService.setHealthChecks(Arrays.asList(httpHealthCheck.getName()));
      backendService.setRegion(region);
      backendService.setProtocol("tcp");
      googleIntegration.createBackendService(backendService);
      logger.debug("Backend Service created!");

      // 4.3 Add instance groups to your backend service
      // gcloud compute backend-services add-backend my-int-lb \
      // --instance-group us-ig1 \
      // --instance-group-zone us-central1-b \
      // --region us-central1



      // gcloud compute backend-services add-backend my-int-lb \
      // --instance-group us-ig2 \
      // --instance-group-zone us-central1-c \
      // --region us-central1

      // 4.4 Create a forwarding rule
      // gcloud compute forwarding-rules create my-int-lb-forwarding-rule \
      // --load-balancing-scheme internal \
      // --ports 80 \
      // --network my-custom-network \
      // --subnet my-custom-subnet \
      // --region us-central1 \
      // --backend-service my-int-lb


    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      // finish
      cleanupFirewall(firewallName);
      cleanupFirewall(firewallName2);
      cleanupVpc(vpcName);
      cleanupInstance(zone, iName1, false);
      cleanupInstance(zone, iName2, false);
      cleanupInstance(zone, iName3, false);
      cleanupInstance(zone, iName4, false);
      cleanupInstanceGroup(zone, groupName);
      cleanupInstanceGroup(zone, groupName2);
    }
  }



  /**
   * https://cloud.google.com/compute/docs/load-balancing/network/example
   */
  @Test
  public void testLoadBalancerNetwork() {
    String tag = "-lbn-" + generator.generate(3);
    String iName1 = "www1" + tag;
    String iName2 = "www2" + tag;
    String iName3 = "www3" + tag;
    String firewallName = "www-firewall" + tag;
    String ipName = "www-address" + tag;
    String healtCheckName = "www-hc" + tag;
    String targetPoolName = "www-tp" + tag;
    String forwardingRuleName = "www-fr" + tag;

    try {
      Instance instance1 = this.createInstance(zone, iName1);
      Instance instance2 = this.createInstance(zone, iName2);
      Instance instance3 = this.createInstance(zone, iName3);


      // gcloud compute firewall-rules create www-firewall-network-lb \
      // --target-tags network-lb-tag --allow tcp:80
      Firewall firewall = new Firewall();
      firewall.setName(firewallName);
      firewall.setTargetTags(Arrays.asList("http-tag"));
      Allowed allowed80 = new Allowed();
      allowed80.setIPProtocol("tcp");
      allowed80.setPorts(Arrays.asList("80"));
      firewall.setAllowed(Arrays.asList(allowed80));
      firewall = googleIntegration.createFirewall(firewall);
      logger.debug("Firewall created!");
      assertNotNull(firewall);

      // Create a static external IP address for your load balancer.
      // gcloud compute addresses create network-lb-ip-1 \
      // --region us-central1
      Address address = new Address();
      address.setName(ipName);
      address.setRegion(region);
      address.setDescription("IPV4");
      address = googleIntegration.createAddress(address);
      logger.debug("Address created!");
      assertNotNull(address);

      // Add an HTTP health check object.
      // gcloud compute http-health-checks create basic-check
      HttpHealthCheck httpHealthCheck = new HttpHealthCheck();
      httpHealthCheck.setName(healtCheckName);
      httpHealthCheck = googleIntegration.createHttpHealthCheck(httpHealthCheck);
      logger.debug("HealthCheck created!");
      assertNotNull(httpHealthCheck);

      // gcloud compute target-pools create www-pool \
      // --region us-central1 --http-health-check basic-check
      // Add your instances to the target pool
      // gcloud compute target-pools add-instances www-pool \
      // --instances www1,www2,www3 \
      // --instances-zone us-central1-b
      TargetPool targetPool = new TargetPool();
      targetPool.setName(targetPoolName);
      targetPool.setRegion(region);
      targetPool.setHealthChecks(Arrays.asList(httpHealthCheck.getSelfLink()));
      targetPool.setInstances(Arrays.asList(instance1.getInstanceId().getSelfLink(),
          instance2.getInstanceId().getSelfLink(), instance3.getInstanceId().getSelfLink()));
      targetPool = googleIntegration.createTargetPool(targetPool);
      logger.debug("TargetPool created!");
      assertNotNull(targetPool);

      // gcloud compute forwarding-rules create www-rule \
      // --region us-central1 \
      // --ports 80 \
      // --address network-lb-ip-1 \
      // --target-pool www-pool
      ForwardingRule forwardingRule = new ForwardingRule();
      forwardingRule.setName(forwardingRuleName);
      forwardingRule.setRegion(region);
      forwardingRule.setPortRange("80");
      forwardingRule.setIPAddress(address.getSelfLink());
      forwardingRule.setTarget(targetPool.getSelfLink());
      forwardingRule = googleIntegration.createForwardingRule(forwardingRule);
      logger.debug("Forwarding Rule created!");
      assertNotNull(forwardingRule);

    } catch (Exception e) {
      logger.error("error testing", e);
      fail(e.getLocalizedMessage());
    } finally {
      // clean up
      cleanupInstance(zone, iName1, false);
      cleanupInstance(zone, iName2, false);
      cleanupInstance(zone, iName3, false);
      cleanupFirewall(firewallName);
      cleanupForwardingRule(region, forwardingRuleName);
      cleanupAddress(region, ipName);
      cleanupTargetPool(region, targetPoolName);
      cleanupHttpHealthCheck(healtCheckName);
    }

  }


  /**
   * Basic test
   *
   * @throws IOException
   */
  @Test
  public void testTargetPool() throws IOException {
    String name = generator.generate(4);

    try {
      // CREATE
      TargetPool tp = new TargetPool();
      tp.setName(name);
      TargetPool created = googleIntegration.createTargetPool(tp);
      assertNotNull(created);
      assertEquals(created.getName(), name);

      // LIST
      Iterable<TargetPool> list = googleIntegration.listTargetPool(region).getItems();
      if (list != null) {
        for (TargetPool item : list) {
          logger.debug("TargetPool: {}", item);
        }
      }

      // DESCRIBE
      TargetPool get = googleIntegration.getTargetPool(region, name);
      assertEquals(get.getId(), created.getId());

      // DELETE
      googleIntegration.deleteTargetPool(region, name);

    } catch (Exception e) {
      logger.error("error testing", e);
      fail(e.getLocalizedMessage());
    } finally {
      // clean up
      cleanupTargetPool(region, name);
    }
  }



}
