package google;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import cloudos.Providers;
import cloudos.billings.BillingFile;
import cloudos.billings.CloudosReportBucket;
import cloudos.billings.CloudosReportBucketRepository;
import cloudos.billings.GoogleBillingFileProcessor;
import cloudos.costanalysis.CostAnalysisTestUtils;
import cloudos.google.integration.GoogleStorageIntegration;
import cloudos.models.CallResult;
import cloudos.models.CallStatus;

import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Bucket;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;

import test.CloudOSTest;

/**
 * Created by luisrocha on 11/07/17.
 */
public class GoogleBillingFileProcessorTest extends CloudOSTest {

  private static final String BUCKET_NAME_PREFIX = "cloudosbilling";
  private static final String TEST_SUBFOLDER = GoogleBillingFileProcessorTest.class.getSimpleName();

  @Autowired
  private GoogleBillingFileProcessor fileProcessor;

  @Value("${cloudos.billings.directory}")
  private String directory;

  @Autowired
  private CloudosReportBucketRepository bucketRepository;

  @Autowired
  private GoogleStorageIntegration googleStorageIntegration;

  @Value(CostAnalysisTestUtils.GOOGLE_USAGE_REPORT_CSV)
  private Resource googleUsageReportResource;

  @Before
  public void setUp() throws Exception {

    // save the bucket it does not exist
    if (this.bucketRepository.findByName(BUCKET_NAME_PREFIX) == null) {
      CloudosReportBucket reportBucket = CloudosReportBucket.builder()
                                              .name(BUCKET_NAME_PREFIX)
                                              .provider(Providers.GOOGLE_COMPUTE_ENGINE)
                                              .build();
      this.bucketRepository.save(reportBucket);
      assertTrue(this.bucketRepository.exists(reportBucket));
    }
  }

  @Test
  public void init() throws Exception {
    this.fileProcessor.init();
  }

  @Test
  public void downloadReport() {
    boolean found = false;

    // temporary test directory to be deleted at the end
    Path testFileDir =
        Paths.get(directory, GoogleBillingFileProcessor.GOOGLE_BILLINGS_HOME, TEST_SUBFOLDER);
    try {

      this.fileProcessor.init();
      Iterable<Bucket> buckets = googleStorageIntegration.listBuckets();
      Files.createDirectories(testFileDir);

      if (buckets != null) {
        while (!found && buckets.iterator().hasNext()) {

          Bucket bucket = buckets.iterator().next();
          Iterable<Blob> blobs = googleStorageIntegration.listBlobs(bucket.getName());

          if (blobs != null) {
            while (!found && blobs.iterator().hasNext()) {
              Blob blob = blobs.iterator().next();
              if (MediaType.APPLICATION_JSON_VALUE.equals(blob.getContentType())) {
                found = true;
                String result = fileProcessor.dumpBlobContentsToFile(blob, testFileDir.toString());
                assertNotNull(result);
              }
            }
          }
        }
      } else {
        logger.warn("Could not find any bucket to test...");
      }
    } catch (Exception e) {
      fail(e.getMessage());
    } finally {
      FileUtils.deleteQuietly(new File(testFileDir.toString()));
    }
  }

  @Test
  public void processBillingFileUsingCsvFile() throws Exception {

    // copy original resource file to a temp location, because it is going to get deleted later
    File file = googleUsageReportResource.getFile();
    File destFile = Paths.get(directory, file.getName()).toFile();
    FileUtils.copyFile(file, destFile);

    BillingFile billingFile = new BillingFile();
    billingFile.setProcessed(Boolean.FALSE);
    billingFile.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
    billingFile.setBucket("junit");

    LocalDate nextDay = CostAnalysisTestUtils.GOOGLE_USAGE_REPORT_CSV_DATE.plusDays(1);
    Date createDate = Date.from(nextDay.atStartOfDay(ZoneId.systemDefault()).toInstant());

    billingFile.setCreated(createDate);

    /* google billing files have a number prepended to its name */
    billingFile.setName(destFile.getName() + "/1234567890");
    billingFile.setId(destFile.getName());
    billingFile.setPrefix(destFile.getParent());

    CallResult<Void> callResult = fileProcessor.processBillingFile(billingFile);
    assertSame(CallStatus.SUCCESS, callResult.getStatus());

  }
}
