package google;

import cloudos.google.integration.GoogleMetrics;
import cloudos.google.integration.GoogleMetricsIntegration;

import com.google.api.MetricDescriptor;
import com.google.monitoring.v3.Aggregation.Aligner;
import com.google.monitoring.v3.Aggregation.Reducer;
import com.google.monitoring.v3.Point;
import com.google.monitoring.v3.TimeSeries;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

public class GoogleMetricsIntegrationTest extends CloudOSTest {

  @Autowired
  private GoogleMetricsIntegration metricsIntegration;

  @Test
  public void listCPUMetrics() {
    logger.debug("CPU");
    // interval
    int timeWindowInSeconds = 10 * 60;
    int timeFrameInSeconds = 2 * 60;
    printResponse(metricsIntegration.getTimeSeriesForAll(
        GoogleMetrics.COMPUTE_GOOGLEAPIS_COM_INSTANCE_CPU_UTILIZATION, timeWindowInSeconds,
        timeFrameInSeconds, Aligner.ALIGN_MEAN, Reducer.REDUCE_MEAN));
    printResponse(metricsIntegration.getTimeSeriesForAll(
        GoogleMetrics.COMPUTE_GOOGLEAPIS_COM_INSTANCE_CPU_UTILIZATION, timeWindowInSeconds,
        timeFrameInSeconds, Aligner.ALIGN_MEAN, null));
    printResponse(metricsIntegration.getTimeSeriesForAll(
        GoogleMetrics.COMPUTE_GOOGLEAPIS_COM_INSTANCE_CPU_RESERVED_CORES, timeWindowInSeconds,
        timeFrameInSeconds, Aligner.ALIGN_MEAN, Reducer.REDUCE_MEAN));
    printResponse(metricsIntegration.getTimeSeriesForAll(
        GoogleMetrics.COMPUTE_GOOGLEAPIS_COM_INSTANCE_CPU_RESERVED_CORES, timeWindowInSeconds,
        timeFrameInSeconds, Aligner.ALIGN_MEAN, null));
    printResponse(metricsIntegration.getTimeSeriesForAll(
        GoogleMetrics.COMPUTE_GOOGLEAPIS_COM_INSTANCE_CPU_USAGE_TIME, timeWindowInSeconds,
        timeFrameInSeconds, Aligner.ALIGN_MEAN, Reducer.REDUCE_MEAN));
    printResponse(metricsIntegration.getTimeSeriesForAll(
        GoogleMetrics.COMPUTE_GOOGLEAPIS_COM_INSTANCE_CPU_USAGE_TIME, timeWindowInSeconds,
        timeFrameInSeconds, Aligner.ALIGN_MEAN, null));
  }

  @Test
  public void listDiskMetrics() {
    logger.debug("Disk");
    // interval
    int timeWindowInSeconds = 20 * 60;
    int timeFrameInSeconds = 2 * 60;
    printResponse(metricsIntegration.getTimeSeriesForAll(
        GoogleMetrics.COMPUTE_GOOGLEAPIS_COM_INSTANCE_DISK_READ_BYTES_COUNT, timeWindowInSeconds,
        timeFrameInSeconds, Aligner.ALIGN_MEAN, Reducer.REDUCE_MEAN));
    printResponse(metricsIntegration.getTimeSeriesForAll(
        GoogleMetrics.COMPUTE_GOOGLEAPIS_COM_INSTANCE_DISK_READ_BYTES_COUNT, timeWindowInSeconds,
        timeFrameInSeconds, Aligner.ALIGN_MEAN, null));
    printResponse(metricsIntegration.getTimeSeriesForAll(
        GoogleMetrics.COMPUTE_GOOGLEAPIS_COM_INSTANCE_DISK_WRITE_BYTES_COUNT, timeWindowInSeconds,
        timeFrameInSeconds, Aligner.ALIGN_MEAN, Reducer.REDUCE_MEAN));
    printResponse(metricsIntegration.getTimeSeriesForAll(
        GoogleMetrics.COMPUTE_GOOGLEAPIS_COM_INSTANCE_DISK_WRITE_BYTES_COUNT, timeWindowInSeconds,
        timeFrameInSeconds, Aligner.ALIGN_MEAN, null));
  }

  @Test
  public void listNetworksMetrics() {
    logger.debug("Disk");
    // interval
    int timeWindowInSeconds = 10 * 60;
    int timeFrameInSeconds = 1 * 60;
    printResponse(metricsIntegration.getTimeSeriesForAll(
        GoogleMetrics.COMPUTE_GOOGLEAPIS_COM_INSTANCE_NETWORK_RECEIVED_BYTES_COUNT,
        timeWindowInSeconds, timeFrameInSeconds, Aligner.ALIGN_MEAN, Reducer.REDUCE_MEAN));
    printResponse(metricsIntegration.getTimeSeriesForAll(
        GoogleMetrics.COMPUTE_GOOGLEAPIS_COM_INSTANCE_NETWORK_RECEIVED_BYTES_COUNT,
        timeWindowInSeconds, timeFrameInSeconds, Aligner.ALIGN_MEAN, null));
    printResponse(metricsIntegration.getTimeSeriesForAll(
        GoogleMetrics.COMPUTE_GOOGLEAPIS_COM_INSTANCE_NETWORK_SENT_BYTES_COUNT, timeWindowInSeconds,
        timeFrameInSeconds, Aligner.ALIGN_MEAN, Reducer.REDUCE_MEAN));
    printResponse(metricsIntegration.getTimeSeriesForAll(
        GoogleMetrics.COMPUTE_GOOGLEAPIS_COM_INSTANCE_NETWORK_SENT_BYTES_COUNT, timeWindowInSeconds,
        timeFrameInSeconds, Aligner.ALIGN_MEAN, null));
  }

  @SuppressWarnings("Duplicates")
  private void printResponse(Iterable<TimeSeries> iterable) {
    Map<String, List<Point>> map = new HashMap<>();
    for (TimeSeries timeSeries : iterable) {
      String instance = timeSeries.getResource().getLabelsMap().get("instance_id");
      if (!map.containsKey(instance)) {
        map.put(instance, new ArrayList<>());
      }
      List<Point> pointsList = timeSeries.getPointsList();
      map.get(instance).addAll(pointsList);
    }
    Iterator<Entry<String, List<Point>>> iterator = map.entrySet().iterator();
    while (iterator.hasNext()) {
      Map.Entry<String, List<Point>> entry = iterator.next();
      printResource(entry.getKey());
      List<Point> points = entry.getValue();
      Collections.sort(points, new Comparator<Point>() {
        @Override
        public int compare(Point o1, Point o2) {
          long i = o1.getInterval().getStartTime().getSeconds()
              - o2.getInterval().getStartTime().getSeconds();
          return i == 0 ? 0 : (i < 0 ? -1 : 1);
        }
      });
      for (Point point : points) {
        printPoint(point);
      }
    }
  }

  private void printPoint(Point point) {
    logger.debug("P: " + GoogleMetricsIntegration.formatInterval(point.getInterval()) + " "
        + point.getValue().getDescriptorForType().getName() + " "
        + point.getValue().getDoubleValue());
  }

  private void printResource(String monitoredResource) {
    logger.debug("Instance: " + monitoredResource);
  }

  @Test
  @Ignore
  public void all() {
    // interval
    int timeWindowInSeconds = 60 * 60;
    int timeFrameInSeconds = 1 * 60;
    Iterable<MetricDescriptor> listAllMetrics = metricsIntegration.listAllMetrics();
    for (MetricDescriptor metricDescriptor : listAllMetrics) {
      try {
        Iterable<TimeSeries> retrieveMetricTimeSeries =
            metricsIntegration.getTimeSeriesForAll(metricDescriptor.getType(), timeWindowInSeconds,
                timeFrameInSeconds, Aligner.ALIGN_MEAN, null);
        printResponse(retrieveMetricTimeSeries);
      } catch (Exception e) {
        logger.debug("error");
        // e.printStackTrace();
      }
    }
  }
}
