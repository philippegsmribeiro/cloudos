package google;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import cloudos.google.GoogleKeyGenerator;
import cloudos.google.integration.GoogleComputeIntegration;

import com.google.cloud.compute.Image;
import com.google.cloud.compute.ImageId;
import com.google.cloud.compute.Instance;
import com.google.cloud.compute.InstanceInfo;
import com.google.cloud.compute.MachineType;
import com.google.cloud.compute.Operation;
import com.google.cloud.compute.Operation.OperationError;
import com.google.cloud.compute.Region;

import java.util.List;
import org.apache.commons.collections4.IterableUtils;
import org.junit.After;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;
import test.SlowTest;

// @Ignore
@Category(SlowTest.class)
public class GoogleIntegrationTest extends CloudOSTest {

  private static final int TIMEOUT = 1000 * 60 * 5;

  // machine1
  private String zone = "us-west1-a";
  private String machineTypeLow = "f1-micro";
  private String imageCentOSP = "centos-cloud";
  private String imageCentOSI = "centos-7-v20161027";
  private String instanceName = generator.generate(4);

  // machine2
  private String zone2 = "europe-west1-b";
  private String machineTypeHigh = "n1-standard-8";
  private String imageOpenSuseP = "opensuse-cloud";
  private String imageOpenSuseI = "opensuse-13-2-v20161029";
  private String instanceName2 = generator.generate(4);

  @Autowired
  private GoogleComputeIntegration googleIntegration;
  @Autowired
  private GoogleKeyGenerator googleKeyGenerator;

  /**
   * Retrieve a list of public images CLOUD-74 We should be able to list all the images available in
   * Google Compute Engine
   */
  @Test
  public void listPublicImages() {
    Iterable<Image> list = googleIntegration.listPublicImages();
    for (Image item : list) {
      logger.debug(
          item.getImageId() + " " + item.getDescription() + " " + item.getImageId().getSelfLink());
    }
    assertThat(IterableUtils.size(list), not(0));
  }

  /**
   * Retrieve a list of regions and zones CLOUD-74 We should be able to list all the Regions in
   * Google Compute Engine.
   */
  @Test
  public void listRegion() {
    Iterable<Region> list = googleIntegration.listRegions();
    int count = 0;
    for (Region item : list) {
      count++;
      logger.debug(item.getDescription() + " " + item.getZones().toString());
    }
    assertThat(count, not(0));
  }

  @Test
  public void listMachineTypes() {
    Iterable<MachineType> list = googleIntegration.listMachineType();
    int count = 0;
    for (MachineType item : list) {
      count++;
      logger.debug(item.getDescription() + " " + item.getMachineTypeId().getSelfLink());
    }
    assertThat(count, not(0));
  }

  @Test
  public void listInstances() {
    Iterable<Instance> list = googleIntegration.listInstances();
    assertNotNull(list);
    for (Instance item : list) {
      logger.debug(item.getDescription() + " " + item.getInstanceId().getSelfLink());
    }
  }

  /**
   * Create a instance CLOUD-74 We should be allowed to create any number of instances in Google
   * Compute Engine, of any type, of any size, at any region, for all supported images (Windows, Red
   * Hat, Ubuntu, etc).
   */
  @Test
  public void createInstanceLow() {
    manageInstance(zone, instanceName, machineTypeLow, ImageId.of(imageCentOSP, imageCentOSI));
  }

  /**
   * Create a instance CLOUD-74 We should be allowed to create any number of instances in Google
   * Compute Engine, of any type, of any size, at any region, for all supported images (Windows, Red
   * Hat, Ubuntu, etc).
   */
  @Test
  @Ignore
  public void createInstanceHigh() {
    manageInstance(zone2, instanceName2, machineTypeHigh,
        ImageId.of(imageOpenSuseP, imageOpenSuseI));
  }

  private void manageInstance(String zone, String instanceName, String machineType, ImageId image) {
    try {
      // create instance1
      createInstance(zone, instanceName, machineType, image);
      // retrieve the instance
      Instance instance = googleIntegration.getInstance(zone, instanceName);
      assertNotNull(instance);
      logger.debug(instance.toString());
      if (InstanceInfo.Status.RUNNING.equals(instance.getStatus())) {
        // stop
        stopInstance(zone, instanceName);
        // update
        instance = googleIntegration.getInstance(zone, instanceName);
        logger.debug(instance.toString());
        assertThat(instance.getStatus(), not(InstanceInfo.Status.RUNNING));
      }
      //
      if (InstanceInfo.Status.STOPPING.equals(instance.getStatus())
          || InstanceInfo.Status.TERMINATED.equals(instance.getStatus())) {
        // start
        startInstance(zone, instanceName);
        // update
        instance = googleIntegration.getInstance(zone, instanceName);
        logger.debug(instance.toString());
        assertThat(instance.getStatus(), is(InstanceInfo.Status.RUNNING));
      }

      // delete instance1
      deleteInstance(zone, instanceName);
    } catch (Exception e) {
      fail("WHOOPS! Threw ExceptionNotToThrow" + e.toString());
    }
  }

  private void startInstance(String zone, String instanceName) throws Exception {
    logger.debug("start instance " + instanceName);
    Operation startInstance = googleIntegration.startInstance(zone, instanceName);
    List<OperationError> error = googleIntegration.blockUntilComplete(startInstance, TIMEOUT);
    assertThat(error, nullValue());
  }

  private void stopInstance(String zone, String instanceName) throws Exception {
    logger.debug("stop instance " + instanceName);
    Operation startInstance = googleIntegration.stopInstance(zone, instanceName);
    List<OperationError> error = googleIntegration.blockUntilComplete(startInstance, TIMEOUT);
    assertThat(error, nullValue());
  }

  private void createInstance(String zone, String instanceName, String machineType, ImageId image)
      throws Exception {
    logger.debug("creating instance " + instanceName);
    Operation createInstanceOperation =
        googleIntegration.createInstance(zone, instanceName, machineType, image,
            googleKeyGenerator.getSshPubKey(), GoogleKeyGenerator.keyName, null, false, "sdf");
    List<OperationError> error =
        googleIntegration.blockUntilComplete(createInstanceOperation, TIMEOUT);
    assertThat(error, nullValue());
  }

  private void deleteInstance(String zone, String instanceName) throws Exception {
    logger.debug("deleting instance " + instanceName);
    Operation deleteInstanceOperation = googleIntegration.deleteInstance(zone, instanceName);
    List<OperationError> error =
        googleIntegration.blockUntilComplete(deleteInstanceOperation, TIMEOUT);
    assertThat(error, nullValue());
  }

  /** Clean all test instances to avoid billing */
  @After
  public void cleanUp() {
    logger.debug("cleanup");
    Instance instance = googleIntegration.getInstance(zone, instanceName);
    if (instance != null) {
      googleIntegration.deleteInstance(zone, instanceName);
    }
    Instance instance2 = googleIntegration.getInstance(zone2, instanceName2);
    if (instance2 != null) {
      googleIntegration.deleteInstance(zone2, instanceName2);
    }
  }

}
