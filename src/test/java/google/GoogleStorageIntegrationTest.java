package google;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import cloudos.google.integration.GoogleStorageIntegration;
import cloudos.google.integration.GoogleStorageLocation;

import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.BucketInfo;
import com.google.cloud.storage.StorageClass;

import org.bson.types.ObjectId;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;
import test.SlowTest;

// @Ignore
@Category(SlowTest.class)
public class GoogleStorageIntegrationTest extends CloudOSTest {

  @Autowired private GoogleStorageIntegration googleStorageIntegration;

  /** Test listing the buckets */
  @Test
  public void testListBuckets() {
    Iterable<Bucket> listBuckets = googleStorageIntegration.listBuckets();
    listBuckets.forEach(bucket -> printLogBucket(bucket));
  }

  /**
   * Utility to print the bucket info
   *
   * @param bucket
   */
  private void printLogBucket(Bucket bucket) {
    if (bucket != null) {
      logger.debug(
          "Google Bucket id:{}\tname:{}\tcreateTime:{}\tlocation:{}\tstorageClass:{}\tversioningEnabled:{}",
          bucket.getGeneratedId(),
          bucket.getName(),
          bucket.getCreateTime(),
          bucket.getLocation(),
          bucket.getStorageClass(),
          bucket.versioningEnabled());
    } else {
      logger.debug("Bucket is NULL");
    }
  }

  /** CRUD test */
  @Test
  public void testCRUDBucket() {
    // create
    String bucketName = "tb" + new ObjectId().toHexString();
    logger.debug("Creating bucket {}", bucketName);
    Bucket createBucket =
        googleStorageIntegration.createBucket(
            bucketName, GoogleStorageLocation.MULTI_REGIONAL.EU, StorageClass.COLDLINE);
    assertNotNull(createBucket);
    printLogBucket(createBucket);

    // retrieve
    logger.debug("Retrieving bucket {}", bucketName);
    Bucket retrievedBucket = googleStorageIntegration.retrieveBucket(bucketName);
    assertNotNull(retrievedBucket);
    assertEquals(createBucket.getGeneratedId(), retrievedBucket.getGeneratedId());
    printLogBucket(retrievedBucket);

    // update
    logger.debug("Updating bucket {}", bucketName);
    Bucket updatedBucket =
        googleStorageIntegration.updateBucket(
            BucketInfo.newBuilder(bucketName).setVersioningEnabled(true).build());
    assertNotNull(updatedBucket);
    assertEquals(createBucket.getGeneratedId(), updatedBucket.getGeneratedId());
    printLogBucket(updatedBucket);

    // retrieve after updating
    logger.debug("Retrieving after updating bucket {}", bucketName);
    retrievedBucket = googleStorageIntegration.retrieveBucket(bucketName);
    assertNotNull(updatedBucket);
    assertEquals(createBucket.getGeneratedId(), updatedBucket.getGeneratedId());
    printLogBucket(updatedBucket);

    // delete
    logger.debug("Deleting bucket {}", bucketName);
    boolean deletedBucket = googleStorageIntegration.deleteBucket(createBucket.getName());
    assertTrue(deletedBucket);

    // retrieve after deleting
    logger.debug("Retrieving after deleting bucket {}", bucketName);
    retrievedBucket = googleStorageIntegration.retrieveBucket(bucketName);
    assertNull(retrievedBucket);
  }

  /** Test listing the blobs */
  @Test
  public void testListBlobs() {
    // get the bucket
    Bucket bucket = googleStorageIntegration.listBuckets().iterator().next();
    assertNotNull(bucket);
    printLogBucket(bucket);

    Iterable<Blob> listBlobs = googleStorageIntegration.listBlobs(bucket.getName());
    listBlobs.forEach(blob -> printLogBlob(blob));
  }

  /**
   * Utility to print the bucket info
   *
   * @param bucket
   */
  private void printLogBlob(Blob bucket) {
    if (bucket != null) {
      logger.debug(
          "Google Blob id:{}\tname:{}\tcreateTime:{}\tlCacheControl:{}\tContentType:{}\tCrc32c:{}",
          bucket.getGeneratedId(),
          bucket.getName(),
          bucket.getCreateTime(),
          bucket.getCacheControl(),
          bucket.getContentType(),
          bucket.getCrc32c());
    } else {
      logger.debug("Blob is NULL");
    }
  }

  /** CRUD test */
  @Test
  public void testCRUDBlob() {
    // get the bucket
    Bucket bucket = googleStorageIntegration.listBuckets().iterator().next();
    assertNotNull(bucket);
    printLogBucket(bucket);

    //
    String content = "test blob";

    // create
    String blobName = generator.generate(30);
    logger.debug("Creating blob {}", blobName);
    Blob createdBlob =
        googleStorageIntegration.createBlob(bucket.getName(), blobName, content.getBytes());
    assertNotNull(createdBlob);
    printLogBlob(createdBlob);

    // retrieve
    logger.debug("Retrieving blob {}", blobName);
    Blob retrievedBlob = googleStorageIntegration.retrieveBlob(bucket.getName(), blobName);
    assertNotNull(retrievedBlob);
    assertEquals(createdBlob.getGeneratedId(), retrievedBlob.getGeneratedId());
    printLogBlob(retrievedBlob);

    // read
    logger.debug("Reading blob {}", blobName);
    byte[] readBlob = googleStorageIntegration.readBlob(BlobId.of(bucket.getName(), blobName));
    logger.debug("Reading blob {}: {} ", blobName, new String(readBlob));
    assertEquals(content, new String(readBlob));

    // update
    logger.debug("Updating blob {}", blobName);
    Blob updatedblob =
        googleStorageIntegration.updateBlob(
            BlobInfo.newBuilder(BlobId.of(bucket.getName(), blobName))
                .setContentType("text/plain")
                .build());
    assertNotNull(updatedblob);
    assertEquals(createdBlob.getGeneratedId(), updatedblob.getGeneratedId());
    printLogBlob(updatedblob);

    // retrieve after updating
    logger.debug("Retrieving after updating blob {}", blobName);
    retrievedBlob = googleStorageIntegration.retrieveBlob(bucket.getName(), blobName);
    assertNotNull(updatedblob);
    assertEquals(createdBlob.getGeneratedId(), updatedblob.getGeneratedId());
    printLogBlob(updatedblob);

    // delete
    logger.debug("Deleting blob {}", blobName);
    boolean deletedblob =
        googleStorageIntegration.deleteBlob(createdBlob.getBucket(), createdBlob.getName());
    assertTrue(deletedblob);

    // retrieve after deleting
    logger.debug("Retrieving after deleting blob {}", blobName);
    retrievedBlob = googleStorageIntegration.retrieveBlob(bucket.getName(), blobName);
    assertNull(retrievedBlob);
  }
}
