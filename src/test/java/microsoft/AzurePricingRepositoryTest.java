package microsoft;

import cloudos.microsoft.AzureMeter;
import cloudos.microsoft.AzurePricing;
import cloudos.microsoft.AzurePricingRepository;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

@Ignore
public class AzurePricingRepositoryTest extends CloudOSTest {

  @Autowired private AzurePricingRepository azurePricingRepository;

  @Test
  public void testStorePricingDataMongoWithDotKeyProblem() throws IOException {
    String currency = "USDDSD";

    AzurePricing findByCurrency = azurePricingRepository.findByCurrency(currency);

    if (findByCurrency != null) {
      azurePricingRepository.delete(findByCurrency);

      findByCurrency = null;
    }

    List<AzureMeter> meters = new ArrayList<>();

    Map<String, String> meterRates = new LinkedHashMap<>();
    meterRates.put("18.09", "90.01");

    meters.add(
        new AzureMeter(
            "CLODOS-6059-4cbb-a132-54a187aaac46",
            "Compute Hours",
            "Virtual Machines",
            "Basic_D6 VM (Non-Windows)",
            "Hours",
            meterRates,
            "2015-02-01T00:00:00Z",
            BigDecimal.ZERO,
            "US"));

    AzurePricing azurePricing = new AzurePricing(currency, "locale", false, meters);

    azurePricingRepository.save(azurePricing);

    findByCurrency = azurePricingRepository.findByCurrency(currency);

    assert findByCurrency.getCurrency() != null;
  }
}
