package microsoft;

import cloudos.microsoft.AzureInstanceRepository;
import cloudos.microsoft.AzurePricingRepository;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

/** Created by philipperibeiro on 11/25/16. */
@Ignore
public class AzureInstanceTest extends CloudOSTest {

  @Autowired private AzurePricingRepository azurePricingRepository;
  @Autowired private AzureInstanceRepository azureInstanceRepository;

  @Test
  public void testRegexQuery() {
    // Get a valid Name
    logger.debug("--------------------- Regex Query ------------------");
    String name = "Standard_A2_v2";
    //AzureInstance instance = this.azureInstanceRepository.findByName(name);
    //log.info(String.format("Instance: %s", instance));
    //assertNotNull(instance);

    // now apply the regex to find the corresponding price.
    /*
    String regexString = "Standard_A2_v2 VM";
    List<AzurePricing> pricings = this.azurePricingRepository.findByMeterSubCategory(regexString);
    log.info(String.format("Pricings: %s", pricings));
    assertNotNull(pricings);
    assertNotEquals(pricings.size(), 0);
    */
    logger.debug("----------------------------------------------------");
  }

  @Test
  public void testFeatureGetFeatureVector() {
    String name = "A10";
    //AzureInstance instance = this.azureInstanceRepository.findByName(name);
    //List<Double> featureVector = instance.getFeatureVector();
    //log.info(featureVector);
    //assertNotNull(featureVector);
    //assertNotEquals(featureVector.size(), 0);
    //assertEquals(featureVector.size(), 7);
  }
}
