package microsoft;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import cloudos.microsoft.AzureManagement;

import com.microsoft.azure.management.compute.KnownLinuxVirtualMachineImage;
import com.microsoft.azure.management.compute.VirtualMachineSizeTypes;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;

import test.CloudOSTest;

@Ignore
public class AzureManagementTest extends CloudOSTest {

  @Value("${azure.client}")
  private String client;

  @Value("${azure.tenant}")
  private String tenant;

  @Value("${azure.key}")
  private String key;

  @Value("${azure.defaultPassword}")
  private String passwordVM;

  @Value("${azure.defaultUserName}")
  private String usernameVM;

  AzureManagement managementCloud = null;

  private String resourceGroupName;

  @Before
  public void setUp() throws Exception {

    this.managementCloud = new AzureManagement(client, tenant, key);
    this.resourceGroupName = "testcos131resource";

    assertNotNull(this.managementCloud);
  }

  @Ignore
  @Test
  public void testCreateSingleInstance() {

    assertNotNull(this.managementCloud);

    this.managementCloud.createInstance(
        KnownLinuxVirtualMachineImage.UBUNTU_SERVER_16_04_LTS.imageReference(),
        VirtualMachineSizeTypes.STANDARD_A0,
        1,
        1,
        this.resourceGroupName,
        usernameVM,
        passwordVM);

    // check if there's a single instance in the instanceId list.
    assertSame(this.managementCloud.getVirtualMachineCount(this.resourceGroupName), 1);
  }

  @After
  public void tearDown() throws Exception {
    assertNotNull(this.managementCloud);
    this.managementCloud.terminate(this.resourceGroupName);
    assertSame(this.managementCloud.getVirtualMachineCount(this.resourceGroupName), 0);
  }

  @Test(expected = AssertionError.class)
  public void testSearchImages() {
    assertNotNull(this.managementCloud);
    this.managementCloud.getAllMachineImages(null, null);
  }

  @Test
  public void testAvailableRegion() {
    assertNotNull(this.managementCloud);
    assert (this.managementCloud.getRegions().length != 0);
  }

  @Test
  public void testStopMachine() {
    assertNotNull(this.managementCloud);

    this.managementCloud.stop(this.resourceGroupName);
  }

  @Test
  public void testStartMachine() {
    assertNotNull(this.managementCloud);

    this.managementCloud.start(this.resourceGroupName);
  }

  @Test
  public void testRestartMachine() {
    assertNotNull(this.managementCloud);

    this.managementCloud.restart(this.resourceGroupName);
  }
}
