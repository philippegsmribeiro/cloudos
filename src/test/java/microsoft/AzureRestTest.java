package microsoft;

import static org.junit.Assert.assertNotNull;

import cloudos.microsoft.AzureRest;

import java.io.IOException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;

import test.CloudOSTest;

@Ignore
public class AzureRestTest extends CloudOSTest {

  @Value("${azure.client}")
  private String client;

  @Value("${azure.tenant}")
  private String tenant;

  @Value("${azure.key}")
  private String key;

  @Value("${azure.subscription}")
  private String subscription;

  AzureRest azureRest = null;

  @Before
  public void setUp() throws Exception {
    this.azureRest = new AzureRest(client, tenant, key);

    assertNotNull(this.azureRest);
  }

  @Test
  public void testGetDefaultSubscription() throws IOException {
    assert this.azureRest.getDefaultSubcription() != null;
  }

  @Test
  @Ignore
  public void testSearchRateCard() throws IOException {
    String offerDurableId = "MS-AZR-0003p";
    String currency = "USD";
    String locale = "en-US";
    String regionInfo = "US";

    assert this.azureRest.searchRateCard(offerDurableId, currency, locale, regionInfo) != null;
  }

  @Test
  public void testMetricDefinitionForResource() throws IOException {
    String resourceUri =
        "/subscriptions/1aa211a2-3b81-4a80-bc2f-7d387fab9dd0/resourceGroups/regrkafka/providers/Microsoft.Compute/virtualMachines/cosnodea";
    AzureRest.MetricDefinition[] metricDefinitions =
        this.azureRest.metricDefinitionForResource(resourceUri);

    for (AzureRest.MetricDefinition metricDefinition : metricDefinitions) {
      logger.debug(metricDefinition);
    }
  }

  @Test
  public void testMetricValueResource() throws IOException {
    String resourceUri =
        "/subscriptions/1aa211a2-3b81-4a80-bc2f-7d387fab9dd0/resourceGroups/regrkafka/providers/Microsoft.Compute/virtualMachines/cosnodea";
    String filter =
        "name.value eq 'Network In' and startTime eq 2017-03-12 and endTime eq 2017-03-13";

    AzureRest.MetricValueResource[] metricValueResources =
        this.azureRest.metricValueResource(resourceUri, filter);

    if (metricValueResources != null) {
      for (AzureRest.MetricValueResource metricValueResource : metricValueResources) {
        logger.debug(metricValueResource);
      }
    }
  }

  @Test
  public void testGetInstanceViewOfVirtualMachine() throws IOException {

    AzureRest.InstanceViewOfVirtualMachine instanceViewOfVirtualMachine =
        this.azureRest.getInstanceViewOfVirtualMachine("regrkafka", "cosnodea");

    logger.debug(instanceViewOfVirtualMachine);
  }
}
