package microsoft;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import cloudos.microsoft.AzureOffer;
import cloudos.microsoft.AzureOfferService;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.junit.Ignore;
import org.junit.Test;

import test.CloudOSTest;

@Ignore
public class AzureOfferServiceTest extends CloudOSTest {

  @Test
  public void testLoadOffersData() {
    List<AzureOffer> azureOffers = new AzureOfferService().listAllAzureOffers();

    assertNotEquals(azureOffers.size(), 0);
  }

  @Test
  public void testHasNoDuplicatedData() {

    boolean duplicated = false;

    List<AzureOffer> azureOffers = new AzureOfferService().listAllAzureOffers();

    Set<String> fullOffersNumber = new LinkedHashSet<>();

    for (AzureOffer azureOffer : azureOffers) {
      List<String> offersFullNumber = azureOffer.getOffersFullNumber();

      for (String s : offersFullNumber) {
        if (!fullOffersNumber.add(s)) {
          duplicated = true;
        }
      }
    }

    assertEquals(duplicated, false);
  }
}
