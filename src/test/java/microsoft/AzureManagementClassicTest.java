package microsoft;

import cloudos.microsoft.AzureManagementClassic;

import com.microsoft.windowsazure.exception.ServiceException;
import com.microsoft.windowsazure.management.models.RoleSizeListResponse;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import org.xml.sax.SAXException;

import test.CloudOSTest;

/** Created by gleimar on 24/11/2016. */
@Ignore
public class AzureManagementClassicTest extends CloudOSTest {

  @Value("${azure.classic.subscription}")
  private String subscriptionId;

  @Value("${azure.classic.passwordKey}")
  private String keyStorePassword;

  @Test
  public void testKey() {

    AzureManagementClassic azureManagementClassic =
        new AzureManagementClassic(this.subscriptionId, this.keyStorePassword);
    Path pathKey = azureManagementClassic.getPathKey();

    logger.debug(String.format("path key: %s", pathKey.toString()));

    Assert.assertNotNull(pathKey);
  }

  @Test
  public void testFetchData()
      throws ServiceException, ParserConfigurationException, URISyntaxException, SAXException,
          IOException {

    AzureManagementClassic azureManagementClassic =
        new AzureManagementClassic(this.subscriptionId, this.keyStorePassword);
    List<RoleSizeListResponse.RoleSize> rolesSize = azureManagementClassic.getRolesSize();

    for (RoleSizeListResponse.RoleSize roleSize : rolesSize) {
      logger.debug(
          String.format(
              "Cores: %s Label: %s Memory: %s",
              roleSize.getCores(), roleSize.getLabel(), roleSize.getMemoryInMb()));
    }

    Assert.assertNotNull(rolesSize);
  }
}
