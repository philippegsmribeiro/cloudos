package microsoft;

import cloudos.microsoft.AzureMonitor;
import cloudos.microsoft.AzureRest;

import java.io.IOException;
import java.time.LocalDate;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;

import test.CloudOSTest;

/** Created by gleimar on 19/03/2017. */
@Ignore
public class AzureMonitorTest extends CloudOSTest {

  @Value("${azure.client}")
  private String client;

  @Value("${azure.tenant}")
  private String tenant;

  @Value("${azure.key}")
  private String key;

  @Value("${azure.subscription}")
  private String subscription;

  @Test
  public void testGetDataPoint() throws IOException {

    AzureMonitor azureMonitor =
        new AzureMonitor(this.client, this.tenant, this.key, this.subscription);

    String resourceUri =
        "/subscriptions/1aa211a2-3b81-4a80-bc2f-7d387fab9dd0/resourceGroups/regrkafka/providers/Microsoft.Compute/virtualMachines/cosnodea";
    LocalDate now = LocalDate.now();

    LocalDate yesterday = now.minusDays(1);

    AzureRest.MetricValueResource[] datapoints =
        azureMonitor.getDatapoints(
            resourceUri,
            AzureMonitor.MetricName.PERCENTAGE_CPU,
            AzureMonitor.AggregationType.TOTAL,
            yesterday,
            now,
            "PT1M");

    logger.debug(String.format("size: %s", datapoints.length));

    if (datapoints.length > 0) {
      logger.debug(datapoints[0]);
    }
  }
}
