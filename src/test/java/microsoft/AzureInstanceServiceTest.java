package microsoft;

import cloudos.microsoft.AzureInstanceService;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

/** Created by gleimar on 24/11/2016. */
public class AzureInstanceServiceTest extends CloudOSTest {

  @Autowired private AzureInstanceService azureInstanceService;

  @Ignore
  @Test
  public void testSaveData() {
    azureInstanceService.fetchInstanceDataIntoDatabase();
  }
}
