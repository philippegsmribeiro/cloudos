package amazon;

import static org.junit.Assert.assertNotNull;

import cloudos.amazon.AWSCloudWatchBillingService;
import cloudos.amazon.AWSCredentialService;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.cloudwatch.model.Datapoint;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

/** Created by philipperibeiro on 5/5/17. */
public class AWSCloudWatchBillingServiceTest extends CloudOSTest {

  @Autowired AWSCredentialService awsCredentialService;

  @Test
  public void testAWSCloudWatchBillingService() throws Exception {
    AWSCloudWatchBillingService billingService1 =
        new AWSCloudWatchBillingService(Regions.US_EAST_1.getName(),
            awsCredentialService.getAWSCredentials());
    assertNotNull(billingService1);
  }

  @Test
  public void testGetCost() throws Exception {
    long interval = 1000 * 60 * 60 * 24 * 15;
    int window = 60 * 60 * 12;
    List<String> metrics = Arrays.asList("Average", "Maximum");

    AWSCloudWatchBillingService billingService =
        new AWSCloudWatchBillingService(
            Regions.US_EAST_1.getName(), awsCredentialService.getAWSCredentials());
    List<Datapoint> datapoints = billingService.getCost(interval, window, "USD", metrics);
    assertNotNull(datapoints);
  }
}
