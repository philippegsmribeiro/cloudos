package amazon;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import cloudos.amazon.AWSCredentialService;
import cloudos.amazon.AmazonEC2OnDemand;
import cloudos.amazon.Key;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.Instance;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

@Ignore
public class AmazonEC2OnDemandTest extends CloudOSTest {

  private static final String securityGroup = "default";
  private static final String keyPairName = generator.generate(10);
  private static Key key;
  private static AmazonEC2OnDemand onDemand;
  private static AmazonEC2 client;

  @Autowired
  AWSCredentialService awsCredentialService;

  @BeforeClass
  public void setUp() throws Exception {
    client =
        AmazonEC2ClientBuilder.standard()
            .withCredentials(awsCredentialService.getAWSCredentials())
            .withRegion(Regions.US_EAST_1)
            .build();
    key = new Key();
    assertNotNull(key);
    assertNotNull(client);
    String privateKey = key.getPrivateKey(keyPairName, client);
    assertNotNull(privateKey);
    onDemand = new AmazonEC2OnDemand(securityGroup, client);
  }

  @AfterClass
  public static void tearDown() throws Exception {
    assertNotNull(onDemand);
    onDemand.terminate();
    key.deleteKey(keyPairName, client);
  }

  @Test
  public void testAvailabilityZones() {
    List<String> zones = onDemand.availabilityZones();
    assertNotNull(zones);
    assertNotEquals(zones.size(), 0);
  }

  @Test
  public void testOnDemandInstanceString() {
    assertNotNull(onDemand);
    String value = onDemand.toString();
    logger.debug(value);
    assertNotNull(value);
  }

  @Test
  public void testCreateSingleInstance() throws Exception {
    assertNotNull(onDemand);

    List<Instance> instances =
        onDemand.createInstance(
            "ami-b63769a1", "t2.micro", 1, 1, keyPairName, securityGroup, null, null, null, null);
    // check if there's a single instance in the instanceId list.
    assertSame(instances.size(), 1);
    logger.debug("-----------------------------------------");
    logger.debug(instances);
    logger.debug("-----------------------------------------");
    List<String> instanceIds =
        instances.stream().map(Instance::getInstanceId).collect(Collectors.toList());
    onDemand.terminate(instanceIds);
  }

  @Test
  public void testMultipleInstances() throws Exception {
    assertNotNull(onDemand);
    List<Instance> instances =
        onDemand.createInstance(
            "ami-b63769a1", "t2.micro", 1, 3, keyPairName, securityGroup, null, null, null, null);
    // check if there's a single instance in the instanceId list.
    assertSame(instances.size(), 3);
    logger.debug("-----------------------------------------");
    logger.debug(instances);
    logger.debug("-----------------------------------------");
    List<String> instanceIds =
        instances.stream().map(Instance::getInstanceId).collect(Collectors.toList());
    onDemand.terminate(instanceIds);
  }
}
