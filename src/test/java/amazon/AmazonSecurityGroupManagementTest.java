package amazon;

/**
 * Test case for the AmazonSecurityGroupManagement class @Copyright CyberTextron Inc. 2017
 *
 * @license: private
 * @author Philippe Ribeiro
 * @date: 5/5/2017
 */
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import cloudos.Providers;
import cloudos.models.AwsSecurityGroup;
import cloudos.models.CloudosIpPermission;
import cloudos.models.CloudosSecurityGroup;
import cloudos.models.CloudosSecurityGroupRepository;
import cloudos.securitygroup.AmazonSecurityGroupManagement;
import cloudos.securitygroup.CloudSecurityGroupException;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.model.IpPermission;
import com.amazonaws.services.ec2.model.IpRange;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

public class AmazonSecurityGroupManagementTest extends CloudOSTest {

  private static String SOURCE_ANYWHERE_PATTERN_1 = "0.0.0.0/0";
  private static Integer SSH_PROTOCOL_PORT = 22;
  private static final String region = Regions.US_WEST_1.getName();

  @Autowired
  AmazonSecurityGroupManagement amazonSecurityGroupManagement;
  @Autowired
  private CloudosSecurityGroupRepository cloudosSecurityGroupRepository;

  private List<String> securityGroups = new ArrayList<>();

  @Test
  public void testCreateSecurityGroup() throws CloudSecurityGroupException {
    String groupId = null;
    try {
      String groupName = generator.generate(10);
      groupId = amazonSecurityGroupManagement.createSecurityGroup(region, groupName,
          "This is a security group test");
    } catch (Exception ex) {
      fail("WHOOPS! Threw ExceptionNotToThrow" + ex.toString());
    }
    assertNotNull(groupId);
    amazonSecurityGroupManagement.deleteSecurityGroup(region, groupId);
  }

  @Test
  public void testDeleteSecurityGroup() {
    try {
      securityGroups.forEach(t -> {
        try {
          amazonSecurityGroupManagement.deleteSecurityGroup(region, t);
        } catch (CloudSecurityGroupException e) {
          throw new RuntimeException(e);
        }
      });
    } catch (Exception ex) {
      fail("WHOOPS! Threw ExceptionNotToThrow" + ex.toString());
    }
  }

  @Test
  public void testListAllSecurityGroups() throws CloudSecurityGroupException {
    List<com.amazonaws.services.ec2.model.SecurityGroup> securityGroups =
        amazonSecurityGroupManagement.findAllSecurityGroups(region);
    assertNotNull(securityGroups);

    // there should always be at least one security group
    assertThat(securityGroups.size(), greaterThan(0));
    securityGroups.forEach(securityGroup -> assertNotNull(securityGroup.getVpcId()));
  }

  @Test
  public void testDescribeSecurityGroup() {
    int length = 10;
    String groupName = generator.generate(length);
    try {
      String groupId = amazonSecurityGroupManagement.createSecurityGroup(region, groupName,
          "This is a security group test");
      assertNotNull(groupId);
      com.amazonaws.services.ec2.model.SecurityGroup sg =
          amazonSecurityGroupManagement.describe(region, groupName);
      assertNotNull(sg);
      assertEquals(sg.getGroupName(), groupName);
      assertNotNull(sg.getVpcId());

      // delete the security group
      amazonSecurityGroupManagement.deleteSecurityGroup(region, groupId);
    } catch (Exception ex) {
      fail("WHOOPS! Threw ExceptionNotToThrow" + ex.toString());
    }
  }

  @Test
  public void testConfigureSecurityGroup() {
    int length = 10;
    String groupName = generator.generate(length);
    try {
      String groupId = amazonSecurityGroupManagement.createSecurityGroup(region, groupName,
          "This is a security group test");
      assertNotNull(groupId);
      com.amazonaws.services.ec2.model.SecurityGroup sg =
          amazonSecurityGroupManagement.describe(region, groupName);
      assertNotNull(sg);
      assertEquals(sg.getGroupName(), groupName);

      // configure the security group
      IpRange ip_range = new IpRange().withCidrIp("0.0.0.0/0");
      amazonSecurityGroupManagement.configureGroupIngress(region, groupName, "tcp", 22, 22,
          ip_range);

      IpPermission ip_perm = new IpPermission().withIpProtocol("tcp").withToPort(80)
          .withFromPort(80).withIpv4Ranges(ip_range);

      List<IpPermission> ipPermissions = Arrays.asList(ip_perm);
      amazonSecurityGroupManagement.configureGroupIngress(region, groupName, ipPermissions);

      // configure the security group
      amazonSecurityGroupManagement.configureGroupEgress(region, groupName, "tcp", 22, 22,
          ip_range);
      amazonSecurityGroupManagement.configureGroupIngress(region, groupName, ipPermissions);

      // delete the security group
      amazonSecurityGroupManagement.deleteSecurityGroup(region, groupId);

    } catch (Exception ex) {
      fail("WHOOPS! Threw ExceptionNotToThrow" + ex.toString());
    }
  }

  @Test
  public void testSSHInboundDefaultRules() {

    try {

      String groupName = generator.generate(10);

      AwsSecurityGroup awsSecurityGroup = new AwsSecurityGroup();
      awsSecurityGroup.setDescription("This is a security group test - testSSHInboundDefaultRules");
      awsSecurityGroup.setName(groupName);
      awsSecurityGroup.setRegion(region);
      awsSecurityGroup.setProvider(Providers.AMAZON_AWS);
      awsSecurityGroup = amazonSecurityGroupManagement.create(awsSecurityGroup);

      assertNotNull(awsSecurityGroup);
      assertNotNull(awsSecurityGroup.getId());
      assertNotNull(awsSecurityGroup.getName());
      assertNotNull(awsSecurityGroup.getDescription());
      assertNotNull(awsSecurityGroup.getProvider());
      assertNotNull(awsSecurityGroup.getGroupId());
      assertEquals(awsSecurityGroup.getName(), groupName);
      assertEquals(awsSecurityGroup.getDescription(),
          "This is a security group test - testSSHInboundDefaultRules");
      assertEquals(awsSecurityGroup.getProvider(), Providers.AMAZON_AWS);

      amazonSecurityGroupManagement.synchronizeSecurityGroups(region);

      List<CloudosSecurityGroup> cloudosSecurityGroups = cloudosSecurityGroupRepository
          .findByNameAndProviderAndRegion(groupName, Providers.AMAZON_AWS, region);

      assertNotNull(cloudosSecurityGroups);
      assertEquals(cloudosSecurityGroups.size(), 1);

      CloudosSecurityGroup cloudosSecurityGroup = cloudosSecurityGroups.get(0);
      assertEquals(cloudosSecurityGroup.getName(), groupName);
      assertEquals(cloudosSecurityGroup.getDescription(),
          "This is a security group test - testSSHInboundDefaultRules");
      assertEquals(cloudosSecurityGroup.getInbounds().size(), 1);

      for (CloudosIpPermission cloudosIpPermission : cloudosSecurityGroup.getInbounds()) {
        assertEquals(cloudosIpPermission.getProtocol().getName(), "tcp");
        assertEquals(cloudosIpPermission.getPortFrom(), SSH_PROTOCOL_PORT);
        assertEquals(cloudosIpPermission.getPortTo(), SSH_PROTOCOL_PORT);

        if (!cloudosIpPermission.getSource().equals(SOURCE_ANYWHERE_PATTERN_1)) {
          fail("Missing SOURCE_ANYWHERE_PATTERN_1 0.0.0.0/0");
        }

      }

      cloudosSecurityGroupRepository.delete(awsSecurityGroup);
      amazonSecurityGroupManagement.delete(awsSecurityGroup);

    } catch (Exception ex) {
      fail("WHOOPS! Threw ExceptionNotToThrow" + ex.toString());
    }

  }

  @Test
  public void testSynchronizeSecurityGroups() {

    try {
      String groupName = generator.generate(10);
      String groupId = amazonSecurityGroupManagement.createSecurityGroup(region, groupName,
          "This is a security group test - testSynchronizeSecurityGroups");

      assertNotNull(groupId);

      amazonSecurityGroupManagement.synchronizeSecurityGroups(region);

      List<CloudosSecurityGroup> cloudosSecurityGroups = cloudosSecurityGroupRepository
          .findByNameAndProviderAndRegion(groupName, Providers.AMAZON_AWS, region);

      assertNotNull(cloudosSecurityGroups);
      assertEquals(cloudosSecurityGroups.size(), 1);

      CloudosSecurityGroup cloudosSecurityGroup = cloudosSecurityGroups.get(0);
      assertEquals(cloudosSecurityGroup.getName(), groupName);
      assertEquals(cloudosSecurityGroup.getDescription(),
          "This is a security group test - testSynchronizeSecurityGroups");

      cloudosSecurityGroupRepository.delete(cloudosSecurityGroup);
      amazonSecurityGroupManagement.deleteSecurityGroup(region, groupId);

    } catch (Exception ex) {
      fail("WHOOPS! Threw ExceptionNotToThrow" + ex.toString());
    }

  }

}
