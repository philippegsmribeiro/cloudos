package amazon;

import cloudos.amazon.AmazonPricing;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

public class AmazonPricingTest extends CloudOSTest {

  @Autowired
  private AmazonPricing awsPricing;

  /**
   * Ignored as the AmazonPricingTest will do the same test.
   * @throws Exception
   */
  @Test
  @Ignore
  public void testGetAWSPricing() throws Exception {
    awsPricing.getAwsPricing();
  }
}
