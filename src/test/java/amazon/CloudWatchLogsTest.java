package amazon;

import static org.junit.Assert.assertNotNull;

import cloudos.amazon.AWSCredentialService;
import cloudos.amazon.AmazonEC2OnDemand;
import cloudos.amazon.CloudWatchLogs;
import cloudos.amazon.Key;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

/** Created by philipperibeiro on 2/18/17. */
@Ignore
public class CloudWatchLogsTest extends CloudOSTest {

  @Autowired AWSCredentialService awsCredentialService;
  private final String securityGroup = "default";
  private final String keyPairName = generator.generate(10);
  private Key key;
  private AmazonEC2OnDemand onDemand;
  private AmazonEC2 client;
  private static boolean isInitialized = false;
  private List<String> instanceIds;

  @Before
  @SuppressWarnings("Duplicates")
  public void setUp() throws Exception {
    if (!isInitialized) {
      this.client =
          AmazonEC2ClientBuilder.standard()
              .withCredentials(awsCredentialService.getAWSCredentials())
              .withRegion(Regions.US_EAST_1)
              .build();
      this.key = new Key();
      assertNotNull(this.key);
      assertNotNull(this.client);
      String privateKey = this.key.getPrivateKey(this.keyPairName, this.client);
      assertNotNull(privateKey);
      this.onDemand = new AmazonEC2OnDemand(this.securityGroup, client);
      isInitialized = true;
      this.instanceIds = new ArrayList<>();
    }
  }

  @Test
  @Ignore
  public void testCloudWatchLogs() throws Exception {
    assertNotNull(this.onDemand);
    CloudWatchLogs cloudWatchLogs =
        new CloudWatchLogs(Regions.US_EAST_1.getName(), awsCredentialService.getAWSCredentials());
    cloudWatchLogs.createLogGroup("testGroup");
    cloudWatchLogs.createLogStream("testGroup", "testStream");
    cloudWatchLogs.putLogEvent("testGroup", "testStream");
  }

  @After
  public void tearDown() throws Exception {
    this.onDemand.terminate(this.instanceIds);
    assertNotNull(this.onDemand);
    this.key.deleteKey(this.keyPairName, this.client);
  }
}
