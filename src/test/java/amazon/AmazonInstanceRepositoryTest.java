package amazon;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import cloudos.amazon.AmazonInstance;
import cloudos.amazon.AmazonInstanceRepository;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import test.CloudOSTest;

@Ignore
public class AmazonInstanceRepositoryTest extends CloudOSTest {

  @Autowired private AmazonInstanceRepository amazonInstanceRepository;

  @Autowired private MongoTemplate mongoTemplate;

  @Test
  public void testCountFunction() {
    logger.debug(
        String.format("actives %s=%s", "true", amazonInstanceRepository.countByActive(true)));
    logger.debug(
        String.format("actives %s=%s", "false", amazonInstanceRepository.countByActive(false)));
  }

  @Test
  public void testFindByAttributeInstanceType() {
    List<AmazonInstance> instances =
        this.amazonInstanceRepository.findByAttributesInstanceType("m3.xlarge");
    assertNotNull(instances);
    assertNotEquals(instances.size(), 0);
  }

  @Test
  public void testFindOne() {
    AmazonInstance instance =
        this.amazonInstanceRepository.findOneByAttributesInstanceType("m3.xlarge");
    logger.debug(instance);
    assertNotNull(instance);
  }

  @Test
  @SuppressWarnings({"cast", "unchecked"})
  public void testGetDistinctAttributeInstanceType() {
    List<String> instanceTypes =
        mongoTemplate.getCollection("aws_instances").distinct("attributes.instanceType");
    assertNotNull(instanceTypes);
    assertNotEquals(instanceTypes.size(), 0);
    logger.debug(instanceTypes);
  }
}
