package amazon;

import static org.junit.Assert.assertNotNull;

import cloudos.loadbalancer.AmazonLoadBalancerManagement;
import cloudos.loadbalancer.LoadBalancerException;
import cloudos.models.AmazonLoadBalancer;
import cloudos.models.AmazonLoadBalancerListener;
import cloudos.models.AmazonLoadBalancerType;
import cloudos.models.AmazonTargetGroup;
import cloudos.securitygroup.AmazonSecurityGroupManagement;
import cloudos.targetgroups.AmazonTargetGroupManagement;
import cloudos.targetgroups.TargetGroupException;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.model.SecurityGroup;
import com.amazonaws.services.elasticloadbalancingv2.model.Action;
import com.amazonaws.services.elasticloadbalancingv2.model.ActionTypeEnum;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Ignore
public class AmazonLoadBalancerTest extends AmazonLoadBalancerSupportTest {

  private final String TARGET_GROUP_NAME = String.format("cloudos%s", generator.generate(8));

  private final String APPLICATION_LOAD_BALANCER_NAME =
      String.format("cloudosAPPLICATION%s", generator.generate(8));

  private final String NETWORK_LOAD_BALANCER_NAME =
      String.format("cloudosNETWORK%s", generator.generate(8));

  private static String networkLoadBalancerARN = null;
  private static String applicationLoadBalancerARN = null;
  private static String targetGroupARN = null;
  private static AmazonTargetGroup amazonTargetGroupRequest = new AmazonTargetGroup();
  private static AmazonLoadBalancer loadBalancerRequest = new AmazonLoadBalancer();
  private static AmazonLoadBalancerListener loadBalancerListenerRequest =
      new AmazonLoadBalancerListener();

  @Autowired
  private AmazonTargetGroupManagement amazonTargetGroupManagement;

  @Autowired
  private AmazonLoadBalancerManagement amazonLoadBalancerManagement;

  @Autowired
  private AmazonSecurityGroupManagement amazonSecurityGroupManagement;

  @Test
  public void testA0CreateTargetGroup() throws Exception {
    createVPCSubnets();
    setVPCSubnets();

    amazonTargetGroupRequest.setName(TARGET_GROUP_NAME);
    amazonTargetGroupRequest.setProtocol("HTTP");
    amazonTargetGroupRequest.setPort(80);
    amazonTargetGroupRequest.setTargetType("instance");
    amazonTargetGroupRequest.setVpcId(vpcId);
    amazonTargetGroupRequest.setRegion(Regions.US_EAST_1.getName());

    amazonTargetGroupRequest = this.amazonTargetGroupManagement.create(amazonTargetGroupRequest);
    assertNotNull(amazonTargetGroupRequest);

    targetGroupARN = amazonTargetGroupRequest.getTargetGroupArn();
    assertNotNull(targetGroupARN);

  }

  @Test
  public void testA1CreateApplicationLoadBalancer() throws Exception {

    AmazonLoadBalancer request = AmazonLoadBalancer.builder().name(APPLICATION_LOAD_BALANCER_NAME)
        .subnets(subnets).ipAddressType("ipv4").region(Regions.US_EAST_1.getName())
        .type(AmazonLoadBalancerType.Application).build();



    loadBalancerRequest = this.amazonLoadBalancerManagement.create(request);
    assertNotNull(loadBalancerRequest);

    applicationLoadBalancerARN = loadBalancerRequest.getLoadBalancerArn();
    assertNotNull(applicationLoadBalancerARN);

  }

  @Test
  public void testA2DescribeApplicationLoadBalancer() throws Exception {
    AmazonLoadBalancer request =
        AmazonLoadBalancer.builder().loadBalancerArn(applicationLoadBalancerARN)
            .region(Regions.US_EAST_1.getName()).type(AmazonLoadBalancerType.Application).build();

    AmazonLoadBalancer result = this.amazonLoadBalancerManagement.describe(request);
    assertNotNull(result);
  }

  @Test
  public void testA3setIpAddressType() throws Exception {

    loadBalancerRequest.setIpAddressType("ipv4");

    AmazonLoadBalancer result =
        this.amazonLoadBalancerManagement.setIpAddressType(loadBalancerRequest);
    assertNotNull(result);
  }

  @Test
  public void testA4SetSecurityGroups() throws Exception {

    List<String> securityGroupIds = new ArrayList<>();

    List<SecurityGroup> securityGroups =
        amazonSecurityGroupManagement.findAllSecurityGroups(Regions.US_EAST_1.getName());

    for (SecurityGroup securityGroup : securityGroups) {
      if (securityGroup.getVpcId().equals(vpcId)) {
        securityGroupIds.add(securityGroup.getGroupId());
      }
    }

    loadBalancerRequest.setSecurityGroups(securityGroupIds);

    AmazonLoadBalancer result =
        this.amazonLoadBalancerManagement.setSecurityGroups(loadBalancerRequest);
    assertNotNull(result);
  }

  @Test
  public void testA5SetSubnets() throws Exception {

    createExtraVPCSubnets();
    setVPCSubnets();

    loadBalancerRequest.setSubnets(subnets);

    AmazonLoadBalancer result = this.amazonLoadBalancerManagement.setSubnets(loadBalancerRequest);
    assertNotNull(result);
  }

  @Test
  public void testA6CreateListener() throws Exception {

    Action action = new Action();
    action.setType(ActionTypeEnum.Forward);
    action.setTargetGroupArn(targetGroupARN);

    loadBalancerListenerRequest = AmazonLoadBalancerListener.builder().port(80).protocol("HTTP")
        .defaultActions(Collections.singletonList(action)).build();

    loadBalancerListenerRequest = this.amazonLoadBalancerManagement
        .createListener(loadBalancerRequest, loadBalancerListenerRequest);
    assertNotNull(loadBalancerListenerRequest);

    assertNotNull(loadBalancerListenerRequest.getListenerArn());
  }

  @Test
  public void testA7DeleteListener() throws Exception {
    this.amazonLoadBalancerManagement.deleteListener(loadBalancerRequest,
        loadBalancerListenerRequest);
  }

  @Test(expected = LoadBalancerException.class)
  public void testA8DeleteApplicationLoadBalancer() throws Exception {
    AmazonLoadBalancer request =
        AmazonLoadBalancer.builder().loadBalancerArn(applicationLoadBalancerARN)
            .region(Regions.US_EAST_1.getName()).type(AmazonLoadBalancerType.Application).build();

    this.amazonLoadBalancerManagement.delete(request);

    deleteVPCSubnets();

    this.amazonLoadBalancerManagement.describe(request);
  }

  @Test(expected = TargetGroupException.class)
  @Ignore
  public void testA9DeleteTargetGroup() throws Exception {
    this.amazonTargetGroupManagement.delete(amazonTargetGroupRequest);
    deleteVPCSubnets();
    this.amazonTargetGroupManagement.describe(amazonTargetGroupRequest);
  }

  @Test
  public void testB0CreateTargetGroup() throws Exception {

    createVPCSubnets();
    setVPCSubnets();

    amazonTargetGroupRequest = new AmazonTargetGroup();
    amazonTargetGroupRequest.setName(TARGET_GROUP_NAME);
    amazonTargetGroupRequest.setProtocol("TCP");
    amazonTargetGroupRequest.setPort(80);
    amazonTargetGroupRequest.setTargetType("instance");
    amazonTargetGroupRequest.setVpcId(vpcId);
    amazonTargetGroupRequest.setRegion(Regions.US_EAST_1.getName());

    amazonTargetGroupRequest = this.amazonTargetGroupManagement.create(amazonTargetGroupRequest);
    assertNotNull(amazonTargetGroupRequest);

    targetGroupARN = amazonTargetGroupRequest.getTargetGroupArn();
    assertNotNull(targetGroupARN);

  }

  @Test
  public void testB1CreateNetworkLoadBalancer() throws Exception {

    AmazonLoadBalancer request = AmazonLoadBalancer.builder().name(NETWORK_LOAD_BALANCER_NAME)
        .subnets(subnets).ipAddressType("ipv4").region(Regions.US_EAST_1.getName())
        .type(AmazonLoadBalancerType.Network).build();

    loadBalancerRequest = this.amazonLoadBalancerManagement.create(request);
    assertNotNull(loadBalancerRequest);

    networkLoadBalancerARN = loadBalancerRequest.getLoadBalancerArn();
    assertNotNull(networkLoadBalancerARN);

  }

  @Test
  public void testB2DescribeNetworkLoadBalancer() throws LoadBalancerException {

    AmazonLoadBalancer request =
        AmazonLoadBalancer.builder().loadBalancerArn(networkLoadBalancerARN)
            .region(Regions.US_EAST_1.getName()).type(AmazonLoadBalancerType.Network).build();

    AmazonLoadBalancer result = this.amazonLoadBalancerManagement.describe(request);
    assertNotNull(result);
  }

  @Test
  public void testB3setIpAddressType() throws Exception {

    loadBalancerRequest.setIpAddressType("ipv4");

    AmazonLoadBalancer result =
        this.amazonLoadBalancerManagement.setIpAddressType(loadBalancerRequest);
    assertNotNull(result);
  }

  @Test
  public void testB6CreateListener() throws Exception {

    Action action = new Action();
    action.setType(ActionTypeEnum.Forward);
    action.setTargetGroupArn(targetGroupARN);

    loadBalancerListenerRequest = AmazonLoadBalancerListener.builder().port(80).protocol("TCP")
        .defaultActions(Collections.singletonList(action)).build();

    loadBalancerListenerRequest = this.amazonLoadBalancerManagement
        .createListener(loadBalancerRequest, loadBalancerListenerRequest);
    assertNotNull(loadBalancerListenerRequest);

    assertNotNull(loadBalancerListenerRequest.getListenerArn());
  }

  @Test
  public void testB7DeleteListener() throws Exception {
    this.amazonLoadBalancerManagement.deleteListener(loadBalancerRequest,
        loadBalancerListenerRequest);
  }

  @Test(expected = LoadBalancerException.class)
  public void testB8DeleteNetworkLoadBalancer() throws LoadBalancerException {

    AmazonLoadBalancer request =
        AmazonLoadBalancer.builder().loadBalancerArn(networkLoadBalancerARN)
            .region(Regions.US_EAST_1.getName()).type(AmazonLoadBalancerType.Network).build();

    this.amazonLoadBalancerManagement.delete(request);

    this.amazonLoadBalancerManagement.describe(request);
  }

  @Test(expected = TargetGroupException.class)
  public void testB9DeleteTargetGroup() throws Exception {
    this.amazonTargetGroupManagement.delete(amazonTargetGroupRequest);
    deleteVPCSubnets();
    this.amazonTargetGroupManagement.describe(amazonTargetGroupRequest);
  }

}
