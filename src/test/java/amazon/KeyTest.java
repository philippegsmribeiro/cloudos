package amazon;

import static org.junit.Assert.assertNotNull;

import cloudos.amazon.AWSCredentialService;
import cloudos.amazon.Key;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

/**
 * KeyPair test request for AWS. @Copyright CyberTextron Inc. 2016
 *
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class KeyTest extends CloudOSTest {

  @Autowired AWSCredentialService awsCredentialService;
  protected AmazonEC2 client;
  protected Key key;

  protected static String keyname = generator.generate(10);

  @Before
  public void setUp() throws Exception {
    // use an amazon EC2 client
    this.client =
        AmazonEC2ClientBuilder.standard()
            .withCredentials(awsCredentialService.getAWSCredentials())
            .withRegion(Regions.US_EAST_1)
            .build();
    this.key = new Key();
  }

  @Test
  public void test1GetPrivateKey() {
    assertNotNull(this.key);
    assertNotNull(this.client);
    String privateKey = this.key.getPrivateKey(keyname, this.client);
    System.out.println("Created KeyPair: " + privateKey);
  }

  @Test
  public void test2DeleteKey() {
    assertNotNull(this.key);
    assertNotNull(this.client);
    this.key.deleteKey(keyname, this.client);
  }

  @Test
  public void test3SaveKeyPair() {
    assertNotNull(this.key);
    assertNotNull(this.client);
    String newkey = generator.generate(10);
    String keyPair = this.key.saveKeyPair(newkey, this.client);
    assertNotNull(keyPair);
    // delete the key;
    this.key.deleteKey(newkey, this.client);
  }
}
