package amazon;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import com.amazonaws.regions.Regions;

import cloudos.Providers;
import cloudos.models.network.AmazonSubnet;
import cloudos.models.network.CloudosSubnet;
import cloudos.models.network.CloudosSubnetRequest;
import cloudos.models.network.CloudosVpc;
import cloudos.models.network.CloudosVpcRequest;
import cloudos.network.AmazonSubnetManagement;
import cloudos.network.CloudosSubnetManagement;
import cloudos.network.CloudosVpcService;
import test.CloudOSTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public abstract class AmazonLoadBalancerSupportTest extends CloudOSTest {

  protected static String vpcRepositoryId = null;
  protected static List<String> subnets = new ArrayList<String>();
  protected static String vpcId = null;
  protected static CloudosVpc cloudosVpc;

  @Autowired
  private AmazonSubnetManagement amazonSubnetManagement;

  @Autowired
  private CloudosVpcService vpcService;

  @Autowired
  private CloudosSubnetManagement subnetManagement;

  protected void createVPCSubnets() throws Exception {
    CloudosVpcRequest cloudosVpcRequest = new CloudosVpcRequest(Providers.AMAZON_AWS,
        Regions.US_EAST_1.getName(), "172.31.0.0/16", new ArrayList<>(), false, "default",
        String.format("cloudos-%s", generator.generate(6).toLowerCase()));

    cloudosVpc = vpcService.create(cloudosVpcRequest);
    assertNotNull(cloudosVpc);
    vpcId = cloudosVpc.getVpcId();
    vpcRepositoryId = cloudosVpc.getVpcId();

    CloudosSubnetRequest cloudosSubnetRequest = new CloudosSubnetRequest(
        String.format("cloudos-%s", generator.generate(6).toLowerCase()), Providers.AMAZON_AWS,
        cloudosVpc.getVpcId(), "172.31.16.0/20", Regions.US_EAST_1.getName() + "a", false, false);

    CloudosSubnet result1 = subnetManagement.create(cloudosSubnetRequest, cloudosVpc);
    assertNotNull(result1);

    cloudosSubnetRequest = new CloudosSubnetRequest(
        String.format("cloudos-%s", generator.generate(6).toLowerCase()), Providers.AMAZON_AWS,
        cloudosVpc.getVpcId(), "172.31.64.0/20", Regions.US_EAST_1.getName() + "b", false, false);

    CloudosSubnet result2 = subnetManagement.create(cloudosSubnetRequest, cloudosVpc);
    assertNotNull(result2);
  }

  protected void createExtraVPCSubnets() throws Exception {
    CloudosSubnetRequest cloudosSubnetRequest = new CloudosSubnetRequest(
        String.format("cloudos-%s", generator.generate(6).toLowerCase()), Providers.AMAZON_AWS,
        vpcId, "172.31.48.0/20", Regions.US_EAST_1.getName() + "c", false, false);

    CloudosSubnet result1 = subnetManagement.create(cloudosSubnetRequest, cloudosVpc);
    assertNotNull(result1);

    cloudosSubnetRequest = new CloudosSubnetRequest(
        String.format("cloudos-%s", generator.generate(6).toLowerCase()), Providers.AMAZON_AWS,
        vpcId, "172.31.0.0/20", Regions.US_EAST_1.getName() + "d", false, false);

    CloudosSubnet result2 = subnetManagement.create(cloudosSubnetRequest, cloudosVpc);
    assertNotNull(result2);
  }

  protected void setVPCSubnets() throws Exception {
    subnets = new ArrayList<>();
    List<AmazonSubnet> amazonSubnets = (List<AmazonSubnet>) amazonSubnetManagement.list(cloudosVpc);
    for (AmazonSubnet amazonSubnet : amazonSubnets) {
      if (amazonSubnet.getVpc().equals(vpcId)) {
        subnets.add(amazonSubnet.getSubnetId());
      }
    }
  }

  protected void deleteVPCSubnets() throws Exception {
    CloudosVpc cloudosVpc = this.vpcService.describe(Providers.AMAZON_AWS,
        Regions.US_EAST_1.getName(), vpcRepositoryId);
    this.vpcService.delete(cloudosVpc);
  }

}
