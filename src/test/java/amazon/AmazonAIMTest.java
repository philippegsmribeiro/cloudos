package amazon;

import static org.junit.Assert.assertNotNull;

import cloudos.amazon.AWSCredentialService;
import cloudos.amazon.AmazonAIM;

import com.amazonaws.regions.Regions;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

/** Created by philipperibeiro on 5/5/17. */
public class AmazonAIMTest extends CloudOSTest {

  @Autowired AWSCredentialService awsCredentialService;

  @Test
  public void testAmazonAMI() throws Exception {
    // test default constructor
    AmazonAIM amazonAIM =
        new AmazonAIM(Regions.US_EAST_1.getName(), awsCredentialService.getAWSCredentials());
    assertNotNull(amazonAIM);

    AmazonAIM amazonAIM1 =
        new AmazonAIM(Regions.SA_EAST_1.getName(), awsCredentialService.getAWSCredentials());
    assertNotNull(amazonAIM1);
  }
}
