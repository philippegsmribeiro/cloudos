package amazon;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import cloudos.amazon.AWSCredentialService;
import cloudos.amazon.S3;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.model.Bucket;

import java.nio.file.Paths;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

public class S3Test extends CloudOSTest {

  @Autowired AWSCredentialService awsCredentialService;

  private S3 s3;

  @Before
  public void setUp() throws Exception {
    this.s3 = new S3(Regions.US_EAST_1.getName(), awsCredentialService.getAWSCredentials());
  }

  @Test
  public void testGetBucketList() throws Exception {
    List<Bucket> buckets = this.s3.listBuckets();
    assertNotNull(buckets);
    assertThat("Buckets", buckets.size(), greaterThan(-1));
  }

  @Test
  @SuppressWarnings("Duplicates")
  public void testCreateBucket() throws Exception {

    String bucket =
        String.format("cloudos%s", generator.generate(10));
    this.s3.createBucket(bucket);
    assertTrue(this.s3.bucketExists(bucket));
    this.s3.deleteBucket(bucket);
    assertFalse(this.s3.bucketExists(bucket));
  }

  @Test
  @SuppressWarnings("Duplicates")
  public void testDeleteBucket() throws Exception {
    String bucket =
        String.format("cloudos%s", generator.generate(10));
    this.s3.createBucket(bucket);
    // make sure the bucket exists
    assertTrue(this.s3.bucketExists(bucket));
    // delete the bucket
    this.s3.deleteBucket(bucket);
    // make sure the bucket does not exist anymore
    assertFalse(this.s3.bucketExists(bucket));
  }

  @Test
  public void testListBucket() throws Exception {
    List<Bucket> buckets = this.s3.listBuckets();
    assertNotNull(buckets);
    assertNotEquals(buckets.size(), 0);
  }

  @Test
  public void testObjectMd5() throws Exception {
    String path = Paths.get("autoscaler", "production", "decider.zip").toString();
    String md5 = this.s3.getObjectMd5("cloudos-models-development", path);
    assertNotNull(md5);

    logger.debug("MD5 value: {}", md5);
  }

  @Test
  public void testBucketExists() throws Exception {
    String bucket =
        String.format("cloudos%s", generator.generate(10));
    // now attempt to create the bucket in a different region
    this.s3.createBucket(bucket, "us-west-2");
    assertTrue(this.s3.bucketExists(bucket));
    // delete the bucket
    this.s3.deleteBucket(bucket);
    // make sure the bucket does not exist anymore
    assertFalse(this.s3.bucketExists(bucket));
  }
}
