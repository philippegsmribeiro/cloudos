package amazon;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import cloudos.amazon.AWSCredentialSingleton;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;

import org.junit.Test;

import test.CloudOSTest;

public class AwsCredentialSingletonTest extends CloudOSTest {

  @Test
  public void testSingleton() {
    AWSCredentials credentials = null;
    assertNull(credentials);
    /* credentials were correctly created */
    credentials = AWSCredentialSingleton.getCredentials();
    assertNotNull(credentials);
  }

  @Test
  public void testProvider() {
    AWSCredentialsProvider provider = AWSCredentialSingleton.getProvider();
    assertNotNull(provider);
    assertNotNull(provider.getCredentials());
  }
}
