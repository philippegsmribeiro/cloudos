package amazon;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import com.amazonaws.regions.Regions;

import cloudos.models.AmazonTargetGroup;
import cloudos.targetgroups.AmazonTargetGroupManagement;
import cloudos.targetgroups.TargetGroupException;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Ignore
public class AmazonTargetGroupsTest extends AmazonLoadBalancerSupportTest {

  private final String TARGET_GROUP_NAME = String.format("cloudos%s", generator.generate(8));
  private static String targetGroupARN = null;
  private static String idTargetGroup = null;
  private static AmazonTargetGroup amazonTargetGroupRequest = new AmazonTargetGroup();

  @Autowired
  private AmazonTargetGroupManagement amazonTargetGroupManagement;

  @Test
  public void testACreateTargetGroup() throws Exception {
    createVPCSubnets();
    setVPCSubnets();

    amazonTargetGroupRequest = new AmazonTargetGroup();
    amazonTargetGroupRequest.setName(TARGET_GROUP_NAME);
    amazonTargetGroupRequest.setProtocol("HTTP");
    amazonTargetGroupRequest.setPort(80);
    amazonTargetGroupRequest.setTargetType("instance");
    amazonTargetGroupRequest.setVpcId(vpcId);
    amazonTargetGroupRequest.setRegion(Regions.US_EAST_1.getName());

    amazonTargetGroupRequest = this.amazonTargetGroupManagement.create(amazonTargetGroupRequest);
    assertNotNull(amazonTargetGroupRequest);
    targetGroupARN = amazonTargetGroupRequest.getTargetGroupArn();
    idTargetGroup = amazonTargetGroupRequest.getId();

    amazonTargetGroupRequest.setRegion(Regions.US_EAST_1.getName());
    AmazonTargetGroup amazonTargetGroup =
        this.amazonTargetGroupManagement.describe(amazonTargetGroupRequest);
    assertNotNull(amazonTargetGroup);
    assertEquals(amazonTargetGroup.getProtocol(), amazonTargetGroupRequest.getProtocol());
    assertEquals(amazonTargetGroup.getPort(), amazonTargetGroupRequest.getPort());
    assertEquals(amazonTargetGroup.getTargetType(), amazonTargetGroupRequest.getTargetType());
    assertEquals(amazonTargetGroup.getTargetGroupArn(),
        amazonTargetGroupRequest.getTargetGroupArn());
    assertEquals(amazonTargetGroup.getVpcId(), amazonTargetGroupRequest.getVpcId());

    amazonTargetGroup.setRegion(Regions.US_EAST_1.getName());
    amazonTargetGroup = this.amazonTargetGroupManagement.modify(amazonTargetGroup);
    assertNotNull(amazonTargetGroup);
    assertEquals(amazonTargetGroup.getProtocol(), amazonTargetGroupRequest.getProtocol());
    assertEquals(amazonTargetGroup.getPort(), amazonTargetGroupRequest.getPort());
    assertEquals(amazonTargetGroup.getTargetType(), amazonTargetGroupRequest.getTargetType());
    assertEquals(amazonTargetGroup.getTargetGroupArn(),
        amazonTargetGroupRequest.getTargetGroupArn());
    assertEquals(amazonTargetGroup.getVpcId(), amazonTargetGroupRequest.getVpcId());

  }

  @Test(expected = TargetGroupException.class)
  public void testBCreateTargetGroupWithWrongAttributes() throws Exception {
    amazonTargetGroupRequest = new AmazonTargetGroup();
    amazonTargetGroupRequest.setName(null);
    amazonTargetGroupRequest.setProtocol("HTTP");
    amazonTargetGroupRequest.setPort(80);
    amazonTargetGroupRequest.setTargetType("instance");
    amazonTargetGroupRequest.setVpcId(vpcId);
    amazonTargetGroupRequest.setRegion(Regions.US_EAST_1.getName());

    this.amazonTargetGroupManagement.create(amazonTargetGroupRequest);
  }

  @Test(expected = TargetGroupException.class)
  public void testCCreateTargetGroupWithWrongAttributes() throws Exception {
    amazonTargetGroupRequest = new AmazonTargetGroup();
    amazonTargetGroupRequest.setName(TARGET_GROUP_NAME);
    amazonTargetGroupRequest.setProtocol(null);
    amazonTargetGroupRequest.setPort(80);
    amazonTargetGroupRequest.setTargetType("instance");
    amazonTargetGroupRequest.setVpcId(vpcId);
    amazonTargetGroupRequest.setRegion(Regions.US_EAST_1.getName());

    this.amazonTargetGroupManagement.create(amazonTargetGroupRequest);
  }

  @Test(expected = TargetGroupException.class)
  public void testDCreateTargetGroupWithWrongAttributes() throws Exception {
    amazonTargetGroupRequest = new AmazonTargetGroup();
    amazonTargetGroupRequest.setName(TARGET_GROUP_NAME);
    amazonTargetGroupRequest.setPort(80);
    amazonTargetGroupRequest.setTargetType("instance");
    amazonTargetGroupRequest.setVpcId(vpcId);
    amazonTargetGroupRequest.setRegion(Regions.US_EAST_1.getName());

    this.amazonTargetGroupManagement.create(amazonTargetGroupRequest);
  }

  @Test(expected = TargetGroupException.class)
  public void testECreateTargetGroupWithWrongAttributes() throws Exception {
    amazonTargetGroupRequest = new AmazonTargetGroup();
    amazonTargetGroupRequest.setName(TARGET_GROUP_NAME);
    amazonTargetGroupRequest.setProtocol("HTTP");
    amazonTargetGroupRequest.setPort(null);
    amazonTargetGroupRequest.setTargetType("instance");
    amazonTargetGroupRequest.setVpcId(vpcId);
    amazonTargetGroupRequest.setRegion(Regions.US_EAST_1.getName());

    this.amazonTargetGroupManagement.create(amazonTargetGroupRequest);
  }

  public void testFCreateTargetGroupWithWrongAttributes() throws Exception {
    amazonTargetGroupRequest = new AmazonTargetGroup();
    amazonTargetGroupRequest.setName(TARGET_GROUP_NAME);
    amazonTargetGroupRequest.setProtocol("HTTP");
    amazonTargetGroupRequest.setPort(80);
    amazonTargetGroupRequest.setTargetType("WRONG_TYPE");
    amazonTargetGroupRequest.setVpcId(vpcId);
    amazonTargetGroupRequest.setRegion(Regions.US_EAST_1.getName());

    this.amazonTargetGroupManagement.create(amazonTargetGroupRequest);
  }

  @Test(expected = TargetGroupException.class)
  public void testGCreateTargetGroupWithWrongAttributes() throws Exception {
    amazonTargetGroupRequest = new AmazonTargetGroup();
    amazonTargetGroupRequest.setName(TARGET_GROUP_NAME);
    amazonTargetGroupRequest.setProtocol("HTTP");
    amazonTargetGroupRequest.setPort(80);
    amazonTargetGroupRequest.setTargetType("instance");
    amazonTargetGroupRequest.setVpcId(null);
    amazonTargetGroupRequest.setRegion(Regions.US_EAST_1.getName());

    this.amazonTargetGroupManagement.create(amazonTargetGroupRequest);
  }

  @Test(expected = TargetGroupException.class)
  public void testHCreateTargetGroupWithWrongAttributes() throws Exception {
    amazonTargetGroupRequest = new AmazonTargetGroup();
    amazonTargetGroupRequest.setName(TARGET_GROUP_NAME);
    amazonTargetGroupRequest.setProtocol("HTTP");
    amazonTargetGroupRequest.setPort(80);
    amazonTargetGroupRequest.setTargetType("instance");
    amazonTargetGroupRequest.setVpcId(vpcId);
    amazonTargetGroupRequest.setRegion(null);

    this.amazonTargetGroupManagement.create(amazonTargetGroupRequest);
  }

  @Test(expected = TargetGroupException.class)
  public void testKDescribeTargetGroupWithWrongAttributes() throws Exception {
    this.amazonTargetGroupManagement.describe(new AmazonTargetGroup());
  }

  @Test(expected = TargetGroupException.class)
  public void testMModifyTargetGroupWithWrongAttributes() throws Exception {
    this.amazonTargetGroupManagement.modify(new AmazonTargetGroup());
  }

  @Test(expected = TargetGroupException.class)
  public void testODeleteTargetGroupWithWrongAttributes() throws Exception {
    this.amazonTargetGroupManagement.delete(new AmazonTargetGroup());
  }

  @Test(expected = TargetGroupException.class)
  public void testIDeleteTargetGroup() throws Exception {

    try {
      amazonTargetGroupRequest.setTargetGroupArn(targetGroupARN);
      amazonTargetGroupRequest.setRegion(Regions.US_EAST_1.getName());
      amazonTargetGroupRequest.setId(idTargetGroup);
      this.amazonTargetGroupManagement.delete(amazonTargetGroupRequest);
    } catch (Exception e) {
      fail("Error while deleting target group");
    }

    deleteVPCSubnets();
    this.amazonTargetGroupManagement.describe(amazonTargetGroupRequest);

  }

}
