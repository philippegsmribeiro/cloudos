package amazon;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.model.SecurityGroup;
import com.amazonaws.services.elasticloadbalancing.model.HealthCheck;

import cloudos.loadbalancer.AmazonLoadBalancerManagement;
import cloudos.loadbalancer.LoadBalancerException;
import cloudos.models.AmazonLoadBalancer;
import cloudos.models.AmazonLoadBalancerListener;
import cloudos.models.AmazonLoadBalancerType;
import cloudos.models.network.AwsVpc;
import cloudos.network.AmazonVpcManagement;
import cloudos.securitygroup.AmazonSecurityGroupManagement;
import test.CloudOSTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Ignore
public class AmazonClassicLoadBalancerTest extends CloudOSTest {

  private static final String HEALTH_CHECK_TARGET = "HTTP:80/weather/us/wa/seattle";
  private static final int LOAD_BALANCER_PORT = 80;
  private static final int INSTANCE_PORT = 80;
  private static final String LISTENER_PROTOCOL = "HTTP";
  private static final String LOAD_BALANCER_NAME =
      String.format("cloudos%s", generator.generate(8));
  private static AmazonLoadBalancerListener listenerRequest;
  private static AmazonLoadBalancer loadBalancarRequest;

  @Autowired
  private AmazonLoadBalancerManagement amazonLoadBalancerManagement;

  @Autowired
  private AmazonSecurityGroupManagement amazonSecurityGroupManagement;

  @Autowired
  private AmazonVpcManagement amazonVpcManagement;

  @Test
  public void testACreateClassicLoadBalancer() throws Exception {

    HealthCheck healthCheck = new HealthCheck().withHealthyThreshold(10).withInterval(30)
        .withTimeout(5).withUnhealthyThreshold(2).withTarget(HEALTH_CHECK_TARGET);

    List<String> availabilityZones = Collections.singletonList(Regions.US_EAST_1.getName() + "a");

    AmazonLoadBalancerListener listener =
        AmazonLoadBalancerListener.builder().protocol(LISTENER_PROTOCOL)
            .loadBalancerPort(LOAD_BALANCER_PORT).instancePort(INSTANCE_PORT).build();

    loadBalancarRequest =
        AmazonLoadBalancer.builder().name(LOAD_BALANCER_NAME).availabilityZones(availabilityZones)
            .listeners(Collections.singletonList(listener)).healthCheck(healthCheck)
            .region(Regions.US_EAST_1.getName()).type(AmazonLoadBalancerType.Classic).build();

    loadBalancarRequest = this.amazonLoadBalancerManagement.create(loadBalancarRequest);
    assertNotNull(loadBalancarRequest);
  }

  @Test
  public void testBDescribeClassicLoadBalancer() throws LoadBalancerException {
    AmazonLoadBalancer result = this.amazonLoadBalancerManagement.describe(loadBalancarRequest);
    assertNotNull(result);
  }

  @Test
  public void testCSetSecurityGroups() throws Exception {

    List<AwsVpc> vpcs = (List<AwsVpc>) amazonVpcManagement.list(Regions.US_EAST_1.getName());

    String defaultVpcId = null;
    for (AwsVpc vpc : vpcs) {
      if (vpc.getIsDefault()) {
        defaultVpcId = vpc.getVpcId();
      }
    }

    List<String> securityGroupIds = new ArrayList<String>();

    List<SecurityGroup> securityGroups =
        amazonSecurityGroupManagement.findAllSecurityGroups(Regions.US_EAST_1.getName());

    for (SecurityGroup securityGroup : securityGroups) {
      if (securityGroup.getVpcId().equals(defaultVpcId)) {
        securityGroupIds.add(securityGroup.getGroupId());
      }
    }

    loadBalancarRequest.setSecurityGroups(securityGroupIds);

    loadBalancarRequest = this.amazonLoadBalancerManagement.setSecurityGroups(loadBalancarRequest);
    assertNotNull(loadBalancarRequest);
  }

  @Test
  public void testDCreateListener() throws Exception {

    listenerRequest = AmazonLoadBalancerListener.builder().protocol(LISTENER_PROTOCOL)
        .loadBalancerPort(81).instancePort(81).build();

    listenerRequest =
        this.amazonLoadBalancerManagement.createListener(loadBalancarRequest, listenerRequest);

    assertNotNull(loadBalancarRequest);
  }

  @Test
  public void testEDeleteListener() throws Exception {
    this.amazonLoadBalancerManagement.deleteListener(loadBalancarRequest, listenerRequest);
  }


  @Test(expected = LoadBalancerException.class)
  public void testFDeleteClassicLoadBalancer() throws Exception {
    AmazonLoadBalancer request = AmazonLoadBalancer.builder().name(LOAD_BALANCER_NAME)
        .region(Regions.US_EAST_1.getName()).type(AmazonLoadBalancerType.Classic).build();

    this.amazonLoadBalancerManagement.delete(request);
    this.amazonLoadBalancerManagement.describe(request);
  }

  @Test(expected = LoadBalancerException.class)
  public void testRDeleteLoadBalancerWithInvalidParams() throws LoadBalancerException {
    this.amazonLoadBalancerManagement.delete(new AmazonLoadBalancer());
  }


}
