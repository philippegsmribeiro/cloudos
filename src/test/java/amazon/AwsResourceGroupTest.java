package amazon;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import cloudos.amazon.AWSCredentialService;
import cloudos.amazon.AwsResourceGroup;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.inspector.model.ResourceGroup;
import com.amazonaws.services.inspector.model.ResourceGroupTag;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

/** Created by philipperibeiro on 5/20/17. */
public class AwsResourceGroupTest extends CloudOSTest {
  @Autowired AWSCredentialService awsCredentialService;
  private AwsResourceGroup resourceGroup;
  private List<ResourceGroupTag> resourceGroupTagList;
  private static Set<String> arns = new HashSet<>();

  @Before
  public void setUp() throws Exception {
    this.resourceGroup =
        new AwsResourceGroup(Regions.US_EAST_1.getName(), awsCredentialService.getAWSCredentials());
    this.resourceGroupTagList =
        Arrays.asList(
            new ResourceGroupTag()
                .withKey(generator.generate(8))
                .withValue(generator.generate(8)),
            new ResourceGroupTag()
                .withKey(generator.generate(8))
                .withValue(generator.generate(8)));
  }

  @Test
  public void testCreateResourceGroup() throws Exception {
    String arn = this.resourceGroup.createResourceGroup(this.resourceGroupTagList);
    System.out.println("Created a resource group with arn " + arn);
    assertNotNull(arn);
    arns.add(arn);
  }

  @Test
  public void testListResourceGroups() throws Exception {
    List<ResourceGroup> resourceGroups = this.resourceGroup.getResourceGroups(arns);
    assertNotNull(resourceGroups);
    for (ResourceGroup resourceGroup : resourceGroups) {
      System.out.println("Resource Group: " + resourceGroup);
    }
    assertNotEquals(resourceGroups.size(), 0);
  }
}
