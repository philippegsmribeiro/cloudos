package amazon;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import cloudos.amazon.AWSCredentialService;
import cloudos.amazon.SQS;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.model.Message;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

/** Created by philipperibeiro on 6/15/17. */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SQSTest extends CloudOSTest {

  @Autowired AWSCredentialService awsCredentialService;

  private SQS sqs;

  private static String queue_name =
      String.format("Cloudos-%s", generator.generate(10));

  private static String message = generator.generate(30);

  private static List<String> queues = new ArrayList<>();

  @Before
  public void setUp() throws Exception {
    this.sqs = new SQS(Regions.US_EAST_1.getName(), awsCredentialService.getAWSCredentials());
  }

  @Test
  public void testACreateQueue() throws Exception {
    String queue_url = this.sqs.createQueue(queue_name);
    assertNotNull(queue_url);
    queues.add(queue_url);
  }

  @Test
  public void testBListQueues() throws Exception {
    List<String> queue_list = this.sqs.listQueues();
    assertThat(queue_list.size(), greaterThan(0));
  }

  @Test
  public void testCSendMessage() throws Exception {
    assertTrue(this.sqs.sendMessage(queues.get(0), message, 1));
  }

  @Test
  public void testDReadMessage() throws Exception {
    List<Message> messages = this.sqs.readMessages(queues.get(0));
    assertNotNull(messages);
    assertThat(messages.size(), greaterThan(-1));
  }

  @Test
  public void testEMessageContent() throws Exception {
    List<Message> messages = this.sqs.readMessages(queues.get(0));
    assertNotNull(messages);
    assertThat(messages.size(), greaterThan(-1));
    this.sqs.readContent(messages);
  }

  @Test
  public void testFDeleteQueue() throws Exception {
    for (String queue_url : queues) {
      assertTrue(this.sqs.deleteQueue(queue_url));
    }
  }
}
