package amazon;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import java.util.List;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.model.Subnet;
import com.amazonaws.services.ec2.model.Vpc;
import cloudos.amazon.AWSCredentialService;
import cloudos.amazon.VPC;
import test.CloudOSTest;

/** Created by philipperibeiro on 6/26/17. */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Ignore
public class VPCTest extends CloudOSTest {

  @Autowired AWSCredentialService awsCredentialService;

  private VPC vpc;

  private static Vpc created;

  @Before
  public void setUp() throws Exception {
    this.vpc = new VPC(Regions.US_EAST_1.getName(), awsCredentialService.getAWSCredentials());
  }

  @Test
  public void testACreateVPC() throws Exception {
    created = this.vpc.createVpc("10.0.0.0/28");
    assertNotNull(created);
    assertEquals(created.getCidrBlock(), "10.0.0.0/28");
  }

  @Test
  public void testBListVPC() throws Exception {
    List<Vpc> vpcList = this.vpc.listVpcs();
    assertNotNull(vpcList);
    assertThat(vpcList.size(), greaterThan(0));
  }

  @Test
  public void testCListSubnets() throws Exception {
    List<Subnet> subnets = this.vpc.listSubnets();
    assertNotNull(subnets);
    assertThat(subnets.size(), greaterThan(0));
  }

  @Test
  public void testDDescribeVPC() throws Exception {
    List<Vpc> vpcList = this.vpc.describeVpc(created.getVpcId());
    assertNotNull(vpcList);
    assertEquals(vpcList.size(), 1);
  }

  @Test
  public void testEDeleteVPC() throws Exception {
    assertNotNull(created);
    this.vpc.deleteVpc(created.getVpcId());
  }
}
