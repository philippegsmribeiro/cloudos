package amazon;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import cloudos.amazon.AWSCredentialService;
import cloudos.amazon.AmazonEC2OnDemand;
import cloudos.amazon.CloudWatch;
import cloudos.amazon.CloudWatchDimensions;
import cloudos.amazon.CloudWatchMetrics;
import cloudos.amazon.Key;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.cloudwatch.model.ComparisonOperator;
import com.amazonaws.services.cloudwatch.model.Datapoint;
import com.amazonaws.services.cloudwatch.model.Metric;
import com.amazonaws.services.cloudwatch.model.MetricAlarm;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.Instance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;
import test.SlowTest;

/**
 * Unit test for CloudWatch @Copyright 2017 Cybernetic Cloud Inc.
 *
 * @license: Proprietary License.
 * @author Philippe Ribeiro
 * @date: 1/6/2017
 */
@Category(SlowTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CloudWatchTest extends CloudOSTest {

  private final String securityGroup = "default";
  private static final String keyPairName = generator.generate(8);
  private static Key key = new Key();
  private static AmazonEC2OnDemand onDemand;
  private static AmazonEC2 client;
  private static boolean isInitialized = false;
  private static CloudWatch cloudWatch;

  @Autowired
  AWSCredentialService awsCredentialService;

  private static List<String> instanceIds;
  private static String alarmName = String.format("%s_%s", "cloudos", generator.generate(8));
  private static Instance instance = new Instance();

  @Before
  @SuppressWarnings("Duplicates")
  public void setUp() throws Exception {
    if (!isInitialized) {
      //
      cloudWatch =
          new CloudWatch(Regions.US_EAST_2.getName(), awsCredentialService.getAWSCredentials());
      // add initial setup
      client = AmazonEC2ClientBuilder.standard()
          .withCredentials(awsCredentialService.getAWSCredentials()).withRegion(Regions.US_EAST_2)
          .build();
      assertNotNull(key);
      assertNotNull(client);
      String privateKey = key.getPrivateKey(keyPairName, client);
      assertNotNull(privateKey);
      onDemand = new AmazonEC2OnDemand(this.securityGroup, client);
      isInitialized = true;
      instanceIds = new ArrayList<>();

      assertNotNull(onDemand);
      Set<Instance> instancesAlreadyCreated = onDemand.getInstances();
      if(instancesAlreadyCreated.isEmpty()) {
        // create the instance at the end
        List<Instance> instances = onDemand.createInstance("ami-618fab04", "t2.micro", 1, 1,
            CloudWatchTest.keyPairName, this.securityGroup, null, null, null, null);
        // check if there's a single instance in the instanceId list.
        assertEquals(instances.size(), 1);
        instance = instances.get(0);
        instances.forEach(instance -> instanceIds.add(instance.getInstanceId()));
      } else {
        instance = instancesAlreadyCreated.iterator().next();
        instanceIds.add(instance.getInstanceId());
      }

    }
  }

  @Test
  public void testCreateAlarm() throws Exception {

    // configure the new alert
    assertNotNull(CloudWatchTest.cloudWatch.createAlarm("InstanceId", instance.getInstanceId(),
        alarmName, ComparisonOperator.GreaterThanThreshold, 1, "CPUUtilization", "AWS/EC2", false,
        70.0, 60, "Alarm when server CPU utilization exceeds 70%"));

    // Find the Alarm in the list
    Set<MetricAlarm> metricAlarms = CloudWatchTest.cloudWatch.listAlarms();
    boolean found = false;
    for (MetricAlarm metric : metricAlarms) {
      if (metric.getAlarmName().equals(alarmName)) {
        found = true;
      }
    }

    // alarm must be found
    assertTrue(found);
  }

  @Test
  public void testDeleteAlarms() throws Exception {
    // attempt to delete a dummy
    assertTrue(CloudWatchTest.cloudWatch.deleteAlarms("dummy_alarm"));

    // attempt to delete a real alert
    assertTrue(CloudWatchTest.cloudWatch.deleteAlarms(alarmName));
  }

  @Test
  public void testDisableAlarms() throws Exception {
    assertTrue(CloudWatchTest.cloudWatch.disableAlarmAction("dummy_alarm"));

    // attempt to enable a real alert
    assertTrue(CloudWatchTest.cloudWatch.disableAlarmAction(alarmName));
  }

  @Test
  public void testEnableAlarms() throws Exception {
    assertTrue(CloudWatchTest.cloudWatch.enableAlarmAction("dummy_alarm"));

    // attempt to enable a real alert
    assertTrue(CloudWatchTest.cloudWatch.enableAlarmAction(alarmName));
  }

  @Test
  public void testGetdatapoints() throws Exception {
    // every 10 minutes, since the last 24hrs....
    final long twentyFourHrs = 1000 * 60 * 60 * 24;
    final int tenMinutes = 60 * 10;

    /* Perform test one */
    List<String> statistics = Arrays.asList("Average", "Maximum");
    List<Datapoint> datapoints1 = cloudWatch.getDatapoints(Arrays.asList(instance.getInstanceId()),
        CloudWatchDimensions.INSTANCE_ID, CloudWatchMetrics.CPU_UTILIZATION, statistics, tenMinutes,
        twentyFourHrs);
    assertNotNull(datapoints1);
    assertThat("datapoints", datapoints1.size(), greaterThanOrEqualTo(0));

    /* Perform test two */
    List<Datapoint> datapoints2 = cloudWatch.getDatapoints(Arrays.asList(instance.getInstanceId()),
        CloudWatchDimensions.INSTANCE_ID, CloudWatchMetrics.DISK_READ_BYTES, statistics, tenMinutes,
        twentyFourHrs);
    assertNotNull(datapoints2);
    assertThat("datapoints", datapoints2.size(), greaterThanOrEqualTo(0));

    // Create another client for another region
    CloudWatch cloudWatch2 = new CloudWatch("us-west-1", awsCredentialService.getAWSCredentials());

    /* Perform test one */
    statistics = Arrays.asList("Average", "Maximum");
    datapoints1 = cloudWatch2.getDatapoints(Arrays.asList(instance.getInstanceId()),
        CloudWatchDimensions.INSTANCE_ID, CloudWatchMetrics.CPU_UTILIZATION, statistics, tenMinutes,
        twentyFourHrs);
    assertNotNull(datapoints1);
    assertThat("datapoints", datapoints1.size(), greaterThanOrEqualTo(0));

    /* Perform test two */
    datapoints2 = cloudWatch2.getDatapoints(Arrays.asList(instance.getInstanceId()),
        CloudWatchDimensions.INSTANCE_ID, CloudWatchMetrics.DISK_READ_BYTES, statistics, tenMinutes,
        twentyFourHrs);
    assertNotNull(datapoints2);
    assertThat("datapoints", datapoints2.size(), greaterThanOrEqualTo(0));
  }

  @Test
  public void testListAlarms() throws Exception {
    Set<MetricAlarm> metricAlarm = CloudWatchTest.cloudWatch.listAlarms();
    assertNotNull(metricAlarm);
  }

  @Test
  public void testListMetrics() throws Exception {
    Set<Metric> metrics = CloudWatchTest.cloudWatch.listMetrics("CPUUtilization", "AWS/EC2");
    assertNotNull(metrics);
  }

  @Test
  public void testTearDown() throws Exception {
    if (onDemand != null) {
      onDemand.terminate(instanceIds);
      key.deleteKey(keyPairName, client);
    }
  }
}
