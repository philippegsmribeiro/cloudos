package amazon;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import cloudos.amazon.AmazonS3Service;
import cloudos.utils.FileUtils;

import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import java.io.File;
import java.util.List;
import java.util.Set;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import test.CloudOSTest;
/** Created by philipperibeiro on 12/23/16. */
public class AmazonS3ServiceTest extends CloudOSTest {

  @Autowired private AmazonS3Service s3Service;

  @Value("${cloudos.build.endpoint.repository}")
  private String repositoryEndpoint;

  @Test
  public void testCreateBucket() throws Exception {
    String bucketname1 =
        String.format(
            "s3-service-bucket-test%s", generator.generate(10));

    String region1 = "us-east-1";
    Bucket bucket1 = this.s3Service.createBucket(bucketname1, region1);
    logger.debug(String.format("########### Bucket 1: %s", bucket1));
    assertNotNull(bucket1);

    String bucketname2 =
        String.format(
            "s3-service-bucket-test%s", generator.generate(10));

    String region2 = "us-west-1";
    Bucket bucket2 = this.s3Service.createBucket(bucketname2, region2);
    logger.debug(String.format("########### Bucket 1: %s", bucket2));
    assertNotNull(bucket2);

    assertTrue(this.s3Service.deleteBucket(bucketname1, region1));
    assertTrue(this.s3Service.deleteBucket(bucketname2, region2));
  }

  @Test
  public void testUploadFile() throws Exception {
    String bucketname = String.format("s3-service-bucket-upload%s", generator.generate(10));
    s3Service.deleteBucket(bucketname, "us-east-1");
    Bucket bucket = this.s3Service.createBucket(bucketname, "us-east-1");
    logger.debug(String.format("########### Bucket 1: %s", bucket));
    assertNotNull(bucket);

    String currentDir = System.getProperty("user.dir");
    String filepath = currentDir + File.separator + "data/data.zip";
    String key = "data.zip";
    assertTrue(this.s3Service.uploadFile(bucketname, "us-east-1", key, filepath));

    assertTrue(this.s3Service.deleteBucket(bucketname, "us-east-1"));
  }

  @Test
  public void testListBuckets() throws Exception {
    String bucketname = String.format("s3-service-bucket-list%s", generator.generate(10));
    Bucket bucket = this.s3Service.createBucket(bucketname, "us-east-1");
    logger.debug(String.format("########### Bucket 1: %s", bucket));
    assertNotNull(bucket);

    List<Bucket> buckets = this.s3Service.listBucket("us-east-1");
    assertNotNull(buckets);
    assertThat("Number of Buckets", buckets.size(), greaterThan(-1));

    Set<S3ObjectSummary> summarySet = this.s3Service.listBucketContent(bucketname, "us-east-1");
    assertNotNull(summarySet);
    assertThat("Summary Set", summarySet.size(), greaterThan(-1));

    // Delete the bucket
    assertTrue(this.s3Service.deleteBucket(bucketname, "us-east-1"));
  }

  @Test
  @Ignore
  public void testBucketFolder() throws Exception {
    String bucketname = String.format("s3-service-bucket-folder%s", generator.generate(10));
    Bucket bucket = this.s3Service.createBucket(bucketname, "us-east-1");
    logger.debug(String.format("########### Bucket 1: %s", bucket));
    assertNotNull(bucket);

    // Test creating a folder
    String foldername = this.s3Service.createFolder(bucketname, "us-east-1", "testfolder");
    assertNotNull(foldername);

    // Test deleting a folder
    assertTrue(this.s3Service.deleteFolder(bucketname, "us-east-1", foldername));

    // upload a folder to the S3 bucket
    String currentDir = System.getProperty("user.dir");
    String folderpath = currentDir + File.separator + "data";
    assertTrue(this.s3Service.uploadFolder(bucketname, "us-east-1", folderpath));

    // Now download the folder uploaded
    String destination = this.repositoryEndpoint + File.separator + "data";
    assertTrue(this.s3Service.downloadFolder(bucketname, "us-east-1", "data", destination));

    // delete the bucket
    assertTrue(this.s3Service.deleteBucket(bucketname, "us-east-1"));

    FileUtils.delete(new File(destination));
  }

  @Test
  public void testDownloadFile() throws Exception {
    // Create bucket
    String bucketname = String.format("s3-service-bucket-download%s", generator.generate(10));
    Bucket bucket = this.s3Service.createBucket(bucketname, "us-east-1");
    logger.debug(String.format("########### Bucket 1: %s", bucket));
    assertNotNull(bucket);

    // Upload
    String currentDir = System.getProperty("user.dir");
    String filepath = currentDir + File.separator + "data/data.zip";
    String key = "data.zip";
    assertTrue(this.s3Service.uploadFile(bucketname, "us-east-1", key, filepath));

    // Download
    String downloadpath = this.repositoryEndpoint + File.separator + "data.zip";
    assertTrue(this.s3Service.downloadFile(bucketname, "us-east-1", "data.zip", downloadpath, true, false));
    assertTrue(this.s3Service.deleteBucket(bucketname, "us-east-1"));

    File file = new File(downloadpath);
    FileUtils.delete(file);
  }
}
