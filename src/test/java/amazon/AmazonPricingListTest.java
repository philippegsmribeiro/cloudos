package amazon;

import static org.junit.Assert.assertTrue;

import cloudos.amazon.AmazonPricingList;

import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

public class AmazonPricingListTest extends CloudOSTest {

  @Autowired
  private AmazonPricingList pricing;

  @SuppressWarnings("unchecked")
  @Test
  public void testGetOffers() throws Exception {
    Map<String, Object> offers = pricing.getOffers();
    assertTrue(offers.size() >= 1);
    for (Map.Entry<String, Object> entry : offers.entrySet()) {
      if (entry.getKey().equals("offers")) {
        Map<String, Object> map = (Map<String, Object>) entry.getValue();
        for (Map.Entry<String, Object> service : map.entrySet()) {
          Map<String, Object> map2 = (Map<String, Object>) service.getValue();
          logger.debug(service.getKey() + " : " + map2.get("currentVersionUrl"));
        }
      }
    }
  }
}
