package costanalysis;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import cloudos.Providers;
import cloudos.billings.AwsBillingReport;
import cloudos.billings.AwsBillingReportRepository;
import cloudos.billings.GoogleBillingReport;
import cloudos.billings.GoogleBillingReportRepository;
import cloudos.costanalysis.CostAnalysisTestUtils;
import cloudos.costanalysis.GenericBillingReportRepository;
import cloudos.models.costanalysis.GenericBillingReport;
import cloudos.models.costanalysis.GenericBillingReportQueryFilter;
import cloudos.models.costanalysis.GoogleBillingReconciliationEntry;
import cloudos.models.services.CloudosService;
import cloudos.models.services.ServiceCategory;
import cloudos.models.services.ServiceException;
import cloudos.models.services.ServiceRequest;
import cloudos.services.CloudosServiceRepository;
import cloudos.services.CloudosServiceService;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.time.DateUtils;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import test.CloudOSTest;

/**
 * Unit tests for {@link cloudos.costanalysis.GenericBillingReportRepository}.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Log4j2
public class GenericBillingReportRepositoryTest extends CloudOSTest {

  private static final String TEST_STRING = "DELETE-ME";
  private static final double COST = 1.2345d;
  private static final List<AwsBillingReport> AWS_REPORTS_TO_DELETE = new ArrayList<>();
  private static final List<GoogleBillingReport> GOOGLE_REPORTS_TO_DELETE = new ArrayList<>();
  private static final List<GenericBillingReport> GENERIC_REPORTS_TO_DELETE = new ArrayList<>();
  private static final String AWS_PAYER_ACCOUNT_ID = "AWS_PAYER_ACCOUNT_ID_JUNIT";
  private static final Date NOW = new Date();
  private static final Date START_DATE = DateUtils.addDays(NOW, -1);
  private static final String AWS_PRODUCT_CODE = "junit_PRODUCT_CODE_1";
  private static final int NUMBER_OF_BILLING_REPORTS = 1;

  @Autowired
  private GenericBillingReportRepository repository;

  @Autowired
  private AwsBillingReportRepository awsBillingReportRepository;

  @Autowired
  private GoogleBillingReportRepository googleBillingReportRepository;

  @Autowired
  private CostAnalysisTestUtils costAnalysisTestUtils;

  @Autowired
  private CloudosServiceService cloudosServiceService;

  @Autowired
  private CloudosServiceRepository cloudosServiceRepository;
  
  @Test
  public void test0ContextLoads() throws ServiceException {
    assertNotNull(repository);
    assertNotNull(awsBillingReportRepository);
    assertNotNull(googleBillingReportRepository);
    assertNotNull(costAnalysisTestUtils);
    ServiceRequest serviceRequest = ServiceRequest.builder().code("junit_PRODUCT_CODE_1").build();
    List<CloudosService> cloudosServices =
        cloudosServiceService.getServicesByFilter(serviceRequest);
    for (CloudosService cloudosService : cloudosServices) {
      cloudosServiceRepository.delete(cloudosService);
    }
  }

  @Test
  public void test1SaveAwsBillingReport() {

    // creates test data
    AwsBillingReport from = new AwsBillingReport();
    from.setPayerAccountId(AWS_PAYER_ACCOUNT_ID);
    from.setLineItemId(TEST_STRING);
    from.setUsageStartDate(START_DATE);
    from.setUsageEndDate(NOW);
    from.setUnblendedCost(COST);
    from.setProductCode(AWS_PRODUCT_CODE);

    // save fist the generic billing report
    GenericBillingReport genericBillingReport = repository.save(from);
    GENERIC_REPORTS_TO_DELETE.add(genericBillingReport);

    // save the specific billing report
    from = awsBillingReportRepository.save(from);
    AWS_REPORTS_TO_DELETE.add(from);

    assertNotNull(genericBillingReport);
    assertEquals("AMAZON_AWS_DELETEME", genericBillingReport.getId());
  }

  @Test
  public void test2SaveGoogleBillingReconciliationEntry() {
    // creates test data
    GoogleBillingReconciliationEntry entry =
        costAnalysisTestUtils.loadGoogleBillingReconciliationEntry();

    // actual test
    GenericBillingReport saved = repository.save(entry);
    assertNotNull(saved);
  }

  @Test
  public void test3FindByAccountIdExpectsAmazonAwsReportReturned() {
    List<GenericBillingReport> byAccountId = repository.findByAccountId(AWS_PAYER_ACCOUNT_ID);
    assertThat(byAccountId, Matchers.not(Matchers.emptyIterable()));
    assertSame(Providers.AMAZON_AWS, byAccountId.get(0).getProvider());
  }

  @Test
  public void test5FindByProductCodeInAndUsageStartTimeIsBetween() throws ServiceException {
    
    ServiceRequest service = ServiceRequest.builder().code("junit_PRODUCT_CODE_1").name("junit_PRODUCT_CODE_1")
        .providers(Providers.AMAZON_AWS).serviceCategory(ServiceCategory.STORAGE).build();
    cloudosServiceService.createService(service);
    
    List<CloudosService> cloudosServices =
        cloudosServiceService.findByIdProvidersAndIdServiceCategory(ServiceRequest.builder()
            .providers(Providers.AMAZON_AWS).serviceCategory(ServiceCategory.STORAGE).build());

    Set<String> productCodes = new HashSet<String>();
    for (CloudosService cloudosService : cloudosServices) {
      productCodes.add(cloudosService.getCode());
    }
    Date sixMonthsAgo = DateUtils.addMonths(NOW, -6);

    List<GenericBillingReport> result =
        repository.findByProductCodeInAndUsageStartTimeIsBetween(productCodes, sixMonthsAgo, NOW);

    assertNotNull(result);
  }

  @Test
  public void test7FindByFilterUsingOnlyDatesExpectsAtLeastMockedData() {

    // query
    GenericBillingReportQueryFilter filter = new GenericBillingReportQueryFilter();
    filter.setStartDateAfterOrEqualTo(START_DATE);
    filter.setStartDateBeforeOrEqualTo(NOW);

    // execution
    List<GenericBillingReport> result = repository.findByFilter(filter);

    // asserts
    Assert.assertNotNull(result);
    Assert.assertThat(result, Matchers.hasSize(greaterThanOrEqualTo(NUMBER_OF_BILLING_REPORTS)));
  }


  @Test
  public void test8FindByFilterUsingInvalidDatesExpectsEmpty() {

    // query
    GenericBillingReportQueryFilter filter = new GenericBillingReportQueryFilter();
    filter.setStartDateAfterOrEqualTo(NOW);
    filter.setStartDateBeforeOrEqualTo(START_DATE);
    filter.setAccountId(AWS_PAYER_ACCOUNT_ID);

    // execution
    List<GenericBillingReport> result = repository.findByFilter(filter);

    // asserts
    Assert.assertThat(result, Matchers.empty());
  }

  @Test
  public void test81findOneByWrongIdExpectsNull() {
    GenericBillingReport nullValue = repository.findOne("DOES_NOT_EXIST");
    Assert.assertSame(null, nullValue);
  }

  @Test
  public void test82findOneById() {
    GenericBillingReport result = repository.findOne(GENERIC_REPORTS_TO_DELETE.get(0).getId());
    Assert.assertEquals(GENERIC_REPORTS_TO_DELETE.get(0), result);
  }

  @Test
  public void test99DeleteAwsBillingReport() {

    boolean failure = false;

    // AWS
    try {
      awsBillingReportRepository.delete(AWS_REPORTS_TO_DELETE);
    } catch (Exception e) {
      log.error("Could not delete {} billing reports test data", Providers.AMAZON_AWS, e);
      failure = true;
    }

    // GOOGLE
    try {
      googleBillingReportRepository.delete(GOOGLE_REPORTS_TO_DELETE);
    } catch (Exception e) {
      log.error("Could not delete {} billing reports test data", Providers.GOOGLE_COMPUTE_ENGINE,
          e);
      failure = true;
    }

    // GENERIC
    try {
      repository.delete(GENERIC_REPORTS_TO_DELETE);
    } catch (Exception e) {
      log.error("Could not delete generic billing reports test data", e);
      failure = true;
    }

    assertFalse(failure);
  }

}
