package network;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;

import cloudos.Providers;
import cloudos.models.network.CloudosSubnet;
import cloudos.models.network.CloudosSubnetRequest;
import cloudos.models.network.CloudosVpc;
import cloudos.models.network.GoogleSubnet;
import cloudos.models.network.GoogleVpc;
import cloudos.network.GoogleSubnetManagement;
import cloudos.network.GoogleVpcManagement;
import test.CloudOSTest;

public class GoogleSubnetManagementTest extends CloudOSTest {

  private static final String CLOUDOS_VPC_TEST = "cloudos-vpc-test-1";
  private static final String SOUTHAMERICA_EAST_1 = "southamerica-east1";

  @Autowired
  GoogleVpcManagement googleVpcManagement;

  @Autowired
  GoogleSubnetManagement googleSubnetManagement;

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  static GoogleVpc vpc;

  @BeforeClass
  public static void setUp() {
    vpc = GoogleVpc.builder().region(SOUTHAMERICA_EAST_1).name(CLOUDOS_VPC_TEST).build();
  }

  @Test
  public void testListSubnet() {
    List<? extends CloudosSubnet> subnetworks = googleSubnetManagement.list(describeVpc());
    assertNotNull(subnetworks);
    assertThat(subnetworks.size(), greaterThan(0));
  }

  @Test
  @Ignore
  public void testCrudSubnet() throws Exception {
    CloudosVpc cloudosVpc = describeVpc();

    CloudosSubnetRequest cloudosSubnetRequest = CloudosSubnetRequest.builder()
                              .name("subnet-test-junit")
                              .cidr("10.126.0.0/20")
                              .vpc(cloudosVpc.getVpcId())
                              .provider(Providers.GOOGLE_COMPUTE_ENGINE)
                              .build();

    CloudosSubnet created = googleSubnetManagement.create(cloudosSubnetRequest, cloudosVpc);
    assertNotNull(created);

    GoogleSubnet subnet = describeSubnet(cloudosVpc, created);
    assertNotNull(subnet);

    googleSubnetManagement.delete(subnet, cloudosVpc.getRegion());

    subnet = describeSubnet(cloudosVpc, created);
    assertTrue(subnet == null);
  }

  private CloudosVpc describeVpc() {
    return googleVpcManagement.describe(vpc.getRegion(), vpc.getName());
  }

  private GoogleSubnet describeSubnet(CloudosVpc cloudosVpc, CloudosSubnet created) {
    CloudosSubnet subnet = googleSubnetManagement.describe(cloudosVpc.getRegion(),
        cloudosVpc.getVpcId(), created.getSubnetId());
    return (GoogleSubnet) subnet;
  }

}
