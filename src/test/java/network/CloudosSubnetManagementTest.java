package network;

import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import cloudos.Providers;
import cloudos.exceptions.NotImplementedException;
import cloudos.models.network.CloudosSubnet;
import cloudos.models.network.CloudosSubnetRequest;
import cloudos.models.network.CloudosVpc;
import cloudos.models.network.CloudosVpcRequest;
import cloudos.network.CloudosSubnetManagement;
import cloudos.network.CloudosVpcService;
import test.CloudOSTest;

/**
 * Add a new test for the CloudOS Subnet Management.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CloudosSubnetManagementTest extends CloudOSTest {

  @Autowired
  private CloudosSubnetManagement subnetManagement;

  @Autowired
  private CloudosVpcService cloudosVpcService;

  private static CloudosVpcRequest cloudosVpcRequest;

  private static CloudosVpc cloudosVpc;

  private static CloudosSubnet subnet;

  @BeforeClass
  public static void setUp() {

    cloudosVpcRequest = CloudosVpcRequest.builder()
        .provider(Providers.AMAZON_AWS)
        .region("us-west-1")
        .cidrBlock("10.0.1.0/24")
        .instanceTenancy("default")
        .name(String.format("cloudos-%s", generator.generate(6)
            .toLowerCase()))
        .ipv6CidrBlock(false)
        .tags(new ArrayList<>())
        .build();

  }


  @Test
  public void test1Create() throws Exception {
    cloudosVpc = this.cloudosVpcService.create(cloudosVpcRequest);
    assertNotNull(cloudosVpc);
    assertNotNull(cloudosVpc.getVpcId());

    CloudosSubnetRequest cloudosSubnetRequest = CloudosSubnetRequest.builder()
        .provider(Providers.AMAZON_AWS)
        .availabilityZone("us-west-1a")
        .vpc(cloudosVpc.getVpcId())
        .isIpv6(false)
        .isPrivateSubnet(false)
        .cidr("10.0.1.0/24")
        .build();

    subnet = this.subnetManagement.create(cloudosSubnetRequest, cloudosVpc);
    assertNotNull(subnet);
    assertNotNull(subnet.getSubnetId());

    assertEquals(subnet.getProvider(), Providers.AMAZON_AWS);
    assertEquals(subnet.getAvailabilityZone(), "us-west-1a");
    assertEquals(subnet.getName(), cloudosSubnetRequest.getName());
    assertEquals(subnet.getCidr(), cloudosSubnetRequest.getCidr());
  }

  @Test
  public void test2List() throws Exception {
    List<CloudosSubnet> subnets = this.subnetManagement.list(cloudosVpc);
    assertNotNull(subnets);
    assertThat(subnets.size(), greaterThan(0));
    subnets.forEach(subnet -> {
      assertNotNull(subnet.getSubnetId());
      assertEquals(subnet.getVpc(), cloudosVpc.getVpcId());
    });
  }

  @Test
  public void test3Describe() throws NotImplementedException {

    CloudosSubnet another = this.subnetManagement.describe(cloudosVpc.getProvider(),
        cloudosVpc.getRegion(), cloudosVpc.getVpcId(), subnet.getSubnetId());
    assertNotNull(another);
    assertEquals(subnet.getProvider(), another.getProvider());
    assertEquals(subnet.getAvailabilityZone(), another.getAvailabilityZone());
    assertEquals(subnet.getName(), another.getName());
    assertEquals(subnet.getCidr(), another.getCidr());
    assertEquals(subnet.getSubnetId(), another.getSubnetId());
  }

  @Test
  public void test5Delete() throws Exception {
    // attempt to delete the subnet
    this.subnetManagement.delete(subnet, "us-west-1");

    // delete the VPC altogether
    this.cloudosVpcService.delete(cloudosVpc);
  }

}
