package network;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import cloudos.Providers;
import cloudos.models.network.AmazonSubnet;
import cloudos.models.network.AwsVpc;
import cloudos.models.network.CloudosSubnet;
import cloudos.models.network.CloudosSubnetRequest;
import cloudos.models.network.CloudosVpc;
import cloudos.models.network.CloudosVpcRequest;
import cloudos.models.network.VpcUpdateRequest;
import cloudos.network.AmazonSubnetManagement;
import cloudos.network.AmazonVpcManagement;
import test.CloudOSTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Ignore
public class AmazonVpcManagementTest extends CloudOSTest {

  @Autowired
  AmazonVpcManagement amazonVpcManagement;

  @Autowired
  AmazonSubnetManagement amazonSubnetManagement;

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  static CloudosVpcRequest cloudosVpcRequest;

  static CloudosSubnetRequest cloudosSubnetRequest;

  static AwsVpc awsVpc;

  static AmazonSubnet subnet;

  @BeforeClass
  public static void setUp() {
    cloudosVpcRequest = CloudosVpcRequest.builder()
                            .region("us-east-1")
                            .cidrBlock("10.0.0.0/20")
                            .instanceTenancy("default")
                            .name("cloudos-vpc-test")
                            .provider(Providers.AMAZON_AWS)
                            .ipv6CidrBlock(false)
                            .tags(new ArrayList<>())
                            .build();

    String subnetName =
        String.format("cloudos%s", generator.generate(10));

    // create a cloudosSubnetRequest request
    cloudosSubnetRequest = CloudosSubnetRequest.builder()
                            .provider(Providers.AMAZON_AWS)
                            .cidr("10.0.0.0/28")
                            .availabilityZone("us-east-1d")
                            .isIpv6(false)
                            .isPrivateSubnet(false)
                            .name(subnetName)
                            .build();
  }

  @Test
  public void test1CreateVpc() {
    AwsVpc created = (AwsVpc) amazonVpcManagement.create(cloudosVpcRequest);
    assertNotNull(created);
    assertEquals(created.getCidrBlock(), cloudosVpcRequest.getCidrBlock());
    assertNull(created.getIsPrivateSubnet());
    assertEquals(Providers.AMAZON_AWS, created.getProvider());
    logger.debug(created);
    assertEquals("us-east-1", created.getRegion());

    awsVpc = created;
    cloudosSubnetRequest.setVpc(created.getVpcId());

    // create the subnet inside this vpc
    AmazonSubnet amazonSubnet = (AmazonSubnet) amazonSubnetManagement.create(cloudosSubnetRequest,
                                                                             created);
    assertNotNull(amazonSubnet);
    assertEquals(amazonSubnet.getCidr(), cloudosSubnetRequest.getCidr());
    assertEquals(Providers.AMAZON_AWS, amazonSubnet.getProvider());

    subnet = amazonSubnet;

    // attempt to create a VPC in a region that doesn't exist.
    thrown.expect(Exception.class);
    CloudosVpcRequest invalid = new CloudosVpcRequest();
    invalid.setRegion("us-south-3");
    invalid.setCidrBlock("10.0.0.0/28");
    created = (AwsVpc) amazonVpcManagement.create(invalid);
    // it should be null
    assertNull(created);
  }

  @Test
  public void test2ListVpc() {
    // default region
    List<? extends CloudosVpc> vpcList = this.amazonVpcManagement.list("us-east-1");
    assertNotNull(vpcList);
    assertThat(vpcList.size(), greaterThan(0));
    assertEquals(Providers.AMAZON_AWS, vpcList.get(0).getProvider());
    assertEquals("us-east-1", vpcList.get(0).getRegion());

    List<? extends CloudosSubnet> cloudosSubnets = this.amazonSubnetManagement.list(awsVpc);
    logger.debug(cloudosSubnets);
    assertNotNull(cloudosSubnets);
    assertThat(cloudosSubnets.size(), greaterThan(0));
    assertEquals(cloudosSubnets.get(0).getProvider(), Providers.AMAZON_AWS);

    // another region
    vpcList = this.amazonVpcManagement.list("us-west-1");
    assertNotNull(vpcList);
    assertThat(vpcList.size(), greaterThan(0));
    assertEquals(vpcList.get(0).getProvider(), Providers.AMAZON_AWS);
    assertEquals(vpcList.get(0).getRegion(), "us-west-1");

    // another region
    vpcList = this.amazonVpcManagement.list("eu-west-1");
    assertNotNull(vpcList);
    assertThat(vpcList.size(), greaterThan(0));
    assertEquals(vpcList.get(0).getProvider(), Providers.AMAZON_AWS);
    assertEquals(vpcList.get(0).getRegion(), "eu-west-1");
    // assertEquals(vpcList.get(0).getSubnets().size(), 3);
    logger.debug("######### AWS VPC #################");
    logger.debug(vpcList);
    logger.debug("###################################");

    // not for subnets
    cloudosSubnets = this.amazonSubnetManagement.list(awsVpc);
    logger.debug(cloudosSubnets);
    assertNotNull(cloudosSubnets);
    assertThat(cloudosSubnets.size(), greaterThan(0));
    assertEquals(cloudosSubnets.get(0).getProvider(), Providers.AMAZON_AWS);

    // no region
    thrown.expect(Exception.class);
    vpcList = this.amazonVpcManagement.list("us-south-6");
    assertNull(vpcList);
  }

  @Test
  public void test3DescribeVpc() {
    CloudosVpc cloudosVpc = this.amazonVpcManagement.describe(awsVpc.getRegion(), awsVpc.getVpcId());
    assertNotNull(cloudosVpc);
    assertEquals(cloudosVpc.getProvider(), Providers.AMAZON_AWS);
    assertEquals(cloudosVpc.getRegion(), awsVpc.getRegion());

    // include an invalid VPC
    AwsVpc invalidVpc = AwsVpc.builder().region("invalidregion").build();
    cloudosVpc = this.amazonVpcManagement.describe(invalidVpc.getRegion(), invalidVpc.getVpcId());
    // defaults to US-east-1
    assertNotNull(cloudosVpc);
  }

  @Test
  public void test4DecribeSubnet() {
    CloudosSubnet found = this.amazonSubnetManagement.describe(awsVpc.getRegion(), awsVpc.getVpcId(),
        subnet.getSubnetId());
    assertNotNull(found);
    assertEquals(found.getProvider(), Providers.AMAZON_AWS);
  }

  @Test
  public void test5UpdateVpc() {
    CloudosVpc cloudosVpc = this.amazonVpcManagement.describe(awsVpc.getRegion(), awsVpc.getVpcId());
    assertNotNull(cloudosVpc);
    assertNotNull(cloudosVpc.getVpcId());
    assertEquals(cloudosVpc.getProvider(), Providers.AMAZON_AWS);
    assertEquals(cloudosVpc.getRegion(), awsVpc.getRegion());

    VpcUpdateRequest request = VpcUpdateRequest.builder()
                                    .isIpv6Cidr(true)
                                    .region(cloudosVpc.getRegion())
                                    .provider(cloudosVpc.getProvider())
                                    .vpcId(cloudosVpc.getVpcId())
                                    .build();

    CloudosVpc cloudosVpc1 = this.amazonVpcManagement.update(request);
    assertNotNull(cloudosVpc1);
    assertEquals(cloudosVpc1.getVpcId(), cloudosVpc.getVpcId());
    assertEquals(cloudosVpc1.getName(), cloudosVpc.getName());
    assertEquals(cloudosVpc1.getProvider(), cloudosVpc.getProvider());
    assertEquals(cloudosVpc1.getRegion(), cloudosVpc.getRegion());

  }


  @Test
  public void test6DeleteSubnet() {
    // attempt to delete the subnet
    Assertions.assertThatCode(
        () -> amazonSubnetManagement.delete(subnet, "us-east-1")).doesNotThrowAnyException();
  }

  @Test
  public void test7DeleteVpc() {
    try {
      // attempt to delete the VPC
      Assertions.assertThatCode(
          () -> amazonVpcManagement.delete(awsVpc)).doesNotThrowAnyException();

      // now attempt to delete it again. Nothing should happen
      amazonVpcManagement.delete(awsVpc);
    } catch (Exception ex) {
      logger.error(ex.getMessage());
    }
  }

}
