package network;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;

import cloudos.Providers;
import cloudos.models.network.CloudosVpc;
import cloudos.models.network.CloudosVpcRequest;
import cloudos.models.network.GoogleVpc;
import cloudos.network.GoogleVpcManagement;
import test.CloudOSTest;

public class GoogleVpcManagementTest extends CloudOSTest {

  private static final String CLOUDOS_VPC_TEST = "cloudos-vpc-test";
  private static final String US_EAST_1 = "us-east1";

  @Autowired
  GoogleVpcManagement googleVpcManagement;

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  static CloudosVpcRequest cloudosVpcRequest;

  @BeforeClass
  public static void setUp() {
    cloudosVpcRequest = CloudosVpcRequest.builder()
                            .region(US_EAST_1)
                            .name(CLOUDOS_VPC_TEST)
                            .provider(Providers.GOOGLE_COMPUTE_ENGINE)
                            .tags(new ArrayList<>())
                            .build();
  }

  @Test
  public void testListVpc() {
    // default region
    List<? extends CloudosVpc> vpcList = googleVpcManagement.list(US_EAST_1);
    assertNotNull(vpcList);
    assertThat(vpcList.size(), greaterThan(0));
  }

  @Test
  @Ignore
  public void testVpcCrud() {
    CloudosVpc newVpc = googleVpcManagement.create(cloudosVpcRequest);
    assertNotNull(newVpc);
    assertEquals(newVpc.getName(), cloudosVpcRequest.getName());

    GoogleVpc googleVpc = GoogleVpc.builder().name(CLOUDOS_VPC_TEST).build();
    CloudosVpc describedVpc = googleVpcManagement.describe(googleVpc.getRegion(), googleVpc.getName());
    assertNotNull(describedVpc);

    googleVpcManagement.delete(googleVpc);

    describedVpc = googleVpcManagement.describe(googleVpc.getRegion(), googleVpc.getName());
    assertNull(describedVpc);
  }

}
