package network;

import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import cloudos.Providers;
import cloudos.models.network.AwsVpc;
import cloudos.models.network.CloudosSubnet;
import cloudos.models.network.CloudosSubnetRequest;
import cloudos.models.network.CloudosVpc;
import cloudos.models.network.CloudosVpcRequest;
import cloudos.network.AmazonSubnetManagement;
import cloudos.network.AmazonVpcManagement;
import cloudos.network.CloudosVpcService;
import test.CloudOSTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Ignore
public class AmazonSubnetManagementTest extends CloudOSTest {

  @Autowired
  AmazonSubnetManagement amazonSubnetManagement;

  @Autowired
  AmazonVpcManagement amazonVpcManagement;

  @Autowired
  CloudosVpcService cloudosVpcService;

  static CloudosVpcRequest cloudosVpcRequest;

  static CloudosSubnetRequest cloudosSubnetRequest;

  static CloudosSubnet subnet;

  static CloudosVpc cloudosVpc;

  @BeforeClass
  public static void setUp() {

    cloudosVpcRequest = CloudosVpcRequest.builder()
        .region("us-east-1")
        .cidrBlock("10.0.0.0/28")
        .instanceTenancy("default")
        .name("cloudos-vpc-test")
        .provider(Providers.AMAZON_AWS)
        .ipv6CidrBlock(false)
        .tags(new ArrayList<>())
        .build();

    String subnetName =
        String.format("cloudos%s", generator.generate(10));

    // create a cloudosSubnetRequest request
    cloudosSubnetRequest = CloudosSubnetRequest.builder()
        .provider(Providers.AMAZON_AWS)
        .cidr("10.0.0.0/28")
        .availabilityZone("us-east-1d")
        .isIpv6(false)
        .isPrivateSubnet(false)
        .name(subnetName)
        .build();

  }

  @Test
  @Ignore
  public void test1CreateSubnet() {
    cloudosVpc = this.amazonVpcManagement.create(cloudosVpcRequest);
    assertNotNull(cloudosVpc);
    assertNotNull(cloudosVpc.getVpcId());


    logger.debug("Submitting subnet request {}", cloudosSubnetRequest);
    subnet = this.amazonSubnetManagement.create(cloudosSubnetRequest, cloudosVpc);
    assertNotNull(subnet);
    assertNotNull(subnet.getSubnetId());
    assertEquals(subnet.getAvailabilityZone(), "us-east-1d");
    assertEquals(subnet.getCidr(), "10.0.0.0/28");
    assertEquals(subnet.getProvider(), Providers.AMAZON_AWS);

  }

  @Test
  public void test2ListSubnet() {
    List<CloudosSubnet> subnets = (List<CloudosSubnet>) this.amazonSubnetManagement.list(cloudosVpc);
    assertNotNull(subnets);
    assertThat(subnets.size(), greaterThan(0));

    subnets.forEach(sub -> {
      assertEquals(sub.getVpc(), cloudosVpc.getVpcId());
      assertEquals(sub.getProvider(), cloudosVpc.getProvider());
    });
  }


  @Test
  public void test3DescribeSubnet() {
  }

  @Test
  public void test4DeleteSubnet() {
    // delete the VPC at the end
    this.amazonVpcManagement.delete((AwsVpc) cloudosVpc);
  }

}
