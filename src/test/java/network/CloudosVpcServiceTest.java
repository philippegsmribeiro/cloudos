package network;

import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import com.amazonaws.services.ec2.model.AmazonEC2Exception;

import cloudos.Providers;
import cloudos.models.network.CloudosVpc;
import cloudos.models.network.CloudosVpcRequest;
import cloudos.network.CloudosVpcService;
import test.CloudOSTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Ignore
public class CloudosVpcServiceTest extends CloudOSTest {

  @Autowired
  CloudosVpcService cloudosVpcService;

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  static CloudosVpcRequest cloudosVpcRequest;

  @BeforeClass
  public static void setUp() {
    cloudosVpcRequest = CloudosVpcRequest.builder()
                                         .provider(Providers.AMAZON_AWS)
                                         .region("us-west-1")
                                         .cidrBlock("10.0.0.0/28")
                                         .instanceTenancy("default")
                                         .name(String.format("cloudos-%s", generator.generate(6)
                                             .toLowerCase()))
                                         .ipv6CidrBlock(false)
                                         .tags(new ArrayList<>())
                                         .build();
  }

  @Test
  public void test1Create() throws Exception {
    CloudosVpc cloudosVpc = this.cloudosVpcService.create(cloudosVpcRequest);
    assertNotNull(cloudosVpc);
    assertNotNull(cloudosVpc.getVpcId());
    assertEquals(cloudosVpc.getName(), cloudosVpcRequest.getName());
    assertEquals(cloudosVpc.getProvider(), cloudosVpcRequest.getProvider());
    assertEquals(cloudosVpc.getRegion(), cloudosVpcRequest.getRegion());

    // remove the VPC at the end of the test
    this.cloudosVpcService.delete(cloudosVpc);
  }

  @Test
  public void test2List() throws Exception {
    List<CloudosVpc> vpcList = this.cloudosVpcService.list(Providers.AMAZON_AWS, "us-west-1");
    assertNotNull(vpcList);
    assertThat(vpcList.size(), greaterThan(0));

    vpcList.forEach(cloudosVpc -> assertNotNull(cloudosVpc.getVpcId()));

    vpcList = this.cloudosVpcService.list(Providers.AMAZON_AWS, "eu-west-1");
    assertNotNull(vpcList);
    assertThat(vpcList.size(), greaterThan(0));
    assertEquals(3, vpcList.get(0).getSubnets().size());
  }

  @Test(expected = AmazonEC2Exception.class)
  public void test4Describe() throws Exception {
    List<CloudosVpc> vpcList = this.cloudosVpcService.list(Providers.AMAZON_AWS, "us-west-1");
    assertNotNull(vpcList);
    assertThat(vpcList.size(), greaterThan(0));

    // obtain one from the list
    CloudosVpc cloudosVpc = vpcList.get(0);
    CloudosVpc another = this.cloudosVpcService.describe(cloudosVpc.getProvider(),
        cloudosVpc.getRegion(), cloudosVpc.getVpcId());
    assertNotNull(another);
    assertEquals(cloudosVpc.getVpcId(), another.getVpcId());
    assertEquals(cloudosVpc.getRegion(), another.getRegion());
    assertEquals(cloudosVpc.getProvider(), another.getProvider());

    // use an invalid VPC
    CloudosVpc invalid = this.cloudosVpcService.describe(cloudosVpc.getProvider(),
        cloudosVpc.getRegion(), "invalidvpc");
    assertNull(invalid);
  }
}
