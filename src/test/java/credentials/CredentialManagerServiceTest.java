package credentials;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import cloudos.CredentialManagerService;
import cloudos.Providers;
import cloudos.credentials.CloudCredentialActionException;
import cloudos.credentials.CloudCredentialActionRequest;
import cloudos.encryptionKeys.AmazonKMSService;
import cloudos.encryptionKeys.AmazonKMSServiceException;
import cloudos.exceptions.NotImplementedException;
import cloudos.google.GoogleCredential;
import cloudos.models.AwsCredential;
import cloudos.models.AzureCredential;
import cloudos.models.CloudosCredential;
import cloudos.models.CloudosCredentialRepository;
import cloudos.utils.FileUtils;
import test.CloudOSTest;

/** Created by gleimar on 20/05/2017. */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CredentialManagerServiceTest extends CloudOSTest {

  @Value("${google.credentials.project}")
  private String googleProject;

  @Value("${google.credentials.json}")
  private String json;

  @Value("${aws_access_key_id}")
  private String awsAccessKey;

  @Value("${aws_secret_key}")
  private String awsSecretKey;

  @Value("${azure.client}")
  private String azureClient;

  @Value("${azure.tenant}")
  private String azureTenant;

  @Value("${azure.key}")
  private String azureKey;

  @Autowired
  private CredentialManagerService credentialManagerService;

  @Autowired
  private CloudosCredentialRepository cloudosCredentialRepository;

  @Autowired
  private AmazonKMSService amazonKMSService;

  @Test(expected = CloudCredentialActionException.class)
  public void testSaveBadCredentialAWS()
      throws NotImplementedException, CloudCredentialActionException {
    AwsCredential awsCredentials =
        new AwsCredential("AKIAIX6U3MSCSVOOXHHA", "WcKyasdasfnQRBCcb8j4lPJ40T9+UCOk3YuLx/b7T3S+");
    CloudCredentialActionRequest cloudCreateCredentialRequest = new CloudCredentialActionRequest();
    cloudCreateCredentialRequest.setCloudosCredential(awsCredentials);
    CloudosCredential cloudosCredential =
        this.credentialManagerService.save(cloudCreateCredentialRequest);
    assert cloudosCredential == null;
  }

  @Test(expected = CloudCredentialActionException.class)
  public void testSaveBadCredentialGoogle()
      throws NotImplementedException, CloudCredentialActionException {
    GoogleCredential googleCredentials = new GoogleCredential("AKIAIX6U3MSCSVOOXHHA",
        "WcKyasdasfnQRBCcb8j4lPJ40T9+UCOk3YuLx/b7T3S+");
    CloudCredentialActionRequest cloudCreateCredentialRequest = new CloudCredentialActionRequest();
    cloudCreateCredentialRequest.setCloudosCredential(googleCredentials);
    CloudosCredential cloudosCredential =
        this.credentialManagerService.save(cloudCreateCredentialRequest);
    assert cloudosCredential == null;
  }

  @Test
  public void testSave() throws NotImplementedException, CloudCredentialActionException {
    AwsCredential awsCredentials = new AwsCredential(awsAccessKey, awsSecretKey);
    CloudCredentialActionRequest cloudCreateCredentialRequest = new CloudCredentialActionRequest();
    cloudCreateCredentialRequest.setCloudosCredential(awsCredentials);
    CloudosCredential cloudosCredential =
        this.credentialManagerService.save(cloudCreateCredentialRequest);
    logger.debug(String.format("CloudosCredential id: %s", cloudosCredential.getId()));
    assert cloudosCredential != null;
    assert StringUtils.isNotBlank(cloudosCredential.getId());
  }

  @Test
  public void testDelete()
      throws NotImplementedException, CloudCredentialActionException, AmazonKMSServiceException {
    List<CloudosCredential> list = this.credentialManagerService.list(Providers.AMAZON_AWS);
    CloudCredentialActionRequest cloudCreateCredentialRequest = new CloudCredentialActionRequest();
    cloudCreateCredentialRequest.setCloudosCredential(list.get(0));
    this.credentialManagerService.delete(cloudCreateCredentialRequest);
    list = this.credentialManagerService.list(Providers.AMAZON_AWS);
    assert CollectionUtils.isEmpty(list);
  }

  @Test
  public void testDefaultCredentials() throws Exception {
    this.credentialManagerService.setDefaultCredentials();
  }

  @After
  public void after() throws Exception {
    this.credentialManagerService.setDefaultCredentials();
  }

  @Test
  public void testXEncryptDecryptAwsCredential() throws Exception {

    this.cloudosCredentialRepository.deleteAll();

    AwsCredential awsCredentials = new AwsCredential(awsAccessKey, awsSecretKey);
    CloudCredentialActionRequest cloudCreateCredentialRequest = new CloudCredentialActionRequest();
    cloudCreateCredentialRequest.setCloudosCredential(awsCredentials);
    CloudosCredential cloudosCredential =
        this.credentialManagerService.save(cloudCreateCredentialRequest);
    assert cloudosCredential != null;
    assert StringUtils.isNotBlank(cloudosCredential.getId());

    cloudosCredential = cloudosCredentialRepository.findById(cloudosCredential.getId());
    assert cloudosCredential != null;
    assert StringUtils.isNotBlank(cloudosCredential.getId());

    awsCredentials = (AwsCredential) cloudosCredential;

    assert awsCredentials.getAccessKeyId().equals(amazonKMSService
        .encryptMessage(amazonKMSService.getCredentialEncryptedDataKey(), awsAccessKey));
    assert awsCredentials.getSecretAccessKey().equals(amazonKMSService
        .encryptMessage(amazonKMSService.getCredentialEncryptedDataKey(), awsSecretKey));

    assert !awsCredentials.getSecretAccessKey().equals(awsAccessKey);
    assert !awsCredentials.getSecretAccessKey().equals(awsSecretKey);

    assert awsAccessKey.equals(amazonKMSService.decryptMessage(
        amazonKMSService.getCredentialEncryptedDataKey(), awsCredentials.getAccessKeyId()));
    assert awsSecretKey.equals(amazonKMSService.decryptMessage(
        amazonKMSService.getCredentialEncryptedDataKey(), awsCredentials.getSecretAccessKey()));

    this.cloudosCredentialRepository.deleteAll();

  }

  @Test
  public void testXEncryptDecryptAzureCredential() throws Exception {

    this.cloudosCredentialRepository.deleteAll();

    AzureCredential credential = new AzureCredential(azureClient, azureTenant, azureKey);

    CloudCredentialActionRequest cloudCreateCredentialRequest = new CloudCredentialActionRequest();
    cloudCreateCredentialRequest.setCloudosCredential(credential);
    CloudosCredential cloudosCredential =
        this.credentialManagerService.save(cloudCreateCredentialRequest);
    assert cloudosCredential != null;
    assert StringUtils.isNotBlank(cloudosCredential.getId());
    assert amazonKMSService.getCredentialEncryptedDataKey() != null;

    cloudosCredential = cloudosCredentialRepository.findById(cloudosCredential.getId());
    assert cloudosCredential != null;
    assert StringUtils.isNotBlank(cloudosCredential.getId());
    assert amazonKMSService.getCredentialEncryptedDataKey() != null;

    credential = (AzureCredential) cloudosCredential;
    assert credential.getClient().equals(amazonKMSService
        .encryptMessage(amazonKMSService.getCredentialEncryptedDataKey(), azureClient));
    assert credential.getKey().equals(amazonKMSService
        .encryptMessage(amazonKMSService.getCredentialEncryptedDataKey(), azureKey));

    assert !credential.getClient().equals(azureClient);
    assert !credential.getKey().equals(azureKey);

    assert azureClient.equals(amazonKMSService
        .decryptMessage(amazonKMSService.getCredentialEncryptedDataKey(), credential.getClient()));
    assert azureKey.equals(amazonKMSService
        .decryptMessage(amazonKMSService.getCredentialEncryptedDataKey(), credential.getKey()));

    this.cloudosCredentialRepository.deleteAll();

  }

  @Test
  public void testXEncryptDecryptGoogleCredential() throws Exception {

    this.cloudosCredentialRepository.deleteAll();

    byte[] readAllBytes = Files.readAllBytes(Paths
        .get(FileUtils.getFileFromResourcesFile(json, this.getClass().getClassLoader()).toURI()));
    String jsonContent = new String(readAllBytes);

    GoogleCredential awsCredentials = new GoogleCredential(googleProject, jsonContent);
    CloudCredentialActionRequest cloudCreateCredentialRequest = new CloudCredentialActionRequest();
    cloudCreateCredentialRequest.setCloudosCredential(awsCredentials);
    CloudosCredential cloudosCredential =
        this.credentialManagerService.save(cloudCreateCredentialRequest);
    assert cloudosCredential != null;
    assert StringUtils.isNotBlank(cloudosCredential.getId());

    cloudosCredential = cloudosCredentialRepository.findById(cloudosCredential.getId());
    assert cloudosCredential != null;
    assert StringUtils.isNotBlank(cloudosCredential.getId());

    awsCredentials = (GoogleCredential) cloudosCredential;

    assert awsCredentials.getJsonCredential().equals(amazonKMSService
        .encryptMessage(amazonKMSService.getCredentialEncryptedDataKey(), jsonContent));

    assert !awsCredentials.getJsonCredential().equals(jsonContent);

    assert jsonContent.equals(amazonKMSService.decryptMessage(
        amazonKMSService.getCredentialEncryptedDataKey(), awsCredentials.getJsonCredential()));

    this.cloudosCredentialRepository.deleteAll();

  }

  @Test
  public void testZhasCredentials() throws Exception {

    this.cloudosCredentialRepository.deleteAll();
    assert credentialManagerService.hasCredentials() == false;

    AwsCredential awsCredentials = new AwsCredential(awsAccessKey, awsSecretKey);
    CloudCredentialActionRequest cloudCreateCredentialRequest = new CloudCredentialActionRequest();
    cloudCreateCredentialRequest.setCloudosCredential(awsCredentials);
    credentialManagerService.save(cloudCreateCredentialRequest);

    assert credentialManagerService.hasCredentials() == true;
    
    this.cloudosCredentialRepository.deleteAll();
    assert credentialManagerService.hasCredentials() == false;
  }


}
