package credentials;

import cloudos.CredentialController;
import cloudos.CredentialManagerService;
import cloudos.Providers;
import cloudos.credentials.CloudCredentialActionRequest;
import cloudos.google.GoogleCredential;
import cloudos.models.AwsCredential;
import cloudos.models.CloudosCredential;
import cloudos.storage.StorageService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import test.CloudOSTest;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by philipperibeiro on 12/16/16.
 */
public class CredentialControllerTest extends CloudOSTest {

    @Value("${aws_access_key_id}")
    private String awsAccessKey;

    @Value("${aws_secret_key}")
    private String awsSecretKey;

    @Value("${storage.file_upload_dir}")
    private String uploadDir;

    private MockMvc mvc;

    @Autowired
    private CredentialController credentialController;

    @Autowired
    private CredentialManagerService credentialManagerService;

    @Autowired
    private StorageService storageService;

    @Autowired
    private ObjectMapper objectMapper;

    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.standaloneSetup(credentialController).build();
    }

    @Test
    public void testCreateCloudCredentialsAWS() throws Exception {
        // the URL must exist but not allow for get requests
        this.mvc.perform(get(CredentialController.CREDENTIAL + "/save"))
                .andExpect(status().is4xxClientError());


        /*CloudCredentialActionRequest cloudCreateCredentialRequest = new CloudCredentialActionRequest();
        cloudCreateCredentialRequest.setCloudosCredential(awsCredentials);
        String payload = mapper.writeValueAsString(cloudCreateCredentialRequest);*/

        //Using string because the credentials is ignored in GET
        String payload = String.format("{ \"cloudosCredential\" : {  \"accessKeyId\": \"%s\", \"secretAccessKey\": \"%s\", \"provider\": \"AMAZON_AWS\" } }", awsAccessKey, awsSecretKey);

        ResultActions perform = this.mvc.perform(
                post(CredentialController.CREDENTIAL + "/save").content(payload).contentType(contentType));
        perform.andDo(h -> logger.debug(h.getResponse().getContentAsString()));
        perform.andExpect(status().is2xxSuccessful());

        // send a bad request
        /*awsCredentials = new AwsCredential();
        cloudCreateCredentialRequest.setCloudosCredential(awsCredentials);*/
        String badPayload = "{ \"cloudosCredential\" : { }}";
        this.mvc.perform(
                post(CredentialController.CREDENTIAL + "/save")
                        .content(badPayload)
                        .contentType(contentType))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testCreateCloudCredentials() throws Exception {

        // the URL must exist but not allow for get requests
        this.mvc.perform(get(CredentialController.CREDENTIAL + "/save"))
                .andExpect(status().is4xxClientError());


        // gcloud
        /*GoogleCredential googleCredential = new GoogleCredential("oval-campaign-146701",
                "{ \"type\": \"service_account\", \"project_id\": \"oval-campaign-146701\", \"private_key_id\": \"d524116cc4c5a9f8c301a47c210ae01b6f3373af\", \"private_key\": \"-----BEGIN PRIVATE KEY-----\\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC6/tKGjlZlSLHF\\n8LckxZjJDcCihBcmyyfquTFwN8bJt/HI01IAg0T6NeOZ4A/V/TAOLLcylLKp9+Jh\\nZQoZCIaNsitemNjybiqvEptqHKLKAv39MQ2sfewj9ZzfK3Y6KwLCTE/Otj2VGmCg\\n5rGKXjeB5EduG6UQ8GfYba4zQjlwhN+8Gg7UPmc2VecvhnVvYbVBBuihi/Vw6j4H\\ni4av5iwBVioZSK6cttggo8MiwdhK7XIP3ZKvG3N9XRtPXT8F4Yb9AGw+0nDswB92\\nMOKbNZPuWA9bXmxMwGDujIrt1bkwpbW+fdDF4G6Y0lG4elNnRopI1y5XSpEqkwTY\\nk5xyHSaLAgMBAAECggEBAKFEcruotcnTz70txETqRMHgImoGERFb06kS3hgxgjcl\\ndFgGJ4pZq3s+TnO+UOnz2SXgWn48z3jWV143jWXZfHFnXw/OMguiBx3FplL1L0+Z\\nFQLu8yP31VeN0ySEjwnLXi3DMnDLqoCUpxEc47C5YJWdEh1bqhfZvTViIgIC8xad\\nhykavnZsCVkaoKuInol0tCkr6zGC1FF3Hxm0nMQV5YV+9wXroVhKCVXuOYKLdcFU\\ndBLhJvy4xehO6H7mt/yZOYqUGCUxuwIJlJqWyNyuOkpGfa/Prgmz+lkdzS8cwebA\\nYuDe5dECVY6S7bas+QfD9RNg0UQg6vf2Xl2pIgBcxkECgYEA6pTRvE48TtP47bk5\\nMb8Tty3llrk29ewcojzE32389ix68Kv/7WqIJgT9DtUoWP+prhBSbMFbJ92mkGZb\\nUneebNSmxSeZFuaTnq8s3+TW65HQ8SIMpRklceRZ8aszeSlc1/AOinCDHeHQBk+R\\ns8BmcOuo3lOr4fT+PkuRGuFPMk0CgYEAzBG2zgHGhqkA226BZartXViXm3nlld/Y\\nlc9Al0ZR0DaZEzaOsQR5jL/6jfaKbBFZ6ryFfK9RxFFQpXGweQ9TWAeawSyO1DpC\\nSVGr6GPd62TSc2I9muFFLviyC3Nca7ZAvg8R3fDiYGaHZwN5kJfHb/fzMTpu95EI\\nOk2jGdq6uDcCgYEAjX8E3+vlDrtnnKUsLaiPEOAba4X+8+ne/7FeI1Np0WfVJFTr\\ng97NjvlVV1wWAVD9naP0w+sBvBNDxDgpf53trsG6cpPHEPw2MPKDQ+36AZAsEzcx\\nn2iCoKdX5aEZ5Eyh1xLTAoiJXj32R8g5H86/O/+6FZksxZgMv7C/LxKWvcUCgYAU\\nRIogacK66xwfrxkA7PoINF1SXsfzm7jaICxtGdnUUSfg5aFEaYpc9VBAO0VCOHLy\\nwmiLmqoHUoAs/l6ll2USsCapmvBQmbytpYPqX8jRsUDJnu85a7hAUwjz5DBArmg9\\nuCdxIJSLOIOoMosnVjvDnh+0OvZ8dhYYHE/fqH60/wKBgFr1XKpeWObv/446sQDE\\n+YKrKu6dCye12PgZVXyBpnlOo3YhQiQi/V5/fOh63yK8lyPzXzpxWz5pvmsykYAx\\n6y2ofChcatvZiuZNHgWFOLKLno1nN6KMWxotbZoJ1Q/iWfH9LJtqJzjuENCPxRg7\\n4C1Zg47994ocsY4DJ2x3QEDV\\n-----END PRIVATE KEY-----\\n\", \"client_email\": \"oval-campaign-146701@appspot.gserviceaccount.com\", \"client_id\": \"104834931195951607982\", \"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\", \"token_uri\": \"https://accounts.google.com/o/oauth2/token\", \"auth_provider_x509_cert_url\": \"https://www.googleapis.com/oauth2/v1/certs\", \"client_x509_cert_url\": \"https://www.googleapis.com/robot/v1/metadata/x509/oval-campaign-146701%40appspot.gserviceaccount.com\"}");
        CloudCredentialActionRequest cloudCreateCredentialRequest = new CloudCredentialActionRequest();
        cloudCreateCredentialRequest.setCloudosCredential(googleCredential);
        String payload = mapper.writeValueAsString(cloudCreateCredentialRequest);*/

        //Using string because the credentials is ignored in GET
        String payload = "{\"cloudosCredential\":{\"provider\":\"GOOGLE_COMPUTE_ENGINE\",\"id\":null,\"provider\":\"GOOGLE_COMPUTE_ENGINE\",\"decrypted\":true,\"creation\":\"2018-04-17 01:01:32\",\"project\":\"oval-campaign-146701\",\"jsonCredential\":\"{ \\\"type\\\": \\\"service_account\\\", \\\"project_id\\\": \\\"oval-campaign-146701\\\", \\\"private_key_id\\\": \\\"d524116cc4c5a9f8c301a47c210ae01b6f3373af\\\", \\\"private_key\\\": \\\"-----BEGIN PRIVATE KEY-----\\\\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC6/tKGjlZlSLHF\\\\n8LckxZjJDcCihBcmyyfquTFwN8bJt/HI01IAg0T6NeOZ4A/V/TAOLLcylLKp9+Jh\\\\nZQoZCIaNsitemNjybiqvEptqHKLKAv39MQ2sfewj9ZzfK3Y6KwLCTE/Otj2VGmCg\\\\n5rGKXjeB5EduG6UQ8GfYba4zQjlwhN+8Gg7UPmc2VecvhnVvYbVBBuihi/Vw6j4H\\\\ni4av5iwBVioZSK6cttggo8MiwdhK7XIP3ZKvG3N9XRtPXT8F4Yb9AGw+0nDswB92\\\\nMOKbNZPuWA9bXmxMwGDujIrt1bkwpbW+fdDF4G6Y0lG4elNnRopI1y5XSpEqkwTY\\\\nk5xyHSaLAgMBAAECggEBAKFEcruotcnTz70txETqRMHgImoGERFb06kS3hgxgjcl\\\\ndFgGJ4pZq3s+TnO+UOnz2SXgWn48z3jWV143jWXZfHFnXw/OMguiBx3FplL1L0+Z\\\\nFQLu8yP31VeN0ySEjwnLXi3DMnDLqoCUpxEc47C5YJWdEh1bqhfZvTViIgIC8xad\\\\nhykavnZsCVkaoKuInol0tCkr6zGC1FF3Hxm0nMQV5YV+9wXroVhKCVXuOYKLdcFU\\\\ndBLhJvy4xehO6H7mt/yZOYqUGCUxuwIJlJqWyNyuOkpGfa/Prgmz+lkdzS8cwebA\\\\nYuDe5dECVY6S7bas+QfD9RNg0UQg6vf2Xl2pIgBcxkECgYEA6pTRvE48TtP47bk5\\\\nMb8Tty3llrk29ewcojzE32389ix68Kv/7WqIJgT9DtUoWP+prhBSbMFbJ92mkGZb\\\\nUneebNSmxSeZFuaTnq8s3+TW65HQ8SIMpRklceRZ8aszeSlc1/AOinCDHeHQBk+R\\\\ns8BmcOuo3lOr4fT+PkuRGuFPMk0CgYEAzBG2zgHGhqkA226BZartXViXm3nlld/Y\\\\nlc9Al0ZR0DaZEzaOsQR5jL/6jfaKbBFZ6ryFfK9RxFFQpXGweQ9TWAeawSyO1DpC\\\\nSVGr6GPd62TSc2I9muFFLviyC3Nca7ZAvg8R3fDiYGaHZwN5kJfHb/fzMTpu95EI\\\\nOk2jGdq6uDcCgYEAjX8E3+vlDrtnnKUsLaiPEOAba4X+8+ne/7FeI1Np0WfVJFTr\\\\ng97NjvlVV1wWAVD9naP0w+sBvBNDxDgpf53trsG6cpPHEPw2MPKDQ+36AZAsEzcx\\\\nn2iCoKdX5aEZ5Eyh1xLTAoiJXj32R8g5H86/O/+6FZksxZgMv7C/LxKWvcUCgYAU\\\\nRIogacK66xwfrxkA7PoINF1SXsfzm7jaICxtGdnUUSfg5aFEaYpc9VBAO0VCOHLy\\\\nwmiLmqoHUoAs/l6ll2USsCapmvBQmbytpYPqX8jRsUDJnu85a7hAUwjz5DBArmg9\\\\nuCdxIJSLOIOoMosnVjvDnh+0OvZ8dhYYHE/fqH60/wKBgFr1XKpeWObv/446sQDE\\\\n+YKrKu6dCye12PgZVXyBpnlOo3YhQiQi/V5/fOh63yK8lyPzXzpxWz5pvmsykYAx\\\\n6y2ofChcatvZiuZNHgWFOLKLno1nN6KMWxotbZoJ1Q/iWfH9LJtqJzjuENCPxRg7\\\\n4C1Zg47994ocsY4DJ2x3QEDV\\\\n-----END PRIVATE KEY-----\\\\n\\\", \\\"client_email\\\": \\\"oval-campaign-146701@appspot.gserviceaccount.com\\\", \\\"client_id\\\": \\\"104834931195951607982\\\", \\\"auth_uri\\\": \\\"https://accounts.google.com/o/oauth2/auth\\\", \\\"token_uri\\\": \\\"https://accounts.google.com/o/oauth2/token\\\", \\\"auth_provider_x509_cert_url\\\": \\\"https://www.googleapis.com/oauth2/v1/certs\\\", \\\"client_x509_cert_url\\\": \\\"https://www.googleapis.com/robot/v1/metadata/x509/oval-campaign-146701%40appspot.gserviceaccount.com\\\"}\",\"configFilePath\":null}}";

        ResultActions perform = this.mvc.perform(
                post(CredentialController.CREDENTIAL + "/save").content(payload).contentType(contentType));
        perform.andDo(h -> logger.debug(h.getResponse().getContentAsString()));
        perform.andExpect(status().is2xxSuccessful());
    }

    @Test
    public void testListCloudCredentials() throws Exception {
        // the URL must exist but not allow for get requests
        this.mvc.perform(get(CredentialController.CREDENTIAL + "/list"))
                .andExpect(status().is4xxClientError());
        String payload = gson.toJson(Providers.AMAZON_AWS);
        ResultActions perform = this.mvc.perform(
                post(CredentialController.CREDENTIAL + "/list").content(payload).contentType(contentType));
        perform.andDo(h -> logger.debug(h.getResponse().getContentAsString()));
        MvcResult result = perform.andExpect(status().is2xxSuccessful()).andReturn();

        //Ensure the sensitive data is not exposed
        AwsCredential[] awsCredentialList = this.objectMapper.readValue(result.getResponse().getContentAsString(), AwsCredential[].class);
        Assert.assertNotNull(awsCredentialList);
        Arrays.stream(awsCredentialList).forEach(awsCredential -> Assert.assertNull(awsCredential.getSecretAccessKey()));
    }

    @Test
    public void testDeletingCloudCredentials() throws Exception {
        // the URL must exist but not allow for get requests
        this.mvc.perform(get(CredentialController.CREDENTIAL + "/delete"))
                .andExpect(status().is4xxClientError());

        List<CloudosCredential> list = credentialManagerService.list(Providers.GOOGLE_COMPUTE_ENGINE);
        CloudCredentialActionRequest cloudCreateCredentialRequest = new CloudCredentialActionRequest();
        cloudCreateCredentialRequest.setCloudosCredential(list.get(0));
        String payload = mapper.writeValueAsString(cloudCreateCredentialRequest);
        ResultActions perform = this.mvc.perform(post(CredentialController.CREDENTIAL + "/delete")
                .content(payload).contentType(contentType));
        perform.andDo(h -> logger.debug(h.getResponse().getContentAsString()));
        perform.andExpect(status().is2xxSuccessful());
    }

    @Test
    public void testCreateGoogleCredentialsUploadingConfigFile() throws Exception {

        Files.createDirectories(Paths.get(uploadDir));

        String jsonConfig = "{ \"type\": \"service_account\", \"project_id\": \"oval-campaign-146701\", \"private_key_id\": \"d524116cc4c5a9f8c301a47c210ae01b6f3373af\", \"private_key\": \"-----BEGIN PRIVATE KEY-----\\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC6/tKGjlZlSLHF\\n8LckxZjJDcCihBcmyyfquTFwN8bJt/HI01IAg0T6NeOZ4A/V/TAOLLcylLKp9+Jh\\nZQoZCIaNsitemNjybiqvEptqHKLKAv39MQ2sfewj9ZzfK3Y6KwLCTE/Otj2VGmCg\\n5rGKXjeB5EduG6UQ8GfYba4zQjlwhN+8Gg7UPmc2VecvhnVvYbVBBuihi/Vw6j4H\\ni4av5iwBVioZSK6cttggo8MiwdhK7XIP3ZKvG3N9XRtPXT8F4Yb9AGw+0nDswB92\\nMOKbNZPuWA9bXmxMwGDujIrt1bkwpbW+fdDF4G6Y0lG4elNnRopI1y5XSpEqkwTY\\nk5xyHSaLAgMBAAECggEBAKFEcruotcnTz70txETqRMHgImoGERFb06kS3hgxgjcl\\ndFgGJ4pZq3s+TnO+UOnz2SXgWn48z3jWV143jWXZfHFnXw/OMguiBx3FplL1L0+Z\\nFQLu8yP31VeN0ySEjwnLXi3DMnDLqoCUpxEc47C5YJWdEh1bqhfZvTViIgIC8xad\\nhykavnZsCVkaoKuInol0tCkr6zGC1FF3Hxm0nMQV5YV+9wXroVhKCVXuOYKLdcFU\\ndBLhJvy4xehO6H7mt/yZOYqUGCUxuwIJlJqWyNyuOkpGfa/Prgmz+lkdzS8cwebA\\nYuDe5dECVY6S7bas+QfD9RNg0UQg6vf2Xl2pIgBcxkECgYEA6pTRvE48TtP47bk5\\nMb8Tty3llrk29ewcojzE32389ix68Kv/7WqIJgT9DtUoWP+prhBSbMFbJ92mkGZb\\nUneebNSmxSeZFuaTnq8s3+TW65HQ8SIMpRklceRZ8aszeSlc1/AOinCDHeHQBk+R\\ns8BmcOuo3lOr4fT+PkuRGuFPMk0CgYEAzBG2zgHGhqkA226BZartXViXm3nlld/Y\\nlc9Al0ZR0DaZEzaOsQR5jL/6jfaKbBFZ6ryFfK9RxFFQpXGweQ9TWAeawSyO1DpC\\nSVGr6GPd62TSc2I9muFFLviyC3Nca7ZAvg8R3fDiYGaHZwN5kJfHb/fzMTpu95EI\\nOk2jGdq6uDcCgYEAjX8E3+vlDrtnnKUsLaiPEOAba4X+8+ne/7FeI1Np0WfVJFTr\\ng97NjvlVV1wWAVD9naP0w+sBvBNDxDgpf53trsG6cpPHEPw2MPKDQ+36AZAsEzcx\\nn2iCoKdX5aEZ5Eyh1xLTAoiJXj32R8g5H86/O/+6FZksxZgMv7C/LxKWvcUCgYAU\\nRIogacK66xwfrxkA7PoINF1SXsfzm7jaICxtGdnUUSfg5aFEaYpc9VBAO0VCOHLy\\nwmiLmqoHUoAs/l6ll2USsCapmvBQmbytpYPqX8jRsUDJnu85a7hAUwjz5DBArmg9\\nuCdxIJSLOIOoMosnVjvDnh+0OvZ8dhYYHE/fqH60/wKBgFr1XKpeWObv/446sQDE\\n+YKrKu6dCye12PgZVXyBpnlOo3YhQiQi/V5/fOh63yK8lyPzXzpxWz5pvmsykYAx\\n6y2ofChcatvZiuZNHgWFOLKLno1nN6KMWxotbZoJ1Q/iWfH9LJtqJzjuENCPxRg7\\n4C1Zg47994ocsY4DJ2x3QEDV\\n-----END PRIVATE KEY-----\\n\", \"client_email\": \"oval-campaign-146701@appspot.gserviceaccount.com\", \"client_id\": \"104834931195951607982\", \"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\", \"token_uri\": \"https://accounts.google.com/o/oauth2/token\", \"auth_provider_x509_cert_url\": \"https://www.googleapis.com/oauth2/v1/certs\", \"client_x509_cert_url\": \"https://www.googleapis.com/robot/v1/metadata/x509/oval-campaign-146701%40appspot.gserviceaccount.com\"}";

        MockMultipartFile configFile = new MockMultipartFile("data", "filename.txt", "text/plain", jsonConfig.getBytes());
        storageService.store(configFile, true);

        GoogleCredential googleCredential = new GoogleCredential("oval-campaign-146701", null);
        googleCredential.setConfigFilePath(configFile.getOriginalFilename());

        CloudCredentialActionRequest cloudCreateCredentialRequest = new CloudCredentialActionRequest();
        cloudCreateCredentialRequest.setCloudosCredential(googleCredential);
        String payload = mapper.writeValueAsString(cloudCreateCredentialRequest);
        ResultActions perform = this.mvc.perform(
                post(CredentialController.CREDENTIAL + "/save").content(payload).contentType(contentType));
        perform.andDo(h -> logger.debug(h.getResponse().getContentAsString()));
        perform.andExpect(status().is2xxSuccessful());
    }

    @After
    public void after() throws Exception {
        this.credentialManagerService.setDefaultCredentials();
    }
}
