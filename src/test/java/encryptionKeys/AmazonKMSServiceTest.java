package encryptionKeys;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import cloudos.encryptionKeys.AmazonKMSService;
import cloudos.encryptionKeys.AmazonKMSServiceException;
import test.CloudOSTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AmazonKMSServiceTest extends CloudOSTest {

  private static String masterKeyARN;

  @Autowired
  private AmazonKMSService amazonKMSService;

  @Test
  public void testAGetCloudosUsernameMasterKey() throws AmazonKMSServiceException {
    byte[] userDataKey = amazonKMSService.getUserEncryptedDataKey();

    assertNotNull(userDataKey);

    assert amazonKMSService.isKeyStateEqualsTo(AmazonKMSService.CLOUDOS_USERS_KEY_ALIAS,
        AmazonKMSService.KEY_STATE_ENABLED) == true;
  }

  @Test
  public void testBGetCloudosCredentialMasterKey() throws AmazonKMSServiceException {
    byte[] credentialDataKey = amazonKMSService.getCredentialEncryptedDataKey();

    assertNotNull(credentialDataKey);

    assert amazonKMSService.isKeyStateEqualsTo(AmazonKMSService.CLOUDOS_CREDENTIALS_KEY_ALIAS,
        AmazonKMSService.KEY_STATE_ENABLED) == true;
  }

  @Test
  public void testCCreateKey() throws AmazonKMSServiceException {
    masterKeyARN = amazonKMSService.createMasterKey("alias/test" + generator.generate(50));

    assertNotNull(masterKeyARN);

    assert amazonKMSService.isKeyStateEqualsTo(masterKeyARN,
        AmazonKMSService.KEY_STATE_ENABLED) == true;
  }

  @Test
  public void testDEncryptData() throws AmazonKMSServiceException, UnsupportedEncodingException,
      InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException,
      IllegalBlockSizeException {

    final byte[] encryptedKey = amazonKMSService.generateEncryptedDataKey(masterKeyARN);

    for (int i = 0; i < 1; i++) {
      String message = generator.generate(50);
      String cipherText = amazonKMSService.encryptMessage(encryptedKey, message);
      String decryptedData = amazonKMSService.decryptMessage(encryptedKey, cipherText);
      assertNotNull(decryptedData);
      assertEquals(decryptedData, message);
    }

  }

  @Test
  public void testFDeleteKey() throws AmazonKMSServiceException {

    amazonKMSService.scheduleKeyDeletion(masterKeyARN);

    assert amazonKMSService.isKeyStateEqualsTo(masterKeyARN,
        AmazonKMSService.KEY_STATE_PENDING_DELETION) == true;

  }



}
