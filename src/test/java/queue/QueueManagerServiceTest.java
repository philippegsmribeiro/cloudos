package queue;

import static org.junit.Assert.assertTrue;
import cloudos.Providers;
import cloudos.billings.BillingFile;
import cloudos.deploy.deployments.Deployment;
import cloudos.google.GoogleInstance;
import cloudos.models.ActionType;
import cloudos.models.CloudActionRequest;
import cloudos.models.CloudosDatapoint;
import cloudos.models.Instance;
import cloudos.models.InstanceStatus;
import cloudos.models.alerts.CloudosAlert;
import cloudos.notifications.Notification;
import cloudos.queue.QueueManagerService;
import cloudos.queue.QueueService;
import cloudos.queue.QueueServiceException;
import cloudos.queue.message.AbstractQueueMessage;
import cloudos.queue.message.AbstractQueueMessage.AbstractQueueMessageConsumer;
import cloudos.queue.message.AlertCreatedMessage;
import cloudos.queue.message.AlertCreatedMessage.AlertCreatedMessageConsumer;
import cloudos.queue.message.AlertDeletedMessage;
import cloudos.queue.message.AlertDeletedMessage.AlertDeletedMessageConsumer;
import cloudos.queue.message.AlertUpdatedMessage;
import cloudos.queue.message.AlertUpdatedMessage.AlertUpdatedMessageConsumer;
import cloudos.queue.message.BillingFileDownloadedMessage;
import cloudos.queue.message.BillingFileDownloadedMessage.BillingFileDownloadedMessageConsumer;
import cloudos.queue.message.ClientInstallMessage;
import cloudos.queue.message.ClientInstallMessage.ClientInstallMessageConsumer;
import cloudos.queue.message.CloudActionRequestMessage;
import cloudos.queue.message.CloudActionRequestMessage.CloudActionRequestMessageConsumer;
import cloudos.queue.message.DatapointMessage;
import cloudos.queue.message.DatapointMessage.DatapointMessageConsumer;
import cloudos.queue.message.DeploymentMessage;
import cloudos.queue.message.DeploymentMessage.DeploymentMessageConsumer;
import cloudos.queue.message.InstanceCreatedMessage;
import cloudos.queue.message.InstanceCreatedMessage.InstanceCreatedMessageConsumer;
import cloudos.queue.message.InstanceDeletedMessage;
import cloudos.queue.message.InstanceDeletedMessage.InstanceDeletedMessageConsumer;
import cloudos.queue.message.InstanceUpdatedMessage;
import cloudos.queue.message.InstanceUpdatedMessage.InstanceUpdatedMessageConsumer;
import cloudos.queue.message.NotificationMessage;
import cloudos.queue.message.NotificationMessage.NotificationMessageConsumer;
import cloudos.queue.message.QueueMessageType;
import java.util.Arrays;
import java.util.List;
import org.bson.types.ObjectId;
import org.jfree.util.Log;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.CloudOSTest;

public class QueueManagerServiceTest extends CloudOSTest {

  @Autowired
  QueueManagerService queueManagerService;
  @Autowired
  QueueService queueService;

  @Test
  public void testCloudActionRequest() throws QueueServiceException {
    queueService.purgeQueue(QueueMessageType.CLOUD_ACTION_REQUEST);
    String id = new ObjectId().toHexString();
    queueManagerService.sendCloudActionRequestNotification(
        CloudActionRequest.createGCloudRequest(null, null, id, ActionType.START), "test");
    this.readIt(new CloudActionRequestMessageConsumer() {

      @Override
      public void accept(CloudActionRequestMessage message) {
        if (message.getRequest().getZone().equals(id)) {
          // delete it
          assertTrue(queueService.deleteMessage(message));
        } else {
          Assert.fail("Message not found");
        }
      }
    });
  }

  @Test
  public void testDeployment() throws QueueServiceException {
    queueService.purgeQueue(QueueMessageType.DEPLOYMENTS);
    String id = new ObjectId().toHexString();
    Deployment deployment = new Deployment();
    deployment.setId(id);
    queueManagerService.sendDeploymentMessage(deployment, "text");
    this.readIt(new DeploymentMessageConsumer() {

      @Override
      public void accept(DeploymentMessage message) {
        if (message.getDeployment().getId().equals(id)) {
          // delete it
          assertTrue(queueService.deleteMessage(message));
        } else {
          Assert.fail("Message not found");
        }
      }
    });
  }

  @Test
  public void testInstanceCreated() throws QueueServiceException {
    queueService.purgeQueue(QueueMessageType.INSTANCE_CREATED);
    String id = new ObjectId().toHexString();
    GoogleInstance instance = new GoogleInstance();
    instance.setId(id);
    queueManagerService.sendInstanceCreatedMessage(instance);
    this.readIt(new InstanceCreatedMessageConsumer() {

      @Override
      public void accept(InstanceCreatedMessage message) {
        if (message.getInstance().getId().equals(id)) {
          // delete it
          assertTrue(queueService.deleteMessage(message));
        } else {
          Assert.fail("Message not found");
        }
      }
    });
  }

  @Test
  public void testInstanceDeleted() throws QueueServiceException {
    queueService.purgeQueue(QueueMessageType.INSTANCE_DELETED);
    String id = new ObjectId().toHexString();
    Instance instance = new Instance();
    instance.setId(id);
    queueManagerService.sendInstanceDeletedMessage(instance);
    this.readIt(new InstanceDeletedMessageConsumer() {

      @Override
      public void accept(InstanceDeletedMessage message) {
        if (message.getInstance().getId().equals(id)) {
          // delete it
          assertTrue(queueService.deleteMessage(message));
        } else {
          Assert.fail("Message not found");
        }
      }
    });
  }

  @Test
  public void testInstanceUpdated() throws QueueServiceException {
    queueService.purgeQueue(QueueMessageType.INSTANCE_UPDATED);
    String id = new ObjectId().toHexString();
    GoogleInstance instance = new GoogleInstance();
    instance.setId(id);
    InstanceStatus status = InstanceStatus.RUNNING;
    queueManagerService.sendInstanceUpdatedMessage(status, instance);
    this.readIt(new InstanceUpdatedMessageConsumer() {

      @Override
      public void accept(InstanceUpdatedMessage message) {
        if (message.getOldInstanceStatus().equals(status)
            && message.getNewInstance().getId().equals(id)) {
          // delete it
          assertTrue(queueService.deleteMessage(message));
        } else {
          Assert.fail("Message not found");
        }
      }
    });
  }

  @Test
  public void testAlertCreated() throws QueueServiceException {
    queueService.purgeQueue(QueueMessageType.ALERT_CREATED);
    String id = new ObjectId().toHexString();
    CloudosAlert alert = new CloudosAlert();
    alert.setId(id);
    queueManagerService.sendAlertCreatedMessage(alert);
    this.readIt(new AlertCreatedMessageConsumer() {

      @Override
      public void accept(AlertCreatedMessage message) {
        if (message.getAlert().getId().equals(id)) {
          // delete it
          assertTrue(queueService.deleteMessage(message));
        } else {
          Assert.fail("Message not found");
        }
      }
    });
  }

  @Test
  public void testAlertDeleted() throws QueueServiceException {
    queueService.purgeQueue(QueueMessageType.ALERT_DELETED);
    String id = new ObjectId().toHexString();
    CloudosAlert alert = new CloudosAlert();
    alert.setId(id);
    queueManagerService.sendAlertDeletedMessage(alert);
    this.readIt(new AlertDeletedMessageConsumer() {

      @Override
      public void accept(AlertDeletedMessage message) {
        if (message.getAlert().getId().equals(id)) {
          // delete it
          assertTrue(queueService.deleteMessage(message));
        } else {
          Assert.fail("Message not found");
        }
      }
    });
  }

  @Test
  public void testAlertUpdated() throws QueueServiceException {
    queueService.purgeQueue(QueueMessageType.ALERT_UPDATED);
    String id = new ObjectId().toHexString();
    CloudosAlert alert = new CloudosAlert();
    alert.setId(id);
    queueManagerService.sendAlertUpdatedMessage(alert);
    this.readIt(new AlertUpdatedMessageConsumer() {

      @Override
      public void accept(AlertUpdatedMessage message) {
        if (message.getAlert().getId().equals(id)) {
          // delete it
          assertTrue(queueService.deleteMessage(message));
        } else {
          Assert.fail("Message not found");
        }
      }
    });
  }

  @Test
  public void testNotification() throws QueueServiceException {
    queueService.purgeQueue(QueueMessageType.NOTIFICATION);
    String id = new ObjectId().toHexString();
    Notification notification = new Notification();
    notification.setId(id);
    queueManagerService.sendNotificationMessage(notification);
    this.readIt(new NotificationMessageConsumer() {

      @Override
      public void accept(NotificationMessage message) {
        if (message.getNotification().getId().equals(id)) {
          // delete it
          assertTrue(queueService.deleteMessage(message));
        } else {
          Assert.fail("Message not found");
        }
      }
    });
  }

  @Test
  public void testDatapoints() throws QueueServiceException {
    queueService.purgeQueue(QueueMessageType.DATAPOINT);
    String id = new ObjectId().toHexString();
    CloudosDatapoint cloudosDatapoint = new CloudosDatapoint();
    cloudosDatapoint.setCloudId("test");
    cloudosDatapoint.setId(id);
    queueManagerService.sendDatapointMessage(Arrays.asList(cloudosDatapoint));
    this.readIt(new DatapointMessageConsumer() {

      @Override
      public void accept(DatapointMessage message) {
        if (message.getDatapoints().get(0).getId().equals(id)) {
          // delete it
          assertTrue(queueService.deleteMessage(message));
        } else {
          Assert.fail("Message not found");
        }
      }
    });
  }

  @Test
  public void testClientInstall() throws QueueServiceException {
    queueService.purgeQueue(QueueMessageType.CLIENT_INSTALL);
    String id = new ObjectId().toHexString();
    Deployment deployment = new Deployment();
    deployment.setId(id);
    queueManagerService.sendClientInstallMessage(deployment, "text");
    this.readIt(new ClientInstallMessageConsumer() {

      @Override
      public void accept(ClientInstallMessage message) {
        if (message.getDeployment().getId().equals(id)) {
          // delete it
          assertTrue(queueService.deleteMessage(message));
        } else {
          Assert.fail("Message not found");
        }
      }
    });
  }

  @Test
  public void testSendBillingFileDownloadedMessage() throws QueueServiceException {

    queueService.purgeQueue(QueueMessageType.BILLING_FILE);
    final String id = new ObjectId().toHexString();
    BillingFile billingFile = new BillingFile();
    billingFile.setId(id);
    billingFile.setProvider(Providers.AMAZON_AWS);

    queueManagerService.sendBillingFileDownloadedMessage(billingFile);
    this.readIt(new BillingFileDownloadedMessageConsumer() {

      @Override
      public void accept(BillingFileDownloadedMessage message) {
        if (message.getBillingFile().getId().equals(id)) {
          // delete it
          assertTrue(queueService.deleteMessage(message));
        } else {
          Assert.fail("Message not found");
        }
      }

    });
  }

  /**
   * Read the message using the consumer
   */
  private void readIt(AbstractQueueMessageConsumer<?> c) throws QueueServiceException {
    try {
      List<AbstractQueueMessage> readMessages =
          queueService.readMessages(c.getType().newInstance().getType());
      for (AbstractQueueMessage message : readMessages) {
        if (c.getType().equals(message.getClass())) {
          c.acceptAbstractQueueMessage(message);
        }
      }
    } catch (Exception e) {
      Log.error("Error reading the message", e);
    }
  }

}
