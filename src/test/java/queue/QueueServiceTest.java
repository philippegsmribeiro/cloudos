package queue;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import cloudos.deploy.deployments.Deployment;
import cloudos.queue.QueueService;
import cloudos.queue.QueueServiceException;
import cloudos.queue.message.AbstractQueueMessage;
import cloudos.queue.message.DeploymentMessage;
import cloudos.queue.message.QueueMessageType;

import java.util.List;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

public class QueueServiceTest extends CloudOSTest {

  @Autowired QueueService queueService;

  /**
   * Send a message, read the messages and verify if the message is there
   *
   * @throws QueueServiceException
   * @throws InterruptedException
   */
  @Test
  @Ignore
  public void testQueue() throws QueueServiceException, InterruptedException {
    assertTrue(queueService.purgeQueue(QueueMessageType.DEPLOYMENTS));
    final DeploymentMessage message = new DeploymentMessage();
    message.setDeployment(new Deployment());
    message.setDescription("test");
    // send a messageDeploymentMessage
    assertTrue(queueService.sendMessage(message));
    // sleep for 1 second
    Thread.sleep(1000);
    // read the messages
    final List<AbstractQueueMessage> readMessages =
        queueService.readMessages(QueueMessageType.DEPLOYMENTS);
    // try to find the sent message
    AbstractQueueMessage readMessage = null;
    for (AbstractQueueMessage msg : readMessages) {
      if (message.getDescription().equals(msg.getDescription())) {
        readMessage = msg;
        break;
      }
    }
    // it has to find it
    assertNotNull(readMessage);
    // delete it
    assertTrue(queueService.deleteMessages(readMessages));
  }
}
