package utils;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import cloudos.utils.ZipArchive;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.io.FilenameUtils;
import org.junit.Test;

import test.CloudOSTest;

public class ZipArchiveTest extends CloudOSTest {

  private ZipArchive zip = new ZipArchive();

  @Test
  public void testZipFile() {
    String content = "This is a test string";
    String filename = "zipfile.txt";
    this.createFile(filename, content);
    String directory = Paths.get(".").toAbsolutePath().normalize().toString();
    logger.debug("------------------------------------");
    logger.debug("Parent directory: " + directory);
    logger.debug("------------------------------------");
    // 2. Create a zip file
    String zipFile = FilenameUtils.removeExtension(filename) + ".zip";
    try {
      this.zip.zipFile(filename, directory, zipFile);

      assertNotNull(zipFile);
      // 3. Check file is not null
      File file = new File(zipFile);
      assertTrue(file.exists());
      // 4. Remove the file created
      this.deleteFile(filename);
      // 5. Remove the tar file
      this.deleteFile(zipFile);
    } catch (IOException ex) {
      logger.error(ex.getMessage());
    }
  }

  @Test
  public void testZipFolder() {
    String directory = Paths.get(".").toAbsolutePath().normalize().toString();
    String sourceFolder = directory + File.separator + "credentials";
    logger.debug("------------------------------------");
    logger.debug("Parent directory: " + directory);
    logger.debug("------------------------------------");
    // Attempt to create a zip file from a directory
    String zipFile = sourceFolder + ".zip";
    try {
      this.zip.zipFolder(zipFile, sourceFolder);
      File file = new File(zipFile);
      assertTrue(file.exists());
      // 5. Remove the tar file
      this.deleteFile(zipFile);
    } catch (IOException ex) {
      logger.error(ex.getMessage());
    }
  }

  @Test
  public void testUnzip() {
    String content = "This is a test string";
    String filename = "zipfile.txt";
    this.createFile(filename, content);
    String directory = Paths.get(".").toAbsolutePath().normalize().toString();
    logger.debug("------------------------------------");
    logger.debug("Parent directory: " + directory);
    logger.debug("------------------------------------");
    // 2. Create a zip file
    String zipFile = FilenameUtils.removeExtension(filename) + ".zip";
    try {
      this.zip.zipFile(filename, directory, zipFile);

      // 4. Remove the file created
      this.deleteFile(filename);
      // 3. Check file is not null
      File file = new File(zipFile);
      assertTrue(file.exists());

      // Unzip the file
      this.zip.unzip(zipFile, directory);
      File file2 = new File(filename);
      assertTrue(file2.exists());

      this.deleteFile(filename);
      // 5. Remove the tar file
      this.deleteFile(zipFile);
    } catch (IOException ex) {
      logger.error(ex.getMessage());
    }
  }

  @SuppressWarnings("Duplicates")
  private void createFile(String filename, String text) {
    try {
      File file = new File(filename);
      if (file.createNewFile()) {
        logger.debug("File is created");
        Files.write(Paths.get(file.getAbsolutePath()), text.getBytes());
        logger.debug("Done");
      } else {
        logger.warn("File already exists");
      }

    } catch (IOException ex) {
      logger.error(ex.getMessage());
    }
  }

  private void deleteFile(String filename) {
    File file = new File(filename);
    if (file.delete()) {
      logger.debug(file.getName() + " is deleted!");
    } else {
      logger.debug("Delete operation is failed.");
    }
  }
}
