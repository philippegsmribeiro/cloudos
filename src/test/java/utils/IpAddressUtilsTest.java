package utils;

import static org.junit.Assert.assertEquals;

import cloudos.utils.IpAddressUtils;

import java.util.regex.Pattern;

import org.junit.Test;

import test.CloudOSTest;

/** Created by gleimar on 26/03/2017. */
public class IpAddressUtilsTest extends CloudOSTest {

  @Test
  public void testGetIpAddress() {

    String regexOnlyIpV4PublicAddress =
        "^([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(?<!172\\.(16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31))(?<!127)(?<!^10)(?<!^0)\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(?<!192\\.168)(?<!172\\.(16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31))\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(?<!\\.0$)(?<!\\.255$)$";
    Pattern pattern = Pattern.compile(regexOnlyIpV4PublicAddress);

    String externalIpAddress = IpAddressUtils.getExternalIpAddress();

    assertEquals(pattern.matcher(externalIpAddress).find(), true);
  }
}
