package utils;

import cloudos.utils.KeyPairUtil;

import com.jcraft.jsch.KeyPair;

import java.util.Map;

import org.junit.Test;

import test.CloudOSTest;

/** Created by gleimar on 01/05/2017. */
public class KeyPairUtilTest extends CloudOSTest {

  @Test
  public void testKeyGeneration() {
    Map.Entry<String, String> keyPair =
        KeyPairUtil.generateKeyPair(KeyPair.RSA, 2048, "cloudos test");

    assert keyPair != null;

    logger.debug(String.format("private key:%s", keyPair.getKey()));
    assert keyPair.getKey() != null;

    logger.debug(String.format("public key:%s", keyPair.getValue()));
    assert keyPair.getValue() != null;
  }
}
