package utils;

import java.io.IOException;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

@Configuration
public class PropertyTestConfiguration {

  /**
   * Configure a Property test file
   *
   * @return
   * @throws IOException
   */
  @Bean
  public PropertyPlaceholderConfigurer propertyPlaceholderConfigurer() throws IOException {
    final PropertyPlaceholderConfigurer ppc = new PropertyPlaceholderConfigurer();
    ppc.setLocations(
        ArrayUtils.addAll(
            new PathMatchingResourcePatternResolver()
                .getResources("classpath*:application.properties"),
            new PathMatchingResourcePatternResolver()
                .getResources("classpath*:application-test.properties")));
    return ppc;
  }
}
