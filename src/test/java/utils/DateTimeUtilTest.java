package utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import cloudos.utils.DateTimeUtil;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import org.junit.Test;

import test.CloudOSTest;

/** Created by gleimar on 11/02/2017. */
public class DateTimeUtilTest extends CloudOSTest {

  @Test
  public void testConversionMilliseconds() {
    long timeMilliseconds =
        DateTimeUtil.convertToMilliseconds("2017-02-11 13:16:35.181", "yyyy-MM-dd HH:mm:ss.SSS");

    logger.debug(String.format("timeMilliseconds:%s", timeMilliseconds));
    logger.debug(String.format("dateFormat : %s", DateTimeUtil.convertToDate(timeMilliseconds)));

    assertEquals(timeMilliseconds, 1486818995181l);
  }

  @Test
  public void testDateConverterEpoch() {

    LocalDateTime localDateTime = DateTimeUtil.convertToDate(1262044800000l);

    String str = "2009-12-29 00:00";
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    LocalDateTime compareDate = LocalDateTime.parse(str, formatter);

    logger.debug(localDateTime.format(formatter));
    logger.debug(compareDate.format(formatter));
    logger.debug(compareDate.toEpochSecond(ZoneOffset.ofTotalSeconds(0)));

    assertTrue(localDateTime.equals(compareDate));
  }
}
