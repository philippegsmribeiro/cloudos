package utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import cloudos.utils.FileUtils;

import org.junit.Test;

import test.CloudOSTest;

public class FileUtilsTest extends CloudOSTest {


  @Test
  public void testMd5Hash() throws Exception {
    String filename  =
        new org.datavec.api.util.ClassPathResource(
            "/machinelearning/autoscaler/train_autoscale_classifier.csv")
            .getFile()
            .getPath();
    // get the hash value of the file
    String md5 = FileUtils.getMd5Hash(filename);
    assertNotNull(md5);

    // do it again
    assertEquals(md5, FileUtils.getMd5Hash(filename));
  }
}
