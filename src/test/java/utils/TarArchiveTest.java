package utils;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import cloudos.utils.TarArchive;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Test;

import test.CloudOSTest;

public class TarArchiveTest extends CloudOSTest {

  private TarArchive tar = new TarArchive();

  @SuppressWarnings("Duplicates")
  private void createFile(String filename, String text) {
    try {
      File file = new File(filename);
      if (file.createNewFile()) {
        logger.debug("File is created");
        Files.write(Paths.get(file.getAbsolutePath()), text.getBytes());
        logger.debug("Done");
      } else {
        logger.warn("File already exists");
      }

    } catch (IOException ex) {
      logger.error(ex.getMessage());
    }
  }

  private void deleteFile(String filename) {
    File file = new File(filename);
    if (file.delete()) {
      logger.debug(file.getName() + " is deleted!");
    } else {
      logger.debug("Delete operation is failed.");
    }
  }

  @Test
  public void testCreateArchive() {
    // 1. Create a file
    String content = "This is a test string";
    String filename = "tarfile.txt";
    this.createFile(filename, content);
    String directory = Paths.get(".").toAbsolutePath().normalize().toString();
    logger.debug("------------------------------------");
    logger.debug("Parent directory: " + directory);
    logger.debug("------------------------------------");
    // 2. Create a tar file
    try {
      String tarfile = this.tar.createArchive(filename, directory);
      assertNotNull(tarfile);
      // 3. Check file is not null
      File file = new File(tarfile);
      assertTrue(file.exists());
      // 4. Remove the file created
      this.deleteFile(filename);
      // 5. Remove the tar file
      this.deleteFile(tarfile);
    } catch (IOException ex) {
      logger.error(ex.getMessage());
    }
  }

  @Test
  public void testCreateDirectory() {
    // Tar a directory now
    String filename = "credentials";
    String directory = Paths.get(".").toAbsolutePath().normalize().toString();
    logger.debug("------------------------------------");
    logger.debug("Parent directory: " + directory);
    logger.debug("------------------------------------");
    // 2. Create a tar file
    try {
      String tarfile = this.tar.createArchive(filename, directory);
      assertNotNull(tarfile);
      // 3. Check file is not null
      File file = new File(tarfile);
      assertTrue(file.exists());
      // 5. Remove the tar file
      this.deleteFile(tarfile);
    } catch (IOException ex) {
      logger.error(ex.getMessage());
    }
  }

  @Test
  public void testUntarArchive() {
    // 1. Create a file
    String content = "This is a test string";
    String filename = "anothertarfile.txt";
    this.createFile(filename, content);
    String directory = Paths.get(".").toAbsolutePath().normalize().toString();
    logger.debug("------------------------------------");
    logger.debug("Parent directory: " + directory);
    logger.debug("------------------------------------");
    // 2. Create a tar file
    try {
      String tarfile = this.tar.createArchive(filename, directory);
      assertNotNull(tarfile);
      // 3. Check file is not null
      File file = new File(tarfile);
      assertTrue(file.exists());
      // 4. Remove the file created
      this.deleteFile(filename);
      this.tar.untarArchive(tarfile);
      // 5. Remove the tar file
      this.deleteFile(tarfile);
    } catch (Exception ex) {
      logger.error(ex.getMessage());
    }
  }
}
