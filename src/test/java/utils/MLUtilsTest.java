package utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import cloudos.machinelearning.MLUtils;
import cloudos.models.CloudosDatapoint;
import cloudos.models.CloudosDatapointsRepository;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections4.ListUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import test.CloudOSTest;

public class MLUtilsTest extends CloudOSTest {

  @Autowired
  CloudosDatapointsRepository datapointsRepository;

  @Value("${cloudos.ml.autoscaler}/train_datapoints_0.csv")
  String trainDatapoints;

  @Value("${cloudos.ml.autoscaler}/test_datapoints_0.csv")
  String testDatapoints;

  @Value("${cloudos.ml.autoscaler}")
  String autoscaler;

  @Before
  public void setUp() throws Exception {
    File training = new File(autoscaler);
    if (!training.exists()) {
      training.mkdirs();
    }
  }

  @Test
  public void testCalculateStorage() {
    // test the calculateStorage
    assertTrue(MLUtils.calculateStorage(null) == 0);
    assertTrue(MLUtils.calculateStorage("") == 0);
    assertTrue(MLUtils.calculateStorage("1") == 1.0);
    assertTrue(MLUtils.calculateStorage("10") == 10.0);
    assertTrue(MLUtils.calculateStorage("10 x 10") == 100.0);
    assertTrue(MLUtils.calculateStorage("10 X 10") == 100.0);
  }

  @Test
  public void testGetStorage() {
    assertTrue(MLUtils.getStorage(null) == 0);
    assertTrue(MLUtils.getStorage("") == 0);
    assertTrue(MLUtils.getStorage("1") == 1);
    assertTrue(MLUtils.getStorage("10") == 10);
    assertTrue(MLUtils.getStorage("24 x 2000 HDD") == 48000.0);
    assertTrue(MLUtils.getStorage("1 x 80 SSD") == 80);
    assertTrue(MLUtils.getStorage("EBS only") == 0);
  }

  @Test
  public void testSaveDatapoints() {
    // return the datapoints already sorted.
    List<CloudosDatapoint> datapoints = datapointsRepository.findTop100ByOrderByTimestamp();

    if (datapoints != null && datapoints.size() > 0) {
      int size = datapoints.size();

      int trainSize = new Double(size * 0.7).intValue();
      // Separate Test and Train

      MLUtils.saveDatapoints(datapoints.subList(0, trainSize), trainDatapoints);

      MLUtils.saveDatapoints(datapoints.subList(trainSize + 1, size - 1), testDatapoints);
    }
  }

  @Test
  public void testSlice() {
    List<Integer> largeList = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    List<List<Integer>> output = ListUtils.partition(largeList, 5);
    assertEquals(output.size(), 2);
    assertEquals(output.get(0).size(), 5);
  }

  @Test
  public void testToArray() {
    List<Integer> largeList = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    List<List<Integer>> output = ListUtils.partition(largeList, 5);
    List<Integer>[] arrayList = MLUtils.toArray(output);
    assertEquals(output.size(), arrayList.length);
  }
}
