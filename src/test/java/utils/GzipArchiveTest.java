package utils;

import static org.junit.Assert.assertTrue;

import cloudos.utils.GzipArchive;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Test;

import test.CloudOSTest;

/** Created by philipperibeiro on 11/24/16. */
public class GzipArchiveTest extends CloudOSTest {

  private static GzipArchive gzip = new GzipArchive();

  @Test
  public void gunzipItTest() throws Exception {
    String content = "This is a test string";
    String filename = "gziptest.txt";
    this.createFile(filename, content);
    String directory = Paths.get(".").toAbsolutePath().normalize().toString();
    logger.debug("------------------------------------");
    logger.debug("Parent directory: " + directory);
    logger.debug("------------------------------------");
    String gzipfile = filename.replace("txt", "gz");
    gzip.gzipIt(filename, gzipfile);
    File file = new File(gzipfile);
    assertTrue(file.exists());
    // 4. Remove the file created
    this.deleteFile(filename);
    gzip.gunzipIt(gzipfile, filename);
    // 5. Remove the tar file
    this.deleteFile(gzipfile);
    this.deleteFile(filename);
  }

  @Test
  public void gzipItTest() throws Exception {
    String content = "This is a test string";
    String filename = "gziptest.txt";
    this.createFile(filename, content);
    String directory = Paths.get(".").toAbsolutePath().normalize().toString();
    logger.debug("------------------------------------");
    logger.debug("Parent directory: " + directory);
    logger.debug("------------------------------------");
    String gzipfile = filename.replace("txt", "gz");
    gzip.gzipIt(filename, gzipfile);
    File file = new File(gzipfile);
    assertTrue(file.exists());
    // 4. Remove the file created
    this.deleteFile(filename);
    // 5. Remove the tar file
    this.deleteFile(gzipfile);
  }

  @SuppressWarnings("Duplicates")
  private void createFile(String filename, String text) {
    try {
      File file = new File(filename);
      if (file.createNewFile()) {
        logger.debug("File is created");
        Files.write(Paths.get(file.getAbsolutePath()), text.getBytes());
        logger.debug("Done");
      } else {
        logger.warn("File already exists");
      }

    } catch (IOException ex) {
      logger.error(ex.getMessage());
    }
  }

  private void deleteFile(String filename) {
    File file = new File(filename);
    if (file.delete()) {
      logger.debug(file.getName() + " is deleted!");
    } else {
      logger.debug("Delete operation is failed.");
    }
  }
}
