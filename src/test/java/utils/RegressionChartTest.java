package utils;

import cloudos.utils.RegressionChart;

import java.awt.GraphicsEnvironment;

import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RefineryUtilities;
import org.junit.Test;

import test.CloudOSTest;

/** Created by philipperibeiro on 11/24/16. */
public class RegressionChartTest extends CloudOSTest {

  static {
    System.setProperty("java.awt.headless", "true");
    boolean headless = GraphicsEnvironment.isHeadless();
    System.out.println("Headless: " + headless);
  }

  /**
   * Creates and returns a sample dataset. The data was randomly generated.
   *
   * @return a sample dataset.
   */
  private XYDataset createSampleData() {

    XYSeries series = new XYSeries("Series 1");
    series.add(2.0, 56.27);
    series.add(3.0, 41.32);
    series.add(4.0, 31.45);
    series.add(5.0, 30.05);
    series.add(6.0, 24.69);
    series.add(7.0, 19.78);
    series.add(8.0, 20.94);
    series.add(9.0, 16.73);
    series.add(10.0, 14.21);
    series.add(11.0, 12.44);
    XYDataset result = new XYSeriesCollection(series);
    return result;
  }

  /** The starting point for the regression demo. */
  @Test(expected = java.awt.HeadlessException.class)
  public void testRegressionChart() throws Exception {
    RegressionChart appFrame = new RegressionChart("Regression Chart", this.createSampleData());
    appFrame.pack();
    RefineryUtilities.centerFrameOnScreen(appFrame);
    appFrame.setVisible(false);
  }
}
