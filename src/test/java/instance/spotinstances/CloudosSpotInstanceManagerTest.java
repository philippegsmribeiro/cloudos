package instance.spotinstances;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.OrderingComparison.greaterThanOrEqualTo;
import static org.junit.Assert.assertNotNull;

import cloudos.Providers;
import cloudos.instances.spotinstances.CloudosSpotInstanceManager;
import cloudos.models.spotinstances.CloudSpotPricingRequest;
import cloudos.models.spotinstances.SpotProductDescription;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

public class CloudosSpotInstanceManagerTest extends CloudOSTest {

  @Autowired
  private CloudosSpotInstanceManager spotInstanceManager;

  @Test
  public void testPricing() throws Exception {
    CloudSpotPricingRequest spotPricingRequest = CloudSpotPricingRequest.builder()
        .provider(Providers.AMAZON_AWS)
        .region("us-east-1")
        .availabilityZone("us-east-1b")
        .startTime(new Date(System.currentTimeMillis() - 3600 * 1000))
        .endTime(new Date())
        .instanceTypes(Collections.singletonList("m3.medium"))
        .productDescription(Collections.singletonList(SpotProductDescription.AWS_LINUX_UNIX.toString()))
        .build();
    List<?> spotPrices = this.spotInstanceManager.pricing(spotPricingRequest);
    assertNotNull(spotPrices);
    assertThat(spotPrices.size(), greaterThanOrEqualTo(0));
    spotPrices.forEach(spotPrice -> logger.debug(spotPrice));
  }
}

