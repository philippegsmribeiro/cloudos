package instance.spotinstances;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.hamcrest.number.OrderingComparison.greaterThanOrEqualTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import cloudos.Providers;
import cloudos.amazon.AmazonInstance;
import cloudos.amazon.models.AmazonSpotInstance;
import cloudos.instances.spotinstances.AmazonSpotInstanceManager;
import cloudos.models.ActionType;
import cloudos.models.CloudActionRequest;
import cloudos.models.CloudActionResponse;
import cloudos.models.spotinstances.CloudSpotCancelRequest;
import cloudos.models.spotinstances.CloudSpotCreateRequest;
import cloudos.models.spotinstances.CloudSpotFleetCreateRequest;
import cloudos.models.spotinstances.CloudSpotFleetCreateResponse;
import cloudos.models.spotinstances.CloudSpotPricingRequest;
import cloudos.models.spotinstances.SpotProductDescription;

import com.amazonaws.services.ec2.model.AllocationStrategy;
import com.amazonaws.services.ec2.model.ExcessCapacityTerminationPolicy;
import com.amazonaws.services.ec2.model.FleetType;
import com.amazonaws.services.ec2.model.SpotInstanceType;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AmazonSpotInstanceManagerTest extends CloudOSTest {

  @Autowired
  AmazonSpotInstanceManager amazonSpotInstanceManager;

  static CloudSpotCreateRequest request;

  static List<String> instanceIds = new ArrayList<>();

  static List<AmazonInstance> amazonInstances = new ArrayList<>();

  private static String region = "us-east-2";
  private static String machineType = "m4.large";
  private static String image = "ami-336b4456";
  private static String zone = "us-east-2a";
  private static String securityGroup = "default";
  private static Double spotPrice = 0.035;

  @BeforeClass
  public static void setUp() throws Exception {
    request = CloudSpotCreateRequest.createAwsCloudRequest(region, null, machineType,
        image, 2, securityGroup, null, zone, null, spotPrice, 1, "", null,
        null, null, SpotInstanceType.OneTime, null);
  }

  @Test
  public void test1SpotRequest() throws Exception {
    List<AmazonInstance> amazonInstanceList =
        this.amazonSpotInstanceManager.createInstances(request);
    assertNotNull(amazonInstanceList);
    assertThat(amazonInstanceList.size(), greaterThan(0));

    amazonInstances.addAll(amazonInstanceList);

    // list instances created
    amazonInstanceList.forEach(amazonInstance -> {
      logger.debug("AmazonInstance: {}", amazonInstance);
      instanceIds.add(amazonInstance.getInstance().getProviderId());
    });
  }

  @Test
  @SuppressWarnings("unchecked")
  @Ignore
  public void test2CreateSpotFleet() throws Exception {
    CloudSpotFleetCreateRequest fleetCreateRequest =
        CloudSpotFleetCreateRequest.builder().provider(Providers.AMAZON_AWS).region(region)
            .excessCapacityTerminationPolicy(ExcessCapacityTerminationPolicy.Default.toString())
            .allocationStrategy(AllocationStrategy.LowestPrice.toString()).spotPrice(String.valueOf(spotPrice))
            .targetCapacity(1).fulfilledCapacity(2.0).type(FleetType.Request.toString())
            .terminateInstancesWithExpiration(true).replaceUnhealthyInstances(true)
            .iamFleetRole("arn:aws:iam::287334376268:role/spotfleet").build();

    CloudSpotFleetCreateResponse response =
        this.amazonSpotInstanceManager.createSpotFleet(fleetCreateRequest);

    // check the response
    assertNotNull(response);
    assertNotNull(response.getFleetRequestId());

    // check the instances created
    List<AmazonInstance> amazonInstanceList = (List<AmazonInstance>) response.getInstances();

    assertNotNull(amazonInstanceList);
    assertThat(amazonInstanceList.size(), greaterThan(0));

    amazonInstances.addAll(amazonInstanceList);

    // list instances created
    amazonInstanceList.forEach(amazonInstance -> {
      logger.debug("AmazonInstance: {}", amazonInstance);
      instanceIds.add(amazonInstance.getInstance().getProviderId());
    });
  }

  @Test
  public void test3Terminate() throws Exception {
    CloudActionRequest actionRequest = new CloudActionRequest(Providers.AMAZON_AWS, instanceIds,
        region, zone, securityGroup, true, ActionType.TERMINATE);
    List<CloudActionResponse<AmazonInstance>> responseList =
        this.amazonSpotInstanceManager.terminate(actionRequest);
    assertNotNull(responseList);
    assertThat(responseList.size(), greaterThan(0));
  }

  @Test
  public void test4CancelSpotRequest() throws Exception {

    List<String> spotInstanceIds = new ArrayList<>();
    for (AmazonInstance instance : amazonInstances) {
      AmazonSpotInstance spotInstance = (AmazonSpotInstance) instance;
      spotInstanceIds.add(spotInstance.getSpotInstanceRequestId());
    }

    CloudSpotCancelRequest cancelRequest =
        CloudSpotCancelRequest.builder().provider(Providers.AMAZON_AWS).region(region)
            .spotInstanceRequestIds(spotInstanceIds).terminateInstances(true).build();


    assertTrue(this.amazonSpotInstanceManager.cancelSpotRequest(cancelRequest));
  }

  @Test
  public void test5Pricing() throws Exception {
    // for product type use one of
    // (Linux/UNIX | SUSE Linux | Windows | Linux/UNIX (Amazon VPC) | SUSE Linux (Amazon VPC) |
    // Windows (Amazon VPC)).
    // fetch the last hour of pricing history
    CloudSpotPricingRequest spotPricingRequest =
        CloudSpotPricingRequest.builder().provider(Providers.AMAZON_AWS).region(region)
            .availabilityZone(zone)
            .startTime(new Date(System.currentTimeMillis() - 3600 * 1000)).endTime(new Date())
            .instanceTypes(Collections.singletonList(machineType))
            .productDescription(
                Collections.singletonList(SpotProductDescription.AWS_LINUX_UNIX.toString()))
            .build();
    List<?> spotPrices = this.amazonSpotInstanceManager.pricing(spotPricingRequest);
    assertNotNull(spotPrices);
    assertThat(spotPrices.size(), greaterThanOrEqualTo(0));
    spotPrices.forEach(spotPrice -> logger.debug(spotPrice));
  }

}
