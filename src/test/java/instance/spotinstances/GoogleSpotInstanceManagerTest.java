package instance.spotinstances;

import static org.junit.Assert.fail;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.bson.types.ObjectId;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;

import cloudos.Providers;
import cloudos.google.GoogleInstance;
import cloudos.instances.InstanceRequestException;
import cloudos.instances.spotinstances.GoogleSpotInstanceManager;
import cloudos.models.ActionType;
import cloudos.models.CloudActionRequest;
import cloudos.models.spotinstances.CloudSpotCreateRequest;
import cloudos.models.spotinstances.CloudSpotPricingRequest;
import cloudos.pricing.PricingValue;
import test.CloudOSTest;
import test.SlowTest;

@Category(SlowTest.class)
public class GoogleSpotInstanceManagerTest extends CloudOSTest {

  @Autowired
  GoogleSpotInstanceManager googleSpotInstanceManager;

  private String zone = "us-west1-a";
  private String region = "us-west1";
  private String machineType = "f1-micro";
  private String imageProject = "centos-cloud";
  private String image = "centos-7-v20161027";
  private String name = "tm";

  @Test
  public void testCreateSpotInstance() {
    try {
      CloudSpotCreateRequest request =
          CloudSpotCreateRequest.createGCloudRequest(region, name + new ObjectId().toHexString(),
              machineType, image, imageProject, 1, "gcloudsshkey", zone, null);
      List<GoogleInstance> instances = googleSpotInstanceManager.createInstances(request);
      Assert.assertEquals(1, instances.size());
      for (GoogleInstance googleInstance : instances) {
        Assert.assertTrue(googleInstance.getInstance().isSpot());
      }

      // terminate
      CloudActionRequest terminateRequest = CloudActionRequest.createGCloudRequest(
          instances.stream().map(i -> i.getInstance().getName()).collect(Collectors.toList()), region,
          zone, ActionType.TERMINATE);
      terminateRequest.setSynchronous(false);
      googleSpotInstanceManager.terminate(terminateRequest);
    } catch (InstanceRequestException exception) {
      // do nothing
    } catch (Exception exception) {
      fail();
    }
  }

  @Test
  @SuppressWarnings("unchecked")
  public void testGetPricing() {
    CloudSpotPricingRequest request = CloudSpotPricingRequest.builder()
        .provider(Providers.GOOGLE_COMPUTE_ENGINE)
        .region(region)
        .instanceTypes(Collections.singletonList(machineType))
        .build();
    List<PricingValue> pricing = (List<PricingValue>) googleSpotInstanceManager.pricing(request);
    for (PricingValue pricingValue : pricing) {
      Assert.assertTrue(pricingValue.getValue() != null);
    }
  }

}
