package cloudos;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import cloudos.security.WebSecurityConfig;
import cloudos.security.jwt.JwtUtils;

import com.google.gson.Gson;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;

import java.util.Map;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;

import test.CloudOSTest;

public class WebSecurityTest extends CloudOSTest {

  @Autowired private WebApplicationContext context;
  @Autowired private JwtUtils jwtUtils;

  private MockMvc mvc;

  @Before
  public void setup() {
    mvc =
        MockMvcBuilders.webAppContextSetup(context)
            // .alwaysDo(print()) //only for debug purpose
            .apply(springSecurity())
            .build();
  }

  /**
   * test the skip jwt validation flag - for testing reasons
   *
   * @throws Exception
   */
  @Test
  public void testSkipJwtValidation() throws Exception {
    logger.debug("NO JWT");
    jwtUtils.setSkipValidation(true);
    mvc.perform(get(WebSecurityConfig.API_PATH)).andExpect(status().isNotFound());
    jwtUtils.setSkipValidation(false);
    logger.debug("JWT");
    mvc.perform(get(WebSecurityConfig.API_PATH)).andExpect(status().isUnauthorized());
  }

  private ResultActions performLogon(String user, String password) throws Exception {
    return mvc.perform(
        post(WebSecurityConfig.LOGIN_PATH)
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .param("username", user)
            .param("password", password));
  }

  @SuppressWarnings("unchecked")
  private boolean validateTokenResponse(String content, String user, boolean validateRefresh)
      throws Exception {
    logger.debug("validate content");
    assertNotNull(content);
    Map<String, String> map = new Gson().fromJson(content, Map.class);
    assertNotNull(map);
    // token
    String token = map.get("token");
    assertNotNull(token);
    Jws<Claims> parseToken = jwtUtils.parseToken(token);
    assertNotNull(parseToken);
    assertTrue(parseToken.getBody().getSubject().equals(user));
    if (validateRefresh) {
      // refresh token
      String refreshToken = map.get("refreshToken");
      assertNotNull(refreshToken);
      Jws<Claims> parseToken2 = jwtUtils.parseToken(refreshToken);
      assertNotNull(parseToken2);
      assertTrue(parseToken2.getBody().getSubject().equals(user));
    }
    return true;
  }

  @SuppressWarnings("unchecked")
  private String getRefreshToken(String content) throws Exception {
    Map<String, String> map = new Gson().fromJson(content, Map.class);
    return map.get("refreshToken");
  }

  @Test
  @Ignore
  public void testLogon() throws Exception {
    logger.debug("Login");
    String content =
        performLogon("admin", "admin")
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();
    assertNotNull(content);
    validateTokenResponse(content, "admin", true);
  }

  @Test
  public void testRefreshTokenWithoutToken() throws Exception {
    mvc.perform(post(WebSecurityConfig.TOKEN_REFRESH_PATH)).andExpect(status().isUnauthorized());
  }

  @Test
  @Ignore
  public void testRefreshToken() throws Exception {
    // logon first
    ResultActions logon = performLogon("admin", "admin");
    String token = getRefreshToken(logon.andReturn().getResponse().getContentAsString());
    String newTokenMap =
        mvc.perform(
                post(WebSecurityConfig.TOKEN_REFRESH_PATH)
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .param("refresh_token", token))
            .andReturn()
            .getResponse()
            .getContentAsString();
    assertNotNull(newTokenMap);
    validateTokenResponse(newTokenMap, "admin", true);
  }

  @Ignore
  @Test
  public void listRestControllerAndPaths() throws Exception {
    String[] beanNamesForAnnotation = context.getBeanNamesForAnnotation(RestController.class);
    for (String string : beanNamesForAnnotation) {
      Object bean = context.getBean(string);
      if (bean != null) {
        RequestMapping annotation = bean.getClass().getAnnotation(RequestMapping.class);
        if (annotation != null) {
          String[] value = annotation.value();
          if (value != null && value.length > 0) {
            try {
              System.out.println(
                  "Controller: "
                      + string
                      + " - Mapping Found: "
                      + value[0]
                      + " - "
                      + mvc.perform(get(value[0])).andReturn().getResponse().getStatus());
            } catch (Exception e) {
              System.out.println(
                  "Controller: "
                      + string
                      + " - Mapping Found: "
                      + value[0]
                      + " - "
                      + e.getLocalizedMessage());
              // TODO: handle exception
            }
          }
        } else {
          System.out.println("No default mapping for the controller: " + string);
        }
      }
    }
  }

  @Test
  @Ignore
  public void testAccessDenied() throws Exception {
    mvc.perform(get(DashboardController.DASHBOARD)).andExpect(status().isOk());
  }

  @Test
  @Ignore
  public void testLogout() throws Exception {
    mvc.perform(get("/logout")).andExpect(redirectedUrlPattern("/"));
  }

  @Test
  @Ignore
  public void testLoginPage() throws Exception {
    mvc.perform(get("/login")).andExpect(status().isOk()).andExpect(view().name("login"));
  }

  @Test
  @Ignore
  public void testHomePage() throws Exception {
    mvc.perform(get("/")).andExpect(status().isOk());
  }

  private static ResultMatcher redirectedUrlPattern(final String expectedUrlPattern) {
    return result ->
        assertTrue(result.getResponse().getRedirectedUrl().endsWith(expectedUrlPattern));
  }
}
