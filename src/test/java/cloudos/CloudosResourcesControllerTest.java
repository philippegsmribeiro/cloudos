package cloudos;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.core.BooleanBuilder;

import cloudos.amazon.AmazonInstance;
import cloudos.amazon.AmazonInstanceRepository;
import cloudos.data.InstanceData;
import cloudos.data.InstanceDataMetric;
import cloudos.data.InstanceDataType;
import cloudos.google.GoogleInstance;
import cloudos.google.GoogleInstanceRepository;
import cloudos.instances.InstanceService;
import cloudos.models.ClientStatus;
import cloudos.models.Instance;
import cloudos.models.InstanceStatus;
import cloudos.models.QInstance;
import cloudos.provider.ProviderMachineType;
import cloudos.provider.ProviderRegion;
import cloudos.provider.ProvidersService;
import cloudos.utils.datafactory.DataFactory;
import lombok.extern.log4j.Log4j2;
import test.CloudOSTest;

/** Created by philipperibeiro on 3/20/17. */
@Log4j2
public class CloudosResourcesControllerTest extends CloudOSTest {

  private MockMvc mockMvc;

  @Autowired
  private WebApplicationContext webApplicationContext;

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private InstanceService instanceService;

  @Autowired
  private ProvidersService providersService;

  @Autowired
  private AmazonInstanceRepository amazonInstanceRepository;

  @Autowired
  private GoogleInstanceRepository googleInstanceRepository;

  private static List<Instance> listResourcesCache;

  @BeforeClass
  public static void init() {
    listResourcesCache = new ArrayList<Instance>();
  }

  @Before
  public void prepareTest() throws Exception {
    this.mockMvc = webAppContextSetup(webApplicationContext).build();
    BooleanBuilder builder = new BooleanBuilder();
    QInstance instance = new QInstance("instance");
    builder.and(instance.name.startsWith("test"));
    Iterable<Instance> pageResource = instanceService.findAll(builder);
    this.clearResource(pageResource);
    this.initializeDataTest();
  }

  @After
  public void finishTest() {
    this.clearResource(listResourcesCache);
  }


  @Test
  @Ignore
  public void testGetInstanceData() throws Exception {
    List<Instance> findByProviderAndDeleted =
        instanceService.findAllActiveInstancesByProvider(Providers.GOOGLE_COMPUTE_ENGINE);
    if (CollectionUtils.isNotEmpty(findByProviderAndDeleted)) {
      // get first
      Instance instance = findByProviderAndDeleted.get(0);
      // every 10 minutes, since the last 24hrs....
      final long timeWindow = 60 * 60 * 24;
      final int timeFrame = 60 * 10;
      InstanceData data = new InstanceData(Providers.AMAZON_AWS, InstanceDataMetric.CPU_UTILIZATION,
          InstanceDataType.AVERAGE, timeWindow, timeFrame);
      this.mockMvc.perform(
          post(CloudosResourcesController.RESOURCES + "/list/" + instance.getId() + "/datapoints")
              .content(data.toString()).contentType(this.contentType))
          .andExpect(status().isOk());
      this.mockMvc.perform(
          post(CloudosResourcesController.RESOURCES + "/list/" + instance.getId() + "/datapoints")
              .content(data.toString()).contentType(this.contentType))
          .andExpect(status().isOk());

      data = new InstanceData(Providers.AMAZON_AWS, InstanceDataMetric.DISK_READ_BYTES,
          InstanceDataType.AVERAGE, timeWindow, timeFrame);
      this.mockMvc.perform(
          post(CloudosResourcesController.RESOURCES + "/list/" + instance.getId() + "/datapoints")
              .content(data.toString()).contentType(this.contentType))
          .andExpect(status().isOk());
      this.mockMvc.perform(
          post(CloudosResourcesController.RESOURCES + "/list/" + instance.getId() + "/datapoints")
              .content(data.toString()).contentType(this.contentType))
          .andExpect(status().isOk());

      data = new InstanceData(Providers.AMAZON_AWS, InstanceDataMetric.NETWORK_RECEIVED_BYTES,
          InstanceDataType.AVERAGE, timeWindow, timeFrame);
      this.mockMvc.perform(
          post(CloudosResourcesController.RESOURCES + "/list/" + instance.getId() + "/datapoints")
              .content(data.toString()).contentType(this.contentType))
          .andExpect(status().isOk());
      this.mockMvc.perform(
          post(CloudosResourcesController.RESOURCES + "/list/" + instance.getId() + "/datapoints")
              .content(data.toString()).contentType(this.contentType))
          .andExpect(status().isOk());
    }
  }

  @Test
  public void testGetInstanceGoogleData() throws Exception {
    List<Instance> findByProviderAndDeleted =
        instanceService.findAllActiveInstancesByProvider(Providers.GOOGLE_COMPUTE_ENGINE);
    if (CollectionUtils.isNotEmpty(findByProviderAndDeleted)) {
      // get first
      Instance instance = findByProviderAndDeleted.get(0);
      // last 24hrs....
      final long timeWindow = 60 * 60 * 24;
      final int timeFrame = 60 * 10;
      InstanceData data = new InstanceData(Providers.GOOGLE_COMPUTE_ENGINE,
          InstanceDataMetric.CPU_UTILIZATION, InstanceDataType.AVERAGE, timeWindow, timeFrame);
      this.mockMvc.perform(
          post(CloudosResourcesController.RESOURCES + "/list/" + instance.getId() + "/datapoints")
              .content(data.toString()).contentType(this.contentType))
          .andExpect(status().isOk());
      this.mockMvc.perform(
          post(CloudosResourcesController.RESOURCES + "/list/" + instance.getId() + "/datapoints")
              .content(data.toString()).contentType(this.contentType))
          .andExpect(status().isOk());

      data = new InstanceData(Providers.GOOGLE_COMPUTE_ENGINE, InstanceDataMetric.DISK_READ_BYTES,
          InstanceDataType.AVERAGE, timeWindow, timeFrame);
      this.mockMvc.perform(
          post(CloudosResourcesController.RESOURCES + "/list/" + instance.getId() + "/datapoints")
              .content(data.toString()).contentType(this.contentType))
          .andExpect(status().isOk());
      this.mockMvc.perform(
          post(CloudosResourcesController.RESOURCES + "/list/" + instance.getId() + "/datapoints")
              .content(data.toString()).contentType(this.contentType))
          .andExpect(status().isOk());

      data = new InstanceData(Providers.GOOGLE_COMPUTE_ENGINE,
          InstanceDataMetric.NETWORK_RECEIVED_BYTES, InstanceDataType.AVERAGE, timeWindow,
          timeFrame);
      this.mockMvc.perform(
          post(CloudosResourcesController.RESOURCES + "/list/" + instance.getId() + "/datapoints")
              .content(data.toString()).contentType(this.contentType))
          .andExpect(status().isOk());
      this.mockMvc.perform(
          post(CloudosResourcesController.RESOURCES + "/list/" + instance.getId() + "/datapoints")
              .content(data.toString()).contentType(this.contentType))
          .andExpect(status().isOk());
    }
  }

  @Test
  public void listRegionTest() throws Exception {
    mockMvc.perform(get(CloudosResourcesController.RESOURCES + "/list/1234/region"))
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void filterResourceTest() throws Exception {
    mockMvc.perform(get(CloudosResourcesController.RESOURCES + "/list/1234/resource"))
        .andExpect(status().is4xxClientError());
  }



  @Test
  public void getResourceLogTest() throws Exception {
    mockMvc.perform(get(CloudosResourcesController.RESOURCES + "/log/1234"))
        .andExpect(status().is5xxServerError());
  }

  @Test
  public void createResourceAlertTest() throws Exception {
    mockMvc.perform(get(CloudosResourcesController.RESOURCES + "/alert/1234/create"))
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void listResourceAlertTest() throws Exception {
    mockMvc.perform(get(CloudosResourcesController.RESOURCES + "/alert/1234/list"))
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void getCloudCostTest() throws Exception {
    mockMvc.perform(get(CloudosResourcesController.RESOURCES + "/cost/1234/cloud"))
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void getResourceCostTest() throws Exception {
    mockMvc.perform(get(CloudosResourcesController.RESOURCES + "/cost/1234/resource"))
        .andExpect(status().is5xxServerError());
  }

  @Test
  public void createResourcePropertiesTest() throws Exception {
    mockMvc.perform(get(CloudosResourcesController.RESOURCES + "/properties/1234/create"))
        .andExpect(status().is4xxClientError());
  }

  @Test
  @Ignore
  public void getResourceById() throws Exception {
    Instance instanceFilter = listResourcesCache.stream()
        .filter(instance -> Providers.AMAZON_AWS.equals(instance.getProvider())
            || Providers.GOOGLE_COMPUTE_ENGINE.equals(instance.getProvider()))
        .findAny().orElse(null);
    MvcResult mvcResult = this.mockMvc
        .perform(get(CloudosResourcesController.RESOURCES + "/list/" + instanceFilter.getId()))
        .andExpect(status().isOk()).andReturn();

    objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);

    Instance responseData = objectMapper.readValue(mvcResult.getResponse().getContentAsString(),
        new TypeReference<Instance>() {});

    // response JSON payload
    log.info(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseData));
    assertNotNull(responseData);
  }

  @Test
  public void getResourceByResourceIdIsEmpty() throws Exception {
    this.mockMvc.perform(get(CloudosResourcesController.RESOURCES + "/list/ "))
        .andExpect(status().is4xxClientError()).andReturn();
  }
  
  @Test
  public void getResourceByResourceIdNotExists() throws Exception {
    this.mockMvc.perform(get(CloudosResourcesController.RESOURCES + "/list/resourcesNotExitsId"))
        .andExpect(status().is4xxClientError()).andReturn();
  }
  
  @Test
  @Ignore
  public void getResourceByResourceIdWithoutProvider() throws Exception {
    Instance instanceFilter = listResourcesCache.stream()
        .filter(instance -> (!Providers.AMAZON_AWS.equals(instance.getProvider())
            && !Providers.GOOGLE_COMPUTE_ENGINE.equals(instance.getProvider())))
        .findAny().orElse(null);
    this.mockMvc.perform(get(CloudosResourcesController.RESOURCES + "/list/" + instanceFilter.getId()))
        .andExpect(status().is5xxServerError()).andReturn();
  }

  @Test
  @Ignore
  public void getResourceByResourceIdException() throws Exception {
    Instance instanceFilter = listResourcesCache.stream()
        .filter(instance -> (!Providers.AMAZON_AWS.equals(instance.getProvider())
            && !Providers.GOOGLE_COMPUTE_ENGINE.equals(instance.getProvider())))
        .findAny().orElse(null);
    InstanceService instanceServiceMock = mock(InstanceService.class);
    Mockito.doThrow(new IllegalArgumentException("Erro")).when(instanceServiceMock).findById(instanceFilter.getId());
    this.mockMvc.perform(get(CloudosResourcesController.RESOURCES + "/list/" + instanceFilter.getId()))
        .andExpect(status().is5xxServerError()).andReturn();
  }

  /**
   * Method that mock the data to create a list of instances.
   */
  private void initializeDataTest() {
    DataFactory df = new DataFactory();
    Date minDate = df.getDate(2018, 1, 1);
    Date maxDate = new Date();

    Map<Providers, List<ProviderRegion>> regions = providersService.retrieveRegions();
    Map<Providers, List<ProviderMachineType>> machineTypes = providersService.retrieveMachineTypes();
    List<Providers> providers = providersService.retrieveSupportedProviders();
    for (int n = 0; n < 18; n = n + 1) {
      Providers provider = df.getItem(providers);
      Instance testData = Instance.builder().provider(provider)
          .providerId(df.getAlphaNumericRandom()).creationDate(df.getDateBetween(minDate, maxDate))
          .includedDate(df.getDate(minDate, 1, 10)).region(df.getRegion(provider, regions.get(provider)))
          .machineType(df.getMachineType(provider, machineTypes.get(provider)))
          .name(df.getInstanceName("test", DataFactory.INSTANCE, DataFactory.HYPHEN))
          .status(df.getItem(InstanceStatus.values()))
          .clientStatus(df.getItem(ClientStatus.values())).cost(df.getDoubleBetween(10.0, 1000.5))
          .deleted(false).build();
      instanceService.save(testData);
      listResourcesCache.add(testData);
      if (testData.getProvider().equals(Providers.AMAZON_AWS)) {
        AmazonInstance amazonInstance = AmazonInstance.builder()
            .productFamily(testData.getMachineType()).build();
        amazonInstanceRepository.insert(amazonInstance);
      }
      if (testData.getProvider().equals(Providers.GOOGLE_COMPUTE_ENGINE)) {
        GoogleInstance googleInstance = GoogleInstance.builder()
            .description(
                df.getInstanceName(testData.getName(), DataFactory.DESCRIPTION, DataFactory.HYPHEN))
            .key(null).machineType(testData.getMachineType()).instance(testData).build();
        googleInstanceRepository.insert(googleInstance);
      }
    }
  }

  /**
   * Method that clean test data
   */
  private void clearResource(Iterable<Instance> listResource) {
    for (Instance instance : listResource) {
      instanceService.delete(instance.getId());
    }
    listResourcesCache = new ArrayList<>();
  }

  protected String json(Object o) throws IOException {
    return mapper.writeValueAsString(o);
  }

}