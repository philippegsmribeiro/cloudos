package cloudos.pricing;

import cloudos.Providers;
import cloudos.exceptions.NotImplementedException;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import test.CloudOSTest;
import test.HighMemoryTest;

@Category(HighMemoryTest.class)
public class PricingManagementServiceTest extends CloudOSTest {

  @Autowired
  PricingManagementService pricingManagementService;

  @Test
  public void testGoogle() throws NotImplementedException, PricingException {
    pricingManagementService.updatePricingForProvider(Providers.GOOGLE_COMPUTE_ENGINE);
  }

  @Test
  public void testAws() throws Exception {
    pricingManagementService.updatePricingForProvider(Providers.AMAZON_AWS);
  }
}
