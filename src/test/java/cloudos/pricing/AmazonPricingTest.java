package cloudos.pricing;

import cloudos.Providers;
import cloudos.amazon.AmazonInstanceRepository;
import cloudos.instances.InstanceService;
import cloudos.models.Instance;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import test.HighMemoryTest;

@Category(HighMemoryTest.class)
public class AmazonPricingTest extends AbstractPricingTest {

  @Autowired
  AmazonPricingManagement pricingManagement;
  @Autowired
  InstanceService instanceService;
  @Autowired
  AmazonInstanceRepository amazonInstanceRepository;

  @Test
  public void testReachingFile() throws Exception {
    String actualPricingVersion = pricingManagement.getActualPricingVersion(PricingType.INSTANCE);
    Assert.assertNotNull(actualPricingVersion);
  }

  @Test
  public void testLoadingPricing() throws Exception {
    if (hasPricingVersionChanged(Providers.AMAZON_AWS, PricingType.INSTANCE)) {
      pricingManagement.loadPricingData(PricingType.INSTANCE);
    }
  }

  @Test
  @SuppressWarnings("Duplicates")
  public void testGetPricingForInstances() throws Exception {
    List<Instance> instances =
        instanceService.findAllActiveInstancesByProvider(Providers.AMAZON_AWS);
    if (instances == null) {
      return;
    }
    for (Instance instance : instances) {
      PricingValue pricing = pricingManagement.getPricingForInstance(instance);
      if (pricing == null)
        continue;

      logger.debug("Instance: {}", instance);
      logger.debug("Pricing: {}", pricing);
      Assert.assertNotNull(pricing);
      Assert.assertNotNull(pricing.getValue());
    }
  }

  @After
  public void cleanUpMongo() {
    super.cleanUpMongo(Providers.AMAZON_AWS, PricingType.INSTANCE);
  }
}