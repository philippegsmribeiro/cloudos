package cloudos.pricing;

import cloudos.Providers;
import cloudos.google.GoogleInstanceRepository;
import cloudos.instances.InstanceService;
import cloudos.models.Instance;

import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class GooglePricingTest extends AbstractPricingTest {

  @Autowired
  GooglePricingManagement pricingManagement;
  @Autowired
  InstanceService instanceService;
  @Autowired
  GoogleInstanceRepository googleInstanceRepository;

  @Test
  public void testReachingFile() throws Exception {
    String actualPricingVersion = pricingManagement.getActualPricingVersion(PricingType.INSTANCE);
    Assert.assertNotNull(actualPricingVersion);
  }

  @Test
  public void testLoadingPricing() throws Exception {
    if (hasPricingVersionChanged(Providers.GOOGLE_COMPUTE_ENGINE, PricingType.INSTANCE)) {
      pricingManagement.loadPricingData(PricingType.INSTANCE);
    }
  }

  @Test
  public void testGetPricingForInstances() throws Exception {
    List<Instance> instances =
        instanceService.findAllActiveInstancesByProvider(Providers.GOOGLE_COMPUTE_ENGINE);
    for (Instance instance : instances) {
      PricingValue pricing = pricingManagement.getPricingForInstance(instance);
      logger.debug("Instance: {}", instance);
      logger.debug("Pricing: {}", pricing);
      Assert.assertNotNull(pricing);
      Assert.assertNotNull(pricing.getValue());
    }
  }

  @After
  public void cleanUpMongo() {
    cleanUpMongo(Providers.GOOGLE_COMPUTE_ENGINE, PricingType.INSTANCE);
  }
}
