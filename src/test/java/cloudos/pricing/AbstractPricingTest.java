package cloudos.pricing;

import cloudos.Providers;
import cloudos.exceptions.NotImplementedException;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

public abstract class AbstractPricingTest extends CloudOSTest {

  @Autowired
  PricingVersionRepository pricingVersionRepository;
  @Autowired
  PricingManagementService pricingManagementService;

  protected boolean hasPricingVersionChanged(Providers provider, PricingType pricingType)
      throws PricingException, NotImplementedException {
    return pricingManagementService.hasPricingVersionChanged(provider, pricingType);
  }

  protected void cleanUpMongo(Providers provider, PricingType pricingType) {
    List<PricingVersion> versions = pricingVersionRepository
        .findByProviderAndType(provider, pricingType);
    if (CollectionUtils.isNotEmpty(versions)) {
      // get the latest
      List<PricingVersion> prices = versions.stream()
          .sorted((a, b) -> b.getVersion().compareTo(a.getVersion())).collect(Collectors.toList());
      // compare
      prices.remove(0);
      pricingVersionRepository.delete(prices);
    }
  }
}
