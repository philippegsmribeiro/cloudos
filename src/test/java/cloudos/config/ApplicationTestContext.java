package cloudos.config;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * Test application context, used for loading test components not being loaded by {@link
 * cloudos.Application}.
 */
@TestConfiguration
@ComponentScan("cloudos")
public class ApplicationTestContext {


}
