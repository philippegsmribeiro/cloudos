package cloudos.migration;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import cloudos.Providers;
import cloudos.deploy.deployers.AnsibleService;
import cloudos.deploy.deployers.AnsibleServiceTest;
import cloudos.deploy.deployers.Deployers;
import cloudos.deploy.deployments.Deployment;
import cloudos.deploy.deployments.DeploymentStatus;
import cloudos.instances.CloudManagerService;
import cloudos.keys.AmazonKeyManagement;
import cloudos.models.AbstractInstance;
import cloudos.models.ActionType;
import cloudos.models.CloudActionRequest;
import cloudos.models.CloudCreateRequest;
import cloudos.provider.AmazonProviderManagement;
import cloudos.utils.FileUtils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.CloudOSTest;

@Ignore
public class MigrationTest extends CloudOSTest {

  @Autowired
  private AnsibleService ansibleService;
  @Autowired
  private CloudManagerService cloudManagerService;
  @Autowired
  private MigrationService migrationService;
  @Autowired
  AmazonProviderManagement amazonProviderManagement;
  @Autowired
  AmazonKeyManagement amazonKeyManagement;

  private String zoneGoogle = "us-west1-a";
  private String regionGoogle = "us-west1";

  private String zoneAws = "us-east-1b";
  private String regionAws = "us-east-1";

  private static final String awskey = "migrationTest";

  private List<? extends AbstractInstance> getInstanceListForTest(Providers providers,
      String instanceName) throws Exception {
    CloudCreateRequest request = null;
    if (providers == Providers.GOOGLE_COMPUTE_ENGINE) {
      String machineTypeLowGoogle = "f1-micro";
      String imageP = "ubuntu-os-cloud";
      String imageI = "ubuntu-1604-xenial-v20161221";
      request = CloudCreateRequest.createGCloudRequest(regionGoogle, instanceName,
          machineTypeLowGoogle, imageI, imageP, 1, "gcloudsshkey", zoneGoogle);
    } else if (providers == Providers.AMAZON_AWS) {
      String machineType = "t2.micro";
      String image = "ami-f4cc1de2";
      request = CloudCreateRequest.createAwsRequest(regionAws, machineType, image, "default", 1, 1,
          awskey, zoneAws, instanceName);
    }
    return cloudManagerService.createInstances(request);
  }

  @Test
  public void testMigrationGoogle() throws Exception {
    this.testMigration(Providers.GOOGLE_COMPUTE_ENGINE);
  }

  @Test
  public void testMigrationAws() throws Exception {
    amazonProviderManagement.syncRegions();
    if (amazonKeyManagement.verifyKeyExists(awskey, regionAws)) {
      amazonKeyManagement.deleteKey(awskey, regionAws);
    }
    amazonKeyManagement.generateKey(awskey, regionAws);
    //this.testSimple(Providers.AMAZON_AWS);
    this.testMigration(Providers.AMAZON_AWS);
  }


  public void testSimple(Providers providers) throws Exception {
    String instanceName = "test-migration-" + generator.generate(2);
    String clonedName = "";
    try {
      // create instances
      List<? extends AbstractInstance> instances = getInstanceListForTest(providers, instanceName);

      // create snapshot
      AbstractInstance cloneInstance = migrationService.cloneInstance(instances.get(0));
      assertNotNull(cloneInstance);
      clonedName = cloneInstance.getInstance().getProviderId();
    } catch (Exception e) {
      logger.error(e.getLocalizedMessage(), e);
      fail(e.getLocalizedMessage());
    } finally {
      try {
        this.deleteInstance(providers, clonedName, instanceName);
      } catch (Exception e2) {
        logger.error(e2.getLocalizedMessage(), e2);
      }
    }

  }

  public void testMigration(Providers providers) throws Exception {
    String instanceName = "test-migration-" + generator.generate(2);
    String clonedName = "";
    try {
      // create instances
      List<? extends AbstractInstance> instances = getInstanceListForTest(providers, instanceName);

      // do a deploy of nginx
      Deployment deployment = new Deployment();
      deployment.setConfigFilePath(AnsibleServiceTest.class.getResource("nginx.yml").getFile());
      deployment.setInstancesNumber(instances.size());
      deployment.setInstances(instances);
      deployment.setDeployer(Deployers.ANSIBLE);
      deployment.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
      deployment = ansibleService.deploy(deployment);
      assertTrue(DeploymentStatus.FINISHED.equals(deployment.getStatus()));

      // see if the nginx is working
      AbstractInstance abstractInstance = deployment.getInstances().get(0);
      assertNotNull(abstractInstance);
      validateNginx(abstractInstance);

      // create snapshot
      AbstractInstance cloneInstance = migrationService.cloneInstance(abstractInstance);
      assertNotNull(cloneInstance);
      clonedName = cloneInstance.getInstance().getProviderId();

      Thread.sleep(30 * 1000);
      // see if the nginx is working
      validateNginx(cloneInstance);

    } catch (Exception e) {
      logger.error(e.getLocalizedMessage(), e);
      fail(e.getLocalizedMessage());
    } finally {
      try {
        this.deleteInstance(providers, clonedName, instanceName);
      } catch (Exception e2) {
        logger.error(e2.getLocalizedMessage(), e2);
      }
    }
  }

  private void validateNginx(AbstractInstance abstractInstance) throws Exception {
    String ip = abstractInstance.getInstance().getIpAddress();
    String content = readUrlContentWithTimeout(String.format("http://%s", ip), 30 * 1000);
    assertNotNull(content);
    logger.debug(content);
    assertTrue(content.contains("Welcome to nginx!"));
  }

  private String readUrlContentWithTimeout(String url, long timeout) throws InterruptedException {
    long start = System.currentTimeMillis();
    final long pollInterval = 2 * 1000;
    String result = null;
    while (result == null) {
      logger.debug("reading!");
      long elapsed = System.currentTimeMillis() - start;
      if (elapsed >= timeout) {
        throw new InterruptedException("Timed out waiting for operation to complete");
      }
      try {
        String content = FileUtils.readUrlContent(url);
        logger.debug(content);
        if (StringUtils.isNotEmpty(content)) {
          return content;
        }
      } catch (Exception e) {
        logger.debug(e.getLocalizedMessage());
      }
      Thread.sleep(pollInterval);
    }
    return result;
  }



  private void deleteInstance(Providers providers, String... instanceName) throws Exception {
    CloudActionRequest request = null;
    if (providers == Providers.GOOGLE_COMPUTE_ENGINE) {
      request = CloudActionRequest.createGCloudRequest(Arrays.asList(instanceName).stream()
          .filter(i -> StringUtils.isNotBlank(i)).collect(Collectors.toList()), regionGoogle,
          zoneGoogle, ActionType.TERMINATE);
    } else if (providers == Providers.AMAZON_AWS) {
      request = CloudActionRequest.createAwsRequest(Arrays.asList(instanceName).stream()
          .filter(i -> StringUtils.isNotBlank(i)).collect(Collectors.toList()), regionAws, zoneAws,
          "default", ActionType.TERMINATE);
    }
    request.setSynchronous(false);
    request.setFireNotification(false);
    cloudManagerService.terminateInstances(request);
  }
}
