package cloudos;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import test.CloudOSTest;

public class EmailControllerTest extends CloudOSTest {

  private MockMvc mvc;

  @Before
  public void setUp() throws Exception {
    this.mvc = MockMvcBuilders.standaloneSetup(new EmailController()).build();
  }

  @Test
  @Ignore
  public void testEmailController() throws Exception {
    this.mvc.perform(get("/email")).andExpect(status().isOk()).andExpect(view().name("email"));
  }

  @Test
  @Ignore
  public void testEmailComposer() throws Exception {
    this.mvc
        .perform(get("/email_compose"))
        .andExpect(status().isOk())
        .andExpect(view().name("email_compose"));
  }
}
