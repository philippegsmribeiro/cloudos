package cloudos;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import cloudos.encryptionKeys.AmazonKMSServiceException;
import cloudos.exceptions.InvalidRequestException;
import cloudos.user.UserProfile;
import cloudos.user.UserRequest;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import cloudos.credentials.CloudCredentialActionRequest;
import cloudos.models.AwsCredential;
import cloudos.models.CloudosCredentialRepository;
import cloudos.user.Invite;
import cloudos.user.InviteRepository;
import cloudos.user.InviteRequest;
import cloudos.user.User;
import cloudos.user.UserRole;
import cloudos.user.UserService;
import test.CloudOSTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SettingsControllerTest extends CloudOSTest {

  private MockMvc mvc;

  @Value("${aws_access_key_id}")
  private String awsAccessKey;

  @Value("${aws_secret_key}")
  private String awsSecretKey;

  @Autowired
  private UserService userService;

  @Autowired
  private CloudosCredentialRepository cloudosCredentialRepository;

  @Autowired
  private CredentialManagerService credentialManagerService;

  @Autowired
  private SettingsController settingsController;

  @Autowired
  private InviteRepository inviteRepository;

  private static String email = "testdev@cloudtownai.com";

  private static String tenant = "symantec";

  private static String guest = "Philippe";

  @Before
  public void setUp() throws Exception {
    this.mvc = MockMvcBuilders.standaloneSetup(settingsController).build();
  }

  @Test
  public void hasCredentials() throws Exception {


    this.cloudosCredentialRepository.deleteAll();
    MvcResult result = this.mvc.perform(get(SettingsController.SETTINGS + "/has_credentials"))
        .andExpect(status().isOk()).andReturn();

    String content = result.getResponse().getContentAsString();
    assert !Boolean.valueOf(content);

    AwsCredential awsCredentials = new AwsCredential(awsAccessKey, awsSecretKey);
    CloudCredentialActionRequest cloudCreateCredentialRequest = new CloudCredentialActionRequest();
    cloudCreateCredentialRequest.setCloudosCredential(awsCredentials);
    credentialManagerService.save(cloudCreateCredentialRequest);

    result = this.mvc.perform(get(SettingsController.SETTINGS + "/has_credentials"))
        .andExpect(status().isOk()).andReturn();

    content = result.getResponse().getContentAsString();
    assert Boolean.valueOf(content);

    this.cloudosCredentialRepository.deleteAll();
    result = this.mvc.perform(get(SettingsController.SETTINGS + "/has_credentials"))
        .andExpect(status().isOk()).andReturn();

    content = result.getResponse().getContentAsString();
    assert !Boolean.valueOf(content);


  }

  @Test
  public void isUserFirstAccess() throws Exception {
    
    User user = new User();
    user.setEmail(generator.generate(10));
    user.setUsername(generator.generate(10));
    user.setPassword(generator.generate(10));
    user.setRole(UserRole.ADMIN);    

    userService.save(user);
    
    MvcResult result = this.mvc.perform(get(SettingsController.SETTINGS + "/is_user_first_access/"+user.getUsername()))
    .andExpect(status().isOk()).andReturn();
    
    String content = result.getResponse().getContentAsString();
    assert Boolean.valueOf(content);
    
    
    result = this.mvc.perform(get(SettingsController.SETTINGS + "/is_user_first_access/"+user.getUsername()))
    .andExpect(status().isOk()).andReturn();
    
    content = result.getResponse().getContentAsString();
    assert !Boolean.valueOf(content);
    
  }

  @Test
  public void testCreateUser() throws Exception {
    User user = this.createUser();
    assertNotNull(user);

    Invite invite = this.createInvite();
    assertNotNull(invite);

    UserRequest request = UserRequest.builder()
                              .invitationId(invite.getId())
                              .user(user)
                              .build();

    this.mvc
        .perform(
            post(SettingsController.SETTINGS  + "/user")
                .content(mapper.writeValueAsString(request)).contentType(contentType))
        .andExpect(status().isOk())
        .andReturn();

    // create a request without invite
    request = UserRequest.builder()
                  .user(user)
                  .build();

    this.mvc
        .perform(
            post(SettingsController.SETTINGS  + "/user")
                .content(mapper.writeValueAsString(request)).contentType(contentType))
        .andExpect(status().is5xxServerError());

  }

  @Test
  public void testEditUser() throws Exception {
    User user = this.createUser();
    assertNotNull(user);

    UserProfile profile = new UserProfile();
    profile.setCity("San Francisco");
    profile.setCountry("United States");
    profile.setFirstName(generator.generate(8));
    profile.setLastName(generator.generate(8));
    profile.setTelephone(generator.generate(8));

    user.setProfile(profile);

    // create a request without invite
    UserRequest request = UserRequest.builder()
        .user(user)
        .build();

    this.mvc
        .perform(
            put(SettingsController.SETTINGS  + "/user/" + user.getId())
                .content(mapper.writeValueAsString(request)).contentType(contentType))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.profile.city", is(profile.getCity())))
        .andExpect(jsonPath("$.profile.country", is(profile.getCountry())))
        .andReturn();


    // try to update a profile that doesn't exist
    this.mvc
        .perform(
            put(SettingsController.SETTINGS  + "/user/" + "invalid"))
        .andExpect(status().is4xxClientError());

  }

  @Test
  public void testListUsers() throws Exception {
    this.mvc
        .perform(
            get(SettingsController.SETTINGS  + "/user"))
        .andExpect(status().isOk());
  }

  @Test
  public void testDescribeUser() throws Exception {
    User user = this.createUser();
    assertNotNull(user);

    this.mvc
        .perform(
            get(SettingsController.SETTINGS  + "/user/" + user.getId()))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.role", is(user.getRole().toString())))
        .andReturn();

    // add an invalid id
    this.mvc
        .perform(
            get(SettingsController.SETTINGS  + "/user/" + "invalid"))
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testDeleteUser() throws Exception {
    User user = this.createUser();
    assertNotNull(user);

    this.mvc
        .perform(
            delete(SettingsController.SETTINGS  + "/user/" + user.getId()))
        .andExpect(status().isOk());

    // add an invalid id
    this.mvc
        .perform(
            delete(SettingsController.SETTINGS  + "/user/" + "invalid"))
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testInviteUser() throws Exception {
    InviteRequest request = InviteRequest.builder()
                                  .email(email)
                                  .guest(guest)
                                  .tenant(tenant)
                                  .build();

    this.mvc
        .perform(
            post(SettingsController.SETTINGS  + "/user/invite")
                .content(mapper.writeValueAsString(request)).contentType(contentType))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.tenant", is(tenant)))
        .andExpect(jsonPath("$.guest", is(guest)))
        .andReturn();
  }

  @Test
  public void testListInvites() throws Exception {
    this.mvc
        .perform(
            get(SettingsController.SETTINGS  + "/user/invite"))
        .andExpect(status().isOk());
  }

  @Test
  public void testDescribeInvite() throws Exception {
    Invite invite = this.createInvite();
    assertNotNull(invite);

    this.mvc
        .perform(
            get(SettingsController.SETTINGS  + "/user/invite/" + invite.getId()))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.tenant", is(tenant)))
        .andExpect(jsonPath("$.guest", is(guest)))
        .andReturn();

    // try with an invalid id
    this.mvc
        .perform(
            get(SettingsController.SETTINGS  + "/user/invite/" + "invalidId"))
        .andExpect(status().is5xxServerError());
  }

  @Test
  public void testDeleteInvite() throws Exception {
    Invite invite = this.createInvite();
    assertNotNull(invite);

    this.mvc
        .perform(
            delete(SettingsController.SETTINGS  + "/user/invite/" + invite.getId()))
        .andExpect(status().isOk());

    // try with an invalid id
    this.mvc
        .perform(
            delete(SettingsController.SETTINGS  + "/user/invite/" + "invalidId"))
        .andExpect(status().is4xxClientError());

  }

  @Test
  public void testUpdateInvite() throws Exception {
    Invite invite = this.createInvite();
    assertNotNull(invite);

    InviteRequest request = InviteRequest.builder()
        .email(email)
        .guest(guest)
        .tenant(tenant)
        .build();

    this.mvc
        .perform(
            put(SettingsController.SETTINGS  + "/user/invite/" + invite.getId())
                .content(mapper.writeValueAsString(request)).contentType(contentType))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.tenant", is(tenant)))
        .andExpect(jsonPath("$.guest", is(guest)))
        .andReturn();

    // try with an invalid id
    this.mvc
        .perform(
            put(SettingsController.SETTINGS  + "/user/invite/" + "invalidId"))
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testAcceptInvite() throws Exception {
    // create a valid invitation
    Invite invite = this.createInvite();
    assertNotNull(invite);

    // perform the acceptance for the invitation
    this.mvc
        .perform(
            get(SettingsController.SETTINGS  + "/user/invite/" + invite.getId()))
        .andExpect(status().isOk());

    // delete the invitation from the collection
    this.inviteRepository.delete(invite);
    // should return an error
    this.mvc
        .perform(
            get(SettingsController.SETTINGS  + "/user/invite/" + invite.getId()))
        .andExpect(status().is5xxServerError());

  }

  private User createUser() {
    User user = User.builder()
                    .email(generator.generate(8))
                    .firstAccess(false)
                    .password(generator.generate(8))
                    .role(UserRole.USER)
                    .username(generator.generate(8))
                    .userProfile(new UserProfile())
                    .build();

    try {
      return this.userService.save(user);
    } catch (AmazonKMSServiceException | InvalidRequestException e) {
      logger.error(e.getMessage());
    }
    return null;
  }

  private Invite createInvite() {
    Date now = new Date();
    Date myNewDate = DateUtils.addDays(now, 4);
    Invite invite = Invite.builder()
        .email(generator.generate(8))
        .invitationDate(now)
        .tenant(tenant)
        .guest(guest)
        .isPending(true)
        .expirationDate(myNewDate)
        .build();

    return this.inviteRepository.save(invite);

  }
}
