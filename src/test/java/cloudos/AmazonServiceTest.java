package cloudos;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import cloudos.models.AwsKeyPair;
import cloudos.models.AwsKeyPairRepository;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.SecurityGroup;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

/** Created by philipperibeiro on 12/16/16. */
public class AmazonServiceTest extends CloudOSTest {

  @Autowired private AmazonService amazonService;

  @Autowired private AwsKeyPairRepository awsKeyPairRepository;

  private static String groupname =
      String.format("cloudos_test_%s", generator.generate(5));

  private static String securityGroup = "default";

  @Before
  public void setUp() throws Exception {
    this.awsKeyPairRepository.deleteAll();
  }

  @Test
  @Ignore
  public void testCreateInstance() throws Exception {
    List<Instance> instances =
        this.amazonService.createInstances(
            "ami-59e8964e",
            "m1.small",
            1,
            1,
            "test_deploy",
            "default",
            "us-east-1",
            null,
            null,
            null, null);
    assertNotNull(instances);
    assertNotEquals(instances.size(), 0);
    for (Instance instance : instances) {
      logger.debug(String.format("#### Created instance '%s' #####", instance));
    }
    List<String> instanceIds =
        instances.stream().map(instance -> instance.getInstanceId()).collect(Collectors.toList());
    boolean status = this.amazonService.terminateInstances(instanceIds, "us-east-1", "default");
    assertTrue(status);
  }

  @Test
  public void testKeyPair() throws Exception {
    // create a key that does not exist
    AwsKeyPair keyPair =
        this.amazonService.createKeyPair(
            "us-west-1", generator.generate(10));
    assertNotNull(keyPair);

    AwsKeyPair keyPair2 =
        this.amazonService.createKeyPair(
            "us-east-1", generator.generate(10));
    assertNotNull(keyPair2);
    List<AwsKeyPair> keyPairList = this.amazonService.listKeyPairs("us-east-1");
    assertNotNull(keyPairList);
    assertNotEquals(keyPairList.size(), 0);

    AwsKeyPair other = this.amazonService.getKeyPair(keyPair.getRegion(), keyPair.getKeyname());
    assertEquals(other.getKey(), keyPair.getKey());

    // delete all the keys
    for (AwsKeyPair key : keyPairList) {
      assertTrue(this.amazonService.deleteKeyPair(key));
    }

    assertTrue(this.amazonService.deleteKeyPair(keyPair));
  }

  @Test
  public void testListSecurityGroup() throws Exception {
    String region = "us-east-1";
    List<SecurityGroup> groups = this.amazonService.listSecurityGroups(region, securityGroup);
    logger.debug("############ List Security Groups #############");
    logger.debug(groups);
    logger.debug("###############################################");
    assertNotNull(groups);
    assertThat("groups", groups.size(), greaterThanOrEqualTo(0));
  }

  @Test
  public void testCreateSecurityGroup() throws Exception {
    String region = "us-east-1";
    String description = "This is a test security group";
    String groupId =
        this.amazonService.createSecurityGroup(securityGroup, region, groupname, description);
    logger.debug("############ Created Security Group: #############");
    logger.debug(groupId);
    logger.debug("###############################################");
    assertNotNull(groupId);
  }

  @Test
  @Ignore
  public void testDeleteSecurityGroup() throws Exception {
    String region = "us-east-1";
    assertTrue(this.amazonService.deleteSecurityGroup(region, securityGroup, groupname));
  }

  @Test
  public void listSecurityGroups() {
    String region = "us-east-1";
    List<SecurityGroup> groups = this.amazonService.listSecurityGroups(region);
    assertNotNull(groups);
    assertThat("groups", groups.size(), greaterThanOrEqualTo(0));
  }

  @Test
  public void testDescribeSecurityGroup() {
    String region = "us-east-1";
    List<SecurityGroup> groups = this.amazonService.listSecurityGroups(region);
    assertNotNull(groups);
    if (groups.size() > 0) {
      SecurityGroup securityGroup = groups.get(0);
      SecurityGroup another = this.amazonService.describeSecurityGroup(region,
          securityGroup.getGroupId());

      assertNotNull(another);
      assertEquals(securityGroup.getGroupId(), another.getGroupId());
      assertEquals(securityGroup.getVpcId(), another.getVpcId());
      assertEquals(securityGroup.getGroupName(), another.getGroupName());
      assertEquals(securityGroup.getDescription(), another.getDescription());
    }
  }

  @Test
  public void testGetClient() throws Exception {

    String usWest1 = "us-west-1";
    String usEast1 = "us-east-1";
    AmazonEC2 westClient = this.amazonService.getClient(usWest1);

    logger.debug("#################################################");
    logger.debug(westClient);
    logger.debug("#################################################");
    assertNotNull(westClient);
    AmazonEC2 amazonEC2 = this.amazonService.getClient(usWest1);
    logger.debug("#################################################");
    logger.debug(amazonEC2);
    logger.debug("#################################################");
    AmazonEC2 eastClient = this.amazonService.getClient(usEast1);
    assertNotNull(eastClient);
  }
}
