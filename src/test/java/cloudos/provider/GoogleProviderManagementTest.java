package cloudos.provider;

import cloudos.Providers;
import cloudos.provider.GoogleProviderManagement;
import cloudos.provider.ProviderManagement;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import test.SlowTest;

@Category(SlowTest.class)
public class GoogleProviderManagementTest extends AbstractProviderManagementTest {

  @Autowired private GoogleProviderManagement googleResourcesManagement;

  @Override
  protected ProviderManagement getResourcesManagement() {
    return googleResourcesManagement;
  }

  @Override
  protected Providers getProvider() {
    return Providers.GOOGLE_COMPUTE_ENGINE;
  }
}
