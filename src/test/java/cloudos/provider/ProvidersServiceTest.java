package cloudos.provider;

import static org.assertj.core.api.Assertions.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import cloudos.Providers;
import cloudos.exceptions.NotImplementedException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import test.CloudOSTest;
import test.SlowTest;

@Category(SlowTest.class)
public class ProvidersServiceTest extends CloudOSTest {

  @Autowired
  private ProvidersService providersService;
  @Autowired
  private ProviderMachineTypeRepository providerMachineTypeRepository;

  @Before
  public void setup() throws Exception {
    if (providerMachineTypeRepository.count() == 0) {
      importJSON("provider_machine_type", "provider_machine_type.json");
    }
  }

  List<Providers> providers =
      Arrays.asList(Providers.AMAZON_AWS, Providers.GOOGLE_COMPUTE_ENGINE);

  @Test
  public void retrieveRegionsTest() {
    Map<Providers, List<ProviderRegion>> map = providersService.retrieveRegions();
    assertNotNull(map);
    assertTrue(map.keySet().contains(Providers.AMAZON_AWS));
    assertTrue(map.get(Providers.AMAZON_AWS).size() > 0);
    assertTrue(map.keySet().contains(Providers.GOOGLE_COMPUTE_ENGINE));
    assertTrue(map.get(Providers.GOOGLE_COMPUTE_ENGINE).size() > 0);
  }

  @Test
  public void retrieveRegionsByProviderTest() {
    providers.forEach(p -> {
      try {
        List<ProviderRegion> list = providersService.retrieveRegions(p);
        assertNotNull(list);
        assertTrue(list.size() > 0);
        list.forEach(i -> assertEquals(i.getProvider(), p));
      } catch (Exception e) {
        fail("fail!", e);
      }
    });
  }

  @Test
  public void retrieveRegionsWithZonesTest() {
    Map<Providers, List<ProviderRegion>> map = providersService.retrieveRegionsWithZones();
    assertNotNull(map);
    assertTrue(map.keySet().contains(Providers.AMAZON_AWS));
    assertTrue(map.get(Providers.AMAZON_AWS).size() > 0);
    assertTrue(map.keySet().contains(Providers.GOOGLE_COMPUTE_ENGINE));
    assertTrue(map.get(Providers.GOOGLE_COMPUTE_ENGINE).size() > 0);
  }

  @Test
  public void retrieveRegionsWithZonesByProviderTest() {
    providers.forEach(p -> {
      try {
        List<ProviderRegion> list = providersService.retrieveRegionsWithZones(p);
        assertNotNull(list);
        assertTrue(list.size() > 0);
        list.forEach(i -> assertEquals(i.getProvider(), p));
      } catch (Exception e) {
        fail("fail!", e);
      }
    });
  }

  @Test
  public void retrieveImagesTest() {
    Map<Providers, List<ProviderImageType>> map = providersService.retrieveImages();
    assertNotNull(map);
    assertTrue(map.keySet().contains(Providers.AMAZON_AWS));
    assertTrue(map.get(Providers.AMAZON_AWS).size() > 0);
    assertTrue(map.keySet().contains(Providers.GOOGLE_COMPUTE_ENGINE));
    assertTrue(map.get(Providers.GOOGLE_COMPUTE_ENGINE).size() > 0);
  }

  @Test
  public void retrieveImagesByProviderTest() {
    providers.forEach(p -> {
      try {
        List<ProviderImageType> list = providersService.retrieveImages(p);
        assertNotNull(list);
        assertTrue(list.size() > 0);
        list.forEach(i -> assertEquals(i.getProvider(), p));
      } catch (Exception e) {
        fail("fail!", e);
      }
    });
  }

  @Test
  public void retrieveMachineTypesTest() {
    Map<Providers, List<ProviderMachineType>> map = providersService.retrieveMachineTypes();
    assertNotNull(map);
    assertTrue(map.keySet().contains(Providers.AMAZON_AWS));
    assertTrue(map.get(Providers.AMAZON_AWS).size() > 0);
    assertTrue(map.keySet().contains(Providers.GOOGLE_COMPUTE_ENGINE));
    assertTrue(map.get(Providers.GOOGLE_COMPUTE_ENGINE).size() > 0);
  }

  @Test
  public void retrieveMachineTypesByProviderTest() {
    providers.forEach(p -> {
      try {
        List<ProviderMachineType> list = providersService.retrieveMachineTypes(p);
        assertNotNull(list);
        assertTrue(list.size() > 0);
        list.forEach(i -> assertEquals(i.getProvider(), p));
      } catch (Exception e) {
        fail("fail!", e);
      }
    });
  }

  @Test
  public void retrieveKeysByProviderTest() {
    providers.forEach(p -> {
      try {
        List<ProviderKey> list = providersService.retrieveKeys(p);
        assertNotNull(list);
        assertTrue(list.size() > 0);
        list.forEach(i -> assertEquals(i.getProvider(), p));
      } catch (Exception e) {
        fail("fail!", e);
      }
    });
  }

  @Test
  public void retrieveSecurityGroupsByProviderTest() {
    providers.forEach(p -> {
      try {
        List<ProviderSecurityGroup> list = providersService.retrieveSecurityGroups(p);
        assertNotNull(list);
        assertTrue(list.size() > 0);
        list.forEach(i -> assertEquals(i.getProvider(), p));
      } catch (Exception e) {
        fail("fail!", e);
      }
    });
  }

  @Test
  public void retrieveMachineTypeTest() throws NotImplementedException {
    providersService.retrieveMachineType(Providers.AMAZON_AWS, "n1-highmem-8", "us-east-1");
    providersService.retrieveMachineType(Providers.AMAZON_AWS, "m4.4xlarge", "us-east-1");

    providersService.retrieveMachineType(Providers.GOOGLE_COMPUTE_ENGINE, "f1-micro", "us-west1");
    providersService.retrieveMachineType(Providers.GOOGLE_COMPUTE_ENGINE, "g1-small", "us-west1");
  }

  @Test
  public void reloadKeysTest() {
    //providersService.reloadKeys(provider, savedKey);
  }

  @Test
  public void retrieveSupportedProvidersTest() {
    List<Providers> retrieveSupportedProviders = providersService.retrieveSupportedProviders();
    assertNotNull(retrieveSupportedProviders);
    assertTrue(retrieveSupportedProviders.contains(Providers.AMAZON_AWS));
    assertTrue(retrieveSupportedProviders.contains(Providers.GOOGLE_COMPUTE_ENGINE));
  }
}
