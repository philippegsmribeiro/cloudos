package cloudos.provider;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;

import cloudos.Providers;
import cloudos.provider.ProviderMachineType;
import cloudos.provider.ProviderMachineTypeRepository;
import test.CloudOSTest;
import test.SlowTest;

@Category(SlowTest.class)
public class ProviderMachineTypeRepositoryTest extends CloudOSTest {

  @Autowired
  private ProviderMachineTypeRepository providerMachineTypeRepository;

  @Before
  public void setup() throws Exception {
    if(providerMachineTypeRepository.count() == 0) {
      importJSON("provider_machine_type", "provider_machine_type.json");
    }
  }

  @Test
  public void findOneMachineType() {
    List<ProviderMachineType> machineTypeList = providerMachineTypeRepository.findAll();
    assertNotNull(machineTypeList);
    List<ProviderMachineType> machineTypeList1 =
        providerMachineTypeRepository.findByProvider(Providers.AMAZON_AWS);
    assertNotNull(machineTypeList1);
    List<ProviderMachineType> machineTypeList2 = providerMachineTypeRepository
        .findByProviderAndRegion(Providers.AMAZON_AWS, "us-west-1");
    assertNotNull(machineTypeList2);
  }
}
