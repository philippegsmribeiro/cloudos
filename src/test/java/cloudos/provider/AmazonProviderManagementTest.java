package cloudos.provider;

import cloudos.Providers;
import cloudos.provider.AmazonProviderManagement;
import cloudos.provider.ProviderManagement;
import org.junit.Ignore;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import test.SlowTest;

@Ignore
@Category(SlowTest.class)
public class AmazonProviderManagementTest extends AbstractProviderManagementTest {

  @Autowired private AmazonProviderManagement amazonResourcesManagement;

  @Override
  protected ProviderManagement getResourcesManagement() {
    return amazonResourcesManagement;
  }

  @Override
  protected Providers getProvider() {
    return Providers.AMAZON_AWS;
  }
}
