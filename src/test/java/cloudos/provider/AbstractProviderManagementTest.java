package cloudos.provider;

import static org.junit.Assert.assertNotNull;
import cloudos.Providers;
import cloudos.provider.ProviderImageType;
import cloudos.provider.ProviderImageTypeRepository;
import cloudos.provider.ProviderKey;
import cloudos.provider.ProviderKeyRepository;
import cloudos.provider.ProviderMachineType;
import cloudos.provider.ProviderMachineTypeRepository;
import cloudos.provider.ProviderManagement;
import cloudos.provider.ProviderRegion;
import cloudos.provider.ProviderRegionRepository;
import cloudos.provider.ProviderSecurityGroup;
import cloudos.provider.ProviderSecurityGroupRepository;
import java.util.List;
import javax.annotation.Resource;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;
import test.CloudOSTest;

public abstract class AbstractProviderManagementTest extends CloudOSTest {

  protected abstract ProviderManagement getResourcesManagement();

  protected abstract Providers getProvider();

  @Resource
  ProviderRegionRepository regionRepository;
  @Resource
  ProviderImageTypeRepository imageTypeRepository;
  @Resource
  ProviderMachineTypeRepository machineTypeRepository;
  @Resource
  ProviderKeyRepository keyRepository;
  @Resource
  ProviderSecurityGroupRepository securityGroupRepository;

  @Test
  public void retrieveRegions() {
    List<ProviderRegion> findByProvider = regionRepository.findByProvider(getProvider());
    if (CollectionUtils.isNotEmpty(findByProvider)) {
      regionRepository.delete(findByProvider);
    }
    logger.debug("retrieveRegions");
    List<ProviderRegion> regions = getResourcesManagement().retrieveRegions();
    regions.forEach(t->logger.debug(t));
    assertNotNull(regions);
  }

  @Test
  public void retrieveRegionsWithZones() {
    List<ProviderRegion> findByProvider = regionRepository.findByProvider(getProvider());
    if (CollectionUtils.isNotEmpty(findByProvider)) {
      regionRepository.delete(findByProvider);
    }
    logger.debug("retrieveRegionsWithZones");
    List<ProviderRegion> regions = getResourcesManagement().retrieveRegionsWithZones();
    regions.forEach(t->logger.debug(t));
    assertNotNull(regions);
  }

  @Test
  public void retrieveImagesTypes() {
    List<ProviderImageType> findByProvider = imageTypeRepository.findByProvider(getProvider());
    if (CollectionUtils.isNotEmpty(findByProvider)) {
      imageTypeRepository.delete(findByProvider);
    }
    logger.debug("retrieveImagesTypes");
    List<ProviderImageType> imagesTypes = getResourcesManagement().retrieveImagesTypes();
    imagesTypes.forEach(t->logger.debug(t));
    assertNotNull(imagesTypes);
  }

  @Test
  public void retrieveMachineTypes() {
    List<ProviderMachineType> findByProvider = machineTypeRepository.findByProvider(getProvider());
    if (CollectionUtils.isNotEmpty(findByProvider)) {
      machineTypeRepository.delete(findByProvider);
    }
    logger.debug("retrieveMachineTypes");
    List<ProviderMachineType> machineTypes = getResourcesManagement().retrieveMachineTypes();
    machineTypes.forEach(t->logger.debug(t));
    assertNotNull(machineTypes);
  }

  @Test
  public void retrieveKeys() {
    List<ProviderKey> findByProvider = keyRepository.findByProvider(getProvider());
    if (CollectionUtils.isNotEmpty(findByProvider)) {
      keyRepository.delete(findByProvider);
    }
    logger.debug("retrieveKeys");
    List<ProviderKey> keys = getResourcesManagement().retrieveKeys();
    keys.forEach(t->logger.debug(t));
    assertNotNull(keys);
  }

  @Test
  public void retrieveSecurityGroups() {
    List<ProviderSecurityGroup> findByProvider =
        securityGroupRepository.findByProvider(getProvider());
    if (CollectionUtils.isNotEmpty(findByProvider)) {
      securityGroupRepository.delete(findByProvider);
    }
    logger.debug("retrieveSecurityGroups");
    List<ProviderSecurityGroup> securityGroups = getResourcesManagement().retrieveSecurityGroups();
    securityGroups.forEach(t->logger.debug(t));
    assertNotNull(securityGroups);
  }
}
