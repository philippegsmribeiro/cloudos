package cloudos.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import cloudos.encryptionKeys.AmazonKMSService;
import cloudos.encryptionKeys.AmazonKMSServiceException;
import cloudos.exceptions.InvalidRequestException;
import test.CloudOSTest;

public class UserServiceTest extends CloudOSTest {

  @Autowired
  private UserService userService;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private AmazonKMSService amazonKMSService;

  @Test
  public void updateUserProfile() throws InvalidRequestException, AmazonKMSServiceException {

    User user = new User();

    String email = generator.generate(10);
    user.setEmail(email);

    String username = generator.generate(10);
    user.setUsername(username);

    String oldPassword = generator.generate(10);
    user.setPassword(oldPassword);
    user.setProfile(createUserProfile());
    user.setRole(UserRole.ADMIN);

    userService.save(user);
    String encryptedUserName =
        amazonKMSService.encryptMessage(amazonKMSService.getUserEncryptedDataKey(), username);
    User savedUser = userRepository.findByUsername(encryptedUserName);

    assertEquals(user.getId(), savedUser.getId());

    UserProfile profile = user.getProfile();
    UserProfile savedProfile = savedUser.getProfile();
    assertNotEquals(profile.getFirstName(), savedProfile.getFirstName());
    assertNotEquals(profile.getLastName(), savedProfile.getLastName());
    assertNotEquals(profile.getTelephone(), savedProfile.getTelephone());

    assertEquals(amazonKMSService.encryptMessage(amazonKMSService.getUserEncryptedDataKey(),
        profile.getFirstName()), savedProfile.getFirstName());
    assertEquals(amazonKMSService.encryptMessage(amazonKMSService.getUserEncryptedDataKey(),
        profile.getLastName()), savedProfile.getLastName());
    assertEquals(amazonKMSService.encryptMessage(amazonKMSService.getUserEncryptedDataKey(),
        profile.getTelephone()), savedProfile.getTelephone());

    assertEquals(profile.getJobRole(), savedProfile.getJobRole());
    assertEquals(profile.getCountry(), savedProfile.getCountry());
    assertEquals(profile.getState(), savedProfile.getState());
    assertEquals(profile.getCity(), savedProfile.getCity());
    assertEquals(profile.getPicture(), savedProfile.getPicture());

    UserPreferences preferences = user.getProfile().getPreferences();
    UserPreferences savedPreferences = savedUser.getProfile().getPreferences();
    assertEquals(preferences.getDateFormat(), savedPreferences.getDateFormat());
    assertEquals(preferences.getTimeFormat(), savedPreferences.getTimeFormat());
    assertEquals(preferences.isAlertNotifications(), savedPreferences.isAlertNotifications());
    assertEquals(preferences.isEmailNotification(), savedPreferences.isEmailNotification());
    assertEquals(preferences.isUse24HourFormat(), savedPreferences.isUse24HourFormat());
    assertEquals(preferences.isUseSSL(), savedPreferences.isUseSSL());


    UserProfile newUserProfile = createUserProfile();
    userService.updateUserProfile(user.getUsername(), newUserProfile);

    savedUser = userRepository.findByUsername(encryptedUserName);

    savedProfile = savedUser.getProfile();
    assertNotEquals(newUserProfile.getFirstName(), savedProfile.getFirstName());
    assertNotEquals(newUserProfile.getLastName(), savedProfile.getLastName());
    assertNotEquals(newUserProfile.getTelephone(), savedProfile.getTelephone());

    assertEquals(amazonKMSService.encryptMessage(amazonKMSService.getUserEncryptedDataKey(),
        newUserProfile.getFirstName()), savedProfile.getFirstName());
    assertEquals(amazonKMSService.encryptMessage(amazonKMSService.getUserEncryptedDataKey(),
        newUserProfile.getLastName()), savedProfile.getLastName());
    assertEquals(amazonKMSService.encryptMessage(amazonKMSService.getUserEncryptedDataKey(),
        newUserProfile.getTelephone()), savedProfile.getTelephone());

    assertEquals(newUserProfile.getJobRole(), savedProfile.getJobRole());
    assertEquals(newUserProfile.getCountry(), savedProfile.getCountry());
    assertEquals(newUserProfile.getState(), savedProfile.getState());
    assertEquals(newUserProfile.getCity(), savedProfile.getCity());
    assertEquals(newUserProfile.getPicture(), savedProfile.getPicture());

    preferences = newUserProfile.getPreferences();
    savedPreferences = savedUser.getProfile().getPreferences();
    assertEquals(preferences.getDateFormat(), savedPreferences.getDateFormat());
    assertEquals(preferences.getTimeFormat(), savedPreferences.getTimeFormat());
    assertEquals(preferences.isAlertNotifications(), savedPreferences.isAlertNotifications());
    assertEquals(preferences.isEmailNotification(), savedPreferences.isEmailNotification());
    assertEquals(preferences.isUse24HourFormat(), savedPreferences.isUse24HourFormat());
    assertEquals(preferences.isUseSSL(), savedPreferences.isUseSSL());

  }


  private UserProfile createUserProfile() {

    UserProfile profile = new UserProfile();
    profile.setFirstName(generator.generate(10));
    profile.setLastName(generator.generate(10));
    profile.setJobRole(generator.generate(10));
    profile.setCountry(generator.generate(10));
    profile.setState(generator.generate(10));
    profile.setCity(generator.generate(10));
    profile.setTelephone(generator.generate(10));
    profile.setPicture(generator.generate(10));

    UserPreferences preferences = new UserPreferences();
    preferences.setDateFormat(generator.generate(10));
    preferences.setTimeFormat(generator.generate(10));
    preferences.setAlertNotifications(true);
    preferences.setEmailNotification(true);
    preferences.setUse24HourFormat(true);
    preferences.setUseSSL(true);

    profile.setPreferences(preferences);
    return profile;

  }

  @Test
  public void testUpdatePassword()
      throws UsernameNotFoundException, InvalidRequestException, AmazonKMSServiceException {

    User newUser = new User();

    String email = generator.generate(10);
    newUser.setEmail(email);

    String username = generator.generate(10);
    newUser.setUsername(username);

    String oldPassword = generator.generate(10);
    newUser.setPassword(oldPassword);
    newUser.setRole(UserRole.ADMIN);

    newUser = userService.save(newUser);

    UserDetails userDetails = userService.loadUserByUsername(username);

    assertNotNull(userDetails);

    String newPassword = generator.generate(10);

    userService.updatePassword(username, oldPassword, newPassword);

    userDetails = userService.loadUserByUsername(username);
    assertNotNull(userDetails);

    assertEquals(
        amazonKMSService.encryptMessage(amazonKMSService.getUserEncryptedDataKey(), newPassword),
        userDetails.getPassword());

    assertEquals(newPassword, amazonKMSService
        .decryptMessage(amazonKMSService.getUserEncryptedDataKey(), userDetails.getPassword()));

    userRepository.delete(newUser.getId());

  }

  @Test
  public void shouldRaiseValidationExceptionsWhenUpdatingPassword() {

    try {
      userService.updatePassword(null, "", "");
      fail("An exception should have been raised");
    } catch (Exception e) {
      assertTrue(e.getClass().equals(InvalidRequestException.class));
    }

    try {
      userService.updatePassword("", null, "");
      fail("An exception should have been raised");
    } catch (Exception e) {
      assertTrue(e.getClass().equals(InvalidRequestException.class));
    }

    try {
      userService.updatePassword("", "", null);
      fail("An exception should have been raised");
    } catch (Exception e) {
      assertTrue(e.getClass().equals(InvalidRequestException.class));
    }

    try {
      userService.updatePassword("", "", "");
      fail("An exception should have been raised");
    } catch (Exception e) {
      assertTrue(e.getClass().equals(InvalidRequestException.class));
    }

    try {
      userService.updatePassword("invalidUSERNAME", "aaa", "bbb");
      fail("An exception should have been raised");
    } catch (Exception e) {
      assertTrue(e.getClass().equals(UsernameNotFoundException.class));
    }


  }
  
  @Test
  public void isUserFirstAccess() throws Exception{
    
    User user = new User();
    user.setEmail(generator.generate(10));
    user.setUsername(generator.generate(10));
    user.setPassword(generator.generate(10));
    user.setRole(UserRole.ADMIN);    

    userService.save(user);
    
    assertTrue(userService.isUserFirstAccess(user.getUsername()));
    
    assertFalse(userService.isUserFirstAccess(user.getUsername()));
  }


}
