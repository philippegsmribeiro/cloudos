package cloudos.user;

import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import cloudos.encryptionKeys.AmazonKMSServiceException;
import test.CloudOSTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InviteServiceTest extends CloudOSTest {

  @Autowired
  private InviteService inviteService;

  @Autowired
  private InviteRepository inviteRepository;

  private static String email = "testdev@cloudtownai.com";
  private static String tenant = "cybertextron";
  private static String guest = "Philippe";
  private static Calendar calendar = Calendar.getInstance();


  @Before
  public void setup () {
    calendar.setTime(new Date());
  }

  @Test
  public void test1SendEmail() {
    // send an invitation email
    assertTrue(this.inviteService.sendInvitationEmail(email));
  }

  @Test
  public void test2CreateInvitation() throws Exception {

    InviteRequest request = InviteRequest.builder()
                              .tenant(tenant)
                              .guest(guest)
                              .email(email)
                              .build();

    Invite invite = this.inviteService.create(request);
    assertNotNull(invite);
    assertNotNull(invite.getId());
    assertEquals(invite.getGuest(), request.getGuest());
    assertEquals(invite.getIsPending(), true);
    assertEquals(invite.getTenant(), request.getTenant());
    assertNotEquals(invite.getEmail(), request.getEmail());

    // attempt to create the same invitation
    Invite invalid = this.inviteService.create(request);
    assertNull(invalid);

  }

  @Test
  public void test3DescribeInvitation() {
    Invite invite = this.create();

    // check a valid invitation exists
    Invite another = this.inviteService.describe(invite.getId());
    assertNotNull(another);
    assertEquals(invite.getEmail(), another.getEmail());
    assertEquals(invite.getGuest(), another.getGuest());
    assertEquals(invite.getTenant(), another.getTenant());
    assertEquals(invite.getId(), another.getId());
    assertEquals(invite.getExpirationDate(), another.getExpirationDate());
    assertEquals(invite.getInvitationDate(), another.getInvitationDate());
    assertEquals(invite.getIsPending(), another.getIsPending());

    // check an invalid invitation
    Invite invalid = Invite.builder()
        .id("invalid")
        .email("invalid")
        .tenant("invalid")
        .guest("invalid")
        .build();
    another = this.inviteService.describe(invalid.getId());
    assertNull(another);
  }

  @Test
  public void test4ListInvitation() {
    Date today = new Date();
    List<Invite> invites = this.inviteService.list();
    assertNotNull(invites);
    assertThat(invites.size(), greaterThan(0));
    // check no expired
    invites.forEach(invite -> assertTrue(today.before(invite.getExpirationDate())));
  }

  @Test
  public void test5EditInvitation() throws AmazonKMSServiceException {
    Invite invite = this.create();

    InviteRequest request = InviteRequest.builder()
        .tenant(tenant)
        .guest(guest)
        .email(email)
        .build();

    assertNotNull(invite);
    assertNotNull(invite.getId());
    assertEquals(invite.getGuest(), request.getGuest());
    assertEquals(invite.getIsPending(), true);
    assertEquals(invite.getTenant(), request.getTenant());

    // update the email
    request.setEmail("production@cloudtownai.com");
    Invite invite1 = this.inviteService.edit(invite.getId(), request);
    assertNotNull(invite1);
    assertEquals(invite1.getGuest(), request.getGuest());
    assertEquals(invite1.getIsPending(), true);
    assertEquals(invite1.getTenant(), request.getTenant());
    assertNotEquals(invite.getEmail(), invite1.getEmail());

  }

  @Test
  public void test6DeleteInvitation() {
    Invite invite = this.create();
    // attempt to delete a valid one
    assertTrue(this.inviteService.delete(invite.getId()));
    // attempt to delete the same invitation
    assertFalse(this.inviteService.delete(invite.getId()));
    // attempt to delete an invalid invitation
    Invite invalid = Invite.builder()
                           .id("invalid")
                           .email("invalid")
                           .tenant("invalid")
                           .guest("invalid")
                           .build();
    assertFalse(this.inviteService.delete(invalid.getId()));

  }

  @Test
  public void test7IsExpired() {
    Invite invite = this.create();

    // should not be expired
    assertFalse(this.inviteService.isExpired(invite));

    Calendar calendar=Calendar.getInstance();
    calendar.setTime(new Date());
    calendar.set(Calendar.SECOND,(calendar.get(Calendar.SECOND)-10));

    // set the expiration date for now
    invite.setExpirationDate(calendar.getTime());
    assertTrue(this.inviteService.isExpired(invite));
  }


  private Invite create() {
    Date now = new Date();
    Date myNewDate = DateUtils.addDays(now, 4);
    Invite invite = Invite.builder()
        .email(generator.generate(8))
        .invitationDate(now)
        .tenant(tenant)
        .guest(guest)
        .isPending(true)
        .expirationDate(myNewDate)
        .build();

    return this.inviteRepository.save(invite);
  }
}
