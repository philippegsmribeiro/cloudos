package cloudos.billings;

import static org.junit.Assert.assertEquals;

import cloudos.billings.AwsBillingReport;
import cloudos.billings.AwsBillingReportMapper;

import java.io.IOException;

import org.apache.commons.csv.CSVRecord;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;

import test.CloudOSTest;

/** Unit tests for {@link cloudos.billings.AwsBillingReportMapper}. */
public class AwsBillingReportMapperTest extends CloudOSTest {

  private static final Double USAGE_AMOUNT = Double.valueOf(0.00000014d);

  @Autowired private AwsBillingReportMapper mapper;

  @Value("classpath:cloudos/cloudosreport-single-record.csv")
  private Resource csvReportSingleRow;

  @Test
  public void mapRowAndValidateUsageAmount() throws IOException {

    Iterable<CSVRecord> csvRecords =
        mapper.getCsvRecords(csvReportSingleRow.getFile().getAbsolutePath());
    AwsBillingReport firstReport = mapper.mapRow(csvRecords.iterator().next());

    assertEquals(USAGE_AMOUNT, firstReport.getUsageAmount());
  }
}
