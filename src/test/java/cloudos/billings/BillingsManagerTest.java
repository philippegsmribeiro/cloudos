package cloudos.billings;

import cloudos.billings.AwsBillingFileProcessor;
import cloudos.billings.BillingsManager;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

/** Created by philipperibeiro on 6/2/17. */
public class BillingsManagerTest extends CloudOSTest {

  @Autowired private AwsBillingFileProcessor awsBillingFileProcessor;

  private BillingsManager billingsManager;

  @Before
  public void setUp() throws Exception {
    this.billingsManager = new BillingsManager(this.awsBillingFileProcessor);
  }

  @Test
  @Ignore
  public void testRun() throws Exception {
    this.billingsManager.run();
  }
}
