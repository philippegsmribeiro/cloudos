package cloudos.billings;

import static org.junit.Assert.assertSame;

import cloudos.Providers;
import cloudos.billings.BillingReport;
import cloudos.billings.BillingReportUtils;
import cloudos.models.costanalysis.GenericBillingReport;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import test.CloudOSTest;

/** Unit tests for {@link BillingReportUtils}. */
public class BillingReportUtilsTest extends CloudOSTest {

  @Test
  public void groupBillingReportsByProvider() {
    GenericBillingReport report1 = new GenericBillingReport();
    report1.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);

    GenericBillingReport report2 = new GenericBillingReport();
    GenericBillingReport report3 = new GenericBillingReport();
    report2.setProvider(Providers.AMAZON_AWS);
    report3.setProvider(Providers.AMAZON_AWS);

    List<GenericBillingReport> reports = Arrays.asList(report1, report2, report3);
    Map<Providers, List<GenericBillingReport>> providersListMap = BillingReportUtils
        .groupBillingReportsByProvider(reports);
    assertSame(1, providersListMap.get(report1.getProvider()).size());
    assertSame(2, providersListMap.get(report2.getProvider()).size());
  }
}
