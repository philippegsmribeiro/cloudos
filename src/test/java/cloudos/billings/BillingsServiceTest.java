package cloudos.billings;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import cloudos.Providers;
import cloudos.billings.BillingsService;
import cloudos.billings.CloudosReportBucket;
import cloudos.billings.CloudosReportBucketRepository;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

/** Created by philipperibeiro on 7/16/17. */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BillingsServiceTest extends CloudOSTest {

  @Autowired BillingsService billingsService;

  @Autowired CloudosReportBucketRepository bucketRepository;

  static CloudosReportBucket bucket;

  @BeforeClass
  public static void setUp() {
    String name =
        String.format("cloudos_report_%s", generator.generate(10));
    bucket = CloudosReportBucket.builder()
                .name(name)
                .provider(Providers.MICROSOFT_AZURE)
                .region("us-central-1")
                .build();
  }

  @Test
  public void test1CreateBucket() {
    // count the number of buckets in the repository
    long countBefore = this.bucketRepository.count();
    // attempt to save the save the bucket into the repository
    CloudosReportBucket result = billingsService.createBucket(bucket);

    assertNotNull(result);
    assertNotNull(result.getId());
    assertEquals(result.getName(), bucket.getName());
    assertEquals(result.getProvider(), bucket.getProvider());
    assertEquals(result.getRegion(), bucket.getRegion());

    // attemp to find the newly created bucket in the repository
    CloudosReportBucket reportBucket = billingsService.describeBucket(result.getId());
    assertNotNull(reportBucket);
    assertEquals(reportBucket.getName(), bucket.getName());
    assertEquals(reportBucket.getProvider(), bucket.getProvider());
    assertEquals(reportBucket.getRegion(), bucket.getRegion());

    long countAfter = this.bucketRepository.count();
    assertEquals(countAfter, countBefore + 1);

    // BUG CLOUD-654:
    String name =
        String.format("cloudos_report_%s", generator.generate(10));
    CloudosReportBucket anotherBucket = CloudosReportBucket.builder()
            .name(name)
            .provider(Providers.MICROSOFT_AZURE)
            .region("us-central-1")
            .build();
    result = billingsService.createBucket(anotherBucket);
    assertNotNull(result);
    assertNotNull(result.getId());
    assertEquals(result.getName(), anotherBucket.getName());
    assertEquals(result.getProvider(), anotherBucket.getProvider());
    assertEquals(result.getRegion(), anotherBucket.getRegion());
    long countAfterAnother = this.bucketRepository.count();
    assertEquals(countAfterAnother, countAfter + 1);

    billingsService.deleteBucket(anotherBucket);
  }

  @Test
  public void test2DescribeBucket() {
    // attemp to find the newly created bucket in the repository
    CloudosReportBucket reportBucket = billingsService.describeBucket(bucket.getId());
    assertNotNull(reportBucket);
    assertEquals(reportBucket.getName(), bucket.getName());
    assertEquals(reportBucket.getProvider(), bucket.getProvider());
    assertEquals(reportBucket.getRegion(), bucket.getRegion());
  }

  @Test
  public void test3ListBuckets() {
    List<CloudosReportBucket> bucketList = billingsService.getBuckets();
    assertNotNull(bucketList);
    assertThat(bucketList.size(), greaterThan(0));
    assertTrue(bucketList.contains(bucket));

    // BUG CLOUD-654:
    String name =
        String.format("cloudos_report_%s", generator.generate(10));
    CloudosReportBucket anotherBucket = CloudosReportBucket.builder()
        .name(name)
        .provider(Providers.MICROSOFT_AZURE)
        .region("us-central-1")
        .build();
    CloudosReportBucket result = billingsService.createBucket(anotherBucket);
    assertNotNull(result);
    assertNotNull(result.getId());
    assertEquals(result.getName(), anotherBucket.getName());
    assertEquals(result.getProvider(), anotherBucket.getProvider());
    assertEquals(result.getRegion(), anotherBucket.getRegion());

    List<CloudosReportBucket> bucketList2 = billingsService.getBuckets();
    assertNotNull(bucketList);
    assertThat(bucketList2.size(), greaterThan(bucketList.size()));
    assertTrue(bucketList.contains(bucket));

    // delete
    billingsService.deleteBucket(anotherBucket);
  }

  @Test
  public void test4DeleteBucket() {
    // delete the bucket
    billingsService.deleteBucket(bucket);
    // we deleted the bucket ... it shouldn't be there.
    assertFalse(billingsService.bucketExists(bucket));
  }
}
