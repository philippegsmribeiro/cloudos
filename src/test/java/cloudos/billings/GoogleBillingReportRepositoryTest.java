package cloudos.billings;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

/** Unit tests for {@link GoogleBillingReportRepository}. */
public class GoogleBillingReportRepositoryTest extends CloudOSTest {

  private static final int MAX_RESOURCES_COST_WINDOW = 45;

  @Autowired private GoogleBillingReportRepository repository;

  @Test
  public void findByStartTimeBetween() {
    Date startTime =
        new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(MAX_RESOURCES_COST_WINDOW));
    Date endTime = new Date();

    List<GoogleBillingReport> result = repository.findByStartTimeBetween(startTime, endTime);
    for (GoogleBillingReport item : result) {
      Assert.assertTrue(item.getStartTime().after(startTime));
      Assert.assertTrue(item.getStartTime().before(endTime));
    }
  }
}
