package cloudos.billings;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import cloudos.Providers;
import cloudos.queue.QueueListenerForTest;
import cloudos.queue.message.QueueMessageType;
import java.util.List;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.CloudOSTest;

@Ignore
public class BillingFileProcessorTests extends CloudOSTest {

  public static final String GOOGLE_BUCKET = "tb595d8ed88a98ae152f9f5916";
  public static final String AWS_BUCKET = "cloudosbilling";

  @Autowired
  CloudosReportBucketRepository bucketRepository;
  @Autowired
  BillingFileProcessorFactory billingFileProcessorFactory;
  @Autowired
  BillingFileRepository fileRepository;
  @Autowired
  QueueListenerForTest queueListenerForTest;

  @Before
  public void setUp() {
    bucketRepository.deleteAll();

    // save the bucket if not there
    CloudosReportBucket bucket = CloudosReportBucket.builder()
                                                    .name(AWS_BUCKET)
                                                    .provider(Providers.AMAZON_AWS)
                                                    .build();
    bucketRepository.save(bucket);

    // save the bucket if not there
    bucket =CloudosReportBucket.builder()
                               .name(GOOGLE_BUCKET)
                               .provider(Providers.GOOGLE_COMPUTE_ENGINE)
                               .build();
    bucketRepository.save(bucket);
    
    //fileRepository.deleteAll();
    queueListenerForTest.purge(QueueMessageType.BILLING_FILE);
    while(queueListenerForTest.getTotalMessages(QueueMessageType.BILLING_FILE) != 0) {
      logger.debug("Waiting");
    }
  }

  @Test
  public void testPollAws() throws Exception {
    this.testPoll(Providers.AMAZON_AWS);
  }
  
  @Test
  public void testPollGoogle() throws Exception {
    this.testPoll(Providers.GOOGLE_COMPUTE_ENGINE);
  }
  
  private void testPoll(Providers provider) throws Exception {
    BillingFileProcessor billingFileProcessor =
        billingFileProcessorFactory.getBillingFileProcessor(provider);
    assertNotNull(billingFileProcessor);

    billingFileProcessor.poll();

    // validate
    List<BillingFile> byProvider = fileRepository.getByProvider(provider);
    Assert.assertNotNull(byProvider);
    Assert.assertNotEquals(0, byProvider.size());
    //byProvider.forEach(t -> Assert.assertFalse(t.isProcessed()));
    logger.debug("Files: {}", byProvider.size());
    
    // validate messages
    //Thread.sleep(2000);
    //long totalMessages = queueListenerForTest.getTotalMessages(QueueMessageType.BILLING_FILE);
    //assertEquals(byProvider.size(), totalMessages);
    
    // process file
    
    for (BillingFile billingFile : byProvider) {
      try {
        this.testProcess(provider, billingFile);
      } catch (Exception e) {
        logger.error("error!", e);
      }
    }
    
    //Optional<BillingFile> findFirst = byProvider.stream().filter(t -> t.getName().contains("20180301-20180401") && t.getName().endsWith("cloudosreport-1.csv.gz")).findFirst();
    
    //this.testProcess(provider, byProvider.get(1));
    System.out.println();
  }
  
  private void testProcess(Providers provider, BillingFile billingFile) throws Exception {
    BillingFileProcessor billingFileProcessor =
        billingFileProcessorFactory.getBillingFileProcessor(provider);
    assertNotNull(billingFileProcessor);
    
    billingFileProcessor.processBillingFile(billingFile);
    
    // validate
    BillingFile findFile = fileRepository.findOne(billingFile.getId());
    Assert.assertNotNull(findFile);
    //Assert.assertTrue(findFile.isProcessed());
  }
}
