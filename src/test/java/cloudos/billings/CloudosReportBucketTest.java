package cloudos.billings;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import cloudos.Providers;
import cloudos.billings.CloudosReportBucket;
import cloudos.billings.CloudosReportBucketRepository;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

/** Created by philipperibeiro on 5/6/17. */
public class CloudosReportBucketTest extends CloudOSTest {

  @Autowired private CloudosReportBucketRepository bucketRepository;

  private CloudosReportBucket bucket = CloudosReportBucket.builder()
                                          .name("cloudos_test_report")
                                          .provider(Providers.MICROSOFT_AZURE)
                                          .build();

  @Test
  public void testAdd() {
    this.bucketRepository.deleteAll();

    // save the report bucket
    this.bucketRepository.save(bucket);

    // check the bucket exists
    CloudosReportBucket bucket1 = this.bucketRepository.findByName(bucket.getName());
    assertNotNull(bucket1);
    assertEquals(bucket.getName(), bucket1.getName());

    assertTrue(this.bucketRepository.exists(bucket));
  }

  @Test
  public void testDelete() {
    // check the bucket exists
    CloudosReportBucket bucket1 = this.bucketRepository.findByName(bucket.getName());
    assertNotNull(bucket1);
    assertEquals(bucket.getName(), bucket1.getName());

    // delete the bucket
    this.bucketRepository.delete(bucket1);
    List<CloudosReportBucket> bucketList = this.bucketRepository.findAll();
    bucketList.forEach(logger::info);
    assertNotSame(this.bucketRepository.exists(bucket), true);
  }
}
