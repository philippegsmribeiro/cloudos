package cloudos.billings;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

import cloudos.Providers;
import cloudos.billings.AwsBillingReportMapper;
import cloudos.billings.AwsBillingReportRepository;
import cloudos.billings.BillingReportMapper;
import cloudos.billings.BillingReportMapperFactory;
import cloudos.billings.GoogleBillingReportRepository;
import cloudos.costanalysis.BillingAccountRepository;
import cloudos.costanalysis.GenericBillingReportRepository;
import cloudos.models.CallResult;
import cloudos.models.CallStatus;

import java.io.File;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;

import test.CloudOSTest;

/**
 * Created by philipperibeiro on 6/17/17.
 */
public class BillingReportMapperFactoryTest extends CloudOSTest {

  @Value("classpath:cloudos/cloudosreport-1.csv")
  private Resource awsReport;

  @Mock
  private AwsBillingReportRepository awsBillingReportRepository;

  @Mock
  private GoogleBillingReportRepository googleBillingReportRepository;

  @Mock
  private GenericBillingReportRepository genericBillingReportRepository;

  @Mock
  private BillingAccountRepository billingAccountRepository;

  @Autowired
  @InjectMocks
  private BillingReportMapperFactory billingReportMapperFactory;

  @Test
  public void testAwsFactory() throws Exception {
    ClassLoader classLoader = getClass().getClassLoader();
    String fileName = awsReport.getFile().getAbsolutePath();
    assertNotNull(fileName);

    // pick a Aws Billing Report Mapper
    AwsBillingReportMapper mapper =
        (AwsBillingReportMapper) billingReportMapperFactory.getMapper(Providers.AMAZON_AWS);

    // attempt to parse the cvs file
    CallResult<Void> result = mapper.getBillingReport(fileName, this.awsBillingReportRepository);

    assertSame(CallStatus.SUCCESS, result.getStatus());
  }

  @Test
  public void testGoogleFactory() throws Exception {
    ClassLoader classLoader = getClass().getClassLoader();
    File file =
        new File(classLoader.getResource("google/bucket-billing-2017-07-04.json").getFile());
    String filename = file.getAbsolutePath();
    assertNotNull(filename);

    // pick the Billing Report Mapper
    BillingReportMapper mapper =
        billingReportMapperFactory.getMapper(Providers.GOOGLE_COMPUTE_ENGINE);

    // attempt to parse the cvs file
    CallResult<Void> result = mapper.getBillingReport(filename, this.googleBillingReportRepository);

    assertSame(CallStatus.SUCCESS, result.getStatus());
  }

  @Test
  public void invalidMapperProviderExpectsNull() {
    BillingReportMapper mapper = billingReportMapperFactory.getMapper(null);
    assertNull(mapper);
  }
}
