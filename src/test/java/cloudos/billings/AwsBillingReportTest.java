package cloudos.billings;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import cloudos.billings.AwsBillingReport;
import cloudos.billings.AwsBillingReportRepository;

import java.util.List;
import java.util.function.Predicate;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

/** Created by philipperibeiro on 6/2/17. */
public class AwsBillingReportTest extends CloudOSTest {

  @Autowired private AwsBillingReportRepository billingReportRepository;

  @Test
  public void testAddAwsBillingReport() throws Exception {
    AwsBillingReport report = new AwsBillingReport();
    report.setPublicOnDemandCost(0.12);
    report.setProductName("Amazon AWS");
    report.setResourceId("i-12sdadasdas");

    this.billingReportRepository.save(report);

    List<AwsBillingReport> another = this.billingReportRepository.findByResourceId("i-12sdadasdas");
    assertNotNull(another);

    this.billingReportRepository.delete(another);
  }

  @Test
  public void testDeleteAwsBillingReport() throws Exception {
    AwsBillingReport report = new AwsBillingReport();
    report.setPublicOnDemandCost(0.12);
    report.setProductName("Amazon AWS");
    report.setResourceId("i-12sdadasdas");

    this.billingReportRepository.save(report);

    List<AwsBillingReport> another = this.billingReportRepository.findByResourceId("i-12sdadasdas");
    assertNotNull(another);
    Predicate<AwsBillingReport> found =
        (report1) -> report1.getResourceId().equals(report.getResourceId());
    assertTrue(found.test(report));

    this.billingReportRepository.delete(another);

    another = this.billingReportRepository.findByResourceId("i-12sdadasdas");
    assertNotNull(another);
    assertEquals(another.size(), 0);
  }
}
