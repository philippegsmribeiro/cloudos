package cloudos.billings;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import cloudos.Providers;
import cloudos.billings.BillingFile;
import cloudos.billings.BillingFileRepository;

import java.util.Date;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

/** Created by philipperibeiro on 5/29/17. */
public class BillingFileTest extends CloudOSTest {

  @Autowired private BillingFileRepository fileRepository;

  @Test
  public void testAddBillingFile() {
    BillingFile billingFile = this.createBillingFile();
    // save the file and check that the name is the same
    this.fileRepository.save(billingFile);
    BillingFile another = this.fileRepository.getByName(billingFile.getName());
    // check they are the same
    assertEquals(billingFile.getName(), another.getName());
    this.fileRepository.delete(billingFile);
  }

  @Test
  public void testDeleteBillingFile() {
    BillingFile billingFile = this.createBillingFile();
    // save the file and check that the name is the same
    this.fileRepository.save(billingFile);
    this.fileRepository.delete(billingFile);
    assertNull(this.fileRepository.getByName(billingFile.getName()));
  }

  private BillingFile createBillingFile() {
    String name = generator.generate(10);
    String summary = generator.generate(10);
    String bucket = generator.generate(10);
    return new BillingFile(null, name, summary, name, bucket, Providers.MICROSOFT_AZURE, new Date(),
        false, false);
  }
}
