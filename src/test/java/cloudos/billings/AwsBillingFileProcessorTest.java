package cloudos.billings;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import cloudos.Providers;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import test.CloudOSTest;

/** Created by philipperibeiro on 5/29/17. */
public class AwsBillingFileProcessorTest extends CloudOSTest {

  private static String bucket = "cloudosbilling2";

  @Autowired
  private AwsBillingFileProcessor fileProcessor;

  @Value("${cloudos.billings.directory}")
  private String directory;

  @Autowired private CloudosReportBucketRepository bucketRepository;

  @Before
  public void setUp() throws Exception {
    // save the bucket it does not exist
    if (this.bucketRepository.findByName(bucket) == null) {
      CloudosReportBucket reportBucket = CloudosReportBucket.builder()
                                              .name(bucket)
                                              .provider(Providers.AMAZON_AWS)
                                              .build();
      this.bucketRepository.save(reportBucket);
      assertTrue(this.bucketRepository.exists(reportBucket));
    }
    // clean up for now
    // this.fileRepository.deleteAll();
    // this.billingReportRepository.deleteAll();
  }

  @Test
  public void testInit() throws Exception {
    Exception ex = null;
    try {
      this.fileProcessor.init();
    } catch (Exception e) {
      ex = e;
    }
    assertEquals(null, ex);
  }

  @Test
  @Ignore
  public void testPoll() throws Exception {
    assertTrue(this.bucketRepository.findByName(bucket) != null);
    this.fileProcessor.poll();
  }
}
