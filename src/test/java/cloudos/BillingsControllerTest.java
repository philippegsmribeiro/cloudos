package cloudos;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import cloudos.billings.BillingsService;
import cloudos.billings.CloudosReportBucket;
import test.CloudOSTest;

/** Created by philipperibeiro on 5/6/17. */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BillingsControllerTest extends CloudOSTest {

  private MockMvc mvc;

  @Autowired private BillingsService billingsService;

  @Autowired private WebApplicationContext webApplicationContext;
  // create a new Report Bucket

  static String name =
      String.format("cloudos_report_%s", generator.generate(10));
  static CloudosReportBucket reportBucket = CloudosReportBucket.builder()
                                                .name(name)
                                                .provider(Providers.MICROSOFT_AZURE)
                                                .region("us-central-1")
                                                .timestamp(new Date())
                                                .id(new ObjectId().toHexString())
                                                .build();

  @Autowired
  void setConverters(HttpMessageConverter<?>[] converters) {

    this.mappingJackson2HttpMessageConverter =
        Arrays.asList(converters)
            .stream()
            .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
            .findAny()
            .orElse(null);

    assertNotNull(
        "the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
  }

  @Before
  public void setUp() throws Exception {
    this.mvc = webAppContextSetup(webApplicationContext).build();
  }

  @Test
  public void testABillings() throws Exception {
    this.mvc.perform(get(BillingsController.BILLINGS + "/")).andExpect(status().is4xxClientError());
  }

  @Test
  public void testBListBuckets() throws Exception {
    this.mvc.perform(get(BillingsController.BILLINGS + "/buckets")).andExpect(status().isOk());
  }

  @Test
  public void testCAddBuckets() throws Exception {
    this.mvc
        .perform(
            post(BillingsController.BILLINGS + "/buckets")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json(reportBucket))
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated());

    // attempt to add the same bucket again
    this.mvc
        .perform(
            post(BillingsController.BILLINGS + "/buckets")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json(reportBucket))
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is5xxServerError());
  }

  @Test
  public void testDDescribeBuckets() throws Exception {
    List<CloudosReportBucket> reportBucketList = this.billingsService.getBuckets();
    assertNotNull(reportBucketList);
    assertThat(reportBucketList.size(), greaterThan(0));

    this.mvc
        .perform(get(BillingsController.BILLINGS + "/buckets/" + reportBucketList.get(0).getId()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(contentType))
        .andExpect(jsonPath("$.id", is(reportBucketList.get(0).getId())))
        .andExpect(jsonPath("$.region", is(reportBucketList.get(0).getRegion())))
        .andExpect(jsonPath("$.name", is(reportBucketList.get(0).getName())));

    // attempt to describe a bucket that does not exist
    this.mvc
        .perform(get(BillingsController.BILLINGS + "/buckets/" + "iddoesnotexist"))
        .andExpect(status().isNotFound());
  }

  @Test
  public void testEDeleteBucket() throws Exception {

    this.mvc
        .perform(
            delete(BillingsController.BILLINGS + "/buckets")
                .content(json(reportBucket))
                .contentType(contentType))
        .andExpect(status().is2xxSuccessful());

    // attempt to find a bucket that does not exist
    this.mvc
        .perform(
            delete(BillingsController.BILLINGS + "/buckets")
                .content(json(reportBucket))
                .contentType(contentType))
        .andExpect(status().isNotFound());
  }
}
