package cloudos.monitoring.billings;

import static org.junit.Assert.assertEquals;
import cloudos.Providers;
import cloudos.billings.BillingFile;
import cloudos.billings.BillingFileProcessorTests;
import cloudos.billings.BillingFileRepository;
import cloudos.billings.CloudosReportBucket;
import cloudos.billings.CloudosReportBucketRepository;
import cloudos.queue.QueueListenerForTest;
import cloudos.queue.message.QueueMessageType;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import test.CloudOSTest;
import test.SlowTest;

@Category(SlowTest.class)
public class BillingsMonitorPoolingUpdateTest extends CloudOSTest {

  @Autowired
  BillingsMonitorPoolingUpdate monitor;
  @Autowired
  CloudosReportBucketRepository bucketRepository;
  @Autowired
  BillingFileRepository fileRepository;
  @Autowired
  QueueListenerForTest queueListenerForTest;


  @Test
  @Ignore
  public void testMonitorAws() throws Exception {
    // save the bucket if not there
    Providers provider = Providers.AMAZON_AWS;
    List<CloudosReportBucket> findByProvider = bucketRepository.findByProvider(provider);
    CloudosReportBucket bucket = CloudosReportBucket.builder()
                                                    .name(BillingFileProcessorTests.AWS_BUCKET)
                                                    .provider(provider)
                                                    .build();
    if (CollectionUtils.isEmpty(findByProvider)) {
      bucketRepository.save(bucket);
    }
    // test
    monitor.run(provider);
    
    // validate
    List<BillingFile> byProvider = fileRepository.getByProvider(provider);
    Assert.assertNotNull(byProvider);
    Assert.assertNotEquals(0, byProvider.size());
    byProvider.forEach(t -> Assert.assertFalse(t.isProcessed()));
    
    Thread.sleep(5000);

    // validate messages
    long totalMessages = queueListenerForTest.getTotalMessages(QueueMessageType.BILLING_FILE);
    assertEquals(byProvider.size(), totalMessages);
  }

  @Test
  @Ignore
  public void testMonitorGoogle() throws Exception {
    // clean the files
    fileRepository.deleteAll();
    queueListenerForTest.purge(QueueMessageType.BILLING_FILE);

    // save the bucket if not there
    Providers provider = Providers.GOOGLE_COMPUTE_ENGINE;
    List<CloudosReportBucket> findByProvider = bucketRepository.findByProvider(provider);
    CloudosReportBucket bucket = CloudosReportBucket.builder()
                                                    .name(BillingFileProcessorTests.GOOGLE_BUCKET)
                                                    .provider(provider)
                                                    .build();
    if (CollectionUtils.isEmpty(findByProvider)) {
      bucketRepository.save(bucket);
    }
    // test
    monitor.run(provider);
    // validate
    List<BillingFile> byProvider = fileRepository.getByProvider(provider);
    Assert.assertNotNull(byProvider);
    Assert.assertNotEquals(0, byProvider.size());
    byProvider.forEach(t -> Assert.assertFalse(t.isProcessed()));

    Thread.sleep(5000);

    // validate messages
    long totalMessages = queueListenerForTest.getTotalMessages(QueueMessageType.BILLING_FILE);
    assertEquals(byProvider.size(), totalMessages);
  }
}
