package cloudos.monitoring.notifications;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.CloudOSTest;

public class NotificationMonitorRefreshTest extends CloudOSTest {

  @Autowired
  NotificationMonitorRefresh monitor;

  @Test
  public void testMonitor() throws Exception {
    monitor.runTask();
  }
}