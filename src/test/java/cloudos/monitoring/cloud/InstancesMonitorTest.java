package cloudos.monitoring.cloud;

import cloudos.Providers;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.CloudOSTest;

public class InstancesMonitorTest extends CloudOSTest {

  @Autowired
  InstancesMonitor monitor;

  @Test
  public void testMonitor() throws Exception {
    monitor.run(Providers.AMAZON_AWS);
  }
  
  @Test
  public void testMonitor2() throws Exception {
    monitor.run(Providers.GOOGLE_COMPUTE_ENGINE);
  }
}
