package cloudos.monitoring.cloud;

import cloudos.Providers;
import cloudos.data.InstanceDataMetric;
import cloudos.models.CloudosDatapoint;
import cloudos.resources.AmazonResourcesService;
import cloudos.resources.GoogleResourcesService;
import java.util.List;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.CloudOSTest;

public class DatapointsMonitorTest extends CloudOSTest {

  @Autowired
  DatapointsMonitor monitor;
  @Autowired
  private AmazonResourcesService amazonInstanceManagement;
  @Autowired
  private GoogleResourcesService googleInstanceManagement;

  @Test
  public void testMonitor() throws Exception {
    monitor.run(Providers.AMAZON_AWS);
  }
  
  @Test
  public void testMonitor2() throws Exception {
    monitor.run(Providers.GOOGLE_COMPUTE_ENGINE);
  }

  @Test
  @Ignore
  public void testAWS() {
    for (int i = 0; i < 120; i++) {
      try {
        List<CloudosDatapoint> datapoints = amazonInstanceManagement.findAllDatapoints(
            DatapointsMonitor.WINDOW, DatapointsMonitor.PERIOD, InstanceDataMetric.CPU_UTILIZATION,
            InstanceDataMetric.DISK_READ_BYTES, InstanceDataMetric.DISK_WRITE_BYTES,
            InstanceDataMetric.NETWORK_RECEIVED_BYTES, InstanceDataMetric.NETWORK_SENT_BYTES);
        System.out.println("***************************************");
        System.out.println(datapoints.toString());
        Thread.sleep(1000);
      } catch (Exception e) {
        // TODO: handle exception
      }
    }
  }

  @Test
  @Ignore
  public void testGoogle() {
    for (int i = 0; i < 120; i++) {
      try {
        List<CloudosDatapoint> datapoints =
            googleInstanceManagement.findAllDatapoints(
                DatapointsMonitor.WINDOW,
                60,
                InstanceDataMetric.CPU_UTILIZATION,
                InstanceDataMetric.DISK_READ_BYTES,
                InstanceDataMetric.DISK_WRITE_BYTES,
                InstanceDataMetric.NETWORK_RECEIVED_BYTES,
                InstanceDataMetric.NETWORK_SENT_BYTES);
        System.out.println("***************************************");
        System.out.println(datapoints.toString());
        Thread.sleep(1000);
      } catch (Exception e) {
        // TODO: handle exception
      }
    }
  }
}
