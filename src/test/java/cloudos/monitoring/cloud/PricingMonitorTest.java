package cloudos.monitoring.cloud;

import cloudos.Providers;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import test.CloudOSTest;
import test.HighMemoryTest;

@Category(HighMemoryTest.class)
public class PricingMonitorTest extends CloudOSTest {

  @Autowired
  PricingMonitor monitor;

  @Test
  @Ignore
  public void testMonitor() throws Exception {
    monitor.run(Providers.AMAZON_AWS);
  }
  
  @Test
  @Ignore
  public void testMonitor2() throws Exception {
    monitor.run(Providers.GOOGLE_COMPUTE_ENGINE);
  }
}