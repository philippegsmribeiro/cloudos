package cloudos.monitoring.dashboard;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.CloudOSTest;

public class DashboardResourcesMonitorTest extends CloudOSTest {

  @Autowired
  DashboardResourcesMonitor monitor;

  @Test
  public void testMonitor() {
    monitor.runTask();
  }
}