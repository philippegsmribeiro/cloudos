package cloudos.monitoring.dashboard;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.CloudOSTest;

public class DashboardRegionsMonitorTest extends CloudOSTest {

  @Autowired
  DashboardRegionsMonitor monitor;

  @Test
  public void testMonitor() {
    monitor.runTask();
  }
}