package cloudos.monitoring.costanalysis;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.CloudOSTest;

public class ProviderDailyCostMonitorTest extends CloudOSTest {

  @Autowired
  ProviderDailyCostMonitor monitor;

  @Test
  public void testMonitor() throws Exception {
    monitor.runTask();
  }
}