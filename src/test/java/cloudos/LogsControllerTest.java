package cloudos;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import cloudos.data.LogRecordRequest;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import test.CloudOSTest;

/** Created by philipperibeiro on 3/31/17. */
public class LogsControllerTest extends CloudOSTest {

  private MockMvc mvc;

  @Autowired private LogsController logsController;

  @Test
  @Ignore
  public void testSaveLogRecord() throws Exception {
    LogRecordRequest logRecordRequest = new LogRecordRequest();
    this.mvc
        .perform(
            post(LogsController.LOGS + "/list/i-09d349e13a79d4bcc/logs")
                .content(logRecordRequest.toString())
                .contentType(this.contentType))
        .andExpect(status().is5xxServerError());
  }

  @Test
  @Ignore
  public void testGetLogRecord() throws Exception {
    this.mvc
        .perform(get(LogsController.LOGS + "/list/i-09d349e13a79d4bcc/logs"))
        .andExpect(status().is5xxServerError());
  }
}
