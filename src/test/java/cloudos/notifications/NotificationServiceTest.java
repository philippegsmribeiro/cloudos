package cloudos.notifications;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import java.util.Calendar;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runners.MethodSorters;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import cloudos.exceptions.InvalidRequestException;
import cloudos.notifications.Notification;
import cloudos.notifications.NotificationCategory;
import cloudos.notifications.NotificationChannel;
import cloudos.notifications.NotificationRepository;
import cloudos.notifications.NotificationSearchRequest;
import cloudos.notifications.NotificationType;
import cloudos.notifications.NotificationsService;
import test.CloudOSTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NotificationServiceTest extends CloudOSTest {

  @Autowired
  NotificationsService notificationsService;

  @Autowired
  NotificationRepository notificationRepository;

  @Rule
  public ExpectedException expectedEx = ExpectedException.none();
  
  private static boolean started = false;
  
  @Before
  public void before() {
    if(!started) {
      started = true;
      notificationRepository.deleteAll();
    }
  }

  @Test
  public void testAList1() {
    NotificationSearchRequest request = new NotificationSearchRequest(0, 10, null, null);
    Page<Notification> response = notificationsService.listNotification(request);
    List<Notification> listNotification = response.getContent();
    Assert.assertNotNull(listNotification);
    Assert.assertEquals(listNotification.size(), response.getNumberOfElements());
  }

  @Test
  public void testBList2() {
    NotificationSearchRequest request = new NotificationSearchRequest(0, 1, null, null);
    Page<Notification> response = notificationsService.listNotification(request);
    List<Notification> listNotification = response.getContent();
    Assert.assertNotNull(listNotification);
    Assert.assertEquals(listNotification.size(), response.getNumberOfElements());
    if (listNotification.size() > 0) {
      Assert.assertEquals(listNotification.size(), 1);
    }
  }

  @Test
  public void testCList3() {
    NotificationSearchRequest request =
        new NotificationSearchRequest(0, 1, NotificationCategory.ERROR, null);
    Page<Notification> response = notificationsService.listNotification(request);
    List<Notification> listNotification = response.getContent();
    Assert.assertNotNull(listNotification);
    Assert.assertEquals(listNotification.size(), listNotification.stream()
        .filter(t -> t.getCategory().equals(NotificationCategory.ERROR)).count());
  }

  @Test
  public void testDList4() {
    String user = "admin";
    Mockito.when(requestScope.getUserName()).thenReturn(user);
    NotificationSearchRequest request = new NotificationSearchRequest(0, 1, null, false);
    Page<Notification> response = notificationsService.listNotification(request);
    List<Notification> listNotification = response.getContent();
    Assert.assertNotNull(listNotification);
    Assert.assertEquals(listNotification.size(),
        listNotification.stream().filter(t -> t.getReadUsers().contains(user)).count());

    request = new NotificationSearchRequest(0, 1, null, true);
    response = notificationsService.listNotification(request);
    listNotification = response.getContent();
    Assert.assertNotNull(listNotification);
    Assert.assertEquals(listNotification.size(),
        listNotification.stream().filter(t -> !t.getReadUsers().contains(user)).count());
  }

  @Test
  public void testEMarkRead() {
    String user = "admin";
    Mockito.when(requestScope.getUserName()).thenReturn(user);

    // retrieve a notification
    NotificationSearchRequest request = new NotificationSearchRequest(0, 10, null, true);
    List<Notification> listNotification =
        notificationsService.listNotification(request).getContent();
    Assert.assertNotNull(listNotification);
    Assert.assertThat(listNotification.size(), greaterThanOrEqualTo(0));

    if (listNotification.size() == 0) {
      return;
    }

    Notification notification = listNotification.get(0);

    // verify if user has read the notification
    boolean wasRead = false;
    if (notification.getReadUsers() != null && notification.getReadUsers().contains(user)) {
      // if contains
      wasRead = true;

      // mark as unread
      notification = notificationsService.markNotificationAsUnread(notification);
      Assert.assertFalse(
          notification.getReadUsers() != null && notification.getReadUsers().contains(user));
    }

    // mark as read
    notification = notificationsService.markNotificationAsRead(notification);
    Assert.assertTrue(
        notification.getReadUsers() != null && notification.getReadUsers().contains(user));

    if (!wasRead) {
      // mark as unread
      notification = notificationsService.markNotificationAsUnread(notification);
      Assert.assertFalse(
          notification.getReadUsers() != null && notification.getReadUsers().contains(user));
    }
  }

  @Test
  public void testFDeleteNotification() throws InvalidRequestException {
    Notification notification = new Notification();
    notification.setCategory(NotificationCategory.INFO);
    notification.setType(NotificationType.ALERT);
    notification.setChannel(NotificationChannel.MONITORING);
    notification.setTitle(String.format("Alert deleted."));
    notification.setMessage(String.format("Alert not found in the provider."));
    notificationRepository.save(notification);
    NotificationSearchRequest request =
        new NotificationSearchRequest(0, 10, NotificationCategory.INFO, null);
    Page<Notification> response = notificationsService.listNotification(request);

    List<Notification> listNotification = response.getContent();
    Assert.assertNotNull(listNotification);
    Assert.assertEquals(listNotification.size(), response.getNumberOfElements());
    if (listNotification.size() > 0) {
      Assert.assertEquals(listNotification.size(), 1);
    }
    for (Notification notificationForDeleted : listNotification) {
      notificationsService.deleteNotification(notificationForDeleted.getId());
    }
    NotificationSearchRequest requestResult =
        new NotificationSearchRequest(0, 10, NotificationCategory.INFO, null);
    Page<Notification> responseResult = notificationsService.listNotification(requestResult);

    Assert.assertEquals(0, responseResult.getNumberOfElements());

  }

  @Test
  public void testGDeleteNotificationException() throws InvalidRequestException {
    expectedEx.expect(InvalidRequestException.class);
    expectedEx.expectMessage("Notification does not exist.");
    notificationsService.deleteNotification("AA21D");
  }

  @Test
  public void testHDeleteNotificationAfter24hrsAnd7days() throws InvalidRequestException {
    Notification notification = new Notification();
    notification.setCategory(NotificationCategory.SUCCESS);
    notification.setType(NotificationType.GENERAL);
    notification.setChannel(NotificationChannel.MONITORING);
    notification.setTitle(String.format("Alert deleted."));
    notification.setMessage(String.format("Alert not found in the provider."));
    Calendar now = Calendar.getInstance();
    now.add(Calendar.HOUR, -26);
    notification.setReadDate(now.getTime());
    notificationRepository.save(notification);

    Notification notificationSuccess = new Notification();
    notificationSuccess.setCategory(NotificationCategory.SUCCESS);
    notificationSuccess.setType(NotificationType.ALERT);
    notificationSuccess.setChannel(NotificationChannel.MONITORING);
    notificationSuccess.setTitle(String.format("Alert deleted."));
    notificationSuccess.setMessage(String.format("Alert."));
    Calendar nowSuccess = Calendar.getInstance();
    nowSuccess.add(Calendar.HOUR, -169);
    notificationSuccess.setReadDate(nowSuccess.getTime());
    notificationRepository.save(notificationSuccess);

    notificationsService.processDeletionNotification();

    NotificationSearchRequest request = new NotificationSearchRequest(0, 10, NotificationCategory.SUCCESS, null);
    Page<Notification> response = notificationsService.listNotification(request);

    List<Notification> listNotification = response.getContent();
    Assert.assertTrue(listNotification.isEmpty());
  }
  
  @Test
  public void testINotDeleteNotificationBefore24hrsAnd7days() throws InvalidRequestException {
    Notification notification = new Notification();
    notification.setCategory(NotificationCategory.WARNING);
    notification.setType(NotificationType.GENERAL);
    notification.setChannel(NotificationChannel.MONITORING);
    notification.setTitle(String.format("Alert deleted."));
    notification.setMessage(String.format("Alert not found in the provider."));
    Calendar now = Calendar.getInstance();
    now.add(Calendar.HOUR, -23);
    notification.setReadDate(now.getTime());
    notificationRepository.save(notification);

    Notification notificationSuccess = new Notification();
    notificationSuccess.setCategory(NotificationCategory.WARNING);
    notificationSuccess.setType(NotificationType.ALERT);
    notificationSuccess.setChannel(NotificationChannel.MONITORING);
    notificationSuccess.setTitle(String.format("Alert deleted."));
    notificationSuccess.setMessage(String.format("Alert."));
    Calendar nowSuccess = Calendar.getInstance();
    nowSuccess.add(Calendar.HOUR, -150);
    notificationSuccess.setReadDate(nowSuccess.getTime());
    notificationRepository.save(notificationSuccess);

    notificationsService.processDeletionNotification();

    NotificationSearchRequest request = new NotificationSearchRequest(0, 10, NotificationCategory.WARNING, null);
    Page<Notification> response = notificationsService.listNotification(request);

    List<Notification> listNotification = response.getContent();
    Assert.assertTrue(listNotification.size() ==2);
  }
};


