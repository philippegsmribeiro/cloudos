package cloudos.notifications;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import cloudos.notifications.Notification;
import cloudos.notifications.NotificationCategory;
import cloudos.notifications.NotificationChannel;
import cloudos.notifications.NotificationCountResponse;
import cloudos.notifications.NotificationRepository;
import cloudos.notifications.NotificationResponse;
import cloudos.notifications.NotificationSearchRequest;
import cloudos.notifications.NotificationSearchResponse;
import cloudos.notifications.NotificationType;
import cloudos.notifications.NotificationsController;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import test.CloudOSTest;

/** Created by philipperibeiro on 3/30/17. */
public class NotificationsControllerTest extends CloudOSTest {

  private MockMvc mvc;

  @Autowired
  private NotificationsController notificationsController;

  @Autowired
  private NotificationRepository notificationRepository;
  
  @Before
  public void setUp() throws Exception {
    this.mvc = MockMvcBuilders.standaloneSetup(notificationsController).build();
  }

  @Test
  public void testController() throws Exception {
    this.mvc.perform(get(NotificationsController.NOTIFICATIONS + "/"))
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testList() throws Exception {
    NotificationSearchRequest request = new NotificationSearchRequest(0, 10, null, null);
    this.mvc
        .perform(post(NotificationsController.NOTIFICATIONS + "/list")
            .content(super.gson.toJson(request)).contentType(contentType))
        .andExpect(status().is2xxSuccessful());
  }
  

  @Test
  public void deleteNotification() throws Exception {
    Notification notification = new Notification();
    notification.setCategory(NotificationCategory.INFO);
    notification.setType(NotificationType.ALERT);
    notification.setChannel(NotificationChannel.MONITORING);
    notification.setTitle(String.format("Alert deleted."));
    notification
        .setMessage(String.format("Alert not found in the provider."));
    notification =  notificationRepository.save(notification);
    this.mvc
        .perform(delete(NotificationsController.NOTIFICATIONS +"/"+ notification.getId()))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testCountUnread() throws Exception {
    String user = "admin";
    Mockito.when(requestScope.getUserName()).thenReturn(user);
    String contentAsString =
        this.mvc.perform(get(NotificationsController.NOTIFICATIONS + "/countUnread"))
            .andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsString();
    Assert.assertNotNull(Long.parseLong(contentAsString));
  }

  @Test
  public void testCount() throws Exception {
    String user = "admin";
    Mockito.when(requestScope.getUserName()).thenReturn(user);
    String contentAsString =
        this.mvc.perform(get(NotificationsController.NOTIFICATIONS + "/countNotifications"))
            .andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsString();
    NotificationCountResponse response =
        mapper.readValue(contentAsString, NotificationCountResponse.class);
    Assert.assertNotNull(response.getTotal());
    Assert.assertNotNull(response.getTotalError());
    Assert.assertNotNull(response.getTotalInfo());
    Assert.assertNotNull(response.getTotalSuccess());
    Assert.assertNotNull(response.getTotalUnread());
    Assert.assertNotNull(response.getTotalWarning());
  }

  @Test
  public void testMarkAsRead() throws Exception {
    String user = "admin";
    Mockito.when(requestScope.getUserName()).thenReturn(user);

    NotificationSearchRequest request = new NotificationSearchRequest(0, 10, null, true);
    // retrieve a notification
    String response = this.mvc
        .perform(post(NotificationsController.NOTIFICATIONS + "/list")
            .content(super.gson.toJson(request)).contentType(contentType))
        .andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsString();
    List<Notification> notifications = mapper.readValue(response,
        NotificationSearchResponse.class).getNotifications();

    if (notifications == null || notifications.size() == 0) {
      return;
    }

    Notification notification = notifications.get(0);

    // verify if user has read the notification
    boolean wasRead = false;
    if (notification.getReadUsers() != null && notification.getReadUsers().contains(user)) {
      // if contains
      wasRead = true;

      // mark as unread
      notification = this.markAsUnread(notification.getId());
    }

    // mark as read
    notification = this.markAsRead(notification.getId());

    if (!wasRead) {

      // mark as unread
      notification = this.markAsUnread(notification.getId());
    }
  }

  private Notification markAsRead(String notificationId) throws Exception {
    String contentAsString2 = this.mvc
        .perform(post(NotificationsController.NOTIFICATIONS + "/markAsRead/" + notificationId))
        .andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsString();
    NotificationResponse notificationReturned =
        mapper.readValue(contentAsString2, NotificationResponse.class);
    return notificationReturned.getNotification();
  }

  private Notification markAsUnread(String notificationId) throws Exception {
    String contentAsString2 = this.mvc
        .perform(post(NotificationsController.NOTIFICATIONS + "/markAsUnread/" + notificationId))
        .andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsString();
    NotificationResponse notificationReturned =
        mapper.readValue(contentAsString2, NotificationResponse.class);
    return notificationReturned.getNotification();
  }
}
