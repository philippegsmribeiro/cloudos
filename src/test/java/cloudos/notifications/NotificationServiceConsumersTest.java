package cloudos.notifications;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import cloudos.machinelearning.autoscaler.AutoscaleLabels;
import cloudos.models.alerts.GenericAlert;
import cloudos.models.autoscaler.AutoscaleRequest;
import cloudos.queue.message.autoscale.AutoscaleMessage;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import test.CloudOSTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NotificationServiceConsumersTest extends CloudOSTest {

  @Autowired
  NotificationsService notificationsService;

  @Autowired
  NotificationRepository notificationRepository;

  @Test
  public void createAutoscalerMessage() {
    // not readonly
    String instanceInfo = "iTest2";
    String cloudGroupId = "cgTest2";
    AutoscaleLabels label = AutoscaleLabels.UPSCALE;

    AutoscaleMessage message = new AutoscaleMessage();
    AutoscaleRequest autoscaleRequest = AutoscaleRequest.builder().instanceInfo(instanceInfo)
        .cloudGroupId(cloudGroupId).label(label).readonly(false).build();
    message.setAutoscaleRequest(autoscaleRequest);
    String notificationId = notificationsService.createAutoscalerMessage(message);

    Notification notification = notificationRepository.findOne(notificationId);
    assertNotNull(notification);
    assertEquals(notification.getTitle(), NotificationsService.AUTOSCALER_TITLE);
    assertEquals(notification.getMessage(),
        String.format(NotificationsService.AUTOSCALER_MESSAGE, cloudGroupId, instanceInfo, label));
  }

  @Test
  public void createAutoscalerMessageReadOnly() {
    // readonly
    String instanceInfo = "iTest";
    String cloudGroupId = "cgTest";
    AutoscaleLabels label = AutoscaleLabels.DOWNSCALE;

    AutoscaleMessage message = new AutoscaleMessage();
    AutoscaleRequest autoscaleRequest = AutoscaleRequest.builder().instanceInfo(instanceInfo)
        .cloudGroupId(cloudGroupId).label(label).readonly(true).build();
    message.setAutoscaleRequest(autoscaleRequest);
    String notificationId = notificationsService.createAutoscalerMessage(message);

    Notification notification = notificationRepository.findOne(notificationId);
    assertNotNull(notification);
    assertEquals(notification.getTitle(), NotificationsService.AUTOSCALER_TITLE);
    assertEquals(notification.getMessage(), String.format(
        NotificationsService.AUTOSCALER_READONLY_MESSAGE, cloudGroupId, instanceInfo, label));
  }

  @Test
  public void createFileSystemInsightNotification() {
    String message = "test";
    String notificationId = notificationsService.createFileSystemInsightNotification(message);

    Notification notification = notificationRepository.findOne(notificationId);
    assertNotNull(notification);
    assertEquals(notification.getTitle(), NotificationsService.FILESYSTEM_INSIGHT_TITLE);
    assertEquals(notification.getMessage(), message);
  }

  @Test
  public void createMemoryInsightNotification() {
    String message = "test";
    String notificationId = notificationsService.createMemoryInsightNotification(message);

    Notification notification = notificationRepository.findOne(notificationId);
    assertNotNull(notification);
    assertEquals(notification.getTitle(), NotificationsService.MEMORY_INSIGHT_TITLE);
    assertEquals(notification.getMessage(), message);
  }

  @Test
  public void createCpuInsightNotification() {
    String message = "test";
    String notificationId = notificationsService.createCpuInsightNotification(message);

    Notification notification = notificationRepository.findOne(notificationId);
    assertNotNull(notification);
    assertEquals(notification.getTitle(), NotificationsService.CPU_INSIGHT_TITLE);
    assertEquals(notification.getMessage(), message);
  }

  @Test
  public void createProcessInsightNotification() {
    String message = "test";
    String notificationId = notificationsService.createProcessInsightNotification(message);

    Notification notification = notificationRepository.findOne(notificationId);
    assertNotNull(notification);
    assertEquals(notification.getTitle(), NotificationsService.PROCESS_INSIGHT_TITLE);
    assertEquals(notification.getMessage(), message);
  }
  
  @Test
  public void createNewAlertNotification() {
    String alarmName = "test";
    GenericAlert message = new GenericAlert();
    message.setAlarmName(alarmName);
    String notificationId = notificationsService.createNewAlertNotification(message);
    
    Notification notification = notificationRepository.findOne(notificationId);
    assertNotNull(notification);
    assertEquals(notification.getTitle(), NotificationsService.ALERT_CREATED_TITLE);
    assertEquals(notification.getMessage(), String.format(NotificationsService.ALERT_CREATED_MESSAGE, alarmName));
  }
  
  @Test
  public void createDeletedAlertNotification() {
    String alarmName = "test";
    GenericAlert message = new GenericAlert();
    message.setAlarmName(alarmName);
    String notificationId = notificationsService.createDeletedAlertNotification(message);
    
    Notification notification = notificationRepository.findOne(notificationId);
    assertNotNull(notification);
    assertEquals(notification.getTitle(), NotificationsService.ALERT_DELETED_TITLE);
    assertEquals(notification.getMessage(), String.format(NotificationsService.ALERT_DELETED_MESSAGE, alarmName));
  }
  
  @Test
  public void createAlertUpdateNotification() {
    String alarmName = "test";
    GenericAlert message = new GenericAlert();
    message.setAlarmName(alarmName);
    String notificationId = notificationsService.createAlertUpdateNotification(message);
    
    Notification notification = notificationRepository.findOne(notificationId);
    assertNotNull(notification);
    assertEquals(notification.getTitle(), NotificationsService.ALERT_UPDATED_TITLE);
    assertEquals(notification.getMessage(), String.format(NotificationsService.ALERT_UPDATED_MESSAGE, alarmName));
  }



};


