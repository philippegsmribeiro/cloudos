package cloudos;

import cloudos.healthchecker.ResourceChart;
import cloudos.healthchecker.ResourceChartRepository;
import cloudos.instances.InstanceService;
import cloudos.models.Instance;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import test.CloudOSTest;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class HealthCheckerControllerTest extends CloudOSTest {

    private MockMvc mvc;

    private Instance instance;

    @Autowired
    private HealthCheckerController healthCheckerController;

    @Autowired
    InstanceService instanceService;

    @Autowired
    ResourceChartRepository resourceChartRepository;

    @Before
    public void setUp() throws Exception {

        //Remove the existing charts to prevent serialization error. Todo: Remove this
        //this.resourceChartRepository.deleteAll();

        this.mvc = MockMvcBuilders.standaloneSetup(healthCheckerController).build();
        this.instance = Instance.builder()
                .id(generator.generate(5))
                .providerId(generator.generate(5))
                .cost(1.04)
                .clientInstalled(true)
                .ipAddress("127.0.0.1")
                .region("us-west-1")
                .provider(Providers.AMAZON_AWS)
                .zone("us-west-1a").build();

        // check if the instance exists ... if it does then remove it.
        if (this.instanceService.findById(this.instance.getId()) != null) {
            this.instanceService.delete(this.instance.getId());
        }

        this.instanceService.save(this.instance);

        Map<Long, BigDecimal> chart = new HashMap<>();

        chart.put(1L, BigDecimal.TEN);
        chart.put(2L, BigDecimal.TEN);
        chart.put(3L, BigDecimal.TEN);

        ResourceChart resourceChart = ResourceChart.builder()
                .resourceId(this.instance.getId())
                .timestamp(new Date())
                .chart(chart)
                .build();

        this.resourceChartRepository.save(resourceChart);
    }

    @Test
    public void testHealthChecker() throws Exception {
        this.mvc
                .perform(get(HealthCheckerController.HEALTHCHECKER))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testGetInstance() throws Exception {
        String id = instance.getId();
        this.mvc
                .perform(get(HealthCheckerController.HEALTHCHECKER + "/" + id))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.ipAddress", is("127.0.0.1")))
                .andExpect(jsonPath("$.clientInstalled", is(true)));
    }

    @Test
    public void testGetResourcesCost() throws Exception {
        this.mvc
                .perform(get(HealthCheckerController.HEALTHCHECKER + "/get_resources_cost"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.cost", greaterThanOrEqualTo(0.0)));
    }

    @Test
    public void testDeleteInstance() throws Exception {
        // save the existing instance.
        Instance newInstance = Instance.builder()
                .id(generator.generate(5))
                .providerId(generator.generate(5))
                .cost(1.04)
                .clientInstalled(true)
                .ipAddress("127.0.0.1")
                .region("us-west-1")
                .provider(Providers.AMAZON_AWS)
                .zone("us-west-1a").build();
        this.instanceService.save(newInstance);

        Instance instance = this.instanceService.findById(newInstance.getId());
        String id = instance.getId();
        this.mvc
                .perform(delete(HealthCheckerController.HEALTHCHECKER + "/" + id))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetResourceCost() throws Exception {
        /*List<Instance> instanceList =
                instanceService.findAllActiveInstancesByProvider(Providers.AMAZON_AWS);
        assertNotNull(instanceList);
        if (instanceList.size() == 0) {
            logger.warn("No instances found at the repository ... skipping test");
            return;
        }*/
        // let's get a single instance...
        //Instance instance = instanceList.get(0);

        this.mvc
                .perform(
                        get(
                                HealthCheckerController.HEALTHCHECKER
                                        + "/get_resource_cost/"
                                        + this.instance.getId()))
                .andExpect(status().isOk());
    }
}
