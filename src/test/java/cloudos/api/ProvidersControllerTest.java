package cloudos.api;

import static org.assertj.core.api.Assertions.fail;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import cloudos.Providers;
import cloudos.provider.ProvidersService;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import test.CloudOsControllerTest;

public class ProvidersControllerTest extends CloudOsControllerTest {

  private MockMvc mvc;
  private ProvidersController providersController;
  private ProvidersService providersService;

  List<Providers> providers =
      Arrays.asList(Providers.AMAZON_AWS, Providers.GOOGLE_COMPUTE_ENGINE);
  
  @Before
  public void setUp() throws Exception {
    providersService = mock(ProvidersService.class);
    providersController = new ProvidersController(providersService);
    mvc = MockMvcBuilders.standaloneSetup(providersController).build();
  }

  @Test
  public void retrieveProvidersTest() throws Exception {
    MockHttpServletResponse response = super.executeGet(mvc, ProvidersController.PROVIDERS);
    assertEquals(response.getStatus(), HttpStatus.OK.value());
    
    // exception
    when(providersService.retrieveSupportedProviders()).thenThrow(new RuntimeException());
    response = super.executeGet(mvc, ProvidersController.PROVIDERS);
    assertEquals(response.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR.value());
  }

  @Test
  public void retrieveRegionsTest() throws Exception {
    MockHttpServletResponse response = super.executeGet(mvc, ProvidersController.PROVIDERS + ProvidersController.REGIONS);
    assertEquals(response.getStatus(), HttpStatus.OK.value());
    
    // exception
    when(providersService.retrieveRegions()).thenThrow(new RuntimeException());
    response = super.executeGet(mvc, ProvidersController.PROVIDERS + ProvidersController.REGIONS);
    assertEquals(response.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR.value());
  }
  
  @Test
  public void retrieveRegionsWithZonesTest() throws Exception {
    MockHttpServletResponse response = super.executeGet(mvc, ProvidersController.PROVIDERS + ProvidersController.REGIONS_AVAIBILITY_ZONES);
    assertEquals(response.getStatus(), HttpStatus.OK.value());
    
    // exception
    when(providersService.retrieveRegionsWithZones()).thenThrow(new RuntimeException());
    response = super.executeGet(mvc, ProvidersController.PROVIDERS + ProvidersController.REGIONS_AVAIBILITY_ZONES);
    assertEquals(response.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR.value());
  }
  
  @Test
  public void retrieveRegionsByProviderTest() throws Exception {
    providers.forEach(p -> {
      try {
        MockHttpServletResponse response = super.executeGet(mvc, ProvidersController.PROVIDERS + ProvidersController.REGIONS_BY_PROVIDER, p);
        assertEquals(response.getStatus(), HttpStatus.OK.value());
      } catch (Exception e) {
        fail("fail!", e);
      }
    });
    
    // exception
    when(providersService.retrieveRegions(any())).thenThrow(new RuntimeException());
    providers.forEach(p -> {
      try {
        MockHttpServletResponse response = super.executeGet(mvc, ProvidersController.PROVIDERS + ProvidersController.REGIONS_BY_PROVIDER, p);
        assertEquals(response.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR.value());
      } catch (Exception e) {
        fail("fail!", e);
      }
    });
  }
  
  @Test
  public void retrieveRegionsAvaibilityZonesByProviderTest() throws Exception {
    providers.forEach(p -> {
      try {
        MockHttpServletResponse response = super.executeGet(mvc, ProvidersController.PROVIDERS + ProvidersController.REGIONS_AVAIBILITY_ZONES_BY_PROVIDER, p);
        assertEquals(response.getStatus(), HttpStatus.OK.value());
      } catch (Exception e) {
        fail("fail!", e);
      }
    });
    
    // exception
    when(providersService.retrieveRegionsWithZones(any())).thenThrow(new RuntimeException());
    providers.forEach(p -> {
      try {
        MockHttpServletResponse response = super.executeGet(mvc, ProvidersController.PROVIDERS + ProvidersController.REGIONS_AVAIBILITY_ZONES_BY_PROVIDER, p);
        assertEquals(response.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR.value());
      } catch (Exception e) {
        fail("fail!", e);
      }
    });
  }
  
  @Test
  public void retrieveImagesTest() throws Exception {
    MockHttpServletResponse response = super.executeGet(mvc, ProvidersController.PROVIDERS + ProvidersController.IMAGES);
    assertEquals(response.getStatus(), HttpStatus.OK.value());
    
    // exception
    when(providersService.retrieveImages()).thenThrow(new RuntimeException());
    response = super.executeGet(mvc, ProvidersController.PROVIDERS + ProvidersController.IMAGES);
    assertEquals(response.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR.value());
  }
  
  @Test
  public void retrieveImagesByProviderTest() throws Exception {
    providers.forEach(p -> {
      try {
        MockHttpServletResponse response = super.executeGet(mvc, ProvidersController.PROVIDERS + ProvidersController.IMAGES_BY_PROVIDER, p);
        assertEquals(response.getStatus(), HttpStatus.OK.value());
      } catch (Exception e) {
        fail("fail!", e);
      }
    });
    
    // exception
    when(providersService.retrieveImages(any())).thenThrow(new RuntimeException());
    providers.forEach(p -> {
      try {
        MockHttpServletResponse response = super.executeGet(mvc, ProvidersController.PROVIDERS + ProvidersController.IMAGES_BY_PROVIDER, p);
        assertEquals(response.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR.value());
      } catch (Exception e) {
        fail("fail!", e);
      }
    });
  }
  
  @Test
  public void retrieveMachineTypesTest() throws Exception {
    MockHttpServletResponse response = super.executeGet(mvc, ProvidersController.PROVIDERS + ProvidersController.MACHINE_TYPES);
    assertEquals(response.getStatus(), HttpStatus.OK.value());
    
    // exception
    when(providersService.retrieveMachineTypes()).thenThrow(new RuntimeException());
    response = super.executeGet(mvc, ProvidersController.PROVIDERS + ProvidersController.MACHINE_TYPES);
    assertEquals(response.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR.value());
  }
  
  @Test
  public void retrieveMachineTypesByProviderTest() throws Exception {
    providers.forEach(p -> {
      try {
        MockHttpServletResponse response = super.executeGet(mvc, ProvidersController.PROVIDERS + ProvidersController.MACHINE_BY_PROVIDER, p);
        assertEquals(response.getStatus(), HttpStatus.OK.value());
      } catch (Exception e) {
        fail("fail!", e);
      }
    });
    
    // exception
    when(providersService.retrieveMachineTypes(any())).thenThrow(new RuntimeException());
    providers.forEach(p -> {
      try {
        MockHttpServletResponse response = super.executeGet(mvc, ProvidersController.PROVIDERS + ProvidersController.MACHINE_BY_PROVIDER, p);
        assertEquals(response.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR.value());
      } catch (Exception e) {
        fail("fail!", e);
      }
    });
  }
  
  @Test
  public void retrieveKeysByProviderTest() throws Exception {
    providers.forEach(p -> {
      try {
        MockHttpServletResponse response = super.executeGet(mvc, ProvidersController.PROVIDERS + ProvidersController.KEYS_BY_PROVIDER, p);
        assertEquals(response.getStatus(), HttpStatus.OK.value());
      } catch (Exception e) {
        fail("fail!", e);
      }
    });
    
    // exception
    when(providersService.retrieveKeys(any())).thenThrow(new RuntimeException());
    providers.forEach(p -> {
      try {
        MockHttpServletResponse response = super.executeGet(mvc, ProvidersController.PROVIDERS + ProvidersController.KEYS_BY_PROVIDER, p);
        assertEquals(response.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR.value());
      } catch (Exception e) {
        fail("fail!", e);
      }
    });
  }
  
  @Test
  public void retrieveSecurityGroupByProviderTest() throws Exception {
    providers.forEach(p -> {
      try {
        MockHttpServletResponse response = super.executeGet(mvc, ProvidersController.PROVIDERS + ProvidersController.SECURITY_GROUPS_BY_PROVIDER, p);
        assertEquals(response.getStatus(), HttpStatus.OK.value());
      } catch (Exception e) {
        fail("fail!", e);
      }
    });
    
    // exception
    when(providersService.retrieveSecurityGroups(any())).thenThrow(new RuntimeException());
    providers.forEach(p -> {
      try {
        MockHttpServletResponse response = super.executeGet(mvc, ProvidersController.PROVIDERS + ProvidersController.SECURITY_GROUPS_BY_PROVIDER, p);
        assertEquals(response.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR.value());
      } catch (Exception e) {
        fail("fail!", e);
      }
    });
  }
}
