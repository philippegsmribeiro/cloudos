package cloudos.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import cloudos.Providers;
import cloudos.api.DeploymentController;
import cloudos.deploy.deployers.Deployers;
import cloudos.deploy.deployments.Deployment;
import cloudos.deploy.deployments.DeploymentStatus;
import cloudos.models.CloudCreateRequest;

import java.io.File;
import java.nio.file.Files;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import test.CloudOSTest;

public class DeploymentControllerTest extends CloudOSTest {

  private MockMvc mvc;

  private String gcloud_zone = "us-west1-a";
  private String gcloud_region = "us-west1";
  private String gcloud_machine = "f1-micro";
  private String gcloud_imageProject = "ubuntu-os-cloud";
  private String gcloud_imageImage = "ubuntu-1604-xenial-v20161221";

  private final String aws_securityGroup = "default";
  private final String aws_keyPairName = "gcloudsshkey";
  private final String aws_region = "us-east-1";
  private final String aws_zone = "us-east-1a";
  private final String aws_machine = "m1.small";
  private final String aws_image = "ami-1acdd90d";

  @Autowired private DeploymentController deploymentRestController;

  @Before
  public void setUp() throws Exception {
    this.mvc = MockMvcBuilders.standaloneSetup(deploymentRestController).build();
    // clean the repository before start testing.
  }

  @Test
  public void testController() throws Exception {
    this.mvc
        .perform(get(DeploymentController.DEPLOYMENT + "/"))
        .andExpect(status().is4xxClientError());
  }

  @Test
  @SuppressWarnings("unchecked")
  public void testList() throws Exception {
    String json = this.mvc
        .perform(get(DeploymentController.DEPLOYMENT + "/list/0/100"))
        .andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsString();
    HashMap<String,Object> pageDeployment = super.mapper.readValue(json, HashMap.class);
    assertNotNull(pageDeployment);
    assertEquals(0, pageDeployment.get("number"));
  }

  @Test
  @SuppressWarnings("unchecked")
  public void testListStatus() throws Exception {
    byte[] json = this.mvc
        .perform(
            get(DeploymentController.DEPLOYMENT + "/list/0/100/" + DeploymentStatus.ERROR.name()))
        .andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsByteArray();
    HashMap<String,Object> pageDeployment = super.mapper.readValue(json, HashMap.class);
    assertNotNull(pageDeployment);
    assertEquals(0, pageDeployment.get("number"));
  }

  @Test
  public void testRetrieval() throws Exception {
    this.mvc
        .perform(get(DeploymentController.DEPLOYMENT + "/" + new Deployment().getId()))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  @Ignore
  public void testRestart() throws Exception {
    this.mvc
        .perform(
            post(DeploymentController.DEPLOYMENT + "/" + new Deployment().getId() + "/restart"))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  @Ignore
  public void testCreateWrongConfiguration() throws Exception {
    Deployment deployment = new Deployment();
    deployment.setConfiguration("abc");
    deployment.setInstancesNumber(1);
    deployment.setDeployer(Deployers.ANSIBLE);
    deployment.setProvider(Providers.AMAZON_AWS);
    deployment.setCloudCreateRequest(
        CloudCreateRequest.createAwsRequest(
            aws_region,
            aws_machine,
            aws_image,
            aws_securityGroup,
            deployment.getInstancesNumber(),
            deployment.getInstancesNumber(),
            aws_keyPairName,
            aws_zone,
            null));
    String content = gson.toJson(deployment);
    this.mvc
        .perform(
            post(DeploymentController.DEPLOYMENT + "/create")
                .content(content)
                .contentType(contentType))
        .andExpect(status().isBadRequest());
  }

  @Test
  @Ignore
  public void testCreateAWS() throws Exception {
    Deployment deployment = new Deployment();
    final byte[] config =
        Files.readAllBytes(new File(this.getClass().getResource("test.txt").toURI()).toPath());
    deployment.setConfiguration(new String(config));
    deployment.setInstancesNumber(1);
    deployment.setDeployer(Deployers.ANSIBLE);
    deployment.setProvider(Providers.AMAZON_AWS);
    deployment.setCloudCreateRequest(
        CloudCreateRequest.createAwsRequest(
            aws_region,
            aws_machine,
            aws_image,
            aws_securityGroup,
            deployment.getInstancesNumber(),
            deployment.getInstancesNumber(),
            aws_keyPairName,
            aws_zone,
            null));
    String content = gson.toJson(deployment);
    this.mvc
        .perform(
            post(DeploymentController.DEPLOYMENT + "/create")
                .content(content)
                .contentType(contentType))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  @Ignore
  public void testCreateGoogle() throws Exception {
    Deployment deployment = new Deployment();
    final byte[] config =
        Files.readAllBytes(new File(this.getClass().getResource("test.txt").toURI()).toPath());
    deployment.setConfiguration(new String(config));
    deployment.setInstancesNumber(1);
    deployment.setDeployer(Deployers.ANSIBLE);
    deployment.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
    deployment.setCloudCreateRequest(
        CloudCreateRequest.createGCloudRequest(
            gcloud_region,
            "testdeploymentsrest",
            gcloud_machine,
            gcloud_imageImage,
            gcloud_imageProject,
            1,
            aws_keyPairName,
            gcloud_zone));
    String content = gson.toJson(deployment);
    this.mvc
        .perform(
            post(DeploymentController.DEPLOYMENT + "/create")
                .content(content)
                .contentType(contentType))
        .andExpect(status().is2xxSuccessful());
  }
}
