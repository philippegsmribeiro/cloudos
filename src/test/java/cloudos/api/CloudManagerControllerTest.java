package cloudos.api;

import static cloudos.Providers.ALIBABA_ALIYUN;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Subnet;
import com.amazonaws.services.ec2.model.Vpc;
import com.fasterxml.jackson.databind.ObjectMapper;
import cloudos.AmazonService;
import cloudos.Providers;
import cloudos.SecurityGroupManagerService;
import cloudos.amazon.VPC;
import cloudos.api.CloudManagerController;
import cloudos.exceptions.NotImplementedException;
import cloudos.keys.KeyManagerService;
import cloudos.keys.cloudoskey.CloudosKey;
import cloudos.models.ActionType;
import cloudos.models.AwsSecurityGroup;
import cloudos.models.network.AwsVpc;
import cloudos.models.CloudActionRequest;
import cloudos.models.CloudCreateKeyRequest;
import cloudos.models.CloudCreateRequest;
import cloudos.models.CloudosSecurityGroup;
import cloudos.models.network.CloudosSubnetRequest;
import cloudos.models.network.CloudosVpc;
import cloudos.models.network.CloudosVpcRequest;
import cloudos.network.CloudosSubnetManagement;
import cloudos.network.CloudosVpcCache;
import cloudos.network.CloudosVpcService;
import test.CloudOSTest;

/** Created by philipperibeiro on 12/16/16. */
public class CloudManagerControllerTest extends CloudOSTest {

  private MockMvc mvc;

  private static final String region = "us-east-1";
  private static final String zone = "us-east-1a";
  private static final String securityGroup = "default";
  private static final String key = "gcloudsshkey";

  @Autowired
  private CloudManagerController cloudManagerController;

  @Autowired
  private AmazonService amazonService;

  @Autowired
  private KeyManagerService keyManagerService;

  @Autowired
  private CloudosVpcService cloudosVpcService;

  @Autowired
  private SecurityGroupManagerService securityGroupManagerService;

  @Autowired
  private CloudosSubnetManagement subnetManagement;

  @Autowired
  private CloudosVpcCache cloudosVpcCache;

  @Before
  public void setUp() throws Exception {
    this.mvc = MockMvcBuilders.standaloneSetup(cloudManagerController).build();
    // clean the repository before start testing.
  }

  @Test
  public void testcloud_manager() throws Exception {
    // the /cloud_manager is not supported and not accessible
    this.mvc.perform(get(CloudManagerController.CLOUDMANAGER + ""))
        .andExpect(status().is4xxClientError());
  }

  @Test
  @Ignore
  public void testCreateInstances() throws Exception {
    // the URL must exist but not allow for get requests
    this.mvc.perform(get(CloudManagerController.CLOUDMANAGER + "/create"))
        .andExpect(status().is4xxClientError());
    // test with a valid provider
    CloudCreateRequest cloudCreateRequest = CloudCreateRequest.createAwsRequest(region, "t2.micro",
        "ami-e13739f6", securityGroup, 1, 1, key, zone, null);
    // create a CloudCreateRequest request object
    String content = gson.toJson(cloudCreateRequest);
    this.mvc.perform(post(CloudManagerController.CLOUDMANAGER + "/create").content(content)
        .contentType(contentType)).andExpect(status().is2xxSuccessful());
    // test with an unimplemented provider
    content = gson.toJson(ALIBABA_ALIYUN);
    this.mvc.perform(post(CloudManagerController.CLOUDMANAGER + "/create").content(content)
        .contentType(contentType)).andExpect(status().is4xxClientError());
  }

  @Test
  public void testTerminateInstances() throws Exception {
    // the URL must exist but not allow for get requests
    this.mvc.perform(get(CloudManagerController.CLOUDMANAGER + "/terminate"))
        .andExpect(status().is4xxClientError());
    // test with a valid provider
    Set<Instance> instances = this.amazonService.listInstances(region, securityGroup);
    List<String> list =
        instances.stream().map(instance -> instance.getInstanceId()).collect(Collectors.toList());
    CloudActionRequest request = CloudActionRequest.createAwsRequest(list, region, null,
        securityGroup, ActionType.TERMINATE);
    String content = gson.toJson(request);
    ResultActions perform =
        this.mvc.perform(post(CloudManagerController.CLOUDMANAGER + "/terminate").content(content)
            .contentType(contentType));
    if (CollectionUtils.isNotEmpty(instances)) {
      perform.andExpect(status().is2xxSuccessful());
    } else {
      perform.andExpect(status().is4xxClientError());
    }
  }

  @Test
  public void testRebootInstances() throws Exception {
    // the URL must exist but not allow for get requests
    this.mvc.perform(get(CloudManagerController.CLOUDMANAGER + "/reboot"))
        .andExpect(status().is4xxClientError());
    Set<Instance> instances = this.amazonService.listInstances(region, securityGroup);
    List<String> list =
        instances.stream().map(instance -> instance.getInstanceId()).collect(Collectors.toList());
    CloudActionRequest request =
        CloudActionRequest.createAwsRequest(list, region, null, securityGroup, ActionType.RESTART);
    // test with a valid provider
    String content = gson.toJson(request);
    ResultActions perform = this.mvc.perform(post(CloudManagerController.CLOUDMANAGER + "/reboot")
        .content(content).contentType(contentType));
    if (CollectionUtils.isNotEmpty(instances)) {
      perform.andExpect(status().is2xxSuccessful());
    } else {
      perform.andExpect(status().is4xxClientError());
    }
  }

  @Test
  public void testListInstances() throws Exception {
    // the URL must exist but not allow for get requests
    this.mvc.perform(get(CloudManagerController.CLOUDMANAGER + "/list"))
        .andExpect(status().is4xxClientError());
    // test with a valid provider
    CloudActionRequest request =
        CloudActionRequest.createAwsRequest(null, region, null, securityGroup, ActionType.FETCH);
    String content = gson.toJson(request);
    this.mvc.perform(post(CloudManagerController.CLOUDMANAGER + "/list").content(content)
        .contentType(contentType)).andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testStartInstances() throws Exception {
    // the URL must exist but not allow for get requests
    this.mvc.perform(get(CloudManagerController.CLOUDMANAGER + "/start"))
        .andExpect(status().is4xxClientError());
    Set<Instance> instances = this.amazonService.listInstances(region, securityGroup);
    List<String> list =
        instances.stream().map(instance -> instance.getInstanceId()).collect(Collectors.toList());
    CloudActionRequest request =
        CloudActionRequest.createAwsRequest(list, region, null, securityGroup, ActionType.START);
    request.setSynchronous(false);
    // test with a valid provider
    String content = gson.toJson(request);
    ResultActions perform = this.mvc.perform(post(CloudManagerController.CLOUDMANAGER + "/start")
        .content(content).contentType(contentType));
    if (CollectionUtils.isNotEmpty(instances)) {
      perform.andExpect(status().is2xxSuccessful());
    } else {
      perform.andExpect(status().is4xxClientError());
    }
  }

  @Test
  public void testStopInstances() throws Exception {
    // the URL must exist but not allow for get requests
    this.mvc.perform(get(CloudManagerController.CLOUDMANAGER + "/stop"))
        .andExpect(status().is4xxClientError());
    // test with a valid provider
    Set<Instance> instances = this.amazonService.listInstances(region, securityGroup);
    List<String> list =
        instances.stream().map(instance -> instance.getInstanceId()).collect(Collectors.toList());
    CloudActionRequest request =
        CloudActionRequest.createAwsRequest(list, region, null, securityGroup, ActionType.STOP);
    String content = gson.toJson(request);
    ResultActions perform = this.mvc.perform(post(CloudManagerController.CLOUDMANAGER + "/stop")
        .content(content).contentType(contentType));
    if (CollectionUtils.isNotEmpty(instances)) {
      perform.andExpect(status().is2xxSuccessful());
    } else {
      perform.andExpect(status().is4xxClientError());
    }
  }

  @Test
  public void testCreateKeyAWS() throws Exception {
    this.testCreateKeys(Providers.AMAZON_AWS);
  }

  @Test
  public void testCreateKeyGoogle() throws Exception {
    this.testCreateKeys(Providers.GOOGLE_COMPUTE_ENGINE);
  }

  public void testCreateKeys(Providers provider) throws Exception {
    String keyName = generator.generate(10);
    // the URL must exist but not allow for get requests
    this.mvc.perform(get(CloudManagerController.CLOUDMANAGER + "/create_keys"))
        .andExpect(status().is4xxClientError());

    // test with a valid provider
    this.keyManagerService.deleteKey(keyName, provider, region);

    CloudCreateKeyRequest cloudCreateKeyRequest =
        new CloudCreateKeyRequest(provider, region, keyName);
    String payload = gson.toJson(cloudCreateKeyRequest);

    ResultActions perform =
        this.mvc.perform(post(CloudManagerController.CLOUDMANAGER + "/create_keys").content(payload)
            .contentType(contentType));

    perform.andDo(h -> logger.debug(h.getResponse().getContentAsString()));

    perform.andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDeleteKeys() throws Exception {
    String keyName = generator.generate(10);
    // the URL must exist but not allow for get requests
    this.mvc.perform(get(CloudManagerController.CLOUDMANAGER + "/delete_keys"))
        .andExpect(status().is4xxClientError());

    // test with a valid provider
    CloudCreateKeyRequest cloudCreateKeyRequest =
        new CloudCreateKeyRequest(Providers.GOOGLE_COMPUTE_ENGINE, region, keyName);
    ResponseEntity<?> key = this.cloudManagerController.createKeys(cloudCreateKeyRequest);
    assert key.getStatusCode().is2xxSuccessful();

    CloudosKey cloudosKey = (CloudosKey) key.getBody();
    this.mvc.perform(post(CloudManagerController.CLOUDMANAGER + "/delete_keys")
        .param("cloudosKeyId", cloudosKey.getId())).andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testListKeys() throws Exception {
    // the URL must exist but not allow for get requests
    this.mvc.perform(post(CloudManagerController.CLOUDMANAGER + "/list_keys"))
        .andExpect(status().is4xxClientError());
    // test with a valid provider
    this.mvc
        .perform(
            get(CloudManagerController.CLOUDMANAGER + "/list_keys").param("region", "us-west-1"))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testListKeysGrouped() throws Exception {
    String url = String.format(CloudManagerController.CLOUDMANAGER + "/list_keys_grouped/%s",
        Providers.AMAZON_AWS.toString());

    // the URL must exist but not allow for get requests
    this.mvc.perform(post(url)).andExpect(status().is4xxClientError());
    // test with a valid provider
    this.mvc.perform(get(url)).andExpect(status().is2xxSuccessful());
  }

  @Test
  @Ignore
  public void testCreateSecurityGroup() throws Exception {

    String securityGroupName = String.format("cos-sg-%s", generator.generate(5));
    String securityGroupDescription = "Security Group for test purpose.";

    // the URL must exist but not allow for get requests
    this.mvc.perform(get(CloudManagerController.CLOUDMANAGER + "/create_security_group"))
        .andExpect(status().is4xxClientError());

    AwsSecurityGroup awsSecurityGroup = new AwsSecurityGroup();
    awsSecurityGroup.setName(securityGroupName);
    awsSecurityGroup.setRegion(region);
    awsSecurityGroup.setDescription(securityGroupDescription);

    ObjectMapper mapper = new ObjectMapper();

    String payload = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(awsSecurityGroup);

    ResultActions perform =
        this.mvc.perform(post(CloudManagerController.CLOUDMANAGER + "/create_security_group")
            .content(payload).contentType(contentType));

    String contentAsString = perform.andExpect(status().is2xxSuccessful()).andReturn().getResponse()
        .getContentAsString();

    // cleanup stored data
    CloudosSecurityGroup cloudosSecurityGroupCreated =
        mapper.readValue(contentAsString, CloudosSecurityGroup.class);

    this.securityGroupManagerService.delete(cloudosSecurityGroupCreated);
  }

  @Test
  public void testListSecurityGroup() throws Exception {
    String url = String.format(CloudManagerController.CLOUDMANAGER + "/list_security_group/%s",
        Providers.AMAZON_AWS.toString());

    // the URL must exist but not allow for get requests
    this.mvc.perform(post(url)).andExpect(status().is4xxClientError());
    // test with a valid provider
    this.mvc.perform(get(url)).andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testGetSecurityGroup() throws Exception {

    String url = String.format(CloudManagerController.CLOUDMANAGER + "/get_security_group/%s",
        "595964db398ebd51a484fb15");

    // the URL must exist but not allow for get requests
    this.mvc.perform(post(url)).andExpect(status().is4xxClientError());
    // test with a valid provider
    this.mvc.perform(get(url)).andExpect(status().is2xxSuccessful());
  }

  @Test
  @Ignore
  public void testDeleteSecurityGroup() throws Exception {
    // the URL must exist but not allow for get requests
    this.mvc.perform(get(CloudManagerController.CLOUDMANAGER + "/delete_security_group"))
        .andExpect(status().is4xxClientError());

    String securityGroupName = String.format("cos-sg-%s", generator.generate(5));
    String securityGroupDescription = "Security Group for delete test purpose.";

    AwsSecurityGroup awsSecurityGroup = new AwsSecurityGroup();
    awsSecurityGroup.setName(securityGroupName);
    awsSecurityGroup.setRegion(region);
    awsSecurityGroup.setDescription(securityGroupDescription);

    CloudosSecurityGroup cloudosSecurityGroup =
        this.securityGroupManagerService.create(awsSecurityGroup);

    String payload =
        this.mapper.writerWithDefaultPrettyPrinter().writeValueAsString(cloudosSecurityGroup);

    // test with a valid provider
    this.mvc.perform(post(CloudManagerController.CLOUDMANAGER + "/delete_security_group")
        .content(payload).contentType(contentType)).andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testGetAvailabilityZones() throws Exception {
    // the URL must exist but not allow for get requests
    this.mvc.perform(get(CloudManagerController.CLOUDMANAGER + "/availability_zones"))
        .andExpect(status().is4xxClientError());
    // test with a valid provider
    this.mvc.perform(post(CloudManagerController.CLOUDMANAGER + "/availability_zones")
        .param("region", "us-west-1")).andExpect(status().is5xxServerError());
  }

  @Test
  public void testCreateVpc() throws Exception {
    CloudosVpcRequest request = this.getRequest("us-east-1", "10.0.0.0/20");
    MvcResult result = this.mvc
        .perform(post(CloudManagerController.CLOUDMANAGER + "/vpc").content(gson.toJson(request))
            .contentType(contentType))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.region", is(request.getRegion()))).andReturn();

    // delete here to avoid dependencies.
    CloudosVpc cloudosVpc =
        this.mapper.readValue(result.getResponse().getContentAsString(), AwsVpc.class);

    // delete the vpc
    this.deleteVpc(cloudosVpc);
  }

  @Test
  public void testListVpc() throws Exception {
    this.mvc
        .perform(get(CloudManagerController.CLOUDMANAGER + "/vpc")
            .param("provider", Providers.AMAZON_AWS.toString())
            .param("region", "us-west-1"))
        .andExpect(status().isOk());
  }

  @Test
  public void testDescribeVpc() throws Exception {

    CloudosVpcRequest request = this.getRequest("us-west-1", "10.0.0.0/20");
    CloudosVpc cloudosVpc = this.cloudosVpcService.create(request);

    // describe a Vpc that does exist.
    this.mvc.perform(get(CloudManagerController.CLOUDMANAGER + "/vpc/" + cloudosVpc.getVpcId())
        .param("provider", Providers.AMAZON_AWS.toString())
        .param("region", cloudosVpc.getRegion()))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.region", is(cloudosVpc.getRegion())))
        .andExpect(jsonPath("$.name", is(cloudosVpc.getName())))
        .andExpect(jsonPath("$.vpcId", is(cloudosVpc.getVpcId())));

    // delete the vpc
    this.deleteVpc(cloudosVpc);
  }

  @Test
  @Ignore
  public void testDeleteVpc() throws Exception {
    CloudosVpcRequest request = this.getRequest("us-west-1", "10.0.0.0/20");

    CloudosVpc cloudosVpc = this.cloudosVpcService.create(request);

    this.mvc.perform(delete(CloudManagerController.CLOUDMANAGER + "/vpc/" + cloudosVpc.getVpcId())
        .param("provider", Providers.AMAZON_AWS.toString())
        .param("region", cloudosVpc.getRegion()))
        .andExpect(status().isOk());

    // delete the vpc
    this.deleteVpc(cloudosVpc);
  }

  @Test
  @Ignore
  public void testCreateSubnet() throws Exception {
    CloudosVpcRequest request = this.getRequest("us-east-1", "10.0.0.0/20");

    CloudosVpc cloudosVpc = this.cloudosVpcService.create(request);

    // create a cloudosSubnetRequest request
    CloudosSubnetRequest cloudosSubnetRequest = CloudosSubnetRequest.builder()
        .provider(Providers.AMAZON_AWS)
        .cidr("10.0.0.0/20")
        .availabilityZone(zone)
        .isIpv6(false)
        .isPrivateSubnet(false)
        .name(String.format("cloudos%s", generator.generate(8)))
        .build();

    this.mvc
        .perform(
            post(CloudManagerController.CLOUDMANAGER + "/vpc/" + cloudosVpc.getVpcId() + "/subnet")
                .content(gson.toJson(cloudosSubnetRequest)).contentType(contentType)
        .param("region", cloudosVpc.getRegion()))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.availabilityZone", is(cloudosSubnetRequest.getAvailabilityZone()))).andReturn();

    // delete the vpc
    this.deleteVpc(cloudosVpc);
  }

  @Test
  public void testListSubnet() throws Exception {
    // get or create the VPC object
    VPC vpc = this.cloudosVpcCache.getVpc("eu-west-1");
    List<Vpc> vpcs = vpc.listVpcs();
    if (vpcs != null && vpcs.size() > 0) {
      // describe a Vpc that does exist.
      this.mvc.perform(get(CloudManagerController.CLOUDMANAGER + "/vpc/" + vpcs.get(0).getVpcId())
          .param("provider", Providers.AMAZON_AWS.toString())
          .param("region", "eu-west-1"))
          .andExpect(status().isOk());
    }
  }

  @Test
  public void testDescribeSubnet() throws Exception {
    VPC vpc = this.cloudosVpcCache.getVpc("eu-west-1");
    List<Vpc> vpcs = vpc.listVpcs();
    if (vpcs != null && vpcs.size() > 0) {
      Vpc vpc1 = vpcs.get(0);
      Subnet subnet = vpc.listSubnets().get(0);
      // describe a Vpc that does exist.
      this.mvc.perform(get(CloudManagerController.CLOUDMANAGER + "/vpc/" + vpc1.getVpcId())
          .param("provider", Providers.AMAZON_AWS.toString())
          .param("region", "eu-west-1"))
          .andExpect(status().isOk())
          .andExpect(jsonPath("$.region", is("eu-west-1")))
          .andExpect(jsonPath("$.vpcId", is(subnet.getVpcId())));
    }
  }

  private void deleteVpc(CloudosVpc cloudosVpc) {
    try {
      this.cloudosVpcService.delete(cloudosVpc);
    } catch (NotImplementedException e) {
      logger.error(e);
    }
  }

  private CloudosVpcRequest getRequest(String region, String cidrBlock) {
    CloudosVpcRequest cloudosVpcRequest = new CloudosVpcRequest();
    cloudosVpcRequest.setRegion(region);
    cloudosVpcRequest.setCidrBlock(cidrBlock);
    cloudosVpcRequest.setProvider(Providers.AMAZON_AWS);

    return cloudosVpcRequest;
  }



  @After
  @SuppressWarnings("Duplicates")
  public void tearDown() throws Exception {
    Set<Instance> instances = this.amazonService.listInstances(region, securityGroup);
    List<String> list =
        instances.stream().map(instance -> instance.getInstanceId()).collect(Collectors.toList());
    this.amazonService.terminateInstances(list, region, securityGroup);
  }
}
