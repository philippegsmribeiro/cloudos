package cloudos;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cloudos.models.services.CloudosService;
import cloudos.models.services.ServiceCategory;
import cloudos.models.services.ServiceRequest;
import cloudos.models.services.ServiceResponse;
import cloudos.services.CloudosServiceService;
import lombok.extern.log4j.Log4j2;
import test.CloudOSTest;

/**
 * Unit tests for {@link CloudosServiceController}.
 * 
 * @author Alex Calagua
 */
@Log4j2
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CloudosServiceControllerTest extends CloudOSTest {

  private MockMvc mvc;

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private CloudosServiceController servicesController;

  @Autowired
  private CloudosServiceService cloudosServiceService;

  @Autowired
  void setConverters(HttpMessageConverter<?>[] converters) {
    this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
        .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().orElse(null);
    assertNotNull("the JSON message converter must not be null",
        this.mappingJackson2HttpMessageConverter);
  }

  @Before
  public void setUp() throws Exception {
    this.mvc = MockMvcBuilders.standaloneSetup(servicesController).build();
    ServiceRequest serviceRequest = ServiceRequest.builder().code("TestController").build();
    List<CloudosService> cloudosServices =
        cloudosServiceService.getServicesByFilter(serviceRequest);
    for (CloudosService cloudosService : cloudosServices) {
      cloudosServiceService.deleteService(cloudosService.getId());
    }
  }

  @Test
  public void testACreateService() throws Exception {

    ServiceRequest service = ServiceRequest.builder().code("TestControllerAmazonEC2").name("EC2")
        .providers(Providers.AMAZON_AWS).serviceCategory(ServiceCategory.COMPUTING).build();
    MvcResult mvcResult = this.mvc
        .perform(
            post(CloudosServiceController.SERVICES + "/").contentType(MediaType.APPLICATION_JSON)
                .content(json(service)).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is2xxSuccessful()).andReturn();
    assertNotNull(mvcResult);

  }

  @Test
  public void testBGetServices() throws Exception {
    ServiceRequest service = ServiceRequest.builder().code("TestControllerAmazonEC21").name("EC2")
        .providers(Providers.AMAZON_AWS).serviceCategory(ServiceCategory.COMPUTING).build();
    cloudosServiceService.createService(service);

    MvcResult mvcResult = this.mvc
        .perform(get(CloudosServiceController.SERVICES + "/")
            .param("providers", Providers.AMAZON_AWS.toString())
            .param("serviceCategory", ServiceCategory.COMPUTING.toString()))
        .andExpect(status().isOk()).andReturn();

    List<CloudosService> responseData = objectMapper.readValue(
        mvcResult.getResponse().getContentAsString(), new TypeReference<List<CloudosService>>() {});

    // response JSON payload
    log.info(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseData));

  }

  @Test
  public void testCDeleteServices() throws Exception {
    ServiceRequest service = ServiceRequest.builder().code("TestControllerAmazonEC22").name("EC2")
        .providers(Providers.AMAZON_AWS).serviceCategory(ServiceCategory.COMPUTING).build();
    ServiceResponse response = cloudosServiceService.createService(service);

    MvcResult mvcResult = this.mvc.perform(
        delete(CloudosServiceController.SERVICES + "/").param("id", response.getService().getId()))
        .andExpect(status().isOk()).andReturn();
    assertNotNull(mvcResult);
  }

  @Test
  public void testDUpdateService() throws Exception {

    ServiceRequest serviceInsert =
        ServiceRequest.builder().code("TestControllerAmazonEC23").name("EC2")
            .providers(Providers.AMAZON_AWS).serviceCategory(ServiceCategory.COMPUTING).build();
    ServiceResponse serviceResponseInsert = cloudosServiceService.createService(serviceInsert);
    serviceResponseInsert.getService().setName("EC2 Changed");
    ServiceRequest serviceUpdate =
        ServiceRequest.builder().id(serviceResponseInsert.getService().getId())
            .code(serviceResponseInsert.getService().getCode())
            .name(serviceResponseInsert.getService().getName())
            .providers(serviceResponseInsert.getService().getProviders())
            .serviceCategory(serviceResponseInsert.getService().getServiceCategory())
            .serviceStatus(serviceResponseInsert.getService().getServiceStatus()).build();

    MvcResult mvcResult = this.mvc
        .perform(
            put(CloudosServiceController.SERVICES + "/").contentType(MediaType.APPLICATION_JSON)
                .content(json(serviceUpdate)).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();
    assertNotNull(mvcResult);

  }

}
