package cloudos.costanalysis;

import cloudos.Providers;
import cloudos.models.CallResult;
import cloudos.models.CallStatus;
import cloudos.models.costanalysis.GenericBillingReport;
import cloudos.models.costanalysis.ProviderTrendEntry;
import cloudos.utils.DateUtil;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import test.CloudOSTest;

/**
 * Unit tests for provider trend job.
 */
public class ProviderTrendJobTest extends CloudOSTest {

  private ProviderTrendJob job;

  @Mock
  private GenericBillingReportRepository genericBillingReportRepository;

  @Mock
  private ProviderTrendEntryRepository providerTrendRepository;

  @Before
  public void setUp() {
    this.job = new ProviderTrendJob(genericBillingReportRepository, providerTrendRepository);
  }

  @Test
  public void execute() {

    LocalDate now = LocalDate.now();

    List<GenericBillingReport> testData = createExecuteTestDate(now);

    Mockito.when(
        genericBillingReportRepository.findByUsageStartTimeBetween(Mockito.any(), Mockito.any()))
        .thenReturn(testData);

    CallResult<ProviderTrendEntry> result = job.execute(now);
    Assert.assertSame(CallStatus.SUCCESS, result.getStatus());
    Assert.assertThat(result.getResult().getCosts(), Matchers.not(Matchers.empty()));
  }

  private List<GenericBillingReport> createExecuteTestDate(LocalDate date) {
    return Arrays.asList(
        new GenericBillingReport("junit-id", "junit-lineItemId", Providers.AMAZON_AWS,
            "junit-account", "junit-product-code", "junit-resource-id",
            DateUtil.toDate(LocalDate.now().minusDays(1)), DateUtil.toDate(date), BigDecimal.ONE,
            "junit-location", false),
        new GenericBillingReport("junit-id-2", "junit-lineItemId-2",
            Providers.GOOGLE_COMPUTE_ENGINE, "junit-account-2", "junit-product-code",
            "junit-resource-id-2", DateUtil.toDate(LocalDate.now().minusDays(1)),
            DateUtil.toDate(date), BigDecimal.valueOf(-5.0), "junit-location-2", false));
  }

}
