package cloudos.costanalysis;

import cloudos.models.CallResult;
import cloudos.models.CallStatus;
import cloudos.models.costanalysis.GoogleBillingReconciliationEntry;
import cloudos.models.costanalysis.GoogleBillingReconciliationEntryId;
import java.math.BigDecimal;
import java.time.LocalDate;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.CloudOSTest;

public class GoogleBillingsReconciliationServiceTest extends CloudOSTest {

  private static final String MEASUREMENT_ID = "com.google.cloud/services/compute-engine/NetworkInterZoneEgress";
  private static final String RESOURCE_URI = "https://www.googleapis.com/compute/v1/projects/oval-campaign-146701-1/zones/asia-east1-a/instances/otluesjg";
  private static final String RESOURCE_ID_1 = "otluesjg";
  private static final String RESOURCE_ID_2 = "b880-w0ncv2ro";
  private static final String RESOURCE_1_COST = "3.333333";
  private static final String RESOURCE_2_COST = "6.666667";

  @Autowired
  private CostAnalysisTestUtils costAnalysisTestUtils;

  @Autowired
  private GoogleBillingsReconciliationService service;

  @Autowired
  private GoogleBillingsReconciliationEntryRepository entryRepository;

  @Test
  public void reconcile() throws Exception {

    // prepare data if it does not exist
    costAnalysisTestUtils.loadGoogleUsageReports();
    costAnalysisTestUtils.loadGoogleBillingReports();

    // actual test
    LocalDate reportDate = CostAnalysisTestUtils.GOOGLE_USAGE_REPORT_CSV_DATE;

    CallResult<Void> reconcile = service.reconcile(reportDate);
    Assert.assertSame(CallStatus.SUCCESS, reconcile.getStatus());

    GoogleBillingReconciliationEntryId entry1 = new GoogleBillingReconciliationEntryId(
        reportDate, MEASUREMENT_ID, RESOURCE_ID_1);
    GoogleBillingReconciliationEntryId entry2 = new GoogleBillingReconciliationEntryId(
        reportDate, MEASUREMENT_ID, RESOURCE_ID_2);

    // assertions
    GoogleBillingReconciliationEntry one = entryRepository.findOne(entry1);
    Assert.assertTrue(
        new BigDecimal(RESOURCE_1_COST, GoogleBillingsReconciliationService.MATH_CONTEXT)
            .compareTo(one.getCostAmount()) == 0);

    GoogleBillingReconciliationEntry two = entryRepository.findOne(entry2);
    Assert.assertTrue(
        new BigDecimal(RESOURCE_2_COST, GoogleBillingsReconciliationService.MATH_CONTEXT)
            .compareTo(two.getCostAmount()) == 0);
  }

  @Test
  public void reconcileAll() throws Exception {
    // actual test
    CallResult<Void> result = service.reconcile();

    // assertions
    Assert.assertSame(CallStatus.SUCCESS, result.getStatus());
  }

  @Test
  public void getResourceIdFromResourceUri() {
    String resourceId = service.getResourceIdFromResourceUri(RESOURCE_URI);
    Assert.assertEquals(RESOURCE_ID_1, resourceId);
  }

  @Test
  public void calculateReconciliationEntryCost() {

    BigDecimal totalCost = BigDecimal.TEN;
    BigDecimal totalUsageAmount = BigDecimal.valueOf(100L);
    BigDecimal thisUsageAmount = BigDecimal.TEN;

    BigDecimal result = service
        .calculateReconciliationEntryCost(totalCost, totalUsageAmount, thisUsageAmount);

    Assert.assertTrue(BigDecimal.ONE.compareTo(result) == 0);
  }

  @Test
  public void calculateReconciliationEntryCostWithZeroValuesExpectsZero() {
    Assert.assertTrue(BigDecimal.ZERO.compareTo(
        service.calculateReconciliationEntryCost(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO))
        == 0);
  }

}