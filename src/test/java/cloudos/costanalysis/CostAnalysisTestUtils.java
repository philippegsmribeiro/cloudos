package cloudos.costanalysis;

import static org.junit.Assert.assertTrue;
import cloudos.Providers;
import cloudos.billings.GoogleBillingReportMapper;
import cloudos.billings.GoogleBillingReportRepository;
import cloudos.models.CallResult;
import cloudos.models.CallStatus;
import cloudos.models.Instance;
import cloudos.models.costanalysis.BillingAccount;
import cloudos.models.costanalysis.BillingCost;
import cloudos.models.costanalysis.DailyResourceCost;
import cloudos.models.costanalysis.DailyResourceCostId;
import cloudos.models.costanalysis.GenericBillingReport;
import cloudos.models.costanalysis.GoogleBillingReconciliationEntry;
import cloudos.models.costanalysis.GoogleBillingReconciliationEntryId;
import cloudos.models.costanalysis.ProviderServiceCostComparisonEntry;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.core.io.Resource;

/**
 * Cost analysis test utils class. Used for constants and mocks.
 */
@TestComponent
public class CostAnalysisTestUtils {

  public static final String COSTANALYSIS_GENERIC_BILLING_REPORTS_DATA_JSON = "classpath:cloudos/costanalysis/generic-billing-reports-data.json";
  public static final String BILLING_REPORTS_COST_SUM = "20.6525980";
  public static final String GOOGLE_USAGE_REPORT_CSV = "classpath:cloudos/costanalysis/usage_gce_1019432884833_20171106.csv";
  public static final LocalDate GOOGLE_USAGE_REPORT_CSV_DATE = LocalDate.of(2017, 11, 06);
  public static final String GOOGLE_BILLING_REPORT_2017_11_06 = "classpath:cloudos/costanalysis/google-billing-2017-11-06.json";
  public static final String GOOGLE_BILLING_REPORT_2017_11_07 = "classpath:cloudos/costanalysis/google-billing-2017-11-07.json";
  public static final Long NUMERIC_PROJECT_ID = Long.valueOf(1019432884833L);

  @Autowired
  private BillingAccountRepository billingAccountRepository;

  @Autowired
  private GoogleUsageReportMapper googleUsageReportMapper;

  @Autowired
  private GoogleBillingReportMapper googleBillingReportMapper;

  @Autowired
  private GoogleBillingReportRepository googleBillingReportRepository;

  @Autowired
  private GoogleBillingsReconciliationEntryRepository googleBillingsReconciliationEntryRepository;

  @Value(GOOGLE_USAGE_REPORT_CSV)
  private Resource googleUsageReportResource;

  @Value(GOOGLE_BILLING_REPORT_2017_11_06)
  private Resource googleBillingReport1;

  @Value(GOOGLE_BILLING_REPORT_2017_11_07)
  private Resource googleBillingReport2;

  private List<BillingAccount> mockedBillingAccounts = new ArrayList<>();

  /**
   * Validates the {@link ProviderServiceCostComparisonEntry} according to the mocked data.
   */
  public void validateProviderServiceCostComparisonEntry(
      final ProviderServiceCostComparisonEntry result) {
    BigDecimal expectedValue = new BigDecimal(CostAnalysisTestUtils.BILLING_REPORTS_COST_SUM);
    assertTrue(expectedValue.compareTo(result.getMonthToDateCost().getCost()) == 0);
    assertTrue(expectedValue.compareTo(result.getTotal30DaysCost().getCost()) == 0);
    assertTrue(expectedValue.compareTo(result.getTotal60DaysCost().getCost()) == 0);
  }

  /**
   * Creates a random billing account.
   *
   * @param provider the billing account provider.
   */
  public BillingAccount createBillingAccount(Providers provider) {
    if (provider == null) {
      return null;
    }

    String accountId = RandomStringUtils.randomAlphanumeric(20);
    BillingAccount billingAccount = new BillingAccount(accountId, provider);
    billingAccount = save(billingAccount);
    return billingAccount;
  }

  /**
   * Saves a {@link BillingAccount} into the database and appends the instance to mocked billing
   * accounts list (to be deleted later by the unit test).
   *
   * @return the saved {@link BillingAccount} instance.
   */
  private BillingAccount save(BillingAccount billingAccount) {
    if (billingAccount == null) {
      return null;
    }
    billingAccount = billingAccountRepository.save(billingAccount);
    mockedBillingAccounts.add(billingAccount);
    return billingAccount;
  }

  private void deleteMockedBillingAccounts() {
    mockedBillingAccounts.forEach(item -> billingAccountRepository.delete(item));
  }

  /**
   * Deletes all mocked data.
   */
  public void deleteMockedData() {
    this.deleteMockedBillingAccounts();
  }

  /**
   * Loads google usage reports.
   */
  public void loadGoogleUsageReports() throws Exception {
    CallResult<Void> googleUsageReportResult = googleUsageReportMapper
        .processReport(googleUsageReportResource.getFile());
    Assert.assertSame(CallStatus.SUCCESS, googleUsageReportResult.getStatus());
  }

  /**
   * Loads google billings reports.
   */
  public void loadGoogleBillingReports() throws Exception {

    CallResult<Void> result = googleBillingReportMapper
        .getBillingReport(googleBillingReport1.getFile(), googleBillingReportRepository);
    Assert.assertSame(CallStatus.SUCCESS, result.getStatus());

    result = googleBillingReportMapper
        .getBillingReport(googleBillingReport2.getFile(), googleBillingReportRepository);
    Assert.assertSame(CallStatus.SUCCESS, result.getStatus());
  }

  /**
   * Loads a {@link GoogleBillingReconciliationEntry}.
   */
  public GoogleBillingReconciliationEntry loadGoogleBillingReconciliationEntry() {

    LocalDate reportDate = LocalDate.of(2017, 11, 6);
    String measurementId = "com.google.cloud/services/compute-engine/NetworkInterZoneEgress";
    String accountId = "junit-account";
    Date startTime = java.sql.Date.valueOf(reportDate);
    Date endTime = java.sql.Date.valueOf(reportDate.plusDays(1L));
    String resourceId = "junit-resource";

    GoogleBillingReconciliationEntryId id = new GoogleBillingReconciliationEntryId(reportDate,
        measurementId, resourceId);

    GoogleBillingReconciliationEntry entry = new GoogleBillingReconciliationEntry(id,
        reportDate, accountId, startTime, endTime, measurementId, resourceId, BigDecimal.ONE, "USD",
        "us-east1", Long.valueOf(0L), null, null, LocalDateTime.now());

    entry = googleBillingsReconciliationEntryRepository.save(entry);

    return entry;
  }
  
  /**
   * Mock data for createRetrieveProviderRegionCostData method.
   */
  public List<GenericBillingReport> createRetrieveProviderRegionCostData(Date startDateObj) {

    GenericBillingReport genericBillingReport1 = new GenericBillingReport("junit-id",
        "junit-lineItemId", Providers.AMAZON_AWS, "1", "junit-produceCode", "resource1",
        startDateObj, DateUtils.addDays(startDateObj, 1), BigDecimal.ONE, "junit-location", false);

    GenericBillingReport genericBillingReport2 =
        new GenericBillingReport("junit-id2", "junit-lineItemId2", Providers.AMAZON_AWS, "2",
            "junit-produceCode2", "resource2", genericBillingReport1.getUsageEndTime(),
            DateUtils.addDays(genericBillingReport1.getUsageEndTime(), 1), BigDecimal.TEN,
            "junit-location", false);

    return Arrays.asList(genericBillingReport1, genericBillingReport2);
  }
  
  /**
   * Mock a {@link Instance} list from a {@link GenericBillingReport} list.
   */
  public List<Instance> mockInstances(List<GenericBillingReport> reports) {
    List<Instance> instances = new ArrayList<>();
    for (GenericBillingReport report : reports) {
      Instance i = new Instance();
      i.setProvider(report.getProvider());
      i.setSpot(Boolean.FALSE);
      i.setReserved(Boolean.FALSE);
      i.setProviderId(report.getResourceId());
      instances.add(i);
    }
    return instances;
  }

  /**
   * Creates a mock instance of {@link GenericBillingReport}.
   */
  public GenericBillingReport createGenericBillingReportMock(String id, String lineItemId,
      Providers provider, String accountId, String productCode, String resourceId,
      Date usageStartTime, Date usageEndTime, BigDecimal usageCost, String location) {
    id = ObjectUtils.defaultIfNull(id, RandomStringUtils.randomAlphabetic(24));
    lineItemId = ObjectUtils.defaultIfNull(lineItemId, RandomStringUtils.randomAlphabetic(24));
    provider = ObjectUtils.defaultIfNull(provider,
        Providers.values()[RandomUtils.nextInt(0, Providers.values().length)]);
    accountId = ObjectUtils.defaultIfNull(accountId, RandomStringUtils.randomAlphabetic(24));
    productCode = ObjectUtils.defaultIfNull(productCode, RandomStringUtils.randomAlphabetic(24));
    resourceId = ObjectUtils.defaultIfNull(resourceId, RandomStringUtils.randomAlphabetic(24));
    Date now = new Date();
    usageStartTime = ObjectUtils.defaultIfNull(usageStartTime, now);
    usageEndTime = DateUtils.addDays(now, 1);
    usageCost =
        ObjectUtils.defaultIfNull(usageCost, BigDecimal.valueOf(RandomUtils.nextDouble(0d, 1d)));
    location = ObjectUtils.defaultIfNull(location, RandomStringUtils.randomAlphabetic(24));
    return new GenericBillingReport(id, lineItemId, provider, accountId, productCode, resourceId,
        usageStartTime, usageEndTime, usageCost, location, false);
  }

  /**
   * Retrieves 11 mocked {@link DailyResourceCost} instances.
   */
  public List<DailyResourceCost> mock11DailyResourceCost() {
    List<DailyResourceCost> result = new ArrayList<>();
    result.add(new DailyResourceCost(
        new DailyResourceCostId(LocalDate.now(), Providers.GOOGLE_COMPUTE_ENGINE, "11"),
        new BillingCost(BigDecimal.valueOf(0d)), new Date()));
    result.add(new DailyResourceCost(
        new DailyResourceCostId(LocalDate.now(), Providers.GOOGLE_COMPUTE_ENGINE, "2"),
        new BillingCost(BigDecimal.valueOf(9d)), new Date()));
    result.add(new DailyResourceCost(
        new DailyResourceCostId(LocalDate.now(), Providers.GOOGLE_COMPUTE_ENGINE, "1"),
        new BillingCost(BigDecimal.valueOf(10d)), new Date()));
    result.add(new DailyResourceCost(
        new DailyResourceCostId(LocalDate.now(), Providers.GOOGLE_COMPUTE_ENGINE, "3"),
        new BillingCost(BigDecimal.valueOf(8d)), new Date()));
    result.add(new DailyResourceCost(
        new DailyResourceCostId(LocalDate.now(), Providers.GOOGLE_COMPUTE_ENGINE, "4"),
        new BillingCost(BigDecimal.valueOf(7d)), new Date()));
    result.add(new DailyResourceCost(
        new DailyResourceCostId(LocalDate.now(), Providers.GOOGLE_COMPUTE_ENGINE, "6"),
        new BillingCost(BigDecimal.valueOf(5d)), new Date()));
    result.add(new DailyResourceCost(
        new DailyResourceCostId(LocalDate.now(), Providers.GOOGLE_COMPUTE_ENGINE, "7"),
        new BillingCost(BigDecimal.valueOf(4d)), new Date()));
    result.add(new DailyResourceCost(
        new DailyResourceCostId(LocalDate.now(), Providers.GOOGLE_COMPUTE_ENGINE, "0"),
        new BillingCost(BigDecimal.valueOf(1d)), new Date()));
    result.add(new DailyResourceCost(
        new DailyResourceCostId(LocalDate.now(), Providers.GOOGLE_COMPUTE_ENGINE, "9"),
        new BillingCost(BigDecimal.valueOf(2d)), new Date()));
    result.add(new DailyResourceCost(
        new DailyResourceCostId(LocalDate.now(), Providers.GOOGLE_COMPUTE_ENGINE, "8"),
        new BillingCost(BigDecimal.valueOf(3d)), new Date()));
    result.add(new DailyResourceCost(
        new DailyResourceCostId(LocalDate.now(), Providers.GOOGLE_COMPUTE_ENGINE, "5"),
        new BillingCost(BigDecimal.valueOf(6d)), new Date()));
    return result;
  }
}
