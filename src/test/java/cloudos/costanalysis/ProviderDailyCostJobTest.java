package cloudos.costanalysis;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import cloudos.Providers;
import cloudos.models.CallResult;
import cloudos.models.CallStatus;
import cloudos.models.costanalysis.GenericBillingReport;
import cloudos.models.costanalysis.ProviderDailyCostEntry;
import cloudos.utils.DateUtil;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import test.CloudOSTest;

/**
 * Unit tests for ProviderCostJob.
 */
public class ProviderDailyCostJobTest extends CloudOSTest {

  @Autowired
  private CostAnalysisTestUtils utils;

  @MockBean(name = "genericBillingReportRepository")
  private GenericBillingReportRepository genericBillingReportRepository;

  @MockBean(name = "providerDailyCostEntryRepository")
  private ProviderDailyCostEntryRepository providerDailyCostEntryRepository;

  private ProviderDailyCostJob job;

  @Before
  public void setUp() {
    this.job =
        new ProviderDailyCostJob(genericBillingReportRepository, providerDailyCostEntryRepository);
  }

  @Test
  public void updateCostSingleDay() {

    // create mock data

    // execute the test
    LocalDate now = LocalDate.now();
    LocalDate yesterday = now.minusDays(1);

    Date start = DateUtil.toDate(yesterday);
    Date end = DateUtil.toDate(now);

    when(genericBillingReportRepository.findByUsageStartTimeBetween(start, end))
        .thenReturn(getUpdateCostMockData(start, end));

    CallResult<List<ProviderDailyCostEntry>> callResult = job.updateCost(yesterday, now);
    assertNotNull(callResult);
    assertSame(CallStatus.SUCCESS, callResult.getStatus());
    assertThat(callResult.getResult(),
        Matchers.allOf(Matchers.notNullValue(), Matchers.not(Matchers.empty())));
    assertSame(2, callResult.getResult().size());
  }
  
  /**
   * Generates {@link GenericBillingReport} mock data.
   */
  private List<GenericBillingReport> getUpdateCostMockData(Date start, Date end) {
    GenericBillingReport r1 = utils.createGenericBillingReportMock(null, null, Providers.AMAZON_AWS,
        null, null, null, start, end, BigDecimal.TEN, null);
    GenericBillingReport r2 = utils.createGenericBillingReportMock(null, null, Providers.AMAZON_AWS,
        null, null, null, start, end, BigDecimal.ONE, null);
    GenericBillingReport r3 = utils.createGenericBillingReportMock(null, null,
        Providers.GOOGLE_COMPUTE_ENGINE, null, null, null, start, end, BigDecimal.ONE, null);
    GenericBillingReport r4 = utils.createGenericBillingReportMock(null, null,
        Providers.GOOGLE_COMPUTE_ENGINE, null, null, null, start, end, BigDecimal.ONE, null);
    return Arrays.asList(r1, r2, r3, r4);
  }

}
