package cloudos.costanalysis;

import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import cloudos.Providers;
import cloudos.models.CallResult;
import cloudos.models.CallStatus;
import cloudos.models.costanalysis.BillingCost;
import cloudos.models.costanalysis.DailyResourceCost;
import cloudos.models.costanalysis.DailyResourceCostId;
import cloudos.models.costanalysis.GenericBillingReport;
import cloudos.utils.DateUtil;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.time.DateUtils;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import test.CloudOSTest;


public class DailyResourceCostJobTest extends CloudOSTest {

  private static final BillingCost BILLING_COST_2 = new BillingCost(BigDecimal.valueOf(1));
  private static final BillingCost BILLING_COST_1 = new BillingCost(BigDecimal.valueOf(2));
  private static final Providers PROVIDER_2 = Providers.GOOGLE_COMPUTE_ENGINE;
  private static final Providers PROVIDER_1 = Providers.AMAZON_AWS;
  private static final String RESOURCE_ID = "resource1";

  private DailyResourceCostJob job;

  @MockBean
  private GenericBillingReportRepository genericBillingReportRepository;

  @MockBean
  private DailyResourceCostService dailyResourceCostService;

  @Autowired
  private CostAnalysisTestUtils utils;

  @Before
  public void setUp() {
    this.job = new DailyResourceCostJob(genericBillingReportRepository, dailyResourceCostService);
  }

  @Test
  public void execute() {

    LocalDate end = LocalDate.now();
    LocalDate start = end.minusDays(1);

    Date startDate = DateUtil.toDate(start);
    Date endDate = DateUtil.toDate(end);

    List<DailyResourceCost> expected = getExecuteExpectedResult(start);
    List<GenericBillingReport> mockedReports = mockExecuteGenericBillingReports(start, end);
    when(genericBillingReportRepository.findByUsageStartTimeBetween(startDate, endDate))
        .thenReturn(mockedReports);

    CallResult<List<DailyResourceCost>> result = job.execute(start, end);
    assertSame(CallStatus.SUCCESS, result.getStatus());
    assertThat(result.getResult(),
        Matchers.allOf(Matchers.notNullValue(), Matchers.not(Matchers.empty())));
    assertTrue(result.getResult().containsAll(expected));
  }

  private List<DailyResourceCost> getExecuteExpectedResult(LocalDate start) {

    DailyResourceCostId id1 = new DailyResourceCostId(start, PROVIDER_1, RESOURCE_ID);
    DailyResourceCost d1 = new DailyResourceCost(id1, BILLING_COST_1, new Date());

    DailyResourceCostId id2 = new DailyResourceCostId(start, PROVIDER_2, RESOURCE_ID);
    DailyResourceCost d2 = new DailyResourceCost(id2, BILLING_COST_2, new Date());

    return Arrays.asList(d1, d2);
  }

  private List<GenericBillingReport> mockExecuteGenericBillingReports(LocalDate start,
      LocalDate end) {

    Date r1Date = DateUtil.toDate(start);
    // r1 and r2 should be grouped in the same entry and get its cost summed
    GenericBillingReport r1 = utils.createGenericBillingReportMock(null, null, PROVIDER_1,
        null, null, RESOURCE_ID, r1Date, DateUtils.addDays(r1Date, 1), BigDecimal.ONE, null);
    GenericBillingReport r2 = utils.createGenericBillingReportMock(null, null, PROVIDER_1,
        null, null, RESOURCE_ID, r1Date, DateUtils.addDays(r1Date, 1), BigDecimal.ONE, null);
    // r3 has a different provider will result in a different entry
    GenericBillingReport r3 =
        utils.createGenericBillingReportMock(null, null, PROVIDER_2, null,
            null, RESOURCE_ID, r1Date, DateUtils.addDays(r1Date, 1), BigDecimal.ONE, null);

    return Arrays.asList(r1, r2, r3);
  }

}
