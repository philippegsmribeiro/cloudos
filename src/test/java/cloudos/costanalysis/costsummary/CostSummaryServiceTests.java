package cloudos.costanalysis.costsummary;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import cloudos.Providers;
import cloudos.billings.AwsBillingReport;
import cloudos.costanalysis.GenericBillingReportRepository;
import cloudos.models.costanalysis.GoogleBillingReconciliationEntry;
import cloudos.models.costanalysis.GoogleBillingReconciliationEntryId;
import cloudos.utils.DateUtil;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.CloudOSTest;

public class CostSummaryServiceTests extends CloudOSTest {

  @Autowired
  private CostSummaryService costSummaryService;
  @Autowired
  private CostSummaryRepository costSummaryRepository;
  @Autowired
  private GenericBillingReportRepository reportRepository;

  private static boolean setup = false;

  @Before
  public void cleanup() {
    if (!setup) {

      costSummaryRepository.deleteAll();
      reportRepository.deleteAll();

      // get all year data
      LocalDateTime today = LocalDateTime.now();
      LocalDateTime threeMonthsAgo = today.minusMonths(3);
      Date startDate = DateUtil.toDate(threeMonthsAgo);
      Date endDate = DateUtil.toDate(today);
      if (reportRepository.findByUsageStartTimeBetween(startDate, endDate).size() == 0) {
        // if no data, create the data for testing
        String accountId = generator.generate(8);
        String[] productCodes = new String[] {"EC2", "S3"};

        List<AwsBillingReport> reportsToSave = new ArrayList<>();
        while (threeMonthsAgo.isBefore(today)) {

          LocalDateTime usageStartDateTime = threeMonthsAgo.withHour(0).withMinute(0).withSecond(0);
          Date usageStartDate = DateUtil.toDate(usageStartDateTime);

          LocalDateTime usageEndDateTime = threeMonthsAgo.withHour(1).withMinute(0).withSecond(0);
          Date usageEndDate = DateUtil.toDate(usageEndDateTime);

          BigDecimal usageCost = BigDecimal.TEN;
          String lineItemId = generator.generate(8);

          int i = threeMonthsAgo.getDayOfWeek().ordinal() % 2;

          AwsBillingReport awsBillingReport = new AwsBillingReport();
          awsBillingReport.setPayerAccountId(accountId);
          awsBillingReport.setBlendedCost(usageCost.doubleValue());
          awsBillingReport.setUsageStartDate(usageStartDate);
          awsBillingReport.setUsageEndDate(usageEndDate);
          awsBillingReport.setLineItemId(lineItemId);
          awsBillingReport.setResourceId("resourceId");
          awsBillingReport.setProductCode(productCodes[i]);
          awsBillingReport.setLocation("location");

          reportsToSave.add(awsBillingReport);

          threeMonthsAgo = threeMonthsAgo.plusDays(1);
        }
        reportRepository.saveAll(reportsToSave);

        threeMonthsAgo = today.minusMonths(3);
        List<GoogleBillingReconciliationEntry> reportsToSave2 = new ArrayList<>();
        while (threeMonthsAgo.isBefore(today)) {

          LocalDateTime usageStartDateTime = threeMonthsAgo.withHour(0).withMinute(0).withSecond(0);
          Date usageStartDate = DateUtil.toDate(usageStartDateTime);

          LocalDateTime usageEndDateTime = threeMonthsAgo.withHour(1).withMinute(0).withSecond(0);
          Date usageEndDate = DateUtil.toDate(usageEndDateTime);

          BigDecimal usageCost = BigDecimal.ONE;
          String lineItemId = generator.generate(8);

          int i = threeMonthsAgo.getDayOfWeek().ordinal() % 2;

          GoogleBillingReconciliationEntry awsBillingReport =
              new GoogleBillingReconciliationEntry();
          awsBillingReport.setId(new GoogleBillingReconciliationEntryId(usageStartDateTime.toLocalDate(), lineItemId, "resourceId"));
          awsBillingReport.setAccountId(accountId);
          awsBillingReport.setCostAmount(usageCost);
          awsBillingReport.setStartTime(usageStartDate);
          awsBillingReport.setEndTime(usageEndDate);
          awsBillingReport.setMeasurementId(lineItemId);
          awsBillingReport.setResourceId("resourceId");
          awsBillingReport.setMeasurementId(lineItemId);
          awsBillingReport.setLocation("location");

          reportsToSave2.add(awsBillingReport);

          threeMonthsAgo = threeMonthsAgo.plusDays(1);
        }
        reportRepository.save(reportsToSave2);
      }
      setup = true;
    }
  }

  @Test
  public void updateTest() {
    // run the update
    CostSummary costSummary = costSummaryService.updateCostSummary(null);
    assertNotNull(costSummary);
    // finf the updated
    CostSummary retrieveUpdatedCostSummary = costSummaryService.retrieveUpdatedCostSummary(null);
    assertNotNull(retrieveUpdatedCostSummary);
    // assert they are the same
    assertEquals(costSummary.getId(), retrieveUpdatedCostSummary.getId());
    // validate fields
  }

  @Test
  public void updateAWSTest() {
    // run the update
    CostSummary costSummary = costSummaryService.updateCostSummary(Providers.AMAZON_AWS);
    assertNotNull(costSummary);
    // finf the updated
    CostSummary retrieveUpdatedCostSummary =
        costSummaryService.retrieveUpdatedCostSummary(Providers.AMAZON_AWS);
    assertNotNull(retrieveUpdatedCostSummary);
    // assert they are the same
    assertEquals(costSummary.getId(), retrieveUpdatedCostSummary.getId());
    // validate fields
  }

  @Test
  public void updateGoogleTest() {
    // run the update
    CostSummary costSummary = costSummaryService.updateCostSummary(Providers.GOOGLE_COMPUTE_ENGINE);
    assertNotNull(costSummary);
    // finf the updated
    CostSummary retrieveUpdatedCostSummary =
        costSummaryService.retrieveUpdatedCostSummary(Providers.GOOGLE_COMPUTE_ENGINE);
    assertNotNull(retrieveUpdatedCostSummary);
    // assert they are the same
    assertEquals(costSummary.getId(), retrieveUpdatedCostSummary.getId());
    // validate fields
  }
}
