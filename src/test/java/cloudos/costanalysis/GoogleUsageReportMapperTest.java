package cloudos.costanalysis;

import static org.junit.Assert.assertNotNull;

import cloudos.models.CallResult;
import cloudos.models.CallStatus;
import java.io.File;
import org.apache.commons.csv.CSVRecord;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import test.CloudOSTest;

/**
 * Unit tests for GoogleUsageReportMapper.
 */
public class GoogleUsageReportMapperTest extends CloudOSTest {

  @Autowired
  private GoogleUsageReportMapper mapper;

  @Value(CostAnalysisTestUtils.GOOGLE_USAGE_REPORT_CSV)
  private Resource input;

  @Autowired
  private GoogleUsageReportEntryRepository repository;

  @Test
  public void contextLoads() {
    assertNotNull(input);
    assertNotNull(mapper);
    assertNotNull(repository);
  }

  @Test
  public void getCsvRecords() throws Exception {
    Iterable<CSVRecord> csvRecordsIterable = mapper.getCsvRecords(input.getFile());
    Assert.assertNotNull(csvRecordsIterable);
    Assert.assertTrue(csvRecordsIterable.iterator().hasNext());
  }

  @Test
  public void processReport() throws Exception {
    File file = input.getFile();
    CallResult<Void> callResult = mapper.processReport(file);
    Assert.assertSame(CallStatus.SUCCESS, callResult.getStatus());
  }

  @Test
  public void getNumericProjectIdFromFileName() throws Exception {
    Long numericProjectId = mapper.getNumericProjectIdFromFileName(input.getFile().getName());
    Assert.assertEquals(CostAnalysisTestUtils.NUMERIC_PROJECT_ID, numericProjectId);
  }

}
