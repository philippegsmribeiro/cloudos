package cloudos.costanalysis;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import cloudos.Providers;
import cloudos.models.costanalysis.BillingCost;
import cloudos.models.costanalysis.DailyResourceCost;
import cloudos.models.costanalysis.DailyResourceCostId;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.CloudOSTest;

public class DailyResourceCostServiceTest extends CloudOSTest {

  @Autowired
  private DailyResourceCostService service;

  @Test
  public void crudTest() {

    // create
    LocalDate now = LocalDate.now();
    DailyResourceCostId id = new DailyResourceCostId(now, Providers.AMAZON_AWS, "resourceId");
    DailyResourceCost item = new DailyResourceCost(id, new BillingCost(BigDecimal.TEN), new Date());
    item = service.save(item);
    assertNotNull(id);

    // retrieve
    DailyResourceCost item2 = service.findById(id);
    assertEquals(item, item2);

    // update
    item.setCost(new BillingCost(BigDecimal.ONE));
    item = service.save(item);
    DailyResourceCost item3 = service.findById(id);
    assertTrue(BigDecimal.ONE.compareTo(item3.getCost().getTotalCost()) == 0);

    // delete
    service.delete(item);
    DailyResourceCost result = service.findById(id);
    assertNull(result);
  }

  @Test
  public void findByDateRange() {

    LocalDate end = LocalDate.now();
    LocalDate start = end.minusWeeks(1);

    DailyResourceCost lowerLimit = service.save(newDailyResourceCost(start));
    DailyResourceCost upperLimit = service.save(newDailyResourceCost(end));
    DailyResourceCost another = service.save(newDailyResourceCost(start.minusDays(1)));
    DailyResourceCost another2 = service.save(newDailyResourceCost(end.plusDays(1)));

    List<DailyResourceCost> found = service.findByDateRange(start, end);

    assertTrue(found.contains(lowerLimit));
    assertTrue(found.contains(upperLimit));
    assertFalse(found.contains(another));
    assertFalse(found.contains(another2));
  }

  private DailyResourceCost newDailyResourceCost(LocalDate date) {
    DailyResourceCost item = new DailyResourceCost();
    String resourceId = RandomStringUtils.randomAlphabetic(10);
    item.setId(new DailyResourceCostId(date,
        Providers.values()[RandomUtils.nextInt(0, Providers.values().length)], resourceId));
    item.setCost(new BillingCost(BigDecimal.valueOf(RandomUtils.nextDouble(0, 100))));
    return item;
  }

}
