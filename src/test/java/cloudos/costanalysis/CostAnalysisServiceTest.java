package cloudos.costanalysis;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.core.BooleanBuilder;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.hamcrest.Matchers;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import cloudos.Providers;
import cloudos.billings.AwsBillingReport;
import cloudos.billings.AwsBillingReportRepository;
import cloudos.billings.BillingReport;
import cloudos.billings.GoogleBillingReport;
import cloudos.billings.GoogleBillingReportRepository;
import cloudos.exceptions.InvalidRequestException;
import cloudos.exceptions.NotImplementedException;
import cloudos.instances.InstanceService;
import cloudos.models.CostProvidersReservationRequest;
import cloudos.models.Instance;
import cloudos.models.costanalysis.BillingAccount;
import cloudos.models.costanalysis.BillingAccountCost;
import cloudos.models.costanalysis.BillingCostProviderTotal;
import cloudos.models.costanalysis.BillingEstimatedCostMonth;
import cloudos.models.costanalysis.DailyResourceCost;
import cloudos.models.costanalysis.GenericBillingReport;
import cloudos.models.costanalysis.InstanceCost;
import cloudos.models.costanalysis.InstanceTypeCost;
import cloudos.models.costanalysis.LocationCost;
import cloudos.models.costanalysis.OfferingCostByMonths;
import cloudos.models.costanalysis.OfferingUtilizationReport;
import cloudos.models.costanalysis.ProviderDailyCostEntry;
import cloudos.models.costanalysis.ProviderRegionalCost;
import cloudos.models.costanalysis.ProviderServiceCost;
import cloudos.models.costanalysis.ProviderServiceCostComparisonEntry;
import cloudos.models.costanalysis.ProviderTrendEntry;
import cloudos.models.costanalysis.ResourceCost;
import cloudos.models.costanalysis.RetrieveInstanceCostRequest;
import cloudos.models.costanalysis.RetrieveProviderServiceCostRequest;
import cloudos.models.costanalysis.ServiceCategoryCost;
import cloudos.models.services.CloudosService;
import cloudos.models.services.ServiceCategory;
import cloudos.models.services.ServiceException;
import cloudos.models.services.ServiceRequest;
import cloudos.services.CloudosServiceRepository;
import cloudos.services.CloudosServiceService;
import cloudos.utils.DateUtil;
import test.CloudOSTest;
import test.OtherTest;

/**
 * Unit tests for CostAnalysisService.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Category(OtherTest.class)
@Ignore
public class CostAnalysisServiceTest extends CloudOSTest {

  private static final PageRequest FIRST_ITEM = new PageRequest(0, 1);

  List<BillingReport> reports = new ArrayList<>();

  @Mock
  private GenericBillingReportRepository genericBillingReportRepository;

  @Mock
  private InstanceService instanceService;

  @Mock
  private ProviderTrendEntryRepository providerTrendEntryRepository;

  @Mock
  private ProviderDailyCostEntryRepository providerDailyCostEntryRepository;

  @Mock
  private DailyResourceCostService dailyResourceCostService;

  @Value(CostAnalysisTestUtils.COSTANALYSIS_GENERIC_BILLING_REPORTS_DATA_JSON)
  private Resource resource;

  @Autowired
  @InjectMocks
  private CostAnalysisService service;

  @Autowired
  private AwsBillingReportRepository awsBillingReportRepository;

  @Autowired
  private GoogleBillingReportRepository googleBillingReportRepository;

  @Autowired
  private BillingAccountRepository billingAccountRepository;

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private CostAnalysisTestUtils utils;

  @Autowired
  private CloudosServiceRepository cloudosServiceRepository;

  @Autowired
  private CloudosServiceService cloudosServiceService;


  @Test
  public void test0_contextLoads() {
    assertNotNull(service);
    assertNotNull(awsBillingReportRepository);
    assertNotNull(googleBillingReportRepository);
    assertNotNull(billingAccountRepository);
    assertNotNull(resource);
    List<CloudosService> cloudosServices = cloudosServiceService.findAllActive();
    for (CloudosService cloudosService : cloudosServices) {
      cloudosServiceRepository.delete(cloudosService);
    }
  }

  @Test
  public void test1_createBillingAccount() {

    Page<AwsBillingReport> awsBillingReports = awsBillingReportRepository.findAll(FIRST_ITEM);
    reports.addAll(awsBillingReports.getContent());

    Page<GoogleBillingReport> googleBillingReports =
        googleBillingReportRepository.findAll(FIRST_ITEM);
    reports.addAll(googleBillingReports.getContent());

    reports.forEach(report -> {
      billingAccountRepository
          .save(new BillingAccount(report.getAccountId(), report.getProvider()));
    });
  }

  @Test
  public void test2_listBillingAccounts() {
    if (!reports.isEmpty()) {
      List<BillingAccount> result =
          service.findBillingAccountsByProvider(reports.get(0).getProvider());
      assertFalse(result.isEmpty());
    }
  }

  @Test
  public void test3_listTop10BillingAccounts() {
    Date startDate = DateUtils.addMonths(new Date(), -12);
    Date endDate = new Date();
    List<BillingAccountCost> result = service.listTop10BillingAccounts(startDate, endDate);
    assertNotNull(result);
  }

  @Test
  public void listTopServiceCategories() throws ServiceException {
    this.mockCloudosService();
    Date startDate = DateUtils.addMonths(new Date(), -12);
    Date endDate = new Date();
    List<ServiceCategoryCost> result = service.listTopServiceCategories(startDate, endDate);
    assertNotNull(result);
  }

  @Test
  public void listTop10InstanceTypesByCost() {
    List<InstanceTypeCost> instances = service.listTop10InstanceTypesByCost();
    assertNotNull(instances);
  }

  @Test
  public void getCostEstimatedMonthCurrentProvider() {
    BillingEstimatedCostMonth estimatedCosts = service.getCostEstimatedMonthCurrentProvider();
    assertNotNull(estimatedCosts);
  }


  @Test
  public void retrieveCostByProviders() {
    LocalDate to = LocalDate.now();
    LocalDate from = to.minusMonths(1);
    BooleanBuilder builder = Mockito.any();
    // mock data
    List<GenericBillingReport> mockReports =
        utils.createRetrieveProviderRegionCostData(DateUtil.toDate(from));
    when(genericBillingReportRepository.findAll(builder)).thenReturn(mockReports);
    List<BillingCostProviderTotal> responseData =
        service.getCostByProvider(Providers.AMAZON_AWS, from, to);
    assertNotNull(responseData);
  }


  @Test
  public void getCostByProviderAndReservation()
      throws InvalidRequestException, NotImplementedException {
    CostProvidersReservationRequest costProvidersReservationRequest =
        new CostProvidersReservationRequest();
    costProvidersReservationRequest.setProvider(Providers.AMAZON_AWS);
    costProvidersReservationRequest.setReservation("OnDemand");
    BillingCostReservation estimatedCosts =
        service.getCostByProviderAndReservation(costProvidersReservationRequest);
    assertNotNull(estimatedCosts);
  }

  @Test
  @Ignore
  public void getServiceCategoryCost() throws Exception {

    this.mockCloudosService();

    // PREPARE MOCKED DATA
    List<GenericBillingReport> mockedData = objectMapper.readValue(resource.getInputStream(),
        new TypeReference<List<GenericBillingReport>>() {});

    Mockito.when(genericBillingReportRepository.findByProductCodeInAndUsageStartTimeIsBetween(any(),
        any(), any())).thenReturn(mockedData);

    // ACTUAL TEST
    Providers provider = Providers.GOOGLE_COMPUTE_ENGINE;
    ServiceCategory serviceCategory = ServiceCategory.STORAGE;
    ProviderServiceCostComparisonEntry result =
        service.getServiceCategoryCost60DaysReport(provider, serviceCategory);

    // ASSERTS
    utils.validateProviderServiceCostComparisonEntry(result);
  }

  /**
   * @throws ServiceException
   */
  private void mockCloudosService() throws ServiceException {
    ServiceRequest service1 = ServiceRequest.builder()
        .code("com.google.cloud/services/cloud-storage/StorageMultiRegionalUsGbsec").name("EC2")
        .providers(Providers.GOOGLE_COMPUTE_ENGINE).serviceCategory(ServiceCategory.STORAGE)
        .build();
    cloudosServiceService.createService(service1, false);

    ServiceRequest service2 = ServiceRequest.builder()
        .code("com.google.cloud/services/cloud-storage/ClassARequestMultiRegional").name("EC2")
        .providers(Providers.GOOGLE_COMPUTE_ENGINE).serviceCategory(ServiceCategory.STORAGE)
        .build();
    cloudosServiceService.createService(service2, false);

    ServiceRequest service3 =
        ServiceRequest.builder().code("com.google.cloud/services/app-engine/BigstoreStorage")
            .name("EC2").providers(Providers.GOOGLE_COMPUTE_ENGINE)
            .serviceCategory(ServiceCategory.STORAGE).build();
    cloudosServiceService.createService(service3, false);

    ServiceRequest service4 =
        ServiceRequest.builder().code("com.google.cloud/services/compute-engine/StoragePdCapacity")
            .name("EC2").providers(Providers.GOOGLE_COMPUTE_ENGINE)
            .serviceCategory(ServiceCategory.STORAGE).build();
    cloudosServiceService.createService(service4, false);

    ServiceRequest service5 =
        ServiceRequest.builder().code("com.google.cloud/services/big-query/Storage").name("EC2")
            .providers(Providers.GOOGLE_COMPUTE_ENGINE).serviceCategory(ServiceCategory.STORAGE)
            .build();
    cloudosServiceService.createService(service5, false);
  }

  @Test
  public void findBillingAccountById() {
    // create mock data
    Providers provider = Providers.values()[RandomUtils.nextInt(0, Providers.values().length)];
    BillingAccount expected = utils.createBillingAccount(provider);
    BillingAccount result = service.findBillingAccountById(expected.getId());
    assertEquals(expected, result);
  }

  @Test
  public void retrieveInstanceCost() {

    LocalDate now = LocalDate.now();
    Providers provider = Providers.AMAZON_AWS;
    String resourceId = "junit-resource-id";
    String machineType = "t2.nano";

    Instance instance = new Instance();
    instance.setProvider(provider);
    instance.setMachineType(machineType);
    instance.setProviderId(resourceId);
    instance.setIncludedDate(DateUtil.toDate(now));
    List<Instance> instances = new ArrayList<>(Arrays.asList(instance));
    Page<Instance> pages = new PageImpl<Instance>(instances);
    LocalDate startDate = now.minusDays(1);
    LocalDate endDate = LocalDate.now().plusDays(1);

    Date startTime = DateUtil.toDate(startDate);
    Date endTime = DateUtil.toDate(endDate);

    Mockito.when(instanceService.findInstancesByFilter(Mockito.any(), Mockito.any()))
        .thenReturn(pages);

    GenericBillingReport genericBillingReport =
        new GenericBillingReport("junit-id", "junit-lineItemId", provider, "1", "junit-produceCode",
            resourceId, startTime, endTime, BigDecimal.ONE, "junit-location", false);

    List<GenericBillingReport> genericBillingReports = Arrays.asList(genericBillingReport);

    BooleanBuilder builder = Mockito.any();
    Mockito.when(genericBillingReportRepository.findAll(builder)).thenReturn(genericBillingReports);

    RetrieveInstanceCostRequest request = new RetrieveInstanceCostRequest();

    request.setStartDate(startTime);
    request.setEndDate(endTime);
    request.setProvider(provider);
    request.setMachineType(machineType);
    request.setPage(0);
    request.setPageSize(100);
    List<InstanceCost> instanceCosts = service.retrieveInstanceCost(request);

    assertThat(instanceCosts, not(empty()));
  }
  
  @Test
  public void retrieveInstanceCostWhitoutPagination() {

    LocalDate now = LocalDate.now();
    Providers provider = Providers.AMAZON_AWS;
    String resourceId = "junit-resource-id";
    String machineType = "t2.nano";

    Instance instance = new Instance();
    instance.setProvider(provider);
    instance.setMachineType(machineType);
    instance.setProviderId(resourceId);
    instance.setIncludedDate(DateUtil.toDate(now));
    List<Instance> instances = new ArrayList<>(Arrays.asList(instance));

    LocalDate startDate = now.minusDays(1);
    LocalDate endDate = LocalDate.now().plusDays(1);

    Date startTime = DateUtil.toDate(startDate);
    Date endTime = DateUtil.toDate(endDate);

    Mockito.when(instanceService.findInstancesByFilter(Mockito.any()))
        .thenReturn(instances);

    GenericBillingReport genericBillingReport =
        new GenericBillingReport("junit-id", "junit-lineItemId", provider, "1", "junit-produceCode",
            resourceId, startTime, endTime, BigDecimal.ONE, "junit-location", false);

    List<GenericBillingReport> genericBillingReports = Arrays.asList(genericBillingReport);

    BooleanBuilder builder = Mockito.any();
    Mockito.when(genericBillingReportRepository.findAll(builder)).thenReturn(genericBillingReports);

    RetrieveInstanceCostRequest request = new RetrieveInstanceCostRequest();

    request.setStartDate(startTime);
    request.setEndDate(endTime);
    request.setProvider(provider);
    request.setMachineType(machineType);
    List<InstanceCost> instanceCosts = service.retrieveInstanceCost(request);

    assertThat(instanceCosts, not(empty()));
  }

  @Test
  public void retrieveProviderRegionCost() {

    LocalDate startDate = LocalDate.now().minusDays(30);
    LocalDate endDate = LocalDate.now();
    Date startDateObj = DateUtil.toDate(startDate);
    Date endDateObj = DateUtil.toDate(endDate);

    // test data
    List<GenericBillingReport> genericBillingReports =
        utils.createRetrieveProviderRegionCostData(startDateObj);

    Mockito
        .when(genericBillingReportRepository.findByUsageStartTimeBetween(startDateObj, endDateObj))
        .thenReturn(genericBillingReports);

    // expected result
    LocationCost locationCost = new LocationCost("junit-location", BigDecimal.valueOf(11));
    ProviderRegionalCost expected =
        new ProviderRegionalCost(Providers.AMAZON_AWS, Arrays.asList(locationCost));

    // actual test
    List<ProviderRegionalCost> result = service.retrieveProviderRegionalCosts(startDate, endDate);
    assertNotNull(result);
    assertThat(result, Matchers.contains(expected));
  }

  @Test
  public void getLatestProviderTrend() {
    ProviderTrendEntry value =
        new ProviderTrendEntry("junit", LocalDate.now(), Collections.emptyList(), new Date());
    when(providerTrendEntryRepository.findFirstByOrderByUpdatedDesc()).thenReturn(value);

    ProviderTrendEntry result = service.getLatestProviderTrend();
    assertEquals(value, result);
  }

  @Test
  public void retrieveOfferingUtilization() {

    LocalDate end = LocalDate.now();
    LocalDate start = end.minusMonths(1);

    // mock data
    List<GenericBillingReport> mockReports =
        utils.createRetrieveProviderRegionCostData(DateUtil.toDate(start));
    List<Instance> instances = utils.mockInstances(mockReports);
    when(genericBillingReportRepository.findByUsageStartTimeBetween(DateUtil.toDate(start),
        DateUtil.toDate(end))).thenReturn(mockReports);
    when(instanceService.findByCreationDateAfter(DateUtil.toDate(start))).thenReturn(instances);

    // test execution
    List<OfferingUtilizationReport> result = service.retrieveOfferingUtilization(start, end);
    assertThat(result, not(empty()));
    OfferingUtilizationReport firstResult = result.get(0);
    assertEquals(Providers.AMAZON_AWS, firstResult.getProvider());
    assertTrue(BigDecimal.valueOf(48).compareTo(firstResult.getHours()) == 0);
    assertTrue(BigDecimal.valueOf(11).compareTo(firstResult.getCost().getTotalCost()) == 0); // 11
    assertEquals(Long.valueOf(2), firstResult.getNumberOfInstances()); // 2
  }
  
  @Test
  public void retrieveOfferingComparison() {

    LocalDate end = LocalDate.now();
    LocalDate start = end.minusMonths(1);

    // mock data
    List<GenericBillingReport> mockReports =
        utils.createRetrieveProviderRegionCostData(DateUtil.toDate(start));
    List<Instance> instances = utils.mockInstances(mockReports);
    when(genericBillingReportRepository.findByUsageStartTimeBetween(DateUtil.toDate(start),
        DateUtil.toDate(end))).thenReturn(mockReports);
    when(instanceService.findByCreationDateAfter(DateUtil.toDate(start))).thenReturn(instances);

    // test execution
    OfferingCostByMonths result = service.getOfferingCostByMonthComparison();
    assertNotNull(result);
    assertTrue(BigDecimal.valueOf(11).compareTo(result.getTotalMonthToDate()) == 0);
  }
  

  @Test
  public void retrieveProviderDailyCostEntriesBetween() {

    LocalDate end = LocalDate.now();
    LocalDate start = end.minusDays(1);

    List<ProviderDailyCostEntry> resultList = Arrays.asList(
        new ProviderDailyCostEntry(null, Providers.AMAZON_AWS, start, BigDecimal.ONE, new Date()));
    when(providerDailyCostEntryRepository.findByDateBetween(start, end)).thenReturn(resultList);

    List<ProviderDailyCostEntry> result =
        service.retrieveProviderDailyCostEntries(null, start, end);

    assertThat(result, Matchers.allOf(Matchers.notNullValue(), Matchers.not(Matchers.empty())));
  }

  @Test
  public void retrieveProviderDailyCostEntriesBetweenAndProvider() {

    LocalDate end = LocalDate.now();
    LocalDate start = end.minusDays(1);

    List<ProviderDailyCostEntry> resultList = Arrays.asList(
        new ProviderDailyCostEntry(null, Providers.AMAZON_AWS, start, BigDecimal.ONE, new Date()));
    when(providerDailyCostEntryRepository.findByProviderAndDateBetween(Providers.AMAZON_AWS, start,
        end)).thenReturn(resultList);

    List<ProviderDailyCostEntry> result =
        service.retrieveProviderDailyCostEntries(Providers.AMAZON_AWS, start, end);

    assertThat(result, Matchers.allOf(Matchers.notNullValue(), Matchers.not(Matchers.empty())));
  }

  @Test
  public void listTop10ResourcesByCostWithEmptyResultExpectsEmpty() {
    List<DailyResourceCost> emptyList = Collections.emptyList();
    when(dailyResourceCostService.findByDateRange(Mockito.any(), Mockito.any()))
        .thenReturn(emptyList);
    List<ResourceCost> result = service.listTop10ResourcesByCost(null, null);
    assertThat(result, Matchers.allOf(Matchers.notNullValue(), Matchers.empty()));
  }

  @Test
  public void listTop10ResourcesByCosExpectsOnly10ElementsSorted() {

    LocalDate end = LocalDate.now();
    LocalDate start = end.minusMonths(1);

    List<DailyResourceCost> mockData = utils.mock11DailyResourceCost();
    when(dailyResourceCostService.findByDateRange(start, end)).thenReturn(mockData);
    List<ResourceCost> result = service.listTop10ResourcesByCost(start, end);
    assertEquals(10, result.size());

    ResourceCost firstItem = result.get(0);
    ResourceCost lastItem = result.get(9);
    assertTrue(firstItem.getTotalCost().compareTo(lastItem.getTotalCost()) > 0);
  }

  @Test
  public void getCostByProviderAndServiceAndDate()
      throws InvalidRequestException, NotImplementedException {
    RetrieveProviderServiceCostRequest request = new RetrieveProviderServiceCostRequest();
    request.setProvider(Providers.AMAZON_AWS);
    List<ProviderServiceCost> estimatedCosts = service.getCostProviderAndService(request);
    assertNotNull(estimatedCosts);
  }
}
