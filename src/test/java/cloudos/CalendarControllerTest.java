package cloudos;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import test.CloudOSTest;

public class CalendarControllerTest extends CloudOSTest {

  private MockMvc mvc;

  @Before
  public void setUp() throws Exception {
    this.mvc = MockMvcBuilders.standaloneSetup(new CalendarController()).build();
  }

  @Test
  @Ignore
  public void testCalendarController() throws Exception {
    this.mvc
        .perform(get("/calendar"))
        .andExpect(status().isOk())
        .andExpect(view().name("calendar"));
  }
}
