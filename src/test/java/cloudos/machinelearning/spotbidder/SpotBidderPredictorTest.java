package cloudos.machinelearning.spotbidder;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.junit.Assert.assertEquals;

import cloudos.Providers;
import cloudos.machinelearning.spotbidder.AmazonSpotBidderPredictor;
import cloudos.machinelearning.spotbidder.GoogleSpotBidderPredictor;
import cloudos.models.spotinstances.CloudSpotPricingRequest;
import cloudos.models.spotinstances.SpotProductDescription;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

public class SpotBidderPredictorTest extends CloudOSTest {

  @Autowired
  private AmazonSpotBidderPredictor amazonPredictor;

  @Autowired
  private GoogleSpotBidderPredictor googlePredictor;

  @Test
  public void testPricingPrediction() throws Exception {
    List<String> products = Collections.singletonList(SpotProductDescription.AWS_LINUX_UNIX.toString());
    CloudSpotPricingRequest pricingRequest = CloudSpotPricingRequest.builder()
                                                .provider(Providers.AMAZON_AWS)
                                                .region("us-east-1")
                                                .availabilityZone("us-east-1b")
                                                .instanceTypes(Collections.singletonList("m3.medium"))
                                                .productDescription(products)
                                                .build();

    double pricing = this.amazonPredictor.predict(pricingRequest);
    logger.debug("Predicted price: {}", pricing);
    assertThat(pricing, greaterThan(0.0));

    assertEquals(googlePredictor.predict(pricingRequest), 0, 0.0);
  }

}
