package cloudos.machinelearning.spotbidder;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import cloudos.machinelearning.spotbidder.SpotBidderIterator;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.datavec.api.util.ClassPathResource;
import org.junit.Before;
import org.junit.Test;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;

import test.CloudOSTest;

public class SpotBidderIteratorTest extends CloudOSTest {

  private String filename;
  private SpotBidderIterator iterator;

  @Before
  public void setUp() throws Exception {
    String path = "machinelearning/spotbidder/spotprice.csv";
    this.filename =
        new ClassPathResource(path)
            .getFile()
            .getPath();
  }

  @Test
  public void testIteratorConstructor() throws Exception {
    int batchSize = 64; // mini-batch size
    int exampleLength = 22; // time series length
    double splitRatio = 0.9; // 90% for training, 10% for testing
    int epochs = 1; // training epochs

    logger.debug("Creating dataset iterator for file {}...", this.filename);
    iterator = new SpotBidderIterator(this.filename, batchSize, exampleLength, splitRatio);

    logger.debug("Loading test dataset...");
    List<Pair<INDArray, INDArray>> test = iterator.getTestDataSet();

    for (int i = 0; i < epochs; i++) {
      DataSet dataSet;
      while (iterator.hasNext()) {
        dataSet = iterator.next();
        logger.debug("Dataset: {}", dataSet);
      }
    }

    assertNotNull(test);
    assertThat(test.size(), greaterThan(0));
  }
}
