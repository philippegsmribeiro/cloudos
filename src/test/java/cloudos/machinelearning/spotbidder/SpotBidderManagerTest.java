package cloudos.machinelearning.spotbidder;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import cloudos.Providers;
import cloudos.machinelearning.spotbidder.SpotBidderManager;
import cloudos.machinelearning.spotbidder.SpotBidderRequestStatus;
import cloudos.models.AbstractInstance;
import cloudos.models.ActionType;
import cloudos.models.CloudActionRequest;
import cloudos.models.spotinstances.CloudSpotCreateRequest;
import cloudos.models.spotinstances.CloudSpotCreateResponse;
import cloudos.models.spotinstances.CloudSpotPricingRequest;
import cloudos.models.spotinstances.CloudSpotPricingResponse;
import cloudos.models.spotinstances.CloudSpotTerminateResponse;
import cloudos.models.spotinstances.SpotProductDescription;

import com.amazonaws.services.ec2.model.SpotInstanceType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;
import test.SlowTest;

@Category(SlowTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
  public class SpotBidderManagerTest extends CloudOSTest {

  @Autowired
  private SpotBidderManager spotBidderManager;

  static List<String> instanceIds = new ArrayList<>();

  @Test
  public void testPredictSpotPrice() throws Exception {
    List<String> products =
        Collections.singletonList(SpotProductDescription.AWS_LINUX_UNIX.toString());
    CloudSpotPricingRequest pricingRequest = CloudSpotPricingRequest.builder()
        .provider(Providers.AMAZON_AWS).region(region).availabilityZone(zone)
        .instanceTypes(Collections.singletonList(machineType)).productDescription(products).build();

    // predict the spot price
    CloudSpotPricingResponse response = this.spotBidderManager.predictSpotPrice(pricingRequest);
    logger.debug("Predicted price: {}", response);
    assertThat(response.getPrice(), greaterThan(0.0));
  }

  String region = "us-west-1";
  String zone = "us-west-1c";
  String securityGroup = "default";
  String image = "ami-00436f60";
  String machineType = "t1.micro";

  @Test
  @SuppressWarnings("unchecked")
  public void test1Create() throws Exception {
    CloudSpotCreateRequest request = CloudSpotCreateRequest.createAwsCloudRequest(region, null,
        machineType, image, 1, securityGroup, null, zone, null, 0.025, 1, "", null,
        null, null, SpotInstanceType.OneTime, null);

    CloudSpotCreateResponse response = this.spotBidderManager.create(request);
    assertNotNull(response);

    List<AbstractInstance> amazonInstanceList = (List<AbstractInstance>) response.getInstances();
    assertNotNull(amazonInstanceList);
    assertThat(amazonInstanceList.size(), greaterThan(0));

    // list instances created
    amazonInstanceList.forEach(amazonInstance -> {
      logger.debug("AmazonInstance: {}", amazonInstance);
      instanceIds.add(amazonInstance.getInstance().getProviderId());
    });

    assertEquals(response.getAvailabilityZone(), request.getZone());
    assertEquals(response.getMinCount(), request.getMinCount(), 0.001);
    assertEquals(response.getProvider(), request.getProvider());
    assertEquals(response.getSpotPrice(), request.getSpotPrice());
    assertEquals(response.getSpotInstanceType(), request.getSpotInstanceType());
    assertEquals(response.getStatus(), SpotBidderRequestStatus.FULFILLED);

  }

  @Test
  public void test2Terminate() throws Exception {
    CloudActionRequest terminateRequest = new CloudActionRequest(Providers.AMAZON_AWS, instanceIds,
        region, zone, securityGroup, true, ActionType.TERMINATE);
    terminateRequest.setSynchronous(false);
    CloudSpotTerminateResponse terminate = this.spotBidderManager.terminate(terminateRequest);

    assertNotNull(terminate);
    assertEquals(terminate.getProvider(), terminateRequest.getProvider());
    assertEquals(terminate.getRegion(), terminateRequest.getRegion());
  }

}
