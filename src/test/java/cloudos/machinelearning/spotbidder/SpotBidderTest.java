package cloudos.machinelearning.spotbidder;

import static org.junit.Assert.assertNotNull;
import cloudos.instances.InstanceService;
import cloudos.machinelearning.Models;
import cloudos.machinelearning.ModelsRepository;
import cloudos.policies.CloudPolicyService;
import cloudos.provider.ProvidersService;
import java.io.File;
import java.nio.file.Paths;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import test.CloudOSTest;


public class SpotBidderTest extends CloudOSTest {

  @Value("${cloudos.ml.spotbidder}")
  private String spotbidderPath;
  private static String homeDir = System.getProperty("user.home");

  /* The Machine Type repository will used to fetch information about the instance */
  @Autowired
  ProvidersService providersService;

  @Autowired
  ModelsRepository modelsRepository;

  private SpotBidder spotBidder;

  private static String region = "eu-west-1";

  private static String path = homeDir +
      File.separator + "workspace/cloudtown-data" + File.separator +
      region + File.separator;

  private static String provider = "aws";

  private static String root = homeDir +
      File.separator +  "workspace/cloudtown-data";

  private static final String filename = homeDir + File.separator +
      "/workspace/cloudtown-data/ap-northeast-1a/linux/m4.large.csv";

  /* Define the home directory for the ml products for CloudOS */
  @Value("${cloudos.ml.home}")
  String home;

  @Autowired
  private SpotBidderPricingRepository spotBidderPricingRepository;
  @Autowired
  private InstanceService instanceService;
  @Autowired
  private CloudPolicyService cloudPolicyService;


  @Before
  public void setUp() throws Exception {
    this.spotBidder = new SpotBidder(this.spotbidderPath, path, region, modelsRepository,
        spotBidderPricingRepository, this.instanceService, this.cloudPolicyService);
  }

  @Test
  @Ignore
  public void testTrainSingleModel() throws Exception {
      String instanceType = "c1.medium";
      String region = "ap-northeast-1";
      String product = "linux";
      String availabilityZone = "ap-northeast-1a";
      // set the path
      this.spotBidder.train(false, filename, provider, region, availabilityZone, product,
          instanceType);
  }

  @Test
  @Ignore
  public void testTrainAllModels() throws Exception {
    File folder = new File(root);
    File[] regions = folder.listFiles();
    if (regions == null) {
      logger.warn("There are no elements under the root directory {}", root);
      return;
    }
    for (File regionFolder : regions) {
      // list all the files under that region
      if (regionFolder.isDirectory()) {
        logger.debug("Processing region {}", regionFolder.getName());
        File[] availabilityZones = regionFolder.listFiles();
        if (availabilityZones != null) {
          for (File zone : availabilityZones) {
            File[] products = zone.listFiles();
            if (products != null) {
              // list all the product offered
              for (File product : products) {
                if (product.isDirectory()) {
                  // list all the instance types
                  File[] instances = product.listFiles();
                  if (instances != null) {
                    for (File instance : instances) {
                      String instanceType = instance.getName().replace(".csv", "");
                      logger.debug("Processing file {} for instance type {}", instance.getName(),
                          instanceType);
                      // train the model
                      this.spotBidder.train(false,
                          instance.getAbsolutePath(),
                          provider,
                          regionFolder.getName(),
                          zone.getName(),
                          product.getName(),
                          instanceType);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  @Test
  public void testLoadModel() throws Exception {

    // get the local path
    String path = Paths.get(
        homeDir,
        Models.SPOTBIDDER.toString(),
        Models.Task.AWS.toString(),
        "eu-west-1",
        "bidder.zip")
        .toString();
    logger.debug(path);

    SpotBidder spotBidder = new SpotBidder(spotbidderPath, path, "eu-west-1", modelsRepository,
        spotBidderPricingRepository, this.instanceService, this.cloudPolicyService);

    assertNotNull(spotBidder.getNetwork());

  }
}
