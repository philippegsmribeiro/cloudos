package cloudos.machinelearning.autoscaler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import cloudos.Providers;
import cloudos.insights.InsightsService;
import cloudos.instances.CloudManagerService;
import cloudos.instances.InstanceRequestException;
import cloudos.instances.InstanceService;
import cloudos.machinelearning.ModelsRepository;
import cloudos.models.AbstractInstance;
import cloudos.models.Instance;
import cloudos.models.cloudgroup.CloudGroup;
import cloudos.models.cloudgroup.CloudGroupService;
import cloudos.policies.CloudPolicyService;
import cloudos.pricing.PricingException;
import cloudos.provider.ProvidersService;
import cloudos.queue.QueueListenerForTest;
import cloudos.queue.QueueManagerService;
import cloudos.queue.message.QueueMessageType;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import test.CloudOSTest;

@Log4j2
public abstract class UpscaleAbstractTest extends CloudOSTest {

  private static Autoscaler autoscaler;
  private static boolean initialized = false;

  @Value("${cloudos.ml.autoscaler}")
  private String autoscalerPath;

  /* The Machine Type repository will used to fetch information about the instance */
  @Autowired
  ProvidersService providersService;

  @Autowired
  ModelsRepository modelsRepository;

  @Autowired
  InstanceService instanceService;

  @Autowired
  CloudManagerService cloudManagerService;

  @Autowired
  QueueManagerService queueManagerService;

  @Autowired
  CloudGroupService cloudGroupService;

  @Autowired
  QueueListenerForTest queueListenerForTest;

  @Autowired
  InsightsService insightsService;
  
  @Autowired
  CloudPolicyService cloudPolicyService;

  @Before
  public void setUp() throws Exception {
    if (!initialized) {
      insightsService.setReadOnlyMode(false);
      autoscaler = new Autoscaler(this.providersService, 
                                  this.modelsRepository,
                                  this.instanceService, 
                                  this.cloudManagerService, 
                                  this.autoscalerPath, 
                                  this.queueManagerService,
                                  this.cloudGroupService, 
                                  this.insightsService,
                                  this.cloudPolicyService);
      loadPricing();
      initialized = true;
    }
  }

  public void testUpscale(Providers provider) {
    CloudGroup cloudGroup = null;
    List<Instance> instancesFinal = new ArrayList<>();
    try {
      // 1. create cloudgroup
      log.info("Creating a cloud group!");
      cloudGroup = this.createCloudGroup(provider);
      assertNotNull(cloudGroup);
      // 2. create instance
      log.info("Creating an instance!");
      List<? extends AbstractInstance> instances = this.createInstances(cloudGroup);
      assertNotNull(instances);
      assertNotNull(instances.get(0));
      instancesFinal.add(instances.get(0).getInstance());
      assertEquals(instances.get(0).getInstance().getCloudGroupId(), cloudGroup.getId());
      log.info("Validating instance inside the cloudgroup!");

      //
      Integer count = instanceService.countInstancesByCloudGroupId(cloudGroup.getId());
      log.info("Validating Cloudgroup instance: {}", count);
      assertTrue(count == 1);

      // clean the queues
      log.info("Cleaning the queues!");
      queueListenerForTest.purge(QueueMessageType.AUTOSCALE);
      queueListenerForTest.purge(QueueMessageType.SPOT_PRICING_PREDICT_REQUEST);
      queueListenerForTest.purge(QueueMessageType.SPOT_PRICING_PREDICT_RESPONSE);
      queueListenerForTest.purge(QueueMessageType.SPOT_CREATE_REQUEST);
      queueListenerForTest.purge(QueueMessageType.SPOT_CREATE_RESPONSE);

      //
      cloudGroup = this.cloudGroupService.describeCloudGroup(cloudGroup.getId());
      assertTrue(!cloudGroup.getIsBlocked().get());

      // 3. call upscale
      log.info("Forcing an upscale!");
      autoscaler.upscale(instances.get(0).getInstance(), cloudGroup);

      cloudGroup = this.cloudGroupService.describeCloudGroup(cloudGroup.getId());
      // it is not blocked yet
      assertTrue(!cloudGroup.getIsBlocked().get());

      // 4. force read the messages
      Thread.sleep(1000);
      log.info("Reading upscale message!");
      queueListenerForTest.read(QueueMessageType.AUTOSCALE);

      cloudGroup = this.cloudGroupService.describeCloudGroup(cloudGroup.getId());
      // it is blocked
      assertTrue(cloudGroup.getIsBlocked().get());

      try {
        // forcing a second upscale on the same group
        // it will not do anything
        log.info("Forcing an upscale!");
        autoscaler.upscale(instances.get(0).getInstance(), cloudGroup);

        Thread.sleep(1000);
        log.info("Reading upscale message!");
        queueListenerForTest.read(QueueMessageType.AUTOSCALE);

      } catch (Exception e) {
        log.error(e);
      }

      Thread.sleep(1000);
      log.info("Reading spot pricing request message!");
      queueListenerForTest.read(QueueMessageType.SPOT_PRICING_PREDICT_REQUEST);
      Thread.sleep(1000);
      log.info("Reading spot pricing response message!");
      queueListenerForTest.read(QueueMessageType.SPOT_PRICING_PREDICT_RESPONSE);
      Thread.sleep(1000);
      log.info("Reading spot create request message!");
      queueListenerForTest.read(QueueMessageType.SPOT_CREATE_REQUEST);
      Thread.sleep(1000);
      log.info("Reading spot create response message!");
      queueListenerForTest.read(QueueMessageType.SPOT_CREATE_RESPONSE);
      Thread.sleep(1000);


      count = instanceService.countInstancesByCloudGroupId(cloudGroup.getId());
      log.info("Validating Cloudgroup instance: {}", count);
      assertTrue(count == 2);

      cloudGroup = this.cloudGroupService.describeCloudGroup(cloudGroup.getId());
      assertTrue(!cloudGroup.getIsBlocked().get());
      // get the instances final to terminate them
      instancesFinal = instanceService.findAllByCloudGroupId(cloudGroup.getId());

      System.out.println();
    } catch (Exception e) {
      log.error("Error", e);
      fail(e.getLocalizedMessage());
    } finally {
      if (CollectionUtils.isNotEmpty(instancesFinal)) {
        // terminate
        log.info("Terminating instances!");
        terminateInstance(instancesFinal);
      }
      if (cloudGroup != null) {
        //
        log.info("Deleting cloudgroup!");
        boolean deleted = this.deleteCloudGroup(cloudGroup.getId());
        assertTrue(deleted);
      }
    }
  }

  protected abstract List<? extends AbstractInstance> createInstances(CloudGroup cloudGroup) throws InstanceRequestException;

  private CloudGroup createCloudGroup(Providers provider) {
    CloudGroup cloudGroup = CloudGroup.builder().provider(provider)
        .name("AWSCloudGroupTest_" + generator.generate(6)).min(1).max(10).build();
    // create a group
    CloudGroup group = this.cloudGroupService.createCloudGroup(cloudGroup);
    return group;
  }

  private boolean deleteCloudGroup(String cloudGroupId) {
    return this.cloudGroupService.deleteCloudGroup(cloudGroupId);
  }

  protected abstract void terminateInstance(List<Instance> instances) ;

  protected abstract void loadPricing() throws PricingException;
}
