package cloudos.machinelearning.autoscaler;

import static org.junit.Assert.fail;
import cloudos.Providers;
import cloudos.insights.InsightsService;
import cloudos.instances.CloudManagerService;
import cloudos.instances.InstanceService;
import cloudos.machinelearning.ModelsRepository;
import cloudos.models.Instance;
import cloudos.models.InstanceStatus;
import cloudos.models.cloudgroup.CloudGroup;
import cloudos.models.cloudgroup.CloudGroupService;
import cloudos.policies.AutoscalerPolicy;
import cloudos.policies.AutoscalerPolicyEntry;
import cloudos.policies.CloudPolicyService;
import cloudos.provider.ProvidersService;
import cloudos.queue.QueueListenerForTest;
import cloudos.queue.QueueManagerService;
import com.amazonaws.regions.Regions;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Test;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import test.CloudOSTest;

public class AutoscalerTest extends CloudOSTest {

  private static Autoscaler autoscaler;
  private static boolean initialized = false;

  @Value("${cloudos.ml.autoscaler}")
  private String autoscalerPath;

  /* The Machine Type repository will used to fetch information about the instance */
  @Autowired
  ProvidersService providersService;

  @Autowired
  ModelsRepository modelsRepository;

  @Autowired
  InstanceService instanceService;

  @Autowired
  CloudManagerService cloudManagerService;

  @Autowired
  QueueManagerService queueManagerService;

  @Autowired
  CloudGroupService cloudGroupService;

  @Autowired
  QueueListenerForTest queueListenerForTest;

  @Autowired
  InsightsService insightsService;
  
  @Autowired
  CloudPolicyService cloudPolicyService;
  
  @Before
  public void setUp() throws Exception {
    if (!initialized) {
      autoscaler = new Autoscaler(this.providersService, 
                                  this.modelsRepository,
                                  this.instanceService, 
                                  this.cloudManagerService, 
                                  this.autoscalerPath, 
                                  this.queueManagerService,
                                  this.cloudGroupService, 
                                  this.insightsService,
                                  this.cloudPolicyService);
      initialized = true;
    }
  }
  
  @Test
  public void testPredict() {
    String sufix = generator.generate(4);
    
    String providerId = "i-test" + sufix;
    String region = Regions.US_EAST_1.getName();
    String cloudGroupId = "cg-id-test" + sufix;
    String cloudGroupName = "cg-name-test"  + sufix;
    String policyName = "policy-" + sufix;

    double[] data = new double[] {0.00,  0.00,  0.00,  0.00,  0.00,  0.00,  0.00};
    Pair<String, INDArray> vector = new ImmutablePair<String, INDArray>(providerId, Nd4j.create(data));
    
    try {
      // assert error if instance doesnt exists
      autoscaler.predict(region, vector);
      fail();
    } catch (PredictionException e1) {
      logger.debug(e1.getLocalizedMessage());
    }
    
    // create fake instance
    Instance instance = new Instance();
    instance.setProvider(Providers.AMAZON_AWS);
    instance.setRegion(region);
    instance.setProviderId(providerId);
    instanceService.save(instance);

    try {
      // assert error if instance not running
      autoscaler.predict(region, vector);
      fail();
    } catch (PredictionException e1) {
      logger.debug(e1.getLocalizedMessage());
    }

    // update the stats
    instance.setStatus(InstanceStatus.RUNNING);
    instanceService.save(instance);

    try {
      // assert error if not cloudgroup
      autoscaler.predict(region, vector);
      fail();
    } catch (PredictionException e1) {
      logger.debug(e1.getLocalizedMessage());
    }

    // update the cloudgroup
    instance.setCloudGroupId(cloudGroupId);
    instanceService.save(instance);
    
    try {
      // assert error if cloudgroup not valid
      autoscaler.predict(region, vector);
      fail();
    } catch (PredictionException e1) {
      logger.debug(e1.getLocalizedMessage());
    }
    
    CloudGroup cloudGroup = CloudGroup.builder()
                                      .id(cloudGroupId)
                                      .min(1)
                                      .max(10)
                                      .name(cloudGroupName)
                                      .provider(instance.getProvider())
                                      .build();
    cloudGroupService.createCloudGroup(cloudGroup);

    List<AutoscalerPolicyEntry> entries = Collections.singletonList(
        AutoscalerPolicyEntry.builder()
            .startingGroupSize(1)
            .instanceProtection(false)
            .threshold(0.80)
            .resourceCategory(ResourceCategory.CPU_USAGE)
            .build()
    );
    AutoscalerPolicy autoscalerPolicy = AutoscalerPolicy.builder()
        .autoscalePolicies(entries)
        .build();

    autoscalerPolicy.setName(policyName);

    cloudPolicyService.save(cloudGroupId, autoscalerPolicy);
    
    try {
      autoscaler.predict(region, vector);
    } catch (PredictionException e) {
      e.printStackTrace();
    }
  }
}
