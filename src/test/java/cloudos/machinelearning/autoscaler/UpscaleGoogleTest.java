package cloudos.machinelearning.autoscaler;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cloudos.Providers;
import cloudos.instances.GoogleInstanceManagement;
import cloudos.instances.InstanceRequestException;
import cloudos.models.AbstractInstance;
import cloudos.models.ActionType;
import cloudos.models.CloudActionRequest;
import cloudos.models.CloudCreateRequest;
import cloudos.models.Instance;
import cloudos.models.cloudgroup.CloudGroup;
import cloudos.pricing.GooglePricingManagement;
import cloudos.pricing.PricingException;
import cloudos.pricing.PricingType;

public class UpscaleGoogleTest extends UpscaleAbstractTest {

  @Test
  public void testUpscaleGoogle() {
    this.testUpscale(Providers.GOOGLE_COMPUTE_ENGINE);
  }

  private String zone = "us-west1-a";
  private String region = "us-west1";
  private String machineType = "f1-micro";
  private String imageProject = "ubuntu-os-cloud";
  private String image = "ubuntu-1604-xenial-v20171212";

  @Autowired
  private GoogleInstanceManagement googleManagement;

  @Autowired
  private GooglePricingManagement googlePricing;

  @Override
  protected List<? extends AbstractInstance> createInstances(CloudGroup cloudGroup) throws InstanceRequestException {
      CloudCreateRequest createGCloudRequest = CloudCreateRequest.createGCloudRequest(region,
          generator.generate(5), machineType, image, imageProject, 1, "gcloudsshkey", zone);
      createGCloudRequest.setCloudGroup(cloudGroup.getId());
      return googleManagement.createInstances(createGCloudRequest);
  }

  @Override
  protected void terminateInstance(List<Instance> instances) {
    CloudActionRequest terminateRequest = CloudActionRequest.createGCloudRequest(
        instances.stream().map(i -> i.getName()).collect(Collectors.toList()), region,
        zone, ActionType.TERMINATE);
    terminateRequest.setSynchronous(false);
    googleManagement.terminate(terminateRequest);
  }

  @Override
  protected void loadPricing() throws PricingException {
    if(googlePricing.getPricingForInstanceType(machineType, region, false, imageProject) == null) {
      googlePricing.loadPricingData(PricingType.INSTANCE);
    }
  }
}
