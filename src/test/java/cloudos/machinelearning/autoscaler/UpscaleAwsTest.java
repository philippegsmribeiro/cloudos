package cloudos.machinelearning.autoscaler;

import cloudos.Providers;
import cloudos.instances.AmazonInstanceManagement;
import cloudos.instances.InstanceRequestException;
import cloudos.models.AbstractInstance;
import cloudos.models.ActionType;
import cloudos.models.CloudActionRequest;
import cloudos.models.CloudCreateRequest;
import cloudos.models.Instance;
import cloudos.models.cloudgroup.CloudGroup;
import cloudos.pricing.AmazonPricingManagement;
import cloudos.pricing.PricingException;
import cloudos.pricing.PricingType;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;

import test.SlowTest;

@Category(SlowTest.class)
public class UpscaleAwsTest extends UpscaleAbstractTest {

  @Test
  @Ignore
  public void testUpscaleAws() {
    this.testUpscale(Providers.AMAZON_AWS);
  }

  private String aws_region = "us-east-1";
  private String aws_zone = "us-east-1a";
  private String aws_machineType = "t2.nano";
  private String aws_image = "ami-f4cc1de2";
  private String securityGroup = "default";

  @Autowired
  private AmazonInstanceManagement amazonManagement;
  @Autowired
  AmazonPricingManagement amazonPricingManagement;

  @Override
  protected List<? extends AbstractInstance> createInstances(CloudGroup cloudGroup) throws InstanceRequestException {
      CloudCreateRequest createAwsRequest =
          CloudCreateRequest.createAwsRequest(aws_region, aws_machineType, aws_image, securityGroup,
              1, 1, "gcloudsshkey", aws_zone, generator.generate(5));
      createAwsRequest.setCloudGroup(cloudGroup.getId());
      return amazonManagement.createInstances(createAwsRequest);
  }

  @Override
  protected void terminateInstance(List<Instance> instances) {
    CloudActionRequest terminateRequest = CloudActionRequest.createGCloudRequest(
        instances.stream().map(i -> i.getName()).collect(Collectors.toList()), aws_region,
        aws_zone, ActionType.TERMINATE);
    terminateRequest.setSynchronous(false);
    amazonManagement.terminate(terminateRequest);
  }

  @Override
  protected void loadPricing() throws PricingException {
    if(amazonPricingManagement.getPricingForInstanceType(aws_machineType,aws_region,false,aws_image) == null) {
      amazonPricingManagement.loadPricingData(PricingType.INSTANCE);
    }
  }
}
