package cloudos.keys;

import cloudos.ItemKeyRegionGrouped;
import cloudos.Providers;
import cloudos.exceptions.NotImplementedException;
import cloudos.keys.cloudoskey.CloudosKey;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import test.CloudOSTest;

/** Created by gleimar on 30/04/2017. */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class KeyManagerServiceTest extends CloudOSTest {

  @Autowired private KeyManagerService keyManagerService;

  private String keyName = "cloudosKeyTest";
  private String awsRegion = "us-west-1";
  
  @Test
  public void testAWS1GenerateKey() throws KeyManagementException {
    this.testGenerateKey(Providers.AMAZON_AWS, awsRegion);
  }

  @Test
  public void testGCE1GenerateKey() throws KeyManagementException {
    this.testGenerateKey(Providers.GOOGLE_COMPUTE_ENGINE, "");
  }

  @Test
  public void testAWS2DeleteKey() throws KeyManagerServiceException, KeyManagementException {
    this.testDeleteKey(Providers.AMAZON_AWS, awsRegion);
  }

  @Test
  public void testGCE3DeleteKey() throws KeyManagerServiceException, KeyManagementException {
    this.testDeleteKey(Providers.GOOGLE_COMPUTE_ENGINE, "");
  }

  @Test
  public void testGCE2KeyPair() throws NotImplementedException, KeyManagementException {
    CloudosKey cloudosKey =
        this.keyManagerService.findKey(keyName, Providers.GOOGLE_COMPUTE_ENGINE, "");

    Map.Entry<Path, Path> keyPairFiles = this.keyManagerService.generateKeyPairFiles(cloudosKey);

    assert keyPairFiles != null;

    logger.debug(String.format("path private key: %s", keyPairFiles.getKey()));
    assert keyPairFiles.getKey() != null;

    logger.debug(String.format("path public key: %s", keyPairFiles.getValue()));
    assert keyPairFiles.getValue() != null;
  }

  @Test
  public void listKeyByProvider() {
    List<ItemKeyRegionGrouped> keysByProvider =
        this.keyManagerService.findKeyByProviderGroupedByRegion(Providers.AMAZON_AWS);

    keysByProvider.forEach(
        item -> {
          StringBuilder keysName = new StringBuilder();

          item.getKeys()
              .forEach(key -> keysName.append(String.format("KeyName: %s ", key.getKeyName())));

          logger.debug(String.format("Region: %s Keys : %s", item.getName(), keysName.toString()));
        });
  }

  public void testGenerateKey(Providers provider, String region) throws KeyManagementException {
    try {

      CloudosKey key = this.keyManagerService.findKey(keyName, provider, region);

      assert key == null || key.getDeleted() != null;

      CloudosKey cloudosKey = this.keyManagerService.generateKey(keyName, provider, region);

      assert cloudosKey != null;
    } catch (NotImplementedException e) {
      logger.error("", e);
    } catch (KeyManagerServiceException e) {
      logger.error(e);
    }
  }

  public void testDeleteKey(Providers provider, String region)
      throws KeyManagerServiceException, KeyManagementException {
    try {
      CloudosKey cloudosKey = this.keyManagerService.deleteKey(keyName, provider, region);

      assert cloudosKey != null;

      CloudosKey key = this.keyManagerService.findKey(keyName, provider, region);

      assert key == null;
    } catch (NotImplementedException e) {
      logger.error(e);
    }
  }
}
