package cloudos.keys.cloudoskey;

import cloudos.Providers;
import cloudos.keys.cloudoskey.CloudosKey;
import cloudos.keys.cloudoskey.CloudosKeyService;
import com.amazonaws.regions.Regions;
import org.junit.After;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import test.CloudOSTest;

/** Created by gleimar on 01/07/2017. */
public class CloudosKeyRepositoryTest extends CloudOSTest {
  private static final Logger logger = LoggerFactory.getLogger(CloudosKeyRepositoryTest.class);

  @Autowired 
  private CloudosKeyService cloudosKeyService;

  private static CloudosKey cloudosKeyCreated = null;

  @Test
  public void testFilterDeletedKey() {

    CloudosKey cloudosKey = this.generateRandomKey();

    logger.debug("CloudosKey:{}", cloudosKey);

    cloudosKeyCreated = this.cloudosKeyService.save(cloudosKey);

    CloudosKey deleted =
        this.cloudosKeyService.findDeletedKeyByKeyNameAndProviderAndRegion(
            cloudosKey.getKeyName(), cloudosKey.getProvider(), cloudosKey.getRegion());

    logger.debug("CloudosKey: {}", deleted);

    assert deleted == null;
  }

  @After
  public void tearDown() {
    if (cloudosKeyCreated != null) {
      this.cloudosKeyService.delete(cloudosKeyCreated);
    }
  }

  private CloudosKey generateRandomKey() {

    String keyName = "cos-test-" + generator.generate(5);

    CloudosKey cloudosKey = new CloudosKey();
    cloudosKey.setKeyName(keyName);
    cloudosKey.setRegion(Regions.AP_NORTHEAST_1.getName());
    cloudosKey.setProvider(Providers.AMAZON_AWS);

    return cloudosKey;
  }
}
