package cloudos;

import cloudos.credentials.CloudCredentialActionException;
import cloudos.exceptions.NotImplementedException;
import cloudos.keys.KeyManagerServiceTest;
import cloudos.models.AwsSecurityGroup;
import cloudos.models.CloudosSecurityGroup;
import cloudos.securitygroup.CloudSecurityGroupException;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

/** Created by gleimar on 20/05/2017. */
public class SecurityGroupManagerServiceTest extends CloudOSTest {
  private static final Logger logger = Logger.getLogger(KeyManagerServiceTest.class);

  @Autowired private SecurityGroupManagerService securityGroupManagerService;

  private static CloudosSecurityGroup cosSecurityCreated = null;

  @Test
  public void test1Create()
      throws NotImplementedException, CloudCredentialActionException, CloudSecurityGroupException {
    AwsSecurityGroup awsSecurityGroup = new AwsSecurityGroup();
    awsSecurityGroup.setName(
        String.format("cos-sg-%s", generator.generate(5)));
    awsSecurityGroup.setGroupId("groupIdTest");
    awsSecurityGroup.setRegion("us-west-1");
    awsSecurityGroup.setDescription("Security group description test case");

    CloudosSecurityGroup cloudosSecurityGroup =
        this.securityGroupManagerService.create(awsSecurityGroup);

    logger.debug(String.format("cloudosSecurityGroup id: %s", cloudosSecurityGroup.getId()));

    assert cloudosSecurityGroup != null;
    assert StringUtils.isNotBlank(cloudosSecurityGroup.getId());

    cosSecurityCreated = cloudosSecurityGroup;
  }

  @Test
  public void test2Delete() throws CloudSecurityGroupException, NotImplementedException {
    this.securityGroupManagerService.delete(cosSecurityCreated);
  }
}
