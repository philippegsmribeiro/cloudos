package cloudos.deploy.deployers;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import cloudos.amazon.AWSCredentialService;
import cloudos.amazon.AmazonEC2OnDemand;
import cloudos.amazon.Key;
import cloudos.deploy.deployers.SaltService;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.Instance;
import com.google.common.collect.Sets;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.log4j.Log4j2;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import test.CloudOSTest;

@Ignore
@Log4j2
public class SaltServiceTest extends CloudOSTest {

  private String privateKey;

  @Autowired private SaltService salt;

  private static boolean initialized = false;

  private final String securityGroup = "default";
  private final String keyPairName = "test_key";
  private Key key;
  private AmazonEC2OnDemand onDemand;
  private AmazonEC2 client;
  private List<String> instanceIds;

  @Value("${cloudos.build.salt.host}")
  private String host;

  @Value("${cloudos.build.salt.username}")
  private String username;

  @Value("${cloudos.build.salt.password}")
  private String password;

  @Autowired AWSCredentialService awsCredentialService;

  @Before
  public void setUp() throws Exception {
    if (!initialized) {
      this.client =
          AmazonEC2ClientBuilder.standard()
              .withCredentials(awsCredentialService.getAWSCredentials())
              .withRegion(Regions.US_EAST_1)
              .build();
      this.key = new Key();
      assertNotNull(this.key);
      assertNotNull(this.client);
      this.privateKey = this.key.getPrivateKey(this.keyPairName, this.client);
      assertNotNull(this.privateKey);
      this.onDemand = new AmazonEC2OnDemand(this.securityGroup, client);
      this.instanceIds = new ArrayList<>();

      initialized = true;
    }
  }

  @Test
  @Ignore
  public void testSshConfig() throws Exception {
    this.salt =
        new SaltService()
            .withHost(this.host)
            .withUsername(this.username)
            .withPassword(this.password)
            .withSaltClient(this.host);
    assertNotNull(this.salt.getClient());
    log.info(this.salt.getUrl());
    try {
      this.salt.sshConfig();
    } catch (Exception ex) {
      assertNull(ex);
    }
  }

  @Test
  @Ignore
  public void testEvents() throws Exception {
    this.salt =
        new SaltService()
            .withHost(this.host)
            .withUsername(this.username)
            .withPassword(this.password)
            .withSaltClient(this.host);
    assertNotNull(this.salt.getClient());
    log.info(this.salt.getUrl());
    try {
      this.salt.events();
    } catch (Exception ex) {
      assertNull(ex);
    }
  }

  @Test
  @Ignore
  public void testInfo() throws Exception {
    this.salt =
        new SaltService()
            .withHost(this.host)
            .withUsername(this.username)
            .withPassword(this.password)
            .withSaltClient(this.host);
    this.salt.getInfo();
  }

  @Test
  @Ignore
  public void testCall() throws Exception {
    this.salt =
        new SaltService()
            .withHost(this.host)
            .withUsername(this.username)
            .withPassword(this.password)
            .withSaltClient(this.host);
    log.info("################################");
    log.info(this.salt.getClient());
    log.info("################################");
    assertNotNull(this.salt.getClient());
    try {
      this.salt.call();
    } catch (Exception ex) {
      assertNull(ex);
    }
  }

  @Test
  @Ignore
  public void testLocate() throws Exception {
    this.salt =
        new SaltService()
            .withHost(this.host)
            .withUsername(this.username)
            .withPassword(this.password)
            .withSaltClient(this.host);
    assertNotNull(this.salt.getClient());
    log.info(this.salt.getUrl());
    try {
      this.salt.locate();
    } catch (Exception ex) {
      assertNull(ex);
    }
  }

  @Test
  public void testWheel() throws Exception {
    this.salt =
        new SaltService()
            .withHost(this.host)
            .withUsername(this.username)
            .withPassword(this.password)
            .withSaltClient(this.host);
    log.info("################################");
    log.info(this.salt.getClient());
    log.info("################################");
    assertNotNull(this.salt.getClient());
    this.salt.wheels();
    this.salt.keyAccept("fake.key");
  }

  @Test
  @Ignore
  public void testSaltCluster() throws Exception {
    this.salt.setCredentials(this.privateKey);
    // create two minion instances
    List<Instance> instances =
        this.onDemand.createInstance(
            "ami-59e8964e",
            "m1.small",
            1,
            1,
            this.keyPairName,
            this.securityGroup,
            null,
            null,
            null, null);
    for (Instance instance : instances) {
      this.instanceIds.add(instance.getInstanceId());
    }

    this.salt.setMinions(Sets.newHashSet(instances));

    // Test the master
    assertNotNull(this.salt.getCredentials());
    assertNotNull(this.salt.getMinions());

    // Test the minions
    assertNotNull(this.salt.getMinions());
    try {
      this.salt.initSaltCluster();
    } catch (Exception ex) {
      assertNull(ex);
    }
  }

  @After
  public void tearDown() throws Exception {
    if (this.key != null) {
      this.key.deleteKey(this.keyPairName, this.client);
    }
    if (this.onDemand != null) {
      this.onDemand.terminate(this.instanceIds);
    }
  }
}
