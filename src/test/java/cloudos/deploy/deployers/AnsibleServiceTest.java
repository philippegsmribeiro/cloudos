package cloudos.deploy.deployers;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import cloudos.Providers;
import cloudos.deploy.DeployFailedException;
import cloudos.deploy.deployers.AnsibleService;
import cloudos.deploy.deployers.Deployers;
import cloudos.deploy.deployments.Deployment;
import cloudos.deploy.deployments.DeploymentStatus;
import cloudos.instances.CloudManagerService;
import cloudos.models.AbstractInstance;
import cloudos.models.ActionType;
import cloudos.models.CloudActionRequest;
import cloudos.models.CloudCreateRequest;
import cloudos.utils.FileUtils;

import java.util.Arrays;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Ignore
public class AnsibleServiceTest extends CloudOSTest {

  @Autowired
  private AnsibleService ansibleService;
  @Autowired
  private CloudManagerService cloudManagerService;

  private String zone = "us-west1-a";
  private String region = "us-west1";
  private String machineTypeLow = "f1-micro";
  private String imageP = "ubuntu-os-cloud";
  private String imageI = "ubuntu-1604-xenial-v20161221";

  /**
   * Test an ansible deployment from a simple text config
   *
   * @throws Exception
   */
  @Test
  public void test1NginxDeploymentText() throws Exception {
    String instanceName = "test-ansible-" + generator.generate(2);
    try {
      Deployment deployment = new Deployment();
      deployment.setConfigFilePath(this.getClass().getResource("nginx.yml").getFile());
      List<? extends AbstractInstance> instances = getInstanceListForTest(instanceName);
      deployment.setInstancesNumber(instances.size());
      deployment.setInstances(instances);
      deployment.setDeployer(Deployers.ANSIBLE);
      deployment.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
      deployment = ansibleService.deploy(deployment);
      assertTrue(DeploymentStatus.FINISHED.equals(deployment.getStatus()));

      AbstractInstance abstractInstance = deployment.getInstances().get(0);
      // see if the nginx is working
      String ip = abstractInstance.getInstance().getIpAddress();
      String content = FileUtils.readUrlContent(String.format("http://%s", ip));
      assertNotNull(content);
      logger.debug(content);
      assertTrue(content.contains("Welcome to nginx!"));
    } catch (Exception e) {
      logger.error(e.getLocalizedMessage(), e);
      fail(e.getLocalizedMessage());
    } finally {
      try {
        this.deleteInstance(instanceName);
      } catch (Exception e2) {
        logger.error(e2.getLocalizedMessage(), e2);
      }
    }
  }

  /**
   * Test an ansible deployment from a zip file
   *
   * @throws Exception
   */
  @Test
  @Ignore
  public void test2CloudOSClientZip() throws Exception {
    String instanceName = "test-ansible-" + generator.generate(2);
    try {
      Deployment deployment = new Deployment();
      deployment.setConfigFilePath(this.getClass().getResource("cloudos-client.yml").getFile());
      List<? extends AbstractInstance> instances = getInstanceListForTest(instanceName);
      deployment.setInstancesNumber(instances.size());
      deployment.setInstances(instances);
      deployment.setDeployer(Deployers.ANSIBLE);
      deployment.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
      deployment = ansibleService.deploy(deployment);
      assertTrue(DeploymentStatus.FINISHED.equals(deployment.getStatus()));
    } catch (Exception e) {
      logger.error(e.getLocalizedMessage(), e);
      fail(e.getLocalizedMessage());
    } finally {
      try {
        this.deleteInstance(instanceName);
      } catch (Exception e2) {
        logger.error(e2.getLocalizedMessage(), e2);
      }
    }
  }

  /**
   * Test an ansible deployment from a zip file
   *
   * @throws Exception
   */
  @Test
  public void test3LampZip() throws Exception {
    String instanceName = "test-ansible-" + generator.generate(2);
    try {
      Deployment deployment = new Deployment();
      deployment.setConfigFilePath(this.getClass().getResource("lamp.yml").getFile());
      List<? extends AbstractInstance> instances = getInstanceListForTest(instanceName);
      deployment.setInstancesNumber(instances.size());
      deployment.setInstances(instances);
      deployment.setDeployer(Deployers.ANSIBLE);
      deployment.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
      deployment = ansibleService.deploy(deployment);
      assertTrue(DeploymentStatus.FINISHED.equals(deployment.getStatus()));
    } catch (Exception e) {
      logger.error(e.getLocalizedMessage(), e);
      fail(e.getLocalizedMessage());
    } finally {
      try {
        this.deleteInstance(instanceName);
      } catch (Exception e2) {
        logger.error(e2.getLocalizedMessage(), e2);
      }
    }
  }

  /**
   * Test an ansible deployment from a zip file
   *
   * @throws Exception
   */
  @Test
  public void test3Zip() throws Exception {
    String instanceName = "test-ansible-" + generator.generate(2);
    try {
      Deployment deployment = new Deployment();
      deployment.setConfigFilePath(this.getClass().getResource("provisioning.zip").getFile());
      List<? extends AbstractInstance> instances = getInstanceListForTest(instanceName);
      deployment.setInstancesNumber(instances.size());
      deployment.setInstances(instances);
      deployment.setDeployer(Deployers.ANSIBLE);
      deployment.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
      deployment = ansibleService.deploy(deployment);
      assertTrue(DeploymentStatus.FINISHED.equals(deployment.getStatus()));
    } catch (Exception e) {
      logger.error(e.getLocalizedMessage(), e);
      fail(e.getLocalizedMessage());
    } finally {
      try {
        this.deleteInstance(instanceName);
      } catch (Exception e2) {
        logger.error(e2.getLocalizedMessage(), e2);
      }
    }
  }

  /**
   * Test a deployment of a wrong zip file (without the playbook.yml file)
   *
   * @throws Exception
   */
  @Test(expected = DeployFailedException.class)
  public void test4WrongZip() throws Exception {
    String instanceName = "test-ansible" + generator.generate(2);
    try {
      Deployment deployment = new Deployment();
      deployment.setConfigFilePath(this.getClass().getResource("provisioningWrong.zip").getFile());
      List<? extends AbstractInstance> instances = getInstanceListForTest(instanceName);
      deployment.setInstancesNumber(instances.size());
      deployment.setInstances(instances);
      deployment.setDeployer(Deployers.ANSIBLE);
      deployment.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
      deployment = ansibleService.deploy(deployment);
    } catch (Exception e) {
      if(e instanceof DeployFailedException) {
        throw e;
      } else {
        logger.error(e.getLocalizedMessage(), e);
        fail(e.getLocalizedMessage());
      }
    } finally {
      try {
        this.deleteInstance(instanceName);
      } catch (Exception e2) {
        logger.error(e2.getLocalizedMessage(), e2);
      }
    }

  }

  private void deleteInstance(String instanceName) throws Exception {
    CloudActionRequest request = CloudActionRequest.createGCloudRequest(Arrays.asList(instanceName),
        region, zone, ActionType.TERMINATE);
    request.setSynchronous(false);
    cloudManagerService.terminateInstances(request);
  }

  private List<? extends AbstractInstance> getInstanceListForTest(String instanceName)
      throws Exception {
    CloudCreateRequest request = CloudCreateRequest.createGCloudRequest(region, instanceName,
        machineTypeLow, imageI, imageP, 1, "gcloudsshkey", zone);
    return cloudManagerService.createInstances(request);
  }
}
