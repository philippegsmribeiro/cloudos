package cloudos.deploy;

import static org.junit.Assert.assertTrue;
import cloudos.Providers;
import cloudos.deploy.client.ClientInstallService;
import cloudos.deploy.deployments.Deployment;
import cloudos.deploy.deployments.DeploymentStatus;
import cloudos.instances.CloudManagerService;
import cloudos.models.AbstractInstance;
import cloudos.models.CloudCreateRequest;

import java.util.List;

import org.bson.types.ObjectId;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Ignore
public class ClientInstallServiceTest extends CloudOSTest {

  @Autowired private ClientInstallService clientInstallService;
  @Autowired private CloudManagerService cloudManagerService;

  private String zone = "us-west1-a";
  private String region = "us-west1";
  private String machineTypeLow = "f1-micro";
  private String imageP = "ubuntu-os-cloud";
  private String imageI = "ubuntu-1604-xenial-v20161221";
  private String instanceName = "test-ansible";

  private final String aws_securityGroup = "default";
  private final String aws_keyPairName = "gcloudsshkey";
  private final String aws_region = "us-east-1";
  private final String aws_zone = "us-east-1a";
  private final String aws_machine = "m1.small";
  private final String aws_image = "ami-1acdd90d";

  private String deploymentId = generator.generate(20);

  @Test
  public void testClientInstallGoogle() throws Exception {
    List<? extends AbstractInstance> instances = getInstanceListForTest();
    Deployment deployment =
        clientInstallService.installClient(
            instances, Providers.GOOGLE_COMPUTE_ENGINE, deploymentId);
    assertTrue(deployment != null);
    assertTrue(DeploymentStatus.FINISHED.equals(deployment.getStatus()));
  }

  @Test
  public void testClientInstallAWS() throws Exception {
    List<? extends AbstractInstance> instances = getInstanceListForTestAWS();
    Deployment deployment =
        clientInstallService.installClient(instances, Providers.AMAZON_AWS, deploymentId);
    assertTrue(deployment != null);
    assertTrue(DeploymentStatus.FINISHED.equals(deployment.getStatus()));
  }

  private List<? extends AbstractInstance> getInstanceListForTest() throws Exception {
    CloudCreateRequest request =
        CloudCreateRequest.createGCloudRequest(
            region,
            instanceName + new ObjectId().toHexString(),
            machineTypeLow,
            imageI,
            imageP,
            1,
            aws_keyPairName,
            zone);
    return cloudManagerService.createInstances(request);
  }

  private List<? extends AbstractInstance> getInstanceListForTestAWS() throws Exception {
    CloudCreateRequest request =
        CloudCreateRequest.createAwsRequest(
            aws_region,
            aws_machine,
            aws_image,
            aws_securityGroup,
            1,
            1,
            aws_keyPairName,
            aws_zone,
            null);
    return cloudManagerService.createInstances(request);
  }
}
