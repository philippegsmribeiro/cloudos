package cloudos.deploy;

import cloudos.Providers;
import cloudos.deploy.deployers.Deployers;
import cloudos.deploy.deployments.Deployment;
import cloudos.deploy.deployments.DeploymentStatus;
import cloudos.models.AbstractInstance;
import cloudos.models.CloudCreateRequest;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.Future;
import javax.annotation.Resource;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import test.CloudOSTest;

// ignore because of ansible
@Ignore
public class DeploymentManagerServiceTest extends CloudOSTest {

  private final String gcloud_zone = "us-west1-a";
  private final String gcloud_region = "us-west1";
  private final String gcloud_machine = "f1-micro";
  private final String gcloud_imageProject = "ubuntu-os-cloud";
  private final String gcloud_imageImage = "ubuntu-1604-xenial-v20161221";
  private final String gcloud_keyPairName = "gcloudsshkey";
  private final String instanceName = "test" + generator.generate(5);

  private final String aws_securityGroup = "test";
  private final String aws_keyPairName = "gcloudsshkey";
  private final String aws_region = "us-east-1";
  private final String aws_zone = "us-east-1a";
  private final String aws_machine = "m1.small";
  private final String aws_image = "ami-1acdd90d";

  @Resource
  DeploymentManagerService deploymentManagerService;

  /**
   * Test a normal deploy GOOGLE
   *
   * @throws Exception
   */
  @Test
  public void testDeploy() throws Exception {
    // create the deploy
    Deployment deployment = new Deployment();
    Path path = new ClassPathResource("deployments\\simple.yml").getFile().toPath();
    final byte[] config = Files.readAllBytes(path);
    deployment.setConfiguration(new String(config));
    deployment.setInstancesNumber(1);
    deployment.setDeployer(Deployers.ANSIBLE);
    deployment.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
    deployment.setCloudCreateRequest(
        CloudCreateRequest.createGCloudRequest(gcloud_region, instanceName, gcloud_machine,
            gcloud_imageImage, gcloud_imageProject, 1, gcloud_keyPairName, gcloud_zone));
    //
    Deployment createDeployment = deploymentManagerService.createDeployment(deployment, false);
    Assert.assertNotNull(createDeployment);

    // start the deploy
    Future<Deployment> startDeploying =
        deploymentManagerService.startDeploying(createDeployment.getId());
    Deployment startedDeployment = startDeploying.get();
    Assert.assertNotNull(startedDeployment);

    //
    Assert.assertEquals(startedDeployment.getStatus(), DeploymentStatus.FINISHED);

    // assert beats are installed
    for (AbstractInstance instance : startedDeployment.getInstances()) {
      Assert.assertTrue(instance.getInstance().getClientInstalled());
    }
  }

  /**
   * Test a normal deploy AWS
   *
   * @throws Exception
   */
  @Test
  public void testDeployAWS() throws Exception {
    // create the deploy
    Deployment deployment = new Deployment();
    final byte[] config =
        Files.readAllBytes(new File(this.getClass().getResource("test.txt").toURI()).toPath());
    deployment.setConfiguration(new String(config));
    deployment.setInstancesNumber(1);
    deployment.setDeployer(Deployers.ANSIBLE);
    deployment.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
    deployment.setCloudCreateRequest(CloudCreateRequest.createAwsRequest(aws_region, aws_machine,
        aws_image, aws_securityGroup, deployment.getInstancesNumber(),
        deployment.getInstancesNumber(), aws_keyPairName, aws_zone, instanceName));
    //
    Deployment createDeployment = deploymentManagerService.createDeployment(deployment, false);
    Assert.assertNotNull(createDeployment);

    // start the deploy
    Future<Deployment> startDeploying =
        deploymentManagerService.startDeploying(createDeployment.getId());
    Deployment startedDeployment = startDeploying.get();
    Assert.assertNotNull(startedDeployment);

    //
    Assert.assertEquals(DeploymentStatus.FINISHED, startedDeployment.getStatus());

    // assert beats are installed
    for (AbstractInstance instance : startedDeployment.getInstances()) {
      Assert.assertTrue(instance.getInstance().getClientInstalled());
    }
  }

  /**
   * Test a failing deploy and see if the instance was terminated GOOGLE
   *
   * @throws Exception
   */
  @Test
  public void testFailingDeploy() throws Exception {
    // create the deploy
    Deployment deployment = new Deployment();
    final byte[] config = Files
        .readAllBytes(new File(this.getClass().getResource("test_failing.txt").toURI()).toPath());
    deployment.setConfiguration(new String(config));
    deployment.setInstancesNumber(1);
    deployment.setDeployer(Deployers.ANSIBLE);
    deployment.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
    deployment.setCloudCreateRequest(
        CloudCreateRequest.createGCloudRequest(gcloud_region, instanceName + "fail", gcloud_machine,
            gcloud_imageImage, gcloud_imageProject, 1, gcloud_keyPairName, gcloud_zone));
    //
    Deployment createDeployment = deploymentManagerService.createDeployment(deployment, false);
    Assert.assertNotNull(createDeployment);

    // start the deploy
    Future<Deployment> startDeploying =
        deploymentManagerService.startDeploying(createDeployment.getId());
    Deployment startedDeployment = startDeploying.get();
    Assert.assertNotNull(startedDeployment);

    //
    Assert.assertEquals(DeploymentStatus.ERROR, startedDeployment.getStatus());

    // verify instance was terminated
    for (AbstractInstance instance : startedDeployment.getInstances()) {
      Assert.assertTrue(instance.getInstance().getDeleted());
    }
  }

  /**
   * Test a failing deploy and see if the instance was terminated AWS
   *
   * @throws Exception
   */
  @Test
  public void testFailingDeployAWS() throws Exception {
    // create the deploy
    Deployment deployment = new Deployment();
    final byte[] config = Files
        .readAllBytes(new File(this.getClass().getResource("test_failing.txt").toURI()).toPath());
    deployment.setConfiguration(new String(config));
    deployment.setInstancesNumber(1);
    deployment.setDeployer(Deployers.ANSIBLE);
    deployment.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
    deployment.setCloudCreateRequest(CloudCreateRequest.createAwsRequest(aws_region, aws_machine,
        aws_image, aws_securityGroup, deployment.getInstancesNumber(),
        deployment.getInstancesNumber(), aws_keyPairName, aws_zone, instanceName));
    //
    Deployment createDeployment = deploymentManagerService.createDeployment(deployment, false);
    Assert.assertNotNull(createDeployment);

    // start the deploy
    Future<Deployment> startDeploying =
        deploymentManagerService.startDeploying(createDeployment.getId());
    Deployment startedDeployment = startDeploying.get();
    Assert.assertNotNull(startedDeployment);

    //
    Assert.assertEquals(startedDeployment.getStatus(), DeploymentStatus.ERROR);

    // verify instance was terminated
    for (AbstractInstance instance : startedDeployment.getInstances()) {
      Assert.assertTrue(instance.getInstance().getDeleted());
    }
  }
}
