package cloudos;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import cloudos.billings.GoogleBillingReportRepository;
import cloudos.costanalysis.BillingCostReservation;
import cloudos.costanalysis.CostAnalysisTestUtils;
import cloudos.costanalysis.DailyResourceCostRepository;
import cloudos.costanalysis.GenericBillingReportRepository;
import cloudos.costanalysis.ProviderDailyCostEntryRepository;
import cloudos.costanalysis.ProviderTrendEntryRepository;
import cloudos.instances.InstanceService;
import cloudos.models.Instance;
import cloudos.models.Top10BillingAccountsRequest;
import cloudos.models.TopServiceCategoriesRequest;
import cloudos.models.costanalysis.BillingAccount;
import cloudos.models.costanalysis.BillingAccountCost;
import cloudos.models.costanalysis.BillingCost;
import cloudos.models.costanalysis.BillingCostProviderTotal;
import cloudos.models.costanalysis.BillingEstimatedCostMonth;
import cloudos.models.costanalysis.DailyResourceCost;
import cloudos.models.costanalysis.DailyResourceCostId;
import cloudos.models.costanalysis.GenericBillingReport;
import cloudos.models.costanalysis.InstanceCost;
import cloudos.models.costanalysis.InstanceTypeCost;
import cloudos.models.costanalysis.OfferingCostByMonths;
import cloudos.models.costanalysis.OfferingUtilizationReport;
import cloudos.models.costanalysis.ProviderDailyCostEntry;
import cloudos.models.costanalysis.ProviderServiceCost;
import cloudos.models.costanalysis.ProviderServiceCostComparisonEntry;
import cloudos.models.costanalysis.ProviderTrendEntry;
import cloudos.models.costanalysis.RetrieveInstanceCostRequest;
import cloudos.models.costanalysis.RetrieveProviderRegionalCostsRequest;
import cloudos.models.costanalysis.RetrieveProviderServiceCostRequest;
import cloudos.models.costanalysis.ServiceCategoryCost;
import cloudos.models.costanalysis.Top10ResourcesResponse;
import cloudos.utils.DateUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.querydsl.core.BooleanBuilder;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.time.DateUtils;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import test.CloudOSTest;
import test.OtherTest;

/**
 * Unit tests for {@link CostAnalysisController}.
 */
@Log4j2
@Category(OtherTest.class)
public class CostAnalysisControllerTest extends CloudOSTest {

  private final File mockedGoogleBillingReports;

  private MockMvc mvc;

  @Autowired
  private MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter;

  @MockBean(name = "genericBillingReportRepository")
  private GenericBillingReportRepository genericBillingReportRepository;

  @MockBean
  private ProviderTrendEntryRepository providerTrendEntryRepository;

  @MockBean(name = "instanceService")
  private InstanceService instanceService;

  @MockBean(name = "providerDailyCostEntryRepository")
  private ProviderDailyCostEntryRepository providerDailyCostEntryRepository;

  @Value(CostAnalysisTestUtils.COSTANALYSIS_GENERIC_BILLING_REPORTS_DATA_JSON)
  private Resource resource;

  @MockBean(name = "googleBillingReportRepository")
  private GoogleBillingReportRepository googleBillingReportRepository;

  @MockBean
  private DailyResourceCostRepository dailyResourceCostRepository;

  @Autowired
  private CostAnalysisController costAnalysisController;

  @Autowired
  private CostAnalysisTestUtils utils;

  @Autowired
  private WebApplicationContext wac;
  private MockMvc mockMvc;

  public CostAnalysisControllerTest() {
    mockedGoogleBillingReports = new File(getClass().getClassLoader()
        .getResource("cloudos/costanalysis/google-billing-reports-mock.json").getFile());
  }

  @Before
  public void setUp() throws Exception {
    this.mvc = MockMvcBuilders.standaloneSetup(costAnalysisController).build();
    DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
    this.mockMvc = builder.build();
  }

  @After
  public void deleteMockedData() throws Exception {
    utils.deleteMockedData();
  }

  @Test
  public void contextLoads() throws Exception {
    assertNotNull(costAnalysisController);
    assertNotNull(mockedGoogleBillingReports);
  }

  @Test
  public void retrieveTop10Accounts() throws Exception {

    String urlTemplate =
        CostAnalysisController.CONTEXT + CostAnalysisController.TOP_10_ACCOUNTS_ENDPOINT;

    Date endDate = new Date();
    Date startDate = DateUtils.addYears(endDate, -1);
    Top10BillingAccountsRequest request = new Top10BillingAccountsRequest(startDate, endDate);

    // mock data
    List<GenericBillingReport> mockedData = mapper.readValue(resource.getInputStream(),
        new TypeReference<List<GenericBillingReport>>() {});

    when(genericBillingReportRepository.findByUsageStartTimeBetween(any(), any()))
        .thenReturn(mockedData);

    MvcResult mvcResult =
        this.mvc
            .perform(post(urlTemplate).contentType(MediaType.APPLICATION_JSON)
                .content(json(request)).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk()).andReturn();

    assertNotNull(mvcResult);

    List<BillingAccountCost> responseData =
        mapper.readValue(mvcResult.getResponse().getContentAsString(),
            new TypeReference<List<BillingAccountCost>>() {});

    // response JSON payload
    log.debug(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseData));

    assertTrue(responseData.size() == 1);
  }

  @Test
  public void retrieveTop5ServiceCategories() throws Exception {

    String urlTemplate =
        CostAnalysisController.CONTEXT + CostAnalysisController.TOP_5_SERVICE_CATEGORIES_ENDPOINT;

    Date endDate = new Date();
    Date startDate = DateUtils.addYears(endDate, -1);
    TopServiceCategoriesRequest request = new TopServiceCategoriesRequest(startDate, endDate);

    List<GenericBillingReport> mockedData = mapper.readValue(resource.getInputStream(),
        new TypeReference<List<GenericBillingReport>>() {});

    when(genericBillingReportRepository.findByUsageStartTimeBetween(any(), any()))
        .thenReturn(mockedData);

    MvcResult mvcResult =
        this.mvc
            .perform(post(urlTemplate).contentType(MediaType.APPLICATION_JSON)
                .content(json(request)).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk()).andReturn();

    List<ServiceCategoryCost> responseData =
        mapper.readValue(mvcResult.getResponse().getContentAsString(),
            new TypeReference<List<ServiceCategoryCost>>() {});

    // response JSON payload
    log.debug(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseData));

    assertTrue(responseData.size() == 1);
  }

  @Test
  public void retrieveTop10InstanceTypesByCost() throws Exception {
    String urlTemplate =
        CostAnalysisController.CONTEXT + CostAnalysisController.TOP_INSTANCES_ENDPOINT;

    MvcResult mvcResult = this.mvc.perform(get(urlTemplate).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();

    List<InstanceTypeCost> responseData =
        mapper.readValue(mvcResult.getResponse().getContentAsString(),
            new TypeReference<List<InstanceTypeCost>>() {});

    // response JSON payload
    log.debug(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseData));
  }

  @Test
  public void retrieveEstimatedCostProviders() throws Exception {
    String urlTemplate =
        CostAnalysisController.CONTEXT + CostAnalysisController.ESTIMATED_COST_PROVIDERS_ENDPOINT;

    MvcResult mvcResult = this.mvc.perform(get(urlTemplate).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();

    BillingEstimatedCostMonth responseData =
        mapper.readValue(mvcResult.getResponse().getContentAsString(),
            new TypeReference<BillingEstimatedCostMonth>() {});

    // response JSON payload
    log.debug(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseData));
  }

  @Test
  public void retrieveCostByProviders() throws Exception {
    String urlTemplate =
        CostAnalysisController.CONTEXT + CostAnalysisController.COST_BY_PROVIDERS_ENDPOINT;
    LocalDate to = LocalDate.now();
    LocalDate from = to.minusMonths(1);
    BooleanBuilder builder = Mockito.any();
    // mock data
    List<GenericBillingReport> mockReports =
        utils.createRetrieveProviderRegionCostData(DateUtil.toDate(from));
    when(genericBillingReportRepository.findAll(builder)).thenReturn(mockReports);
    MvcResult mvcResult = this.mvc
        .perform(get(urlTemplate).param("provider", Providers.AMAZON_AWS.name())
            .param("start", from.format(DateTimeFormatter.ISO_DATE))
            .param("end", to.format(DateTimeFormatter.ISO_DATE)))
        .andExpect(status().isOk()).andReturn();

    List<BillingCostProviderTotal> responseData =
        mapper.readValue(mvcResult.getResponse().getContentAsString(),
            new TypeReference<List<BillingCostProviderTotal>>() {});

    // response JSON payload
    log.debug(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseData));
  }


  @Test
  public void retrieveCostByProviderAndReservation() throws Exception {
    String urlTemplate =
        CostAnalysisController.CONTEXT + CostAnalysisController.COST_PROVIDERS_RESERVATION_ENDPOINT;

    MvcResult mvcResult =
        this.mvc.perform(get(urlTemplate).param("provider", Providers.AMAZON_AWS.toString())
            .param("reservation", "OnDemand")).andExpect(status().isOk()).andReturn();

    BillingCostReservation responseData =
        mapper.readValue(mvcResult.getResponse().getContentAsString(),
            new TypeReference<BillingCostReservation>() {});

    // response JSON payload
    log.debug(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseData));
  }

  @Test
  public void retrieveCostByProviderAndReservationException() throws Exception {
    String urlTemplate =
        CostAnalysisController.CONTEXT + CostAnalysisController.COST_PROVIDERS_RESERVATION_ENDPOINT;

    this.mvc.perform(get(urlTemplate).param("provider", Providers.AMAZON_AWS.toString())
        .param("reservation", "Demand")).andExpect(status().isBadRequest()).andReturn();

  }

  @Test
  @Ignore
  public void retrieveStorageCosts() throws Exception {

    // PREPARE MOCKED DATA
    List<GenericBillingReport> mockedData = mapper.readValue(resource.getInputStream(),
        new TypeReference<List<GenericBillingReport>>() {});

    when(genericBillingReportRepository.findByProductCodeInAndUsageStartTimeIsBetween(any(), any(),
        any())).thenReturn(mockedData);

    // ACTUAL TEST
    String urlTemplate =
        CostAnalysisController.CONTEXT + CostAnalysisController.STORAGE_COSTS_ENDPOINT;

    MvcResult mvcResult = this.mvc.perform(get(urlTemplate).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();

    ProviderServiceCostComparisonEntry responseData =
        mapper.readValue(mvcResult.getResponse().getContentAsString(),
            new TypeReference<ProviderServiceCostComparisonEntry>() {});

    // ASSERTS
    utils.validateProviderServiceCostComparisonEntry(responseData);
  }

  @Test
  @Ignore
  public void retrieveNetworkingCosts() throws Exception {

    // PREPARE MOCKED DATA
    List<GenericBillingReport> mockedData = mapper.readValue(resource.getInputStream(),
        new TypeReference<List<GenericBillingReport>>() {});

    when(genericBillingReportRepository.findByProductCodeInAndUsageStartTimeIsBetween(any(), any(),
        any())).thenReturn(mockedData);

    // ACTUAL TEST
    String urlTemplate =
        CostAnalysisController.CONTEXT + CostAnalysisController.NETWORKING_COSTS_ENDPOINT;

    MvcResult mvcResult = this.mvc.perform(get(urlTemplate).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();

    ProviderServiceCostComparisonEntry responseData =
        mapper.readValue(mvcResult.getResponse().getContentAsString(),
            new TypeReference<ProviderServiceCostComparisonEntry>() {});

    // ASSERTS
    utils.validateProviderServiceCostComparisonEntry(responseData);
  }

  @Test
  public void retrieveBillings() throws Exception {

    // PREPARE MOCKED DATA
    List<GenericBillingReport> mockedData = mapper.readValue(resource.getInputStream(),
        new TypeReference<List<GenericBillingReport>>() {});

    when(genericBillingReportRepository.findByFilter(any())).thenReturn(mockedData);

    // ACTUAL TEST
    String urlTemplate =
        CostAnalysisController.CONTEXT + CostAnalysisController.BILLING_REPORTS_ENDPOINT;

    MvcResult mvcResult = this.mvc.perform(get(urlTemplate).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();

    List<GenericBillingReport> responseData =
        mapper.readValue(mvcResult.getResponse().getContentAsString(),
            new TypeReference<List<GenericBillingReport>>() {});

    // ASSERTS
    Assert.assertThat(responseData, Matchers.not(empty()));
    Assert.assertThat(responseData, Matchers.hasSize(mockedData.size()));
  }

  @Override
  protected String json(Object o) throws IOException {
    MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
    this.mappingJackson2HttpMessageConverter.write(o, MediaType.APPLICATION_JSON,
        mockHttpOutputMessage);
    return mockHttpOutputMessage.getBodyAsString();
  }

  @Test
  public void retrieveBillingReportById() throws Exception {

    // PREPARE MOCKED DATA
    GenericBillingReport mock = new GenericBillingReport();
    String id = "mock";
    mock.setId(id);

    when(genericBillingReportRepository.findOne(id)).thenReturn(mock);

    // ACTUAL TEST
    String urlTemplate =
        CostAnalysisController.CONTEXT + CostAnalysisController.BILLING_REPORTS_BY_ID_ENDPOINT;

    MvcResult mvcResult = this.mvc.perform(get(urlTemplate, id).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();

    GenericBillingReport responseData = mapper.readValue(
        mvcResult.getResponse().getContentAsString(), new TypeReference<GenericBillingReport>() {});

    // ASSERTS
    assertEquals(mock, responseData);
  }

  @Test
  public void retrieveBillingAccountsByProvider() throws Exception {

    // ACTUAL TEST
    String urlTemplate =
        CostAnalysisController.CONTEXT + CostAnalysisController.BILLING_ACCOUNTS_ENDPOINT;

    // parameters
    Providers provider = Providers.AMAZON_AWS;

    // MOCK DATA
    utils.createBillingAccount(Providers.AMAZON_AWS);

    MvcResult mvcResult = this.mvc.perform(
        get(urlTemplate).param("provider", provider.toString()).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();

    List<BillingAccount> responseData = mapper.readValue(
        mvcResult.getResponse().getContentAsString(), new TypeReference<List<BillingAccount>>() {});

    assertThat(responseData, not(empty()));
  }

  @Test
  public void retrieveBillingAccountsById() throws Exception {

    // ACTUAL TEST
    String urlTemplate =
        CostAnalysisController.CONTEXT + CostAnalysisController.BILLING_ACCOUNTS_BY_ID_ENDPOINT;

    // MOCK DATA
    Providers provider = Providers.MICROSOFT_AZURE;
    BillingAccount account = utils.createBillingAccount(provider);
    final String accountId = account.getId();
    MvcResult mvcResult =
        this.mvc.perform(get(urlTemplate, accountId).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk()).andReturn();

    BillingAccount responseData =
        mapper.readValue(mvcResult.getResponse().getContentAsString(), BillingAccount.class);

    assertEquals(account, responseData);
  }

  @Test
  public void retrieveInstanceCost() throws Exception {
    this.mockInstanceBillings();
    // ACTUAL TEST
    String urlTemplate = CostAnalysisController.CONTEXT + CostAnalysisController.INSTANCE_COST;

    RetrieveInstanceCostRequest request = new RetrieveInstanceCostRequest();
    request.setProvider(Providers.AMAZON_AWS);
    request.setMachineType("f1-micro");
    request.setPage(0);
    request.setPageSize(10);
    LocalDate now = LocalDate.now();
    request.setStartDate(DateUtil.toDate(now.minusDays(1)));
    request.setEndDate(DateUtil.toDate(now.plusDays(1)));

    MvcResult mvcResult =
        this.mvc
            .perform(post(urlTemplate).contentType(MediaType.APPLICATION_JSON)
                .content(json(request)).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk()).andReturn();

    List<InstanceCost> responseData = mapper.readValue(mvcResult.getResponse().getContentAsString(),
        new TypeReference<List<InstanceCost>>() {});

    assertNotNull(responseData);
  }


  @Test
  public void retrieveInstanceCostCsv() throws Exception {

    this.mockInstanceBillings();

    RetrieveInstanceCostRequest request = new RetrieveInstanceCostRequest();
    request.setProvider(Providers.AMAZON_AWS);
    request.setMachineType("f1-micro");
    request.setPage(0);
    request.setPageSize(10);
    LocalDate now = LocalDate.now();
    request.setStartDate(DateUtil.toDate(now.minusDays(1)));
    request.setEndDate(DateUtil.toDate(now.plusDays(1)));
    String urlTemplate = CostAnalysisController.CONTEXT + CostAnalysisController.INSTANCE_COST_CSV;
    MockHttpServletRequestBuilder builder =
        MockMvcRequestBuilders.post(urlTemplate).contentType(MediaType.APPLICATION_JSON)
            .content(json(request)).accept(new MediaType("text", "csv"));
    ResultActions resultActions = this.mockMvc.perform(builder);
    resultActions.andExpect(MockMvcResultMatchers.status().isOk())
        .andDo(MockMvcResultHandlers.print());
  }

  private void mockInstanceBillings() {
    LocalDate now = LocalDate.now();
    Providers provider = Providers.AMAZON_AWS;
    String resourceId = "junit-resource-id";
    String machineType = "t2.nano";

    Instance instance = new Instance();
    instance.setProvider(provider);
    instance.setMachineType(machineType);
    instance.setProviderId(resourceId);
    instance.setIncludedDate(DateUtil.toDate(now));

    List<Instance> instances = new ArrayList<>(Arrays.asList(instance));
    Page<Instance> pages = new PageImpl<Instance>(instances);

    Mockito.when(instanceService.findInstancesByFilter(Mockito.any(), Mockito.any()))
        .thenReturn(pages);
    LocalDate startDate = now.minusDays(1);
    LocalDate endDate = LocalDate.now().plusDays(1);
    Date startTime = DateUtil.toDate(startDate);
    Date endTime = DateUtil.toDate(endDate);
    GenericBillingReport genericBillingReport =
        new GenericBillingReport("junit-id", "junit-lineItemId", provider, "1", "junit-produceCode",
            resourceId, startTime, endTime, BigDecimal.ONE, "junit-location",false);

    List<GenericBillingReport> genericBillingReports = Arrays.asList(genericBillingReport);

    BooleanBuilder builder = Mockito.any();
    Mockito.when(genericBillingReportRepository.findAll(builder)).thenReturn(genericBillingReports);
  }


  @Test
  public void retrieveProviderRegionalCosts() throws Exception {
    // mock data
    List<GenericBillingReport> mockReports =
        utils.createRetrieveProviderRegionCostData(DateUtil.getCalendarForNow().getTime());
    BooleanBuilder builder = Mockito.any();
    when(genericBillingReportRepository.findAll(builder)).thenReturn(mockReports);
    // ACTUAL TEST
    String urlTemplate =
        CostAnalysisController.CONTEXT + CostAnalysisController.PROVIDER_REGIONAL_COST;

    RetrieveProviderRegionalCostsRequest request = new RetrieveProviderRegionalCostsRequest();
    request.setEndDate(LocalDate.now());
    request.setStartDate(request.getEndDate().minusDays(30));
    
    MvcResult mvcResult =
        this.mvc
            .perform(post(urlTemplate).contentType(MediaType.APPLICATION_JSON)
                .content(json(request)).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk()).andReturn();

    List<InstanceCost> responseData = mapper.readValue(mvcResult.getResponse().getContentAsString(),
        new TypeReference<List<InstanceCost>>() {});

    assertNotNull(responseData);
  }

  @Test
  public void retrieveLatestProviderTrendEntry() throws Exception {

    // MOCK DATA
    ProviderTrendEntry value = new ProviderTrendEntry();
    when(providerTrendEntryRepository.findFirstByOrderByUpdatedDesc()).thenReturn(value);

    // ACTUAL TEST
    String urlTemplate = CostAnalysisController.CONTEXT + CostAnalysisController.GET_PROVIDER_TREND;

    MvcResult mvcResult = this.mvc.perform(get(urlTemplate).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();

    ProviderTrendEntry responseData =
        mapper.readValue(mvcResult.getResponse().getContentAsString(), ProviderTrendEntry.class);

    assertEquals(value, responseData);
  }

  @Test
  public void retrieveOfferingComparison() throws Exception {

    LocalDate to = LocalDate.now();
    LocalDate from = to.minusMonths(1);

    // mock data
    List<GenericBillingReport> mockReports =
        utils.createRetrieveProviderRegionCostData(DateUtil.toDate(from));
    List<Instance> instances = utils.mockInstances(mockReports);
    when(genericBillingReportRepository.findByUsageStartTimeBetween(DateUtil.toDate(from),
        DateUtil.toDate(to))).thenReturn(mockReports);
    when(instanceService.findByCreationDateAfter(DateUtil.toDate(from))).thenReturn(instances);

    // ACTUAL TEST
    String urlTemplate =
        CostAnalysisController.CONTEXT + CostAnalysisController.OFFERING_COMPARISON;

    MvcResult mvcResult = this.mvc.perform(get(urlTemplate).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();

    OfferingCostByMonths responseData = mapper.readValue(
        mvcResult.getResponse().getContentAsString(), new TypeReference<OfferingCostByMonths>() {});

    assertNotNull(responseData);
  }

  @Test
  public void retrieveOfferingUtilization() throws Exception {

    LocalDate to = LocalDate.now();
    LocalDate from = to.minusMonths(1);

    // mock data
    List<GenericBillingReport> mockReports =
        utils.createRetrieveProviderRegionCostData(DateUtil.toDate(from));
    List<Instance> instances = utils.mockInstances(mockReports);
    when(genericBillingReportRepository.findByUsageStartTimeBetween(DateUtil.toDate(from),
        DateUtil.toDate(to))).thenReturn(mockReports);
    when(instanceService.findByCreationDateAfter(DateUtil.toDate(from))).thenReturn(instances);

    // ACTUAL TEST
    String urlTemplate =
        CostAnalysisController.CONTEXT + CostAnalysisController.OFFERING_UTILIZATION;

    MvcResult mvcResult = this.mvc
        .perform(get(urlTemplate).param("from", from.format(DateTimeFormatter.ISO_DATE))
            .param("to", to.format(DateTimeFormatter.ISO_DATE)).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();

    List<OfferingUtilizationReport> responseData =
        mapper.readValue(mvcResult.getResponse().getContentAsString(),
            new TypeReference<List<OfferingUtilizationReport>>() {});

    assertThat(responseData.size(), greaterThanOrEqualTo(0));
  }


  @Test
  public void retrieveProviderDailyCost() throws Exception {

    LocalDate to = LocalDate.now();
    LocalDate from = to.minusMonths(1);

    // MOCK DATA
    List<ProviderDailyCostEntry> resultList = Arrays.asList(
        new ProviderDailyCostEntry(null, Providers.AMAZON_AWS, from, BigDecimal.ONE, new Date()));
    when(providerDailyCostEntryRepository.findByDateBetween(from, to)).thenReturn(resultList);

    // ACTUAL TEST
    String urlTemplate =
        CostAnalysisController.CONTEXT + CostAnalysisController.PROVIDER_DAILY_COST;

    MvcResult mvcResult = this.mvc
        .perform(get(urlTemplate).param("start", from.format(DateTimeFormatter.ISO_DATE))
            .param("end", to.format(DateTimeFormatter.ISO_DATE)).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();

    List<ProviderDailyCostEntry> responseData =
        mapper.readValue(mvcResult.getResponse().getContentAsString(),
            new TypeReference<List<ProviderDailyCostEntry>>() {});

    assertThat(responseData, Matchers.allOf(Matchers.notNullValue(), not(empty())));
  }

  @Test
  public void retrieveProviderDailyCostByProvider() throws Exception {

    LocalDate to = LocalDate.now();
    LocalDate from = to.minusMonths(1);

    // MOCK DATA
    List<ProviderDailyCostEntry> resultList = Arrays.asList(
        new ProviderDailyCostEntry(null, Providers.AMAZON_AWS, from, BigDecimal.ONE, new Date()));
    when(providerDailyCostEntryRepository.findByProviderAndDateBetween(Providers.AMAZON_AWS, from,
        to)).thenReturn(resultList);

    // ACTUAL TEST
    String urlTemplate =
        CostAnalysisController.CONTEXT + CostAnalysisController.PROVIDER_DAILY_COST;

    MvcResult mvcResult = this.mvc
        .perform(get(urlTemplate).param("provider", Providers.AMAZON_AWS.name())
            .param("start", from.format(DateTimeFormatter.ISO_DATE))
            .param("end", to.format(DateTimeFormatter.ISO_DATE)).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();

    List<ProviderDailyCostEntry> responseData =
        mapper.readValue(mvcResult.getResponse().getContentAsString(),
            new TypeReference<List<ProviderDailyCostEntry>>() {});

    assertThat(responseData, Matchers.allOf(Matchers.notNullValue(), not(empty())));
  }

  @Test
  public void retrieveTop10ResourceCost() throws Exception {

    LocalDate end = LocalDate.now();
    LocalDate start = end.minusMonths(1);

    List<DailyResourceCost> mockedResourceCost = mockRetrieveTop10ResourceCost(start);
    when(dailyResourceCostRepository.findByDateBetween(start, end)).thenReturn(mockedResourceCost);

    // ACTUAL TEST
    String urlTemplate =
        CostAnalysisController.CONTEXT + CostAnalysisController.TOP_10_RESOURCE_COST;

    MvcResult mvcResult =
        this.mvc.perform(get(urlTemplate).param("start", start.format(DateTimeFormatter.ISO_DATE))
            .param("end", end.format(DateTimeFormatter.ISO_DATE))
            .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();

    Top10ResourcesResponse responseData = mapper
        .readValue(mvcResult.getResponse().getContentAsString(), Top10ResourcesResponse.class);

    assertNotNull(responseData);
    assertThat(responseData.getResources(),
        allOf(Matchers.notNullValue(), Matchers.not(Matchers.empty())));
  }

  private List<DailyResourceCost> mockRetrieveTop10ResourceCost(LocalDate start) {
    DailyResourceCostId id1 = new DailyResourceCostId(start, Providers.AMAZON_AWS, "resource1");
    DailyResourceCost r1 = new DailyResourceCost(id1, new BillingCost(BigDecimal.ONE), new Date());

    DailyResourceCostId id2 =
        new DailyResourceCostId(start, Providers.GOOGLE_COMPUTE_ENGINE, "resource1");
    DailyResourceCost r2 = new DailyResourceCost(id2, new BillingCost(BigDecimal.ONE), new Date());
    return Arrays.asList(r1, r2);
  }

  @Test
  public void retriveCostProviderAndService() throws Exception {

    String urlTemplate =
        CostAnalysisController.CONTEXT + CostAnalysisController.COST_PROVIDERS_SERVICE_ENDPOINT;

    RetrieveProviderServiceCostRequest request = new RetrieveProviderServiceCostRequest();
    request.setStartDate(DateUtil.toDate(LocalDate.now().minusDays(90)));
    request.setEndDate(DateUtil.toDate(LocalDate.now()));

    MvcResult mvcResult =
        this.mvc
            .perform(post(urlTemplate).contentType(MediaType.APPLICATION_JSON)
                .content(json(request)).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk()).andReturn();

    List<ProviderServiceCost> responseData =
        mapper.readValue(mvcResult.getResponse().getContentAsString(),
            new TypeReference<List<ProviderServiceCost>>() {});

    assertNotNull(responseData);
  }
}
