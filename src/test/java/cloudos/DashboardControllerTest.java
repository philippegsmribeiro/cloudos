package cloudos;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.type.TypeReference;

import cloudos.dashboard.Event;
import cloudos.dashboard.Provider;
import cloudos.utils.DateUtil;
import test.CloudOSTest;

public class DashboardControllerTest extends CloudOSTest {

  private MockMvc mvc;

  @Autowired
  private DashboardController dashboardController;

  @Before
  public void setUp() throws Exception {
    this.mvc = MockMvcBuilders.standaloneSetup(dashboardController).build();
  }

  @Test
  public void testDashboardController() throws Exception {
    this.mvc.perform(get(DashboardController.DASHBOARD)).andExpect(status().is4xxClientError());
  }

  @Test
  public void testGetRegions() throws Exception {
    this.mvc.perform(get(DashboardController.DASHBOARD + "/list/regions"))
        .andExpect(status().isOk()).andExpect(content().contentType(contentType));
  }

  @Test
  @Ignore
  public void testGetCost() throws Exception {
    this.mvc.perform(get(DashboardController.DASHBOARD + "/list/cost")).andExpect(status().isOk())
        .andExpect(content().contentType(contentType))
        .andExpect(jsonPath("$.totalDailyCost.month", is(DateUtil.getDay())))
        .andExpect(jsonPath("$.totalDailyCost.cost", greaterThanOrEqualTo(0.0)))
        .andExpect(jsonPath("$.totalMonthyCost.month", is(DateUtil.getMonth())))
        .andExpect(jsonPath("$.totalMonthyCost.cost", greaterThanOrEqualTo(0.0)))
        .andExpect(jsonPath("$.totalWeeklyCost.month", is(DateUtil.getWeek())))
        .andExpect(jsonPath("$.totalWeeklyCost.cost", greaterThanOrEqualTo(0.0)));
  }

  @Test
  public void testGetCostEstimated() throws Exception {
    this.mvc.perform(get(DashboardController.DASHBOARD + "/list/cost_estimated"))
        .andExpect(status().isOk()).andExpect(content().contentType(contentType))
        .andExpect(jsonPath("$.weeklyPercentage", greaterThanOrEqualTo(0.0)))
        .andExpect(jsonPath("$.monthyPercentage", greaterThanOrEqualTo(0.0)))
        .andExpect(jsonPath("$.monthlyCost", greaterThanOrEqualTo(0.0)))
        .andExpect(jsonPath("$.weeklyCost", greaterThanOrEqualTo(0.0)))
        .andExpect(jsonPath("$.yearlyCost", greaterThanOrEqualTo(0.0)))
        .andExpect(jsonPath("$.weeklyEstimatedCost", greaterThanOrEqualTo(0.0)))
        .andExpect(jsonPath("$.monthlyEstimatedCost", greaterThanOrEqualTo(0.0)))
        .andExpect(jsonPath("$.yearlyEstimatedCost", greaterThanOrEqualTo(0.0)))
        .andExpect(jsonPath("$.weeklyEstimatedPercentage", greaterThanOrEqualTo(0.0)))
        .andExpect(jsonPath("$.monthlyEstimatedPercentage", greaterThanOrEqualTo(0.0)));
  }

  @Test
  @Ignore
  public void testGetResources() throws Exception {
    this.mvc.perform(get(DashboardController.DASHBOARD + "/list/resources"))
        .andExpect(status().isOk()).andExpect(content().contentType(contentType))
        .andExpect(jsonPath("$.month", is(DateUtil.getMonth())))
        .andExpect(jsonPath("$.total", greaterThanOrEqualTo(0.0)));
  }

  @Test
  @Ignore
  public void testGetInstances() throws Exception {
    this.mvc.perform(get(DashboardController.DASHBOARD + "/list/instances"))
        .andExpect(status().isOk());
  }

  @Test
  public void testGetProviders() throws Exception {
    String json = this.mvc.perform(get(DashboardController.DASHBOARD + "/list/providers"))
        .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
    List<Provider> result =
        super.mapper.readValue(json, new TypeReference<ArrayList<Provider>>() {});
    if (CollectionUtils.isNotEmpty(result)) {
      for (Provider provider : result) {
        Arrays.asList(Providers.GOOGLE_COMPUTE_ENGINE.toString(), Providers.AMAZON_AWS.toString(),
            Providers.MICROSOFT_AZURE.toString()).contains(provider.getProviders());
      }
    }
  }

  @Test
  public void testGetEvents() throws Exception {
    Event event = new Event("[14:00] VM1 CPU increased by 10%");
    this.mvc.perform(get(DashboardController.DASHBOARD + "/list/events")).andExpect(status().isOk())
        .andExpect(content().contentType(contentType))
        .andExpect(jsonPath("$[0].content", is(event.getContent())));
  }

  @Test
  public void testGetCpuUsage() throws Exception {
    this.mvc.perform(get(DashboardController.DASHBOARD + "/list/cpu_usage"))
        .andExpect(status().isOk());
    this.mvc
        .perform(get(
            DashboardController.DASHBOARD + "/list/cpu_usage/" + Providers.GOOGLE_COMPUTE_ENGINE))
        .andExpect(status().isOk());
    this.mvc.perform(get(DashboardController.DASHBOARD + "/list/cpu_usage/" + Providers.AMAZON_AWS))
        .andExpect(status().isOk());
  }

  @Test
  public void testGetMemoryUsage() throws Exception {
    this.mvc.perform(get(DashboardController.DASHBOARD + "/list/memory_usage"))
        .andExpect(status().isOk());
    this.mvc.perform(get(
        DashboardController.DASHBOARD + "/list/memory_usage/" + Providers.GOOGLE_COMPUTE_ENGINE))
        .andExpect(status().isOk());
    this.mvc
        .perform(get(DashboardController.DASHBOARD + "/list/memory_usage/" + Providers.AMAZON_AWS))
        .andExpect(status().isOk());
  }

  @Test
  public void testGetDiskUsage() throws Exception {
    this.mvc.perform(get(DashboardController.DASHBOARD + "/list/disk_usage"))
        .andExpect(status().isOk());
    this.mvc
        .perform(get(
            DashboardController.DASHBOARD + "/list/disk_usage/" + Providers.GOOGLE_COMPUTE_ENGINE))
        .andExpect(status().isOk());
    this.mvc
        .perform(get(DashboardController.DASHBOARD + "/list/disk_usage/" + Providers.AMAZON_AWS))
        .andExpect(status().isOk());
  }

  @Test
  public void testGetNetworkUsage() throws Exception {
    this.mvc.perform(get(DashboardController.DASHBOARD + "/list/network_usage"))
        .andExpect(status().isOk());
    this.mvc.perform(get(
        DashboardController.DASHBOARD + "/list/network_usage/" + Providers.GOOGLE_COMPUTE_ENGINE))
        .andExpect(status().isOk());
    this.mvc
        .perform(get(DashboardController.DASHBOARD + "/list/network_usage/" + Providers.AMAZON_AWS))
        .andExpect(status().isOk());
  }

  @Test
  public void getMemoryUsageChart() throws Exception {
    this.mvc
        .perform(get(DashboardController.DASHBOARD + "/chart/memory_usage/" + Providers.AMAZON_AWS))
        .andExpect(status().isOk());
    this.mvc.perform(get(
        DashboardController.DASHBOARD + "/chart/memory_usage/" + Providers.GOOGLE_COMPUTE_ENGINE))
        .andExpect(status().isOk());
  }

}
