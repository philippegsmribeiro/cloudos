package cloudos.instances;

import cloudos.AmazonService;
import cloudos.Providers;
import cloudos.amazon.AmazonInstance;
import cloudos.models.ActionType;
import cloudos.models.CloudActionRequest;
import cloudos.models.CloudCreateRequest;
import cloudos.provider.AmazonProviderManagement;
import cloudos.provider.ProviderRegion;
import com.amazonaws.services.ec2.model.Instance;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import lombok.extern.log4j.Log4j2;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

@Log4j2
public class AmazonInstanceManagementTest extends AbstractInstanceManagementTest {

  private String region = "us-east-1";
  private String zone = "us-east-1a";
  private String machineType = "t2.micro";
  private String image = "ami-f4cc1de2";

  @Autowired
  private AmazonInstanceManagement amazonManagement;
  @Autowired
  private AmazonProviderManagement amazonResourcesManagement;
  @Autowired
  InstanceRepository instanceRepository;
  @Autowired
  AmazonService amazonService;

  @Test
  @Ignore
  public void terminate() throws InstanceRequestException {
    List<ProviderRegion> retrieveRegions = amazonResourcesManagement.retrieveRegionsWithZones();
    for (ProviderRegion region : retrieveRegions) {
      // fetch all
      List<AmazonInstance> fetchInstances = this.fetchInstances(
          amazonManagement,
          CloudActionRequest.createAwsRequest(
              null, region.getRegion(), region.getKey(), null, ActionType.FETCH));

      for (AmazonInstance amazonInstance : fetchInstances) {
        CloudActionRequest cloudActionRequest = CloudActionRequest.createAwsRequest(Arrays.asList(amazonInstance.getInstance().getProviderId()), amazonInstance.getInstance().getRegion(), amazonInstance.getInstance().getZone(), amazonInstance.getInstance().getSecurityGroup(), ActionType.TERMINATE);
        cloudActionRequest.setSynchronous(false);
        amazonManagement.terminate(cloudActionRequest);
      }
    }
  }

  public void fetchAll() throws InstanceRequestException {
    List<ProviderRegion> retrieveRegions = amazonResourcesManagement.retrieveRegionsWithZones();
    for (ProviderRegion region : retrieveRegions) {
      // fetch all
      this.fetchInstances(
          amazonManagement,
          CloudActionRequest.createAwsRequest(
              null, region.getRegion(), region.getKey(), null, ActionType.FETCH));
    }
  }

  @Test
  public void sync() {
    List<cloudos.provider.ProviderRegion> retrieveRegions =
        amazonResourcesManagement.retrieveRegions();
    for (ProviderRegion region : retrieveRegions) {
      // execute the sync
      amazonManagement.syncByRegion(region);
      // compare db and provider
      Set<Instance> listInstances = amazonService.listInstances(region.getRegion(), null);
      List<cloudos.models.Instance> dbInstances =
          instanceRepository.findByProviderAndDeletedAndRegion(
              Providers.AMAZON_AWS, false, region.getRegion());
      Assert.assertEquals(listInstances.size(), dbInstances.size());
    }
  }

  @Test
  @Ignore
  public void testOne() throws InstanceRequestException {
    super.fullTest(
        amazonManagement,
        CloudCreateRequest.createAwsRequest(
            region, machineType, image, "default", 1, 1, "gcloudsshkey", zone, null));
  }

  @Test
  @Ignore
  public void testMultiple() throws InstanceRequestException {
    super.fullTest(
        amazonManagement,
        CloudCreateRequest.createAwsRequest(
            region, machineType, image, "default", 3, 3, "gcloudsshkey", zone, null));
  }

  @Test
  @Ignore
  public void terminateAllInstances() {
    List<cloudos.provider.ProviderRegion> retrieveRegions =
        amazonResourcesManagement.retrieveRegions();
    for (ProviderRegion providerRegion : retrieveRegions) {
      if(providerRegion.getKey() != null) {
        amazonService.listInstances(providerRegion.getKey(), null).forEach(instance -> {
          log.info("Deleting instance: {}", instance.getInstanceId());
          amazonService.terminateInstances(Arrays.asList(instance.getInstanceId()), providerRegion.getKey(), null);
        });;
      }
    }
  }
}
