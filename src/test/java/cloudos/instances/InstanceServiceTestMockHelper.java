package cloudos.instances;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.text.RandomStringGenerator;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestComponent;

import cloudos.Providers;
import cloudos.models.Instance;
import cloudos.models.InstanceStatus;

@TestComponent
public class InstanceServiceTestMockHelper {

  @Autowired
  private InstanceRepository repository;

  private final List<Instance> mockInstances = new ArrayList<>();

  static protected RandomStringGenerator generator = new RandomStringGenerator.Builder()
      .withinRange('a', 'z').build();

  /**
   * Creates test data.
   */
  public void createTestData() {
    Instance instance = new Instance();
    instance.setDeleted(Boolean.FALSE);
    instance.setProviderId(generator.generate(10));
    instance.setProvider(Providers.values()[RandomUtils.nextInt(0, Providers.values().length)]);
    instance
        .setStatus(InstanceStatus.values()[RandomUtils.nextInt(0, InstanceStatus.values().length)]);
    instance.setCost(RandomUtils.nextDouble());
    instance.setId(generator.generate(10));
    Date now = new Date();
    instance.setIncludedDate(now);
    instance.setCreationDate(now);
    this.save(instance);
  }

  /**
   * Create test data.
   *
   * @param qty the amount of items the it should create
   */
  public void createTestData(int qty) {
    if (qty <= 0) {
      return;
    }
    for (int i = 0; i <= qty; i++) {
      createTestData();
    }
  }

  /**
   * Deletes all the test data.
   */
  public void deleteTestData() {
    repository.delete(mockInstances);
  }

  /**
   * Saves the instance and adds it to the collection to be deleted.
   *
   * @param instance the instance to be saved
   * @return the instance after saving it
   */
  private Instance save(Instance instance) {
    if (!repository.exists(instance.getId())) {
      Instance saved = repository.save(instance);
      Assert.assertNotNull(saved);
      mockInstances.add(saved);
      return saved;
    }
    return instance;
  }
}
