package cloudos.instances;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import cloudos.Providers;
import cloudos.instances.InstanceService;
import cloudos.models.Instance;
import cloudos.models.InstanceStatus;
import cloudos.models.cloudgroup.CloudGroupFilterRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import test.CloudOSTest;

public class InstanceServiceTest extends CloudOSTest {

  private static final String MACHINE_TYPE = "t2.nano";

  @Autowired
  private InstanceService instanceService;

  @Autowired
  private InstanceServiceTestMockHelper helper;

  @After
  public void deleteTestData() {
    helper.deleteTestData();
  }

  @Test
  public void testFindAllActive() {
    List<Instance> findAllActiveInstances = instanceService.findAllActiveInstances();
    // verify if all are active (not deleted)
    for (Instance instance : findAllActiveInstances) {
      assertFalse(instance.getDeleted());
    }
  }

  @Test
  public void testIndex() {
    Instance instance1 = new Instance();
    instance1.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
    instance1.setProviderId("1");
    instanceService.save(instance1);
    Instance instance3 = new Instance();
    instance3.setProvider(Providers.AMAZON_AWS);
    instance3.setProviderId("1");
    instanceService.save(instance3);

    instanceService.delete(instance1.getId());
    instanceService.delete(instance3.getId());
  }

  @Test(expected = DuplicateKeyException.class)
  public void testIndexDuplicate() {
    Instance instance1 = new Instance();
    instance1.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
    instance1.setProviderId("2");
    instanceService.save(instance1);
    Instance instance2 = new Instance();
    instance2.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
    instance2.setProviderId("2");
    instanceService.save(instance2);
  }

  @Test
  public void testFindAllByProvider() {
    List<Instance> findAllActiveInstances = instanceService.findAllActiveInstances();
    HashMap<Providers, ArrayList<Instance>> mapProviders =
        findAllActiveInstances.stream().collect(Collectors.groupingBy(Instance::getProvider,
            HashMap::new, Collectors.toCollection(ArrayList::new)));
    for (Entry<Providers, ArrayList<Instance>> entry : mapProviders.entrySet()) {
      List<Instance> findAllActiveInstancesByProvider =
          instanceService.findAllActiveInstancesByProvider(entry.getKey());
      Assert.assertEquals(entry.getValue().size(), findAllActiveInstancesByProvider.size());
    }
  }

  @Test
  public void testFindAllByProviderAndProviderId() {
    List<Instance> findAllActiveInstances = instanceService.findAllActiveInstances();
    for (Instance instance : findAllActiveInstances) {
      Instance findInstance = instanceService.findByProviderAndProviderId(instance.getProvider(),
          instance.getProviderId());
      Assert.assertEquals(instance.getId(), findInstance.getId());
    }
  }

  @Test
  public void testFindAllWithoutCloudGroup() {
    CloudGroupFilterRequest emptyRequest = CloudGroupFilterRequest.builder().build();
    // there should be no matching instances in this case
    assertNull(instanceService.findAllWithoutCloudGroup(emptyRequest));

    // find with one provider
    CloudGroupFilterRequest request = CloudGroupFilterRequest
        .builder()
        .provider(Providers.AMAZON_AWS)
        .build();

    List<Instance> instances = instanceService.findAllWithoutCloudGroup(request);
    // there may be no instances in the repository
    if (instances != null) {
      assertThat(instances.size(), greaterThanOrEqualTo(0));
    }

    request = CloudGroupFilterRequest
        .builder()
        .provider(Providers.AMAZON_AWS)
        .region("us-east-2")
        .availabilityZone("us-east-2a")
        .instanceType("t2.micro")
        .build();

    instances = instanceService.findAllWithoutCloudGroup(request);
    // there may be no instances in the repository
    if (instances != null) {
      assertThat(instances.size(), greaterThanOrEqualTo(0));
      if (instances.size() > 0) {
        boolean onlyValid = instances.stream()
            .anyMatch(instance -> InstanceStatus.STOPPED.equals(instance.getStatus()) ||
                InstanceStatus.RUNNING.equals(instance.getStatus()));
        assertTrue(onlyValid);
      }
    }
  }

  @Test
  public void findAllActiveAndNotTerminatedInstances() {

    // TEST DATA
    helper.createTestData(30);

    //ACTUAL TEST
    List<Instance> result = instanceService.findAllActiveAndNotTerminatedInstances();

    // ASSERTS
    assertThat(result, not(empty()));
    for (Instance instance : result) {
      assertFalse(InstanceStatus.TERMINATED.equals(instance.getStatus()));
    }
  }

  @Test
  public void findByProviderAndDateBetweenExpectsAtLeastOneResult() {

    Date now = new Date();

    Instance testData = new Instance();
    testData.setProvider(Providers.AMAZON_AWS);
    testData.setProviderId(RandomStringUtils.randomAlphanumeric(10));
    testData.setCreationDate(now);
    testData.setIncludedDate(now);
    testData.setDeletedDate(DateUtils.addDays(now, -2));
    testData.setMachineType(MACHINE_TYPE);
    instanceService.save(testData);

    List<Instance> instances = instanceService
        .findByProviderAndMachineTypeAndDateBetween(Providers.AMAZON_AWS, testData.getMachineType(),
            DateUtils.addDays(now, -1), DateUtils.addDays(now, 1));

    assertThat(instances, not(empty()));
  }
}
