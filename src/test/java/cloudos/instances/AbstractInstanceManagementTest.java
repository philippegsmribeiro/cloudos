package cloudos.instances;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import cloudos.instances.InstanceManagement;
import cloudos.instances.InstanceRequestException;
import cloudos.models.AbstractInstance;
import cloudos.models.ActionType;
import cloudos.models.CloudActionRequest;
import cloudos.models.CloudActionResponse;
import cloudos.models.CloudCreateRequest;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import test.CloudOSTest;

public abstract class AbstractInstanceManagementTest extends CloudOSTest {

  /**
   * Create instances
   *
   * @param provider
   * @param createRequest
   * @return
   * @throws InstanceRequestException
   */
  public <T extends AbstractInstance> List<T> createInstances(
      InstanceManagement<T> provider, CloudCreateRequest createRequest)
      throws InstanceRequestException {
    List<T> createAndSave = provider.createInstances(createRequest);
    assertNotNull(createAndSave);
    assertTrue(createAndSave.size() == createRequest.getCount());
    return createAndSave;
  }

  /**
   * Fetch instances
   *
   * @param provider
   * @param createRequest
   * @return
   * @throws InstanceRequestException
   */
  public <T extends AbstractInstance> List<T> fetchInstances(
      InstanceManagement<T> provider, CloudActionRequest request) throws InstanceRequestException {
    List<T> createAndSave = provider.fetch(request);
    assertNotNull(createAndSave);
    return createAndSave;
  }

  /**
   * Starts an instance
   *
   * @param provider
   * @param instance
   * @return
   * @throws InstanceRequestException
   */
  public <T extends AbstractInstance> List<T> startInstance(
      InstanceManagement<T> provider, CloudActionRequest instance) throws InstanceRequestException {
    List<CloudActionResponse<T>> action = provider.start(instance);
    logResponse(action);
    return action.stream().map(CloudActionResponse<T>::getInstance).collect(Collectors.toList());
  }

  /**
   * Stops an instance
   *
   * @param provider
   * @param instance
   * @return
   * @throws InstanceRequestException
   */
  public <T extends AbstractInstance> List<T> stopInstance(
      InstanceManagement<T> provider, CloudActionRequest instance) throws InstanceRequestException {
    List<CloudActionResponse<T>> action = provider.stop(instance);
    logResponse(action);
    return action.stream().map(CloudActionResponse<T>::getInstance).collect(Collectors.toList());
  }

  /**
   * Stops an instance
   *
   * @param provider
   * @param instance
   * @return
   * @throws InstanceRequestException
   */
  public <T extends AbstractInstance> List<T> rebootInstance(
      InstanceManagement<T> provider, CloudActionRequest instance) throws InstanceRequestException {
    List<CloudActionResponse<T>> action = provider.reboot(instance);
    logResponse(action);
    return action.stream().map(CloudActionResponse<T>::getInstance).collect(Collectors.toList());
  }

  /**
   * Terminate an instance
   *
   * @param provider
   * @param instance
   * @return
   * @throws InstanceRequestException
   */
  public <T extends AbstractInstance> List<T> terminateInstance(
      InstanceManagement<T> provider, CloudActionRequest instance) throws InstanceRequestException {
    List<CloudActionResponse<T>> action = provider.terminate(instance);
    logResponse(action);
    return action
        .stream()
        .map(CloudActionResponse<T>::getInstance)
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
  }

  /**
   * Execute the full test - creation - stop - start - reboot - list - terminate
   *
   * @param provider
   * @param createRequest
   * @throws InstanceRequestException
   */
  public <T extends AbstractInstance> void fullTest(
      InstanceManagement<T> provider, CloudCreateRequest createRequest)
      throws InstanceRequestException {
    // create instances
    logger.debug("creating");
    List<T> instances = this.createInstances(provider, createRequest);
    logInstances(instances);

    // stop
    logger.debug("stopping");
    instances =
        this.stopInstance(
            provider, createCloudActionRequest(createRequest, instances, ActionType.STOP));
    logInstances(instances);

    // start
    logger.debug("starting");
    instances =
        this.startInstance(
            provider, createCloudActionRequest(createRequest, instances, ActionType.START));
    logInstances(instances);

    // reboot
    logger.debug("rebooting");
    instances =
        this.rebootInstance(
            provider, createCloudActionRequest(createRequest, instances, ActionType.RESTART));
    logInstances(instances);

    // list
    logger.debug("fetching");
    List<T> fetchInstances =
        this.fetchInstances(
            provider, createCloudActionRequest(createRequest, instances, ActionType.FETCH));
    logInstances(fetchInstances);
    // match them
    assertArrayEquals(
        getInstanceNames(instances).toArray(new String[] {}),
        getInstanceNames(fetchInstances).toArray(new String[] {}));

    // terminate
    logger.debug("terminating");
    instances =
        this.terminateInstance(
            provider, createCloudActionRequest(createRequest, instances, ActionType.TERMINATE));
    if (instances != null)
      // log
      logInstances(instances);
  }

  private static <T extends AbstractInstance> CloudActionRequest createCloudActionRequest(
      CloudCreateRequest createRequest, List<T> instances, ActionType type) {
    return new CloudActionRequest(
        createRequest.getProvider(),
        getInstanceNames(instances),
        createRequest.getRegion(),
        createRequest.getZone(),
        createRequest.getSecurityGroup(),
        true,
        type);
  }

  private <T extends AbstractInstance> void logInstances(List<T> instances) {
    instances
        .stream()
        .filter(Objects::nonNull)
        .forEach(
            instance ->
                logger.debug(
                    instance.getInstance().getProviderId()
                        + " "
                        + /* instance.getInstance().getIpaddress() +*/ " "
                        + instance.getInstance().getStatus()));
  }

  private <T extends AbstractInstance> void logResponse(List<CloudActionResponse<T>> responses) {
    responses
        .stream()
        .filter(Objects::nonNull)
        .forEach(
            response ->
                logger.debug(
                    response.getName()
                        + " "
                        + response.getStatus()
                        + " "
                        + response.getInstance()));
  }

  private static <T extends AbstractInstance> List<String> getInstanceNames(List<T> instances) {
    return instances
        .stream()
        .filter(Objects::nonNull)
        .map(t -> t.getInstance().getProviderId())
        .sorted()
        .collect(Collectors.toList());
  }
}
