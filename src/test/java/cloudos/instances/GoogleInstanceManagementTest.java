package cloudos.instances;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import cloudos.Providers;
import cloudos.google.integration.GoogleComputeIntegration;
import cloudos.models.ActionType;
import cloudos.models.CloudActionRequest;
import cloudos.models.CloudCreateRequest;
import cloudos.models.InstanceStatus;
import cloudos.provider.GoogleProviderManagement;
import cloudos.provider.ProviderRegion;
import com.google.cloud.compute.Instance;
import com.google.cloud.compute.InstanceInfo.Status;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.IterableUtils;
import org.bson.types.ObjectId;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import test.SlowTest;

@Category(SlowTest.class)
@Log4j2
public class GoogleInstanceManagementTest extends AbstractInstanceManagementTest {

  private String zone = "us-west1-a";
  private String region = "us-west1";
  private String machineType = "f1-micro";
  private String imageProject = "centos-cloud";
  private String image = "centos-7-v20161027";
  private String name = "tm";

  @Autowired
  private GoogleInstanceManagement googleManagement;
  @Autowired
  private GoogleProviderManagement googleProviderManagement;
  @Autowired
  InstanceRepository instanceRepository;
  @Autowired
  GoogleComputeIntegration googleIntegration;

  @Test
  public void fetchAll() throws InstanceRequestException {
    List<ProviderRegion> retrieveRegions = googleProviderManagement.retrieveRegions();
    for (ProviderRegion region : retrieveRegions) {
      // fetch all
      this.fetchInstances(googleManagement, CloudActionRequest.createGCloudRequest(null,
          region.getRegion(), region.getKey(), ActionType.FETCH));
    }
  }

  @Test
  public void sync() {
    // execute the sync
    googleManagement.syncInstances();
    // compare db and provider
    List<cloudos.models.Instance> dbInstances =
        instanceRepository.findByProviderAndDeleted(Providers.GOOGLE_COMPUTE_ENGINE, false);
    // get the new instance list
    Iterable<Instance> listInstances = googleIntegration.listInstances();
    Assert.assertEquals(IterableUtils.size(listInstances), dbInstances.size());
  }

  @Test
  @Ignore
  public void testOne() throws InstanceRequestException {
    super.fullTest(googleManagement,
        CloudCreateRequest.createGCloudRequest(region, name + new ObjectId().toHexString(),
            machineType, image, imageProject, 1, "gcloudsshkey", zone));
  }

  @Test
  @Ignore
  public void testMultiple() throws InstanceRequestException {
    super.fullTest(googleManagement,
        CloudCreateRequest.createGCloudRequest(region, name + new ObjectId().toHexString(),
            machineType, image, imageProject, 3, "gcloudsshkey", zone));
  }

  @Test
  @Ignore
  public void terminateAllInstances() {
    googleIntegration.listInstances().forEach((instance) -> {
      log.info("Deleting instance: {}", instance.getInstanceId().getInstance());
      googleIntegration.deleteInstance(instance.getInstanceId().getZone(),
          instance.getInstanceId().getInstance());
    });
  }

  @Test
  public void testGetStatus() {
    Instance instance = Mockito.mock(Instance.class);
    // 
    when(instance.getStatus()).thenReturn(Status.RUNNING);
    assertTrue(googleManagement.getStatus(instance) == InstanceStatus.RUNNING);
    // 
    when(instance.getStatus()).thenReturn(Status.PROVISIONING);
    assertTrue(googleManagement.getStatus(instance) == InstanceStatus.PROVISIONING);
    // 
    when(instance.getStatus()).thenReturn(Status.STAGING);
    assertTrue(googleManagement.getStatus(instance) == InstanceStatus.PROVISIONING);
    // 
    when(instance.getStatus()).thenReturn(Status.STOPPING);
    assertTrue(googleManagement.getStatus(instance) == InstanceStatus.STOPPING);
    // 
    when(instance.getStatus()).thenReturn(Status.TERMINATED);
    assertTrue(googleManagement.getStatus(instance) == InstanceStatus.STOPPED);
  }
}
