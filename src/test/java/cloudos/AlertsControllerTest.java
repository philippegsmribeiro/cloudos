package cloudos;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cloudos.models.alerts.AlertRequest;
import cloudos.models.alerts.AlertRequestGeneric;
import cloudos.models.alerts.AlertResponse;
import cloudos.models.alerts.CategoryMetricResponse;
import cloudos.models.alerts.CloudosAlert;
import cloudos.models.alerts.CloudosDimension;
import cloudos.models.alerts.MetricMissingDataAlertResponse;
import cloudos.models.alerts.MetricPeriodAlertResponse;
import cloudos.models.alerts.MetricResponse;
import cloudos.models.alerts.MetricStatusAlarmResponse;
import lombok.extern.log4j.Log4j2;
import test.CloudOSTest;

/** Created by philipperibeiro on 3/30/17. */
@Log4j2
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AlertsControllerTest extends CloudOSTest {

  private MockMvc mvc;

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private AlertsController alertsController;

  @Autowired
  void setConverters(HttpMessageConverter<?>[] converters) {
    this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
        .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().orElse(null);
    assertNotNull("the JSON message converter must not be null",
        this.mappingJackson2HttpMessageConverter);
  }

  @Before
  public void setUp() throws Exception {
    this.mvc = MockMvcBuilders.standaloneSetup(alertsController).build();
  }

  @Test
  public void testADeleteAlert() throws Exception {
    MvcResult mvcResult = this.mvc
        .perform(delete(AlertsController.ALERTS + "/").contentType(MediaType.APPLICATION_JSON)
            .content(json(this.getAmazonRequestAlertGeneric())).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is2xxSuccessful()).andReturn();
    assertNotNull(mvcResult);

  }

  @Test
  public void testB1CreateAlert() throws Exception {
    AlertRequest alert = this.getAmazonRequestAlert("Alert Memory");
    alert.setStatistic("Other Statistic");
    MvcResult mvcResult = this.mvc
        .perform(post(AlertsController.ALERTS + "/").contentType(MediaType.APPLICATION_JSON)
            .content(json(alert)).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is4xxClientError()).andReturn();
    assertNotNull(mvcResult);

  }

  @Test
  public void testBCreateAlert() throws Exception {
    MvcResult mvcResult =
        this.mvc.perform(post(AlertsController.ALERTS + "/").contentType(MediaType.APPLICATION_JSON)
            .content(json(this.getAmazonRequestAlert("Alert Memory")))
            .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
    assertNotNull(mvcResult);
  }

  @Test
  public void testC1UpdateAlert() throws Exception {
    AlertRequest alertRequest = this.getAmazonRequestAlert("Alert Memory 1");
    alertRequest.setAlarmDescription("Description changed");
    MvcResult mvcResult = this.mvc
        .perform(put(AlertsController.ALERTS + "/").contentType(MediaType.APPLICATION_JSON)
            .content(json(alertRequest)).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is5xxServerError()).andReturn();
    assertNotNull(mvcResult);

  }

  @Test
  public void testCUpdateAlert() throws Exception {
    AlertRequest alertRequest = this.getAmazonRequestAlert("Alert Memory");
    alertRequest.setAlarmDescription("Description changed");
    MvcResult mvcResult = this.mvc
        .perform(put(AlertsController.ALERTS + "/").contentType(MediaType.APPLICATION_JSON)
            .content(json(alertRequest)).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();
    assertNotNull(mvcResult);

  }

  @Test
  public void testDGetAlerts() throws Exception {
    MvcResult mvcResult = this.mvc
        .perform(
            get(AlertsController.ALERTS + "/").param("provider", Providers.AMAZON_AWS.toString())
                .param("alarmName", "").param("region", "us-west-2"))
        .andExpect(status().isOk()).andReturn();

    List<CloudosAlert> responseData = objectMapper.readValue(
        mvcResult.getResponse().getContentAsString(), new TypeReference<List<CloudosAlert>>() {});

    // response JSON payload
    log.info(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseData));

  }

  @Test
  public void testE1GetAlertByName() throws Exception {
    MvcResult mvcResult = this.mvc
        .perform(get(AlertsController.ALERTS + "/" + "Alert Memory 2")
            .param("provider", Providers.AMAZON_AWS.toString()).param("region", "us-west-2"))
        .andExpect(status().is5xxServerError()).andReturn();
    assertNotNull(mvcResult);
  }

  @Test
  public void testEGetAlertByName() throws Exception {
    MvcResult mvcResult = this.mvc
        .perform(get(AlertsController.ALERTS + "/" + "Alert Memory")
            .param("provider", Providers.AMAZON_AWS.toString()).param("region", "us-west-2"))
        .andExpect(status().isOk()).andReturn();

    AlertResponse<CloudosAlert> responseData =
        objectMapper.readValue(mvcResult.getResponse().getContentAsString(),
            new TypeReference<AlertResponse<CloudosAlert>>() {});

    // response JSON payload
    log.info(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseData));

  }

  @Test
  public void testFDeleteAlert() throws Exception {
    MvcResult mvcResult = this.mvc
        .perform(delete(AlertsController.ALERTS + "/").contentType(MediaType.APPLICATION_JSON)
            .content(json(this.getAmazonRequestAlertGeneric())).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();
    assertNotNull(mvcResult);
  }


  @Test
  public void testGGetMetrics() throws Exception {
    MvcResult mvcResult =
        this.mvc
            .perform(get(AlertsController.ALERTS + "/metrics")
                .param("provider", Providers.AMAZON_AWS.toString()).param("metricName", "")
                .param("dimensionFilters",
                    "[{\"name\":\"InstanceId\"}]")
                .param("metricNamespace", "AWS/EC2").param("region", "us-west-2"))
            .andExpect(status().isOk()).andReturn();
    List<MetricResponse> responseData = objectMapper.readValue(
        mvcResult.getResponse().getContentAsString(), new TypeReference<List<MetricResponse>>() {});
    // response JSON payload
    log.info(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseData));

  }
 
  @Test
  public void testHGetNameSpaceMetrics() throws Exception {
    MvcResult mvcResult = this.mvc
        .perform(get(AlertsController.ALERTS + "/metrics/namespaces")
            .param("provider", Providers.AMAZON_AWS.toString()).param("region", "us-west-2"))
        .andExpect(status().isOk()).andReturn();
    
    List<String> responseData = objectMapper.readValue(
        mvcResult.getResponse().getContentAsString(), new TypeReference<List<String>>() {});
    
    // response JSON payload
    log.info(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseData));

  }
  
  @Test
  public void testIGetCategoriesMetrics() throws Exception {
    MvcResult mvcResult =
        this.mvc
            .perform(get(AlertsController.ALERTS + "/metrics/categories")
                .param("provider", Providers.AMAZON_AWS.toString())
                .param("metricNamespace", "AWS/S3").param("region", "us-west-2"))
            .andExpect(status().isOk()).andReturn();
 
    List<CategoryMetricResponse> responseData = objectMapper.readValue(
        mvcResult.getResponse().getContentAsString(), new TypeReference<List<CategoryMetricResponse>>() {});
    
    // response JSON payload
    log.info(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseData));

  }
  
  @Test
  public void getListMetricStatusAlarm() throws Exception {
    MvcResult mvcResult = this.mvc
        .perform(get(AlertsController.ALERTS + "/metrics/alarmstatus")
            .param("provider", Providers.AMAZON_AWS.toString()))
        .andExpect(status().isOk()).andReturn();
    
    List<MetricStatusAlarmResponse> responseData = objectMapper.readValue(
        mvcResult.getResponse().getContentAsString(), new TypeReference<List<MetricStatusAlarmResponse>>() {});
    
    // response JSON payload
    log.info(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseData));

  }
  
  @Test
  public void getListMetricPeriodAlert() throws Exception {
    MvcResult mvcResult = this.mvc
        .perform(get(AlertsController.ALERTS + "/metrics/periods")
            .param("provider", Providers.AMAZON_AWS.toString()))
        .andExpect(status().isOk()).andReturn();
    
    List<MetricPeriodAlertResponse> responseData = objectMapper.readValue(
        mvcResult.getResponse().getContentAsString(), new TypeReference<List<MetricPeriodAlertResponse>>() {});
    
    // response JSON payload
    log.info(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseData));

  }
  
  @Test
  public void getListMetricMissingDataAlert() throws Exception {
    MvcResult mvcResult = this.mvc
        .perform(get(AlertsController.ALERTS + "/metrics/missingdatas")
            .param("provider", Providers.AMAZON_AWS.toString()))
        .andExpect(status().isOk()).andReturn();
    
    List<MetricMissingDataAlertResponse> responseData = objectMapper.readValue(
        mvcResult.getResponse().getContentAsString(), new TypeReference<List<MetricMissingDataAlertResponse>>() {});
    
    // response JSON payload
    log.info(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseData));

  }

  /**
   * Method that creates the request object to create an alert.
   * 
   * @param name alert name.
   * @return AlertRequest alert that will be created.
   */
  private AlertRequest getAmazonRequestAlert(String name) {
    AlertRequest alert = new AlertRequest();
    alert.setProvider(Providers.AMAZON_AWS);
    alert.setAlarmName(name);
    alert.setRegion("us-west-2");
    alert.setComparisonOperator("GreaterThanThreshold");
    alert.setEvaluationPeriods(1);
    alert.setMetricName("CPUUtilization");
    alert.setNamespace("AWS/EC2");
    alert.setPeriod(60);
    alert.setStatistic("Average");
    alert.setThreshold(new Double("70.0"));
    alert.setActionsEnabled(false);
    alert.setAlarmDescription("Alarm when server CPU utilization exceeds 70%");
    alert.setUnit("Seconds");
    
    List<CloudosDimension> cloudosDimension = new ArrayList<CloudosDimension>();
    cloudosDimension
        .add(CloudosDimension.builder().name("InstanceId").value("i-07c387131520beeac").build());
    alert.setDimensions(cloudosDimension);

    return alert;
  }


  /**
   * Method that creates the request object to delete an alert.
   * 
   * @return AlertRequestGeneric alert.
   */
  private AlertRequestGeneric getAmazonRequestAlertGeneric() {
    AlertRequestGeneric alert = new AlertRequestGeneric();
    alert.setProvider(Providers.AMAZON_AWS);
    alert.setAlarmName("Alert Memory");
    alert.setRegion("us-west-2");
    return alert;
  }



}
