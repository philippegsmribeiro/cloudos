package cloudos.service;

import static org.junit.Assert.assertTrue;
import java.util.List;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import cloudos.Providers;
import cloudos.exceptions.NotImplementedException;
import cloudos.models.services.CloudosService;
import cloudos.models.services.ServiceCategory;
import cloudos.models.services.ServiceException;
import cloudos.models.services.ServiceRequest;
import cloudos.models.services.ServiceResponse;
import cloudos.services.CloudosServiceRepository;
import cloudos.services.CloudosServiceService;
import test.CloudOSTest;

/**
 * Unit tests for ServiceService.
 *
 * @author Alex Calagua
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CloudosServiceTest extends CloudOSTest {

  @Rule
  public ExpectedException expectedEx = ExpectedException.none();

  @Autowired
  private CloudosServiceRepository cloudosServiceRepository;

  @Autowired
  private CloudosServiceService cloudosServiceService;

  @Before
  public void setup() throws ServiceException {
    ServiceRequest serviceRequest = ServiceRequest.builder().code("TestService").build();
    List<CloudosService> cloudosServices =
        cloudosServiceService.getServicesByFilter(serviceRequest);
    for (CloudosService cloudosService : cloudosServices) {
      cloudosServiceRepository.delete(cloudosService);
    }
  }

  @Test
  public void testAInsertService() throws ServiceException, NotImplementedException {
    ServiceRequest service = ServiceRequest.builder().code("TestServiceAmazonEC21").name("EC2")
        .providers(Providers.AMAZON_AWS).serviceCategory(ServiceCategory.COMPUTING).build();
    ServiceResponse serviceResponse = cloudosServiceService.createService(service);
    assertTrue(serviceResponse.getService() != null);

  }

  @Test
  public void testAInsertServiceDuplicated() throws ServiceException, NotImplementedException {
    expectedEx.expect(ServiceException.class);
    expectedEx.expectMessage("Service already exists");

    ServiceRequest service = ServiceRequest.builder().code("TestServiceAmazonEC22").name("EC2")
        .providers(Providers.AMAZON_AWS).serviceCategory(ServiceCategory.COMPUTING).build();
    cloudosServiceService.createService(service);

    ServiceRequest service2 = ServiceRequest.builder().code("TestServiceAmazonEC22").name("EC2")
        .providers(Providers.AMAZON_AWS).serviceCategory(ServiceCategory.COMPUTING).build();
    cloudosServiceService.createService(service2);
  }

  @Test
  public void testAInsertServiceDuplicatedWhitoutException()
      throws ServiceException, NotImplementedException {

    ServiceRequest service = ServiceRequest.builder().code("TestServiceAmazonEC22").name("EC2")
        .providers(Providers.AMAZON_AWS).serviceCategory(ServiceCategory.COMPUTING).build();
    cloudosServiceService.createService(service);

    ServiceRequest service2 = ServiceRequest.builder().code("TestServiceAmazonEC22").name("EC2")
        .providers(Providers.AMAZON_AWS).serviceCategory(ServiceCategory.COMPUTING).build();
    cloudosServiceService.createService(service2, false);
  }


  @Test
  public void testBValidatedInsertService() throws ServiceException, NotImplementedException {
    expectedEx.expect(ServiceException.class);
    expectedEx.expectMessage("Code request can not be null");
    ServiceRequest service = ServiceRequest.builder().code("").name("EC2")
        .providers(Providers.AMAZON_AWS).serviceCategory(ServiceCategory.COMPUTING).build();
    cloudosServiceService.createService(service);
  }


  @Test
  public void testCUpdateService() throws ServiceException, NotImplementedException {
    ServiceRequest serviceInsert =
        ServiceRequest.builder().code("TestServiceAmazonEC23").name("EC2")
            .providers(Providers.AMAZON_AWS).serviceCategory(ServiceCategory.COMPUTING).build();
    ServiceResponse serviceResponseInsert = cloudosServiceService.createService(serviceInsert);
    serviceResponseInsert.getService().setName("EC2 Changed");
    ServiceRequest serviceUpdate =
        ServiceRequest.builder().id(serviceResponseInsert.getService().getId())
            .code(serviceResponseInsert.getService().getCode())
            .name(serviceResponseInsert.getService().getName())
            .providers(serviceResponseInsert.getService().getProviders())
            .serviceCategory(serviceResponseInsert.getService().getServiceCategory())
            .serviceStatus(serviceResponseInsert.getService().getServiceStatus()).build();
    cloudosServiceService.updateService(serviceUpdate);

    CloudosService cloudosService =
        cloudosServiceService.findById(serviceResponseInsert.getService().getId());
    assertTrue(cloudosService.getName().equals(serviceUpdate.getName()));
  }

  @Test
  public void testDDeleteOtherService() throws ServiceException, NotImplementedException {
    ServiceRequest service = ServiceRequest.builder().code("TestServiceAmazonEC24").name("EC2")
        .providers(Providers.AMAZON_AWS).serviceCategory(ServiceCategory.COMPUTING).build();
    cloudosServiceService.createService(service);

    expectedEx.expect(ServiceException.class);
    expectedEx.expectMessage("Other service can not be deleted");
    CloudosService cloudosOther = cloudosServiceRepository.findByCodeAndProvidersAndServiceCategory(
        "OTHER", Providers.AMAZON_AWS, ServiceCategory.COMPUTING);
    cloudosServiceService.deleteService(cloudosOther.getId());
  }

  @Test
  public void testEDeleteService() throws ServiceException, NotImplementedException {
    ServiceRequest service = ServiceRequest.builder().code("TestServiceAmazonEC25").name("EC2")
        .providers(Providers.AMAZON_AWS).serviceCategory(ServiceCategory.COMPUTING).build();
    ServiceResponse responseService = cloudosServiceService.createService(service);
    cloudosServiceService.deleteService(responseService.getService().getId());
  }

  @Test
  public void testFFindServiceById() throws ServiceException, NotImplementedException {
    ServiceRequest service = ServiceRequest.builder().code("TestServiceAmazonEC26").name("EC2")
        .providers(Providers.AMAZON_AWS).serviceCategory(ServiceCategory.COMPUTING).build();
    ServiceResponse serviceResponse = cloudosServiceService.createService(service);

    CloudosService response = cloudosServiceService.findById(serviceResponse.getService().getId());

    assertTrue(response != null);
  }

  @Test
  public void testGFindServiceByIdProvidersAndIdServiceCategory()
      throws ServiceException, NotImplementedException {
    ServiceRequest service = ServiceRequest.builder().code("TestServiceAmazonEC27").name("EC2")
        .providers(Providers.AMAZON_AWS).serviceCategory(ServiceCategory.COMPUTING).build();
    cloudosServiceService.createService(service);

    List<CloudosService> cloudosServices =
        cloudosServiceService.findByIdProvidersAndIdServiceCategory(service);

    assertTrue(!cloudosServices.isEmpty());
  }

  @Test
  public void testHFindServiceByFilter() throws ServiceException, NotImplementedException {
    ServiceRequest service = ServiceRequest.builder().code("TestServiceAmazonEC28").name("EC2")
        .providers(Providers.AMAZON_AWS).serviceCategory(ServiceCategory.COMPUTING).build();
    cloudosServiceService.createService(service);
    ServiceRequest service2 = ServiceRequest.builder().code("TestServiceAmazonC3").name("C2")
        .providers(Providers.AMAZON_AWS).serviceCategory(ServiceCategory.COMPUTING).build();
    cloudosServiceService.createService(service2);
    ServiceRequest service3 = ServiceRequest.builder().code("TestServiceGoogleC2").name("C2")
        .providers(Providers.GOOGLE_COMPUTE_ENGINE).serviceCategory(ServiceCategory.COMPUTING)
        .build();
    cloudosServiceService.createService(service3);
    ServiceRequest serviceRequest = ServiceRequest.builder().code("TestServiceAmazon").build();
    List<CloudosService> cloudosServices =
        cloudosServiceService.getServicesByFilter(serviceRequest);

    assertTrue(!cloudosServices.isEmpty());
  }

  @Test
  public void testIDeleteServiceNotExists() throws ServiceException, NotImplementedException {
    String id = "3232ewe232rr3";
    expectedEx.expect(ServiceException.class);
    expectedEx.expectMessage("Cloudos service with id 3232ewe232rr3 does not exist");
    cloudosServiceService.deleteService(id);
  }

  @Test
  public void testJUpdateServiceNotExists() throws ServiceException, NotImplementedException {
    ServiceRequest serviceUpdate =
        ServiceRequest.builder().id("3232ewe232rr3sdsdsd").code("TestServiceAmazonEC28").name("EC2")
            .providers(Providers.AMAZON_AWS).serviceCategory(ServiceCategory.COMPUTING).build();
    expectedEx.expect(ServiceException.class);
    expectedEx.expectMessage("Cloudos service with id 3232ewe232rr3sdsdsd does not exist");

    cloudosServiceService.updateService(serviceUpdate);
  }

}
