package cloudos.alert;

import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import cloudos.Providers;
import cloudos.alerts.AlertException;
import cloudos.alerts.AlertService;
import cloudos.alerts.CloudosAlertService;
import cloudos.exceptions.NotImplementedException;
import cloudos.models.alerts.AlertRequest;
import cloudos.models.alerts.AlertRequestGeneric;
import cloudos.models.alerts.AlertResponse;
import cloudos.models.alerts.CategoryMetricResponse;
import cloudos.models.alerts.CloudosAlert;
import cloudos.models.alerts.CloudosDimension;
import cloudos.models.alerts.CloudosNotification;
import cloudos.models.alerts.GenericAlert;
import cloudos.models.alerts.MetricMissingDataAlertResponse;
import cloudos.models.alerts.MetricNotificationTopicResponse;
import cloudos.models.alerts.MetricPeriodAlertResponse;
import cloudos.models.alerts.MetricRequest;
import cloudos.models.alerts.MetricResponse;
import cloudos.models.alerts.MetricStatusAlarmResponse;
import test.CloudOSTest;

/**
 * Unit tests for AlertsService.
 *
 * @author Alex Calagua
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AlertServiceTest extends CloudOSTest {

  private static final String US_WEST_2 = "us-west-2";
  @Rule
  public ExpectedException expectedEx = ExpectedException.none();

  @Autowired
  private AlertService alertService;

  @Autowired
  private CloudosAlertService cloudosAlertService;


  private static final List<GenericAlert> ALERT_TO_DELETE = new ArrayList<>();

  @Before
  public void setup() throws AlertException, NotImplementedException {
    // clean the aws
    AlertRequest alertFilter = new AlertRequest();
    alertFilter.setProvider(Providers.AMAZON_AWS);
    alertFilter.setRegion(US_WEST_2);
    List<? extends GenericAlert> listResult = alertService.getListAlert(alertFilter);
    for (GenericAlert genericAlert : listResult) {
      alertService.deleteAlert(getAmazonRequestAlert(genericAlert.getAlarmName()));
    }
  }

  @Test
  public void testACrudAlert() throws AlertException, NotImplementedException {
    AlertResponse<?> alertResponse =
        alertService.createAlert(getAmazonRequestAlert("Test A Alert CPU"));
    assertTrue(!alertResponse.getResponse().isEmpty());

    AlertRequest amazonAlertForUpdate = getAmazonRequestAlert("Test A Alert CPU");
    amazonAlertForUpdate.setAlarmDescription("New Description");
    amazonAlertForUpdate.setProvider(Providers.AMAZON_AWS);
    amazonAlertForUpdate.setId(alertResponse.getAlert().getId());
    alertService.updateAlert(amazonAlertForUpdate);

    List<? extends GenericAlert> listResult =
        alertService.getListAlert(getAmazonRequestAlert("Test A Alert CPU"));
    assertTrue(!listResult.isEmpty());

    for (GenericAlert genericAlert : listResult) {
      AlertRequestGeneric alertRequest = new AlertRequestGeneric();
      alertRequest.setAlarmName(genericAlert.getAlarmName());
      alertRequest.setProvider(genericAlert.getProvider());
      alertRequest.setRegion(genericAlert.getRegion());
      AlertResponse<?> alertAmazonDeleted = alertService.deleteAlert(alertRequest);
      assertTrue(!alertAmazonDeleted.getResponse().isEmpty());
    }

  }

  @Test
  public void testCrudAlertteste() throws AlertException, NotImplementedException {
    CloudosAlert alert = CloudosAlert.builder().comparisonOperator("GreaterThanThreshold")
        .evaluationPeriods(1).period(60).metricName("BucketSizeBytes").namespace("AWS/S3")
        .statistic("Average").threshold(new Double("8.0")).actionsEnabled(true)
        .alarmDescription("Alarm when server CPU utilization exceeds 70%").unit("Seconds")
        .provider(Providers.AMAZON_AWS).alarmName("Test A Alert CPU\"").region(US_WEST_2).build();
    List<CloudosNotification> cloudosNotification = new ArrayList<CloudosNotification>();
    cloudosNotification.add(CloudosNotification.builder()
        .codeNotificationTopic("arn:aws:sns:us-west-2:079337523389:notification_alarm")
        .codeStatus("OK").build());
    alert.setNotifications(cloudosNotification);
    CloudosAlert alertResponse = cloudosAlertService.createAlert(alert);
    assertTrue(alertResponse != null);
    List<? extends GenericAlert> listResult = cloudosAlertService.getListAlert();
    assertTrue(!listResult.isEmpty());
  }

  @Test
  public void testBUpdateAlertExceptionNotExits() throws AlertException, NotImplementedException {
    expectedEx.expect(AlertException.class);
    expectedEx.expectMessage("Alert was not found");
    AlertRequest alertAmazonForUpdate = getAmazonRequestAlert("Test B Alert CPU + MEMORY");
    alertService.updateAlert(alertAmazonForUpdate);
  }

  @Test
  public void testCListAllAlert() throws AlertException, NotImplementedException {
    alertService.createAlert(getAmazonRequestAlert("Test C Alert Memory"));
    AlertRequest alertFilter = new AlertRequest();
    alertFilter.setProvider(Providers.AMAZON_AWS);
    alertFilter.setRegion(US_WEST_2);
    alertFilter.setAlarmName("");
    List<? extends GenericAlert> listResult = alertService.getListAlert(alertFilter);
    for (GenericAlert genericAlert : listResult) {
      ALERT_TO_DELETE.add(genericAlert);
    }
    assertTrue(!listResult.isEmpty());
  }

  @Test
  public void testDListAlertFilter() throws AlertException, NotImplementedException {
    AlertResponse<?> alertResponseCreate1 =
        alertService.createAlert(getAmazonRequestAlert("Test D AlertCPU"));
    AlertResponse<?> alertResponseCreate2 =
        alertService.createAlert(getAmazonRequestAlert("Test D AlertMemory"));
    AlertResponse<?> alertResponseCreate3 =
        alertService.createAlert(getAmazonRequestAlert("Test CPU 70%"));
    AlertRequest alertFilter = new AlertRequest();
    alertFilter.setProvider(Providers.AMAZON_AWS);
    alertFilter.setRegion(US_WEST_2);
    alertFilter.setAlarmName("Test D");
    List<? extends GenericAlert> listResult = alertService.getListAlert(alertFilter);

    ALERT_TO_DELETE.add(alertResponseCreate1.getAlert());
    ALERT_TO_DELETE.add(alertResponseCreate2.getAlert());
    ALERT_TO_DELETE.add(alertResponseCreate3.getAlert());
    assertTrue(listResult.size() == 2);
  }

  @Test
  public void testEListMetrics() throws AlertException, NotImplementedException {
    MetricRequest metricRequest = new MetricRequest();
    metricRequest.setProvider(Providers.AMAZON_AWS);
    metricRequest.setRegion(US_WEST_2);
    metricRequest.setMetricNamespace("AWS/S3");
    List<MetricResponse> listResult = alertService.getListMetric(metricRequest);
    assertTrue(listResult.isEmpty());
  }

  @Test
  public void testFListNameSpaceOfMetrics() throws AlertException, NotImplementedException {
    MetricRequest metricRequest = new MetricRequest();
    metricRequest.setProvider(Providers.AMAZON_AWS);
    metricRequest.setRegion(US_WEST_2);
    List<String> listResult = alertService.getListNameSpaceOfMetrics(metricRequest);
    assertTrue(!listResult.isEmpty());
  }

  @Test
  public void testFListCategoriesMetrics() throws AlertException, NotImplementedException {
    MetricRequest metricRequest = new MetricRequest();
    metricRequest.setProvider(Providers.AMAZON_AWS);
    metricRequest.setMetricNamespace("AWS/S3");
    metricRequest.setRegion(US_WEST_2);
    List<CategoryMetricResponse> listResult =
        alertService.getListCategoriesOfMetrics(metricRequest);
    assertTrue(!listResult.isEmpty());
  }

  /**
   * Method that creates the request object to create an alert.
   *
   * @param name alert name.
   * @return AlertAmazon alert that will be created.
   */
  private AlertRequest getAmazonRequestAlert(String name) {
    AlertRequest alert = AlertRequest.builder().comparisonOperator("GreaterThanThreshold")
        .evaluationPeriods(1).period(60).metricName("BucketSizeBytes").namespace("AWS/S3")
        .statistic("Average").threshold(new Double("8.0")).actionsEnabled(true)
        .alarmDescription("Alarm when server CPU utilization exceeds 70%").unit("Seconds")
        .provider(Providers.AMAZON_AWS).alarmName(name).region(US_WEST_2).build();

    List<CloudosDimension> cloudosDimension = new ArrayList<CloudosDimension>();
    cloudosDimension
        .add(CloudosDimension.builder().name("StorageType").value("StandardStorage").build());
    cloudosDimension
        .add(CloudosDimension.builder().name("BucketName").value("cloudos-data").build());
    alert.setDimensions(cloudosDimension);


    List<CloudosNotification> cloudosNotification = new ArrayList<CloudosNotification>();
    cloudosNotification.add(CloudosNotification.builder()
        .codeNotificationTopic("arn:aws:sns:us-west-2:079337523389:notification_alarm")
        .codeStatus("OK").build());
    alert.setNotifications(cloudosNotification);
    return alert;
  }

  @Test
  public void testGExceptionRequestIsNull() throws AlertException, NotImplementedException {
    expectedEx.expect(AlertException.class);
    expectedEx.expectMessage("Metric request can not be null");
    MetricRequest metricRequest = null;
    alertService.getListMetric(metricRequest);
  }

  @Test
  public void testHExceptionProviderIsNull() throws AlertException, NotImplementedException {
    expectedEx.expect(AlertException.class);
    expectedEx.expectMessage("Provider can not be null");
    MetricRequest metricRequest = new MetricRequest();
    alertService.getListMetric(metricRequest);
  }

  @Test
  public void testICreateAlertInAmazonAndRepository()
      throws AlertException, NotImplementedException {
    AlertResponse<?> alertResponse =
        alertService.createAlert(getAmazonRequestAlert("Test I Alert CPU 70%"));
    ALERT_TO_DELETE.add(alertResponse.getAlert());
    CloudosAlert alert = (CloudosAlert) alertResponse.getAlert();
    CloudosAlert cloudosAlert = cloudosAlertService.getAlertByAlarmNameAndRegion(
        alertResponse.getAlert().getAlarmName(), alertResponse.getAlert().getRegion());
    assertTrue(alert.equals(cloudosAlert));
  }

  @Test
  public void testJCreateSynchronizeAlertAmazonAndRepository()
      throws AlertException, NotImplementedException {

    // Create alert in Amazon an delete in repository mongodb
    AlertResponse<?> alertResponse =
        alertService.createAlert(getAmazonRequestAlert("Test J Alert CPU 80%"));
    ALERT_TO_DELETE.add(alertResponse.getAlert());
    cloudosAlertService.deleteAlert(alertResponse.getAlert().getId());

    CloudosAlert alertDeleted = cloudosAlertService.getAlertByAlarmNameAndRegion(
        alertResponse.getAlert().getAlarmName(), alertResponse.getAlert().getRegion());

    assertTrue(alertDeleted == null);


    // Look for an alert on amazon. As the alert has been deleted in mongo db the service will
    // create the alert again in mongo db to stay synchronized
    AlertRequest alertFilter = new AlertRequest();
    alertFilter.setProvider(Providers.AMAZON_AWS);
    alertFilter.setRegion(US_WEST_2);
    alertFilter.setAlarmName("Test J Alert CPU 80%");
    CloudosAlert alert = (CloudosAlert) alertService.getAlertByName(alertFilter).getAlert();

    // Look for the alert in mongo db to compare with the alert initially created in amazon
    CloudosAlert cloudosAlert = cloudosAlertService.getAlertByAlarmNameAndRegion(
        alertResponse.getAlert().getAlarmName(), alertResponse.getAlert().getRegion());

    assertTrue(alert.equals(cloudosAlert));
  }


  @Test
  public void testKUpdateSynchronizeAlertAmazonAndRepository()
      throws AlertException, NotImplementedException {

    // Create alert in Amazon an update in repository mongodb
    AlertResponse<?> alertResponse =
        alertService.createAlert(getAmazonRequestAlert("Test K Alert CPU 90%"));
    ALERT_TO_DELETE.add(alertResponse.getAlert());
    CloudosAlert cloudosAlertForUpdate = (CloudosAlert) alertResponse.getAlert();
    cloudosAlertForUpdate.setAlarmDescription("Description changed");
    cloudosAlertService.updateAlert(cloudosAlertForUpdate);


    // Search alert in amazon and update alert in mongo db.
    AlertRequest alertFilter = new AlertRequest();
    alertFilter.setProvider(Providers.AMAZON_AWS);
    alertFilter.setRegion(US_WEST_2);
    alertFilter.setAlarmName("Test K Alert CPU 90%");
    CloudosAlert alert = (CloudosAlert) alertService.getAlertByName(alertFilter).getAlert();

    // Look for the alert in mongo db to compare with the alert initially created in amazon
    CloudosAlert cloudosAlert = cloudosAlertService.getAlertByAlarmNameAndRegion(
        alertResponse.getAlert().getAlarmName(), alertResponse.getAlert().getRegion());

    assertTrue(alert.equals(cloudosAlert));
  }

  @Test
  public void testMDeleteAlert() throws AlertException, NotImplementedException {
    for (GenericAlert genericAlert : ALERT_TO_DELETE) {
      AlertRequestGeneric alertRequest = new AlertRequestGeneric();
      alertRequest.setAlarmName(genericAlert.getAlarmName());
      alertRequest.setProvider(genericAlert.getProvider());
      alertRequest.setRegion(genericAlert.getRegion());
      alertService.deleteAlert(alertRequest);
    }
    List<? extends GenericAlert> listResult = cloudosAlertService.getListAlert();
    System.out.println(listResult.size());
  }

  @Test
  public void testNMetricStatusAlarm() throws AlertException, NotImplementedException {
    MetricRequest metricRequest = new MetricRequest();
    metricRequest.setProvider(Providers.AMAZON_AWS);
    List<MetricStatusAlarmResponse> listResult =
        alertService.getListMetricStatusAlarm(metricRequest);
    assertTrue(!listResult.isEmpty());
  }

  @Test
  public void testOListNotificationTopicAlarm() throws AlertException, NotImplementedException {
    MetricRequest metricRequest = new MetricRequest();
    metricRequest.setProvider(Providers.AMAZON_AWS);
    metricRequest.setRegion(US_WEST_2);
    List<MetricNotificationTopicResponse> listResult =
        alertService.getListNotificationTopic(metricRequest);
    assertTrue(listResult.isEmpty());
  }

  @Test
  public void testPListMetricMissingDataAlert() throws AlertException, NotImplementedException {
    MetricRequest metricRequest = new MetricRequest();
    metricRequest.setProvider(Providers.AMAZON_AWS);
    metricRequest.setRegion(US_WEST_2);
    List<MetricMissingDataAlertResponse> listResult =
        alertService.getListMetricMissingDataAlert(metricRequest);
    assertTrue(!listResult.isEmpty());
  }

  @Test
  public void testRListMetricPeriodAlert() throws AlertException, NotImplementedException {
    MetricRequest metricRequest = new MetricRequest();
    metricRequest.setProvider(Providers.AMAZON_AWS);
    metricRequest.setRegion(US_WEST_2);
    List<MetricPeriodAlertResponse> listResult =
        alertService.getListMetricPeriodAlert(metricRequest);
    assertTrue(!listResult.isEmpty());
  }

}
