package cloudos;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import test.CloudOSTest;

public class CloudosControllerTest extends CloudOSTest {

  private MockMvc mvc;

  @Before
  public void setUp() throws Exception {
    this.mvc = MockMvcBuilders.standaloneSetup(new CloudosController()).build();
  }

  @Test
  public void testCloudosController() throws Exception {
    this.mvc
        .perform(MockMvcRequestBuilders.get("/").accept(contentType))
        .andExpect(status().isOk());
  }
}
