package cloudos.dashboard;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import cloudos.dashboard.DashboardRegion;
import cloudos.dashboard.DashboardRegionRepository;
import cloudos.dashboard.RegionEntry;
import cloudos.dashboard.RegionEntryRepository;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

import test.CloudOSTest;

/** Created by philipperibeiro on 6/28/17. */
public class RegionEntryTest extends CloudOSTest {

  @Autowired RegionEntryRepository repository;

  @Autowired DashboardRegionRepository dashboardRegionRepository;

  @Before
  public void setUp() throws Exception {
    List<DashboardRegion> regionList =
        dashboardRegionRepository.getRegionByAvailable(true, new PageRequest(0, 5));

    RegionEntry entry = new RegionEntry(regionList);
    // save the entry in the repository
    this.repository.save(entry);
  }

  @Test
  public void testAddRegionEntry() throws Exception {
    List<DashboardRegion> regionList =
        dashboardRegionRepository.getRegionByAvailable(true, new PageRequest(0, 3));

    RegionEntry entry = new RegionEntry(regionList);
    // save the entry in the repository
    this.repository.save(entry);

    // attempt to fetch the entry now
    RegionEntry other = this.repository.findFirstByOrderByTimestampDesc();
    assertEquals(entry.getTimestamp(), other.getTimestamp());
  }

  @Test
  public void testDeleteRegionEntry() throws Exception {

    List<DashboardRegion> regionList =
        dashboardRegionRepository.getRegionByAvailable(true, new PageRequest(0, 1));

    // save one
    RegionEntry entry = new RegionEntry(regionList);
    // save the entry in the repository
    this.repository.save(entry);

    // delete the entry
    // attempt to fetch the entry now
    RegionEntry other = this.repository.findFirstByOrderByTimestampDesc();
    this.repository.delete(other);

    // fetch the latest
    RegionEntry regionEntry = this.repository.findFirstByOrderByTimestampDesc();
    // chech the two have different time stamps.
    assertNotNull(regionEntry.getTimestamp());
  }
}
