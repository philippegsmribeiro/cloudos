package cloudos.dashboard;

import static org.hamcrest.number.OrderingComparison.greaterThanOrEqualTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import cloudos.Providers;
import cloudos.dashboard.Cost;
import cloudos.dashboard.CostEntry;
import cloudos.dashboard.CostEntryRepository;
import cloudos.dashboard.CostEstimatedEntry;
import cloudos.dashboard.CostEstimatedEntryRepository;
import cloudos.dashboard.DashboardService;
import cloudos.dashboard.InstanceInfo;
import cloudos.dashboard.Provider;
import cloudos.dashboard.RegionEntry;
import cloudos.dashboard.ResourceEntry;
import cloudos.dashboard.ResourceEntryRepository;
import cloudos.instances.InstanceService;
import cloudos.models.ClientStatus;
import cloudos.models.CloudosDatapointAggregated;
import cloudos.models.Instance;
import cloudos.models.InstanceStatus;
import cloudos.utils.DateUtil;

import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

/** Created by philipperibeiro on 2/3/17. */
public class DashboardServiceTest extends CloudOSTest {

  @Autowired
  DashboardService dashboardService;

  @Autowired
  ResourceEntryRepository entryRepository;

  @Autowired
  CostEntryRepository costEntryRepository;

  @Autowired
  CostEstimatedEntryRepository costEstimatedEntryRepository;

  @Autowired
  InstanceService instanceService;

  @Before
  public void setUp() throws Exception {
    // correctly setup the test database
    if (dashboardService.getResources() == null) {
      ResourceEntry entry = new ResourceEntry();
      entryRepository.save(entry);
    }

    if (dashboardService.getCost() == null) {
      CostEntry entry = new CostEntry();
      costEntryRepository.save(entry);
    }

    if (dashboardService.getCostEstimated() == null) {
      CostEstimatedEntry entry = new CostEstimatedEntry();
      costEstimatedEntryRepository.save(entry);
    }
  }

  @Test
  public void testGetInstances() throws Exception {
    List<InstanceInfo> instanceInfos = this.dashboardService.getInstances();
    assertNotNull(instanceInfos);
    if (instanceInfos.size() > 0) {
      InstanceInfo instanceInfo = instanceInfos.get(0);
      logger.debug(instanceInfo);
    }
  }

  @Test
  public void testGetCost() throws Exception {
    CostEntry entry = this.dashboardService.getCost();
    assertNotNull(entry);
    logger.debug("Obtained cost entry {}", entry);
  }

  @Test
  public void testGetCostEstimated() throws Exception {
    CostEstimatedEntry entry = this.dashboardService.getCostEstimated();
    assertNotNull(entry);
    logger.debug("Obtained cost estimated entry {}", entry);
  }

  @Test
  public void testCalculateCostEstimated() throws Exception {
    Cost totalDailyCost = new Cost();
    totalDailyCost.setCost(1.0);
    Cost totalMonthyCost = new Cost();
    totalMonthyCost.setCost(100.0);
    totalMonthyCost.setPercentage(1.0);
    Cost totalWeeklyCost = new Cost();
    totalWeeklyCost.setCost(23.0);
    totalWeeklyCost.setPercentage(1.2);
    Cost totalYearlyCost = new Cost();
    totalYearlyCost.setCost(1200.0);

    CostEntry costEntry = new CostEntry(totalDailyCost, totalMonthyCost, totalWeeklyCost, null);

    CostEstimatedEntry entry =
        this.dashboardService.calculateCostEstimatedEntry(costEntry, totalYearlyCost);
    assertNotNull(entry);
    logger.debug("Obtained cost estimated entry {}", entry);
  }

  @Test
  @Ignore
  public void testGetResources() throws Exception {
    ResourceEntry entry = this.dashboardService.getResources();
    assertNotNull(entry);
    assertEquals(entry.getMonth(), DateUtil.getMonth());
    assertThat(entry.getTotal(), org.hamcrest.Matchers.greaterThanOrEqualTo(0.0));
    assertThat(entry.getResources().size(), greaterThanOrEqualTo(0));
    logger.debug(entry);
  }

  @Test
  public void testGetProviders() throws Exception {
    List<Provider> providers = this.dashboardService.getProviders();
    assertNotNull(providers);
    logger.debug(providers);
  }

  @Test
  public void testGetRegions() throws Exception {
    // @TODO: This test is leaky ... we have to fix it.
    RegionEntry regions = this.dashboardService.getRegions();
    assertNotNull(regions);
    logger.debug(regions);
  }

  @Test
  @Ignore
  public void testCPU() throws Exception {
    Map<Providers, List<CloudosDatapointAggregated>> cloudCPUUsage =
        this.dashboardService.getCloudCpuUsage(null);
    logger.debug(cloudCPUUsage);
    // aws
    Providers provider = Providers.AMAZON_AWS;
    cloudCPUUsage = this.dashboardService.getCloudCpuUsage(provider);
    assertTrue(cloudCPUUsage.containsKey(provider));
    // google
    provider = Providers.GOOGLE_COMPUTE_ENGINE;
    cloudCPUUsage = this.dashboardService.getCloudCpuUsage(provider);
    assertTrue(cloudCPUUsage.containsKey(provider));
  }

  @Test
  @Ignore
  public void testDisk() throws Exception {
    Map<Providers, List<CloudosDatapointAggregated>> cloudDiskUsage =
        this.dashboardService.getCloudDiskUsage(null);
    logger.debug(cloudDiskUsage);
    // aws
    Providers provider = Providers.AMAZON_AWS;
    cloudDiskUsage = this.dashboardService.getCloudDiskUsage(provider);
    assertTrue(cloudDiskUsage.containsKey(provider));
    // google
    provider = Providers.GOOGLE_COMPUTE_ENGINE;
    cloudDiskUsage = this.dashboardService.getCloudDiskUsage(provider);
    assertTrue(cloudDiskUsage.containsKey(provider));
  }

  @Test
  public void testMemory() throws Exception {
    Map<Providers, List<CloudosDatapointAggregated>> cloudMemoryUsage =
        this.dashboardService.getCloudMemoryUsage(null);
    logger.debug(cloudMemoryUsage);
    // aws
    Providers provider = Providers.AMAZON_AWS;
    cloudMemoryUsage = this.dashboardService.getCloudMemoryUsage(provider);
    assertTrue(cloudMemoryUsage.containsKey(provider));
    // google
    provider = Providers.GOOGLE_COMPUTE_ENGINE;
    cloudMemoryUsage = this.dashboardService.getCloudMemoryUsage(provider);
    assertTrue(cloudMemoryUsage.containsKey(provider));
  }

  @Test
  @Ignore
  public void testNetWork() throws Exception {
    Map<Providers, List<CloudosDatapointAggregated>> cloudNetworkUsage =
        this.dashboardService.getCloudNetworkUsage(null);
    logger.debug(cloudNetworkUsage);
    // aws
    Providers provider = Providers.AMAZON_AWS;
    cloudNetworkUsage = this.dashboardService.getCloudNetworkUsage(provider);
    assertTrue(cloudNetworkUsage.containsKey(provider));
    // google
    provider = Providers.GOOGLE_COMPUTE_ENGINE;
    cloudNetworkUsage = this.dashboardService.getCloudNetworkUsage(provider);
    assertTrue(cloudNetworkUsage.containsKey(provider));
  }

  @Test
  @Ignore
  public void updateRegionsExpectsNoExceptions() {
    // create fake instance just for testing
    Instance instance = new Instance();
    instance.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
    instance.setClientInstalled(true);
    instance.setClientStatus(ClientStatus.INSTALLED);
    instance.setDeleted(false);
    instance.setRegion("us-west1");
    instance.setProviderId("test");
    instance.setStatus(InstanceStatus.RUNNING);
    instance = instanceService.save(instance);

    dashboardService.updateRegions();

    // delete it
    instanceService.delete(instance.getId());
  }

  @Test
  public void updateResourcesExpectsNoExceptions() {
    dashboardService.updateResources();
  }
}
