package cloudos.dashboard;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import cloudos.dashboard.Cost;
import cloudos.dashboard.CostEntry;
import cloudos.dashboard.CostEntryRepository;
import cloudos.dashboard.DashboardJob;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

/** Created by philipperibeiro on 6/23/17. */
public class CostEntryTest extends CloudOSTest {

  @Autowired
  CostEntryRepository costEntryRepository;

  private static List<CostEntry> entryList;

  @BeforeClass
  public static void setUp() throws Exception {
    List<BigDecimal> weeklyCost = Arrays
        .asList(BigDecimal.valueOf(10.0), BigDecimal.valueOf(11.0), BigDecimal.valueOf(12.0));

    entryList =
        Arrays.asList(
            new CostEntry(
                new Cost(10.0, new Date().toString()),
                new Cost(11.0, DashboardJob.WEEKLY_COST),
                new Cost(12.0, DashboardJob.MONTHLY_COST),
                weeklyCost),
            new CostEntry(
                new Cost(11.0, "May"),
                new Cost(11.0, DashboardJob.WEEKLY_COST),
                new Cost(12.0, DashboardJob.MONTHLY_COST),
                weeklyCost),
            new CostEntry(
                new Cost(12.0, "April"),
                new Cost(11.0, DashboardJob.WEEKLY_COST),
                new Cost(12.0, DashboardJob.MONTHLY_COST),
                weeklyCost));
  }

  @Test
  public void testSaveCostEntry() throws Exception {
    long entries = this.costEntryRepository.count();
    this.costEntryRepository.save(entryList);
    // make sure the 4 entries are accounted for
    assertEquals(entries + 3, this.costEntryRepository.count());
  }

  @Test
  public void testDeleteCostEntry() throws Exception {
    this.costEntryRepository.delete(entryList);
    for (CostEntry entry : entryList) {
      assertNull(this.costEntryRepository.findOne(entry.getId()));
    }
  }
}
