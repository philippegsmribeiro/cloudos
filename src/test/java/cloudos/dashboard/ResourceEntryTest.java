package cloudos.dashboard;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertEquals;

import cloudos.Providers;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.CloudOSTest;

/** Created by philipperibeiro on 6/23/17. */
public class ResourceEntryTest extends CloudOSTest {

  @Autowired ResourceEntryRepository entryRepository;
  private static ResourceEntry entry;

  @BeforeClass
  public static void setUp() {
    List<Resource> resources =
        Arrays.asList(
            new Resource(getRandomString(), 10.0, Providers.AMAZON_AWS),
            new Resource(getRandomString(), 11.0, Providers.MICROSOFT_AZURE),
            new Resource(getRandomString(), 12.0, Providers.GOOGLE_COMPUTE_ENGINE));

    entry = ResourceEntry.builder()
              .resources(resources)
              .month(new Date().toString())
              .total(33.0)
              .build();
  }

  @Test
  public void testResourceEntry() {
    long count = this.entryRepository.count();
    this.entryRepository.save(entry);
    assertEquals(count + 1, this.entryRepository.count());
  }

  @Test
  public void testDeleteResourceEntry() {
    this.entryRepository.delete(entry);
    assertNull(this.entryRepository.findOne(entry.getId()));
  }

  private static String getRandomString() {
    return generator.generate(10);
  }
}
