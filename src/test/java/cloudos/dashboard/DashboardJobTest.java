package cloudos.dashboard;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import cloudos.costanalysis.GenericBillingReportRepository;
import cloudos.dashboard.Cost;
import cloudos.dashboard.DashboardJob;
import cloudos.dashboard.DashboardRegion;
import cloudos.dashboard.DashboardRegionRepository;
import cloudos.dashboard.Resource;
import cloudos.instances.InstanceService;
import cloudos.models.Instance;
import cloudos.models.costanalysis.GenericBillingReport;
import cloudos.utils.DateUtil;
import com.google.common.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.PageRequest;
import test.CloudOSTest;

/** Created by philipperibeiro on 6/23/17. */
public class DashboardJobTest extends CloudOSTest {

  private static final String PAYER_ACCOUNT_ID = "287334376268";

  private static final String CLOUDOS_REGIONS_JSON = "cloudos/regions.json";

  private static DashboardJob job;

  @Autowired
  private InstanceService instanceService;

  @Autowired
  private DashboardRegionRepository dashboardRegionRepository;

  @Autowired
  private GenericBillingReportRepository genericBillingReportRepository;

  @BeforeClass
  public static void setUp() {
    job = new DashboardJob();
  }

  @AfterClass
  public static void tearDown() throws Exception {
    job = null;
  }

  @Test
  public void testCostJob() {
    String month = DateUtil.getMonth();
    List<GenericBillingReport> billingReports = genericBillingReportRepository
        .findByAccountId(PAYER_ACCOUNT_ID, new PageRequest(0, 100));
    Cost cost = job.getCost(billingReports, month);
    logger.debug(cost);
    assertNotNull("Cost Response cannot be null", cost);
    assertEquals(cost.getMonth(), month);
  }

  @Test
  public void testGetResources() {
    List<GenericBillingReport> billingReports = genericBillingReportRepository
        .findByAccountId(PAYER_ACCOUNT_ID, new PageRequest(0, 100));
    List<Resource> resources = job.getResources(new ArrayList<>(billingReports));
    assertNotNull(resources);
  }

  @Test
  @SuppressWarnings("Duplicates")
  public void testGetInstancesByRegion() {
    List<DashboardRegion> dashboardRegions = dashboardRegionRepository.getRegionByAvailable(true);
    if (CollectionUtils.isEmpty(dashboardRegions)) {
      dashboardRegions = loadRegions();
      logger.debug("DashboardRegion: {}", dashboardRegions);
      if (!CollectionUtils.isEmpty(dashboardRegions)) {
        dashboardRegionRepository.save(dashboardRegions);
      }
    }
    List<Instance> instances = instanceService.findAllActiveAndNotTerminatedInstances();
    if (CollectionUtils.isEmpty(instances)) {
      logger.warn("There are no instances or regions to be processed");
      return;
    }
    List<DashboardRegion> regions = job.getInstancesByRegion(instances, dashboardRegions);
    logger.debug("Regions: {}", regions);
    assertNotNull(regions);
  }

  @SuppressWarnings("Duplicates")
  private List<DashboardRegion> loadRegions() {

    final Type REGION_TYPE = new TypeToken<List<DashboardRegion>>() {
    }.getType();
    List<DashboardRegion> dashboardRegions;
    try {
      String filename = new ClassPathResource(CLOUDOS_REGIONS_JSON).getFilename();
      // Create JsonReader from Json.
      JsonReader reader = new JsonReader(new FileReader(filename));
      dashboardRegions = gson.fromJson(reader, REGION_TYPE);
      // save the regions to the repository
      return dashboardRegions;
    } catch (IOException e) {
      logger.error(e.getMessage());
      return new ArrayList<>();
    }
  }

  @Test
  public void testGetTotalWeeklyCost() {
    // return all the Usage reports by the last 7 days
    List<GenericBillingReport> billingReports = genericBillingReportRepository
        .findByUsageStartTimeBetween(
            new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(7)), new Date(),
            new PageRequest(0, 100));

    assertNotNull(billingReports);
    logger.debug("Found {} reports for the past 7 days", billingReports.size());
    Cost cost = job.getTotalWeeklyCost(billingReports);
    logger.debug(cost);
    assertEquals(cost.getMonth(), DashboardJob.WEEKLY_COST);
  }

  @Test
  public void testGetTotalMonthlyCost() {
    // return all the Usage reports by the last 30 days
    List<GenericBillingReport> billingReports = genericBillingReportRepository
        .findByUsageStartTimeBetween(
            new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(30)), new Date(),
            new PageRequest(0, 100));

    assertNotNull(billingReports);
    logger.debug("Found {} reports for the past month", billingReports.size());
    Cost cost = job.getTotalMonthlyCost(billingReports);
    logger.debug(cost);
    assertEquals(cost.getMonth(), DashboardJob.MONTHLY_COST);
  }

  @Test
  public void testGetWeeklyCost() {
    List<Date> week =
        Arrays.asList(new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(24)),
            new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(25)),
            new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(26)),
            new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(27)),
            new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(28)),
            new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(29)),
            new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(30)));

    List<GenericBillingReport> billingReports = genericBillingReportRepository
        .findByUsageStartTimeBetween(
            new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(30)), new Date(),
            new PageRequest(0, 100));
    assertNotNull(billingReports);
    logger.debug("Found {} reports for the past 7 days", billingReports.size());
    List<BigDecimal> result = job
        .getWeeklyCost(new ArrayList<>(billingReports), week);
    logger.debug(result);
    logger.debug("================= Weekly Cost ===============");
    result.forEach(v -> {
      logger.debug("Cost was {}", v);
    });
    logger.debug("==============================================");
  }
}