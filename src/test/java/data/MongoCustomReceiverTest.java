package data;

import static junit.framework.TestCase.assertEquals;

import cloudos.data.MongoCustomReceiver;
import cloudos.utils.DateTimeUtil;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;

import test.CloudOSTest;

/** Created by gleimar on 22/01/2017. */
public class MongoCustomReceiverTest extends CloudOSTest {

  @Value("${spring.data.mongodb.host}")
  protected String host;

  @Value("${spring.data.mongodb.port}")
  protected Integer port;

  @Value("${spring.data.mongodb.database}")
  protected String database;

  @Value("${spring.data.mongodb.username}")
  private String username;

  @Value("${spring.data.mongodb.password}")
  private String password;

  @Test
  public void testGetNextTime() {
    MongoCustomReceiver mongoCustomReceiver = new MongoCustomReceiver("", 0, "", "", "", "");
    Long dateTime =
        DateTimeUtil.convertToMilliseconds("22/01/2017 12:00:00", "dd/MM/yyyy HH:mm:ss");

    long nextOffsetTime = mongoCustomReceiver.getNextOffsetTime(30, dateTime);

    long dateTimeOffset =
        DateTimeUtil.convertToMilliseconds("22/01/2017 12:00:30", "dd/MM/yyyy HH:mm:ss");

    logger.debug(
        String.format(
            "Initial : %s ; Offset : %s ; Result : %s", dateTime, nextOffsetTime, dateTimeOffset));

    assertEquals(nextOffsetTime, dateTimeOffset);
  }
}
