package models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import cloudos.Providers;
import cloudos.data.InstanceDataMetric;
import cloudos.data.InstanceDataUnit;
import cloudos.models.CloudosDatapoint;
import cloudos.models.CloudosDatapointsRepository;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

/** Created by philipperibeiro on 3/24/17. */
public class CloudosDatapointTest extends CloudOSTest {

  @Autowired private CloudosDatapointsRepository cloudosDatapointsRepository;

  @Test
  public void testSave() throws Exception {
    CloudosDatapoint cloudosDatapoint =
        new CloudosDatapoint(
            Providers.AMAZON_AWS,
            "us-west-1",
            "i-xxxxxxx",
            "i-xxxxxxx",
            InstanceDataMetric.CPU_UTILIZATION,
            null,
            0.0,
            InstanceDataUnit.PERCENTUAL);

    // test saving the datapoint to the collection
    this.cloudosDatapointsRepository.save(cloudosDatapoint);

    // now test finding the object
    CloudosDatapoint anotherDatapoint =
        this.cloudosDatapointsRepository.findOne(cloudosDatapoint.id);
    assertNotNull(anotherDatapoint);
    assertEquals(cloudosDatapoint.metric, anotherDatapoint.metric);
    assertEquals(cloudosDatapoint.provider, anotherDatapoint.provider);

    // test deleting the object
    this.cloudosDatapointsRepository.delete(cloudosDatapoint.id);
    anotherDatapoint = this.cloudosDatapointsRepository.findOne(cloudosDatapoint.id);
    // this object should not be found
    assertNull(anotherDatapoint);
  }
}
