package models;

import static org.junit.Assert.assertEquals;

import cloudos.amazon.AWSCredentialService;
import cloudos.amazon.Key;
import cloudos.models.AwsKeyPair;
import cloudos.models.AwsKeyPairRepository;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

/** Created by philipperibeiro on 12/17/16. */
public class AwsKeyPairTest extends CloudOSTest {

  @Autowired private AwsKeyPairRepository awsKeyPairRepository;

  private static AmazonEC2 client;

  private static boolean initialized = false;

  @Autowired AWSCredentialService awsCredentialService;
  // @Configuration
  // @TestPropertySource(value="classpath:application-test.properties")
  // static class ContextConfiguration {
  // }

  @Before
  public void setUp() throws Exception {
    if (!initialized) {
      client =
          AmazonEC2ClientBuilder.standard()
              .withCredentials(awsCredentialService.getAWSCredentials())
              .withRegion(Regions.US_EAST_1)
              .build();
      this.awsKeyPairRepository.deleteAll();
      initialized = true;
    }
  }

  @Test
  public void testCreateAwsKeyPair() {
    Key key = new Key();
    String privateKey = key.getPrivateKey("awskeypair", client);
    AwsKeyPair keyPair = new AwsKeyPair("us-west-1", "awskeypair", true, privateKey);
    // save the keypair
    this.awsKeyPairRepository.save(keyPair);
    AwsKeyPair other = this.awsKeyPairRepository.findOne(keyPair.getId());
    assertEquals(keyPair.getKey(), other.getKey());
    assertEquals(this.awsKeyPairRepository.count(), 1);
    // clean up the key
    key.deleteKey("awskeypair", client);
    // set it to false
    keyPair.setValid(false);
    this.awsKeyPairRepository.save(keyPair);
    // check if the field was properly set
    other = this.awsKeyPairRepository.findOne(keyPair.getId());
    assertEquals(other.isValid(), keyPair.isValid());
  }

  @Test
  public void testFindByKeyname() {
    Key key = new Key();
    String privateKey = key.getPrivateKey("awskeypair2", client);
    AwsKeyPair keyPair = new AwsKeyPair("us-west-1", "awskeypair2", true, privateKey);
    // save the keypair
    this.awsKeyPairRepository.save(keyPair);
    AwsKeyPair other = this.awsKeyPairRepository.findByKeyname("awskeypair2");
    assertEquals(keyPair.getKey(), other.getKey());
    AwsKeyPair other2 =
        this.awsKeyPairRepository.findByRegionAndKeyname("us-west-1", "awskeypair2");
    assertEquals(keyPair.getKey(), other2.getKey());
    // clean up the key
    key.deleteKey("awskeypair2", client);
  }

  @After
  public void tearDown() throws Exception {
    this.awsKeyPairRepository.deleteAll();
  }
}
