package models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;

import cloudos.models.Hit;
import cloudos.models.Metricbeat;
import cloudos.models.elasticsearch.Process;
import cloudos.models.elasticsearch.System;
import test.CloudOSTest;

/** Created by philipperibeiro on 7/10/17. */
public class MetricbeatTest extends CloudOSTest {

  static DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
  static JsonDeserializer<Date> dateJsonDeserializer =
      (json, typeOfT, context) -> {
        try {
          return json == null ? null : df.parse(json.getAsString());
        } catch (ParseException e) {
          e.printStackTrace();
        }
        return null;
      };

  static Gson gson =
      new GsonBuilder().registerTypeAdapter(Date.class, dateJsonDeserializer).create();

  @Test
  public void testBeatSerialize() throws Exception {
    String json =
        "{\"hostname\":\"ip-172-31-13-171\",\"name\":\"ip-172-31-13-171\",\"version\":\"5.4.3\"}";

    Metricbeat.Beat beat = gson.fromJson(json, Metricbeat.Beat.class);
    logger.debug("Beat: {}", beat);
    assertNotNull(beat);
    assertEquals(beat.hostname, "ip-172-31-13-171");
  }

  @Test
  public void testMetricSet() throws Exception {
    String json = "{\"module\":\"system\",\"name\":\"process\",\"rtt\":42024}";
    Metricbeat.MetricSet metricSet = gson.fromJson(json, Metricbeat.MetricSet.class);
    logger.debug("MetricSet: {}", metricSet);
    assertNotNull(metricSet);
    assertEquals(metricSet.module, "system");
  }

  @Test
  public void testBlkio() throws Exception {
    String json =
        "{\"id\":\"systemd-udevd.service\",\"path\":\"/system.slice/systemd-udevd.service\",\"total\":"
            + "{\"bytes\":19217920,\"ios\":285}}";

    Process.CGroup.Blkio blkio = gson.fromJson(json, Process.CGroup.Blkio.class);
    logger.debug("Blkio: {}", blkio);
    assertNotNull(blkio);
    assertEquals(blkio.path, "/system.slice/systemd-udevd.service");
  }

  @Test
  public void testCpu() throws Exception {
    String json =
        "{\"cfs\":{\"period\":{\"us\":100000},\"quota\":{\"us\":0},\"shares\":1024},\"id\":"
            + "\"systemd-udevd.service\",\"path\":\"/system.slice/systemd-udevd.service\",\"rt\":{\"period\":{\"us\":0},"
            + "\"runtime\":{\"us\":0}},\"stats\":{\"periods\":0,\"throttled\":{\"ns\":0,\"periods\":0}}}";
    Process.CGroup.Cpu cpu = gson.fromJson(json, Process.CGroup.Cpu.class);
    logger.debug("CPU: {}", cpu);
    assertNotNull(cpu);
    assertEquals(cpu.id, "systemd-udevd.service");
  }

  @Test
  public void testCpuacct() throws Exception {
    String json =
        "{\"id\":\"systemd-udevd.service\",\"path\":\"/system.slice/systemd-udevd.service\",\"percpu\":"
            + "{\"1\":1331763761},\"stats\":{\"system\":{\"ns\":970000000},\"user\":{\"ns\":170000000}},\"total\":"
            + "{\"ns\":1331763761}}";
    Process.CGroup.Cpuacct cpuacct = gson.fromJson(json, Process.CGroup.Cpuacct.class);
    logger.debug("CPUAcct: {}", cpuacct);
    assertNotNull(cpuacct);
    assertEquals(cpuacct.id, "systemd-udevd.service");
  }

  @Test
  public void testMemory() throws Exception {
    String kmem_tcp =
        "{\"failures\":0,\"limit\":{\"bytes\":9223372036854772000},\"usage\":{\"bytes\":20430848,\"max\":"
            + "{\"bytes\":89534464}}}";
    Process.CGroup.Memory.KmemTcp kmemTcp =
        gson.fromJson(kmem_tcp, Process.CGroup.Memory.KmemTcp.class);
    logger.debug("KmemTcp: {}", kmemTcp);
    assertNotNull(kmemTcp);
    assertEquals((int) kmemTcp.failures, 0);

    String json =
        "{\"id\":\"systemd-udevd.service\",\"kmem\":{\"failures\":0,\"limit\":{\"bytes\":9223372036854772000},"
            + "\"usage\":{\"bytes\":20430848,\"max\":{\"bytes\":89534464}}},\"kmem_tcp\":{\"failures\":0,\"limit\":"
            + "{\"bytes\":9223372036854772000},\"usage\":{\"bytes\":20430848,\"max\":{\"bytes\":89534464}}},\"mem\":"
            + "{\"failures\":0,\"limit\":{\"bytes\":9223372036854772000},\"usage\":{\"bytes\":20430848,\"max\":"
            + "{\"bytes\":89534464}}},\"memsw\":{\"failures\":0,\"limit\":{\"bytes\":9223372036854772000},\"usage\":"
            + "{\"bytes\":20430848,\"max\":{\"bytes\":89534464}}},\"path\":\"/system.slice/systemd-udevd.service\","
            + "\"stats\":{\"active_anon\":{\"bytes\":1179648},\"active_file\":{\"bytes\":12582912},\"cache\":"
            + "{\"bytes\":19529728},\"hierarchical_memory_limit\":{\"bytes\":9223372036854772000},\"hierarchical_memsw_limit\":"
            + "{\"bytes\":0},\"inactive_anon\":{\"bytes\":32768},\"inactive_file\":{\"bytes\":6635520},"
            + "\"major_page_faults\":85,\"mapped_file\":{\"bytes\":454656},\"page_faults\":63692,\"pages_in\":56760,"
            + "\"pages_out\":51772,\"rss\":{\"bytes\":901120},\"rss_huge\":{\"bytes\":0},\"swap\":{\"bytes\":0},"
            + "\"unevictable\":{\"bytes\":0}}}";
    Process.CGroup.Memory memory = gson.fromJson(json, Process.CGroup.Memory.class);
    logger.debug("Memory: {}", memory);
    assertNotNull(memory);
    assertEquals(memory.id, "systemd-udevd.service");
    assertEquals(memory.path, "/system.slice/systemd-udevd.service");
  }

  @Test
  public void testSystem() throws Exception {

    String json =
        "{\"process\":{\"cgroup\":{\"blkio\":{\"id\":\"lvm2-lvmetad.service\",\"path\":\"/system.slice/lvm2-lvmetad.service\","
            + "\"total\":{\"bytes\":53248,\"ios\":2}},\"cpu\":{\"cfs\":{\"period\":{\"us\":100000},\"quota\":"
            + "{\"us\":0},\"shares\":1024},\"id\":\"lvm2-lvmetad.service\",\"path\":\"/system.slice/lvm2-lvmetad.service\",\"rt\":"
            + "{\"period\":{\"us\":0},\"runtime\":{\"us\":0}},\"stats\":{\"periods\":0,\"throttled\":{\"ns\":0,\"periods\":0}}},"
            + "\"cpuacct\":{\"id\":\"lvm2-lvmetad.service\",\"path\":\"/system.slice/lvm2-lvmetad.service\",\"percpu\":"
            + "{\"1\":1514964},\"stats\":{\"system\":{\"ns\":0},\"user\":{\"ns\":0}},\"total\":{\"ns\":1514964}},\"id\":"
            + "\"lvm2-lvmetad.service\",\"memory\":{\"id\":\"lvm2-lvmetad.service\",\"kmem\":{\"failures\":0,\"limit\":"
            + "{\"bytes\":9223372036854772000},\"usage\":{\"bytes\":274432,\"max\":{\"bytes\":397312}}},\"kmem_tcp\":"
            + "{\"failures\":0,\"limit\":{\"bytes\":9223372036854772000},\"usage\":{\"bytes\":274432,\"max\":"
            + "{\"bytes\":397312}}},\"mem\":{\"failures\":0,\"limit\":{\"bytes\":9223372036854772000},\"usage\":"
            + "{\"bytes\":274432,\"max\":{\"bytes\":397312}}},\"memsw\":{\"failures\":0,\"limit\":{\"bytes\":9223372036854772000},"
            + "\"usage\":{\"bytes\":274432,\"max\":{\"bytes\":397312}}},\"path\":\"/system.slice/lvm2-lvmetad.service\",\"stats\":"
            + "{\"active_anon\":{\"bytes\":217088},\"active_file\":{\"bytes\":4096},\"cache\":{\"bytes\":57344},"
            + "\"hierarchical_memory_limit\":{\"bytes\":9223372036854772000},\"hierarchical_memsw_limit\":{\"bytes\":0},"
            + "\"inactive_anon\":{\"bytes\":4096},\"inactive_file\":{\"bytes\":49152},\"major_page_faults\":1,\"mapped_file\":"
            + "{\"bytes\":49152},\"page_faults\":122,\"pages_in\":69,\"pages_out\":2,\"rss\":{\"bytes\":217088},\"rss_huge\":"
            + "{\"bytes\":0},\"swap\":{\"bytes\":0},\"unevictable\":{\"bytes\":0}}},\"path\":\"/system.slice/lvm2-lvmetad.service\"},"
            + "\"cmdline\":\"/sbin/lvmetad -f\",\"cpu\":{\"start_time\":\"2017-06-28T02:34:24.000Z\",\"total\":{\"pct\":0}},\"fd\":"
            + "{\"limit\":{\"hard\":4096,\"soft\":1024},\"open\":5},\"memory\":{\"rss\":{\"bytes\":1572864,\"pct\":0.0015},"
            + "\"share\":1388544,\"size\":99143680},\"name\":\"lvmetad\",\"pgid\":434,\"pid\":434,\"ppid\":1,\"state\":\"sleeping\","
            + "\"username\":\"root\"}}";
    System system = gson.fromJson(json, System.class);
    assertNotNull(system);
    logger.debug("System: {}", system);
  }

  @Test
  public void testMetricbeat() throws Exception {
    String json =
        "{\"@timestamp\":\"2017-06-28T04:34:37.012Z\",\"beat\":{\"hostname\":\"ip-172-31-13-171\",\"name\":"
            + "\"ip-172-31-13-171\",\"version\":\"5.4.3\"},\"metricset\":{\"module\":\"system\",\"name\":\"process\","
            + "\"rtt\":42024},\"system\":{\"process\":{\"cgroup\":{\"blkio\":{\"id\":\"lvm2-lvmetad.service\",\"path\":"
            + "\"/system.slice/lvm2-lvmetad.service\",\"total\":{\"bytes\":53248,\"ios\":2}},\"cpu\":{\"cfs\":{\"period\":"
            + "{\"us\":100000},\"quota\":{\"us\":0},\"shares\":1024},\"id\":\"lvm2-lvmetad.service\",\"path\":"
            + "\"/system.slice/lvm2-lvmetad.service\",\"rt\":{\"period\":{\"us\":0},\"runtime\":{\"us\":0}},\"stats\":"
            + "{\"periods\":0,\"throttled\":{\"ns\":0,\"periods\":0}}},\"cpuacct\":{\"id\":\"lvm2-lvmetad.service\",\"path\":"
            + "\"/system.slice/lvm2-lvmetad.service\",\"percpu\":{\"1\":1514964},\"stats\":{\"system\":{\"ns\":0},\"user\":"
            + "{\"ns\":0}},\"total\":{\"ns\":1514964}},\"id\":\"lvm2-lvmetad.service\",\"memory\":{\"id\":"
            + "\"lvm2-lvmetad.service\",\"kmem\":{\"failures\":0,\"limit\":{\"bytes\":9223372036854772000},\"usage\":"
            + "{\"bytes\":274432,\"max\":{\"bytes\":397312}}},\"kmem_tcp\":{\"failures\":0,\"limit\":"
            + "{\"bytes\":9223372036854772000},\"usage\":{\"bytes\":274432,\"max\":{\"bytes\":397312}}},\"mem\":"
            + "{\"failures\":0,\"limit\":{\"bytes\":9223372036854772000},\"usage\":{\"bytes\":274432,\"max\":{\"bytes\":397312}}},"
            + "\"memsw\":{\"failures\":0,\"limit\":{\"bytes\":9223372036854772000},\"usage\":{\"bytes\":274432,\"max\":"
            + "{\"bytes\":397312}}},\"path\":\"/system.slice/lvm2-lvmetad.service\",\"stats\":{\"active_anon\":{\"bytes\":217088},"
            + "\"active_file\":{\"bytes\":4096},\"cache\":{\"bytes\":57344},\"hierarchical_memory_limit\":"
            + "{\"bytes\":9223372036854772000},\"hierarchical_memsw_limit\":{\"bytes\":0},\"inactive_anon\":{\"bytes\":4096},"
            + "\"inactive_file\":{\"bytes\":49152},\"major_page_faults\":1,\"mapped_file\":{\"bytes\":49152},\"page_faults\":122,"
            + "\"pages_in\":69,\"pages_out\":2,\"rss\":{\"bytes\":217088},\"rss_huge\":{\"bytes\":0},\"swap\":{\"bytes\":0},"
            + "\"unevictable\":{\"bytes\":0}}},\"path\":\"/system.slice/lvm2-lvmetad.service\"},\"cmdline\":\"/sbin/lvmetad -f\","
            + "\"cpu\":{\"start_time\":\"2017-06-28T02:34:24.000Z\",\"total\":{\"pct\":0}},\"fd\":{\"limit\":{\"hard\":4096,"
            + "\"soft\":1024},\"open\":5},\"memory\":{\"rss\":{\"bytes\":1572864,\"pct\":0.0015},\"share\":1388544,"
            + "\"size\":99143680},\"name\":\"lvmetad\",\"pgid\":434,\"pid\":434,\"ppid\":1,\"state\":\"sleeping\",\"username\":"
            + "\"root\"}},\"type\":\"metricsets\"}";

    Metricbeat metricbeat = gson.fromJson(json, Metricbeat.class);
    logger.debug("Metricbeat: {}", metricbeat);
    assertNotNull(metricbeat);
    assertNotNull(metricbeat.timestamp);
  }

  @Test
  public void testHit() throws Exception {
    String json =
        "{\"_index\":\"metricbeat-2017.06.28\",\"_type\":\"metricsets\",\"_id\":\"AVzs-tAmHF28MK8jYSyX\","
            + "\"_score\":1,\"_source\":{\"@timestamp\":\"2017-06-28T04:34:38.094Z\",\"beat\":{\"hostname\":\"ip-172-31-6-94\","
            + "\"name\":\"ip-172-31-6-94\",\"version\":\"5.4.3\"},\"metricset\":{\"module\":\"system\",\"name\":\"process\","
            + "\"rtt\":39064},\"system\":{\"process\":{\"cgroup\":{\"blkio\":{\"id\":\"systemd-timesyncd.service\",\"path\":"
            + "\"/system.slice/systemd-timesyncd.service\",\"total\":{\"bytes\":286720,\"ios\":11}},\"cpu\":{\"cfs\":"
            + "{\"period\":{\"us\":100000},\"quota\":{\"us\":0},\"shares\":1024},\"id\":\"systemd-timesyncd.service\",\"path\":"
            + "\"/system.slice/systemd-timesyncd.service\",\"rt\":{\"period\":{\"us\":0},\"runtime\":{\"us\":0}},\"stats\":"
            + "{\"periods\":0,\"throttled\":{\"ns\":0,\"periods\":0}}},\"cpuacct\":{\"id\":\"systemd-timesyncd.service\","
            + "\"path\":\"/system.slice/systemd-timesyncd.service\",\"percpu\":{\"1\":11229816},\"stats\":{\"system\":"
            + "{\"ns\":0},\"user\":{\"ns\":0}},\"total\":{\"ns\":11229816}},\"id\":\"systemd-timesyncd.service\",\"memory\":"
            + "{\"id\":\"systemd-timesyncd.service\",\"kmem\":{\"failures\":0,\"limit\":{\"bytes\":9223372036854772000},"
            + "\"usage\":{\"bytes\":573440,\"max\":{\"bytes\":684032}}},\"kmem_tcp\":{\"failures\":0,\"limit\":{\"bytes\":"
            + "9223372036854772000},\"usage\":{\"bytes\":573440,\"max\":{\"bytes\":684032}}},\"mem\":{\"failures\":0,\"limit\":"
            + "{\"bytes\":9223372036854772000},\"usage\":{\"bytes\":573440,\"max\":{\"bytes\":684032}}},\"memsw\":{\"failures\":"
            + "0,\"limit\":{\"bytes\":9223372036854772000},\"usage\":{\"bytes\":573440,\"max\":{\"bytes\":684032}}},\"path\":"
            + "\"/system.slice/systemd-timesyncd.service\",\"stats\":{\"active_anon\":{\"bytes\":278528},\"active_file\":"
            + "{\"bytes\":131072},\"cache\":{\"bytes\":294912},\"hierarchical_memory_limit\":{\"bytes\":9223372036854772000},"
            + "\"hierarchical_memsw_limit\":{\"bytes\":0},\"inactive_anon\":{\"bytes\":0},\"inactive_file\":{\"bytes\":163840},"
            + "\"major_page_faults\":3,\"mapped_file\":{\"bytes\":245760},\"page_faults\":191,\"pages_in\":161,\"pages_out\":21,"
            + "\"rss\":{\"bytes\":278528},\"rss_huge\":{\"bytes\":0},\"swap\":{\"bytes\":0},\"unevictable\":{\"bytes\":0}}},"
            + "\"path\":\"/system.slice/systemd-timesyncd.service\"},\"cmdline\":\"/lib/systemd/systemd-timesyncd\",\"cpu\":{"
            + "\"start_time\":\"2017-06-28T02:34:24.000Z\",\"total\":{\"pct\":0}},\"fd\":{\"limit\":{\"hard\":4096,\"soft\":1024},"
            + "\"open\":14},\"memory\":{\"rss\":{\"bytes\":2523136,\"pct\":0.0024},\"share\":2306048,\"size\":104828928},"
            + "\"name\":\"systemd-timesyn\",\"pgid\":535,\"pid\":535,\"ppid\":1,\"state\":\"sleeping\",\"username\":"
            + "\"systemd-timesync\"}},\"type\":\"metricsets\"}}";

    Hit<Metricbeat> hit = gson.fromJson(json, Hit.class);
    logger.debug("Hit: {}", hit);
    assertNotNull(hit);
    assertNotNull(hit.getSource());
    assertNotNull(hit.getIndex());
    assertEquals(hit.getIndex(), "metricbeat-2017.06.28");
  }
  
  @Test
  public void testJsonHit() throws Exception {
    String jsonHit = "{\"@timestamp\":\"2018-02-13T02:02:41.029Z\",\"beat\":{\"hostname\":\"ip-172-31-44-236\",\"name\":\"eymbrkmo\",\"version\":\"5.5.0\"},\"metricset\":{\"module\":\"system\",\"name\":\"network\",\"rtt\":66},\"system\":{\"network\":{\"in\":{\"bytes\":1476447872,\"dropped\":0,\"errors\":0,\"packets\":12672352},\"name\":\"eth0\",\"out\":{\"bytes\":17474135440,\"dropped\":0,\"errors\":0,\"packets\":9955706}}},"
        + "\"type\":\"metricsets\"}";
    
    String jsonHit2 = "{\"@timestamp\":\"2018-02-12T20:33:22.936Z\",\"beat\":{\"hostname\":\"ip-172-31-10-0\",\"name\":\"i-0f6e0f75607fd3012\",\"version\":\"5.5.0\"},\"metricset\":{\"module\":\"system\",\"name\":\"process\",\"rtt\":25245},\"system\":{\"process\":{\"cpu\":{\"start_time\":\"2018-01-28T00:14:32.000Z\",\"total\":{\"pct\":0.000000}},\"fd\":{\"limit\":{\"hard\":4096,\"soft\":1024},\"open\":0},\"memory\":{\"rss\":{\"bytes\":0,\"pct\":0.000000},\"share\":0,\"size\":0},\"name\":\"scsi_tmf_1\",\"pgid\":0,\"pid\":1443,\"ppid\":2,\"state\":\"sleeping\",\"username\":\"root\"}},\"type\":\"metricsets\"}";
    
    String jsonHit3 = "{\"@timestamp\":\"2018-02-12T20:33:22.936Z\",\"beat\":{\"hostname\":\"ip-172-31-10-0\",\"name\":\"i-0f6e0f75607fd3012\",\"version\":\"5.5.0\"},\"metricset\":{\"module\":\"system\",\"name\":\"process\",\"rtt\":25245},\"system\":{\"process\":{\"cmdline\":\"/usr/share/metricbeat/bin/metricbeat-god -r / -n -p /var/run/metricbeat.pid -- /usr/share/metricbeat/bin/metricbeat -c /etc/metricbeat/metricbeat.yml -path.home /usr/share/metricbeat -path.config /etc/metricbeat -path.data /var/lib/metricbeat -path.logs /var/log/metricbeat\",\"cpu\":{\"start_time\":\"2018-01-28T00:16:22.000Z\",\"total\":{\"pct\":0.000000}},\"fd\":{\"limit\":{\"hard\":4096,\"soft\":1024},\"open\":3},\"memory\":{\"rss\":{\"bytes\":655360,\"pct\":0.000600},\"share\":589824,\"size\":9621504},\"name\":\"metricbeat-god\",\"pgid\":4072,\"pid\":4097,\"ppid\":1,\"state\":\"sleeping\",\"username\":\"root\"}},\"type\":\"metricsets\"}";
    
    String jsonHit4 = "{\"@timestamp\":\"2018-02-12T20:33:24.373Z\",\"beat\":{\"hostname\":\"ip-172-31-27-74\",\"name\":\"twymktek\",\"version\":\"5.5.0\"},\"metricset\":{\"module\":\"system\",\"name\":\"process\",\"rtt\":22009},\"system\":{\"process\":{\"cpu\":{\"start_time\":\"2018-01-23T05:48:47.000Z\",\"total\":{\"pct\":0.000000}},\"fd\":{\"limit\":{\"hard\":4096,\"soft\":1024},\"open\":0},\"memory\":{\"rss\":{\"bytes\":0,\"pct\":0.000000},\"share\":0,\"size\":0},\"name\":\"netns\",\"pgid\":0,\"pid\":13,\"ppid\":2,\"state\":\"sleeping\",\"username\":\"root\"}},\"type\":\"metricsets\"}";

    Metricbeat metricbeat = gson.fromJson(jsonHit, Metricbeat.class);
    logger.debug("Metricbeat: {}", metricbeat);
    assertNotNull(metricbeat);
    assertNotNull(metricbeat.type);
    assertNotNull(metricbeat.toString());

    metricbeat = gson.fromJson(jsonHit2, Metricbeat.class);
    logger.debug("Metricbeat: {}", metricbeat);
    assertNotNull(metricbeat);
    assertNotNull(metricbeat.type);
    assertNotNull(metricbeat.toString());

    metricbeat = gson.fromJson(jsonHit3, Metricbeat.class);
    logger.debug("Metricbeat: {}", metricbeat);
    assertNotNull(metricbeat);
    assertNotNull(metricbeat.type);
    assertNotNull(metricbeat.toString());

    metricbeat = gson.fromJson(jsonHit4, Metricbeat.class);
    logger.debug("Metricbeat: {}", metricbeat);
    assertNotNull(metricbeat);
    assertNotNull(metricbeat.type);
    assertNotNull(metricbeat.toString());
  }
}
