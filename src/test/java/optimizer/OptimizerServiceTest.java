package optimizer;

import cloudos.Providers;
import cloudos.optimizer.CostIncreaseResponse;
import cloudos.optimizer.OptimizerService;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;
import test.SlowTest;

@Category(SlowTest.class)
public class OptimizerServiceTest extends CloudOSTest {

  @Autowired
  OptimizerService optimizerService;

  @Test
  public void testCountInstances() {
    Providers provider = null;
    Integer countInstances = optimizerService.countInstances(provider);
    Integer total = 0;
    for (Providers p : Providers.values()) {
      total += optimizerService.countInstances(p);
    }
    Assert.assertEquals(countInstances, total);
  }

  @Test
  public void testCostIncrease() {
    Providers provider = null;
    CostIncreaseResponse countTotal = optimizerService.getCostIncrease(provider);

    CostIncreaseResponse countTest = new CostIncreaseResponse();
    countTest.setForecastTotal(BigDecimal.ZERO);
    countTest.setLastMonthTotal(BigDecimal.ZERO);
    countTest.setMonthToDateTotal(BigDecimal.ZERO);
    for (Providers p : Providers.values()) {
      CostIncreaseResponse costIncrease = optimizerService.getCostIncrease(p);
      countTest.setForecastTotal(countTest.getForecastTotal().add(costIncrease.getForecastTotal()));
      countTest
          .setLastMonthTotal(countTest.getLastMonthTotal().add(costIncrease.getLastMonthTotal()));
      countTest.setMonthToDateTotal(
          countTest.getMonthToDateTotal().add(costIncrease.getMonthToDateTotal()));
    }
    Assert.assertEquals(countTotal.getForecastTotal().doubleValue(),
        countTest.getForecastTotal().doubleValue(), 0.00001);
    Assert.assertEquals(countTotal.getLastMonthTotal().doubleValue(),
        countTest.getLastMonthTotal().doubleValue(), 0.00001);
    Assert.assertEquals(countTotal.getMonthToDateTotal().doubleValue(),
        countTest.getMonthToDateTotal().doubleValue(), 0.00001);
  }

  @Test
  public void testCostsByService() {
    Providers provider = null;
    Map<String, BigDecimal> costsTotal = optimizerService.getCostsByService(provider);

    Set<String> costsTestKeys = new HashSet<>();
    Set<BigDecimal> costsTestValues = new HashSet<>();
    for (Providers p : Providers.values()) {
      Map<String, BigDecimal> costsByService = optimizerService.getCostsByService(p);
      costsTestKeys.addAll(costsByService.keySet());
      costsTestValues.addAll(costsByService.values());
    }

    Assert.assertEquals(costsTotal.keySet().size(), costsTestKeys.size());
    Assert.assertTrue(costsTotal.keySet().containsAll(costsTestKeys));
    Assert.assertTrue(costsTestKeys.containsAll(costsTotal.keySet()));
    Assert.assertTrue(costsTotal.values().containsAll(costsTestValues));
    Assert.assertTrue(costsTestValues.containsAll(costsTotal.values()));
  }

}
