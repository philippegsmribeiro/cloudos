package optimizer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.elasticloadbalancing.model.HealthCheck;
import com.google.gson.Gson;

import amazon.AmazonLoadBalancerSupportTest;
import cloudos.models.AmazonLoadBalancer;
import cloudos.models.AmazonLoadBalancerListener;
import cloudos.models.AmazonLoadBalancerType;
import cloudos.optimizer.OptimizerController;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Ignore
public class OptimizerControllerAmazonClassicLoadBalancerTest
    extends AmazonLoadBalancerSupportTest {

  private MockMvc mvc;
  private static final int LOAD_BALANCER_PORT = 80;
  private static final int INSTANCE_PORT = 80;
  private static final String LISTENER_PROTOCOL = "HTTP";
  private static final String HEALTH_CHECK_TARGET = "HTTP:80/weather/us/wa/seattle";

  private static final String CLASSIC_LOAD_BALANCER_NAME =
      String.format("cloudos%s", generator.generate(8));

  private static AmazonLoadBalancer amazonClassicLoadBalancerRequest = new AmazonLoadBalancer();

  @Autowired
  private OptimizerController optimizerController;

  @Before
  public void setUp() throws Exception {
    this.mvc = MockMvcBuilders.standaloneSetup(optimizerController).build();
  }

  @Test
  public void testA1CreateClassicLoadBalancer() throws Exception {
    List<String> availabilityZones = Collections.singletonList(Regions.US_EAST_1.getName() + "a");

    HealthCheck healthCheck = new HealthCheck().withHealthyThreshold(10).withInterval(30)
        .withTimeout(5).withUnhealthyThreshold(2).withTarget(HEALTH_CHECK_TARGET);

    AmazonLoadBalancerListener listener =
        AmazonLoadBalancerListener.builder().protocol(LISTENER_PROTOCOL)
            .loadBalancerPort(LOAD_BALANCER_PORT).instancePort(INSTANCE_PORT).build();

    amazonClassicLoadBalancerRequest =
        AmazonLoadBalancer.builder().name(CLASSIC_LOAD_BALANCER_NAME).healthCheck(healthCheck)
            .availabilityZones(availabilityZones).listeners(Collections.singletonList(listener))
            .region(Regions.US_EAST_1.getName()).type(AmazonLoadBalancerType.Classic).build();

    ResultActions resultActions =
        this.mvc
            .perform(post(OptimizerController.OPTIMIZER + "/load_balancer/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(amazonClassicLoadBalancerRequest.toString())
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().is2xxSuccessful());

    String result = resultActions.andReturn().getResponse().getContentAsString();
    amazonClassicLoadBalancerRequest = new Gson().fromJson(result, AmazonLoadBalancer.class);

    assertNotNull(amazonClassicLoadBalancerRequest);

  }

  @Test
  public void testA2DescribeClassicLoadBalancer() throws Exception {

    amazonClassicLoadBalancerRequest.setRegion(Regions.US_EAST_1.getName());

    ResultActions resultActions =
        this.mvc
            .perform(post(OptimizerController.OPTIMIZER + "/load_balancer/describe")
                .contentType(MediaType.APPLICATION_JSON)
                .content(amazonClassicLoadBalancerRequest.toString())
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().is2xxSuccessful());

    String result = resultActions.andReturn().getResponse().getContentAsString();
    AmazonLoadBalancer amazonLoadBalancer = new Gson().fromJson(result, AmazonLoadBalancer.class);

    assertNotNull(amazonLoadBalancer);
    assertEquals(amazonLoadBalancer.getName(), amazonClassicLoadBalancerRequest.getName());

  }

  @Test
  public void testA3DeleteClassicLoadBalancer() throws Exception {

    amazonClassicLoadBalancerRequest.setRegion(Regions.US_EAST_1.getName());
    amazonClassicLoadBalancerRequest.setType(AmazonLoadBalancerType.Classic);

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/load_balancer/delete")
        .contentType(MediaType.APPLICATION_JSON)
        .content(amazonClassicLoadBalancerRequest.toString()).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is2xxSuccessful());

    amazonClassicLoadBalancerRequest.setRegion(Regions.US_EAST_1.getName());

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/load_balancer/describe")
        .contentType(MediaType.APPLICATION_JSON)
        .content(amazonClassicLoadBalancerRequest.toString()).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is5xxServerError());

  }


}
