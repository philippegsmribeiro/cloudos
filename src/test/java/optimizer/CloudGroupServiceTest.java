package optimizer;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import cloudos.Providers;
import cloudos.exceptions.NotImplementedException;
import cloudos.instances.InstanceService;
import cloudos.machinelearning.autoscaler.ResourceCategory;
import cloudos.models.Instance;
import cloudos.models.cloudgroup.CloudGroup;
import cloudos.models.cloudgroup.CloudGroupFilter;
import cloudos.models.cloudgroup.CloudGroupService;
import cloudos.policies.AutoscalerPolicy;
import cloudos.policies.AutoscalerPolicyEntry;
import cloudos.policies.PolicyOptimization;
import cloudos.policies.SpotBidderPolicy;
import cloudos.provider.ProvidersService;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.CloudOSTest;

public class CloudGroupServiceTest extends CloudOSTest {

  @Autowired
  CloudGroupService cloudGroupService;

  @Autowired
  ProvidersService providersService;
  
  @Test
  public void testCreateCloudGroup() throws Exception {
    CloudGroup cloudGroup = CloudGroup.builder().provider(Providers.AMAZON_AWS)
        .name("AWSCloudGroupTest_" + generator.generate(6)).build();

    // create a new group
    CloudGroup another = this.cloudGroupService.createCloudGroup(cloudGroup);
    assertNotNull(another);
    assertEquals(another.getProvider(), cloudGroup.getProvider());
    assertEquals(another.getName(), cloudGroup.getName());

    // attemp to create the same group
    another = this.cloudGroupService.createCloudGroup(cloudGroup);
    assertNull(another);

    // delete it
    cloudGroupService.deleteCloudGroup(cloudGroup.getId());

    // create a Spot Bidder policy
    SpotBidderPolicy spotBidderPolicy = SpotBidderPolicy.builder()
                                            .spotInstancesPercentage(0.5)
                                            .optimization(PolicyOptimization.AVAILABILITY)
                                            .build();

    List<AutoscalerPolicyEntry> entries = Collections.singletonList(
        AutoscalerPolicyEntry.builder()
            .startingGroupSize(1)
            .instanceProtection(false)
            .threshold(0.80)
            .resourceCategory(ResourceCategory.CPU_USAGE)
            .build()
    );
    AutoscalerPolicy autoscalerPolicy = AutoscalerPolicy.builder()
        .autoscalePolicies(entries)
        .build();

    cloudGroup = CloudGroup.builder().provider(Providers.AMAZON_AWS)
        .name("AWSCloudGroupTest_" + generator.generate(6))
        .spotBidderPolicy(spotBidderPolicy)
        .autoscalerPolicy(autoscalerPolicy)
        .build();

    another = this.cloudGroupService.createCloudGroup(cloudGroup);
    assertNotNull(another);
    assertEquals(another.getProvider(), cloudGroup.getProvider());
    assertEquals(another.getName(), cloudGroup.getName());
    assertEquals(another.getSpotBidderPolicy().getSpotInstancesPercentage(),
                 spotBidderPolicy.getSpotInstancesPercentage(), 0.01);
    assertEquals(another.getAutoscalerPolicy().getAutoscalePolicies().get(0).getThreshold(),
        0.80, 0.01);

    // delete it
    cloudGroupService.deleteCloudGroup(cloudGroup.getId());
  }

  @Test
  public void testDeleteCloudGroup() throws Exception {
    CloudGroup group = this.create(Providers.AMAZON_AWS);

    // delete it
    assertTrue(this.cloudGroupService.deleteCloudGroup(group.getId()));

    // attempt to delete an invalid cloud group
    assertFalse(this.cloudGroupService.deleteCloudGroup("InvalidGroup"));

    // delete it
    cloudGroupService.deleteCloudGroup(group.getId());
  }

  @Test
  public void testDescribeCloudGroup() throws Exception {
    CloudGroup group = this.create(Providers.AMAZON_AWS);

    // describe it
    CloudGroup another = this.cloudGroupService.describeCloudGroup(group.getId());
    assertEquals(group.getName(), another.getName());
    assertEquals(group.getProvider(), another.getProvider());

    // check the invalid group
    another = this.cloudGroupService.describeCloudGroup("InvalidGroup");
    assertNull(another);

    // delete it
    cloudGroupService.deleteCloudGroup(group.getId());
  }

  @Test
  public void testListCloudGroups() throws Exception {
    // add two cloud groups

    List<CloudGroup> groups = Arrays.asList(
        CloudGroup.builder().provider(Providers.GOOGLE_COMPUTE_ENGINE).name("GoogleCloudGroupTest")
            .build(),
        CloudGroup.builder().provider(Providers.AMAZON_AWS).name("AWSCloudGroupTest").build());

    // save the two groups
    for (CloudGroup cloudGroup2 : groups) {
      cloudGroupService.createCloudGroup(cloudGroup2);
    }

    // test a list of all providers
    Providers provider = null;
    List<CloudGroup> groupList = this.cloudGroupService.getCloudGroups(provider);
    assertNotNull(groupList);
    // there should be at least two cloud groups.
    assertThat(groupList.size(), greaterThanOrEqualTo(2));

    // there should be at least one Google Compute Engine cloud group.
    provider = Providers.GOOGLE_COMPUTE_ENGINE;
    groupList = this.cloudGroupService.getCloudGroups(provider);
    assertNotNull(groupList);
    // there should be at least two cloud groups.
    assertThat(groupList.size(), greaterThanOrEqualTo(1));

    // there should be at least one Amazon AWS cloud group.
    provider = Providers.AMAZON_AWS;
    groupList = this.cloudGroupService.getCloudGroups(provider);
    assertNotNull(groupList);
    // there should be at least two cloud groups.
    assertThat(groupList.size(), greaterThanOrEqualTo(1));

    // delete the previously created groups
    for (CloudGroup group : groups) {
      // make sure we obtained the id
      CloudGroup another = cloudGroupService.describeCloudGroupByName(group.getName());
      cloudGroupService.deleteCloudGroup(another.getId());
    }
  }

  @Autowired
  InstanceService instanceService;

  @Test
  public void testAddInstanceToCloudGroup() {

    Providers provider = Providers.AMAZON_AWS;
    // create
    CloudGroup cloudGroup = this.create(provider);
    //
    List<Instance> instances =
        instanceService.findAllActiveAndNotStoppedInstancesByProvider(provider);
    //
    if (CollectionUtils.isNotEmpty(instances)) {
      List<String> instancesIds =
          instances.stream().map(i -> i.getId()).collect(Collectors.toList());
      CloudGroup cloudGroupReturned =
          cloudGroupService.addInstanceToCloudGroup(cloudGroup.getId(), instancesIds);
      assertEquals(cloudGroup.getId(), cloudGroupReturned.getId());

      List<Instance> updatedInstances = instanceService.findAllById(instancesIds);
      assertEquals(instances.size(), updatedInstances.size());

      for (Instance instance : updatedInstances) {
        assertEquals(cloudGroup.getId(), instance.getCloudGroupId());
      }
    }

    // delete
    cloudGroupService.deleteCloudGroup(cloudGroup.getId());
  }

  @Test
  public void testRemoveInstanceToCloudGroup() {

    Providers provider = Providers.AMAZON_AWS;
    // create
    CloudGroup cloudGroup = this.create(provider);
    //
    List<Instance> instances =
        instanceService.findAllActiveAndNotStoppedInstancesByProvider(provider);
    //
    if (CollectionUtils.isNotEmpty(instances)) {
      List<String> instancesIds =
          instances.stream().map(i -> i.getId()).collect(Collectors.toList());
      CloudGroup cloudGroupReturned =
          cloudGroupService.addInstanceToCloudGroup(cloudGroup.getId(), instancesIds);
      assertEquals(cloudGroup.getId(), cloudGroupReturned.getId());

      List<Instance> updatedInstances = instanceService.findAllById(instancesIds);
      assertEquals(instances.size(), updatedInstances.size());

      for (Instance instance : updatedInstances) {
        assertEquals(cloudGroup.getId(), instance.getCloudGroupId());
      }

      // remove it
      cloudGroupReturned =
          cloudGroupService.removeInstanceFromCloudGroup(cloudGroup.getId(), instancesIds);

      assertEquals(cloudGroup.getId(), cloudGroupReturned.getId());

      updatedInstances = instanceService.findAllById(instancesIds);
      assertEquals(instances.size(), updatedInstances.size());

      for (Instance instance : updatedInstances) {
        assertNotNull(instance.getCloudGroupId());
      }

    }


    // delete
    cloudGroupService.deleteCloudGroup(cloudGroup.getId());
  }

  @Test
  @Ignore
  public void testCloudGroupFilter() throws NotImplementedException {

    Providers provider = Providers.AMAZON_AWS;
    // create
    CloudGroup cloudGroup = this.create(provider);
    //
    CloudGroupFilter cloudGroupFilter = new CloudGroupFilter();
    cloudGroupFilter.getRegions().add(providersService.retrieveRegions(provider).get(0).getKey());
    cloudGroupFilter.getKeys().add(providersService.retrieveKeys(provider).get(0).getKey());
    cloudGroupFilter.getSecurityGroup()
        .add(providersService.retrieveSecurityGroups(provider).get(0).getKey());
    cloudGroupFilter.getMachineTypes()
        .add(providersService.retrieveMachineTypes(provider).get(0).getKey());
    cloudGroup =
        cloudGroupService.addCloudGroupFilterToCloudGroup(cloudGroup.getId(), cloudGroupFilter);

    assertTrue(cloudGroup.getFilter().equals(cloudGroupFilter));

    // delete
    cloudGroupService.deleteCloudGroup(cloudGroup.getId());
  }

  @Test
  public void testIsBlocked() {
    // create
    Providers provider = Providers.AMAZON_AWS;
    // create
    CloudGroup cloudGroup = this.create(provider);

    logger.debug("Cloud Group ID: {}", cloudGroup.getId());
    // instance should not be blocked
    assertFalse(this.cloudGroupService.isBlocked(cloudGroup.getId()));

    // block the cloud group
    cloudGroup = this.cloudGroupService.setBlock(cloudGroup.getId(), true);
    assertTrue(cloudGroup.getIsBlocked().get());
    assertTrue(this.cloudGroupService.isBlocked(cloudGroup.getId()));

    // unblock the cloud group
    cloudGroup = this.cloudGroupService.setBlock(cloudGroup.getId(), false);
    assertFalse(cloudGroup.getIsBlocked().get());
    assertFalse(this.cloudGroupService.isBlocked(cloudGroup.getId()));
  }


  private CloudGroup create(Providers provider) {
    CloudGroup cloudGroup =
        CloudGroup.builder().provider(provider).name("AWSCloudGroupTest").build();
    // save the group
    cloudGroupService.createCloudGroup(cloudGroup);
    // find it
    CloudGroup group = cloudGroupService.describeCloudGroupByName(cloudGroup.getName());
    assertNotNull(group);

    return group;
  }
}
