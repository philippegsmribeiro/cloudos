package optimizer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.amazonaws.regions.Regions;
import com.google.gson.Gson;

import amazon.AmazonLoadBalancerSupportTest;
import cloudos.models.AmazonLoadBalancer;
import cloudos.models.AmazonLoadBalancerType;
import cloudos.optimizer.OptimizerController;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Ignore
public class OptimizerControllerAmazonLoadBalancerTest extends AmazonLoadBalancerSupportTest {

  private MockMvc mvc;

  private static final String NETWORK_LOAD_BALANCER_NAME =
      String.format("cloudos%s", generator.generate(8));
  private static final String APPLICATION_LOAD_BALANCER_NAME =
      String.format("cloudos%s", generator.generate(8));

  private static AmazonLoadBalancer amazonNetworkLoadBalancerRequest = new AmazonLoadBalancer();
  private static AmazonLoadBalancer amazonApplicationLoadBalancerRequest = new AmazonLoadBalancer();

  @Autowired
  private OptimizerController optimizerController;

  @Before
  public void setUp() throws Exception {
    this.mvc = MockMvcBuilders.standaloneSetup(optimizerController).build();
  }

  @Test
  public void testA1CreateApplicationLoadBalancer() throws Exception {
    createVPCSubnets();
    setVPCSubnets();

    amazonApplicationLoadBalancerRequest = AmazonLoadBalancer.builder()
        .name(APPLICATION_LOAD_BALANCER_NAME).subnets(subnets).ipAddressType("ipv4")
        .region(Regions.US_EAST_1.getName()).type(AmazonLoadBalancerType.Application).build();

    ResultActions resultActions =
        this.mvc
            .perform(post(OptimizerController.OPTIMIZER + "/load_balancer/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(amazonApplicationLoadBalancerRequest.toString())
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().is2xxSuccessful());

    String result = resultActions.andReturn().getResponse().getContentAsString();
    amazonApplicationLoadBalancerRequest = new Gson().fromJson(result, AmazonLoadBalancer.class);

    assertNotNull(amazonApplicationLoadBalancerRequest);

    assertNotNull(amazonApplicationLoadBalancerRequest.getLoadBalancerArn());

  }

  @Test
  public void testA2DescribeApplicationLoadBalancer() throws Exception {

    amazonApplicationLoadBalancerRequest.setRegion(Regions.US_EAST_1.getName());

    ResultActions resultActions =
        this.mvc
            .perform(post(OptimizerController.OPTIMIZER + "/load_balancer/describe")
                .contentType(MediaType.APPLICATION_JSON)
                .content(amazonApplicationLoadBalancerRequest.toString())
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().is2xxSuccessful());

    String result = resultActions.andReturn().getResponse().getContentAsString();
    AmazonLoadBalancer amazonLoadBalancer = new Gson().fromJson(result, AmazonLoadBalancer.class);

    assertNotNull(amazonLoadBalancer);
    assertEquals(amazonLoadBalancer.getName(), amazonApplicationLoadBalancerRequest.getName());

  }

  @Test
  public void testA3DeleteApplicationLoadBalancer() throws Exception {

    amazonApplicationLoadBalancerRequest.setRegion(Regions.US_EAST_1.getName());
    amazonApplicationLoadBalancerRequest.setType(AmazonLoadBalancerType.Application);

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/load_balancer/delete")
        .contentType(MediaType.APPLICATION_JSON)
        .content(amazonApplicationLoadBalancerRequest.toString())
        .accept(MediaType.APPLICATION_JSON)).andExpect(status().is2xxSuccessful());

    amazonApplicationLoadBalancerRequest.setRegion(Regions.US_EAST_1.getName());

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/load_balancer/describe")
        .contentType(MediaType.APPLICATION_JSON)
        .content(amazonApplicationLoadBalancerRequest.toString())
        .accept(MediaType.APPLICATION_JSON)).andExpect(status().is5xxServerError());

    deleteVPCSubnets();
  }

  @Test
  public void testB1CreateNetworkLoadBalancer() throws Exception {
    createVPCSubnets();
    setVPCSubnets();

    amazonNetworkLoadBalancerRequest = AmazonLoadBalancer.builder().name(NETWORK_LOAD_BALANCER_NAME)
        .subnets(subnets).ipAddressType("ipv4").region(Regions.US_EAST_1.getName())
        .type(AmazonLoadBalancerType.Network).build();

    ResultActions resultActions =
        this.mvc
            .perform(post(OptimizerController.OPTIMIZER + "/load_balancer/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(amazonNetworkLoadBalancerRequest.toString())
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().is2xxSuccessful());

    String result = resultActions.andReturn().getResponse().getContentAsString();
    amazonNetworkLoadBalancerRequest = new Gson().fromJson(result, AmazonLoadBalancer.class);

    assertNotNull(amazonNetworkLoadBalancerRequest);

    assertNotNull(amazonNetworkLoadBalancerRequest.getLoadBalancerArn());

  }

  @Test
  public void testB2DescribeNetworkLoadBalancer() throws Exception {

    amazonNetworkLoadBalancerRequest.setRegion(Regions.US_EAST_1.getName());

    ResultActions resultActions =
        this.mvc
            .perform(post(OptimizerController.OPTIMIZER + "/load_balancer/describe")
                .contentType(MediaType.APPLICATION_JSON)
                .content(amazonNetworkLoadBalancerRequest.toString())
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().is2xxSuccessful());

    String result = resultActions.andReturn().getResponse().getContentAsString();
    AmazonLoadBalancer amazonLoadBalancer = new Gson().fromJson(result, AmazonLoadBalancer.class);

    assertNotNull(amazonLoadBalancer);
    assertEquals(amazonLoadBalancer.getName(), amazonNetworkLoadBalancerRequest.getName());

  }

  @Test
  public void testB3DeleteNetworkLoadBalancer() throws Exception {

    amazonNetworkLoadBalancerRequest.setRegion(Regions.US_EAST_1.getName());
    amazonNetworkLoadBalancerRequest.setType(AmazonLoadBalancerType.Network);

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/load_balancer/delete")
        .contentType(MediaType.APPLICATION_JSON)
        .content(amazonNetworkLoadBalancerRequest.toString()).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is2xxSuccessful());

    amazonNetworkLoadBalancerRequest.setRegion(Regions.US_EAST_1.getName());

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/load_balancer/describe")
        .contentType(MediaType.APPLICATION_JSON)
        .content(amazonNetworkLoadBalancerRequest.toString()).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is5xxServerError());

    deleteVPCSubnets();
  }

  @Test
  public void testC1CreateLoadBalancerWithInvalidRequest() throws Exception {

    amazonNetworkLoadBalancerRequest =
        AmazonLoadBalancer.builder().name(null).subnets(subnets).ipAddressType("ipv4")
            .region(Regions.US_EAST_1.getName()).type(AmazonLoadBalancerType.Network).build();

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/load_balancer/create")
        .contentType(MediaType.APPLICATION_JSON)
        .content(amazonNetworkLoadBalancerRequest.toString()).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is5xxServerError());

    amazonNetworkLoadBalancerRequest =
        AmazonLoadBalancer.builder().name(NETWORK_LOAD_BALANCER_NAME).subnets(subnets)
            .ipAddressType("ipv4").region(null).type(AmazonLoadBalancerType.Network).build();

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/load_balancer/create")
        .contentType(MediaType.APPLICATION_JSON)
        .content(amazonNetworkLoadBalancerRequest.toString()).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is4xxClientError());

    amazonNetworkLoadBalancerRequest =
        AmazonLoadBalancer.builder().name(NETWORK_LOAD_BALANCER_NAME).subnets(subnets)
            .ipAddressType("ipv4").region(Regions.US_EAST_1.getName()).type(null).build();

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/load_balancer/create")
        .contentType(MediaType.APPLICATION_JSON)
        .content(amazonNetworkLoadBalancerRequest.toString()).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is5xxServerError());

    amazonNetworkLoadBalancerRequest = AmazonLoadBalancer.builder().name(NETWORK_LOAD_BALANCER_NAME)
        .subnets(subnets).ipAddressType(null).region(Regions.US_EAST_1.getName())
        .type(AmazonLoadBalancerType.Network).build();

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/load_balancer/create")
        .contentType(MediaType.APPLICATION_JSON)
        .content(amazonNetworkLoadBalancerRequest.toString()).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is5xxServerError());

    amazonNetworkLoadBalancerRequest = AmazonLoadBalancer.builder().name(NETWORK_LOAD_BALANCER_NAME)
        .subnets(null).ipAddressType("ipv4").region(Regions.US_EAST_1.getName())
        .type(AmazonLoadBalancerType.Network).build();

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/load_balancer/create")
        .contentType(MediaType.APPLICATION_JSON)
        .content(amazonNetworkLoadBalancerRequest.toString()).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is5xxServerError());


  }

  @Test
  public void testC2DescribeLoadBalancerWithInvalidRequest() throws Exception {

    amazonNetworkLoadBalancerRequest = AmazonLoadBalancer.builder().build();

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/load_balancer/describe")
        .contentType(MediaType.APPLICATION_JSON)
        .content(amazonNetworkLoadBalancerRequest.toString()).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is4xxClientError());

    amazonNetworkLoadBalancerRequest = AmazonLoadBalancer.builder().loadBalancerArn(null)
        .region(Regions.US_EAST_1.getName()).type(AmazonLoadBalancerType.Network).build();

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/load_balancer/describe")
        .contentType(MediaType.APPLICATION_JSON)
        .content(amazonNetworkLoadBalancerRequest.toString()).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is5xxServerError());

    amazonNetworkLoadBalancerRequest =
        AmazonLoadBalancer.builder().loadBalancerArn(NETWORK_LOAD_BALANCER_NAME).region(null)
            .type(AmazonLoadBalancerType.Network).build();

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/load_balancer/describe")
        .contentType(MediaType.APPLICATION_JSON)
        .content(amazonNetworkLoadBalancerRequest.toString()).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is4xxClientError());

    amazonNetworkLoadBalancerRequest =
        AmazonLoadBalancer.builder().loadBalancerArn(NETWORK_LOAD_BALANCER_NAME)
            .region(Regions.US_EAST_1.getName()).type(null).build();

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/load_balancer/describe")
        .contentType(MediaType.APPLICATION_JSON)
        .content(amazonNetworkLoadBalancerRequest.toString()).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is5xxServerError());


  }

  @Test
  public void testC3DeleteLoadBalancerWithInvalidRequest() throws Exception {

    amazonNetworkLoadBalancerRequest = AmazonLoadBalancer.builder().build();

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/load_balancer/delete")
        .contentType(MediaType.APPLICATION_JSON)
        .content(amazonNetworkLoadBalancerRequest.toString()).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is4xxClientError());

    amazonNetworkLoadBalancerRequest = AmazonLoadBalancer.builder().loadBalancerArn(null)
        .region(Regions.US_EAST_1.getName()).type(AmazonLoadBalancerType.Network).id("ID").build();

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/load_balancer/delete")
        .contentType(MediaType.APPLICATION_JSON)
        .content(amazonNetworkLoadBalancerRequest.toString()).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is5xxServerError());

    amazonNetworkLoadBalancerRequest =
        AmazonLoadBalancer.builder().loadBalancerArn(NETWORK_LOAD_BALANCER_NAME).region(null)
            .type(AmazonLoadBalancerType.Network).id("ID").build();

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/load_balancer/delete")
        .contentType(MediaType.APPLICATION_JSON)
        .content(amazonNetworkLoadBalancerRequest.toString()).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is4xxClientError());

    amazonNetworkLoadBalancerRequest =
        AmazonLoadBalancer.builder().loadBalancerArn(NETWORK_LOAD_BALANCER_NAME)
            .region(Regions.US_EAST_1.getName()).type(null).id("ID").build();

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/load_balancer/delete")
        .contentType(MediaType.APPLICATION_JSON)
        .content(amazonNetworkLoadBalancerRequest.toString()).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is5xxServerError());

    amazonNetworkLoadBalancerRequest = AmazonLoadBalancer.builder()
        .loadBalancerArn(NETWORK_LOAD_BALANCER_NAME).region(Regions.US_EAST_1.getName())
        .type(AmazonLoadBalancerType.Network).id(null).build();

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/load_balancer/delete")
        .contentType(MediaType.APPLICATION_JSON)
        .content(amazonNetworkLoadBalancerRequest.toString()).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is5xxServerError());


    deleteVPCSubnets();
  }

}
