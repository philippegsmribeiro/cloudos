package optimizer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.amazonaws.regions.Regions;
import com.google.gson.Gson;

import amazon.AmazonLoadBalancerSupportTest;
import cloudos.models.AmazonTargetGroup;
import cloudos.optimizer.OptimizerController;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Ignore
public class OptimizerControllerAmazonTargetGroupTest extends AmazonLoadBalancerSupportTest {

  private MockMvc mvc;
  private final String TARGET_GROUP_NAME = String.format("cloudos%s", generator.generate(8));
  private static AmazonTargetGroup amazonTargetGroupRequest = new AmazonTargetGroup();

  @Autowired
  private OptimizerController optimizerController;

  @Before
  public void setUp() throws Exception {
    this.mvc = MockMvcBuilders.standaloneSetup(optimizerController).build();
  }

  @Test
  public void testACreateTargetGroup() throws Exception {
    createVPCSubnets();
    setVPCSubnets();

    amazonTargetGroupRequest = new AmazonTargetGroup();
    amazonTargetGroupRequest.setName(TARGET_GROUP_NAME);
    amazonTargetGroupRequest.setProtocol("HTTP");
    amazonTargetGroupRequest.setPort(80);
    amazonTargetGroupRequest.setTargetType("instance");
    amazonTargetGroupRequest.setVpcId(vpcId);
    amazonTargetGroupRequest.setRegion(Regions.US_EAST_1.getName());

    ResultActions resultActions =
        this.mvc
            .perform(post(OptimizerController.OPTIMIZER + "/target_group/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(amazonTargetGroupRequest.toString()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().is2xxSuccessful());

    String result = resultActions.andReturn().getResponse().getContentAsString();
    amazonTargetGroupRequest = new Gson().fromJson(result, AmazonTargetGroup.class);

    assertNotNull(amazonTargetGroupRequest);

    assertNotNull(amazonTargetGroupRequest.getTargetGroupArn());

  }

  @Test
  public void testBDescribeTargetGroup() throws Exception {

    amazonTargetGroupRequest.setRegion(Regions.US_EAST_1.getName());

    ResultActions resultActions =
        this.mvc
            .perform(post(OptimizerController.OPTIMIZER + "/target_group/describe")
                .contentType(MediaType.APPLICATION_JSON)
                .content(amazonTargetGroupRequest.toString()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().is2xxSuccessful());

    String result = resultActions.andReturn().getResponse().getContentAsString();
    amazonTargetGroupRequest = new Gson().fromJson(result, AmazonTargetGroup.class);
    AmazonTargetGroup amazonTargetGroup = new Gson().fromJson(result, AmazonTargetGroup.class);

    assertNotNull(amazonTargetGroup);
    assertEquals(amazonTargetGroup.getName(), amazonTargetGroupRequest.getName());
    assertEquals(amazonTargetGroup.getProtocol(), amazonTargetGroupRequest.getProtocol());
    assertEquals(amazonTargetGroup.getPort(), amazonTargetGroupRequest.getPort());
    assertEquals(amazonTargetGroup.getTargetType(), amazonTargetGroupRequest.getTargetType());
    assertEquals(amazonTargetGroup.getTargetGroupArn(),
        amazonTargetGroupRequest.getTargetGroupArn());
    assertEquals(amazonTargetGroup.getVpcId(), amazonTargetGroupRequest.getVpcId());

  }

  @Test
  public void testCModifyTargetGroup() throws Exception {

    amazonTargetGroupRequest.setRegion(Regions.US_EAST_1.getName());

    ResultActions resultActions =
        this.mvc
            .perform(post(OptimizerController.OPTIMIZER + "/target_group/modify")
                .contentType(MediaType.APPLICATION_JSON)
                .content(amazonTargetGroupRequest.toString()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().is2xxSuccessful());

    String result = resultActions.andReturn().getResponse().getContentAsString();
    amazonTargetGroupRequest = new Gson().fromJson(result, AmazonTargetGroup.class);
    AmazonTargetGroup amazonTargetGroup = new Gson().fromJson(result, AmazonTargetGroup.class);

    assertNotNull(amazonTargetGroup);
    assertEquals(amazonTargetGroup.getName(), amazonTargetGroupRequest.getName());
    assertEquals(amazonTargetGroup.getProtocol(), amazonTargetGroupRequest.getProtocol());
    assertEquals(amazonTargetGroup.getPort(), amazonTargetGroupRequest.getPort());
    assertEquals(amazonTargetGroup.getTargetType(), amazonTargetGroupRequest.getTargetType());
    assertEquals(amazonTargetGroup.getTargetGroupArn(),
        amazonTargetGroupRequest.getTargetGroupArn());
    assertEquals(amazonTargetGroup.getVpcId(), amazonTargetGroupRequest.getVpcId());

  }

  @Test
  public void testDDeleteTargetGroup() throws Exception {

    amazonTargetGroupRequest.setRegion(Regions.US_EAST_1.getName());

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/target_group/delete")
        .contentType(MediaType.APPLICATION_JSON).content(amazonTargetGroupRequest.toString())
        .accept(MediaType.APPLICATION_JSON)).andExpect(status().is2xxSuccessful());

    amazonTargetGroupRequest.setRegion(Regions.US_EAST_1.getName());

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/target_group/describe")
        .contentType(MediaType.APPLICATION_JSON).content(amazonTargetGroupRequest.toString())
        .accept(MediaType.APPLICATION_JSON)).andExpect(status().is5xxServerError());

    deleteVPCSubnets();
  }

  @Test
  public void testECreateTargetGroupWithWrongAttributes() throws Exception {
    AmazonTargetGroup amazonTargetGroupRequest = new AmazonTargetGroup();
    amazonTargetGroupRequest.setName(null);
    amazonTargetGroupRequest.setProtocol("HTTP");
    amazonTargetGroupRequest.setPort(80);
    amazonTargetGroupRequest.setTargetType("instance");
    amazonTargetGroupRequest.setVpcId(vpcId);
    amazonTargetGroupRequest.setRegion(Regions.US_EAST_1.getName());

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/target_group/create")
        .contentType(MediaType.APPLICATION_JSON).content(amazonTargetGroupRequest.toString())
        .accept(MediaType.APPLICATION_JSON)).andExpect(status().is5xxServerError());

    amazonTargetGroupRequest = new AmazonTargetGroup();
    amazonTargetGroupRequest.setName(TARGET_GROUP_NAME);
    amazonTargetGroupRequest.setProtocol(null);
    amazonTargetGroupRequest.setPort(80);
    amazonTargetGroupRequest.setTargetType("instance");
    amazonTargetGroupRequest.setVpcId(vpcId);
    amazonTargetGroupRequest.setRegion(Regions.US_EAST_1.getName());

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/target_group/create")
        .contentType(MediaType.APPLICATION_JSON).content(amazonTargetGroupRequest.toString())
        .accept(MediaType.APPLICATION_JSON)).andExpect(status().is5xxServerError());

    amazonTargetGroupRequest = new AmazonTargetGroup();
    amazonTargetGroupRequest.setName(TARGET_GROUP_NAME);
    amazonTargetGroupRequest.setProtocol("HTTP");
    amazonTargetGroupRequest.setPort(null);
    amazonTargetGroupRequest.setTargetType("instance");
    amazonTargetGroupRequest.setVpcId(vpcId);
    amazonTargetGroupRequest.setRegion(Regions.US_EAST_1.getName());

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/target_group/create")
        .contentType(MediaType.APPLICATION_JSON).content(amazonTargetGroupRequest.toString())
        .accept(MediaType.APPLICATION_JSON)).andExpect(status().is5xxServerError());

    amazonTargetGroupRequest = new AmazonTargetGroup();
    amazonTargetGroupRequest.setName(TARGET_GROUP_NAME);
    amazonTargetGroupRequest.setProtocol("HTTP");
    amazonTargetGroupRequest.setPort(80);
    amazonTargetGroupRequest.setTargetType("WRONG_INSTANCE");
    amazonTargetGroupRequest.setVpcId(vpcId);
    amazonTargetGroupRequest.setRegion(Regions.US_EAST_1.getName());

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/target_group/create")
        .contentType(MediaType.APPLICATION_JSON).content(amazonTargetGroupRequest.toString())
        .accept(MediaType.APPLICATION_JSON)).andExpect(status().is5xxServerError());

    amazonTargetGroupRequest = new AmazonTargetGroup();
    amazonTargetGroupRequest.setName(TARGET_GROUP_NAME);
    amazonTargetGroupRequest.setProtocol("HTTP");
    amazonTargetGroupRequest.setPort(80);
    amazonTargetGroupRequest.setTargetType("instance");
    amazonTargetGroupRequest.setVpcId(null);
    amazonTargetGroupRequest.setRegion(Regions.US_EAST_1.getName());

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/target_group/create")
        .contentType(MediaType.APPLICATION_JSON).content(amazonTargetGroupRequest.toString())
        .accept(MediaType.APPLICATION_JSON)).andExpect(status().is5xxServerError());

    amazonTargetGroupRequest = new AmazonTargetGroup();
    amazonTargetGroupRequest.setName(TARGET_GROUP_NAME);
    amazonTargetGroupRequest.setProtocol("HTTP");
    amazonTargetGroupRequest.setPort(80);
    amazonTargetGroupRequest.setTargetType("instance");
    amazonTargetGroupRequest.setVpcId(vpcId);
    amazonTargetGroupRequest.setRegion(null);

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/target_group/create")
        .contentType(MediaType.APPLICATION_JSON).content(amazonTargetGroupRequest.toString())
        .accept(MediaType.APPLICATION_JSON)).andExpect(status().is4xxClientError());

    this.mvc
        .perform(post(OptimizerController.OPTIMIZER + "/target_group/create")
            .contentType(MediaType.APPLICATION_JSON).content("").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is4xxClientError());

  }

  @Test
  public void testFDescribeTargetGroupWithWrongAttributes() throws Exception {

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/target_group/describe")
        .contentType(MediaType.APPLICATION_JSON).content(new AmazonTargetGroup().toString())
        .accept(MediaType.APPLICATION_JSON)).andExpect(status().is4xxClientError());

    this.mvc
        .perform(post(OptimizerController.OPTIMIZER + "/target_group/describe")
            .contentType(MediaType.APPLICATION_JSON).content("").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is4xxClientError());

  }

  @Test
  public void testGModifyTargetGroupWithWrongAttributes() throws Exception {

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/target_group/modify")
        .contentType(MediaType.APPLICATION_JSON).content(new AmazonTargetGroup().toString())
        .accept(MediaType.APPLICATION_JSON)).andExpect(status().is4xxClientError());

    this.mvc
        .perform(post(OptimizerController.OPTIMIZER + "/target_group/modify")
            .contentType(MediaType.APPLICATION_JSON).content("").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is4xxClientError());

  }

  @Test
  public void testHDeleteTargetGroupWithWrongAttributes() throws Exception {

    this.mvc.perform(post(OptimizerController.OPTIMIZER + "/target_group/delete")
        .contentType(MediaType.APPLICATION_JSON).content(new AmazonTargetGroup().toString())
        .accept(MediaType.APPLICATION_JSON)).andExpect(status().is4xxClientError());

    this.mvc
        .perform(post(OptimizerController.OPTIMIZER + "/target_group/delete")
            .contentType(MediaType.APPLICATION_JSON).content("").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is4xxClientError());

  }


}
