package optimizer;

import static org.hamcrest.number.OrderingComparison.greaterThanOrEqualTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cloudos.Providers;
import cloudos.machinelearning.autoscaler.ResourceCategory;
import cloudos.models.cloudgroup.CloudGroup;
import cloudos.models.cloudgroup.CloudGroupService;
import cloudos.policies.AutoscalerPolicy;
import cloudos.policies.AutoscalerPolicyEntry;
import cloudos.policies.CloudPolicy;
import cloudos.policies.CloudPolicyService;
import cloudos.policies.PolicyOptimization;
import cloudos.policies.SpotBidderPolicy;
import test.CloudOSTest;

public class CloudPolicyServiceTest extends CloudOSTest {

  @Autowired
  private CloudPolicyService cloudPolicyService;

  @Autowired
  private CloudGroupService cloudGroupService;

  private CloudGroup cloudGroup;

  @Before
  public void setUp() throws Exception {
    this.cloudGroup = CloudGroup.builder()
                                .arn("aws:arn:policy")
                                .min(1)
                                .max(10)
                                .name(generator.generate(8))
                                .provider(Providers.AMAZON_AWS)
                                .build();

    this.cloudGroup = this.cloudGroupService.createCloudGroup(cloudGroup);
  }

  @Test
  public void testAddCloudPolicy() {
    // create a Spot Bidder policy
    SpotBidderPolicy spotBidderPolicy = SpotBidderPolicy.builder()
        .spotInstancesPercentage(0.5)
        .optimization(PolicyOptimization.AVAILABILITY)
        .build();

    spotBidderPolicy.setName(generator.generate(8));

    List<AutoscalerPolicyEntry> entries = Collections.singletonList(
        AutoscalerPolicyEntry.builder()
            .startingGroupSize(1)
            .instanceProtection(false)
            .threshold(0.80)
            .resourceCategory(ResourceCategory.CPU_USAGE)
            .build()
    );
    AutoscalerPolicy autoscalerPolicy = AutoscalerPolicy.builder()
        .autoscalePolicies(entries)
        .build();

    autoscalerPolicy.setName(generator.generate(8));

    // save a spot bidder policy
    CloudPolicy cloudPolicy = this.cloudPolicyService.save(this.cloudGroup.getId(),
        spotBidderPolicy);

    assertNotNull(cloudPolicy);
    assertEquals(cloudPolicy.getName(), spotBidderPolicy.getName());

    // save a autoscaler polcy
    CloudPolicy cloudPolicy1 = this.cloudPolicyService.save(this.cloudGroup.getId(),
        autoscalerPolicy);

    assertNotNull(cloudPolicy1);
    assertEquals(cloudPolicy1.getName(), autoscalerPolicy.getName());
  }

  @Test
  public void testListCloudPolicy() {
    List<CloudPolicy> cloudPolicies = this.cloudPolicyService.list(this.cloudGroup.getId());
    assertNotNull(cloudPolicies);
    assertThat(cloudPolicies.size(), greaterThanOrEqualTo(0));
  }

  @Test
  public void testDescribeCloudPolicy() {
    // create a Spot Bidder policy
    SpotBidderPolicy spotBidderPolicy = SpotBidderPolicy.builder()
        .spotInstancesPercentage(0.5)
        .optimization(PolicyOptimization.AVAILABILITY)
        .build();

    spotBidderPolicy.setName(generator.generate(8));
    // save a spot bidder policy
    CloudPolicy cloudPolicy = this.cloudPolicyService.save(this.cloudGroup.getId(),
        spotBidderPolicy);

    // now describe the policy
    CloudPolicy anotherPolicy = this.cloudPolicyService.describe(this.cloudGroup.getId(),
        cloudPolicy.getId());
    assertNotNull(anotherPolicy);
    assertEquals(anotherPolicy.getName(), cloudPolicy.getName());
  }

  @Test
  public void testUpdateCloudPolicy() {
    // create a Spot Bidder policy
    SpotBidderPolicy spotBidderPolicy = SpotBidderPolicy.builder()
        .spotInstancesPercentage(0.5)
        .optimization(PolicyOptimization.AVAILABILITY)
        .build();

    spotBidderPolicy.setName(generator.generate(8));
    // save a spot bidder policy
    CloudPolicy cloudPolicy = this.cloudPolicyService.save(this.cloudGroup.getId(),
        spotBidderPolicy);

    SpotBidderPolicy anotherPolicy = SpotBidderPolicy.builder()
        .spotInstancesPercentage(0.5)
        .optimization(PolicyOptimization.AVAILABILITY)
        .build();
    anotherPolicy.setName(generator.generate(8));

    // now describe the policy
    CloudPolicy updatedPolicy = this.cloudPolicyService.update(this.cloudGroup.getId(),
        cloudPolicy.getId(), anotherPolicy);

    assertNotNull(updatedPolicy);
    assertEquals(updatedPolicy.getName(), anotherPolicy.getName());
    assertEquals(updatedPolicy.getId(), anotherPolicy.getId());
  }

  @Test
  public void testDeleteCloudPolicy() {
    // create a Spot Bidder policy
    SpotBidderPolicy spotBidderPolicy = SpotBidderPolicy.builder()
        .spotInstancesPercentage(0.5)
        .optimization(PolicyOptimization.AVAILABILITY)
        .build();

    spotBidderPolicy.setName(generator.generate(8));
    // save a spot bidder policy
    CloudPolicy cloudPolicy = this.cloudPolicyService.save(this.cloudGroup.getId(),
        spotBidderPolicy);

    // delete the cloud policy now
    assertTrue(this.cloudPolicyService.delete(this.cloudGroup.getId(), cloudPolicy.getId()));
    // describe it, it should be gone
    assertNull(this.cloudPolicyService.describe(this.cloudGroup.getId(), cloudPolicy.getId()));
    // make sure the cloud policy has a null spot policy
    assertNull(this.cloudGroup.getSpotBidderPolicy());
  }

  @After
  public void tearDown() throws Exception {
    if (cloudGroup != null) {
      this.cloudGroupService.deleteCloudGroup(cloudGroup.getId());
    }
  }
}
