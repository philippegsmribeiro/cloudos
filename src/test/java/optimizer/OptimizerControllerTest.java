package optimizer;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.type.TypeReference;

import cloudos.Providers;
import cloudos.instances.InstanceService;
import cloudos.machinelearning.autoscaler.ResourceCategory;
import cloudos.models.Instance;
import cloudos.models.cloudgroup.CloudGroup;
import cloudos.models.cloudgroup.CloudGroupFilterRequest;
import cloudos.models.cloudgroup.CloudGroupService;
import cloudos.optimizer.CostIncreaseResponse;
import cloudos.optimizer.OptimizerController;
import cloudos.optimizer.models.CloudEquivalenceRequest;
import cloudos.policies.AutoscalerPolicy;
import cloudos.policies.AutoscalerPolicyEntry;
import cloudos.policies.CloudPolicy;
import cloudos.policies.CloudPolicyService;
import cloudos.policies.PolicyOptimization;
import cloudos.policies.SpotBidderPolicy;
import test.CloudOSTest;
import test.SlowTest;

@Category(SlowTest.class)
public class OptimizerControllerTest extends CloudOSTest {

  @Autowired
  private OptimizerController optimizerController;

  @Autowired
  private CloudGroupService cloudGroupService;

  @Autowired
  private CloudPolicyService cloudPolicyService;

  @Autowired
  InstanceService instanceService;

  private MockMvc mvc;

  private CloudGroup cloudGroup;

  @Before
  public void setUp() throws Exception {
    this.mvc = MockMvcBuilders.standaloneSetup(optimizerController).build();
    this.cloudGroup = CloudGroup.builder().provider(Providers.AMAZON_AWS)
        .name("AWSCloudGroupTest_" + generator.generate(6)).max(12).build();
    // save it in the repository
    this.cloudGroupService.createCloudGroup(cloudGroup);
  }

  @Test
  public void testController() throws Exception {
    this.mvc.perform(get(OptimizerController.OPTIMIZER + "/"))
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testGetInstances() throws Exception {
    byte[] contentAsByteArray = this.mvc
        .perform(get(OptimizerController.OPTIMIZER + "/count/instances/"))
        .andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsByteArray();
    Integer result = super.mapper.readValue(contentAsByteArray, Integer.class);
    Assert.assertNotNull(result);
  }

  @Test
  public void testGetInstancesByProvider() throws Exception {
    Providers providers = Providers.GOOGLE_COMPUTE_ENGINE;
    byte[] contentAsByteArray = this.mvc
        .perform(get(OptimizerController.OPTIMIZER + "/count/instances/" + providers))
        .andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsByteArray();
    Integer result = super.mapper.readValue(contentAsByteArray, Integer.class);
    Assert.assertNotNull(result);
  }

  @Test
  public void testCostIncrease() throws Exception {
    byte[] contentAsByteArray = this.mvc
        .perform(get(OptimizerController.OPTIMIZER + "/costIncrease/"))
        .andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsByteArray();
    CostIncreaseResponse result =
        super.mapper.readValue(contentAsByteArray, CostIncreaseResponse.class);
    Assert.assertNotNull(result);
    Assert.assertNotNull(result.getLastMonthTotal());
    Assert.assertNotNull(result.getMonthToDateTotal());
    Assert.assertNotNull(result.getForecastTotal());
  }

  @Test
  public void testCostIncreaseByProvider() throws Exception {
    Providers providers = Providers.GOOGLE_COMPUTE_ENGINE;
    byte[] contentAsByteArray = this.mvc
        .perform(get(OptimizerController.OPTIMIZER + "/costIncrease/" + providers))
        .andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsByteArray();
    CostIncreaseResponse result =
        super.mapper.readValue(contentAsByteArray, CostIncreaseResponse.class);
    Assert.assertNotNull(result);
    Assert.assertNotNull(result.getLastMonthTotal());
    Assert.assertNotNull(result.getMonthToDateTotal());
    Assert.assertNotNull(result.getForecastTotal());
  }

  @Test
  public void testCostByService() throws Exception {
    byte[] contentAsByteArray = this.mvc
        .perform(get(OptimizerController.OPTIMIZER + "/costsByService/"))
        .andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsByteArray();
    Map<String, Double> result =
        super.mapper.readValue(contentAsByteArray, new TypeReference<Map<String, Double>>() {});
    Assert.assertNotNull(result);
  }

  @Test
  public void testCostByServiceByProvider() throws Exception {
    Providers providers = Providers.GOOGLE_COMPUTE_ENGINE;
    byte[] contentAsByteArray = this.mvc
        .perform(get(OptimizerController.OPTIMIZER + "/costsByService/" + providers))
        .andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsByteArray();
    Map<String, Double> result =
        super.mapper.readValue(contentAsByteArray, new TypeReference<Map<String, Double>>() {});
    Assert.assertNotNull(result);
  }

  @Test
  public void testCloudCostComparison() throws Exception {
    CloudEquivalenceRequest equivalence = CloudEquivalenceRequest.builder()
                                                .region("us-east1")
                                                .fromProvider(Providers.GOOGLE_COMPUTE_ENGINE)
                                                .toProvider(Providers.AMAZON_AWS).build();
    this.mvc
        .perform(
            get(OptimizerController.OPTIMIZER + "")
                .contentType(MediaType.APPLICATION_JSON)
                .content(equivalence.toString())
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testCreateCloudGroup() throws Exception {
    // =========== Create default Cloud Group =============
    CloudGroup cloudGroup1 = CloudGroup.builder().provider(Providers.AMAZON_AWS).build();

    String writeValueAsString = mapper.writeValueAsString(cloudGroup1);
    logger.debug(writeValueAsString);
    this.mvc
        .perform(post(OptimizerController.OPTIMIZER + "/cloud_group")
            .contentType(MediaType.APPLICATION_JSON).content(writeValueAsString)
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated());

    // =========== Create Cloud Group without cloud groups =========
    CloudGroup cloudGroup2 = CloudGroup.builder().provider(Providers.AMAZON_AWS)
        .name("AWSCloudGroupTest_" + generator.generate(6))
        .max(12)
        .min(1)
        .build();

    writeValueAsString = mapper.writeValueAsString(cloudGroup2);
    logger.debug(writeValueAsString);
    this.mvc
        .perform(post(OptimizerController.OPTIMIZER + "/cloud_group")
            .contentType(MediaType.APPLICATION_JSON).content(writeValueAsString)
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated()).andExpect(jsonPath("$.name", is(cloudGroup2.getName())))
        .andExpect(jsonPath("$.max", is(cloudGroup2.getMax())));

    // create a Spot Bidder policy
    SpotBidderPolicy spotBidderPolicy = SpotBidderPolicy.builder()
        .spotInstancesPercentage(0.5)
        .optimization(PolicyOptimization.AVAILABILITY)
        .build();

    List<AutoscalerPolicyEntry> entries = Collections.singletonList(
        AutoscalerPolicyEntry.builder()
            .startingGroupSize(1)
            .instanceProtection(false)
            .threshold(0.80)
            .resourceCategory(ResourceCategory.CPU_USAGE)
            .build()
    );

    AutoscalerPolicy autoscalerPolicy = AutoscalerPolicy.builder()
        .autoscalePolicies(entries)
        .build();

    CloudGroup cloudGroup3 = CloudGroup.builder().provider(Providers.AMAZON_AWS)
        .name("AWSCloudGroupTest_" + generator.generate(6)).max(12)
        .spotBidderPolicy(spotBidderPolicy)
        .autoscalerPolicy(autoscalerPolicy)
        .build();

    writeValueAsString = mapper.writeValueAsString(cloudGroup3);
    logger.debug(writeValueAsString);
    this.mvc
        .perform(post(OptimizerController.OPTIMIZER + "/cloud_group")
            .contentType(MediaType.APPLICATION_JSON).content(writeValueAsString)
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated()).andExpect(jsonPath("$.name", is(cloudGroup3.getName())))
        .andExpect(jsonPath("$.max", is(cloudGroup3.getMax())));

    // attempt to create the same cloud group
    this.mvc
        .perform(post(OptimizerController.OPTIMIZER + "/cloud_group")
            .contentType(MediaType.APPLICATION_JSON).content(writeValueAsString)
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is5xxServerError());

    // delete group group
    CloudGroup group = this.cloudGroupService.describeCloudGroupByName(cloudGroup3.getName());
    this.cloudGroupService.deleteCloudGroup(group.getId());
  }

  @Test
  public void testListCloudGroup() throws Exception {
    this.mvc.perform(get(OptimizerController.OPTIMIZER + "/cloud_group").contentType(contentType))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDescribeCloudGroup() throws Exception {
    // delete a cloud group that exists
    CloudGroup group = this.cloudGroupService.describeCloudGroupByName(this.cloudGroup.getName());
    this.mvc
        .perform(get(OptimizerController.OPTIMIZER + "/cloud_group/" + group.getId())
            .contentType(contentType))
        .andExpect(status().is2xxSuccessful())
        .andExpect(jsonPath("$.name", is(cloudGroup.getName())))
        .andExpect(jsonPath("$.max", is(cloudGroup.getMax())));

    // attempt to delete a CloudGroup that does not exist
    this.mvc.perform(
        get(OptimizerController.OPTIMIZER + "/cloud_group/invalidgroupid").contentType(contentType))
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteCloudGroup() throws Exception {
    // delete a cloud group that exists
    CloudGroup group = this.cloudGroupService.describeCloudGroupByName(this.cloudGroup.getName());
    this.mvc.perform(delete(OptimizerController.OPTIMIZER + "/cloud_group/" + group.getId())
        .contentType(contentType)).andExpect(status().is2xxSuccessful());

    // attempt to delete a CloudGroup that does not exist
    this.mvc.perform(delete(OptimizerController.OPTIMIZER + "/cloud_group/invalidgroupid")
        .contentType(contentType)).andExpect(status().isNotFound());
  }

  @Test
  public void testAddInstancesToCloudGroup() throws Exception {
    // instances
    List<Instance> instances =
        instanceService.findAllActiveAndNotStoppedInstancesByProvider(Providers.AMAZON_AWS);
    //
    if (CollectionUtils.isNotEmpty(instances)) {
      List<String> instancesIds =
          instances.stream().map(i -> i.getId()).collect(Collectors.toList());
      // add instance to a cloud group
      CloudGroup group = this.cloudGroupService.describeCloudGroupByName(this.cloudGroup.getName());
      this.mvc.perform(
          post(OptimizerController.OPTIMIZER + "/cloud_group/" + group.getId() + "/addInstance/")
              .contentType(MediaType.APPLICATION_JSON)
              .content(mapper.writeValueAsString(instancesIds)).accept(MediaType.APPLICATION_JSON))
          .andExpect(status().is2xxSuccessful());
    }
  }

  @Test
  public void testRemoveInstancesFromCloudGroup() throws Exception {
    // instances
    List<Instance> instances =
        instanceService.findAllActiveAndNotStoppedInstancesByProvider(Providers.AMAZON_AWS);
    //
    if (CollectionUtils.isNotEmpty(instances)) {
      List<String> instancesIds =
          instances.stream().map(i -> i.getId()).collect(Collectors.toList());
      // add instance to a cloud group
      CloudGroup group = this.cloudGroupService.describeCloudGroupByName(this.cloudGroup.getName());
      //
      this.cloudGroupService.addInstanceToCloudGroup(group.getId(), instancesIds);
      this.mvc.perform(
          post(OptimizerController.OPTIMIZER + "/cloud_group/" + group.getId() + "/removeInstance/")
              .contentType(MediaType.APPLICATION_JSON)
              .content(mapper.writeValueAsString(instancesIds)).accept(MediaType.APPLICATION_JSON))
          .andExpect(status().is2xxSuccessful());
    }
  }

  @Test
  public void testRetrieveCloudGroupInstances() throws Exception {
    List<Instance> instances =
        instanceService.findAllActiveAndNotStoppedInstancesByProvider(Providers.AMAZON_AWS);
    //
    if (CollectionUtils.isNotEmpty(instances)) {
      List<String> instancesIds =
          instances.stream().map(i -> i.getId()).collect(Collectors.toList());
      // add instance to a cloud group
      CloudGroup group = this.cloudGroupService.describeCloudGroupByName(this.cloudGroup.getName());
      //
      this.cloudGroupService.addInstanceToCloudGroup(group.getId(), instancesIds);
      byte[] content = this.mvc
          .perform(
              get(OptimizerController.OPTIMIZER + "/cloud_group/" + group.getId() + "/instances")
                  .contentType(contentType))
          .andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsByteArray();

      List<Instance> instancesReturned =
          mapper.readValue(content, new TypeReference<List<Instance>>() {});
      Assert.assertEquals(instances.size(), instancesReturned.size());
    }

  }

  @Test
  public void testGetInstancesWithoutCloudGroup() throws Exception {
    // negative test: find with without a provider provider
    CloudGroupFilterRequest request = CloudGroupFilterRequest
                                          .builder()
                                          .build();
    this.mvc
        .perform(
            post(OptimizerController.OPTIMIZER + "/cloud_group/getInstancesWithoutCloudGroup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(request)))
        .andExpect(status().is4xxClientError());

    request = CloudGroupFilterRequest
                      .builder()
                      .provider(Providers.AMAZON_AWS)
                      .build();

    this.mvc
        .perform(
            post(OptimizerController.OPTIMIZER + "/cloud_group/getInstancesWithoutCloudGroup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(request)))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testSaveCloudPolicy() throws Exception {
    // test saving a Cloud Policy
    SpotBidderPolicy spotBidderPolicy = SpotBidderPolicy.builder()
        .spotInstancesPercentage(0.5)
        .optimization(PolicyOptimization.AVAILABILITY)
        .build();

    spotBidderPolicy.setName(generator.generate(8));
    spotBidderPolicy.setCloudGroupId(this.cloudGroup.getId());

    logger.debug("Spotbidder Policy: {}", spotBidderPolicy);

    this.mvc.perform(
        post(OptimizerController.OPTIMIZER + "/cloud_group/" + this.cloudGroup.getId() + "/policies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(mapper.writeValueAsString(spotBidderPolicy))
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is2xxSuccessful());

    // now save a Autoscaler Policy
    List<AutoscalerPolicyEntry> entries = Collections.singletonList(
        AutoscalerPolicyEntry.builder()
            .startingGroupSize(1)
            .instanceProtection(false)
            .threshold(0.80)
            .resourceCategory(ResourceCategory.CPU_USAGE)
            .build()
    );

    AutoscalerPolicy autoscalerPolicy = AutoscalerPolicy.builder()
        .autoscalePolicies(entries)
        .build();

    logger.debug("Autoscaler Policy: {}", autoscalerPolicy);

    this.mvc.perform(
        post(OptimizerController.OPTIMIZER + "/cloud_group/" + this.cloudGroup.getId() + "/policies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(mapper.writeValueAsString(autoscalerPolicy))
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testListCloudPolicies() throws Exception {
    this.mvc.perform(
        get(OptimizerController.OPTIMIZER + "/cloud_group/" + this.cloudGroup.getId() + "/policies")
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDescribeCloudPolicies() throws Exception {
    SpotBidderPolicy spotBidderPolicy = SpotBidderPolicy.builder()
        .spotInstancesPercentage(0.5)
        .optimization(PolicyOptimization.AVAILABILITY)
        .build();

    spotBidderPolicy.setName(generator.generate(8));
    spotBidderPolicy.setCloudGroupId(this.cloudGroup.getId());

    // create a new entry in the collection
    spotBidderPolicy = (SpotBidderPolicy) this.cloudPolicyService.save(this.cloudGroup.getId(), spotBidderPolicy);

    // attempt to describe it
    this.mvc.perform(
        get(OptimizerController.OPTIMIZER + "/cloud_group/" + this.cloudGroup.getId() +
            "/policies/" + spotBidderPolicy.getId())
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is2xxSuccessful())
        .andExpect(jsonPath("$.name", is(spotBidderPolicy.getName())));
  }

  @Test
  public void testDeleteCloudPolicy() throws Exception {
    SpotBidderPolicy spotBidderPolicy = SpotBidderPolicy.builder()
        .spotInstancesPercentage(0.5)
        .optimization(PolicyOptimization.AVAILABILITY)
        .build();

    spotBidderPolicy.setName(generator.generate(8));
    spotBidderPolicy.setCloudGroupId(this.cloudGroup.getId());

    // create a new entry in the collection
    spotBidderPolicy = (SpotBidderPolicy) this.cloudPolicyService.save(this.cloudGroup.getId(), spotBidderPolicy);

    this.mvc.perform(
        delete(OptimizerController.OPTIMIZER + "/cloud_group/" + this.cloudGroup.getId() +
            "/policies/" + spotBidderPolicy.getId())
          .contentType(contentType))
        .andExpect(status().is2xxSuccessful());

    // attempt to delete a CloudGroup that does not exist
    this.mvc.perform(
        delete(OptimizerController.OPTIMIZER + "/cloud_group/" + this.cloudGroup.getId() +
            "/policies/invalid")
        .contentType(contentType))
        .andExpect(status().isNotFound());
  }

  @Test
  public void testUpdateCloudPolicy() throws Exception {
    // create a Spot Bidder policy
    SpotBidderPolicy spotBidderPolicy = SpotBidderPolicy.builder()
        .spotInstancesPercentage(0.5)
        .optimization(PolicyOptimization.AVAILABILITY)
        .build();

    spotBidderPolicy.setName(generator.generate(8));
    // save a spot bidder policy
    CloudPolicy cloudPolicy = this.cloudPolicyService.save(this.cloudGroup.getId(),
        spotBidderPolicy);

    SpotBidderPolicy anotherPolicy = SpotBidderPolicy.builder()
        .spotInstancesPercentage(0.5)
        .optimization(PolicyOptimization.AVAILABILITY)
        .build();
    anotherPolicy.setName(generator.generate(8));

    this.mvc.perform(
        put(OptimizerController.OPTIMIZER + "/cloud_group/" + this.cloudGroup.getId() +
            "/policies/" + spotBidderPolicy.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(mapper.writeValueAsString(anotherPolicy))
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().is2xxSuccessful())
        .andExpect(jsonPath("$.name", is(anotherPolicy.getName())));
  }

  @After
  public void tearDown() throws Exception {
    // delete group group
    CloudGroup group = this.cloudGroupService.describeCloudGroupByName(this.cloudGroup.getName());
    if (group != null) {
      this.cloudGroupService.deleteCloudGroup(group.getId());
    }
  }
}
