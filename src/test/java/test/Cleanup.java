package test;

import cloudos.AmazonService;
import cloudos.models.AwsKeyPair;
import cloudos.models.network.AwsVpc;
import cloudos.models.network.CloudosVpc;
import cloudos.network.AmazonVpcManagement;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.SecurityGroup;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class Cleanup extends CloudOSTest {

  @Autowired
  private AmazonVpcManagement amazonVpcManagement;

  @Autowired
  private AmazonService amazonService;

  private static final String defaultSecurityGroup = "default";
  private static final String defaultKey = "gcloudsshkey";

  @Test
  public void testCleanup() {
    logger.info("**************** Cleaning up Resources ***************");
    logger.info("##############Cleaning up instances...");
    this.cleanupInstances();
    logger.info("##############Cleaning up VPCs...");
    this.cleanupAllNonDefaultVpcs();
    logger.info("##############Cleaning up Security Groups....");
    this.cleanupAllNonDefaultSecurityGroups();
    logger.info("##############Cleaning up keys....");
    this.cleanupAllNonDefaultKeys();
    logger.info("********************* Done ***************************");
  }

  private void cleanupAllNonDefaultKeys() {
    Regions[] regions = Regions.values();
    for (Regions region : regions) {
      try {
        logger.info(region.getName());
        List<AwsKeyPair> keys = this.amazonService.listKeyPairs(region.getName());
        keys.forEach(key -> {
          if (!key.getKeyname().equals(defaultKey)) {
            this.amazonService.deleteKeyPair(key);
          }
        });
      } catch (Exception e) {
        logger.error(e.getMessage(),e);
      }
    }
  }

  private void cleanupAllNonDefaultSecurityGroups() {
    Regions[] regions = Regions.values();
    for (Regions region : regions) {
      try {
        logger.info(region.getName());
        List<SecurityGroup> securityGroups = this.amazonService.listSecurityGroups(region.getName());
        if (securityGroups != null) {
          securityGroups.forEach(securityGroup -> {
            if (!securityGroup.getGroupName().equals(defaultSecurityGroup)) {
              this.amazonService.deleteSecurityGroup(region.getName(), securityGroup.getGroupId());
            }
          });
        }
      } catch (Exception e) {
        logger.error(e.getMessage(),e);
      }
    }
  }

  /**
   * Remove all the instances, for all the regions of the region
   */
  private void cleanupInstances() {
    Regions[] regions = Regions.values();
    for (Regions region : regions) {
      logger.info(region.getName());
      Set<Instance> instances = this.amazonService.listInstances(region.getName(), defaultSecurityGroup);
      List<String> list =
          instances.stream().map(Instance::getInstanceId).collect(Collectors.toList());
      this.amazonService.terminateInstances(list, region.getName(), defaultSecurityGroup);
    }
  }

  /**
   * Remove all non-default VPCs
   */
  private void cleanupAllNonDefaultVpcs() {
    Regions[] values = Regions.values();
    for (Regions regions : values) {
      try {
        logger.info(regions.getName());
        List<? extends CloudosVpc> vpcList = this.amazonVpcManagement.list(regions.getName());
        for (CloudosVpc cloudosVpc : vpcList) {
          try {
            if(!((AwsVpc) cloudosVpc).getIsDefault()) {
              logger.info("DELETING: {}", cloudosVpc);
              amazonVpcManagement.delete((AwsVpc) cloudosVpc);
            }
          } catch (Exception e) {
            logger.error(e.getMessage(),e);
          }
        }
      } catch (Exception e) {
        logger.error(e.getMessage(),e);
      }
    }
  }

}
