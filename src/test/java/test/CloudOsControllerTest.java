package test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.nio.charset.Charset;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Auxiliary class for Controller tests.
 * The controller test should just test the rest api.
 * The call to the services inside the controller should be mocked.
 */
@RunWith(BlockJUnit4ClassRunner.class)
@SpringBootTest(properties = "spring.profiles.include=test")
public abstract class CloudOsControllerTest {
  
  protected ObjectMapper mapper = new ObjectMapper();

  protected MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
      MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

  protected MockHttpServletResponse executeGet(MockMvc mvc, String path, Object...params) throws Exception {
    return execute(RequestMethod.GET, mvc, path, null, params);
  }

  protected MockHttpServletResponse executePost(MockMvc mvc, String path, Object json, Object...params)
      throws Exception {
    return execute(RequestMethod.POST, mvc, path, json, params);
  }

  protected MockHttpServletResponse executePut(MockMvc mvc, String path, Object json, Object...params)
      throws Exception {
    return execute(RequestMethod.PUT, mvc, path, json, params);
  }

  protected MockHttpServletResponse executeDelete(MockMvc mvc, String path, Object json, Object...params)
      throws Exception {
    return execute(RequestMethod.DELETE, mvc, path, json, params);
  }

  private MockHttpServletResponse execute(RequestMethod method, MockMvc mvc, String path,
      Object json, Object ... params)
      throws Exception {
    MockHttpServletRequestBuilder action = null;
    switch (method) {
      case GET:
        action = get(path, params);
        break;
      case POST:
        action = post(path, params);
        break;
      case DELETE:
        action = delete(path, params);
        break;
      case PUT:
        action = put(path, params);
        break;
      default:
        action = get(path, params);
        break;
    }
    ResultActions perform = null;
    if (json == null) {
      perform = mvc.perform(action);
    } else {
      byte[] writeValueAsBytes = mapper.writeValueAsBytes(json);
      perform = mvc.perform(action.content(writeValueAsBytes).contentType(contentType));
    }
    return perform.andReturn().getResponse();
  }
  
}
