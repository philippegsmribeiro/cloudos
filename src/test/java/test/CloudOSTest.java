package test;

import cloudos.Application;
import cloudos.config.ApplicationTestContext;
import cloudos.security.RequestScope;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import org.apache.commons.text.RandomStringGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.datavec.api.util.ClassPathResource;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {Application.class})
@Import(ApplicationTestContext.class)
@SpringBootTest(properties = "spring.profiles.include=test")
public abstract class CloudOSTest {
  
  // log
  protected Logger logger = LogManager.getLogger(this.getClass());

  protected HttpMessageConverter mappingJackson2HttpMessageConverter;

  // contentType
  protected MediaType contentType =
      new MediaType(
          MediaType.APPLICATION_JSON.getType(),
          MediaType.APPLICATION_JSON.getSubtype(),
          Charset.forName("utf8"));

  // gson
  protected Gson gson = new Gson();

  // mapper
  @Autowired
  protected ObjectMapper mapper;

  @MockBean
  protected RequestScope requestScope;

  @Autowired
  private MongoTemplate mongoTemplate;

  static protected RandomStringGenerator generator = new RandomStringGenerator.Builder()
      .withinRange('a', 'z').build();

  protected String json(Object o) throws IOException {
    MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
    this.mappingJackson2HttpMessageConverter.write(
        o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
    return mockHttpOutputMessage.getBodyAsString();
  }

  /**
   * Import a JSON file into a collection.
   *
   * @param collection
   * @param file
   * @throws IOException 
   * @throws DuplicateKeyException 
   */
  protected void importJSON(String collection, String file) throws DuplicateKeyException, IOException {

    try {
      final String path =
          new ClassPathResource(file)
              .getFile()
              .getPath();
      BufferedReader reader = new BufferedReader(new FileReader(path));
      String json;
      while ((json = reader.readLine()) != null) {
        mongoTemplate.insert(Document.parse(json), collection);
      }
      reader.close();
    } catch (DuplicateKeyException e) {
      throw (DuplicateKeyException) e;
    } catch (IOException e) {
      logger.error("Could not import file: {}", file, e);
      throw e;
    }
  }

}
