package feature.settings;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.junit.BeforeClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.google.gson.Gson;

import cloudos.Providers;
import cloudos.SettingsController;
import cloudos.models.CloudosAccount;
import cloudos.models.CloudosTenant;
import cloudos.user.UserPreferences;
import cloudos.user.UserProfile;
import cloudos.user.User;
import cloudos.user.UserUpdateRequest;
import cloudos.user.UserService;
import cloudos.utils.ReflectionToJson;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java8.En;
import test.CloudOSTest;

public class SettingsControllerSteps extends CloudOSTest implements En {

  private static MockMvc mockMvc;
  private static String url;
  private static String username = null;
  private static CloudosTenant cloudosTenant = new CloudosTenant();
  private static UserProfile userProfile;

  @Autowired
  private UserService userService;

  @Autowired
  private SettingsController settingsController;

  @BeforeClass
  public void setUp() throws Exception {
    mockMvc = MockMvcBuilders.standaloneSetup(this.settingsController).build();
  }


  @When("^Update an existing tenant")
  public void craeteNewTenant() throws Exception {
    url = SettingsController.SETTINGS + "/save_tenant";
    mockMvc = MockMvcBuilders.standaloneSetup(this.settingsController).build();
  }

  @Then("^I should update the tenant attributes")
  public void updateExistingTenant() throws Exception {

    createTenantRequest();

    MockHttpServletRequestBuilder request = post(url).contentType(MediaType.APPLICATION_JSON)
        .content(ReflectionToJson.toString(cloudosTenant));

    ResultActions resultActions = mockMvc.perform(request).andExpect(status().isOk());
    String json = resultActions.andReturn().getResponse().getContentAsString();
    cloudosTenant = new Gson().fromJson(json, CloudosTenant.class);

    assertNotNull(cloudosTenant);
    assertNotNull(cloudosTenant.getId());

    // update attribute values
    createTenantRequest();

    request = post(url).contentType(MediaType.APPLICATION_JSON)
        .content(ReflectionToJson.toString(cloudosTenant));

    resultActions = mockMvc.perform(request).andExpect(status().isOk());
    json = resultActions.andReturn().getResponse().getContentAsString();
    CloudosTenant updatedCloudosTenant = new Gson().fromJson(json, CloudosTenant.class);

    assertEquals(cloudosTenant.getId(), updatedCloudosTenant.getId());
    assertEquals(cloudosTenant.getAccountName(), updatedCloudosTenant.getAccountName());
    assertEquals(cloudosTenant.getDomainName(), updatedCloudosTenant.getDomainName());
    assertEquals(cloudosTenant.getRegion(), updatedCloudosTenant.getRegion());
    assertEquals(cloudosTenant.getCompanySize(), updatedCloudosTenant.getCompanySize());
    assertEquals(cloudosTenant.getCompanyName(), updatedCloudosTenant.getCompanyName());
    assertEquals(cloudosTenant.getUsers().size(), updatedCloudosTenant.getUsers().size());
    assertEquals(cloudosTenant.getUsers().get(0).getId(),
        updatedCloudosTenant.getUsers().get(0).getId());

  }

  @Then("^I shouldn't update the tenant attributes")
  public void cantUpdateExistingTenant() throws Exception {

    // update attribute values
    createTenantRequest();
    cloudosTenant.setDomainName(null);

    MockHttpServletRequestBuilder request = post(url).contentType(MediaType.APPLICATION_JSON)
        .content(ReflectionToJson.toString(cloudosTenant));

    mockMvc.perform(request).andExpect(status().isInternalServerError());

  }

  @When("^I search an existing tenant by username")
  public void searchTenantByUSername() throws Exception {
    username = cloudosTenant.getUsers().get(0).getUsername();
    url = SettingsController.SETTINGS + "/get_tenant_by_username/" + username;
    mockMvc = MockMvcBuilders.standaloneSetup(this.settingsController).build();
  }

  @Then("^I should find a tenant searching by username")
  public void findTenantByUSername() throws Exception {

    ResultActions resultActions = mockMvc.perform(get(url)).andExpect(status().isOk());
    String json = resultActions.andReturn().getResponse().getContentAsString();
    cloudosTenant = new Gson().fromJson(json, CloudosTenant.class);

    assertNotNull(cloudosTenant);
    assertNotNull(cloudosTenant.getId());

    boolean usernameFound = false;
    for (User user : cloudosTenant.getUsers()) {
      if (user.getUsername().equals(username)) {
        usernameFound = true;
      }
    }

    assertTrue(usernameFound);

  }

  @When("^I search a tenant by wrong username")
  public void searchTenantByWrongUsername() throws Exception {
    username = cloudosTenant.getUsers().get(0).getUsername();
    url =
        SettingsController.SETTINGS + "/get_tenant_by_username/" + username + generator.generate(5);
    mockMvc = MockMvcBuilders.standaloneSetup(this.settingsController).build();
  }

  @Then("^I shouldn't find the tenant searching by wrong username")
  public void cantFindTenantByWrongUsername() throws Exception {
    mockMvc.perform(get(url)).andExpect(status().isInternalServerError());
  }

  @When("^I update the user's password")
  public void post_update_password() throws Exception {
    url = SettingsController.SETTINGS + "/update_password";
    mockMvc = MockMvcBuilders.standaloneSetup(this.settingsController).build();
  }

  @Then("^I should update the password")
  public void make_post_update_password() throws Exception {

    userService.save(new User("Rogerio", "Souza", "rosouza", "pass1", "rosouza@cloudos.com"));

    UserUpdateRequest userUpdateRequest = new UserUpdateRequest();
    userUpdateRequest.setUsername("rosouza");
    userUpdateRequest.setOldPassword("pass1");
    userUpdateRequest.setNewPassword("pass2");

    MockHttpServletRequestBuilder request = post(url).contentType(MediaType.APPLICATION_JSON)
        .content(ReflectionToJson.toString(userUpdateRequest));

    mockMvc.perform(request).andExpect(status().isOk());
  }

  @Then("^I shouldn't update the password")
  public void cantUpdatePassword() throws Exception {

    UserUpdateRequest userUpdateRequest = new UserUpdateRequest();
    userUpdateRequest.setUsername("rosouza");
    userUpdateRequest.setOldPassword("pass1");
    userUpdateRequest.setNewPassword("pass2");

    MockHttpServletRequestBuilder request = post(url).contentType(MediaType.APPLICATION_JSON)
        .content(ReflectionToJson.toString(userUpdateRequest));

    mockMvc.perform(request).andExpect(status().isInternalServerError());
  }

  private void createTenantRequest() {
    cloudosTenant.setAccountName(generator.generate(10));
    cloudosTenant.setDomainName(generator.generate(10));
    cloudosTenant.setRegion(generator.generate(10));
    cloudosTenant.setCompanySize(generator.generate(10));
    cloudosTenant.setCompanyName(generator.generate(10));

    setUser();
    createAccountAndCredentials();

  }

  private void setUser() {
    List<User> users = userService.retrieveAllUsers();
    if (!users.isEmpty()) {
      User user = users.get(0);
      user.setProfile(createUserProfile());
      cloudosTenant.getUsers().add(user);
    } else {
      fail("No user found!");
    }
  }

  private UserProfile createUserProfile() {

    UserProfile profile = new UserProfile();
    profile.setFirstName(generator.generate(10));
    profile.setLastName(generator.generate(10));
    profile.setJobRole(generator.generate(10));
    profile.setCountry(generator.generate(10));
    profile.setState(generator.generate(10));
    profile.setCity(generator.generate(10));
    profile.setTelephone(generator.generate(10));
    profile.setPicture(generator.generate(10));

    UserPreferences preferences = new UserPreferences();
    preferences.setDateFormat(generator.generate(10));
    preferences.setTimeFormat(generator.generate(10));
    preferences.setAlertNotifications(true);
    preferences.setEmailNotification(true);
    preferences.setUse24HourFormat(true);
    preferences.setUseSSL(true);

    profile.setPreferences(preferences);
    return profile;

  }

  private void createAccountAndCredentials() {
    CloudosAccount account = new CloudosAccount();
    account.setAccountId(generator.generate(10));
    account.setAccountName(generator.generate(10));
    account.setProvider(Providers.AMAZON_AWS);
    cloudosTenant.getAccounts().add(account);
  }

  @When("^I update a user profile")
  public void updateUserProfile() throws Exception {
    this.username = userService.retrieveAllUsers().get(0).getUsername();
    url = SettingsController.SETTINGS + "/update_user_profile/" + this.username;
    mockMvc = MockMvcBuilders.standaloneSetup(this.settingsController).build();

    this.userProfile = createUserProfile();

    MockHttpServletRequestBuilder request = post(url).contentType(MediaType.APPLICATION_JSON)
        .content(ReflectionToJson.toString(userProfile));

    mockMvc.perform(request).andExpect(status().isOk());

  }

  @When("^I should be able to retrieve the updated user profile")
  public void getUpdatedUserProfile() throws Exception {
    url = SettingsController.SETTINGS + "/get_user_profile/" + this.username;
    mockMvc = MockMvcBuilders.standaloneSetup(this.settingsController).build();

    ResultActions resultActions = mockMvc.perform(get(url)).andExpect(status().isOk());

    String json = resultActions.andReturn().getResponse().getContentAsString();
    UserProfile savedProfile = new Gson().fromJson(json, UserProfile.class);

    assertEquals(userProfile.getFirstName(), savedProfile.getFirstName());
    assertEquals(userProfile.getLastName(), savedProfile.getLastName());
    assertEquals(userProfile.getJobRole(), savedProfile.getJobRole());
    assertEquals(userProfile.getCountry(), savedProfile.getCountry());
    assertEquals(userProfile.getState(), savedProfile.getState());
    assertEquals(userProfile.getCity(), savedProfile.getCity());
    assertEquals(userProfile.getTelephone(), savedProfile.getTelephone());
    assertEquals(userProfile.getPicture(), savedProfile.getPicture());

    UserPreferences preferences = userProfile.getPreferences();
    UserPreferences savedPreferences = savedProfile.getPreferences();
    assertEquals(preferences.getDateFormat(), savedPreferences.getDateFormat());
    assertEquals(preferences.getTimeFormat(), savedPreferences.getTimeFormat());
    assertEquals(preferences.isAlertNotifications(), savedPreferences.isAlertNotifications());
    assertEquals(preferences.isEmailNotification(), savedPreferences.isEmailNotification());
    assertEquals(preferences.isUse24HourFormat(), savedPreferences.isUse24HourFormat());
    assertEquals(preferences.isUseSSL(), savedPreferences.isUseSSL());
  }

}
