package feature.healthchecker.healthchecker;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import cloudos.HealthCheckerController;
import cloudos.Providers;
import cloudos.instances.InstanceService;
import cloudos.models.Instance;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java8.En;

import java.util.List;

import org.junit.BeforeClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import test.CloudOSTest;

/** Created by philipperibeiro on 6/27/17. */
public class HealthcheckerSteps extends CloudOSTest implements En {

  private static MockMvc mockMvc;

  private static Instance instance;

  private static String url;

  @Autowired
  private HealthCheckerController healthCheckerController;

  @Autowired
  InstanceService instanceService;

  @BeforeClass
  public void setUp() throws Exception {
    mockMvc = MockMvcBuilders.standaloneSetup(this.healthCheckerController).build();
    List<Instance> instanceList =
        instanceService.findAllActiveInstancesByProvider(Providers.AMAZON_AWS);
    assertNotNull(instanceList);

    org.junit.Assume.assumeTrue(instanceList.size() > 0);
    // let's get a single instance...
    instance = instanceList.get(0);
  }

  @When("^I get the resources cost")
  public void get_resources_cost() throws Exception {
    url = HealthCheckerController.HEALTHCHECKER + "/get_resources_cost";
  }

  @When("^I get the resource cost")
  public void get_resource_cost() throws Exception {
    assertNotNull(instance);
    url = HealthCheckerController.HEALTHCHECKER + "/get_resource_cost/" + instance.getProviderId();
  }

  @Then("^I should succeed")
  public void make_request() throws Exception {
    mockMvc = MockMvcBuilders.standaloneSetup(this.healthCheckerController).build();

    mockMvc.perform(get(url)).andExpect(status().isOk());
  }
}
