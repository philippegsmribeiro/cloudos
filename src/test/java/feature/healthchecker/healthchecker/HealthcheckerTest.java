package feature.healthchecker.healthchecker;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

import org.junit.Ignore;
import org.junit.runner.RunWith;

/** Created by philipperibeiro on 6/27/17. */
@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber"})
@Ignore
public class HealthcheckerTest {}
