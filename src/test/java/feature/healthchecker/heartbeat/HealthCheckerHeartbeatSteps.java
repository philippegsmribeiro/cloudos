package feature.healthchecker.heartbeat;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.net.UnknownHostException;

import javax.ws.rs.core.MediaType;

import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import cloudos.HealthCheckerHeartbeatController;
import cloudos.elasticsearch.services.HeartbeatService;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import feature.healthchecker.AbstractElasticsearchSteps;

public class HealthCheckerHeartbeatSteps extends AbstractElasticsearchSteps {

  @Autowired
  private HealthCheckerHeartbeatController healthCheckerHeartbeatController;

  @Autowired
  private HeartbeatService heartbeatService;

  @BeforeClass
  public void setUp() throws Exception {
    mockMvc = MockMvcBuilders.standaloneSetup(this.healthCheckerHeartbeatController).build();
  }

  @When("^I post the resource response_status_avg")
  public void post_response_status_avg() throws Exception {
    url = HealthCheckerHeartbeatController.HEALTHCHECKER + "/response_status_avg";
  }

  @When("^I post the resource up_avg")
  public void post_up_avg() throws Exception {
    url = HealthCheckerHeartbeatController.HEALTHCHECKER + "/up_avg";
  }

  @When("^I post the resource validate_rtt_us_avg")
  public void post_validate_rtt_us_avg() throws Exception {
    url = HealthCheckerHeartbeatController.HEALTHCHECKER + "/validate_rtt_us_avg";
  }

  @When("^I post the resource tls_handshake_rtt_us_avg")
  public void post_tls_handshake_rtt_us_avg() throws Exception {
    url = HealthCheckerHeartbeatController.HEALTHCHECKER + "/tls_handshake_rtt_us_avg";
  }

  @When("^I post the resource tcp_connect_rtt_us_avg")
  public void post_tcp_connect_rtt_us_avg() throws Exception {
    url = HealthCheckerHeartbeatController.HEALTHCHECKER + "/tcp_connect_rtt_us_avg";
  }

  @When("^I post the resource socks5_connect_rtt_us_avg")
  public void post_socks5_connect_rtt_us_avg() throws Exception {
    url = HealthCheckerHeartbeatController.HEALTHCHECKER + "/socks5_connect_rtt_us_avg";
  }

  @When("^I post the resource icmp_rtt_us_avg")
  public void post_icmp_rtt_us_avg() throws Exception {
    url = HealthCheckerHeartbeatController.HEALTHCHECKER + "/icmp_rtt_us_avg";
  }

  @When("^I post the resource resolve_rtt_us_avg")
  public void post_resolve_rtt_us_avg() throws Exception {
    url = HealthCheckerHeartbeatController.HEALTHCHECKER + "/resolve_rtt_us_avg";
  }

  @When("^I post the resource duration_us_avg")
  public void post_duration_us_avg() throws Exception {
    url = HealthCheckerHeartbeatController.HEALTHCHECKER + "/duration_us_avg";
  }

  @Then("^I should succeed")
  public void make_post() throws Exception {
    
    setInstanceBeatName(heartbeatService);
    
    mockMvc = MockMvcBuilders.standaloneSetup(this.healthCheckerHeartbeatController).build();

    MockHttpServletRequestBuilder request =
        post(url).contentType(MediaType.APPLICATION_JSON).content(chartParametersJson);

    mockMvc.perform(request).andExpect(status().isOk());
  }
}
