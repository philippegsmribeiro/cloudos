package feature.healthchecker.packetbeat;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.net.UnknownHostException;

import javax.ws.rs.core.MediaType;

import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import cloudos.HealthCheckerPacketbeatController;
import cloudos.elasticsearch.services.PacketbeatService;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import feature.healthchecker.AbstractElasticsearchSteps;

public class HealthCheckerPacketbeatSteps extends AbstractElasticsearchSteps {

  @Autowired
  private HealthCheckerPacketbeatController healthCheckerPacketbeatController;

  @Autowired
  private PacketbeatService packetbeatService;

  @BeforeClass
  public void setUp() throws Exception {
    mockMvc = MockMvcBuilders.standaloneSetup(this.healthCheckerPacketbeatController).build();
  }

  @When("^I post the resource shipper_avg")
  public void post_shipper_avg() throws Exception {
    url = HealthCheckerPacketbeatController.HEALTHCHECKER + "/shipper_avg";
  }

  @When("^I post the resource responsetime_avg")
  public void post_responsetime_avg() throws Exception {
    url = HealthCheckerPacketbeatController.HEALTHCHECKER + "/responsetime_avg";
  }

  @When("^I post the resource http_content_length_avg")
  public void post_http_content_length_avg() throws Exception {
    url = HealthCheckerPacketbeatController.HEALTHCHECKER + "/http_content_length_avg";
  }

  @When("^I post the resource count_avg")
  public void post_count_avg() throws Exception {
    url = HealthCheckerPacketbeatController.HEALTHCHECKER + "/count_avg";
  }

  @When("^I post the resource bytes_out_avg")
  public void post_bytes_out_avg() throws Exception {
    url = HealthCheckerPacketbeatController.HEALTHCHECKER + "/bytes_out_avg";
  }

  @When("^I post the resource bytes_in_avg")
  public void post_bytes_in_avg() throws Exception {
    url = HealthCheckerPacketbeatController.HEALTHCHECKER + "/bytes_in_avg";
  }

  @Then("^I should succeed")
  public void make_post() throws Exception {
    
    setInstanceBeatName(packetbeatService);
    
    mockMvc = MockMvcBuilders.standaloneSetup(this.healthCheckerPacketbeatController).build();

    MockHttpServletRequestBuilder request =
        post(url).contentType(MediaType.APPLICATION_JSON).content(chartParametersJson);

    mockMvc.perform(request).andExpect(status().isOk());
  }
}
