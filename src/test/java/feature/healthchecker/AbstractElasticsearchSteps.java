package feature.healthchecker;

import static org.junit.Assert.fail;

import java.net.UnknownHostException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.test.web.servlet.MockMvc;

import cloudos.elasticsearch.model.ChartParameters;
import cloudos.elasticsearch.services.AbstractElasticsearchService;
import cloudos.utils.ReflectionToJson;
import cucumber.api.java8.En;
import test.CloudOSTest;

public abstract class AbstractElasticsearchSteps extends CloudOSTest implements En {

  protected static MockMvc mockMvc;

  protected static String url;

  protected static String chartParametersJson = null;

  protected static String instanceBeatName = null;

  protected void setInstanceBeatName(AbstractElasticsearchService service)
      throws UnknownHostException {
    if (StringUtils.isBlank(instanceBeatName)) {
      instanceBeatName = service.getLastBeatName();

      if (StringUtils.isBlank(instanceBeatName)) {
        fail("There is no instance on ES beats data!!!");
      }

      ChartParameters chartParameters = new ChartParameters();
      chartParameters.setName(instanceBeatName);
      chartParametersJson = ReflectionToJson.toString(chartParameters);
    }
  }

}
