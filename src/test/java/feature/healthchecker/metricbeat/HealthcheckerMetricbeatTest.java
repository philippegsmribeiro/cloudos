package feature.healthchecker.metricbeat;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/** Created by philipperibeiro on 6/27/17. */
@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber"})
public class HealthcheckerMetricbeatTest {
}
