package feature.healthchecker.metricbeat;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.ws.rs.core.MediaType;

import org.junit.BeforeClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import cloudos.HealthCheckerMetricbeatController;
import cloudos.elasticsearch.services.MetricbeatService;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import feature.healthchecker.AbstractElasticsearchSteps;

public class HealthCheckerMetricbeatSteps extends AbstractElasticsearchSteps {

  @Autowired
  private HealthCheckerMetricbeatController healthCheckerMetricbeatController;

  @Autowired
  private MetricbeatService metricbeatService;

  @BeforeClass
  public void setUp() throws Exception {
    mockMvc = MockMvcBuilders.standaloneSetup(this.healthCheckerMetricbeatController).build();
  }

  @When("^I post the resource _system_cpu_idle_pct_avg")
  public void post_system_cpu_idle_pct_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_cpu_idle_pct_avg";
  }

  @When("^I post the resource system_cpu_total_usage_pct")
  public void post_system_cpu_total_usage_pct() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_cpu_total_usage_pct";
  }

  @When("^I post the resource _system_cpu_iowait_pct_avg")
  public void post_system_cpu_iowait_pct_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_cpu_iowait_pct_avg";
  }

  @When("^I post the resource _system_cpu_irq_pct_avg")
  public void post_system_cpu_irq_pct_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_cpu_irq_pct_avg";
  }

  @When("^I post the resource _system_cpu_nice_pct_avg")
  public void post_system_cpu_nice_pct_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_cpu_nice_pct_avg";
  }

  @When("^I post the resource _system_cpu_soft_irq_pct_avg")
  public void post_system_cpu_soft_irq_pct_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_cpu_soft_irq_pct_avg";
  }

  @When("^I post the resource _system_cpu_steal_pct_avg")
  public void post_system_cpu_steal_pct_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_cpu_steal_pct_avg";
  }

  @When("^I post the resource _system_cpu_system_pct_avg")
  public void post_system_cpu_system_pct_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_cpu_system_pct_avg";
  }

  @When("^I post the resource _system_cpu_user_pct_avg")
  public void post_system_cpu_user_pct_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_cpu_user_pct_avg";
  }

  @When("^I post the resource _system_filesystem_available_avg")
  public void post_system_filesystem_available_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_filesystem_available_avg";
  }

  @When("^I post the resource _system_filesystem_files_avg")
  public void post_system_filesystem_files_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_filesystem_files_avg";
  }

  @When("^I post the resource _0system_filesystem_free_avg")
  public void post_system_filesystem_free_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_filesystem_free_avg";
  }

  @When("^I post the resource _0system_filesystem_free_files_avg")
  public void post_system_filesystem_free_files_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_filesystem_free_files_avg";
  }

  @When("^I post the resource _system_filesystem_total_avg")
  public void post_system_filesystem_total_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_filesystem_total_avg";
  }

  @When("^I post the resource _system_filesystem_used_bytes_avg")
  public void post_system_filesystem_used_bytes_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_filesystem_used_bytes_avg";
  }

  @When("^I post the resource _system_filesystem_used_pct_avg")
  public void post_system_filesystem_used_pct_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_filesystem_used_pct_avg";
  }

  @When("^I post the resource _system_load1_avg")
  public void post_system_load1_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_load1_avg";
  }

  @When("^I post the resource _0system_load15_avg")
  public void post_system_load15_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_load15_avg";
  }

  @When("^I post the resource _system_load5_avg")
  public void post_system_load5_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_load5_avg";
  }

  @When("^I post the resource _system_load_norm1_avg")
  public void post_system_load_norm1_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_load_norm1_avg";
  }

  @When("^I post the resource _system_load_norm15_avg")
  public void post_system_load_norm15_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_load_norm15_avg";
  }

  @When("^I post the resource _system_load_norm5_avg")
  public void post_system_load_norm5_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_load_norm5_avg";
  }

  @When("^I post the resource _system_memory_actual_free_avg")
  public void post_system_memory_actual_free_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_memory_actual_free_avg";
  }

  @When("^I post the resource _system_memory_actual_used_bytes_avg")
  public void post_system_memory_actual_used_bytes_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_memory_actual_used_bytes_avg";
  }

  @When("^I post the resource _system_memory_actual_used_pct_avg")
  public void post_system_memory_actual_used_pct_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_memory_actual_used_pct_avg";
  }

  @When("^I post the resource _system_memory_free_avg")
  public void post_system_memory_free_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_memory_free_avg";
  }

  @When("^I post the resource _system_memory_swap_free_avg")
  public void post_system_memory_swap_free_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_memory_swap_free_avg";
  }

  @When("^I post the resource _system_memory_swap_total_avg")
  public void post_system_memory_swap_total_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_memory_swap_total_avg";
  }

  @When("^I post the resource _system_memory_swap_used_bytes_avg")
  public void post_system_memory_swap_used_bytes_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_memory_swap_used_bytes_avg";
  }

  @When("^I post the resource _system_memory_swap_used_pct_avg")
  public void post_system_memory_swap_used_pct_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_memory_swap_used_pct_avg";
  }

  @When("^I post the resource _system_memory_total_avg")
  public void post_system_memory_total_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_memory_total_avg";
  }

  @When("^I post the resource _system_memory_used_bytes_avg")
  public void post_system_memory_used_bytes_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_memory_used_bytes_avg";
  }

  @When("^I post the resource _system_memory_used_pct_avg")
  public void post_system_memory_used_pct_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_memory_used_pct_avg";
  }

  @When("^I post the resource _system_network_in_bytes_avg")
  public void post_system_network_in_bytes_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_network_in_bytes_avg";
  }

  @When("^I post the resource _system_network_in_dropped_avg")
  public void post_system_network_in_dropped_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_network_in_dropped_avg";
  }

  @When("^I post the resource _system_network_in_errors_avg")
  public void post_system_network_in_errors_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_network_in_errors_avg";
  }

  @When("^I post the resource _system_network_in_packets_avg")
  public void post_system_network_in_packets_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_network_in_packets_avg";
  }

  @When("^I post the resource _system_network_out_bytes_avg")
  public void post_system_network_out_bytes_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_network_out_bytes_avg";
  }

  @When("^I post the resource _system_network_out_dropped_avg")
  public void post_system_network_out_dropped_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_network_out_dropped_avg";
  }

  @When("^I post the resource _system_network_out_errors_avg")
  public void post_system_network_out_errors_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_network_out_errors_avg";
  }

  @When("^I post the resource _system_network_out_packets_avg")
  public void post_system_network_out_packets_avg() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_network_out_packets_avg";
  }

  @When("^I post the resource metricset_rtt")
  public void post_metricset_rtt() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/metricset_rtt";
  }

  @When("^I post the resource system_cpu_idle_pct")
  public void post_system_cpu_idle_pct() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_cpu_idle_pct";
  }

  @When("^I post the resource system_cpu_iowait_pct")
  public void post_system_cpu_iowait_pct() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_cpu_iowait_pct";
  }

  @When("^I post the resource system_cpu_irq_pct")
  public void post_system_cpu_irq_pct() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_cpu_irq_pct";
  }

  @When("^I post the resource system_cpu_nice_pct")
  public void post_system_cpu_nice_pct() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_cpu_nice_pct";
  }

  @When("^I post the resource system_cpu_soft_irq_pct")
  public void post_system_cpu_soft_irq_pct() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_cpu_soft_irq_pct";
  }

  @When("^I post the resource system_cpu_steal_pct")
  public void post_system_cpu_steal_pct() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_cpu_steal_pct";
  }

  @When("^I post the resource system_cpu_system_pct")
  public void post_system_cpu_system_pct() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_cpu_system_pct";
  }

  @When("^I post the resource system_cpu_user_pct")
  public void post_system_cpu_user_pct() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_cpu_user_pct";
  }

  @When("^I post the resource system_filesystem_available")
  public void post_system_filesystem_available() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_filesystem_available";
  }

  @When("^I post the resource system_filesystem_files")
  public void post_system_filesystem_files() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_filesystem_files";
  }

  @When("^I post the resource _system_filesystem_free")
  public void post_system_filesystem_free() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_filesystem_free";
  }

  @When("^I post the resource system_filesystem_free_files")
  public void post_system_filesystem_free_files() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_filesystem_free_files";
  }

  @When("^I post the resource system_filesystem_total")
  public void post_system_filesystem_total() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_filesystem_total";
  }

  @When("^I post the resource system_filesystem_used_bytes")
  public void post_system_filesystem_used_bytes() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_filesystem_used_bytes";
  }

  @When("^I post the resource system_filesystem_used_pct")
  public void post_system_filesystem_used_pct() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_filesystem_used_pct";
  }

  @When("^I post the resource system_load1")
  public void post_system_load1() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_load1";
  }

  @When("^I post the resource _system_load15")
  public void post_system_load15() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_load15";
  }

  @When("^I post the resource system_load5")
  public void post_system_load5() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_load5";
  }

  @When("^I post the resource system_load_norm1")
  public void post_system_load_norm1() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_load_norm1";
  }

  @When("^I post the resource _0system_load_norm15")
  public void post_system_load_norm15() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_load_norm15";
  }

  @When("^I post the resource system_load_norm5")
  public void post_system_load_norm5() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_load_norm5";
  }

  @When("^I post the resource system_memory_actual_free")
  public void post_system_memory_actual_free() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_memory_actual_free";
  }

  @When("^I post the resource system_memory_actual_used_bytes")
  public void post_system_memory_actual_used_bytes() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_memory_actual_used_bytes";
  }

  @When("^I post the resource system_memory_actual_used_pct")
  public void post_system_memory_actual_used_pct() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_memory_actual_used_pct";
  }

  @When("^I post the resource system_memory_free")
  public void post_system_memory_free() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_memory_free";
  }

  @When("^I post the resource system_memory_swap_free")
  public void post_system_memory_swap_free() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_memory_swap_free";
  }

  @When("^I post the resource system_memory_swap_total")
  public void post_system_memory_swap_total() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_memory_swap_total";
  }

  @When("^I post the resource system_memory_swap_used_bytes")
  public void post_system_memory_swap_used_bytes() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_memory_swap_used_bytes";
  }

  @When("^I post the resource system_memory_swap_used_pct")
  public void post_system_memory_swap_used_pct() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_memory_swap_used_pct";
  }

  @When("^I post the resource system_memory_total")
  public void post_system_memory_total() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_memory_total";
  }

  @When("^I post the resource system_memory_used_bytes")
  public void post_system_memory_used_bytes() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_memory_used_bytes";
  }

  @When("^I post the resource system_memory_used_pct")
  public void post_system_memory_used_pct() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_memory_used_pct";
  }

  @When("^I post the resource system_network_in_bytes")
  public void post_system_network_in_bytes() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_network_in_bytes";
  }

  @When("^I post the resource system_network_in_dropped")
  public void post_system_network_in_dropped() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_network_in_dropped";
  }

  @When("^I post the resource system_network_in_errors")
  public void post_system_network_in_errors() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_network_in_errors";
  }

  @When("^I post the resource system_network_in_packets")
  public void post_system_network_in_packets() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_network_in_packets";
  }

  @When("^I post the resource system_network_out_bytes")
  public void post_system_network_out_bytes() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_network_out_bytes";
  }

  @When("^I post the resource system_network_out_dropped")
  public void post_system_network_out_dropped() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_network_out_dropped";
  }

  @When("^I post the resource system_network_out_errors")
  public void post_system_network_out_errors() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_network_out_errors";
  }

  @When("^I post the resource system_network_out_packets")
  public void post_system_network_out_packets() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_network_out_packets";
  }

  @When("^I post the resource system_cpu_cores")
  public void post_system_cpu_cores() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_cpu_cores";
  }

  @When("^I post the resource network_traffic_bytes_chart")
  public void post_network_traffic_bytes_chart() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/network_traffic_bytes_chart";
  }

  @When("^I post the resource network_traffic_packets_chart")
  public void post_network_traffic_packets_chart() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/network_traffic_packets_chart";
  }

  @When("^I post the resource top_10_hostnames_network")
  public void post_top_10_hostnames_network() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/top_10_hostnames_network";
  }

  @When("^I post the resource top_10_hostnames_disk_io")
  public void post_top_10_hostnames_disk_io() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/top_10_hostnames_disk_io";
  }

  @When("^I post the resource top_10_process_cpu_pct")
  public void post_top_10_process_cpu_pct() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/top_10_process_cpu_pct";
  }

  @When("^I post the resource top_10_system_process_memory_rss_pct")
  public void post_top_10_system_process_memory_rss_pct() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/top_10_system_process_memory_rss_pct";
  }

  @When("^I post the resource top_10_processes")
  public void post_top_10_processes() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/top_10_processes";
  }

  @When("^I post the resource memory_usage_bytes_chart")
  public void post_memory_usage_bytes_chart() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/memory_usage_bytes_chart";
  }

  @When("^I post the resource cpu_usage_pct_chart")
  public void post_cpu_usage_pct_chart() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/cpu_usage_pct_chart";
  }

  @When("^I post the resource system_load_chart")
  public void post_system_load_chart() throws Exception {
    url = HealthCheckerMetricbeatController.HEALTHCHECKER + "/system_load_chart";
  }

  @Then("^I should succeed")
  public void make_post() throws Exception {

    setInstanceBeatName(metricbeatService);

    mockMvc = MockMvcBuilders.standaloneSetup(this.healthCheckerMetricbeatController).build();

    MockHttpServletRequestBuilder request =
        post(url).contentType(MediaType.APPLICATION_JSON).content(chartParametersJson);

    mockMvc.perform(request).andExpect(status().isOk());
  }
}
