package feature.deployment;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import cloudos.Providers;
import cloudos.api.ProvidersController;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import test.CloudOSTest;

public class ProviderElementsSteps extends CloudOSTest {

  private MockMvc mockMvc;

  @Autowired ProvidersController providersController;

  @Before
  public void setUp() throws Exception {
    this.mockMvc = MockMvcBuilders.standaloneSetup(providersController).setViewResolvers().build();
  }

  @When("^the client list the providers")
  public void listProviders() throws Throwable {
    this.mockMvc.perform(get(ProvidersController.PROVIDERS + "/")).andExpect(status().isOk());
  }

  @Then("^the client receive a list which contains (.*)$")
  public void verifyingListProviders(Providers provider) throws Throwable {
    this.mockMvc
        .perform(get(ProvidersController.PROVIDERS + "/"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasItem(provider.toString())));
  }

  @Then("^the client receive regions for (.*)$")
  public void gettingRegions(Providers provider) throws Throwable {
    this.mockMvc
        .perform(get(ProvidersController.PROVIDERS + "/regions/" + provider.name()))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$").exists());
  }

  @Then("^the client receive regions zones for (.*)$")
  public void gettingZones(Providers provider) throws Throwable {
    this.mockMvc
        .perform(
            get(
                ProvidersController.PROVIDERS
                    + "/regions/"
                    + provider.name()
                    + "/avaibility_zones"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$").exists());
  }

  @Then("^the client receive images for (.*)$")
  public void gettingImages(Providers provider) throws Throwable {
    this.mockMvc
        .perform(get(ProvidersController.PROVIDERS + "/images/" + provider.name()))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$").exists());
  }

  @Then("^the client receive machineTypes for (.*)$")
  public void gettingMachineTypes(Providers provider) throws Throwable {
    this.mockMvc
        .perform(get(ProvidersController.PROVIDERS + "/machineTypes/" + provider.name()))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$").exists());
  }

  @Then("^the client receive keys for (.*)$")
  public void gettingKeys(Providers provider) throws Throwable {
    this.mockMvc
        .perform(get(ProvidersController.PROVIDERS + "/keys/" + provider.name()))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$").exists());
  }

  @Then("^the client receive security groups for (.*)$")
  public void gettingSecurityGroups(Providers provider) throws Throwable {
    this.mockMvc
        .perform(get(ProvidersController.PROVIDERS + "/securityGroups/" + provider.name()))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$").exists());
  }
}
