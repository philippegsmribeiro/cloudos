package feature.deployment;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import cloudos.Providers;
import cloudos.api.CloudManagerController;
import cloudos.keys.AmazonKeyManagement;
import cloudos.keys.KeyManagementException;
import cloudos.keys.KeyManagerService;
import cloudos.keys.cloudoskey.AmazonKey;
import cloudos.keys.cloudoskey.CloudosKey;
import cloudos.models.CloudCreateKeyRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import test.CloudOSTest;

/** Created by philipperibeiro on 5/14/17. */
public class KeysSteps extends CloudOSTest {

  private MockMvc mvc;
  private String keyName;
  private String regionExists;
  private String regionNotExists;
  private Providers provider;

  private AmazonKey keyExistsOnlyAws;

  private ResultActions performCreateKeyExists;

  private CloudosKey cloudosKeyCreated;

  @Autowired private CloudManagerController cloudManagerController;

  @Autowired private AmazonKeyManagement amazonKeyManagement;

  @Autowired private KeyManagerService keyManagerService;

  @Before
  public void setUp() throws Exception {
    this.mvc = MockMvcBuilders.standaloneSetup(cloudManagerController).build();
  }

  @When("^the client enter provider (.*)$")
  public void the_client_enter_provider(String provider) throws Throwable {
    this.provider = Providers.valueOf(provider);
  }

  @When("the client enter a aleatory keyName")
  public void the_client_enter_a_aleatory_keyName() throws Throwable {
    this.keyName = this.getRandomKeyName();
  }

  @When("^the client enter region that exists (.*)$")
  public void the_client_enter_region(String region) throws Throwable {
    this.regionExists = region;
  }

  @When("^the client enter region that not exists (.*)$")
  public void the_client_enter_region_not_exists(String region) throws Throwable {
    this.regionNotExists = region;
  }

  @Then("^the client receives error")
  public void the_client_receives_error() throws Throwable {
    this.testCreateKey(500, this.regionNotExists);
  }

  @Then("^the client receives ok")
  public void the_client_receives_ok() throws Throwable {
    cloudosKeyCreated = this.testCreateKey(200, this.regionExists);
  }

  public CloudosKey testCreateKey(int statusCode, String region) throws Throwable {
    CloudCreateKeyRequest cloudCreateKeyRequest =
        new CloudCreateKeyRequest(this.provider, region, this.keyName);

    String payload = gson.toJson(cloudCreateKeyRequest);

    logger.debug("message content {} for statusCode {}", payload, statusCode);

    ResultActions perform =
        this.mvc.perform(
            post(CloudManagerController.CLOUDMANAGER + "/create_keys")
                .content(payload)
                .contentType(contentType));

    String contentAsString = perform.andReturn().getResponse().getContentAsString();

    logger.debug(contentAsString);

    perform.andExpect(status().is(statusCode));

    ObjectMapper mapper = new ObjectMapper();

    return mapper.readValue(contentAsString, CloudosKey.class);
  }

  private String getRandomKeyName() {
    return "cos-test-" + generator.generate(5);
  }

  @When("^the client get a AWS key that not exists in cloudos in the region (.*)$")
  public void the_client_get_a_AWS_key_that_not_exists_in_cloudos(String region)
      throws KeyManagementException {
    String awsKeyName = this.getRandomKeyName();

    keyExistsOnlyAws = amazonKeyManagement.generateKey(awsKeyName, region);
  }

  @Then("the client trying to save that key in AWS")
  public void the_client_trying_to_save_that_key_in_AWS() throws Exception {
    CloudCreateKeyRequest cloudCreateKeyRequest =
        new CloudCreateKeyRequest(
            keyExistsOnlyAws.getProvider(),
            keyExistsOnlyAws.getRegion(),
            keyExistsOnlyAws.getKeyName());
    String payload = gson.toJson(cloudCreateKeyRequest);

    performCreateKeyExists =
        this.mvc.perform(
            post(CloudManagerController.CLOUDMANAGER + "/create_keys")
                .content(payload)
                .contentType(contentType));
  }

  @Then("the client receive a error message")
  public void the_client_receive_a_error_message() throws Exception {
    performCreateKeyExists.andDo(h -> logger.debug(h.getResponse().getContentAsString()));

    performCreateKeyExists.andExpect(status().is5xxServerError());
  }

  @After
  public void tearDown() throws Exception {
    if (keyExistsOnlyAws != null) {
      amazonKeyManagement.deleteKey(keyExistsOnlyAws.getKeyName(), keyExistsOnlyAws.getRegion());
    }

    if (cloudosKeyCreated != null && StringUtils.isNotBlank(cloudosKeyCreated.getId())) {
      keyManagerService.deleteKey(cloudosKeyCreated.getId());
    }
  }
}
