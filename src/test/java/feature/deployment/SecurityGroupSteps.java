package feature.deployment;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import cloudos.Providers;
import cloudos.SecurityGroupManagerService;
import cloudos.api.CloudManagerController;
import cloudos.models.AwsSecurityGroup;
import cloudos.models.CloudosSecurityGroup;

import com.fasterxml.jackson.databind.ObjectMapper;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import test.CloudOSTest;

/** */
public class SecurityGroupSteps extends CloudOSTest {

  private MockMvc mvc;
  private String name;
  private String region = "us-west-1";
  private String descriptionGroup;
  private Providers provider;
  private CloudosSecurityGroup cloudosSecurityGroupCreated;

  @Autowired private CloudManagerController cloudManagerController;

  @Autowired private SecurityGroupManagerService securityGroupManagerService;

  @Before
  public void setUp() throws Exception {
    this.mvc = MockMvcBuilders.standaloneSetup(cloudManagerController).build();
  }

  @When("^the client enter the provider (.*)$")
  public void the_client_enter_provider(String provider) throws Throwable {
    this.provider = Providers.valueOf(provider);
  }

  @When("the client enter the group name aleatory")
  public void the_client_enter_a_aleatory_groupName() throws Throwable {
    this.name = this.getRandomGroupName();
  }

  @When("^the client enter the group description aleatory")
  public void the_client_enter_the_group_description_aleatory() throws Throwable {
    this.descriptionGroup = "Security Group for test purpose.";
  }

  @Then("^the client receive a success message")
  public void the_client_receives_success() throws Throwable {

    AwsSecurityGroup awsSecurityGroup = new AwsSecurityGroup();
    awsSecurityGroup.setName(this.name);
    awsSecurityGroup.setRegion(region);
    awsSecurityGroup.setDescription(this.descriptionGroup);
    awsSecurityGroup.setProvider(this.provider);

    ObjectMapper mapper = new ObjectMapper();

    String payload = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(awsSecurityGroup);

    ResultActions perform =
        this.mvc.perform(
            post(CloudManagerController.CLOUDMANAGER + "/create_security_group")
                .content(payload)
                .contentType(contentType));

    String contentAsString =
        perform
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();

    cloudosSecurityGroupCreated = mapper.readValue(contentAsString, CloudosSecurityGroup.class);
  }

  private String getRandomGroupName() {
    return "cos-sg-" + generator.generate(5);
  }

  @After
  public void tearDown() throws Exception {

    if (cloudosSecurityGroupCreated != null
        && StringUtils.isNotBlank(cloudosSecurityGroupCreated.getId())) {

      this.securityGroupManagerService.delete(cloudosSecurityGroupCreated);
    }
  }
}
