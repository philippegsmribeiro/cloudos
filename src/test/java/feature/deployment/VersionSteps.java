package feature.deployment;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import cloudos.VersionController;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import test.CloudOSTest;

/** Created by philipperibeiro on 5/14/17. */
public class VersionSteps extends CloudOSTest {

  private MockMvc mockMvc;

  @Before
  public void setUp() throws Exception {
    this.mockMvc = MockMvcBuilders.standaloneSetup(new VersionController()).build();
  }

  @When("^the client calls /api/version")
  public void the_client_issues_GET_version() throws Throwable {
    this.mockMvc.perform(get(VersionController.VERSION + "/")).andExpect(status().isOk());
  }

  @Then("^the client receives status code of (\\d+)$")
  public void the_client_receives_status_code_of(int statusCode) throws Throwable {
    mockMvc.perform(get(VersionController.VERSION + "/")).andExpect(status().isOk());
  }

  @And("^the client receives server version (.+)$")
  public void the_client_receives_server_version_body(String version) throws Throwable {
    mockMvc
        .perform(get(VersionController.VERSION + "/"))
        .andExpect(status().isOk())
        .andExpect(content().string(containsString(version)));
  }
}
