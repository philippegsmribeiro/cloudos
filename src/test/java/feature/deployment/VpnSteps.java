package feature.deployment;


import cloudos.Providers;
import cloudos.api.CloudManagerController;
import cloudos.models.network.CloudosVpcRequest;

import cucumber.api.java.en.When;
import cucumber.api.java8.En;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import test.CloudOSTest;

public class VpnSteps extends CloudOSTest implements En {

  private CloudosVpcRequest request;

  private String url;

  private MockMvc mockMvc;

  @Autowired private CloudManagerController cloudManagerController;

  public VpnSteps() {
    request = new CloudosVpcRequest();

    And(
        "^the provider is (.*)$",
        (String arg1) -> {
          Providers provider = Providers.valueOf(arg1);
          request.setProvider(provider);
        });

    And(
        "^the region is (.*)$",
        (String region) -> {
          request.setRegion(region);
        });

    And(
        "^the cidr group is (.*)$",
        (String cidr) -> {
          request.setCidrBlock(cidr);
        });
  }

  @org.junit.Before
  public void setUp() throws Exception {
    this.mockMvc = MockMvcBuilders.standaloneSetup(cloudManagerController).build();
  }

  @When("^I create a new VPN")
  public void create_vpn() {
    this.url = CloudManagerController.CLOUDMANAGER + "/vpc";
  }
}
