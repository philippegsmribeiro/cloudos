package feature.dashboard;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import test.SlowTest;

/** Created by philipperibeiro on 6/4/17. */
@Category(SlowTest.class)
@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber"})
public class DashboardTest {}
