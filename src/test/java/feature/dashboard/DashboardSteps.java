package feature.dashboard;

import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import cloudos.DashboardController;
import cloudos.Providers;
import cloudos.dashboard.CostEntry;
import cloudos.dashboard.DashboardService;
import cloudos.dashboard.Event;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java8.En;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import test.CloudOSTest;

/** Created by philipperibeiro on 6/4/17. */
public class DashboardSteps extends CloudOSTest implements En {

  private MockMvc mockMvc;

  @Autowired private DashboardService dashboardService;

  private CostEntry costEntry;

  private String url;

  private MvcResult result;

  public DashboardSteps() {

    And(
        "^The number of instances should be greater than or equal to (\\d+)$",
        (Integer arg1) -> {
          // Write code here that turns the phrase above into concrete actions
          try {
            mockMvc
                .perform(get(this.url))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType));

            assertThat(arg1, greaterThanOrEqualTo(0));
          } catch (Exception e) {
            logger.error(e.getMessage());
          }
        });

    And(
        "^one of the providers is (.*)$",
        (String arg1) -> {
          try {
            mockMvc
                .perform(get(this.url))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(
                    jsonPath(
                        "$[0].providers",
                        anyOf(
                            is(Providers.GOOGLE_COMPUTE_ENGINE.toString()),
                            is(Providers.MICROSOFT_AZURE.toString()),
                            is(Providers.AMAZON_AWS.toString()))));
          } catch (Exception e) {
            logger.error(e.getMessage());
          }
        });

    And(
        "^(.*) is not one of the providers$",
        (String arg1) -> {
          try {
            mockMvc
                .perform(get(this.url))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$[0].providers", not(hasItem(arg1))));
          } catch (Exception e) {
            logger.error(e.getMessage());
          }
        });

    And(
        "^the event is (.*)$",
        (String arg1) -> {
          Event event = new Event("[14:00] VM1 CPU increased by 10%");
          try {
            mockMvc
                .perform(get(this.url))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$[0].content", is(event.getContent())));

          } catch (Exception e) {
            logger.error(e.getMessage());
          }
        });

    And(
        "^the status code is (\\d+)",
        (Integer arg1) -> {
          try {
            mockMvc.perform(get(this.url)).andExpect(status().isOk());
          } catch (Exception e) {
            logger.error(e.getMessage());
          }
        });

    When(
        "^If I list the (.*) CPU usage$",
        (String arg1) -> {
          this.url = DashboardController.DASHBOARD + "/list/memory_usage/" + arg1;
        });

    When(
        "^If I list the (.*) Memory usage$",
        (String arg1) -> {
          this.url = DashboardController.DASHBOARD + "/list/cpu_usage/" + arg1;
        });

    When(
        "^If I list the (.*) Disk usage$",
        (String arg1) -> {
          this.url = DashboardController.DASHBOARD + "/list/disk_usage/" + arg1;
        });

    When(
        "^If I list the (.*) Network usage$",
        (String arg1) -> {
          this.url = DashboardController.DASHBOARD + "/list/network_usage/" + arg1;
        });
  }

  protected String json(Object o) throws IOException {
    return mapper.writeValueAsString(o);
  }

  @Before
  public void setUp() throws Exception {

    this.mockMvc =
        MockMvcBuilders.standaloneSetup(new DashboardController(this.dashboardService)).build();
  }

  @When("^I get the total cost")
  public void get_total_cost() {
    this.costEntry = dashboardService.getCost();
    this.url = DashboardController.DASHBOARD + "/list/cost";
  }

  @When("^I get all the resources")
  public void get_resources() {
    this.url = DashboardController.DASHBOARD + "/list/resources";
  }

  @When("^I get all the regions")
  public void get_regions() {
    this.url = DashboardController.DASHBOARD + "/list/regions";
  }

  @When("^I get all the providers")
  public void get_providers() {
    this.url = DashboardController.DASHBOARD + "/list/providers";
  }

  @When("^I get all the events")
  public void get_events() {
    this.url = DashboardController.DASHBOARD + "/list/events";
  }

  @When("^I get the CPU usage")
  public void get_cpu_usage() {
    this.url = DashboardController.DASHBOARD + "/list/cpu_usage";
  }

  @When("^I get the Memory usage")
  public void get_memory_usage() {
    this.url = DashboardController.DASHBOARD + "/list/memory_usage";
  }

  @When("^I get the Disk usage")
  public void get_disk_usage() {
    this.url = DashboardController.DASHBOARD + "/list/disk_usage";
  }

  @When("^I get the Network usage")
  public void get_network_usage() {
    this.url = DashboardController.DASHBOARD + "/list/network_usage";
  }

  @Then("^I should succeed")
  public void the_client_receives_ok_status_code() throws Throwable {
    result = mockMvc.perform(get(this.url)).andExpect(status().isOk()).andReturn();

    assertNotNull(result);
  }

  @And("^I receive a cost greater or equal zero for the current month")
  public void check_cost_and_month() {
    assertEquals(result.getResponse().getStatus(), 200);
    assertNotNull(this.costEntry);
    assertNotNull(this.costEntry.getTotalDailyCost());
    assertThat(this.costEntry.getTotalDailyCost().getCost(), greaterThanOrEqualTo(0.0));
  }
}
