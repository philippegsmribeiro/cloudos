package feature.tenant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import cloudos.Providers;
import cloudos.encryptionKeys.AmazonKMSService;
import cloudos.models.CloudosAccount;
import cloudos.models.CloudosTenant;
import cloudos.user.UserPreferences;
import cloudos.user.UserProfile;
import cloudos.tenant.EmailRequest;
import cloudos.tenant.TenantController;
import cloudos.user.User;
import cloudos.user.UserRole;
import cloudos.user.UserService;
import cloudos.utils.ReflectionToJson;
import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.core.MediaType;
import org.junit.BeforeClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java8.En;
import test.CloudOSTest;

public class TenantControllerSteps extends CloudOSTest implements En {

  private static MockMvc mockMvc;

  private static String url;

  private static CloudosTenant cloudosTenant = new CloudosTenant();

  @Autowired
  private UserService userService;

  @Autowired
  private TenantController tenantController;

  @Autowired
  private AmazonKMSService amazonKMSService;

  @BeforeClass
  public void setUp() throws Exception {
    mockMvc = MockMvcBuilders.standaloneSetup(this.tenantController).build();
  }

  @When("^I get USA regions")
  public void getUSAregions() throws Exception {
    url = TenantController.TENANT + "/get_regions_by_country_flag/USA";
    mockMvc = MockMvcBuilders.standaloneSetup(this.tenantController).build();
  }

  @Then("^I should get all valid USA regions")
  public void checkAllUSARegions() throws Exception {
    ResultActions resultActions = mockMvc.perform(get(url)).andExpect(status().isOk());
    String json = resultActions.andReturn().getResponse().getContentAsString();
    List<String> regions = new Gson().fromJson(json, List.class);
    assertEquals(regions.get(0), "us-east-1");
    assertEquals(regions.get(1), "us-east-2");
    assertEquals(regions.get(2), "us-west-1");
    assertEquals(regions.get(3), "us-west-2");
  }

  @When("^I get CHN regions")
  public void getCHNregions() throws Exception {
    url = TenantController.TENANT + "/get_regions_by_country_flag/CHN";
    mockMvc = MockMvcBuilders.standaloneSetup(this.tenantController).build();
  }

  @Then("^I should get all valid CHN regions")
  public void checkAllCHNRegions() throws Exception {
    ResultActions resultActions = mockMvc.perform(get(url)).andExpect(status().isOk());
    String json = resultActions.andReturn().getResponse().getContentAsString();
    List<String> regions = new Gson().fromJson(json, List.class);
    assertEquals(regions.get(0), "ap-southeast-1");
    assertEquals(regions.get(1), "ap-northeast-2");
  }

  @When("^I get AUS regions")
  public void getAUSregions() throws Exception {
    url = TenantController.TENANT + "/get_regions_by_country_flag/AUS";
    mockMvc = MockMvcBuilders.standaloneSetup(this.tenantController).build();
  }

  @Then("^I should get all valid AUS regions")
  public void checkAllAUSRegions() throws Exception {
    ResultActions resultActions = mockMvc.perform(get(url)).andExpect(status().isOk());
    String json = resultActions.andReturn().getResponse().getContentAsString();
    List<String> regions = new Gson().fromJson(json, List.class);
    assertEquals(regions.get(0), "ap-southeast-2");
  }

  @When("^I get EUR regions")
  public void getEURregions() throws Exception {
    url = TenantController.TENANT + "/get_regions_by_country_flag/EUR";
    mockMvc = MockMvcBuilders.standaloneSetup(this.tenantController).build();
  }

  @Then("^I should get all valid EUR regions")
  public void checkAllEURRegions() throws Exception {
    ResultActions resultActions = mockMvc.perform(get(url)).andExpect(status().isOk());
    String json = resultActions.andReturn().getResponse().getContentAsString();
    List<String> regions = new Gson().fromJson(json, List.class);
    assertEquals(regions.get(0), "eu-central-1");
    assertEquals(regions.get(1), "eu-west-1");
    assertEquals(regions.get(2), "eu-west-2");
  }

  @When("^I get BRA regions")
  public void getBRAregions() throws Exception {
    url = TenantController.TENANT + "/get_regions_by_country_flag/BRA";
    mockMvc = MockMvcBuilders.standaloneSetup(this.tenantController).build();
  }

  @Then("^I should get all valid BRA regions")
  public void checkAllBRARegions() throws Exception {
    ResultActions resultActions = mockMvc.perform(get(url)).andExpect(status().isOk());
    String json = resultActions.andReturn().getResponse().getContentAsString();
    List<String> regions = new Gson().fromJson(json, List.class);
    assertEquals(regions.get(0), "sa-east-1");
  }

  @When("^I get JPN regions")
  public void getJPNregions() throws Exception {
    url = TenantController.TENANT + "/get_regions_by_country_flag/JPN";
    mockMvc = MockMvcBuilders.standaloneSetup(this.tenantController).build();
  }

  @Then("^I should get all valid JPN regions")
  public void checkAllJPNRegions() throws Exception {
    ResultActions resultActions = mockMvc.perform(get(url)).andExpect(status().isOk());
    String json = resultActions.andReturn().getResponse().getContentAsString();
    List<String> regions = new Gson().fromJson(json, List.class);
    assertEquals(regions.get(0), "ap-northeast-1");
  }


  @When("^I get a new vpc subnet cidr")
  public void getNewVpcSubnetCidr() throws Exception {
    url = TenantController.TENANT + "/generate_random_subnet_cidr";
    mockMvc = MockMvcBuilders.standaloneSetup(this.tenantController).build();
  }

  @When("^I create a new tenant")
  public void createTenant() throws Exception {
    url = TenantController.TENANT + "/create";
    mockMvc = MockMvcBuilders.standaloneSetup(this.tenantController).build();
  }

  @When("^I check for an existing tenant by domain name")
  public void post_check_existing_tenant() throws Exception {
    url = TenantController.TENANT + "/check_existing_tenant_by_domain/"
        + cloudosTenant.getDomainName();
    mockMvc = MockMvcBuilders.standaloneSetup(this.tenantController).build();
  }

  @When("^I check for a non existing tenant by domain name")
  public void post_check_non_existing_tenant() throws Exception {
    url = TenantController.TENANT + "/check_existing_tenant_by_domain/" + generator.generate(20);
    mockMvc = MockMvcBuilders.standaloneSetup(this.tenantController).build();
  }

  @When("^I check for a non existing user by email")
  public void post_check_non_existing_user_by_email() throws Exception {
    url = TenantController.TENANT + "/check_existing_user_by_email";
    mockMvc = MockMvcBuilders.standaloneSetup(this.tenantController).build();
  }

  @When("^I check for an existing user by email")
  public void post_check_an_existing_user_by_email() throws Exception {
    url = TenantController.TENANT + "/check_existing_user_by_email";
    mockMvc = MockMvcBuilders.standaloneSetup(this.tenantController).build();
  }

  @When("^I check for a non existing user by username")
  public void post_check_non_existing_user_by_username() throws Exception {
    url = TenantController.TENANT + "/check_existing_user_by_username/" + generator.generate(20);
    mockMvc = MockMvcBuilders.standaloneSetup(this.tenantController).build();
  }

  @When("^I check for an existing user by username")
  public void post_check_an_existing_user_by_username() throws Exception {
    User user = userService.retrieveAllUsers().get(0);
    String decryptedUsername = amazonKMSService
        .decryptMessage(amazonKMSService.getUserEncryptedDataKey(), user.getUsername());
    url = TenantController.TENANT + "/check_existing_user_by_username/" + decryptedUsername;
    mockMvc = MockMvcBuilders.standaloneSetup(this.tenantController).build();
  }

  @Then("^I should create a tenant")
  public void make_post_create() throws Exception {

    createTenantRequest();

    MockHttpServletRequestBuilder request = post(url).contentType(MediaType.APPLICATION_JSON)
        .content(ReflectionToJson.toString(cloudosTenant));

    mockMvc.perform(request).andExpect(status().isCreated());

  }

  @Then("^I find the tenant by domain name")
  public void get_check_tenant_true() throws Exception {
    ResultActions resultActions = mockMvc.perform(get(url)).andExpect(status().isOk());
    String result = resultActions.andReturn().getResponse().getContentAsString();
    assertTrue(Boolean.valueOf(result));
  }

  @Then("^I don't find the tenant by domain name")
  public void get_check_tenant_false() throws Exception {
    ResultActions resultActions = mockMvc.perform(get(url)).andExpect(status().isOk());
    String result = resultActions.andReturn().getResponse().getContentAsString();
    assertFalse(Boolean.valueOf(result));
  }

  @Then("^I find the user by email")
  public void get_check_user_by_email_true() throws Exception {

    User user = userService.retrieveAllUsers().get(0);

    String decryptedEmail = amazonKMSService
        .decryptMessage(amazonKMSService.getUserEncryptedDataKey(), user.getEmail());

    EmailRequest emailRequest = new EmailRequest();
    emailRequest.setEmail(decryptedEmail);

    MockHttpServletRequestBuilder request = post(url).contentType(MediaType.APPLICATION_JSON)
        .content(ReflectionToJson.toString(emailRequest));

    ResultActions resultActions = mockMvc.perform(request).andExpect(status().isOk());
    String result = resultActions.andReturn().getResponse().getContentAsString();

    assertTrue(Boolean.valueOf(result));
  }

  @Then("^I don't find the user by email")
  public void get_check_user_by_email_false() throws Exception {

    EmailRequest emailRequest = new EmailRequest();
    emailRequest.setEmail(generator.generate(20));

    MockHttpServletRequestBuilder request = post(url).contentType(MediaType.APPLICATION_JSON)
        .content(ReflectionToJson.toString(emailRequest));

    ResultActions resultActions = mockMvc.perform(request).andExpect(status().isOk());
    String result = resultActions.andReturn().getResponse().getContentAsString();

    assertFalse(Boolean.valueOf(result));
  }

  @Then("^I find the user by username")
  public void get_check_user_by_username_true() throws Exception {
    ResultActions resultActions = mockMvc.perform(get(url)).andExpect(status().isOk());
    String result = resultActions.andReturn().getResponse().getContentAsString();
    assertTrue(Boolean.valueOf(result));
  }

  @Then("^I don't find the user by username")
  public void get_check_user_by_username_false() throws Exception {
    ResultActions resultActions = mockMvc.perform(get(url)).andExpect(status().isOk());
    String result = resultActions.andReturn().getResponse().getContentAsString();
    assertFalse(Boolean.valueOf(result));
  }

  private void createTenantRequest() {
    cloudosTenant.setAccountName(generator.generate(10));
    cloudosTenant.setDomainName(generator.generate(10));
    cloudosTenant.setRegion(generator.generate(10));
    cloudosTenant.setCompanySize(generator.generate(10));
    cloudosTenant.setCompanyName(generator.generate(10));

    setUser(cloudosTenant);

    createAccountAndCredentials();

  }

  private void setUser(CloudosTenant cloudosTenant) {

    User user = new User();

    String email = generator.generate(10);
    user.setEmail(email);

    String username = generator.generate(10);
    user.setUsername(username);

    String oldPassword = generator.generate(10);
    user.setPassword(oldPassword);
    user.setProfile(createUserProfile());
    user.setRole(UserRole.ADMIN);

    user.setProfile(createUserProfile());
    cloudosTenant.getUsers().add(user);

  }

  private UserProfile createUserProfile() {

    UserProfile profile = new UserProfile();
    profile.setFirstName(generator.generate(10));
    profile.setLastName(generator.generate(10));
    profile.setJobRole(generator.generate(10));
    profile.setCountry(generator.generate(10));
    profile.setState(generator.generate(10));
    profile.setCity(generator.generate(10));
    profile.setTelephone(generator.generate(10));
    profile.setPicture(generator.generate(10));

    UserPreferences preferences = new UserPreferences();
    preferences.setDateFormat(generator.generate(10));
    preferences.setTimeFormat(generator.generate(10));
    preferences.setAlertNotifications(true);
    preferences.setEmailNotification(true);
    preferences.setUse24HourFormat(true);
    preferences.setUseSSL(true);

    profile.setPreferences(preferences);
    return profile;

  }

  private void createAccountAndCredentials() {
    CloudosAccount account = new CloudosAccount();
    account.setAccountId(generator.generate(10));
    account.setAccountName(generator.generate(10));
    account.setProvider(Providers.AMAZON_AWS);
    cloudosTenant.getAccounts().add(account);
  }

}
