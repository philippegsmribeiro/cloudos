package feature.notifications;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import cloudos.notifications.Notification;
import cloudos.notifications.NotificationSearchRequest;
import cloudos.notifications.NotificationSearchResponse;
import cloudos.notifications.NotificationsController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import test.CloudOSTest;

public class NotificationSteps extends CloudOSTest {

  private MockMvc mockMvc;

  @Autowired NotificationsController notificationsController;

  @Before
  public void setUp() throws Exception {
    this.mockMvc =
        MockMvcBuilders.standaloneSetup(notificationsController).setViewResolvers().build();
  }

  @When("^the client list a notification")
  public void list() throws Throwable {
    NotificationSearchRequest request = new NotificationSearchRequest(0, 10, null, null);
    this.mockMvc
        .perform(post(NotificationsController.NOTIFICATIONS + "/list")
            .content(super.gson.toJson(request)).contentType(contentType))
        .andExpect(status().is2xxSuccessful());
  }

  @Then("^the client receive a notification")
  public void receive() throws Throwable {
    Notification notification = retrieveNotification();
    Assert.assertNotNull(notification);
  }

  @When("^the notification is read")
  public void notificationRead() throws Throwable {
    Notification notification = retrieveNotification();
    Assert.assertNotNull(notification);
    // Assert.assertTrue(notification.getReadUsers().contains("admin"));
  }

  private Notification retrieveNotification()
      throws UnsupportedEncodingException, Exception, IOException, JsonParseException,
          JsonMappingException {
    // retrieve a notification
    NotificationSearchRequest request = new NotificationSearchRequest(0, 1, null, null);
    String response =
        this.mockMvc
            .perform(post(NotificationsController.NOTIFICATIONS + "/list")
                .content(super.gson.toJson(request)).contentType(contentType))
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();
    Notification notification =
        mapper.readValue(response, NotificationSearchResponse.class).getNotifications().get(0);
    return notification;
  }

  @Then("^the client mark as read")
  public void markAsRead() throws Exception {
    String notificationId = retrieveNotification().getId();
    String contentAsString2 =
        this.mockMvc
            .perform(post(NotificationsController.NOTIFICATIONS + "/markAsRead/" + notificationId))
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();
    Assert.assertNotNull(contentAsString2);
  }

  @When("^the notification is unread")
  public void notificationUnread() throws Throwable {
    Notification notification = retrieveNotification();
    Assert.assertNotNull(notification);
    Assert.assertTrue(!notification.getReadUsers().contains("admin2"));
  }

  @Then("^the client mark as unread")
  public void markAsUnread() throws Exception {
    String notificationId = retrieveNotification().getId();
    String contentAsString2 =
        this.mockMvc
            .perform(
                post(NotificationsController.NOTIFICATIONS + "/markAsUnread/" + notificationId))
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();
    Assert.assertNotNull(contentAsString2);
  }
}
