package feature.notifications;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

import org.junit.Ignore;
import org.junit.runner.RunWith;

/** Created by philipperibeiro on 6/4/17. */
@RunWith(Cucumber.class)
@CucumberOptions(
  plugin = {"pretty", "html:target/cucumber"},
  tags = {"~@ignoreScenario"}
)
@Ignore
public class NotificationTest {}
