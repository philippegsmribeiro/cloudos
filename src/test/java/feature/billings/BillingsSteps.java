package feature.billings;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import cloudos.BillingsController;
import cloudos.Providers;
import cloudos.billings.BillingsService;
import cloudos.billings.CloudosReportBucket;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import test.CloudOSTest;

/** Created by philipperibeiro on 6/4/17. */
public class BillingsSteps extends CloudOSTest {

  private MockMvc mockMvc;

  @Autowired private BillingsService billingsService;

  private CloudosReportBucket reportBucket;

  private int status;

  protected String json(Object o) throws IOException {
    return mapper.writeValueAsString(o);
  }

  @Before
  public void setUp() throws Exception {

    this.mockMvc =
        MockMvcBuilders.standaloneSetup(new BillingsController(this.billingsService)).build();
  }

  @Given("^I have an unique bucket name")
  public void unique_bucket_name() throws Throwable {
    String bucket =
        String.format("bucket-%s", generator.generate(10));
    String region = "us-west-1";

    this.reportBucket = new CloudosReportBucket();

    this.reportBucket.setName(bucket);
    this.reportBucket.setRegion(region);
  }

  @Given("^I have an existing bucket name")
  public void get_existing_bucket() throws Throwable {
    List<CloudosReportBucket> reportBucketList = this.billingsService.getBuckets();
    assertNotNull(reportBucketList);
    assertThat(reportBucketList.size(), greaterThan(0));

    this.reportBucket = reportBucketList.get(0);
  }

  @When("^I lists all the buckets")
  public void the_client_list_buckets() throws Throwable {
    this.mockMvc.perform(get(BillingsController.BILLINGS + "/buckets")).andExpect(status().isOk());
  }

  @When("^I describe the bucket")
  public void describe_bucket() throws Throwable {
    mockMvc
        .perform(get(BillingsController.BILLINGS + "/buckets/" + this.reportBucket.getId()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(contentType))
        .andExpect(jsonPath("$.id", is(this.reportBucket.getId())))
        .andExpect(jsonPath("$.region", is(this.reportBucket.getRegion())))
        .andExpect(jsonPath("$.name", is(this.reportBucket.getName())));
  }

  @When("^I delete the bucket")
  public void delete_bucket() throws Throwable {
    List<CloudosReportBucket> reportBucketList = this.billingsService.getBuckets();
    assertNotNull(reportBucketList);
    assertThat(reportBucketList.size(), greaterThan(0));

    this.reportBucket = reportBucketList.get(0);

    mockMvc
        .perform(
            delete(BillingsController.BILLINGS + "/buckets")
                .content(json(this.reportBucket))
                .contentType(contentType))
        .andExpect(status().isOk());
  }

  @When("^I create a new bucket in the provider \"([^\"]*)\"")
  public void create_new_bucket(String provider) throws Throwable {
    switch (provider) {
      case "GOOGLE":
        this.reportBucket.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
        break;
      case "MICROSOFT":
        this.reportBucket.setProvider(Providers.MICROSOFT_AZURE);
        break;
      default:
        this.reportBucket.setProvider(Providers.AMAZON_AWS);
        break;
    }
    // post to the Billings Controller to create a new bucket
    mockMvc
        .perform(
            post(BillingsController.BILLINGS + "/buckets")
                .contentType(contentType)
                .content(json(this.reportBucket)))
        .andExpect(status().isCreated());
  }

  @When("^I attempt to save the bucket")
  public void save_bucket_already_exists() throws Throwable {
    // post to the Billings Controller to create a new bucket
    MvcResult result =
        mockMvc
            .perform(
                post(BillingsController.BILLINGS + "/buckets")
                    .contentType(contentType)
                    .content(json(this.reportBucket)))
            .andReturn();
    status = result.getResponse().getStatus();
  }

  @Then("^I should succeed")
  public void the_client_receives_ok_status_code() throws Throwable {
    mockMvc.perform(get(BillingsController.BILLINGS + "/buckets")).andExpect(status().isOk());
  }

  @Then("^I should fail")
  public void test_fail() throws Throwable {
    assertNotEquals(status, 201);
  }

  @And("^I receive a list of buckets")
  public void the_client_receives_list_of_buckets() throws Throwable {
    List<CloudosReportBucket> reportBucketList = this.billingsService.getBuckets();
    assertNotNull(reportBucketList);
    assertThat(reportBucketList.size(), greaterThanOrEqualTo(0));

    mockMvc
        .perform(get(BillingsController.BILLINGS + "/buckets"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(contentType))
        .andExpect(jsonPath("$", hasSize(reportBucketList.size())));
  }

  @And("^the bucket should exist")
  public void check_bucket_exists() throws Throwable {
    assertNotNull(this.reportBucket);
  }

  @And("^the bucket should not exist")
  public void check_bucket_not_exist() throws Throwable {
    assertNull(this.reportBucket);
  }
}
