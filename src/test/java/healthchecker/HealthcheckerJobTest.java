package healthchecker;

import static org.hamcrest.Matchers.hasEntry;
import static org.junit.Assert.assertNotNull;
import cloudos.Providers;
import cloudos.costanalysis.GenericBillingReportRepository;
import cloudos.healthchecker.HealthcheckerJob;
import cloudos.healthchecker.ResourceChart;
import cloudos.healthchecker.ResourcesCostEntry;
import cloudos.instances.InstanceService;
import cloudos.models.Instance;
import cloudos.models.costanalysis.GenericBillingReport;
import cloudos.provider.ProvidersService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import test.CloudOSTest;

/** Created by philipperibeiro on 6/27/17. */
public class HealthcheckerJobTest extends CloudOSTest {

  private static final String PAYER_ACCOUNT_ID = "287334376268";

  private static HealthcheckerJob healthcheckerJob;

  @Autowired
  private InstanceService instanceService;
  @Autowired
  private ProvidersService providersService;

  @Autowired
  private GenericBillingReportRepository genericBillingReportRepository;

  @BeforeClass
  public static void setUp() {
    healthcheckerJob = new HealthcheckerJob();
  }

  @Test
  //@Ignore
  public void testGetResourcesCostLast10Days() {
    List<Instance> instances = instanceService.findAllActiveInstances();

    List<GenericBillingReport> billingReports =
        genericBillingReportRepository.findByUsageStartTimeBetween(
            new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(30)),
            new Date(),
            new PageRequest(0, 100));

    List<ResourcesCostEntry> entries =
        healthcheckerJob.getResourcesCostLast10Days(instances, new ArrayList<>(billingReports), providersService.retrieveSupportedProviders());

    assertNotNull(entries);
    assertNotNull(hasEntry(entries, Providers.AMAZON_AWS));
  }

  @Test
  public void testUpdateResourcesEstCost() {
    List<Instance> instances =
        instanceService.findByCreationDateAfter(
            new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(30)));

    List<GenericBillingReport> billingReports =
        genericBillingReportRepository.findByUsageStartTimeBetween(
            new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(45)),
            new Date(),
            new PageRequest(0, 100));
    List<Instance> instanceList =
        healthcheckerJob.updateResourcesEstCost(instances, new ArrayList<>(billingReports));
    assertNotNull(instanceList);
  }

  @Test
  @Ignore
  public void testUpdateResourceCost() throws Exception {
    List<GenericBillingReport> reports =
        this.genericBillingReportRepository
            .findByAccountId(PAYER_ACCOUNT_ID, new PageRequest(0, 10));
    assertNotNull(reports);

    if (reports.size() > 0) {
      // get the first one
      GenericBillingReport report = reports.get(0);
      // fetch 100 reports
      List<GenericBillingReport> filteredReports =
          this.genericBillingReportRepository
              .findByResourceId(report.getResourceId(), new PageRequest(0, 100));
      assertNotNull(filteredReports);
      ResourceChart resourceChart =
          healthcheckerJob.updateResourceCost(
              new ArrayList<>(filteredReports), report.getResourceId(), Providers.AMAZON_AWS);
      assertNotNull(resourceChart);
      logger.debug(resourceChart);
    }
  }

  @AfterClass
  public static void tearDown() throws Exception {
    healthcheckerJob.stop();
  }
}
