package healthchecker;

import static org.junit.Assert.assertNotNull;

import cloudos.Providers;
import cloudos.healthchecker.ResourceCostPair;
import cloudos.healthchecker.ResourcesCost;
import cloudos.healthchecker.ResourcesCostEntry;
import cloudos.healthchecker.ResourcesCostRepository;

import java.text.SimpleDateFormat;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

/** Created by philipperibeiro on 6/27/17. */
public class ResourcesCostTest extends CloudOSTest {

  @Autowired ResourcesCostRepository costRepository;

  @Before
  public void setUp() throws Exception {

    LinkedList<ResourceCostPair> datapoints = new LinkedList<>();
    LinkedList<ResourcesCostEntry> resources = new LinkedList<>();
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    datapoints.add(new ResourceCostPair(formatter.parse("2017-05-28T17:00:00"), 1.0201));
    datapoints.add(new ResourceCostPair(formatter.parse("2017-05-29T13:00:00"), 3.03898644));
    datapoints.add(new ResourceCostPair(formatter.parse("2017-05-28T20:00:00"), 3.06050001));
    datapoints.add(new ResourceCostPair(formatter.parse("2017-05-29T04:00:00"), 0.04032258));
    datapoints.add(new ResourceCostPair(formatter.parse("2017-05-28T19:00:00"), 9.120969840000004));
    datapoints.add(new ResourceCostPair(formatter.parse("2017-05-28T21:00:00"), 1.02016896));
    datapoints.add(new ResourceCostPair(formatter.parse("2017-05-29T06:00:00"), 1.00000028));
    datapoints.add(
        new ResourceCostPair(formatter.parse("2017-05-29T12:00:00"), 1.0201612899999999));
    datapoints.add(
        new ResourceCostPair(formatter.parse("2017-05-29T00:00:00"), 1.0201612899999999));
    datapoints.add(
        new ResourceCostPair(formatter.parse("2017-05-29T11:00:00"), 1.0201612899999999));
    datapoints.add(new ResourceCostPair(formatter.parse("2017-05-28T18:00:00"), 1.02016157));
    datapoints.add(new ResourceCostPair(formatter.parse("2017-05-29T01:00:00"), 1.02036873));
    datapoints.add(new ResourceCostPair(formatter.parse("2017-05-29T03:00:00"), 1.02016915));

    ResourcesCostEntry entry = new ResourcesCostEntry(Providers.AMAZON_AWS, datapoints);
    resources.add(entry);

    double totalCost = datapoints.stream().mapToDouble(ResourceCostPair::getCost).sum();
    ResourcesCost resourcesCost = new ResourcesCost(resources, totalCost);
    this.costRepository.save(resourcesCost);
  }

  @Test
  public void testGetLastestResourcesCost() throws Exception {
    ResourcesCost resourcesCost = this.costRepository.findFirstByOrderByTimestampDesc();
    assertNotNull(resourcesCost);
    logger.debug(resourcesCost);
  }
}
