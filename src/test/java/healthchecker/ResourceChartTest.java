package healthchecker;

import cloudos.Providers;
import cloudos.healthchecker.ResourceChart;
import cloudos.healthchecker.ResourceChartRepository;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.CloudOSTest;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

/**
 * Created by philipperibeiro on 7/3/17.
 */
public class ResourceChartTest extends CloudOSTest {

    private final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    ResourceChartRepository chartRepository;

    @Before
    public void setUp() throws Exception {
        Map<Long, BigDecimal> chart = new HashMap<>();
        chart.put(new Date().getTime(), BigDecimal.valueOf(10.0));
        ResourceChart resourceChart =
                new ResourceChart(null, "i-212312131231", BigDecimal.valueOf(10.0), chart, new Date(),
                        Providers.AMAZON_AWS);

        if (this.chartRepository.findFirstByResourceIdOrderByTimestampDesc("i-212312131231") == null) {
            this.chartRepository.save(resourceChart);
        }
    }

    @Test
    public void testAddResourceChart() throws Exception {
        ResourceChart current = this.chartRepository.findFirstByOrderByTimestampDesc();
        assertNotNull(current);
    }
}
