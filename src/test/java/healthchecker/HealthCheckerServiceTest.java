package healthchecker;

import cloudos.healthchecker.HealthCheckerService;
import cloudos.healthchecker.ResourcesCost;
import cloudos.healthchecker.ResourcesCostRepository;
import cloudos.instances.InstanceService;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.CloudOSTest;

/** Created by gleimar on 26/03/2017. */
public class HealthCheckerServiceTest extends CloudOSTest {

  @Autowired
  private InstanceService instanceService;

  @Autowired private ResourcesCostRepository costRepository;

  private HealthCheckerService healthCheckerService;

  @Before
  public void setUp() throws Exception {
    this.healthCheckerService = new HealthCheckerService(instanceService, costRepository);
  }

  @Test
  public void testGetLast10DaysCost() throws Exception {
    logger.debug("Healthchecker Service: {}", this.healthCheckerService);
    ResourcesCost cost = this.healthCheckerService.getLast10DaysCost();
    logger.debug("Latest Resource Cost: {}", cost);
    // assertNotNull(cost);
  }
}
