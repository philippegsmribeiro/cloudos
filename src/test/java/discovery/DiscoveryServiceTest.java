package discovery;

import static org.junit.Assert.fail;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

import org.junit.After;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.amazonaws.regions.Regions;

import cloudos.AmazonService;
import cloudos.CredentialManagerService;
import cloudos.Providers;
import cloudos.credentials.CloudCredentialActionException;
import cloudos.credentials.CloudCredentialActionRequest;
import cloudos.discovery.DiscoveryService;
import cloudos.exceptions.NotImplementedException;
import cloudos.google.GoogleCredential;
import cloudos.models.AwsCredential;
import cloudos.models.CloudosCredentialRepository;
import cloudos.utils.FileUtils;
import test.CloudOSTest;

public class DiscoveryServiceTest extends CloudOSTest {

  @Value("${google.credentials.project}")
  private String googleProject;

  @Value("${google.credentials.json}")
  private String json;

  @Autowired
  private DiscoveryService discoveryService;

  @Autowired
  private CredentialManagerService credentialManagerService;

  @Autowired
  private CloudosCredentialRepository cloudosCredentialRepository;

  @Autowired
  private AmazonService amazonService;

  @Test
  public void testGoogleIntegrationCredentialsChange() throws Exception {

    GoogleCredential oldCredential = (GoogleCredential) cloudosCredentialRepository
        .findByProvider(Providers.GOOGLE_COMPUTE_ENGINE).get(0);
    credentialManagerService.decryptSensitiveData(oldCredential);

    byte[] readAllBytes = Files.readAllBytes(Paths
        .get(FileUtils.getFileFromResourcesFile(json, this.getClass().getClassLoader()).toURI()));
    GoogleCredential googleCredential =
        new GoogleCredential(googleProject, new String(readAllBytes));

    CloudCredentialActionRequest cloudCreateCredentialRequest = new CloudCredentialActionRequest();
    cloudCreateCredentialRequest.setCloudosCredential(googleCredential);
    credentialManagerService.save(cloudCreateCredentialRequest);

    // change back
    cloudCreateCredentialRequest = new CloudCredentialActionRequest();
    cloudCreateCredentialRequest.setCloudosCredential(oldCredential);
    credentialManagerService.save(cloudCreateCredentialRequest);

  }

  @Test
  @Ignore
  public void testAWSCredentialsChange() throws Exception {
    //
    Set<com.amazonaws.services.ec2.model.Instance> listInstances =
        amazonService.listInstances(Regions.US_EAST_1.getName(), null);
    int size = listInstances.size();

    AwsCredential oldCredential =
        (AwsCredential) cloudosCredentialRepository.findByProvider(Providers.AMAZON_AWS).get(0);
    credentialManagerService.decryptSensitiveData(oldCredential);

    String accessKeyId = "AKIAIUBBD5QOWZIVCYBA";
    String secretAccessKey = "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";
    AwsCredential credential = new AwsCredential(accessKeyId, secretAccessKey);

    CloudCredentialActionRequest cloudCreateCredentialRequest = new CloudCredentialActionRequest();
    cloudCreateCredentialRequest.setCloudosCredential(credential);
    credentialManagerService.save(cloudCreateCredentialRequest);

    //
    Set<com.amazonaws.services.ec2.model.Instance> listInstances2 =
        amazonService.listInstances(Regions.US_EAST_1.getName(), null);
    int size2 = listInstances2.size();

    if (size == size2 && size != 0) {
      // assertNotEquals(listInstances.iterator().next().getInstanceId().getProject(),
      // listInstances2.iterator().next().getInstanceId().getProject());
    }

    // change back
    cloudCreateCredentialRequest = new CloudCredentialActionRequest();
    cloudCreateCredentialRequest.setCloudosCredential(oldCredential);
    credentialManagerService.save(cloudCreateCredentialRequest);

    Set<com.amazonaws.services.ec2.model.Instance> listInstances3 =
        amazonService.listInstances(Regions.US_EAST_1.getName(), null);
    int size3 = listInstances3.size();
    if (size != size3) {
      fail("fail changing back");
    }
  }

  @Test
  public void discoveryGoogle() throws NotImplementedException, CloudCredentialActionException {
    GoogleCredential oldCredential = (GoogleCredential) cloudosCredentialRepository
        .findByProvider(Providers.GOOGLE_COMPUTE_ENGINE).get(0);
    credentialManagerService.decryptSensitiveData(oldCredential);
    discoveryService.executeDiscovery(oldCredential);
  }

  @Test
  public void discoveryAWS() throws NotImplementedException, CloudCredentialActionException {
    AwsCredential oldCredential =
        (AwsCredential) cloudosCredentialRepository.findByProvider(Providers.AMAZON_AWS).get(0);
    credentialManagerService.decryptSensitiveData(oldCredential);
    discoveryService.executeDiscovery(oldCredential);
  }

  @After
  public void after() throws Exception {
    this.credentialManagerService.setDefaultCredentials();
  }
}
