package elasticsearch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.elasticsearch.search.SearchHit;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import cloudos.Providers;
import cloudos.elasticsearch.model.DiskIODataPoint;
import cloudos.elasticsearch.model.KibanaSearchParameters;
import cloudos.elasticsearch.model.NetworkIODataPoint;
import cloudos.elasticsearch.model.ProcessDataPoint;
import cloudos.elasticsearch.model.ValueDataPoint;
import cloudos.elasticsearch.model.charts.CpuUsagePctChart;
import cloudos.elasticsearch.model.charts.MemoryUsageBytesChart;
import cloudos.elasticsearch.model.charts.NetworkTrafficBytesChart;
import cloudos.elasticsearch.model.charts.NetworkTrafficPacketsChart;
import cloudos.elasticsearch.model.charts.SystemLoadChart;
import cloudos.elasticsearch.services.MetricbeatService;
import cloudos.instances.InstanceService;
import cloudos.models.Instance;
import cloudos.models.InstanceStatus;
import cloudos.queue.QueueServiceException;
import cloudos.queue.message.AbstractQueueMessage;
import cloudos.queue.message.QueueMessageType;
import cloudos.queue.message.beats.MetricbeatMessage;

/**
 * Created by rogerio.souza on 05/14/17.
 *
 * <p>
 * Tests Elasticsearch API services for Metricbeat
 * 
 */
public class MetricbeatServiceTest extends AbstractElasticSearchServiceTest {

  @Autowired
  private MetricbeatService metricbeatService;

  @Autowired
  private InstanceService instanceService;

  @Before
  public void setup() throws UnknownHostException {
    setup(metricbeatService);
  }

  @Test
  public void testMetricbeatShipData()
      throws UnknownHostException, QueueServiceException, InterruptedException {
    assertTrue(queueService.purgeQueue(QueueMessageType.METRICBEAT));
    List<SearchHit> searchHitList = metricbeatService.shipData(60, 10);

    List<String> hitJsonList = new ArrayList<>();
    for (SearchHit searchHit : searchHitList) {
      hitJsonList.add(searchHit.getSourceAsString());
    }
    
 // wait for the messages
    int tries = 0;
    while (tries < 3) {
      if(queueService.countMessages(QueueMessageType.METRICBEAT) == searchHitList.size()) {
        break;
      }
      // wait
      Thread.sleep(1000);
      tries++;
    }
    
    assertEquals(searchHitList.size(), queueService.countMessages(QueueMessageType.METRICBEAT));

    List<AbstractQueueMessage> messages = queueService.readMessages(QueueMessageType.METRICBEAT);

    for (AbstractQueueMessage message : messages) {
      assertTrue(message.getClass().equals(MetricbeatMessage.class));
      assertTrue(message.getType().equals(QueueMessageType.METRICBEAT));
      MetricbeatMessage beatMessage = (MetricbeatMessage) message;
      assertTrue(hitJsonList.contains(beatMessage.getHitJson()));
    }

    assertTrue(queueService.deleteMessages(messages));
  }

  @Test
  public void testTop10SystemProcessCpuTotalPct() throws Exception {
    List<ValueDataPoint> processList =
        metricbeatService.getTopSystemProcessCpuTotalPct(defautKibanaSearchParameters, TOP_10);

    if (CollectionUtils.isNotEmpty(processList)) {

      assertEquals(10, processList.size());
      ValueDataPoint topProcess = processList.get(0);
      for (ValueDataPoint process : processList) {
        assertNotNull(process.getValue());
        assertNotNull(process.getDescription());
        if (!process.getValue().isNaN()) {
          assertTrue(topProcess.getValue() >= process.getValue());
        }
        topProcess = process;
      }
    }
  }

  @Test
  public void testTop100SystemProcessCpuTotalPct() throws Exception {

    List<ValueDataPoint> processList =
        metricbeatService.getTopSystemProcessCpuTotalPct(defautKibanaSearchParameters, TOP_100);

    if (CollectionUtils.isNotEmpty(processList)) {
      ValueDataPoint topProcess = processList.get(0);
      for (ValueDataPoint process : processList) {
        assertNotNull(process.getValue());
        assertNotNull(process.getDescription());
        if (!process.getValue().isNaN()) {
          assertTrue(topProcess.getValue() >= process.getValue());
        }
        topProcess = process;
      }
    }
  }

  @Test
  public void testTop10SystemProcessMemoryRssPct() throws Exception {
    List<ValueDataPoint> processList =
        metricbeatService.getTopSystemProcessMemoryRssPct(defautKibanaSearchParameters, TOP_10);

    if (CollectionUtils.isNotEmpty(processList)) {
      assertEquals(10, processList.size());
      ValueDataPoint topProcess = processList.get(0);
      for (ValueDataPoint process : processList) {
        assertNotNull(process.getValue());
        assertNotNull(process.getDescription());
        if (!process.getValue().isNaN()) {
          assertTrue(topProcess.getValue() >= process.getValue());
        }
        topProcess = process;
      }
    }
  }

  @Test
  public void testTop100SystemProcessMemoryRssPct() throws Exception {

    metricbeatService.getSystemCpuCores(defautKibanaSearchParameters);

    List<ValueDataPoint> processList =
        metricbeatService.getTopSystemProcessMemoryRssPct(defautKibanaSearchParameters, TOP_100);

    if (CollectionUtils.isNotEmpty(processList)) {
      ValueDataPoint topProcess = processList.get(0);
      for (ValueDataPoint process : processList) {
        assertNotNull(process.getValue());
        assertNotNull(process.getDescription());
        if (!process.getValue().isNaN()) {
          assertTrue(topProcess.getValue() >= process.getValue());
        }
        topProcess = process;
      }
    }
  }

  @Test
  public void testTop10SystemProcesses() throws Exception {
    List<ProcessDataPoint> processList =
        metricbeatService.getTopSystemProcesses(defautKibanaSearchParameters, TOP_10);

    if (CollectionUtils.isNotEmpty(processList)) {
      assertEquals(10, processList.size());
      ProcessDataPoint topProcess = processList.get(0);
      for (ProcessDataPoint process : processList) {
        assertNotNull(process.getName());
        assertNotNull(process.getUser());
        assertNotNull(process.getCpuPct());
        assertNotNull(process.getMemoryPct());
        if (!process.getCpuPct().isNaN()) {
          assertTrue(topProcess.getCpuPct() >= process.getCpuPct());
        }
        topProcess = process;
      }
    }
  }

  @Test
  public void testTop100SystemProcesses() throws Exception {
    List<ProcessDataPoint> processList =
        metricbeatService.getTopSystemProcesses(defautKibanaSearchParameters, TOP_100);

    if (CollectionUtils.isNotEmpty(processList)) {
      ProcessDataPoint topProcess = processList.get(0);
      for (ProcessDataPoint process : processList) {
        assertNotNull(process.getName());
        assertNotNull(process.getUser());
        assertNotNull(process.getCpuPct());
        assertNotNull(process.getMemoryPct());
        if (!process.getCpuPct().isNaN()) {
          assertTrue(topProcess.getCpuPct() >= process.getCpuPct());
        }
        topProcess = process;
      }
    }
  }

  @Test
  public void testCpuUsagePctChartDataByInstance() throws Exception {

    KibanaSearchParameters parameters = defautKibanaSearchParameters;
    CpuUsagePctChart cpuUsagePctChart = metricbeatService.getCpuUsagePctChartData(parameters);

    assertNotNull(cpuUsagePctChart);

    assertNotNull(cpuUsagePctChart.getSystemCpuIowaitPctAvg());
    assertNotNull(cpuUsagePctChart.getSystemCpuIrqPctAvg());
    assertNotNull(cpuUsagePctChart.getSystemCpuNicePctAvg());
    assertNotNull(cpuUsagePctChart.getSystemCpuSoftirqPctAvg());
    assertNotNull(cpuUsagePctChart.getSystemCpuSystemPctAvg());
    assertNotNull(cpuUsagePctChart.getSystemCpuUserPctAvg());

    assertNotNull(cpuUsagePctChart.getSystemCpuIowaitPctDateHistogramAvg());
    assertNotNull(cpuUsagePctChart.getSystemCpuIrqPctDateHistogramAvg());
    assertNotNull(cpuUsagePctChart.getSystemCpuNicePctDateHistogramAvg());
    assertNotNull(cpuUsagePctChart.getSystemCpuSoftirqPctDateHistogramAvg());
    assertNotNull(cpuUsagePctChart.getSystemCpuSystemPctDateHistogramAvg());
    assertNotNull(cpuUsagePctChart.getSystemCpuUserPctDateHistogramAvg());

    assertEquals(61, cpuUsagePctChart.getSystemCpuIowaitPctDateHistogramAvg().size());
    assertEquals(61, cpuUsagePctChart.getSystemCpuIrqPctDateHistogramAvg().size());
    assertEquals(61, cpuUsagePctChart.getSystemCpuNicePctDateHistogramAvg().size());
    assertEquals(61, cpuUsagePctChart.getSystemCpuSoftirqPctDateHistogramAvg().size());
    assertEquals(61, cpuUsagePctChart.getSystemCpuSystemPctDateHistogramAvg().size());
    assertEquals(61, cpuUsagePctChart.getSystemCpuUserPctDateHistogramAvg().size());
  }


  @Test
  public void testMemoryUsageBytesChartDataByInstance() throws Exception {

    MemoryUsageBytesChart memoryUsageBytesChart =
        metricbeatService.getMemoryUsageBytesChartData(defautKibanaSearchParameters);

    assertNotNull(memoryUsageBytesChart);

    assertNotNull(memoryUsageBytesChart.getSystemMemoryFreeAvg());
    assertNotNull(memoryUsageBytesChart.getSystemMemorySwapAvg());
    assertNotNull(memoryUsageBytesChart.getSystemMemoryUsedAvg());
    assertNotNull(memoryUsageBytesChart.getSystemMemoryTotal());

    assertNotNull(memoryUsageBytesChart.getSystemMemoryFreeDateHistogramAvg());
    assertNotNull(memoryUsageBytesChart.getSystemMemorySwapDateHistogramAvg());
    assertNotNull(memoryUsageBytesChart.getSystemMemoryUsedDateHistogramAvg());

    assertEquals(61, memoryUsageBytesChart.getSystemMemoryFreeDateHistogramAvg().size());
    assertEquals(61, memoryUsageBytesChart.getSystemMemorySwapDateHistogramAvg().size());
    assertEquals(61, memoryUsageBytesChart.getSystemMemoryUsedDateHistogramAvg().size());

  }

  @Test
  public void testNetworkTrafficBytesChart() throws Exception {

    NetworkTrafficBytesChart networkTrafficBytesChart =
        metricbeatService.getNetworkTrafficBytesChart(defautKibanaSearchParameters);

    assertNotNull(networkTrafficBytesChart);

    assertNotNull(networkTrafficBytesChart.getSystemNetworkInBytesAvg());
    assertNotNull(networkTrafficBytesChart.getSystemNetworkOutBytesAvg());

    assertNotNull(networkTrafficBytesChart.getSystemNetworkInBytesDateHistogramAvg());
    assertNotNull(networkTrafficBytesChart.getSystemNetworkOutBytesDateHistogramAvg());

    assertEquals(61, networkTrafficBytesChart.getSystemNetworkInBytesDateHistogramAvg().size());
    assertEquals(61, networkTrafficBytesChart.getSystemNetworkOutBytesDateHistogramAvg().size());
  }

  @Test
  public void testNetworkTrafficPacketsChart() throws Exception {

    NetworkTrafficPacketsChart networkTrafficPacketsChart =
        metricbeatService.getNetworkTrafficPacketsChart(defautKibanaSearchParameters);

    assertNotNull(networkTrafficPacketsChart);

    assertNotNull(networkTrafficPacketsChart.getSystemNetworkInPacketsAvg());
    assertNotNull(networkTrafficPacketsChart.getSystemNetworkOutPacketsAvg());

    assertNotNull(networkTrafficPacketsChart.getSystemNetworkInPacketsDateHistogramAvg());
    assertNotNull(networkTrafficPacketsChart.getSystemNetworkOutPacketsDateHistogramAvg());

    assertEquals(61, networkTrafficPacketsChart.getSystemNetworkInPacketsDateHistogramAvg().size());
    assertEquals(61,
        networkTrafficPacketsChart.getSystemNetworkOutPacketsDateHistogramAvg().size());
  }

  @Test
  public void testTop10HostnamesDiskIO() throws Exception {
    List<DiskIODataPoint> hostnameList =
        metricbeatService.getTopHostnamesDiskIO(defautKibanaSearchParameters, TOP_10);

    if (CollectionUtils.isNotEmpty(hostnameList)) {
      assertTrue(hostnameList.size() <= 10);
      DiskIODataPoint topHostname = hostnameList.get(0);
      for (DiskIODataPoint hostname : hostnameList) {
        assertNotNull(hostname.getHostname());
        assertNotNull(hostname.getReadBytes());
        assertNotNull(hostname.getReadTime());
        assertNotNull(hostname.getWriteBytes());
        assertNotNull(hostname.getWriteTime());
        if (!hostname.getReadBytes().isNaN()) {
          assertTrue(topHostname.getReadBytes() >= hostname.getReadBytes());
        }
        topHostname = hostname;
      }
    }
  }

  @Test
  public void testTop100HostnamesDiskIO() throws Exception {
    List<DiskIODataPoint> hostnameList =
        metricbeatService.getTopHostnamesDiskIO(defautKibanaSearchParameters, TOP_100);

    if (CollectionUtils.isNotEmpty(hostnameList)) {
      DiskIODataPoint topHostname = hostnameList.get(0);
      for (DiskIODataPoint hostname : hostnameList) {
        assertNotNull(hostname.getHostname());
        assertNotNull(hostname.getReadBytes());
        assertNotNull(hostname.getReadTime());
        assertNotNull(hostname.getWriteBytes());
        assertNotNull(hostname.getWriteTime());
        if (!hostname.getReadBytes().isNaN()) {
          assertTrue(topHostname.getReadBytes() >= hostname.getReadBytes());
        }
        topHostname = hostname;
      }
    }
  }

  @Test
  public void test10TopHostnamesNetwork() throws Exception {
    List<NetworkIODataPoint> hostnameList =
        metricbeatService.getTopHostnamesNetwork(defautKibanaSearchParameters, TOP_10);

    if (CollectionUtils.isNotEmpty(hostnameList)) {
      assertTrue(hostnameList.size() <= 10);
      NetworkIODataPoint topHostname = hostnameList.get(0);
      for (NetworkIODataPoint hostname : hostnameList) {
        assertNotNull(hostname.getHostname());
        assertNotNull(hostname.getInBytes());
        assertNotNull(hostname.getInPackets());
        assertNotNull(hostname.getOutByes());
        assertNotNull(hostname.getOutPackets());
        if (!hostname.getInBytes().isNaN()) {
          assertTrue(topHostname.getInBytes() >= hostname.getInBytes());
        }
        topHostname = hostname;
      }
    }
  }

  @Test
  public void test100TopHostnamesNetwork() throws Exception {
    List<NetworkIODataPoint> hostnameList =
        metricbeatService.getTopHostnamesNetwork(defautKibanaSearchParameters, TOP_100);

    if (CollectionUtils.isNotEmpty(hostnameList)) {
      NetworkIODataPoint topHostname = hostnameList.get(0);
      for (NetworkIODataPoint hostname : hostnameList) {
        assertNotNull(hostname.getHostname());
        assertNotNull(hostname.getInBytes());
        assertNotNull(hostname.getInPackets());
        assertNotNull(hostname.getOutByes());
        assertNotNull(hostname.getOutPackets());
        if (!hostname.getInBytes().isNaN()) {
          assertTrue(topHostname.getInBytes() >= hostname.getInBytes());
        }
        topHostname = hostname;
      }
    }
  }

  @Test
  public void testSystemLoadChart() throws Exception {

    SystemLoadChart systemLoadChart =
        metricbeatService.getSystemLoadChart(defautKibanaSearchParameters);

    assertNotNull(systemLoadChart.getSystemLoad1Avg());
    assertNotNull(systemLoadChart.getSystemLoad5Avg());
    assertNotNull(systemLoadChart.getSystemLoad15Avg());

    assertNotNull(systemLoadChart.getSystemLoad1DateHistogramAvg());
    assertNotNull(systemLoadChart.getSystemLoad5DateHistogramAvg());
    assertNotNull(systemLoadChart.getSystemLoad15DateHistogramAvg());

    assertEquals(61, systemLoadChart.getSystemLoad1DateHistogramAvg().size());
    assertEquals(61, systemLoadChart.getSystemLoad5DateHistogramAvg().size());
    assertEquals(61, systemLoadChart.getSystemLoad15DateHistogramAvg().size());
  }

  @Test
  public void testLast2ValuesDifference() throws UnknownHostException {

    assertNotNull(metricbeatService.getSystemNetworkInBytes(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemNetworkInPackets(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemNetworkOutBytes(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemNetworkOutPackets(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemNetworkInDropped(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemNetworkInErrors(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemNetworkOutDropped(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemNetworkOutErrors(defautKibanaSearchParameters));
  }

  @Test
  public void testGetLastFieldValue() throws UnknownHostException {

    assertNotNull(metricbeatService.getMetricsetRtt(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemCpuTotalUsagePct(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemCpuIdlePct(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemCpuIowaitPct(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemCpuIrqPct(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemCpuNicePct(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemCpuSoftirqPct(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemCpuStealPct(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemCpuSystemPct(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemCpuUserPct(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemFilesystemAvailable(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemFilesystemFiles(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemFilesystemFree(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemFilesystemFreeFiles(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemFilesystemTotal(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemFilesystemUsedBytes(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemFilesystemUsedPct(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemLoad1(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemLoad15(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemLoad5(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemLoadNorm1(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemLoadNorm15(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemLoadNorm5(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemMemoryActualFree(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemMemoryActualUsedBytes(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemMemoryActualUsedPct(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemMemoryFree(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemMemorySwapFree(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemMemorySwapTotal(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemMemorySwapUsedBytes(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemMemorySwapUsedPct(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemMemoryTotal(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemMemoryUsedBytes(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemMemoryUsedPct(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemCpuCores(defautKibanaSearchParameters));
  }

  @Test
  public void testFieldAvg() throws UnknownHostException {

    assertNotNull(metricbeatService.getMetricsetRttAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemCpuIdlePctAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemCpuIowaitPctAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemCpuIrqPctAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemCpuNicePctAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemCpuSoftirqPctAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemCpuStealPctAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemCpuSystemPctAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemCpuUserPctAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemFilesystemAvailableAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemFilesystemFilesAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemFilesystemFreeAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemFilesystemFreeFilesAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemFilesystemTotalAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemFilesystemUsedBytesAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemFilesystemUsedPctAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemLoad1Avg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemLoad15Avg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemLoad5Avg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemLoadNorm1Avg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemLoadNorm15Avg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemLoadNorm5Avg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemMemoryActualFreeAvg(defautKibanaSearchParameters));
    assertNotNull(
        metricbeatService.getSystemMemoryActualUsedBytesAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemMemoryActualUsedPctAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemMemoryFreeAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemMemorySwapFreeAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemMemorySwapTotalAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemMemorySwapUsedBytesAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemMemorySwapUsedPctAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemMemoryTotalAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemMemoryUsedBytesAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemMemoryUsedPctAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemNetworkInBytesAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemNetworkInDroppedAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemNetworkInErrorsAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemNetworkInPacketsAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemNetworkOutBytesAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemNetworkOutDroppedAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemNetworkOutErrorsAvg(defautKibanaSearchParameters));
    assertNotNull(metricbeatService.getSystemNetworkOutPacketsAvg(defautKibanaSearchParameters));
  }

  @Test
  public void testMemoryUsageBytesChartDataByProvider() throws Exception {

    List<Instance> createInstances = createInstances();

    KibanaSearchParameters parameters = new KibanaSearchParameters();
    Long rangeFrom = parameters.getRangeFrom();
    Long rangeTo = parameters.getRangeTo();

    MemoryUsageBytesChart chart1 = metricbeatService
        .getMemoryUsageBytesChartDataByProvider(Providers.AMAZON_AWS, rangeFrom, rangeTo);

    assertNotNull(chart1);

    assertNotNull(chart1.getSystemMemoryFreeAvg());
    assertNotNull(chart1.getSystemMemorySwapAvg());
    assertNotNull(chart1.getSystemMemoryUsedAvg());
    assertNotNull(chart1.getSystemMemoryTotal());

    assertNotNull(chart1.getSystemMemoryFreeDateHistogramAvg());
    assertNotNull(chart1.getSystemMemorySwapDateHistogramAvg());
    assertNotNull(chart1.getSystemMemoryUsedDateHistogramAvg());

    assertEquals(61, chart1.getSystemMemoryFreeDateHistogramAvg().size());
    assertEquals(61, chart1.getSystemMemorySwapDateHistogramAvg().size());
    assertEquals(61, chart1.getSystemMemoryUsedDateHistogramAvg().size());

    createInstances.stream().forEach(i -> instanceService.delete(i.getId()));

  }

  private List<Instance> createInstances() throws UnknownHostException, InterruptedException {
    String beatnameInstance1 = metricbeatService.getLastBeatName();
    Instance instance1 = new Instance();
    instance1.setProvider(Providers.AMAZON_AWS);
    instance1.setName(beatnameInstance1);
    instance1.setStatus(InstanceStatus.RUNNING);
    instance1.setDeleted(false);
    instance1.setProviderId("1");

    instance1 = instanceService.save(instance1);

    String beatnameInstance2 = metricbeatService.getLastBeatName();

    int count = 0;
    while (beatnameInstance1.equals(beatnameInstance2)) {
      Thread.sleep(1000);
      beatnameInstance2 = metricbeatService.getLastBeatName();

      if (count > 30) {
        fail("There is no instance on ES beats data!!!");
      }
    }

    Instance instance2 = new Instance();
    instance2.setProvider(Providers.AMAZON_AWS);
    instance2.setName(beatnameInstance2);
    instance2.setStatus(InstanceStatus.RUNNING);
    instance2.setDeleted(false);
    instance2.setProviderId("2");

    instance2 = instanceService.save(instance2);

    return Arrays.asList(instance1, instance2);
  }


}
