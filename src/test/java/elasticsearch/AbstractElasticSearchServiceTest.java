package elasticsearch;

import static org.junit.Assert.fail;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import cloudos.elasticsearch.fields.SharedBeatFields;
import cloudos.elasticsearch.model.KibanaField;
import cloudos.elasticsearch.model.KibanaSearchParameters;
import cloudos.elasticsearch.model.KibanaSearchParametersTransformer;
import cloudos.elasticsearch.services.AbstractElasticsearchService;
import cloudos.queue.QueueService;
import test.CloudOSTest;

abstract class AbstractElasticSearchServiceTest extends CloudOSTest {

  public static final Integer TOP_10 = 10;
  public static final Integer TOP_100 = 100;
  public static String instanceBeatName = null;
  public static KibanaSearchParameters defautKibanaSearchParameters = null;

  @Autowired
  protected QueueService queueService;

  @Autowired
  protected KibanaSearchParametersTransformer transformer;

  protected void setup(AbstractElasticsearchService service) throws UnknownHostException {
    if (StringUtils.isBlank(instanceBeatName)) {
      instanceBeatName = service.getLastBeatName();
      if (StringUtils.isBlank(instanceBeatName)) {
        fail("There is no instance on ES beats data!!!");
      }
      defautKibanaSearchParameters = buildInstanceBeatNameKibanaSearchParameters(instanceBeatName);
    }
  }

  protected KibanaSearchParameters buildInstanceBeatNameKibanaSearchParameters(
      String instanceBeatName) {
    KibanaSearchParameters parameters = new KibanaSearchParameters();

    KibanaField beatname =
        new KibanaField(SharedBeatFields.BEAT_NAME, Arrays.asList(instanceBeatName));

    List<KibanaField> filterFields = new ArrayList<>();
    filterFields.add(beatname);
    parameters.setFilterFields(filterFields);

    return parameters;
  }

}
