package elasticsearch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.elasticsearch.search.SearchHit;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cloudos.elasticsearch.services.HeartbeatService;
import cloudos.queue.QueueServiceException;
import cloudos.queue.message.AbstractQueueMessage;
import cloudos.queue.message.QueueMessageType;
import cloudos.queue.message.beats.HeartbeatMessage;

/**
 * Created by rogerio.souza on 05/14/17.
 *
 * <p>
 * Tests Elasticsearch API services for Heartbeat
 */
public class HeartbeatServiceTest extends AbstractElasticSearchServiceTest {

  @Autowired
  private HeartbeatService heartbeatService;

  @Before
  public void setup() throws UnknownHostException {
    setup(heartbeatService);
  }

  @Test
  public void testHeartbeatShipData()
      throws UnknownHostException, QueueServiceException, InterruptedException {
    assertTrue(queueService.purgeQueue(QueueMessageType.HEARTBEAT));
    List<SearchHit> searchHitList = heartbeatService.shipData(60, 10);

    List<String> hitJsonList = new ArrayList<>();
    for (SearchHit searchHit : searchHitList) {
      hitJsonList.add(searchHit.getSourceAsString());
    }

    // wait for the messages
    int tries = 0;
    while (tries < 3) {
      if(queueService.countMessages(QueueMessageType.HEARTBEAT) == searchHitList.size()) {
        break;
      }
      // wait
      Thread.sleep(1000);
      tries++;
    }
    
    assertEquals(searchHitList.size(), queueService.countMessages(QueueMessageType.HEARTBEAT));
    
    List<AbstractQueueMessage> messages = queueService.readMessages(QueueMessageType.HEARTBEAT);

    for (AbstractQueueMessage message : messages) {
      assertTrue(message.getClass().equals(HeartbeatMessage.class));
      assertTrue(message.getType().equals(QueueMessageType.HEARTBEAT));
      HeartbeatMessage beatMessage = (HeartbeatMessage) message;
      assertTrue(hitJsonList.contains(beatMessage.getHitJson()));
    }

    assertTrue(queueService.deleteMessages(messages));
  }

  @Test
  public void testFieldAvg() throws UnknownHostException {

    assertNotNull(heartbeatService.getDurationUsAvg(defautKibanaSearchParameters));
    assertNotNull(heartbeatService.getResolveRttUsAvg(defautKibanaSearchParameters));
    assertNotNull(heartbeatService.getIcmpRttUsAvg(defautKibanaSearchParameters));
    assertNotNull(heartbeatService.getTcpConnectRttUsAvg(defautKibanaSearchParameters));
    assertNotNull(heartbeatService.getSocks5ConnectRttUsAvg(defautKibanaSearchParameters));
    assertNotNull(heartbeatService.getTlsHandshakeRttUsAvg(defautKibanaSearchParameters));
    assertNotNull(heartbeatService.getHttpRttUsAvg(defautKibanaSearchParameters));
    assertNotNull(heartbeatService.getValidateRttUsAvg(defautKibanaSearchParameters));
    assertNotNull(heartbeatService.getResponseStatusAvg(defautKibanaSearchParameters));
    assertNotNull(heartbeatService.getUpAvg(defautKibanaSearchParameters));

  }

}
