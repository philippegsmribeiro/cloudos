package elasticsearch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.elasticsearch.search.SearchHit;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cloudos.elasticsearch.services.PacketbeatService;
import cloudos.queue.QueueServiceException;
import cloudos.queue.message.AbstractQueueMessage;
import cloudos.queue.message.QueueMessageType;
import cloudos.queue.message.beats.PacketbeatMessage;

/**
 * Created by rogerio.souza on 05/14/17.
 *
 * <p>
 * Tests Elasticsearch API services for Packetbeat
 */
public class PacketbeatServiceTest extends AbstractElasticSearchServiceTest {

  @Autowired
  private PacketbeatService packetbeatService;

  @Before
  public void setup() throws UnknownHostException {
    setup(packetbeatService);
  }

  @Test
  public void testPacketbeatShipData()
      throws UnknownHostException, QueueServiceException, InterruptedException {
    assertTrue(queueService.purgeQueue(QueueMessageType.PACKETBEAT));
    List<SearchHit> searchHitList = packetbeatService.shipData(60, 10);

    List<String> hitJsonList = new ArrayList<>();
    for (SearchHit searchHit : searchHitList) {
      hitJsonList.add(searchHit.getSourceAsString());
    }

 // wait for the messages
    int tries = 0;
    while (tries < 3) {
      if(queueService.countMessages(QueueMessageType.PACKETBEAT) == searchHitList.size()) {
        break;
      }
      // wait
      Thread.sleep(1000);
      tries++;
    }
    
    assertEquals(searchHitList.size(), queueService.countMessages(QueueMessageType.PACKETBEAT));

    List<AbstractQueueMessage> messages = queueService.readMessages(QueueMessageType.PACKETBEAT);

    for (AbstractQueueMessage message : messages) {
      assertTrue(message.getClass().equals(PacketbeatMessage.class));
      assertTrue(message.getType().equals(QueueMessageType.PACKETBEAT));
      PacketbeatMessage beatMessage = (PacketbeatMessage) message;
      assertTrue(hitJsonList.contains(beatMessage.getHitJson()));
    }

    assertTrue(queueService.deleteMessages(messages));
  }

  @Test
  public void testFieldAvg() throws UnknownHostException {

    assertNotNull(packetbeatService.getBytesInAvg(defautKibanaSearchParameters));
    assertNotNull(packetbeatService.getBytesOutAvg(defautKibanaSearchParameters));
    assertNotNull(packetbeatService.getCountAvg(defautKibanaSearchParameters));
    assertNotNull(packetbeatService.getHttpContentLengthAvg(defautKibanaSearchParameters));
    assertNotNull(packetbeatService.getResponsetimeAvg(defautKibanaSearchParameters));
    assertNotNull(packetbeatService.getShipperAvg(defautKibanaSearchParameters));

  }

}
