package elasticsearch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.search.SearchHit;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cloudos.elasticsearch.model.KibanaSearchParameters;
import cloudos.elasticsearch.model.LogHit;
import cloudos.elasticsearch.model.LogSearchParameters;
import cloudos.elasticsearch.services.FilebeatService;
import cloudos.queue.QueueServiceException;
import cloudos.queue.message.AbstractQueueMessage;
import cloudos.queue.message.QueueMessageType;
import cloudos.queue.message.beats.FilebeatMessage;

/**
 * Created by rogerio.souza on 05/14/17.
 *
 * <p>
 * Tests Elasticsearch API services for Filebeat
 */
public class FilebeatServiceTest extends AbstractElasticSearchServiceTest {

  @Autowired
  private FilebeatService filebeatService;

  @Test
  public void testFilebeatShipData()
      throws UnknownHostException, QueueServiceException, InterruptedException {
    assertTrue(queueService.purgeQueue(QueueMessageType.FILEBEAT));
    List<SearchHit> searchHitList = filebeatService.shipData(60, 10);

    List<String> hitJsonList = new ArrayList<>();
    for (SearchHit searchHit : searchHitList) {
      hitJsonList.add(searchHit.getSourceAsString());
    }
    
 // wait for the messages
    int tries = 0;
    while (tries < 3) {
      if(queueService.countMessages(QueueMessageType.FILEBEAT) == searchHitList.size()) {
        break;
      }
      // wait
      Thread.sleep(1000);
      tries++;
    }
    
    assertEquals(searchHitList.size(), queueService.countMessages(QueueMessageType.FILEBEAT));

    List<AbstractQueueMessage> messages = queueService.readMessages(QueueMessageType.FILEBEAT);

    for (AbstractQueueMessage message : messages) {
      assertTrue(message.getClass().equals(FilebeatMessage.class));
      assertTrue(message.getType().equals(QueueMessageType.FILEBEAT));
      FilebeatMessage beatMessage = (FilebeatMessage) message;
      assertTrue(hitJsonList.contains(beatMessage.getHitJson()));
    }

    assertTrue(queueService.deleteMessages(messages));
  }

  @Test
  public void testGetHitsByMessageQueryWord() throws UnknownHostException {

    String lastErrorMessage = filebeatService.getLastErrorMessage(new KibanaSearchParameters());

    if (StringUtils.isEmpty(lastErrorMessage)) {
      fail("There is no error message to search for!!!");
    }

    StringTokenizer st = new StringTokenizer(lastErrorMessage);

    while (st.hasMoreElements()) {

      String token = st.nextElement().toString();

      logger.debug(token);

      // Ignoring strings with numbers
      if (token.matches(".*\\d+.*")) {
        continue;
      }

      LogSearchParameters logSearchParameters = new LogSearchParameters();
      logSearchParameters.setSize(10);
      logSearchParameters.setTimestampRangeFrom(new Date().getTime() - TimeUnit.HOURS.toMillis(24));
      logSearchParameters.setTimestampRangeTo(new Date().getTime());

      logSearchParameters.setQuery(token);

      List<LogHit> logHits =
          filebeatService.getHitsByMessageQueryWord(transformer.apply(logSearchParameters));

      assertNotNull(logHits);
      assertTrue(logHits.size() > 0);
      assertTrue(logHits.size() <= 10);
      for (LogHit logHit : logHits) {
        assertNotNull(logHit.getHostname());
        assertNotNull(logHit.getName());
        assertNotNull(logHit.getSource());
        assertNotNull(logHit.getTimestamp());
        assertNotNull(logHit.getType());
        assertNotNull(logHit.getMessage());

        assertTrue(logHit.getMessage().toUpperCase()
            .contains(logSearchParameters.getQuery().toUpperCase()));
      }
    }

  }


}
