package tenant;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cloudos.Providers;
import cloudos.credentials.AmazonCredentialManagement;
import cloudos.credentials.CloudCredentialActionException;
import cloudos.encryptionKeys.AmazonKMSService;
import cloudos.encryptionKeys.AmazonKMSServiceException;
import cloudos.exceptions.InvalidRequestException;
import cloudos.models.AwsCredential;
import cloudos.models.CloudosAccount;
import cloudos.models.CloudosCredentialRepository;
import cloudos.models.CloudosTenant;
import cloudos.models.CloudosTenantRepository;
import cloudos.user.UserPreferences;
import cloudos.user.UserProfile;
import cloudos.tenant.TenantManagerService;
import cloudos.user.User;
import cloudos.user.UserRole;
import cloudos.user.UserService;
import test.CloudOSTest;

public class TenantManagerServiceTest extends CloudOSTest {

  @Autowired
  private TenantManagerService tenantManagerService;

  @Autowired
  private CloudosCredentialRepository credentialRepository;

  @Autowired
  private AmazonKMSService amazonKMSService;

  @Autowired
  private CloudosTenantRepository cloudosTenantRepository;

  @Autowired
  private AmazonCredentialManagement amazonCredentialManagement;

  @Autowired
  private UserService userService;

  @Test
  public void testTenantCRUD() throws Exception {

    CloudosTenant cloudosTenant = new CloudosTenant();

    cloudosTenant.setAccountName(generator.generate(10));
    cloudosTenant.setDomainName(generator.generate(10));
    cloudosTenant.setRegion(generator.generate(10));
    cloudosTenant.setCompanySize(generator.generate(10));
    cloudosTenant.setCompanyName(generator.generate(10));
    setUser(cloudosTenant);
    createAccountAndCredentials(cloudosTenant);

    User nonEncryptedUser = new User();
    BeanUtils.copyProperties(nonEncryptedUser, cloudosTenant.getUsers().get(0));

    UserProfile nonEncryptedProfile = new UserProfile();
    BeanUtils.copyProperties(nonEncryptedProfile, nonEncryptedUser.getProfile());
    nonEncryptedUser.setProfile(nonEncryptedProfile);

    AwsCredential nonEncryptedCredential = new AwsCredential();
    BeanUtils.copyProperties(nonEncryptedCredential,
        cloudosTenant.getAccounts().get(0).getCredentials().get(0));

    CloudosTenant savedCloudosTenant = tenantManagerService.save(cloudosTenant);
    cloudosTenant.getUsers().set(0, nonEncryptedUser);
    cloudosTenant.getAccounts().get(0).getCredentials().set(0, nonEncryptedCredential);

    String generatedId = savedCloudosTenant.getId();
    savedCloudosTenant = tenantManagerService.getTenant(generatedId);
    assertAttributes(cloudosTenant, savedCloudosTenant);
    assertEncryptedAttributes(cloudosTenant, savedCloudosTenant);

    tenantManagerService.delete(generatedId);
    savedCloudosTenant = tenantManagerService.getTenant(generatedId);
    assertNull(savedCloudosTenant);
    assertFalse(tenantManagerService.tenantExists(cloudosTenant.getDomainName()));

  }

  private void setUser(CloudosTenant cloudosTenant) {

    User user = new User();

    String email = generator.generate(10);
    user.setEmail(email);

    String username = generator.generate(10);
    user.setUsername(username);

    String oldPassword = generator.generate(10);
    user.setPassword(oldPassword);
    user.setProfile(createUserProfile());
    user.setRole(UserRole.ADMIN);

    user.setProfile(createUserProfile());
    cloudosTenant.getUsers().add(user);

  }

  private void createAccountAndCredentials(CloudosTenant cloudosTenant)
      throws CloudCredentialActionException {
    CloudosAccount account = new CloudosAccount();
    account.setProvider(Providers.AMAZON_AWS);
    account.setAccountId(generator.generate(10));
    account.setAccountName(generator.generate(10));

    AwsCredential credential =
        (AwsCredential) credentialRepository.findByProvider(Providers.AMAZON_AWS).get(0);
    amazonCredentialManagement.decryptSensitiveData(credential);

    credential.setAccessKeyId(credential.getAccessKeyId());
    credential.setSecretAccessKey(credential.getSecretAccessKey());
    credential.setProvider(Providers.AMAZON_AWS);

    account.getCredentials().add(credential);

    cloudosTenant.getAccounts().add(account);
  }

  private UserProfile createUserProfile() {

    UserProfile profile = new UserProfile();
    profile.setFirstName(generator.generate(10));
    profile.setLastName(generator.generate(10));
    profile.setJobRole(generator.generate(10));
    profile.setCountry(generator.generate(10));
    profile.setState(generator.generate(10));
    profile.setCity(generator.generate(10));
    profile.setTelephone(generator.generate(10));
    profile.setPicture(generator.generate(10));

    UserPreferences preferences = new UserPreferences();
    preferences.setDateFormat(generator.generate(10));
    preferences.setTimeFormat(generator.generate(10));
    preferences.setAlertNotifications(true);
    preferences.setEmailNotification(true);
    preferences.setUse24HourFormat(true);
    preferences.setUseSSL(true);

    profile.setPreferences(preferences);
    return profile;

  }

  private void assertAttributes(CloudosTenant cloudosTenant, CloudosTenant savedCloudosTenant)
      throws AmazonKMSServiceException {
    assertNotNull(savedCloudosTenant);
    assertNotNull(savedCloudosTenant.getId());
    assertEquals(cloudosTenant.getId(), savedCloudosTenant.getId());
    assertEquals(cloudosTenant.getAccountName(), savedCloudosTenant.getAccountName());
    assertEquals(cloudosTenant.getDomainName(), savedCloudosTenant.getDomainName());
    assertEquals(cloudosTenant.getRegion(), savedCloudosTenant.getRegion());
    assertEquals(cloudosTenant.getCompanySize(), savedCloudosTenant.getCompanySize());
    assertEquals(cloudosTenant.getCompanyName(), savedCloudosTenant.getCompanyName());
    assertEquals(cloudosTenant.getUsers().size(), savedCloudosTenant.getUsers().size());
    assertEquals(cloudosTenant.getAccounts().size(), savedCloudosTenant.getAccounts().size());

    User user = cloudosTenant.getUsers().get(0);
    User savedUser = savedCloudosTenant.getUsers().get(0);
    assertEquals(user.getUsername(), savedUser.getUsername());
    assertEquals(amazonKMSService.encryptMessage(amazonKMSService.getUserEncryptedDataKey(),
        user.getPassword()), savedUser.getPassword());

    UserProfile profile = user.getProfile();
    UserProfile savedProfile = savedUser.getProfile();
    assertEquals(profile.getFirstName(), savedProfile.getFirstName());
    assertEquals(profile.getLastName(), savedProfile.getLastName());
    assertEquals(profile.getJobRole(), savedProfile.getJobRole());
    assertEquals(profile.getCountry(), savedProfile.getCountry());
    assertEquals(profile.getState(), savedProfile.getState());
    assertEquals(profile.getCity(), savedProfile.getCity());
    assertEquals(profile.getTelephone(), savedProfile.getTelephone());
    assertEquals(profile.getPicture(), savedProfile.getPicture());

    UserPreferences preferences = user.getProfile().getPreferences();
    UserPreferences savedPreferences = savedUser.getProfile().getPreferences();
    assertEquals(preferences.getDateFormat(), savedPreferences.getDateFormat());
    assertEquals(preferences.getTimeFormat(), savedPreferences.getTimeFormat());
    assertEquals(preferences.isAlertNotifications(), savedPreferences.isAlertNotifications());
    assertEquals(preferences.isEmailNotification(), savedPreferences.isEmailNotification());
    assertEquals(preferences.isUse24HourFormat(), savedPreferences.isUse24HourFormat());
    assertEquals(preferences.isUseSSL(), savedPreferences.isUseSSL());

    AwsCredential credential =
        (AwsCredential) cloudosTenant.getAccounts().get(0).getCredentials().get(0);
    AwsCredential savedCredential =
        (AwsCredential) savedCloudosTenant.getAccounts().get(0).getCredentials().get(0);
    assertEquals(credential.getAccessKeyId(), savedCredential.getAccessKeyId());
    assertEquals(credential.getSecretAccessKey(), savedCredential.getSecretAccessKey());

  }

  private void assertEncryptedAttributes(CloudosTenant cloudosTenant,
      CloudosTenant savedCloudosTenant) throws AmazonKMSServiceException {

    savedCloudosTenant = cloudosTenantRepository.findOne(savedCloudosTenant.getId());

    assertNotNull(savedCloudosTenant);
    assertNotNull(savedCloudosTenant.getId());
    assertNotNull(amazonKMSService.getUserEncryptedDataKey());
    assertEquals(cloudosTenant.getId(), savedCloudosTenant.getId());
    assertEquals(cloudosTenant.getAccountName(), savedCloudosTenant.getAccountName());
    assertEquals(cloudosTenant.getDomainName(), savedCloudosTenant.getDomainName());
    assertEquals(cloudosTenant.getRegion(), savedCloudosTenant.getRegion());
    assertEquals(cloudosTenant.getCompanySize(), savedCloudosTenant.getCompanySize());
    assertEquals(cloudosTenant.getCompanyName(), savedCloudosTenant.getCompanyName());
    assertEquals(cloudosTenant.getUsers().size(), savedCloudosTenant.getUsers().size());
    assertEquals(cloudosTenant.getAccounts().size(), savedCloudosTenant.getAccounts().size());

    User user = cloudosTenant.getUsers().get(0);
    User savedUser = userService.findById(savedCloudosTenant.getUsers().get(0).getId());
    assertEquals(amazonKMSService.encryptMessage(amazonKMSService.getUserEncryptedDataKey(),
        user.getUsername()), savedUser.getUsername());
    assertEquals(amazonKMSService.encryptMessage(amazonKMSService.getUserEncryptedDataKey(),
        user.getPassword()), savedUser.getPassword());

    UserProfile profile = user.getProfile();
    UserProfile savedProfile = savedUser.getProfile();
    assertEquals(amazonKMSService.encryptMessage(amazonKMSService.getUserEncryptedDataKey(),
        profile.getFirstName()), savedProfile.getFirstName());
    assertEquals(amazonKMSService.encryptMessage(amazonKMSService.getUserEncryptedDataKey(),
        profile.getLastName()), savedProfile.getLastName());
    assertEquals(profile.getJobRole(), savedProfile.getJobRole());
    assertEquals(profile.getCountry(), savedProfile.getCountry());
    assertEquals(profile.getState(), savedProfile.getState());
    assertEquals(profile.getCity(), savedProfile.getCity());
    assertEquals(amazonKMSService.encryptMessage(amazonKMSService.getUserEncryptedDataKey(),
        profile.getTelephone()), savedProfile.getTelephone());
    assertEquals(profile.getPicture(), savedProfile.getPicture());

    UserPreferences preferences = user.getProfile().getPreferences();
    UserPreferences savedPreferences = savedUser.getProfile().getPreferences();
    assertEquals(preferences.getDateFormat(), savedPreferences.getDateFormat());
    assertEquals(preferences.getTimeFormat(), savedPreferences.getTimeFormat());
    assertEquals(preferences.isAlertNotifications(), savedPreferences.isAlertNotifications());
    assertEquals(preferences.isEmailNotification(), savedPreferences.isEmailNotification());
    assertEquals(preferences.isUse24HourFormat(), savedPreferences.isUse24HourFormat());
    assertEquals(preferences.isUseSSL(), savedPreferences.isUseSSL());

  }

  @Test
  public void shouldRaiseBeanValidationExceptions() {

    CloudosTenant cloudosTenant = new CloudosTenant();

    try {
      tenantManagerService.save(cloudosTenant);
      fail("Validation exception should have been raised");
    } catch (Exception e) {
      assertTrue(e.getClass().equals(InvalidRequestException.class));
    }

  }

  @Test
  public void getRegionsByCountryFlag() throws InvalidRequestException {
    List<String> regions = tenantManagerService.getRegionsByCountryFlag("USA");
    assertEquals(regions.get(0), "us-east-1");
    assertEquals(regions.get(1), "us-east-2");
    assertEquals(regions.get(2), "us-west-1");
    assertEquals(regions.get(3), "us-west-2");

    regions = tenantManagerService.getRegionsByCountryFlag("AUS");
    assertEquals(regions.get(0), "ap-southeast-2");

    regions = tenantManagerService.getRegionsByCountryFlag("EUR");
    assertEquals(regions.get(0), "eu-central-1");
    assertEquals(regions.get(1), "eu-west-1");
    assertEquals(regions.get(2), "eu-west-2");

    regions = tenantManagerService.getRegionsByCountryFlag("CHN");
    assertEquals(regions.get(0), "ap-southeast-1");
    assertEquals(regions.get(1), "ap-northeast-2");

    regions = tenantManagerService.getRegionsByCountryFlag("BRA");
    assertEquals(regions.get(0), "sa-east-1");

    regions = tenantManagerService.getRegionsByCountryFlag("JPN");
    assertEquals(regions.get(0), "ap-northeast-1");

    try {
      regions = tenantManagerService.getRegionsByCountryFlag(null);
      fail("Validation exception should have been raised");
    } catch (Exception e) {
      assertTrue(e.getClass().equals(InvalidRequestException.class));
    }

    try {
      regions = tenantManagerService.getRegionsByCountryFlag("");
      fail("Validation exception should have been raised");
    } catch (Exception e) {
      assertTrue(e.getClass().equals(InvalidRequestException.class));
    }

    try {
      regions = tenantManagerService.getRegionsByCountryFlag("     ");
      fail("Validation exception should have been raised");
    } catch (Exception e) {
      assertTrue(e.getClass().equals(InvalidRequestException.class));
    }

    try {
      regions = tenantManagerService.getRegionsByCountryFlag("POLAND");
      fail("Validation exception should have been raised");
    } catch (Exception e) {
      assertTrue(e.getClass().equals(InvalidRequestException.class));
    }

  }

}
