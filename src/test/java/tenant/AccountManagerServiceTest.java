package tenant;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cloudos.Providers;
import cloudos.exceptions.InvalidRequestException;
import cloudos.models.CloudosAccount;
import cloudos.models.CloudosTenant;
import cloudos.user.UserPreferences;
import cloudos.user.UserProfile;
import cloudos.tenant.AccountManagerService;
import cloudos.tenant.TenantManagerService;
import cloudos.user.User;
import cloudos.user.UserRole;
import test.CloudOSTest;

public class AccountManagerServiceTest extends CloudOSTest {

  @Autowired
  private TenantManagerService tenantManagerService;

  @Autowired
  private AccountManagerService accountManagerService;

  @Test
  public void testAccountCRUD() throws Exception {

    CloudosTenant cloudosTenant = createTenant();

    CloudosAccount account = new CloudosAccount();
    account.setProvider(Providers.AMAZON_AWS);
    account.setAccountId(generator.generate(10));
    account.setAccountName(generator.generate(10));

    account = accountManagerService.create(account);
    String generatedId = cloudosTenant.getId();
    account = accountManagerService.getAccount(generatedId);

    assertNotNull(cloudosTenant);
    assertEquals(generatedId, cloudosTenant.getId());

    assertEquals(generatedId, cloudosTenant.getId());

    accountManagerService.delete(generatedId);
    tenantManagerService.delete(cloudosTenant.getId());

    account = accountManagerService.getAccount(generatedId);
    assertNull(account);

  }

  private CloudosTenant createTenant() throws Exception {
    CloudosTenant cloudosTenant = new CloudosTenant();
    cloudosTenant.setAccountName(generator.generate(10));
    cloudosTenant.setDomainName(generator.generate(10));
    cloudosTenant.setCompanySize(generator.generate(10));
    cloudosTenant.setCompanyName(generator.generate(10));

    cloudosTenant.setRegion(generator.generate(10));

    setUser(cloudosTenant);

    return tenantManagerService.save(cloudosTenant);

  }

  private void setUser(CloudosTenant cloudosTenant) {

    User user = new User();

    String email = generator.generate(10);
    user.setEmail(email);

    String username = generator.generate(10);
    user.setUsername(username);

    String oldPassword = generator.generate(10);
    user.setPassword(oldPassword);
    user.setProfile(createUserProfile());
    user.setRole(UserRole.ADMIN);

    user.setProfile(createUserProfile());
    cloudosTenant.getUsers().add(user);

  }

  private UserProfile createUserProfile() {

    UserProfile profile = new UserProfile();
    profile.setFirstName(generator.generate(10));
    profile.setLastName(generator.generate(10));
    profile.setJobRole(generator.generate(10));
    profile.setCountry(generator.generate(10));
    profile.setState(generator.generate(10));
    profile.setCity(generator.generate(10));
    profile.setTelephone(generator.generate(10));
    profile.setPicture(generator.generate(10));

    UserPreferences preferences = new UserPreferences();
    preferences.setDateFormat(generator.generate(10));
    preferences.setTimeFormat(generator.generate(10));
    preferences.setAlertNotifications(true);
    preferences.setEmailNotification(true);
    preferences.setUse24HourFormat(true);
    preferences.setUseSSL(true);

    profile.setPreferences(preferences);
    return profile;

  }

  @Test
  public void shouldRaiseBeanValidationExceptions() {

    CloudosAccount cloudosAccount = new CloudosAccount();

    try {
      accountManagerService.create(cloudosAccount);
      fail("Validation exception should have been raised");
    } catch (Exception e) {
      assertTrue(e.getClass().equals(InvalidRequestException.class));
    }

  }

}
