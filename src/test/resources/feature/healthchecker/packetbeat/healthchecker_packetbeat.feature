Feature: The HealthCheckerPacketbeat API 
Scenario: Post resource shipper_avg 
  When I post the resource shipper_avg 
  Then I should succeed
  
Scenario: Post resource responsetime_avg 
  When I post the resource responsetime_avg 
  Then I should succeed
  
Scenario: Post resource http_content_length_avg 
  When I post the resource http_content_length_avg 
  Then I should succeed
  
Scenario: Post resource count_avg 
  When I post the resource count_avg 
  Then I should succeed
  
Scenario: Post resource bytes_out_avg 
  When I post the resource bytes_out_avg 
  Then I should succeed
  
Scenario: Post resource bytes_in_avg 
  When I post the resource bytes_in_avg 
  Then I should succeed
   