Feature: The HealthCheckerHeartbeat API 
Scenario: Post resource response_status_avg 
  When I post the resource response_status_avg 
  Then I should succeed
  
Scenario: Post resource up_avg 
  When  I post the resource up_avg 
  Then I should succeed
  
Scenario: Post resource validate_rtt_us_avg 
  When  I post the resource validate_rtt_us_avg 
  Then I should succeed
  
Scenario: Post resource tls_handshake_rtt_us_avg 
  When  I post the resource tls_handshake_rtt_us_avg 
  Then I should succeed
  
Scenario: Post resource socks5_connect_rtt_us_avg 
  When I post the resource socks5_connect_rtt_us_avg 
  Then I should succeed
  
Scenario: Post resource tcp_connect_rtt_us_avg 
  When  I post the resource tcp_connect_rtt_us_avg 
  Then I should succeed
  
Scenario: Post resource icmp_rtt_us_avg 
  When  I post the resource icmp_rtt_us_avg 
  Then I should succeed
  
Scenario: Post resource resolve_rtt_us_avg 
  When  I post the resource resolve_rtt_us_avg 
  Then I should succeed
  
Scenario: Post resource duration_us_avg 
  When  I post the resource duration_us_avg 
  Then I should succeed
  
