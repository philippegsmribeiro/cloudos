Feature: The HealthCheckerMetricbeat API 
Scenario: Post resource top_10_hostnames_network 
  When I post the resource top_10_hostnames_network 
  Then I should succeed
  
Scenario: Post resource top_10_hostnames_disk_io 
  When I post the resource top_10_hostnames_disk_io 
  Then I should succeed
  
Scenario: Post resource network_traffic_packets_chart 
  When I post the resource network_traffic_packets_chart 
  Then I should succeed
  
Scenario: Post resource network_traffic_bytes_chart 
  When I post the resource network_traffic_bytes_chart 
  Then I should succeed
  
Scenario: Post resource network_traffic_bytes_chart 
  When I post the resource network_traffic_bytes_chart 
  Then I should succeed
  
Scenario: Post resource top_10_process_cpu_pct 
  When I post the resource top_10_process_cpu_pct 
  Then I should succeed
  
Scenario: Post resource top_10_system_process_memory_rss_pct 
  When I post the resource top_10_system_process_memory_rss_pct 
  Then I should succeed
  
Scenario: Post resource top_10_processes 
  When I post the resource top_10_processes 
  Then I should succeed
  
Scenario: Post resource memory_usage_bytes_chart 
  When I post the resource memory_usage_bytes_chart 
  Then I should succeed
  
Scenario: Post resource cpu_usage_pct_chart 
  When I post the resource cpu_usage_pct_chart 
  Then I should succeed
  
Scenario: Post resource system_cpu_total_usage_pct 
  When I post the resource system_cpu_total_usage_pct 
  Then I should succeed
  
Scenario: Post resource system_cpu_idle_pct_avg 
  When I post the resource _system_cpu_idle_pct_avg 
  Then I should succeed
  
Scenario: Post resource system_cpu_iowait_pct_avg 
  When I post the resource _system_cpu_iowait_pct_avg 
  Then I should succeed
  
Scenario: Post resource system_cpu_irq_pct_avg 
  When I post the resource _system_cpu_irq_pct_avg 
  Then I should succeed
  
Scenario: Post resource system_cpu_nice_pct_avg 
  When I post the resource _system_cpu_nice_pct_avg 
  Then I should succeed
  
Scenario: Post resource system_cpu_soft_irq_pct_avg 
  When I post the resource _system_cpu_soft_irq_pct_avg 
  Then I should succeed
  
Scenario: Post resource system_cpu_steal_pct_avg 
  When I post the resource _system_cpu_steal_pct_avg 
  Then I should succeed
  
Scenario: Post resource system_cpu_system_pct_avg 
  When I post the resource _system_cpu_system_pct_avg 
  Then I should succeed
  
Scenario: Post resource system_cpu_user_pct_avg 
  When I post the resource _system_cpu_user_pct_avg 
  Then I should succeed
  
Scenario: Post resource system_filesystem_available_avg 
  When I post the resource _system_filesystem_available_avg 
  Then I should succeed
  
Scenario: Post resource system_filesystem_files_avg 
  When I post the resource _system_filesystem_files_avg 
  Then I should succeed
  
Scenario: Post resource _0system_filesystem_free_avg 
  When I post the resource _0system_filesystem_free_avg 
  Then I should succeed
  
Scenario: Post resource _0system_filesystem_free_files_avg 
  When I post the resource _0system_filesystem_free_files_avg 
  Then I should succeed
  
Scenario: Post resource system_filesystem_total_avg 
  When I post the resource _system_filesystem_total_avg 
  Then I should succeed
  
Scenario: Post resource system_filesystem_used_bytes_avg 
  When I post the resource _system_filesystem_used_bytes_avg 
  Then I should succeed
  
Scenario: Post resource system_filesystem_used_pct_avg 
  When I post the resource _system_filesystem_used_pct_avg 
  Then I should succeed
  
Scenario: Post resource system_load1_avg 
  When I post the resource _system_load1_avg 
  Then I should succeed
  
Scenario: Post resource _0system_load15_avg 
  When I post the resource _0system_load15_avg 
  Then I should succeed
  
Scenario: Post resource _system_load5_avg 
  When I post the resource _system_load5_avg 
  Then I should succeed
  
Scenario: Post resource _system_load_norm1_avg 
  When I post the resource _system_load_norm1_avg 
  Then I should succeed
  
Scenario: Post resource _system_load_norm15_avg 
  When I post the resource _system_load_norm15_avg 
  Then I should succeed
  
Scenario: Post resource _system_load_norm5_avg 
  When I post the resource _system_load_norm5_avg 
  Then I should succeed
  
Scenario: Post resource _system_memory_actual_free_avg 
  When I post the resource _system_memory_actual_free_avg 
  Then I should succeed
  
Scenario: Post resource _system_memory_actual_used_bytes_avg 
  When I post the resource _system_memory_actual_used_bytes_avg 
  Then I should succeed
  
Scenario: Post resource _system_memory_actual_used_pct_avg 
  When I post the resource _system_memory_actual_used_pct_avg 
  Then I should succeed
  
Scenario: Post resource _system_memory_free_avg 
  When I post the resource _system_memory_free_avg 
  Then I should succeed
  
Scenario: Post resource _system_memory_swap_free_avg 
  When I post the resource _system_memory_swap_free_avg 
  Then I should succeed
  
Scenario: Post resource _system_memory_swap_total_avg 
  When I post the resource _system_memory_swap_total_avg 
  Then I should succeed
  
Scenario: Post resource _system_memory_swap_used_bytes_avg 
  When I post the resource _system_memory_swap_used_bytes_avg 
  Then I should succeed
  
Scenario: Post resource _system_memory_swap_used_pct_avg 
  When I post the resource _system_memory_swap_used_pct_avg 
  Then I should succeed
  
Scenario: Post resource _system_memory_total_avg 
  When I post the resource _system_memory_total_avg 
  Then I should succeed
  
Scenario: Post resource _system_memory_used_bytes_avg 
  When I post the resource _system_memory_used_bytes_avg 
  Then I should succeed
  
Scenario: Post resource _system_memory_used_pct_avg 
  When I post the resource _system_memory_used_pct_avg 
  Then I should succeed
  
Scenario: Post resource _system_network_in_bytes_avg 
  When I post the resource _system_network_in_bytes_avg 
  Then I should succeed
  
Scenario: Post resource _system_network_in_dropped_avg 
  When I post the resource _system_network_in_dropped_avg 
  Then I should succeed
  
Scenario: Post resource _system_network_in_errors_avg 
  When I post the resource _system_network_in_errors_avg 
  Then I should succeed
  
Scenario: Post resource _system_network_in_packets_avg 
  When I post the resource _system_network_in_packets_avg 
  Then I should succeed
  
Scenario: Post resource _system_network_out_bytes_avg 
  When I post the resource _system_network_out_bytes_avg 
  Then I should succeed
  
Scenario: Post resource _system_network_out_dropped_avg 
  When I post the resource _system_network_out_dropped_avg 
  Then I should succeed
  
Scenario: Post resource _system_network_out_errors_avg 
  When I post the resource _system_network_out_errors_avg 
  Then I should succeed
  
Scenario: Post resource _system_network_out_packets_avg 
  When I post the resource _system_network_out_packets_avg 
  Then I should succeed
  
Scenario: Post resource metricset_rtt 
  When I post the resource metricset_rtt 
  Then I should succeed
  
Scenario: Post resource system_cpu_idle_pct 
  When I post the resource system_cpu_idle_pct 
  Then I should succeed
  
Scenario: Post resource system_cpu_iowait_pct 
  When I post the resource system_cpu_iowait_pct 
  Then I should succeed
  
Scenario: Post resource system_cpu_irq_pct 
  When I post the resource system_cpu_irq_pct 
  Then I should succeed
  
Scenario: Post resource system_cpu_nice_pct 
  When I post the resource system_cpu_nice_pct 
  Then I should succeed
  
Scenario: Post resource system_cpu_soft_irq_pct 
  When I post the resource system_cpu_soft_irq_pct 
  Then I should succeed
  
Scenario: Post resource system_cpu_steal_pct 
  When I post the resource system_cpu_steal_pct 
  Then I should succeed
  
Scenario: Post resource system_cpu_system_pct 
  When I post the resource system_cpu_system_pct 
  Then I should succeed
  
Scenario: Post resource system_cpu_user_pct 
  When I post the resource system_cpu_user_pct 
  Then I should succeed
  
Scenario: Post resource system_filesystem_available 
  When I post the resource system_filesystem_available 
  Then I should succeed
  
Scenario: Post resource system_filesystem_files 
  When I post the resource system_filesystem_files 
  Then I should succeed
  
Scenario: Post resource _system_filesystem_free 
  When I post the resource _system_filesystem_free 
  Then I should succeed
  
Scenario: Post resource system_filesystem_free_files 
  When I post the resource system_filesystem_free_files 
  Then I should succeed
  
Scenario: Post resource system_filesystem_total 
  When I post the resource system_filesystem_total 
  Then I should succeed
  
Scenario: Post resource system_filesystem_used_bytes 
  When I post the resource system_filesystem_used_bytes 
  Then I should succeed
  
Scenario: Post resource system_filesystem_used_pct 
  When I post the resource system_filesystem_used_pct 
  Then I should succeed
  
Scenario: Post resource system_load1 
  When I post the resource system_load1 
  Then I should succeed
  
Scenario: Post resource _system_load15 
  When I post the resource _system_load15 
  Then I should succeed
  
Scenario: Post resource system_load5 
  When I post the resource system_load5 
  Then I should succeed
  
Scenario: Post resource system_load_norm1 
  When I post the resource system_load_norm1 
  Then I should succeed
  
Scenario: Post resource _0system_load_norm15 
  When I post the resource _0system_load_norm15 
  Then I should succeed
  
Scenario: Post resource system_load_norm5 
  When I post the resource system_load_norm5 
  Then I should succeed
  
Scenario: Post resource system_memory_actual_free 
  When I post the resource system_memory_actual_free 
  Then I should succeed
  
Scenario: Post resource system_memory_actual_used_bytes 
  When I post the resource system_memory_actual_used_bytes 
  Then I should succeed
  
Scenario: Post resource system_memory_actual_used_pct 
  When I post the resource system_memory_actual_used_pct 
  Then I should succeed
  
Scenario: Post resource system_memory_free 
  When I post the resource system_memory_free 
  Then I should succeed
  
Scenario: Post resource system_memory_swap_free 
  When I post the resource system_memory_swap_free 
  Then I should succeed
  
Scenario: Post resource system_memory_swap_total 
  When I post the resource system_memory_swap_total 
  Then I should succeed
  
Scenario: Post resource system_memory_swap_used_bytes 
  When I post the resource system_memory_swap_used_bytes 
  Then I should succeed
  
Scenario: Post resource system_memory_swap_used_pct 
  When I post the resource system_memory_swap_used_pct 
  Then I should succeed
  
Scenario: Post resource system_memory_total 
  When I post the resource system_memory_total 
  Then I should succeed
  
Scenario: Post resource system_memory_used_bytes 
  When I post the resource system_memory_used_bytes 
  Then I should succeed
  
Scenario: Post resource system_memory_used_pct 
  When I post the resource system_memory_used_pct 
  Then I should succeed
  
Scenario: Post resource system_network_in_bytes 
  When I post the resource system_network_in_bytes 
  Then I should succeed
  
Scenario: Post resource system_network_in_dropped 
  When I post the resource system_network_in_dropped 
  Then I should succeed
  
Scenario: Post resource system_network_in_errors 
  When I post the resource system_network_in_errors 
  Then I should succeed
  
Scenario: Post resource system_network_in_packets 
  When I post the resource system_network_in_packets 
  Then I should succeed
  
Scenario: Post resource system_network_out_bytes 
  When I post the resource system_network_out_bytes 
  Then I should succeed
  
Scenario: Post resource system_network_out_dropped 
  When I post the resource system_network_out_dropped 
  Then I should succeed
  
Scenario: Post resource system_network_out_errors 
  When I post the resource system_network_out_errors 
  Then I should succeed
  
Scenario: Post resource system_network_out_packets 
  When I post the resource system_network_out_packets 
  Then I should succeed
  
Scenario: Post resource system_cpu_cores 
  When I post the resource system_cpu_cores 
  Then I should succeed
  
Scenario: Post resource system_load_chart 
  When I post the resource system_load_chart 
  Then I should succeed     