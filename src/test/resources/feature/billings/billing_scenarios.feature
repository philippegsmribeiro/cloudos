Feature: The Billings API
  Scenario: List all the buckets
    When I lists all the buckets
    Then I should succeed
    And I receive a list of buckets

  Scenario: Create a new bucket
    Given I have an unique bucket name
    When I create a new bucket in the provider "AWS"
    Then I should succeed

  Scenario: Describe an existing bucket
    Given I have an existing bucket name
    When I describe the bucket
    Then I should succeed
    And the bucket should exist

  Scenario: Save a bucket that already exists
    Given I have an existing bucket name
    When I attempt to save the bucket
    Then I should fail

  Scenario: Delete an existing bucket
    Given I have an existing bucket name
    When I delete the bucket
    Then I should succeed

