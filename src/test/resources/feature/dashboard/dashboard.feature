Feature: The Dashboard API
  Scenario: Get total cost
    When I get the total cost
    Then I should succeed
    #And I receive a cost greater or equal zero for the current month

  Scenario: Get resources
    When I get all the resources
    Then I should succeed

  Scenario: Get regions
    When I get all the regions
    Then I should succeed
    And The number of instances should be greater than or equal to 0

  Scenario: Get Providers
    When I get all the providers
    Then I should succeed
    #And one of the providers is AMAZON_AWS
    #And RACK_SPACE is not one of the providers

  Scenario: Get Events
    When I get all the events
    Then I should succeed
    And the event is [14:00] VM1 CPU increased by 10%

  Scenario: Get CPU Usage
    When I get the CPU usage
    Then I should succeed
    And the status code is 200

  Scenario: Get the Memory Usage
    When I get the Memory usage
    Then I should succeed
    And the status code is 200

  Scenario: Get the Disk Usage
    When I get the Disk usage
    Then I should succeed
    And the status code is 200

  Scenario: Get the Network Usage
    When I get the Network usage
    Then I should succeed
    And the status code is 200

  Scenario: Get Google Compute Engine CPU usage
    When If I list the GOOGLE_COMPUTE_ENGINE CPU usage
    Then I should succeed
    And the status code is 200

  Scenario: Get Amazon AWS CPU usage
    When If I list the AMAZON_AWS CPU usage
    Then I should succeed
    And the status code is 200

  Scenario: Get Google Compute Engine Memory usage
    When If I list the GOOGLE_COMPUTE_ENGINE Memory usage
    Then I should succeed
    And the status code is 200

  Scenario: Get Amazon AWS Memory usage
    When If I list the AMAZON_AWS Memory usage
    Then I should succeed
    And the status code is 200

  Scenario: Get Google Compute Engine Disk usage
    When If I list the GOOGLE_COMPUTE_ENGINE Disk usage
    Then I should succeed
    And the status code is 200

  Scenario: Get Amazon AWS Disk usage
    When If I list the AMAZON_AWS Disk usage
    Then I should succeed
    And the status code is 200

  Scenario: Get Google Compute Engine Network usage
    When If I list the GOOGLE_COMPUTE_ENGINE Network usage
    Then I should succeed
    And the status code is 200

  Scenario: Get Amazon AWS Network usage
    When If I list the AMAZON_AWS Network usage
    Then I should succeed
    And the status code is 200

