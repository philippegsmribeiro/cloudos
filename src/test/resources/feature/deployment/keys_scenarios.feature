Feature: Key management

  Scenario: Trying save AWS key that exists in AWS but not exists in the cloudos database
    When the client get a AWS key that not exists in cloudos in the region us-east-1
    And the client trying to save that key in AWS
    Then the client receive a error message


  Scenario: save aws key
    When the client enter a aleatory keyName
    And the client enter region that exists us-east-1
    And the client enter provider AMAZON_AWS
    Then the client receives ok

    @ignoreScenario
  Scenario: trying save aws key with region that not exists
    When the client enter a aleatory keyName
    And the client enter region that not exists us-east-1b
    And the client enter provider AMAZON_AWS
    Then the client receives error