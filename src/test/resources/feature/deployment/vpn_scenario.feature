Feature: VPN management

  Scenario: Save a VPN security group
    When I create a new VPN
    And the provider is AMAZON_AWS
    And the region is us-west-1
    And the cidr group is 10.0.0.0/20
