Feature: Validate the provider elements

  Scenario: Verify if AWS is on the list of providers
    When the client list the providers
    Then the client receive a list which contains AMAZON_AWS
    And the client receive regions for AMAZON_AWS
    And the client receive regions zones for AMAZON_AWS
    And the client receive images for AMAZON_AWS
    And the client receive machineTypes for AMAZON_AWS
    And the client receive keys for AMAZON_AWS
    And the client receive security groups for AMAZON_AWS

  Scenario: Verify if GCLOUD is on the list of providers
    When the client list the providers
    Then the client receive a list which contains GOOGLE_COMPUTE_ENGINE
    And the client receive regions for GOOGLE_COMPUTE_ENGINE
    And the client receive regions zones for GOOGLE_COMPUTE_ENGINE
    And the client receive images for GOOGLE_COMPUTE_ENGINE
    And the client receive machineTypes for GOOGLE_COMPUTE_ENGINE
    And the client receive keys for GOOGLE_COMPUTE_ENGINE
    And the client receive security groups for GOOGLE_COMPUTE_ENGINE
    
    