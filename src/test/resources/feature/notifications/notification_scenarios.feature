Feature: Validate the notifications

  Scenario: Verify if there is a notification
    When the client list a notification
    Then the client receive a notification
    
  Scenario: Verify if a notification is read
    When the notification is read
    Then the client mark as unread
    
  Scenario: Verify if a notification is unread
    When the notification is unread
    Then the client mark as read

    
    