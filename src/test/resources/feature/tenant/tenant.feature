Feature: The TenantController API 
Scenario: Create a new tenant
  When I create a new tenant
  Then I should create a tenant
    
Scenario: Check if a tenant exist by domain name
  When I check for an existing tenant by domain name
  Then I find the tenant by domain name       
  
Scenario: Check if a tenant exist by domain name
  When I check for a non existing tenant by domain name
  Then I don't find the tenant by domain name      
  
Scenario: Check if an user exist by email
  When I check for a non existing user by email
  Then I don't find the user by email
  
Scenario: Check if an user exist by email
  When I check for an existing user by email
  Then I find the user by email  
  
Scenario: Check if a user exist by username
  When I check for a non existing user by username
  Then I don't find the user by username
  
Scenario: Check if a user exist by email
  When I check for an existing user by username
  Then I find the user by username    

Scenario: Get USA regions
  When I get USA regions
  Then I should get all valid USA regions
  
Scenario: Get AUS regions
  When I get AUS regions
  Then I should get all valid AUS regions
  
Scenario: Get EUR regions
  When I get EUR regions
  Then I should get all valid EUR regions
  
Scenario: Get BRA regions
  When I get BRA regions
  Then I should get all valid BRA regions
  
Scenario: Get JPN regions
  When I get JPN regions
  Then I should get all valid JPN regions
  
Scenario: Get CHN regions
  When I get CHN regions
  Then I should get all valid CHN regions       