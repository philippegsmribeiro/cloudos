Feature: The SettingsController API 
Scenario: Update an existing tenant
  When Update an existing tenant
  Then I should update the tenant attributes
  
Scenario: Update an existing tenant missging required attributes
  When Update an existing tenant
  Then I shouldn't update the tenant attributes
    
Scenario: Find an existing tenant by username
  When I search an existing tenant by username
  Then I should find a tenant searching by username 
  
Scenario: Find a tenant by wrong username
  When I search a tenant by wrong username
  Then I shouldn't find the tenant searching by wrong username
    
Scenario: Update a users password
  When I update the user's password
  Then I should update the password
  
Scenario: Update a non existing users password
  When I update the user's password
  Then I shouldn't update the password
  
Scenario: Create and Update an User Profile
  When I update a user profile
  When I should be able to retrieve the updated user profile