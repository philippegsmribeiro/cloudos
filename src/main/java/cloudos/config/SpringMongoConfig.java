package cloudos.config;

import com.github.fakemongo.Fongo;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

import java.util.Arrays;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

@Configuration
@Log4j2
public class SpringMongoConfig extends AbstractMongoConfiguration {

  private static final int MAX_CONNECTION_IDLE_TIME = 60 * 1000;

  private static final String FONGO_TEST = "fongoTest";

  private static final String ADMIN = "admin";

  @Value("${spring.data.mongodb.host}")
  private String host;

  @Value("${spring.data.mongodb.port}")
  private Integer port;

  @Value("${spring.data.mongodb.database}")
  private String database;

  @Value("${spring.data.mongodb.username}")
  private String username;

  @Value("${spring.data.mongodb.password}")
  private String password;

  @Autowired
  private cloudos.utils.ProfileUtils profileUtils;

  /**
   * Obtain a new MongoDb factory.
   *
   * @return a new MongoDb factory
   * @throws Exception whenever an error occur
   */
  @Override
  @Bean
  public MongoDbFactory mongoDbFactory() throws Exception {
    return new SimpleMongoDbFactory(getMongoClient(), database);
  }

  /**
   * Obtain the basic Mongo template.
   *
   * @return a Mongo template
   * @throws Exception whenever an error occur
   */
  @Override
  @Bean
  public MongoTemplate mongoTemplate() throws Exception {
    MappingMongoConverter converter = new MappingMongoConverter(
        new DefaultDbRefResolver(mongoDbFactory()), new MongoMappingContext());
    converter.setMapKeyDotReplacement("\\+");

    // enable Java 8 Time API
    converter.afterPropertiesSet();
    return new MongoTemplate(mongoDbFactory(), converter);
  }

  /**
   * Get the name of the database the MongoDb is currently connected.
   *
   * @return the name of the database
   */
  @Override
  protected String getDatabaseName() {
    return this.database;
  }

  /**
   * Override the default mongo definition.
   *
   * @return a Mongo instance
   * @throws Exception whenever an error occur
   */
  @Override
  public Mongo mongo() throws Exception {
    return getMongoClient();
  }

  /**
   * Get a client connection to the MongoDb.
   *
   * @return a new MongoClient connection
   */
  private MongoClient getMongoClient() {
    MongoClientOptions.Builder options = MongoClientOptions.builder();
    if (profileUtils.isTest()) {
      // if test
      if (profileUtils.isDocker()) {
        log.info("#############################");
        log.info("Starting Mongo Docker!!!!!!!!");
        log.info("#############################");
        return new MongoClient(new ServerAddress(this.host, this.port));
      }
      log.info("#############################");
      log.info("Starting FONGO!!!!!!!!");
      log.info("#############################");
      return new Fongo(FONGO_TEST).getMongo();
    }
    // for azure
    options.maxConnectionIdleTime(MAX_CONNECTION_IDLE_TIME);
    return new MongoClient(new ServerAddress(this.host, this.port),
        Arrays.asList(
            MongoCredential.createCredential(this.username, ADMIN, this.password.toCharArray())),
        options.build());
  }
}
