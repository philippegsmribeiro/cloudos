package cloudos.config;

import cloudos.context.ContextAwarePoolExecutor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

/**
 * Configuration class for the thread pools.
 *
 * @author flavio
 */
@Configuration
public class ThreadingConfig {

  public static final String QUEUE = "queue";
  public static final String MONITOR = "monitor";
  public static final String DEPLOYMENT = "deployment";
  public static final String CLIENT_INSTALL = "client-install";
  public static final String OPTIMIZER = "optimizer";
  public static final String BILLINGS = "billings";

  /**
   * Default thread executor.
   *
   * @return ContextAwarePoolExecutor
   */
  @Bean
  @Primary
  public AsyncTaskExecutor getAsyncTaskExecutor() {
    return new ContextAwarePoolExecutor();
  }

  /**
   * Thread executor for the monitoring services.
   *
   * @return ContextAwarePoolExecutor
   */
  @Bean(name = MONITOR)
  public AsyncTaskExecutor getAsyncTaskExecutorMonitor() {
    ContextAwarePoolExecutor asyncTaskExecutor = new ContextAwarePoolExecutor();
    asyncTaskExecutor.setThreadNamePrefix(MONITOR);
    return asyncTaskExecutor;
  }

  /**
   * Thread executor for the queue listener task.
   *
   * @return ContextAwarePoolExecutor
   */
  @Bean(name = QUEUE)
  public AsyncTaskExecutor getAsyncTaskExecutorQueue() {
    ContextAwarePoolExecutor asyncTaskExecutor = new ContextAwarePoolExecutor();
    asyncTaskExecutor.setThreadNamePrefix(QUEUE);
    return asyncTaskExecutor;
  }

  /**
   * Thread executor for the deployments tasks.
   *
   * @return ContextAwarePoolExecutor
   */
  @Bean(name = DEPLOYMENT)
  public AsyncTaskExecutor getAsyncTaskExecutorDeployment() {
    ContextAwarePoolExecutor asyncTaskExecutor = new ContextAwarePoolExecutor();
    asyncTaskExecutor.setThreadNamePrefix(DEPLOYMENT);
    return asyncTaskExecutor;
  }

  /**
   * Thread executor for the client installing tasks NOT ContextAware.
   *
   * @return SimpleAsyncTaskExecutor
   */
  @Bean(name = CLIENT_INSTALL)
  public AsyncTaskExecutor getAsyncTaskExecutorClientInstall() {
    SimpleAsyncTaskExecutor asyncTaskExecutor = new SimpleAsyncTaskExecutor();
    asyncTaskExecutor.setThreadNamePrefix(CLIENT_INSTALL);
    return asyncTaskExecutor;
  }

  /**
   * Thread executor for the optimizer tasks NOT ContextAware.
   *
   * @return SimpleAsyncTaskExecutor
   */
  @Bean(name = OPTIMIZER)
  public AsyncTaskExecutor getAsyncTaskExecutorOptimizer() {
    SimpleAsyncTaskExecutor asyncTaskExecutor = new SimpleAsyncTaskExecutor();
    asyncTaskExecutor.setThreadNamePrefix(OPTIMIZER);
    return asyncTaskExecutor;
  }
  
  /**
   * Thread executor for the optimizer tasks NOT ContextAware.
   *
   * @return SimpleAsyncTaskExecutor
   */
  @Bean(name = BILLINGS)
  public AsyncTaskExecutor getAsyncTaskExecutorBillings() {
    SimpleAsyncTaskExecutor asyncTaskExecutor = new SimpleAsyncTaskExecutor();
    asyncTaskExecutor.setThreadNamePrefix(BILLINGS);
    return asyncTaskExecutor;
  }
}
