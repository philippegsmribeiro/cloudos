package cloudos.config;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.AbstractHttp11Protocol;
import org.apache.coyote.http11.Http11NioProtocol;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;

/** Start the app using tomcat as container and enables the websocket support and ssl connector. */
@Configuration
public class TomcatConfig {

  private static final int COMPRESSION_MIN_SIZE = 256;
  private static final String COMPRESSION_ON = "on";
  private static final String HTTPS = "https";

  @Value("${ssl.enabled}")
  boolean sslEnabled;

  @Value("${ssl.port}")
  Integer port;

  @Value("${ssl.keystore}")
  String keyStore;

  @Value("${ssl.keystore-password}")
  String keyStorePassword;

  @Value("${ssl.keyAlias}")
  String keyAlias;

  @Value("${ssl.keyStoreType}")
  String keyStoreType;

  @Value("${port}")
  Integer tomcatPort;

  /**
   * Tomcat Container.
   *
   * @return a new embedded container factory
   */
  @Bean
  public EmbeddedServletContainerFactory servletContainer() {
    TomcatEmbeddedServletContainerFactory tomcat;
    tomcat = new TomcatEmbeddedServletContainerFactory(tomcatPort);
    tomcat.addConnectorCustomizers(gettomcatConnectorCustomizer());

    if (sslEnabled) {
      // adds the ssl connector
      tomcat.addAdditionalTomcatConnectors(createSslConnector());
    }
    return tomcat;
  }

  /**
   * Obtain a Tomcat connector customized.
   *
   * @return a new customized connector
   */
  private TomcatConnectorCustomizer gettomcatConnectorCustomizer() {
    return connector ->
        configCompression((AbstractHttp11Protocol<?>) connector.getProtocolHandler());
  }

  /**
   * Enabling SSL.
   *
   * @return a new SSL connector
   */
  private Connector createSslConnector() {
    Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
    Http11NioProtocol protocol = (Http11NioProtocol) connector.getProtocolHandler();

    try {
      connector.setScheme(HTTPS);
      connector.setSecure(true);
      connector.setPort(port);
      protocol.setSSLEnabled(true);
      protocol.setKeystoreFile(getKeystoreFile());
      protocol.setKeystorePass(keyStorePassword);
      protocol.setKeyAlias(keyAlias);
      // set compression
      configCompression(protocol);
      return connector;
    } catch (Exception ex) {
      throw new IllegalStateException(
          "can't access keystore: [" + "keystore" + "] or " + "truststore: [" + "keystore" + "]",
          ex);
    }
  }

  /**
   * Config the compression over the http.
   *
   * @param httpProtocol a new HTTP protocol
   */
  private void configCompression(AbstractHttp11Protocol<?> httpProtocol) {
    httpProtocol.setCompression(COMPRESSION_ON);
    httpProtocol.setCompressionMinSize(COMPRESSION_MIN_SIZE);
    String mimeTypes = httpProtocol.getCompressibleMimeType();
    String mimeTypesWithJson = String.format("%s,%s", mimeTypes, MediaType.APPLICATION_JSON_VALUE);
    httpProtocol.setCompressibleMimeType(mimeTypesWithJson);
  }

  /**
   * Generates a reachable file to the container.
   *
   * @return the key store
   */
  private String getKeystoreFile() {
    try {
      File file = File.createTempFile("keystore", ".key");
      Files.copy(
          this.getClass().getClassLoader().getResourceAsStream(keyStore),
          file.toPath(),
          StandardCopyOption.REPLACE_EXISTING);
      return file.getAbsolutePath();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }
}
