package cloudos.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.mapping.event.ValidatingMongoEventListener;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/**
 * Spring configuration that builds the objects ValidatingMongoEventListener and
 * LocalValidatorFactoryBean to implement the JSR-303 Bean Validation API for MongoDB.
 *
 * @author Rogério Souza
 */
@Configuration
public class BeanValidationConfig {

  @Bean
  public ValidatingMongoEventListener validatingMongoEventListener() {
    return new ValidatingMongoEventListener(validator());
  }

  @Bean
  public LocalValidatorFactoryBean validator() {
    return new LocalValidatorFactoryBean();
  }

}
