package cloudos.config;

import cloudos.security.jwt.JwtUtils;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/** Created by philipperibeiro on 2/3/17. */
@Configuration
@Conditional(NotLoadOnTestCondition.class)
@EnableSwagger2
public class SwaggerConfig {

  private static final String ADMIN = "Admin";
  // TODO: rename that for cloudtown
  private static final String APP_PACKAGE = "cloudos";

  /**
   * Enable the Swagger-UI.
   *
   * @return a Swagger docket
   */
  @Bean
  public Docket cloudosApi() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.basePackage(APP_PACKAGE))
        .paths(PathSelectors.any())
        .build()
        .apiInfo(apiInfo(""))
        .securitySchemes(Arrays.asList(apiKey()))
        .securityContexts(Arrays.asList(securityContext()))
        .pathMapping("/").groupName("");
  }

  @Value("${management.context-path}")
  String adminPath;

  /**
   * Swagger admin.
   * 
   * @return a Swagger docket
   */
  @Bean
  public Docket adminApi() {
    return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any())
        .paths(PathSelectors.ant(adminPath + "/*")).build()
        .apiInfo(apiInfo(String.format("-%s", ADMIN)))
        .securitySchemes(Arrays.asList(apiKey())).securityContexts(Arrays.asList(securityContext()))
        .pathMapping("/").groupName(ADMIN);
  }

  /**
   * Define the API info for the Swagger API.
   *
   * @return a new ApiInfo
   */
  private ApiInfo apiInfo(String description) {
    return new ApiInfoBuilder()
        .title("CloudOS REST API")
        .description(String.format("CloudOS REST API %s", description))
        .license("private and proprietary")
        .version("0.6.3")
        .build();
  }

  /**
   * Obtain the ApiKey.
   *
   * @return a new Apikey
   */
  private ApiKey apiKey() {
    return new ApiKey(JwtUtils.HEADER_FIELD, JwtUtils.HEADER_FIELD, "header");
  }

  /**
   * Obtain the security context for Swagger.
   *
   * @return the security context
   */
  private SecurityContext securityContext() {
    return SecurityContext.builder()
        .securityReferences(defaultAuth())
        .forPaths(PathSelectors.regex("/rest/.*"))
        .build();
  }

  /**
   * Obtain the default authentication for the swagger ui.
   *
   * @return a list of security references
   */
  private List<SecurityReference> defaultAuth() {
    AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
    AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
    authorizationScopes[0] = authorizationScope;
    return Arrays.asList(new SecurityReference("X-Authorization", authorizationScopes));
  }
}
