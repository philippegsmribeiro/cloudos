package cloudos.config;

import org.springframework.boot.autoconfigure.condition.ConditionOutcome;
import org.springframework.boot.autoconfigure.condition.SpringBootCondition;
import org.springframework.boot.test.context.SpringBootTestContextBootstrapper;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/** Condition to run/include a service/component when a junit test is running. */
public class LoadOnTestCondition extends SpringBootCondition {

  @Override
  public ConditionOutcome getMatchOutcome(
      ConditionContext context, AnnotatedTypeMetadata metadata) {
    if (context.getEnvironment().getProperty(SpringBootTestContextBootstrapper.class.getName())
        == null) {
      return ConditionOutcome.noMatch("Bean not loaded on JUnit tests");
    }
    return ConditionOutcome.match();
  }
}
