package cloudos;

import cloudos.costanalysis.BillingCostReservation;
import cloudos.costanalysis.CostAnalysisService;
import cloudos.costanalysis.InstanceCostListExport;
import cloudos.exceptions.InvalidRequestException;
import cloudos.exceptions.NotImplementedException;
import cloudos.models.CostProvidersReservationRequest;
import cloudos.models.Top10BillingAccountsRequest;
import cloudos.models.TopServiceCategoriesRequest;
import cloudos.models.costanalysis.BillingAccount;
import cloudos.models.costanalysis.BillingAccountCost;
import cloudos.models.costanalysis.BillingCostProviderTotal;
import cloudos.models.costanalysis.BillingEstimatedCostMonth;
import cloudos.models.costanalysis.GenericBillingReport;
import cloudos.models.costanalysis.GenericBillingReportQueryFilter;
import cloudos.models.costanalysis.InstanceCost;
import cloudos.models.costanalysis.InstanceTypeCost;
import cloudos.models.costanalysis.OfferingCostByMonths;
import cloudos.models.costanalysis.OfferingUtilizationReport;
import cloudos.models.costanalysis.ProviderDailyCostEntry;
import cloudos.models.costanalysis.ProviderRegionalCost;
import cloudos.models.costanalysis.ProviderServiceCost;
import cloudos.models.costanalysis.ProviderServiceCostComparisonEntry;
import cloudos.models.costanalysis.ProviderTrendEntry;
import cloudos.models.costanalysis.ResourceCost;
import cloudos.models.costanalysis.RetrieveInstanceCostRequest;
import cloudos.models.costanalysis.RetrieveProviderRegionalCostsRequest;
import cloudos.models.costanalysis.RetrieveProviderServiceCostRequest;
import cloudos.models.costanalysis.ServiceCategoryCost;
import cloudos.models.costanalysis.Top10ResourcesResponse;
import cloudos.models.services.ServiceCategory;
import cloudos.security.WebSecurityConfig;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


/**
 * Implement the cost analysis controller.
 */
@RestController
@RequestMapping(value = CostAnalysisController.CONTEXT)
@Log4j2
public class CostAnalysisController {

  public static final String CONTEXT = WebSecurityConfig.API_PATH + "/costanalysis";
  public static final String TOP_10_ACCOUNTS_ENDPOINT = "/topaccounts";
  public static final String TOP_10_RESOURCE_COST = "/topresources";
  public static final String TOP_5_SERVICE_CATEGORIES_ENDPOINT = "/topservicecategories";
  public static final String TOP_INSTANCES_ENDPOINT = "/topinstancetypes";
  public static final String ESTIMATED_COST_PROVIDERS_ENDPOINT = "/estimatedcostsproviders";
  public static final String COST_BY_PROVIDERS_ENDPOINT = "/costsbyproviders";
  public static final String COST_PROVIDERS_RESERVATION_ENDPOINT = "/costsprovidersreservation";
  public static final String COST_PROVIDERS_SERVICE_ENDPOINT = "/costsprovidersservice";
  public static final String STORAGE_COSTS_ENDPOINT = "/storage";
  public static final String BILLING_ACCOUNTS_ENDPOINT = "/billingaccounts";
  public static final String BILLING_ACCOUNTS_BY_ID_ENDPOINT = "/billingaccounts/{id}";
  public static final String BILLING_REPORTS_ENDPOINT = "/billings";
  public static final String BILLING_REPORTS_BY_ID_ENDPOINT = "/billings/{id}";
  public static final String NETWORKING_COSTS_ENDPOINT = "/networking";
  public static final String INSTANCE_COST = "/instancecost";
  public static final String INSTANCE_COST_CSV = "/instancecostcsv";
  public static final String PROVIDER_REGIONAL_COST = "/regionalcost";
  public static final String TRIGGER_PROVIDER_TREND_UPDATE = "/trigger/providertrend";
  public static final String TRIGGER_PROVIDER_DAILY_COST_UPDATE = "/trigger/dailycost";
  public static final String TRIGGER_RESOURCE_DAILY_COST_UPDATE = "/trigger/resourcedailycost";
  public static final String GET_PROVIDER_TREND = "/providertrend";
  public static final String OFFERING_UTILIZATION = "/offeringutilization";
  public static final String OFFERING_COMPARISON = "/offeringcomparison";
  public static final String PROVIDER_DAILY_COST = "/dailycost";

  private CostAnalysisService costAnalysisService;
  
  @Autowired
  public CostAnalysisController(CostAnalysisService costAnalysisService) {
    this.costAnalysisService = costAnalysisService;
  }

  /**
   * Retrieve the top 10 billing accounts by usage cost.
   */
  @PostMapping(value = TOP_10_ACCOUNTS_ENDPOINT)
  @ResponseBody
  public ResponseEntity<?> retrieveTop10Accounts(@RequestBody Top10BillingAccountsRequest request) {
    List<BillingAccountCost> result =
        costAnalysisService.listTop10BillingAccounts(request.getStartDate(), request.getEndDate());
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  /**
   * Retrieves the top 10 resource cost.
   * 
   * @param start the start date range (inclusive). Defaults to 1 month ago.
   * @param end the end date range (inclusive). Defaults to 1 current date.
   */
  @ApiOperation(value = "retrieveTop10Resources", response = Top10ResourcesResponse.class)
  @GetMapping(value = TOP_10_RESOURCE_COST)
  @ResponseBody
  public ResponseEntity<?> retrieveTop10Resources(
      @ApiParam @RequestParam(required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate start,
      @ApiParam @RequestParam(required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate end) {

    List<ResourceCost> resources = costAnalysisService.listTop10ResourcesByCost(start, end);
    Top10ResourcesResponse response =
        Top10ResourcesResponse.builder().start(start).end(end).resources(resources).build();
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

  /**
   * Retrieve the top service categories by cost.
   */
  @PostMapping(value = TOP_5_SERVICE_CATEGORIES_ENDPOINT)
  @ResponseBody
  public ResponseEntity<?> retrieveTopServiceCategoriesByCost(
      @RequestBody TopServiceCategoriesRequest request) {
    List<ServiceCategoryCost> result =
        costAnalysisService.listTopServiceCategories(request.getStartDate(), request.getEndDate());
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  /**
   * Retrieve the top instance types by cost, independently of the cloud provider.
   */
  @GetMapping(value = TOP_INSTANCES_ENDPOINT)
  @ResponseBody
  public ResponseEntity<?> retrieveTopInstanceTypesByCost() {
    List<InstanceTypeCost> result = costAnalysisService.listTop10InstanceTypesByCost();
    return new ResponseEntity<>(result, HttpStatus.OK);
  }


  /**
   * Retrieve the estimated cost of the month by each provider.
   */
  @GetMapping(value = ESTIMATED_COST_PROVIDERS_ENDPOINT)
  @ResponseBody
  public ResponseEntity<?> retrieveEstimatedCostProviders() {
    BillingEstimatedCostMonth result = costAnalysisService.getCostEstimatedMonthCurrentProvider();
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  /**
   * Retrieve the cost by provider.
   */
  @GetMapping(value = COST_BY_PROVIDERS_ENDPOINT)
  @ResponseBody
  public ResponseEntity<?> retrieveCostByProviders(
      @ApiParam @RequestParam(required = false) Providers provider,
      @ApiParam @RequestParam(required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate start,
      @ApiParam @RequestParam(required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate end) {
    List<BillingCostProviderTotal> result =
        costAnalysisService.getCostByProvider(provider, start, end);
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  /**
   * Retrieve the cost of a provider by reservation type.
   */
  @GetMapping(value = COST_PROVIDERS_RESERVATION_ENDPOINT)
  @ResponseBody
  public ResponseEntity<?> retrieveCostByProviderAndReservation(
      CostProvidersReservationRequest costProvidersReservationRequest) {
    BillingCostReservation result = null;
    try {
      result = costAnalysisService.getCostByProviderAndReservation(costProvidersReservationRequest);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (InvalidRequestException | NotImplementedException e) {
      log.error("Error", e);
      return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

  }

  /**
   * Retrieve the cost of a provider and service.
   */
  @PostMapping(value = COST_PROVIDERS_SERVICE_ENDPOINT)
  @ResponseBody
  public ResponseEntity<?> retrieveCostByProviderAndService(
      @Valid @RequestBody RetrieveProviderServiceCostRequest request) {
    List<ProviderServiceCost> result = null;
    try {
      result = costAnalysisService.getCostProviderAndService(request);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      log.error("Error", e);
      return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

  }

  /**
   * Retrieve the storage cost between month to date, 30 and 60 days.
   */
  @GetMapping(value = STORAGE_COSTS_ENDPOINT)
  @ResponseBody
  public ResponseEntity<?> retrieveStorageCosts(
      @ApiParam(value = "the cloud provider") @RequestParam(required = false) Providers provider) {
    ProviderServiceCostComparisonEntry result =
        costAnalysisService.getServiceCategoryCost60DaysReport(provider, ServiceCategory.STORAGE);
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  /**
   * Retrieve the networking cost between month to date, 30 and 60 days.
   */
  @GetMapping(value = NETWORKING_COSTS_ENDPOINT)
  @ResponseBody
  public ResponseEntity<?> retrieveNetworkingCosts(
      @ApiParam(value = "the cloud provider") @RequestParam(required = false) Providers provider) {
    ProviderServiceCostComparisonEntry result = costAnalysisService
        .getServiceCategoryCost60DaysReport(provider, ServiceCategory.NETWORKING);
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  /**
   * Retrieves the billing reports by a set of filters.
   *
   * @param provider the cloud provider (optional)
   * @param usageStart the usage start timestamp (optional)
   * @param usageEnd the usage end timestamp (optional)
   * @param lineItemId the report lineItemId (optional)
   * @param accountId the billing payer account id (optional)
   * @param productCode the product code (optional)
   * @param startCost the usage cost start value (optional)
   * @param endCost the usage cost end value (optional)
   */
  @ApiOperation(value = "Retrieves the billing reports", nickname = "retrieveBillings",
      response = GenericBillingReport.class, responseContainer = "List")
  @GetMapping(value = BILLING_REPORTS_ENDPOINT)
  @ResponseBody
  public ResponseEntity<?> retrieveBillings(
      @ApiParam(value = "the cloud provider") @RequestParam(required = false) Providers provider,
      @ApiParam(value = "the usage start timestamp") @RequestParam(
          required = false) Date usageStart,
      @ApiParam(value = "the usage end timestamp") @RequestParam(required = false) Date usageEnd,
      @ApiParam(value = "the lineItemId") @RequestParam(required = false) String lineItemId,
      @ApiParam(value = "the billing payer account Id") @RequestParam(
          required = false) String accountId,
      @ApiParam(value = "the product code") @RequestParam(required = false) String productCode,
      @ApiParam(value = "the usage cost start") @RequestParam(
          required = false) BigDecimal startCost,
      @ApiParam(value = "the usage cost end") @RequestParam(required = false) BigDecimal endCost) {

    GenericBillingReportQueryFilter filter = new GenericBillingReportQueryFilter();
    filter.setProvider(provider);
    filter.setAccountId(accountId);
    filter.setLineItemId(lineItemId);
    filter.setProductCode(productCode);
    filter.setUsageCostGreaterThanOrEqualTo(startCost);
    filter.setUsageCostLowerThanOrEqualTo(endCost);
    filter.setStartDateAfterOrEqualTo(usageStart);
    filter.setStartDateBeforeOrEqualTo(usageEnd);
    Collection<GenericBillingReport> result = costAnalysisService.getBillingReportsByFilter(filter);

    if (result == null) {
      return new ResponseEntity<HttpStatus>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  /**
   * Retrieves the billing report by its id.
   */
  @GetMapping(value = BILLING_REPORTS_BY_ID_ENDPOINT)
  @ResponseBody
  public ResponseEntity<?> retrieveBillingReportById(
      @ApiParam(value = "the billing report id", required = true) @PathVariable String id) {
    GenericBillingReport result = costAnalysisService.getBillingReportById(id);

    if (result == null) {
      return new ResponseEntity<HttpStatus>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  /**
   * Retrieves the billing accounts by provider.
   *
   * @return the billing accounts list or an empty result.
   */
  @ApiOperation(value = "retrieveBillingAccountsByProvider", response = BillingAccount.class,
      responseContainer = "List")
  @GetMapping(value = BILLING_ACCOUNTS_ENDPOINT)
  @ResponseBody
  public ResponseEntity<?> retrieveBillingAccountsByProvider(
      @ApiParam(required = true) @RequestParam(required = true) Providers provider) {
    List<BillingAccount> result = costAnalysisService.findBillingAccountsByProvider(provider);
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  /**
   * Retrieves the billing account by its internal id.
   *
   * @return the billing account or a null result
   */
  @ApiOperation(value = "retrieveBillingAccountById", response = BillingAccount.class)
  @GetMapping(value = BILLING_ACCOUNTS_BY_ID_ENDPOINT)
  @ResponseBody
  public ResponseEntity<?> retrieveBillingAccountById(
      @ApiParam(required = true) @PathVariable String id) {
    BillingAccount result = costAnalysisService.findBillingAccountById(id);
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  /**
   * Retrieves the instance cost filtered by provider, machine type and a date range.
   */
  @ApiOperation(value = "retrieveInstanceCost", response = InstanceCost.class,
      responseContainer = "List", produces = MediaType.APPLICATION_JSON_VALUE)
  @PostMapping(value = INSTANCE_COST)
  @ResponseBody
  public ResponseEntity<?> retrieveInstanceCost(
      @Valid @RequestBody RetrieveInstanceCostRequest request) {
    List<InstanceCost> values = costAnalysisService.retrieveInstanceCost(request);
    return new ResponseEntity<>(values, HttpStatus.OK);
  }

  /**
   * Retrieves the instance cost filtered by provider, machine type and a date range.
   */
  @ApiOperation(value = "retrieveProviderRegionalCosts", response = ProviderRegionalCost.class,
      responseContainer = "List", produces = MediaType.APPLICATION_JSON_VALUE)
  @PostMapping(value = PROVIDER_REGIONAL_COST)
  @ResponseBody
  public ResponseEntity<?> retrieveProviderRegionalCosts(
      @Valid @RequestBody RetrieveProviderRegionalCostsRequest body) {
    log.debug("retrieveProviderRegionalCosts");
    List<ProviderRegionalCost> result =
        costAnalysisService.retrieveProviderRegionalCosts(body.getStartDate(), body.getEndDate());
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  /**
   * Triggers provider trend update job to be executed.
   */
  @ApiOperation(value = "triggerProviderTrendJob")
  @GetMapping(value = TRIGGER_PROVIDER_TREND_UPDATE)
  @ResponseBody
  public ResponseEntity<?> triggerProviderTrendJob() {
    costAnalysisService.triggerProviderTrendJob();
    return new ResponseEntity<>(HttpStatus.OK);
  }

  /**
   * Triggers provider daily cost job to be executed.
   */
  @ApiOperation(value = "triggerProviderDailyCostJob")
  @GetMapping(value = TRIGGER_PROVIDER_DAILY_COST_UPDATE)
  @ResponseBody
  public ResponseEntity<?> triggerProviderDailyCostJob(
      @ApiParam(required = false) @RequestParam(required = false) @DateTimeFormat(
          iso = ISO.DATE) LocalDate start,
      @ApiParam(required = false) @RequestParam(required = false) @DateTimeFormat(
          iso = ISO.DATE) LocalDate end) {
    if (start == null || end == null) {
      end = LocalDate.now();
      start = end.minusMonths(1);
    }
    costAnalysisService.triggerProviderDailyCostJob(start, end);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  /**
   * Triggers daily resource cost job to be executed.
   */
  @ApiOperation(value = "triggerResourceCostJob")
  @GetMapping(value = TRIGGER_RESOURCE_DAILY_COST_UPDATE)
  @ResponseBody
  public ResponseEntity<?> triggerResourceCostJob(
      @ApiParam(required = false) @RequestParam(required = false) @DateTimeFormat(
          iso = ISO.DATE) LocalDate start,
      @ApiParam(required = false) @RequestParam(required = false) @DateTimeFormat(
          iso = ISO.DATE) LocalDate end) {
    if (start == null || end == null) {
      end = LocalDate.now();
      start = end.minusMonths(1);
    }
    costAnalysisService.triggerResourceDailyCost(start, end);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  /**
   * Returns the latest provider trend available.
   */
  @ApiOperation(value = "getLatestProviderTrend", response = ProviderTrendEntry.class)
  @GetMapping(value = GET_PROVIDER_TREND)
  @ResponseBody
  public ResponseEntity<?> getLatestProviderTrend() {
    ProviderTrendEntry result = costAnalysisService.getLatestProviderTrend();
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  /**
   * Returns the offering utilization between a range of dates. (default 30 days)
   */
  @ApiOperation(value = "retrieveOfferingUtilization", response = OfferingUtilizationReport.class)
  @GetMapping(value = OFFERING_UTILIZATION)
  @ResponseBody
  public ResponseEntity<?> retrieveOfferingUtilization(
      @ApiParam @RequestParam(required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate from,
      @ApiParam @RequestParam(required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate to) {
    List<OfferingUtilizationReport> result =
        costAnalysisService.retrieveOfferingUtilization(from, to);
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  /**
   * Returns the offering comparison. (month to date, 30 days and 60 days)
   */
  @ApiOperation(value = "retrieveOfferingComparison", response = OfferingCostByMonths.class)
  @GetMapping(value = OFFERING_COMPARISON)
  @ResponseBody
  public ResponseEntity<?> retrieveOfferingComparison() {
    OfferingCostByMonths result = costAnalysisService.getOfferingCostByMonthComparison();
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  /**
   * Returns the provider daily cost between a range of dates. (default 30 days)
   */
  @ApiOperation(value = "retrieveProviderDailyCost", response = ProviderDailyCostEntry.class,
      responseContainer = "List")
  @GetMapping(value = PROVIDER_DAILY_COST)
  @ResponseBody
  public ResponseEntity<?> retrieveProviderDailyCost(
      @ApiParam @RequestParam(required = false) Providers provider,
      @ApiParam @RequestParam(required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate start,
      @ApiParam @RequestParam(required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate end) {
    List<ProviderDailyCostEntry> result =
        costAnalysisService.retrieveProviderDailyCostEntries(provider, start, end);
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  /**
   * Retrieves the instance cost filtered by provider, machine type and a date range for export csv.
   */
  @PostMapping(value = INSTANCE_COST_CSV, produces = "text/csv")
  @ResponseBody
  public ResponseEntity<?> retrieveInstanceCostCsv(
      @Valid @RequestBody RetrieveInstanceCostRequest request) {
    log.debug("retrieving instance cost csv");
    List<InstanceCost> values = costAnalysisService.retrieveInstanceCost(request);
    InstanceCostListExport instanceCostList = new InstanceCostListExport();
    instanceCostList.setList(values);
    return new ResponseEntity<>(instanceCostList, HttpStatus.OK);
  }
}
