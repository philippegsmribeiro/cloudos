package cloudos.deploy.deployments;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "deployment_event_logs")
@Data
@Builder
@AllArgsConstructor
public class DeploymentEventLog {

  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  private String id;

  private Date creationDate;
  private Date finishingDate;
  private DeploymentEventType type;
  private DeploymentEventStatus status;
  private String title;
  private String detail;
  private String log;

  public DeploymentEventLog() {
    id = new ObjectId().toHexString();
  }
}
