package cloudos.deploy.deployments;

public enum DeploymentStatus {

  /** When deployment is created. */
  NOT_STARTED,
  /** When deployment is creating the instance(s). */
  PROVISIONING,
  /** When deployment has the instance(s) created and will apply the deployment file. */
  STARTED,
  /** When the deployment file finished running. */
  FINISHED,
  /** if an error happened. */
  ERROR
}
