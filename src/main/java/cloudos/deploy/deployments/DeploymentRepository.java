package cloudos.deploy.deployments;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
interface DeploymentRepository extends MongoRepository<Deployment, String> {}
