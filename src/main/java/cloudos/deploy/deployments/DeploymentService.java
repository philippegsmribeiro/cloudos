package cloudos.deploy.deployments;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

@Service
public class DeploymentService {

  @Autowired
  private DeploymentRepository repository;

  /**
   * Find all deployments.
   * 
   * @param status for filtering
   * @param page number
   * @param pageSize size
   * @return page of Deployments
   */
  public Page<Deployment> findAll(DeploymentStatus status, Integer page, Integer pageSize) {
    Pageable pageable = new PageRequest(page, pageSize, Direction.DESC, "creationDate");
    if (status != null) {
      // filter by status
      Deployment exampleDeployment = new Deployment();
      exampleDeployment.setId(null);
      exampleDeployment.setCreationDate(null);
      exampleDeployment.setEventLogs(null);
      exampleDeployment.setStatus(status);
      Example<Deployment> example = Example.of(exampleDeployment);
      return repository.findAll(example, pageable);
    }
    return repository.findAll(pageable);
  }

  /**
   * Find by id.
   * 
   * @param deploymentId id
   * @return
   */
  public Deployment findById(String deploymentId) {
    return repository.findOne(deploymentId);
  }

  /**
   * Save a deployment.
   * 
   * @param deployment to be saved
   * @return saved deployment
   */
  public Deployment save(Deployment deployment) {
    return repository.save(deployment);
  }



}
