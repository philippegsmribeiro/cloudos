package cloudos.deploy.deployments;

public enum DeploymentEventStatus {

  /** Event started. */
  STARTED,
  /** Event incomplete. */
  INCOMPLETE,
  /** Event finished. */
  FINISHED,
  /** Event finished with error. */
  FINISHED_WITH_ERROR
}
