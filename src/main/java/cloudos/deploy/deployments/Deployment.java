package cloudos.deploy.deployments;

import cloudos.Providers;
import cloudos.deploy.deployers.Deployers;
import cloudos.models.AbstractInstance;
import cloudos.models.CloudCreateRequest;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "deployments")
@Data
@Builder
@AllArgsConstructor
public class Deployment {

  public static final String BASENAME = "baseName";

  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  private String id;

  private Date creationDate;
  private Date finishingDate;
  private String configuration;
  private String configFileString;
  private String configFilePath;
  private Providers provider;
  private CloudCreateRequest cloudCreateRequest;
  private Deployers deployer;
  private Integer instancesNumber;

  @DBRef(lazy = true)
  private List<? extends AbstractInstance> instances;

  private DeploymentStatus status;
  private DeploymentRunningStatus runnningStatus;
  private String returnedLog;
  private List<DeploymentEventLog> eventLogs;
  private String creationUserId;
  private String creationUserName;
  private String baseName;

  /** Default Constructor. */
  public Deployment() {
    id = new ObjectId().toHexString();
    creationDate = new Date();
    status = DeploymentStatus.NOT_STARTED;
    eventLogs = new ArrayList<>();
  }

  /**
   * Constructor with parameters.
   *
   * @param configuration info
   * @param provider of the instances
   * @param deployer to be used
   * @param instances list
   * @param instancesNumber number of instances
   */
  public Deployment(
      String configuration,
      Providers provider,
      Deployers deployer,
      List<? extends AbstractInstance> instances,
      Integer instancesNumber) {
    this();
    this.configuration = configuration;
    this.provider = provider;
    this.deployer = deployer;
    this.instances = instances;
    this.instancesNumber = instancesNumber;
  }

}
