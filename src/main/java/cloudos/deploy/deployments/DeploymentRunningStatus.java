package cloudos.deploy.deployments;

public enum DeploymentRunningStatus {

  /** if paused. */
  PAUSED,
  /** if running. */
  RUNNING,
  /** if not running. */
  NOT_RUNNING
}
