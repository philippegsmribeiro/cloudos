package cloudos.deploy.deployments;

public enum DeploymentEventType {

  /** Creation. */
  CREATE,
  /** Starting. */
  START,
  /** Provisioning. */
  PROVISION_INSTANCE,
  /** Client Installation. */
  CLIENT_INSTALL,
  /** Deploy. */
  DEPLOY,
  /** On Error. */
  ERROR,
  /** Deleting instance after error. */
  DELETE_INSTANCE,
  /** Finished. */
  FINISHED
}
