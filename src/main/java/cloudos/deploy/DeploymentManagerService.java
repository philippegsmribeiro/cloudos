package cloudos.deploy;

import cloudos.Providers;
import cloudos.config.ThreadingConfig;
import cloudos.deploy.client.ClientInstallService;
import cloudos.deploy.deployers.DeployerFactory;
import cloudos.deploy.deployers.DeployerService;
import cloudos.deploy.deployments.Deployment;
import cloudos.deploy.deployments.DeploymentEventLog;
import cloudos.deploy.deployments.DeploymentEventStatus;
import cloudos.deploy.deployments.DeploymentEventType;
import cloudos.deploy.deployments.DeploymentRunningStatus;
import cloudos.deploy.deployments.DeploymentService;
import cloudos.deploy.deployments.DeploymentStatus;
import cloudos.exceptions.NotImplementedException;
import cloudos.instances.CloudManagerService;
import cloudos.instances.InstanceRequestException;
import cloudos.instances.InstanceService;
import cloudos.models.AbstractInstance;
import cloudos.models.ActionType;
import cloudos.models.CloudActionRequest;
import cloudos.models.Instance;
import cloudos.notifications.NotificationType;
import cloudos.queue.QueueListener;
import cloudos.queue.QueueManagerService;
import cloudos.queue.message.DeploymentStopMessage.DeploymentStopMessageConsumer;
import cloudos.security.RequestScope;
import cloudos.storage.StorageService;
import java.nio.file.Path;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

/** Manage the deployments services. */
@Service
@Log4j2
public class DeploymentManagerService {

  @Autowired
  private InstanceService instanceService;
  @Autowired
  private DeploymentService deploymentService;
  @Autowired
  private DeployerFactory deployerFactory;
  @Autowired
  private CloudManagerService cloudManagerService;
  @Autowired
  private StorageService storageService;
  @Autowired
  private ClientInstallService clientInstallService;
  @Autowired
  private QueueManagerService queueManagerService;
  @Autowired
  private QueueListener queueListener;

  @Autowired
  @Qualifier(ThreadingConfig.DEPLOYMENT)
  private AsyncTaskExecutor taskExecutor;

  @Autowired
  private RequestScope requestScope;

  /** Hold the async tasks. */
  private Map<String, Future<Deployment>> asyncMap = new HashMap<>();

  /** Post constructor to set up the queue listener. */
  @PostConstruct
  public void setUp() {
    queueListener.addConsumer((DeploymentStopMessageConsumer) t -> {
      String deploymentId = t.getDeploymentId();
      if (StringUtils.isNotBlank(deploymentId)) {
        if (asyncMap.containsKey(deploymentId)) {
          stopThreadDeploy(deploymentId);
        }
      }
    });
  }

  /**
   * Stop the thread of the deployment.
   *
   * @param deploymentId id
   */
  private void stopThreadDeploy(String deploymentId) {
    // cancel the thread
    // TODO: improve this logic
    asyncMap.get(deploymentId).cancel(true);
  }

  /**
   * Return a list of deployments using pagination.
   *
   * @param page to show
   * @param pageSize number of items
   * @param status to filter
   * @return List of Deployments
   */
  public Page<Deployment> listDeployments(Integer page, Integer pageSize, DeploymentStatus status) {
    return deploymentService.findAll(status, page, pageSize);
  }

  /**
   * Retrieve the deploy based on the instance baseName.
   *
   * @param instance object
   * @return Deployment object if found
   */
  public Deployment retrieveByInstance(Instance instance) {
    if (StringUtils.isNotEmpty(instance.getDeploymentId())) {
      return deploymentService.findById(instance.getDeploymentId());
    }
    return null;
  }

  /**
   * Create the deployment.
   *
   * @param deployment object
   * @param start - if auto start after creation
   * @return Deployment after creation
   * @throws DeployConfigurationFileException if config is wrong
   */
  public Deployment createDeployment(Deployment deployment, boolean start)
      throws DeployConfigurationFileException {
    log.info(String.format("--------- Deploying: %s --------", deployment));

    // validate if configuration is set
    if (StringUtils.isBlank(deployment.getConfigFilePath())
        && StringUtils.isBlank(deployment.getConfiguration())
        && StringUtils.isBlank(deployment.getConfigFileString())) {
      throw new DeployConfigurationFileException("Config file not found!");
    }

    if (deployment.getConfigFilePath() != null) {
      // has an uploaded file
      try {
        Path filePath = storageService.load(deployment.getConfigFilePath());
        deployment.setConfigFilePath(filePath.toString());
      } catch (Exception e) {
        throw new DeployConfigurationFileException("Config file not found!");
      }
    }

    // validate the config / playbook
    DeployerService deployer = deployerFactory.getDeployer(deployment);
    deployer.validateConfiguration(deployment);

    // save deployment
    deployment.setStatus(DeploymentStatus.NOT_STARTED);
    deployment.setCreationDate(new Date());
    // create an event log
    DeploymentEventLog eventLog = createEvent(DeploymentEventType.CREATE);
    finalizeEvent(eventLog, false);
    deployment.getEventLogs().add(eventLog);
    // user
    deployment.setCreationUserName(requestScope.getUserName());

    // save baseName
    deployment.setBaseName(deployment.getCloudCreateRequest().getName());

    final Deployment savedDeployment = deploymentService.save(deployment);

    if (start) {
      this.startDeploying(savedDeployment.getId());
    }
    return deployment;
  }

  /**
   * Return a deployment.
   *
   * @param deploymentId id
   * @return Deployment object
   */
  public Deployment retrieveDeployment(String deploymentId) {
    return deploymentService.findById(deploymentId);
  }

  /**
   * Start deploying.
   *
   * @param deploymentId id
   * @return Future call
   */
  public Future<Deployment> startDeploying(String deploymentId) {
    Deployment deployment = deploymentService.findById(deploymentId);
    if (deployment != null) {
      DeploymentEventLog eventLog = createEvent(DeploymentEventType.START);
      deployment.getEventLogs().add(eventLog);
      //
      // do the deployment in a new thread - async
      Future<Deployment> submit = taskExecutor.submit(() -> {
        finalizeEvent(eventLog, false);
        Deployment executeDeployment = executeDeployment(deployment);
        asyncMap.remove(executeDeployment.getId());
        return executeDeployment;
      });
      asyncMap.put(deploymentId, submit);
      return submit;
    }
    return null;
  }

  /**
   * Execute the deployment.
   *
   * @param deployment object
   * @return Deployment object
   */
  private Deployment executeDeployment(Deployment deployment) {
    log.info("Starting deployment {} of {} instance(s) on {}", deployment.getId(),
        deployment.getInstancesNumber(), deployment.getProvider());
    // 1. provisioning the instances
    try {
      deployment = this.provisioning(deployment);
    } catch (DeploymentProvisioningException e) {
      deployment = this.setDeploymentError(deployment, e);
      // rollback
      deployment = this.executeRollbackOnError(deployment);
      return deployment;
    } catch (DeploymentNotReadyException e) {
      return deployment;
    } catch (DeploymentStepAlreadyDoneException e) {
      // ignore and continue
    }

    // 2. call the deployer
    try {
      deployment = this.deploying(deployment);
    } catch (DeployingException e) {
      deployment = this.setDeploymentError(deployment, e);
      // rollback
      deployment = this.executeRollbackOnError(deployment);
      return deployment;
    } catch (DeploymentNotReadyException e) {
      return deployment;
    } catch (DeploymentStepAlreadyDoneException e) {
      // ignore it
    }
    return deployment;
  }

  /**
   * Set the deployment error.
   *
   * @param deployment object
   * @param e exception happened
   * @return deployment object
   */
  private Deployment setDeploymentError(Deployment deployment, Exception e) {
    deployment.setStatus(DeploymentStatus.ERROR);
    deployment.setRunnningStatus(DeploymentRunningStatus.NOT_RUNNING);
    deployment.setReturnedLog(StringUtils.isBlank(deployment.getReturnedLog()) ? ""
        : (deployment.getReturnedLog() + "\n\n")
            + (StringUtils.isBlank(e.getLocalizedMessage()) ? "" : e.getLocalizedMessage()));
    deployment.setFinishingDate(new Date());
    // event
    DeploymentEventLog finishEventLog = createEvent(DeploymentEventType.ERROR);
    finishEventLog.setTitle("Finished with error!");
    deployment.getEventLogs().add(finishEventLog);
    //
    deployment = deploymentService.save(deployment);
    //
    broadcastDeployment(deployment, String.format("Deployment %s Error", deployment.getId()));
    //
    return deployment;
  }

  /**
   * Do the broadcast of the deployment object.
   *
   * @param deployment object
   * @param message text
   */
  private void broadcastDeployment(Deployment deployment, String message) {
    try {
      queueManagerService.sendDeploymentMessage(deployment, message);
    } catch (Exception e) {
      log.error("Error broadcasting message", e);
    }
  }

  /**
   * Aux method to create an event log.
   *
   * @param type of event
   * @return DeploymentEventLog created
   */
  private static DeploymentEventLog createEvent(DeploymentEventType type) {
    DeploymentEventLog deploymentEventLog = new DeploymentEventLog();
    deploymentEventLog.setType(type);
    deploymentEventLog.setStatus(DeploymentEventStatus.STARTED);
    deploymentEventLog.setCreationDate(new Date());
    return deploymentEventLog;
  }

  /**
   * Aux method to set the finalizing params.
   *
   * @param eventLog event
   * @param error happened
   */
  private static void finalizeEvent(DeploymentEventLog eventLog, boolean error) {
    eventLog.setFinishingDate(new Date());
    eventLog.setStatus(
        error ? DeploymentEventStatus.FINISHED_WITH_ERROR : DeploymentEventStatus.FINISHED);
  }

  /**
   * Verify if deployment can execute the status.
   *
   * @param deployment object
   * @param status of deployment
   * @throws DeploymentNotReadyException if not ready
   * @throws DeploymentStepAlreadyDoneException if already done
   */
  private void verifyStatus(Deployment deployment, DeploymentStatus status)
      throws DeploymentNotReadyException, DeploymentStepAlreadyDoneException {
    if (deployment != null && StringUtils.isNotEmpty(deployment.getId())) {
      Deployment findOne = deploymentService.findById(deployment.getId());

      if (findOne != null) {

        // validate the running status
        if (DeploymentRunningStatus.PAUSED == findOne.getRunnningStatus()) {
          throw new DeploymentNotReadyException("Deployment is paused!");
        }

        // validate status
        if (deployment.getStatus().ordinal() > status.ordinal()) {
          throw new DeploymentStepAlreadyDoneException("Status already executed!");
        }
      }
    }
  }

  /**
   * Deploying.
   *
   * @param deployment object
   * @return Deployment object
   * @throws DeploymentNotReadyException if not ready
   * @throws DeploymentStepAlreadyDoneException if already done
   * @throws DeployingException if something goes wrong
   */
  public Deployment deploying(Deployment deployment)
      throws DeploymentNotReadyException, DeploymentStepAlreadyDoneException, DeployingException {
    this.verifyStatus(deployment, DeploymentStatus.STARTED);

    deployment.setStatus(DeploymentStatus.STARTED);
    deployment.setRunnningStatus(DeploymentRunningStatus.RUNNING);
    //
    DeploymentEventLog eventLog = createEvent(DeploymentEventType.DEPLOY);
    deployment.getEventLogs().add(eventLog);
    deploymentService.save(deployment);

    // broadcast
    broadcastDeployment(deployment, String.format("Deployment %s Started", deployment.getId()));

    // if instance exists - do the deployment/deploy
    DeployerService deployer = deployerFactory.getDeployer(deployment);
    try {
      deployment = deployer.deploy(deployment);
    } catch (DeployFailedException e) {
      throw new DeployingException("Deploying error: \n" + deployment.getReturnedLog());
    }

    //
    eventLog.setFinishingDate(new Date());

    //
    deployment.setStatus(DeploymentStatus.FINISHED);
    deployment.setRunnningStatus(DeploymentRunningStatus.NOT_RUNNING);
    deployment.setFinishingDate(new Date());
    //
    DeploymentEventLog finishEventLog = createEvent(DeploymentEventType.FINISHED);
    deployment.getEventLogs().add(finishEventLog);
    //
    deploymentService.save(deployment);

    // broadcast
    broadcastDeployment(deployment, String.format("Deployment %s Finished", deployment.getId()));
    return deployment;
  }

  /**
   * Create the instances.
   *
   * @param deployment object
   * @return Deployment object
   * @throws DeploymentNotReadyException if not ready
   * @throws DeploymentStepAlreadyDoneException if already done
   * @throws DeploymentProvisioningException if something goes wrong
   */
  public Deployment provisioning(Deployment deployment) throws DeploymentNotReadyException,
      DeploymentStepAlreadyDoneException, DeploymentProvisioningException {
    this.verifyStatus(deployment, DeploymentStatus.PROVISIONING);

    String message = null;

    // create instances
    deployment.setStatus(DeploymentStatus.PROVISIONING);
    deployment.setRunnningStatus(DeploymentRunningStatus.RUNNING);
    deploymentService.save(deployment);

    // set the deployment id
    Map<String,String> tags = new HashMap<>();
    tags.put(cloudos.models.Instance.DEPLOYMENT_ID_TAG, deployment.getId());
    deployment.getCloudCreateRequest().setTag(tags);

    // broadcast
    broadcastDeployment(deployment,
        String.format("Deployment %s Provisioning", deployment.getId()));

    Date timeBeforeCreating = new Date();
    try {

      Providers provider = deployment.getProvider();
      log.info("Provider: {}", provider);
      List<? extends AbstractInstance> createdInstances =
          cloudManagerService.createInstances(deployment.getCloudCreateRequest());
      Date creationDate = timeBeforeCreating;
      for (AbstractInstance instance : createdInstances) {
        // event
        DeploymentEventLog eventLogProvisioning =
            createEvent(DeploymentEventType.PROVISION_INSTANCE);
        eventLogProvisioning.setCreationDate(creationDate);
        deployment.getEventLogs().add(eventLogProvisioning);
        eventLogProvisioning.setTitle(instance.getInstance().getProviderId());
        eventLogProvisioning.setDetail(instance.getInstance().getIpAddress());
        finalizeEvent(eventLogProvisioning, false);
        creationDate = instance.getInstance().getCreationDate();
      }
      deployment.setInstances(createdInstances);
      deploymentService.save(deployment);

    } catch (InstanceRequestException | NotImplementedException e) {
      // failed provisioning
      DeploymentEventLog eventLogProvisioning = createEvent(DeploymentEventType.PROVISION_INSTANCE);
      eventLogProvisioning.setCreationDate(timeBeforeCreating);
      deployment.getEventLogs().add(eventLogProvisioning);
      eventLogProvisioning.setDetail(e.getLocalizedMessage());
      eventLogProvisioning.setLog(e.getLocalizedMessage());
      finalizeEvent(eventLogProvisioning, true);
      deploymentService.save(deployment);
      message = e.getLocalizedMessage();

    } finally {
      if (StringUtils.isNotBlank(message)) {
        log.error("Error provisioning the instances");
        // TODO: handle exception
        deployment.setStatus(DeploymentStatus.ERROR);
        deployment.setRunnningStatus(DeploymentRunningStatus.NOT_RUNNING);
        deployment.setReturnedLog("Error provisioning the instances. \n");
        deploymentService.save(deployment);

        throw new DeploymentProvisioningException(message);
      }
    }
    return deployment;
  }

  /**
   * Execute the rollback on error Terminate the created instances.
   *
   * @param deployment object
   * @return Deployment object after rollback
   */
  private Deployment executeRollbackOnError(Deployment deployment) {
    if (!CollectionUtils.isEmpty(deployment.getInstances())) {
      List<String> names = deployment.getInstances().stream()
          .map(t -> t.getInstance().getProviderId()).collect(Collectors.toList());
      try {
        CloudActionRequest request = new CloudActionRequest(deployment.getProvider(),
            names, deployment.getCloudCreateRequest().getRegion(),
            deployment.getCloudCreateRequest().getZone(),
            deployment.getCloudCreateRequest().getSecurityGroup(), true, ActionType.TERMINATE);
        request.setFireNotification(true);
        request.setNotificationType(NotificationType.DEPLOYMENTS);
        cloudManagerService.terminateInstances(request);
        // update the instances on the deployment
        List<Instance> instances =
            instanceService.findByProviderAndProviderIdIn(deployment.getProvider(), names);
        for (AbstractInstance abstractInstance : deployment.getInstances()) {
          for (Instance instance : instances) {
            if (abstractInstance.getInstance().getId().equals(instance.getId())) {
              abstractInstance.setInstance(instance);
              break;
            }
          }
        }
        return deploymentService.save(deployment);
      } catch (Exception e1) {
        log.error("Error terminating instances for deployment {}", deployment.getId(), e1);
      }
    }
    return deployment;
  }

  /**
   * Stop a deployment.
   *
   * @param deployment object
   * @return Deployment after stop
   */
  public Deployment stopDeploying(Deployment deployment) {
    deployment.setRunnningStatus(DeploymentRunningStatus.PAUSED);
    Deployment save = deploymentService.save(deployment);
    if (asyncMap.containsKey(deployment.getId())) {
      // stop thread here
      stopThreadDeploy(deployment.getId());
    } else {
      // send message to the queue to the server who is executing the deployment
      queueManagerService.sendDeploymentStopMessage(deployment.getId());
    }
    return save;
  }

  public class DeploymentNotReadyException extends Exception {
    private static final long serialVersionUID = -8687719692604227584L;

    public DeploymentNotReadyException(String message) {
      super(message);
    }
  }

  public class DeploymentStepAlreadyDoneException extends Exception {
    private static final long serialVersionUID = -8687719692604227584L;

    public DeploymentStepAlreadyDoneException(String message) {
      super(message);
    }
  }

  public class DeployingException extends Exception {
    private static final long serialVersionUID = 146966005260484792L;

    public DeployingException(String message) {
      super(message);
    }
  }

  public class DeploymentProvisioningException extends Exception {
    private static final long serialVersionUID = 146966005260484792L;

    public DeploymentProvisioningException(String message) {
      super(message);
    }
  }

  public class DeploymentConfiguringClientException extends Exception {
    private static final long serialVersionUID = 146966005260484792L;

    public DeploymentConfiguringClientException(String message) {
      super(message);
    }
  }
}
