package cloudos.deploy.deployers;

import cloudos.deploy.deployments.Deployment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Deployer Factory.
 *
 * @author Philippe Ribeiro
 */
@Service
public class DeployerFactory {

  @Autowired
  private AnsibleService ansibleService;

  @Autowired
  private SaltService saltService;

  /**
   * Use the getDeployer method to get object of the type Deployer.
   *
   * @param deployment The deployment information.
   * @return A Deployer type object.
   */
  public DeployerService getDeployer(Deployment deployment) {
    if (Deployers.ANSIBLE.equals(deployment.getDeployer())) {
      return this.ansibleService;
    } else if (Deployers.CHEF.equals(deployment.getDeployer())) {
      return null;
    } else if (Deployers.SALT.equals(deployment.getDeployer())) {
      return this.saltService;
    } else if (Deployers.PUPPET.equals(deployment.getDeployer())) {
      return null;
    }
    return null;
  }
}
