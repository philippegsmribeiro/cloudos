package cloudos.deploy.deployers;

public enum Deployers {

  /** Ansible. */
  ANSIBLE,
  /** Chef. */
  CHEF,
  /** Salt. */
  SALT,
  /** Puppet. */
  PUPPET
}
