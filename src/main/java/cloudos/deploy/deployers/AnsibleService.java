package cloudos.deploy.deployers;

import cloudos.Providers;
import cloudos.SecurityGroupManagerService;
import cloudos.deploy.DeployConfigurationFileException;
import cloudos.deploy.DeployFailedException;
import cloudos.deploy.deployments.Deployment;
import cloudos.deploy.deployments.DeploymentStatus;
import cloudos.exceptions.NotImplementedException;
import cloudos.keys.KeyManagementException;
import cloudos.keys.KeyManagerService;
import cloudos.keys.cloudoskey.CloudosKey;
import cloudos.models.AbstractInstance;
import cloudos.models.Instance;
import cloudos.provider.AmazonProviderManagement;
import cloudos.utils.OsUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.extern.log4j.Log4j2;
import net.lingala.zip4j.core.ZipFile;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/** Ansible implementation. */
@Service
@Log4j2
public class AnsibleService implements DeployerService {

  private static final String ERROR_MESSAGE_INVALID_KEY =
      "Private key not found for %s. Please load it first.";
  private static final String VALIDATION_PLAY_RECAP =
      "PLAY RECAP *********************************************************************";
  private static final String VALIDATION_EXPECTED_ERROR =
      " [WARNING]: provided hosts list is empty, only localhost is available";
  private static final String VALIDATION_EXPECTED_ERROR2 =
      " [WARNING]: Could not match supplied host pattern, ignoring: all";
  private static final String VALIDATION_EXPECTED_ERROR3 =
      ". Note\nthat the implicit localhost does not match 'all'";
  private static final String ANSIBLE_LOG_FAILED_0 = "failed=0";
  private static final String ANSIBLE_LOG_UNREACHABLE_0 = "unreachable=0";
  private static final String ANSIBLE_LOG_UNREACHABLE_MESSAGE = "UNREACHABLE! =>";

  @Value("${cloudos.build.directory}")
  private String deploymentsDirectory;

  @Autowired
  private KeyManagerService keyManagerService;
  @Autowired
  private AmazonProviderManagement amazonProviderManagement;
  @Autowired
  private SecurityGroupManagerService securityGroupManagerService;

  private static final String INVENTORY_NAME = "inventoryCloudOS.txt";
  private static final String[] PLAYBOOK_FILE_NAMES = {"playbook.yml", "main.yml"};
  private static final String LOGFILE = "deploy.log";
  private static final String SCRIPTFILE = "ansible.sh";

  private static final String ANSIBLE_COMMAND_HOST_KEY_CHECK =
      "export ANSIBLE_HOST_KEY_CHECKING=false;";
  private static final String ANSIBLE_COMMAND = ANSIBLE_COMMAND_HOST_KEY_CHECK
      + " chmod 600 %s; ansible-playbook -u %s -i %s '%s' --sudo > ./%s 2>&1";
  private static final String ANSIBLE_PLAYBOOK_TEST_COMMAND =
      ANSIBLE_COMMAND_HOST_KEY_CHECK + " ansible-playbook '%s' --syntax-check --sudo > ./%s 2>&1";

  private static final String UNIX_CMD = "sh %s";
  private static final String WINDOWS_CMD = "bash -c \"%s\"";

  private static final String WINDOWS_BASH_KEY_DIR = "~/cloudos/deployments/%s/%s";

  /*
   * (non-Javadoc)
   *
   * @see cloudos.deployments.DeployerService#deploy(cloudos.deployments.Deployment)
   */
  @Override
  public Deployment deploy(Deployment deployment) throws DeployFailedException {
    Path deploymentDirectoryPath = null;
    try {
      // create deployment dir
      deploymentDirectoryPath = createDeploymentDir(deployment, deploymentsDirectory);

      // get the playbook file name
      String playbookFileName = getConfigFile(deployment, deploymentDirectoryPath);
      // get the key
      String keyName =
          createKeyFile(deployment, deployment.getInstances(), deploymentDirectoryPath);
      // create the inventory file
      String inventoryFileName =
          createInventoryFile(deployment.getInstances(), deploymentDirectoryPath, keyName);

      Providers provider = deployment.getProvider();

      // TODO: review it - maybe the user should be retrieved from the instance object
      String user = "root";
      if (Providers.GOOGLE_COMPUTE_ENGINE.equals(provider)) {
        user = "cloudos";
      } else if (Providers.AMAZON_AWS.equals(provider)) {
        Instance instance = deployment.getInstances().get(0).getInstance();
        user = amazonProviderManagement.retrieveUserBasedOnTheMachineTypeForInstance(instance);
      }

      // verify security group ssh access
      Instance instance = deployment.getInstances().get(0).getInstance();
      securityGroupManagerService.verifySshAccessForSecurityGroup(provider,
          instance.getSecurityGroup(), instance.getRegion());

      // run the command
      String logText = null;
      int attempt = 0;
      // do few attempts of connecting
      int numberMaxOfAttempts = 3;
      do {
        logText =
            runCommand(deploymentDirectoryPath, user, playbookFileName, inventoryFileName, keyName);
        if (StringUtils.isNotBlank(logText) && logText.contains(ANSIBLE_LOG_UNREACHABLE_MESSAGE)) {
          attempt++;
          // if it wasn't able to connect
          try {
            // wait few seconds
            Thread.sleep(15000);
          } catch (Exception e) {
            log.error(e);
          }
        } else {
          break;
        }
      } while (attempt < numberMaxOfAttempts);
      log.info(logText);
      deployment.setReturnedLog(logText);
      // analyse the log
      if (StringUtils.isBlank(logText)) {
        throw new DeployFailedException("Possible error on the playbook!");
      }
      if (StringUtils.isNotBlank(logText) && logText.contains(ANSIBLE_LOG_UNREACHABLE_0)
          && logText.contains(ANSIBLE_LOG_FAILED_0)) {
        // if no errors
        deployment.setStatus(DeploymentStatus.FINISHED);
      } else {
        // if another unpredictable error
        throw new DeployFailedException("Playbook failed, please have a look on the log!");
      }
    } catch (DeployFailedException e) {
      throw e;
    } catch (Exception e) {
      log.error("Generic error.", e);
    } finally {
      if (deploymentDirectoryPath != null) {
        // deleteDir(deploymentDirectoryPath);
      }
    }
    return deployment;
  }

  /**
   * Creates the deployment directory - all files are create inside this dir.
   *
   * @param deployment object
   * @param deploymentsDirectory path
   * @return Path
   */
  private static Path createDeploymentDir(Deployment deployment, String deploymentsDirectory) {
    // create deployment dir
    File deploymentDir =
        new File(deploymentsDirectory + File.separator + "deployment_" + deployment.getId());
    deploymentDir.mkdirs();
    Path deploymentDirectoryPath = deploymentDir.toPath();
    log.info("deployment dir: " + deploymentDirectoryPath.toAbsolutePath().toString());
    return deploymentDirectoryPath;
  }

  /**
   * Get the config file (playbook) path.
   *
   * @param deployment object
   * @param deploymentDirectoryPath path
   * @return String with the config file name
   * @throws DeployFailedException if something goes wrong
   */
  private static String getConfigFile(Deployment deployment, Path deploymentDirectoryPath)
      throws DeployFailedException {
    try {

      if (StringUtils.isNotBlank(deployment.getConfiguration())) {
        // if is standard text config
        // save it as playbook.yml
        String playbookFileName = PLAYBOOK_FILE_NAMES[0];
        File playbookFile =
            new File(deploymentDirectoryPath.toString() + File.separator + playbookFileName);
        Files.write(playbookFile.toPath(), deployment.getConfiguration().getBytes());
        log.info("playbook file: " + playbookFile.toURI().toString());
        log.info("playbook file content: " + deployment.getConfiguration().toString());
        return playbookFileName;
      }

      File configFile = null;

      if (StringUtils.isNotBlank(deployment.getConfigFilePath())) {
        // if uploaded file
        configFile = new File(deployment.getConfigFilePath());
      }

      //
      if (configFile == null || !configFile.exists()) {
        throw new DeployFailedException("Deploy configuration file not found!");
      }

      ZipFile zipFile = new ZipFile(configFile);
      // verify if is a zip file
      if (!zipFile.isValidZipFile()) {
        // is not a zip file
        // try to read as a text file
        Files.copy(configFile.toPath(),
            new File(deploymentDirectoryPath.toString(), configFile.getName()).toPath(),
            StandardCopyOption.REPLACE_EXISTING);
        return configFile.getName();
      }

      // it is a zip file

      // unzip it
      String zipDestPath =
          deploymentDirectoryPath.toAbsolutePath() + File.separator + "" + configFile.getName();
      zipFile.extractAll(zipDestPath);

      String playbookFileName = null;
      // check if playbook is inside the zip files
      for (int i = 0; i < PLAYBOOK_FILE_NAMES.length; i++) {
        // search the files
        Path p = Paths.get(zipDestPath);
        // search 2 levels deep
        final int maxDepth = 2;
        String fileName = PLAYBOOK_FILE_NAMES[i];
        Stream<Path> matches = Files.find(p, maxDepth,
            (path, basicFileAttributes) -> path.getFileName().toString().equals(fileName));
        List<Path> collect = matches.collect(Collectors.toList());
        matches.close();
        if (collect.size() > 0) {
          // sort to get the closest
          Collections.sort(collect, new Comparator<Path>() {
            @Override
            public int compare(Path o1, Path o2) {
              return Integer.compare(o1.toString().length(), o2.toString().length());
            }
          });
          Path path = collect.get(0);
          Path base = deploymentDirectoryPath;
          // get the relative path
          return base.toUri().relativize(path.toUri()).getPath();
        }
      }

      if (StringUtils.isBlank(playbookFileName)) {
        throw new DeployFailedException(
            "Playbook file not found. The file must contains one of those file: "
                + Arrays.toString(PLAYBOOK_FILE_NAMES));
      }
      return playbookFileName;
    } catch (DeployFailedException e) {
      throw e;
    } catch (Exception e) {
      throw new DeployFailedException("Error reading the config file: " + e.getLocalizedMessage());
    }
  }

  /**
   * Run the ansible command.
   *
   * @param deploymentDirectoryPath path
   * @param user name
   * @param playbookFileName playbook
   * @param inventoryFileName inventory
   * @param keyName key
   * @return log
   * @throws DeployFailedException if something goes wrong
   */
  private static String runCommand(Path deploymentDirectoryPath, String user,
      String playbookFileName, String inventoryFileName, String keyName)
      throws DeployFailedException {
    try {
      String logFileName = LOGFILE;
      String scriptName = SCRIPTFILE;

      if (SystemUtils.IS_OS_WINDOWS) {
        // replace to bash path
        playbookFileName = OsUtils.convertWindowsPathToWindowsBashPath(playbookFileName);
        if (playbookFileName.startsWith("/")) {
          playbookFileName = playbookFileName.substring(1);
        }
        inventoryFileName = OsUtils.convertWindowsPathToWindowsBashPath(inventoryFileName);
      }

      // run the command
      String command = String.format(ANSIBLE_COMMAND, keyName, user, inventoryFileName,
          playbookFileName, logFileName);
      // create shell script
      File scriptFile = new File(deploymentDirectoryPath.toString() + File.separator + scriptName);
      Files.write(scriptFile.toPath(), command.getBytes());
      String scriptFilePath = scriptFile.getAbsolutePath();
      if (SystemUtils.IS_OS_WINDOWS) {
        scriptFilePath = OsUtils.convertWindowsPathToWindowsBashPath(scriptFilePath);
      }
      command = String.format(UNIX_CMD, scriptFilePath);
      if (SystemUtils.IS_OS_WINDOWS) {
        String fileName = keyName.substring(keyName.lastIndexOf("/") + 1);
        String homeDir = keyName.substring(0, keyName.lastIndexOf("/"));
        String originalDir =
            OsUtils.convertWindowsPathToWindowsBashPath(deploymentDirectoryPath.toString());
        command = String.format(WINDOWS_CMD, "mkdir -p " + homeDir + " && cp " + originalDir + "/"
            + fileName + " " + keyName + " && " + command);
      }
      String[] tokens = command.split("\\s+");
      ProcessBuilder builder = new ProcessBuilder(tokens);
      log.info(String.format("Executing %s in directory %s", command,
          deploymentDirectoryPath.toAbsolutePath()));
      builder.directory(deploymentDirectoryPath.toFile());
      builder.redirectErrorStream(true);
      Process child = builder.start();
      watch(child);
      // TODO: revisit it
      child.waitFor();
      File logFile = new File(deploymentDirectoryPath.toString() + File.separator + logFileName);
      String logText = new String(Files.readAllBytes(logFile.toPath()));
      log.info(logText);
      return logText;
    } catch (Exception e) {
      throw new DeployFailedException(
          "Error executing ansible command: " + e.getLocalizedMessage());
    }
  }

  private static void watch(final Process process) {
    new Thread() {
      @Override
      public void run() {
        BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line = null;
        try {
          log.info("Executing Ansible - START");
          while ((line = input.readLine()) != null) {
            log.info(line);
          }
          log.info("Executing Ansible - END");
        } catch (IOException e) {
          log.error(e.getMessage());
        }
      }
    }.start();
  }

  /**
   * Create the inventory file.
   *
   * @param instances list
   * @return inventory name
   * @throws DeployFailedException if something goes wrong
   */
  private String createInventoryFile(List<? extends AbstractInstance> instances,
      Path deploymentDirectoryPath, String keyName) throws DeployFailedException {
    try {
      StringBuffer text = new StringBuffer();
      text.append("[cloudos]").append("\n");
      for (AbstractInstance instance : instances) {
        String ip = instance.getInstance().getIpAddress();
        String name = StringUtils.isNotBlank(instance.getInstance().getName())
            ? instance.getInstance().getName()
            : ip.replace('.', '_');
        String line = String.format("%s ansible_ssh_host=%s ansible_ssh_private_key_file=%s", name,
            ip, keyName);
        text.append(line).append("\n");
      }
      // write the inventory file
      File inventoryFile = new File(deploymentDirectoryPath.toString(), INVENTORY_NAME);
      Files.write(inventoryFile.toPath(), text.toString().getBytes());
      return inventoryFile.getName();
    } catch (Exception e) {
      log.error("Error generating the inventory file", e);
      throw new DeployFailedException(
          "Error generating the inventory file. " + e.getLocalizedMessage());
    }
  }

  /**
   * Create the key file.
   *
   * @param deployment info
   * @param instances list
   * @return key file name
   * @throws DeployFailedException if something goes wrong
   */
  private String createKeyFile(Deployment deployment, List<? extends AbstractInstance> instances,
      Path deploymentDirectoryPath) throws DeployFailedException {
    try {
      CloudosKey key = null;
      for (AbstractInstance instance : instances) {
        key = keyManagerService.findKey(instance.getInstance().getKeyName(),
            instance.getInstance().getProvider(), instance.getInstance().getZone());
        if (key == null) {
          throw new DeployFailedException("Key not found.");
        }
      }
      // write the key file
      File newKeyFile = new File(deploymentDirectoryPath.toString(), key.getKeyName());
      if (!newKeyFile.exists()) {
        Files.write(newKeyFile.toPath(), key.getPrivateKeyContent().getBytes());
      }

      if (SystemUtils.IS_OS_WINDOWS) {
        return String.format(WINDOWS_BASH_KEY_DIR, deployment.getId(), newKeyFile.getName());
      }
      return newKeyFile.getName();
    } catch (DeployFailedException e) {
      throw e;
    } catch (Exception e) {
      log.error("Error generating the key file", e);
      throw new DeployFailedException("Error generating the key file. " + e.getLocalizedMessage());
    }
  }

  /*
   * (non-Javadoc)
   *
   * @see cloudos.deployments.DeployerService#validateConfiguration(cloudos.deployments.Deployment)
   */
  @Override
  public boolean validateConfiguration(Deployment deployment)
      throws DeployConfigurationFileException {
    try {
      Path deploymentDirectoryPath = null;
      // create deployment dir
      deploymentDirectoryPath = createDeploymentDir(deployment, deploymentsDirectory);
      // get the playbook file name
      String playbookFileName = getConfigFile(deployment, deploymentDirectoryPath);
      // validate
      String testLog = runTestCommand(deploymentDirectoryPath, playbookFileName);
      //
      String error1 = VALIDATION_EXPECTED_ERROR;
      String error2 = VALIDATION_EXPECTED_ERROR2;
      String error3 = VALIDATION_EXPECTED_ERROR3;
      String playrecap = VALIDATION_PLAY_RECAP;
      String playbookName = "playbook: " + playbookFileName;
      testLog = testLog.replace(error1, "")
                       .replace(error2, "")
                       .replace(error3, "")
                       .replace(playrecap, "")
                       .replace(playbookName, "");
      if (StringUtils.isNotBlank(testLog)) {
        log.info(testLog);
        throw new DeployConfigurationFileException(testLog.trim());
      }

      // validate key
      String keyName = deployment.getCloudCreateRequest().getKeyName();
      Providers provider = deployment.getCloudCreateRequest().getProvider();
      String region = deployment.getCloudCreateRequest().getRegion();
      CloudosKey findKey = keyManagerService.findKey(keyName, provider, region);
      if (findKey == null || StringUtils.isBlank(findKey.getPrivateKeyContent())) {
        throw new DeployConfigurationFileException(
            String.format(ERROR_MESSAGE_INVALID_KEY, keyName));
      }
    } catch (DeployFailedException e) {
      throw new DeployConfigurationFileException(
          "Error trying to validate the file. " + e.getMessage());
    } catch (KeyManagementException | NotImplementedException e) {
      throw new DeployConfigurationFileException(
          "Error trying to validate the key. " + e.getMessage());
    }
    return true;
  }

  /**
   * Run a test command to check the playbook.
   *
   * @param deploymentDirectoryPath path
   * @param playbookFileName playbook
   * @return log
   * @throws DeployFailedException if something goes wrong
   */
  private static String runTestCommand(Path deploymentDirectoryPath, String playbookFileName)
      throws DeployFailedException {
    try {
      String logFileName = "test" + LOGFILE;
      String scriptName = "test" + SCRIPTFILE;

      if (SystemUtils.IS_OS_WINDOWS) {
        // replace to bash path
        playbookFileName = OsUtils.convertWindowsPathToWindowsBashPath(playbookFileName);
        if (playbookFileName.startsWith("/")) {
          playbookFileName = playbookFileName.substring(1);
        }
      }

      // run the command
      String command = String.format(ANSIBLE_PLAYBOOK_TEST_COMMAND, playbookFileName, logFileName);
      // create shell script
      File scriptFile = new File(deploymentDirectoryPath.toString() + File.separator + scriptName);
      Files.write(scriptFile.toPath(), command.getBytes());
      String scriptFilePath = scriptFile.getAbsolutePath();
      if (SystemUtils.IS_OS_WINDOWS) {
        scriptFilePath = OsUtils.convertWindowsPathToWindowsBashPath(scriptFilePath);
      }
      command = String.format(UNIX_CMD, scriptFilePath);
      if (SystemUtils.IS_OS_WINDOWS) {
        command = String.format(WINDOWS_CMD, command);
      }
      String[] tokens = command.split("\\s+");
      ProcessBuilder builder = new ProcessBuilder(tokens);
      log.info(String.format("Test - Executing %s in directory %s", command,
          deploymentDirectoryPath.toAbsolutePath()));
      builder.directory(deploymentDirectoryPath.toFile());
      builder.redirectErrorStream(true);
      Process child = builder.start();
      watch(child);
      // TODO: revisit it
      child.waitFor();
      log.info("------------------------------------------------");
      File logFile = new File(deploymentDirectoryPath.toString() + File.separator + logFileName);
      String logText = new String(Files.readAllBytes(logFile.toPath()));
      log.info(logText);
      return logText;
    } catch (Exception e) {
      throw new DeployFailedException(
          "Error executing ansible Test command: " + e.getLocalizedMessage());
    }
  }
}
