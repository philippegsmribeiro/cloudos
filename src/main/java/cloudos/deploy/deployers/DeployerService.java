package cloudos.deploy.deployers;

import cloudos.deploy.DeployConfigurationFileException;
import cloudos.deploy.DeployFailedException;
import cloudos.deploy.deployments.Deployment;

public interface DeployerService {

  /**
   * Execute the deploy of the deployment.
   *
   * @param deployment object
   * @return deployment object
   * @throws DeployFailedException if something goes wrong
   */
  Deployment deploy(Deployment deployment) throws DeployFailedException;

  /**
   * Validate if deployment configuration is valid.
   *
   * @param deployment object
   * @return true if it is valid
   * @throws DeployConfigurationFileException if something goes wrong
   */
  default boolean validateConfiguration(Deployment deployment)
      throws DeployConfigurationFileException {
    return true;
  }
}
