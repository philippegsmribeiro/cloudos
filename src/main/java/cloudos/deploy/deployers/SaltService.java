package cloudos.deploy.deployers;

import cloudos.deploy.deployments.Deployment;
import cloudos.utils.FileUtils;

import com.amazonaws.services.ec2.model.Instance;
import com.jcabi.ssh.SSH;
import com.jcabi.ssh.Shell;
import com.suse.salt.netapi.AuthModule;
import com.suse.salt.netapi.calls.LocalCall;
import com.suse.salt.netapi.calls.SaltSSHConfig;
import com.suse.salt.netapi.calls.WheelResult;
import com.suse.salt.netapi.calls.modules.Grains;
import com.suse.salt.netapi.calls.modules.Locate;
import com.suse.salt.netapi.calls.modules.Test;
import com.suse.salt.netapi.calls.wheel.Key;
import com.suse.salt.netapi.client.SaltClient;
import com.suse.salt.netapi.datatypes.Event;
import com.suse.salt.netapi.datatypes.Token;
import com.suse.salt.netapi.datatypes.cherrypy.Stats;
import com.suse.salt.netapi.datatypes.target.Glob;
import com.suse.salt.netapi.datatypes.target.MinionList;
import com.suse.salt.netapi.datatypes.target.SSHTarget;
import com.suse.salt.netapi.datatypes.target.Target;
import com.suse.salt.netapi.event.EventListener;
import com.suse.salt.netapi.event.EventStream;
import com.suse.salt.netapi.exception.SaltException;
import com.suse.salt.netapi.results.Result;
import com.suse.salt.netapi.results.SSHResult;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Level;
import java.util.stream.Collectors;

import javax.websocket.CloseReason;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Implement the Salt service API. It will create a Salt deployment system. A Salt System
 * constitutes of a master and minions. The default scenario is the master and minion are located in
 * the same instance.
 *
 * @author philipperibeiro
 */
@Component
@Log4j2
public class SaltService implements DeployerService {

  // I am supporting only AWS for now.
  // @TODO: support other cloud providers.
  private Instance master;
  private Set<Instance> minions;
  private SaltClient client;

  @Value("${cloudos.build.salt.credentials}")
  private String credentials;

  @Value("${cloudos.build.salt.script.master}")
  private String masterScript;

  @Value("${cloudos.build.salt.host}")
  private String host;

  @Value("${cloudos.build.salt.username}")
  private String username;

  @Value("${cloudos.build.salt.password}")
  private String password;

  private static final int DEFAULT_SSH_PORT = 22;
  private static final int DEFAULT_SALT_PORT = 8000;
  private static final int NTHREDS = 10;
  private static final String SUCCESS = "SUCCESS";
  private static final String FAILURE = "FAILURE";

  /** Default constructor. */
  public SaltService() {}

  /**
   * Create constructor specifying the minions.
   *
   * @param minions A set of minions
   */
  public SaltService(Set<Instance> minions) {
    this.minions = minions;
  }

  /**
   * Define constructor for the Salt service API.
   *
   * @param master The salt master server.
   */
  public SaltService(Instance master) {
    this.master = master;
    this.minions = new HashSet<>();
    this.setSaltClient(master.getPublicIpAddress());
  }

  /**
   * Define constructor for the Salt service API.
   *
   * @param master The salt master server.
   * @param minions The minion instances where the provisioning will take place.
   */
  public SaltService(Instance master, Set<Instance> minions) {
    this(master);
    this.minions = minions;
    this.setSaltClient(master.getPublicIpAddress());
  }

  /**
   * Define constructor for the Salt service API.
   *
   * @param master The salt master server.
   * @param minions The minion instances where the provisioning will take place.
   * @param credentials The credentials to access the cloud provider.
   */
  public SaltService(Instance master, Set<Instance> minions, String credentials) {
    this(master);
    this.minions = minions;
    this.credentials = credentials;
    this.setSaltClient(master.getPublicIpAddress());
  }

  /** Init. */
  public void initSaltCluster() throws InterruptedException {
    // configure the minions first
    Runnable task =
        () -> {
          SaltService.this.initMinions();
          SaltService.this.configureCluster();
        };
    Thread thread = new Thread(task);
    thread.run();
  }

  /**
   * Config cluster.
   *
   * @return status
   */
  private String configureCluster() {
    try {
      // configure the minions
      String command =
          String.format(
              "sudo find /etc/salt/minion -type f -exec sed -i 's/#master: salt/master: 40.71.24.194/g' {} \\; && sudo service salt-minion restart",
              this.host);
      log.info(command);
      for (Instance instance : minions) {
        String output =
            new Shell.Plain(
                    new SSH(
                        instance.getPublicIpAddress(),
                        DEFAULT_SSH_PORT,
                        "ubuntu",
                        this.credentials))
                .exec(command);
        log.info(output);
      }

      // configure the master
      String output =
          new Shell.Plain(
                  new SSH(
                      this.master.getPublicIpAddress(), DEFAULT_SSH_PORT, "ubuntu", this.password))
              .exec("sudo salt-key -A -y");
      log.info(output);

      return SUCCESS;
    } catch (Exception ex) {
      log.error(ex.getMessage());
    }
    return FAILURE;
  }

  /** Init will be responsible for installing the Salt Master in the Instance given. */
  private String initMaster() {
    try {
      if (this.master == null || this.master.getPublicIpAddress() == null) {
        throw new Exception(
            String.format("Public IP Address for instance '%s' cannot be null", this.master));
      }
      Shell shell =
          new SSH(this.master.getPublicIpAddress(), DEFAULT_SSH_PORT, "ubuntu", this.credentials);
      File script = new File(this.masterScript);
      new Shell.Safe(shell)
          .exec(
              "cat > install_salt.sh && sudo bash install_salt.sh -L",
              new FileInputStream(script),
              com.jcabi.log.Logger.stream(Level.INFO, this),
              com.jcabi.log.Logger.stream(Level.WARNING, this));
      return SUCCESS;
    } catch (Exception ex) {
      log.error(ex.getMessage());
    }
    return FAILURE;
  }

  /**
   * Init minions.
   *
   * @return status
   */
  private String initMinions() {
    try {
      File script = new File(this.masterScript);
      for (Instance instance : minions) {
        if (instance.getPublicIpAddress() == null) {
          throw new Exception(
              String.format("Public IP Address for instance '%s' cannot be null", instance));
        }
        Shell shell =
            new SSH(instance.getPublicIpAddress(), DEFAULT_SSH_PORT, "ubuntu", this.credentials);
        new Shell.Safe(shell)
            .exec(
                "cat > install_salt.sh && sudo bash install_salt.sh",
                new FileInputStream(script),
                com.jcabi.log.Logger.stream(Level.INFO, this),
                com.jcabi.log.Logger.stream(Level.WARNING, this));
      }
      return SUCCESS;
    } catch (Exception ex) {
      log.error(ex.getMessage());
    }
    return FAILURE;
  }

  /**
   * Key Accept.
   *
   * @param key text
   * @throws SaltException if something goes wrong.
   */
  public void keyAccept(String key) throws SaltException {
    // List accepted and pending minion keys
    WheelResult<Key.Names> keyResults =
        Key.listAll().callSync(this.client, this.username, this.password, AuthModule.PAM);
    Key.Names keys = keyResults.getData().getResult();

    log.info("\n--> Get Minion keys:\n");
    keys.getMinions().forEach(log::info);
  }

  /**
   * Get info.
   *
   * @throws SaltException if something goes wrong.
   */
  public void getInfo() throws SaltException {
    Stats stats = this.client.stats();
    log.info("############ Stats ################");
    log.info(stats);
    log.info("################################");
  }

  /**
   * Locate.
   *
   * @throws SaltException if something goes wrong.
   */
  public void locate() throws SaltException {
    client.login(this.username, this.password, AuthModule.PAM);
    // Ping all minions using a glob matcher
    Target<String> globTarget = new Glob();

    String pattern = "*";

    LocalCall<List<String>> call =
        Locate.locate(pattern, Optional.empty(), Optional.empty(), Optional.empty());
    Map<String, Result<List<String>>> results = call.callSync(client, globTarget);
    log.info("Results without regex, no count:");
    results.forEach((minion, result) -> log.info(minion + " -> " + result.result().get()));

    Locate.LocateOpts opts = new Locate.LocateOpts();
    opts.setRegex(true);
    opts.setCount(true);
    call = Locate.locate(pattern, Optional.empty(), Optional.empty(), Optional.of(opts));
    results = call.callSync(client, globTarget);
    log.info("Results setting regex and count to true:");
    results.forEach((minion, result) -> log.info(minion + " -> " + result.result().get()));

    opts = new Locate.LocateOpts();
    opts.setRegex(true);
    call = Locate.locate(pattern, Optional.empty(), Optional.of(2), Optional.of(opts));
    results = call.callSync(client, globTarget);
    log.info("Results setting regex to true and limiting the results to 2:");
    results.forEach((minion, result) -> log.info(minion + " -> " + result.result().get()));
  }

  /**
   * Events.
   *
   * @throws SaltException if something goes wrong.
   */
  public void events() throws SaltException {
    SaltClient client = new SaltClient(URI.create(this.getUrl()));
    // Get a login token
    Token token = client.login(this.username, this.password, AuthModule.PAM);
    log.info("--------------------------------------");
    log.info("Token: " + token.getToken());
    log.info("--------------------------------------");
    // Init the event stream with a basic listener implementation
    EventStream eventStream =
        client.events(
            new EventListener() {
              @Override
              public void notify(Event e) {
                log.info("--------------------------------------");
                log.info("Tag  -> " + e.getTag());
                log.info("Data -> " + e.getData());
                log.info("--------------------------------------");
              }

              @Override
              public void eventStreamClosed(CloseReason closeReason) {
                log.info("Event stream closed: " + closeReason.getReasonPhrase());
              }
            });

    // Wait for events and close the event stream after 30 seconds
    log.info("-- Waiting for events --");
    try {
      Thread.sleep(30000);
      eventStream.close();
      log.info("-- Stop waiting for events --");
    } catch (InterruptedException | IOException e) {
      log.error(e.getMessage());
    }
  }

  /**
   * Call.
   *
   * @throws SaltException if something goes wrong.
   */
  public void call() throws SaltException {
    // Ping all minions using a glob matcher
    Target<String> globTarget = new Glob("*");
    Map<String, Result<Boolean>> results =
        Test.ping().callSync(this.client, globTarget, this.username, this.password, AuthModule.PAM);
    log.info("--> Ping results:\n");
    results.forEach((minion, result) -> log.info(minion + " -> " + result));

    // Get the grains from a list of minions
    Target<List<String>> minionList = new MinionList("ip-172-31-22-211.ec2.internal");
    Map<String, Result<Map<String, Object>>> grainResults =
        Grains.items(false)
            .callSync(client, minionList, this.username, this.password, AuthModule.PAM);

    grainResults.forEach(
        (minion, grains) -> {
          log.info("\n--> Listing grains for '" + minion + "':\n");
          String grainsOutput =
              grains.fold(
                  error -> "Error: " + error.toString(),
                  grainsMap ->
                      grainsMap
                          .entrySet()
                          .stream()
                          .map(e -> e.getKey() + ": " + e.getValue())
                          .collect(Collectors.joining("\n")));
          log.info(grainsOutput);
        });
  }

  /**
   * wheels.
   *
   * @throws SaltException if something goes wrong.
   */
  public void wheels() throws SaltException {
    SaltClient client = new SaltClient(URI.create(this.getUrl()));
    // Get a login token
    Token token = client.login(this.username, this.password, AuthModule.PAM);
    log.info("--------------------------------------");
    log.info("Token: " + token.getToken());
    log.info("--------------------------------------");
    // List accepted and pending minion keys
    WheelResult<Key.Names> keyResults =
        Key.listAll().callSync(this.client, this.username, this.password, AuthModule.PAM);
    Key.Names keys = keyResults.getData().getResult();

    log.info("\n--> Accepted minion keys:\n");
    keys.getMinions().forEach(log::info);
    log.info("\n--> Pending minion keys:\n");
    keys.getUnacceptedMinions().forEach(log::info);
  }

  /**
   * ssh Config.
   *
   * @throws SaltException if something goes wrong.
   * @throws IOException if something goes wrong.
   */
  public void sshConfig() throws SaltException, IOException {
    String key = FileUtils.getKey(this.credentials);
    // Setup the configuration for Salt SSH (use defaults)
    SaltSSHConfig sshConfig =
        new SaltSSHConfig.Builder()
            .user(this.username)
            .passwd(this.password)
            .ignoreHostKeys(true)
            .build();

    // Ping all minions using a glob matcher
    SSHTarget<String> globTarget = new Glob("*");
    Map<String, Result<SSHResult<Boolean>>> minionResults =
        Test.ping().callSyncSSH(client, globTarget, sshConfig);

    log.info("--> Ping results:");
    minionResults.forEach(
        (minion, result) -> {
          log.info(
              minion
                  + " -> "
                  + result.fold(
                      error -> "Error: " + error.toString(), res -> res.getReturn().orElse(false)));
        });

    // Get grains from all minions
    Map<String, Result<SSHResult<Map<String, Object>>>> grainResults =
        Grains.items(false).callSyncSSH(client, globTarget, sshConfig);

    grainResults.forEach(
        (minion, grains) -> {
          log.info("\n--> Listing grains for '" + minion + "':\n");
          String grainsOutput =
              grains.fold(
                  error -> "Error: " + error.toString(),
                  grainsMap ->
                      grainsMap
                          .getReturn()
                          .map(
                              g ->
                                  g.entrySet()
                                      .stream()
                                      .map(e -> e.getKey() + ": " + e.getValue())
                                      .collect(Collectors.joining("\n")))
                          .orElse("Minion did not return: " + minion));
          log.info(grainsOutput);
        });
  }

  public void addMinions(Set<Instance> extraMinions) {
    this.minions.addAll(extraMinions);
  }

  public void setCredentials(String credentials) {
    this.credentials = credentials;
  }

  public String getCredentials() {
    return this.credentials;
  }

  /**
   * Set the salt-master instance, and allows for method chaining.
   *
   * @param master The salt-master instance
   * @return SaltService: The same instance, with the master set
   */
  public SaltService withMaster(Instance master) {
    this.master = master;
    return this;
  }

  /**
   * Set the salt-minion instances, and allows for method chaining.
   *
   * @param minions A set of minion instances.
   * @return SaltService: The same instance, with the minions set.
   */
  public SaltService withMinions(Set<Instance> minions) {
    this.minions = minions;
    return this;
  }

  /**
   * Create.
   *
   * @param masterIpAddress The IP address of the salt-master
   * @return SaltService: The same SaltService object, however with the salt client configured
   */
  public SaltService withSaltClient(String masterIpAddress) {
    String url = String.format("http://%s:%s", masterIpAddress, DEFAULT_SALT_PORT);
    log.info(url);
    this.client = new SaltClient(URI.create(url));
    return this;
  }

  /**
   * Get the url.
   *
   * @return url
   */
  public String getUrl() {
    String url = String.format("http://%s:%s", this.host, DEFAULT_SALT_PORT);
    return url;
  }

  public void setDefaultSaltClient() {
    String url = String.format("http://%s:%s", this.host, DEFAULT_SALT_PORT);
    this.client = new SaltClient(URI.create(url));
  }

  public void setSaltClient(String masterIpAddress) {
    String url = String.format("http://%s:%s", masterIpAddress, DEFAULT_SALT_PORT);
    this.client = new SaltClient(URI.create(url));
  }

  public SaltClient getClient() {
    return this.client;
  }

  public SaltService withHost(String host) {
    this.host = host;
    return this;
  }

  public SaltService withUsername(String username) {
    this.username = username;
    return this;
  }

  public SaltService withPassword(String password) {
    this.password = password;
    return this;
  }

  public void setMaster(Instance master) {
    this.master = master;
  }

  /**
   * Gets a list of all the minions in the cloud.
   *
   * @return List: A list of minion instances
   */
  public Set<Instance> getMinions() {
    return this.minions;
  }

  /**
   * Get the master.
   *
   * @return Instance: The current salt-master
   */
  public Instance getMaster() {
    return this.master;
  }

  @Override
  public Deployment deploy(Deployment deployment) {
    // TODO Auto-generated method stub
    return deployment;
  }

  public void setMinions(Set<Instance> minions) {
    this.minions = minions;
  }
}
