package cloudos.deploy;

public class DeployConfigurationFileException extends Exception {
  private static final long serialVersionUID = 4652134818457539594L;

  public DeployConfigurationFileException(String message) {
    super(message);
  }
}
