package cloudos.deploy;

public class DeployFailedException extends Exception {
  private static final long serialVersionUID = 4652134818457539594L;

  public DeployFailedException(String message) {
    super(message);
  }
}
