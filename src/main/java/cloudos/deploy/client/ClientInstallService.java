package cloudos.deploy.client;

import cloudos.Providers;
import cloudos.ResourceReadingService;
import cloudos.config.ThreadingConfig;
import cloudos.deploy.DeployFailedException;
import cloudos.deploy.DeploymentManagerService;
import cloudos.deploy.deployers.AnsibleService;
import cloudos.deploy.deployers.Deployers;
import cloudos.deploy.deployments.Deployment;
import cloudos.deploy.deployments.DeploymentRunningStatus;
import cloudos.deploy.deployments.DeploymentStatus;
import cloudos.elasticsearch.fields.SharedBeatFields;
import cloudos.elasticsearch.model.KibanaField;
import cloudos.elasticsearch.model.KibanaSearchParameters;
import cloudos.elasticsearch.model.ProcessDataPoint;
import cloudos.elasticsearch.services.MetricbeatService;
import cloudos.exceptions.NotImplementedException;
import cloudos.instances.InstanceService;
import cloudos.keys.KeyManagementException;
import cloudos.keys.KeyManagerService;
import cloudos.keys.cloudoskey.CloudosKey;
import cloudos.models.AbstractInstance;
import cloudos.models.ClientStatus;
import cloudos.queue.QueueListener;
import cloudos.queue.QueueManagerService;
import cloudos.queue.message.ClientReInstallMessage.ClientReInstallMessageConsumer;
import cloudos.queue.message.InstanceCreatedMessage.InstanceCreatedMessageConsumer;
import cloudos.queue.message.InstanceKeySavedMessage.InstanceKeySavedConsumer;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.model.ZipParameters;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ClientInstallService {

  private static final String MESSAGE_CLIENT_INSTALL_ERROR = "Client Install error!";
  private static final String MESSAGE_CLIENT_INSTALL_FINISHED = "Client Install finished!";
  private static final String MESSAGE_CLIENT_INSTALL_STARTED = "Client Install started!";

  private static final String CLIENT_INSTALL_ZIP = "clientInstall.zip";

  @Value("${cloudos.build.directory}")
  private String deploymentsDirectory;

  @Value("${cloudos.client.ansible.filename}")
  private String ansibleFile;

  @Value("${cloudos.build.directory}")
  private String deployDir;

  @Autowired
  private ResourceReadingService resourceReadingService;

  @Autowired
  private AnsibleService ansibleService;

  @Autowired
  private QueueListener queueListener;

  @Autowired
  private MetricbeatService metricbeatService;

  @Autowired
  private KeyManagerService keyManagerService;

  @Resource
  @Qualifier(ThreadingConfig.CLIENT_INSTALL)
  private AsyncTaskExecutor taskExecutor;

  @Autowired
  private InstanceService instancesService;

  @Autowired
  private QueueManagerService queueManagerService;
  @Autowired
  private DeploymentManagerService deploymentManagerService;

  /** Post constructor method to set up the queue listener. */
  @PostConstruct
  public void configListener() {
    queueListener.addConsumer((InstanceCreatedMessageConsumer) t ->
        verifyAndInstallClient(Arrays.asList(t.getInstance()),
        t.getInstance().getInstance().getName()));

    queueListener.addConsumer((InstanceKeySavedConsumer) t -> {
      // get instances for that key which client is not installed
      List<AbstractInstance> instances =
          instancesService.findNotInstalledClientInstancesByKey(t.getKey());
      verifyAndInstallClient(instances, t.getKey().getKeyName());
    });

    queueListener.addConsumer((ClientReInstallMessageConsumer)
        t -> verifyAndInstallClient(Arrays.asList(t.getInstance()),
        t.getInstance().getInstance().getName()));
  }

  /**
   * Verify if client installation and install it if needed.
   *
   * @param instances list
   * @param deployId identifier for the deploy
   */
  private void verifyAndInstallClient(List<AbstractInstance> instances, String deployId) {
    List<AbstractInstance> instancesToInstall = new ArrayList<>();
    // verify installation
    for (AbstractInstance abstractInstance : instances) {
      if (!verifyClientInstallation(abstractInstance)) {
        // if not installed
        instancesService.setClientStatus(abstractInstance.getInstance(),
            ClientStatus.NOT_INSTALLED);
        // verify requiments
        if (verifyRequirementsForInstallation(abstractInstance)) {
          // add to install
          instancesToInstall.add(abstractInstance);
        }
      } else {
        // if installed
        instancesService.setClientStatus(abstractInstance.getInstance(), ClientStatus.INSTALLED);
      }
    }

    // if there is instances to install, call the installer
    if (CollectionUtils.isNotEmpty(instancesToInstall)) {
      // beats are not installed
      taskExecutor.submit(() -> {
        installClient(instancesToInstall, instancesToInstall.get(0).getInstance().getProvider(),
            deployId);
        return true;
      });
    }
  }

  /**
   * Verify if beats is installed for instance.
   *
   * @param instance to be verified
   * @return true if installed, false otherwise
   */
  private boolean verifyClientInstallation(AbstractInstance instance) {
    try {
      // verify beats
      KibanaSearchParameters kibanaSearchParameters = new KibanaSearchParameters();
      KibanaField hostnameField = new KibanaField(SharedBeatFields.BEAT_HOSTNAME,
          Arrays.asList(instance.getInstance().getName()));
      kibanaSearchParameters.setFilterFields(Arrays.asList(hostnameField));
      List<ProcessDataPoint> searchMatchingByFieldQuery =
          metricbeatService.getTopSystemProcesses(kibanaSearchParameters, 10);
      if (!CollectionUtils.isEmpty(searchMatchingByFieldQuery)) {
        // beats not installed
        return true;
      }
    } catch (Exception e) {
      // TODO: handle exception
      log.error(e);
    }
    return false;
  }

  /**
   * Verify if instance has all requirements to run the installer.
   *
   * @param instance to ve verified
   * @return true if it is ok to install, false otherwise
   */
  private boolean verifyRequirementsForInstallation(AbstractInstance instance) {
    // 1. status need to be NOT_INSTALLED
    if (ClientStatus.NOT_INSTALLED != instance.getInstance().getClientStatus()) {
      return false;
    }

    // 2. cloudos has to have the private key for installation
    if (StringUtils.isBlank(instance.getInstance().getKeyName())) {
      return false;
    }


    try {
      CloudosKey key = keyManagerService.findKey(instance.getInstance().getKeyName(),
          instance.getInstance().getProvider(), instance.getInstance().getZone());

      if (key == null || StringUtils.isBlank(key.getPrivateKeyContent())) {
        return false;
      }
    } catch (NotImplementedException | KeyManagementException e) {
      log.error("Error getting instance key.", e);
      return false;
    }

    // TODO: add more requirements like security group (ssh port)
    return true;

  }



  /**
   * Install the client on the instances.
   *
   * @param instances list
   */
  public Deployment installClient(List<? extends AbstractInstance> instances, Providers provider,
      String deploymentId) {
    if (CollectionUtils.isEmpty(instances)) {
      // no instances to install
      return null;
    }
    try {
      // verify if instance is on a deployment
      Deployment originalDeploy = null;
      AbstractInstance abstractInstance = instances.get(0);
      if (abstractInstance != null) {
        originalDeploy =
            deploymentManagerService.retrieveByInstance(abstractInstance.getInstance());
      }

      // avoid to install the client when instance will be deleted as deployment failed
      if (originalDeploy == null || originalDeploy.getStatus() != DeploymentStatus.ERROR) {

        // 1. create the deployment zip file
        File zip = new File(deployDir, CLIENT_INSTALL_ZIP);
        if (zip.exists()) {
          zip.delete();
        }
        ZipFile zipFile = new ZipFile(zip);

        // 1.1 read the resource dir
        File resourceDir = resourceReadingService.getFile(ansibleFile);
        ZipParameters parameters = new ZipParameters();
        parameters.setIncludeRootFolder(false);
        zipFile.addFolder(resourceDir, parameters);

        // 3. call ansible deployment
        Deployment deployment = new Deployment();
        deployment.setId(String.format("%s-%s",
            originalDeploy != null ? originalDeploy.getId() : deployment.getId(),
            instances.get(0).getInstance().getName()));
        deployment.setConfigFilePath(zip.getAbsolutePath());
        deployment.setInstancesNumber(instances.size());
        deployment.setInstances(instances);
        deployment.setProvider(provider);
        deployment.setDeployer(Deployers.ANSIBLE);
        try {

          saveClientStatus(instances, ClientStatus.INSTALLING);
          // notify
          queueManagerService.sendClientInstallMessage(deployment, MESSAGE_CLIENT_INSTALL_STARTED);

          deployment = ansibleService.deploy(deployment);

        } catch (DeployFailedException e) {
          log.error("Error installing client beats.", e);
          deployment.setStatus(DeploymentStatus.ERROR);
          deployment.setRunnningStatus(DeploymentRunningStatus.NOT_RUNNING);
          deployment.setReturnedLog(StringUtils.isBlank(deployment.getReturnedLog()) ? ""
              : (deployment.getReturnedLog() + "\n\n")
                  + (StringUtils.isBlank(e.getLocalizedMessage()) ? "" : e.getLocalizedMessage()));
          deployment.setFinishingDate(new Date());

          // set status
          saveClientStatus(instances, ClientStatus.ERROR);
          // notify
          queueManagerService.sendClientInstallMessage(deployment, MESSAGE_CLIENT_INSTALL_ERROR);

        } finally {
          if (deployment.getStatus() != DeploymentStatus.ERROR) {
            //
            saveClientStatus(instances, ClientStatus.INSTALLED);
            // notify
            queueManagerService.sendClientInstallMessage(deployment,
                MESSAGE_CLIENT_INSTALL_FINISHED);
          }
        }
        return deployment;
      }

      return null;
    } catch (Exception e) {
      log.error("Error!", e);
      saveClientStatus(instances, ClientStatus.ERROR);
    }
    return null;
  }

  private void saveClientStatus(List<? extends AbstractInstance> instances, ClientStatus status) {
    // set status
    instances.stream().forEach(t -> instancesService.setClientStatus(t.getInstance(), status));
  }
}
