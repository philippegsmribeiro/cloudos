package cloudos.amazon;

/**
 * @Copyright CyberTextron Inc. 2016
 *
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
public enum TimeUnit {
  PER_SECOND,
  PER_MINUTE,
  PER_HOUR,
}
