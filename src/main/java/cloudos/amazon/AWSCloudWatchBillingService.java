package cloudos.amazon;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClientBuilder;
import com.amazonaws.services.cloudwatch.model.Datapoint;
import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsRequest;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsResult;

import java.util.Date;
import java.util.List;

/**
 * Create a CloudWatch based Billing Service. This can be used to fetch costs based within a time
 * window. @Copyright 2017 Cybernetic Cloud Inc.
 *
 * @license: Proprietary License.
 * @author Philippe Ribeiro
 * @date: 1/6/2017
 */
public class AWSCloudWatchBillingService {

  private com.amazonaws.services.cloudwatch.AmazonCloudWatch client;
  private static final String CURRENCY = "Currency";

  /**
   * Create Billing service within a particular region.
   *
   * @param region The name of the AWS region
   */
  public AWSCloudWatchBillingService(String region, AWSCredentialsProvider provider) {
    this.client =
        AmazonCloudWatchClientBuilder.standard()
            .withCredentials(provider)
            .withRegion(region)
            .build();
  }

  /**
   * Get the cost of a particular region.
   *
   * @param interval - the interval to fetch since.
   * @param window - the window period
   * @param currency - the currency
   * @param statistics - which metrics to be fetched
   * @return a list of data points
   */
  public List<Datapoint> getCost(
      long interval, int window, String currency, List<String> statistics) {
    GetMetricStatisticsRequest request =
        new GetMetricStatisticsRequest()
            .withStartTime(new Date(new Date().getTime() - interval))
            .withNamespace(AwsNamespace.AWS_BILLING.toString())
            .withPeriod(window)
            .withDimensions(new Dimension().withName(CURRENCY).withValue(currency))
            .withMetricName(CloudWatchDimensions.ESTIMATED_CHARGES.toString())
            .withStatistics(statistics)
            .withEndTime(new Date());

    GetMetricStatisticsResult result = this.client.getMetricStatistics(request);
    if (result != null) {
      return result.getDatapoints();
    }
    return null;
  }
}
