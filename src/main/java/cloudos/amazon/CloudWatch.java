package cloudos.amazon;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClientBuilder;
import com.amazonaws.services.cloudwatch.model.ComparisonOperator;
import com.amazonaws.services.cloudwatch.model.Datapoint;
import com.amazonaws.services.cloudwatch.model.DeleteAlarmsRequest;
import com.amazonaws.services.cloudwatch.model.DescribeAlarmsRequest;
import com.amazonaws.services.cloudwatch.model.DescribeAlarmsResult;
import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.DisableAlarmActionsRequest;
import com.amazonaws.services.cloudwatch.model.EnableAlarmActionsRequest;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsRequest;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsResult;
import com.amazonaws.services.cloudwatch.model.ListMetricsRequest;
import com.amazonaws.services.cloudwatch.model.ListMetricsResult;
import com.amazonaws.services.cloudwatch.model.Metric;
import com.amazonaws.services.cloudwatch.model.MetricAlarm;
import com.amazonaws.services.cloudwatch.model.MetricDatum;
import com.amazonaws.services.cloudwatch.model.PutMetricAlarmRequest;
import com.amazonaws.services.cloudwatch.model.PutMetricDataRequest;
import com.amazonaws.services.cloudwatch.model.StandardUnit;
import com.amazonaws.services.cloudwatch.model.Statistic;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.extern.log4j.Log4j2;

/**
 * Interface for AWS CloudWatch @Copyright 2017 Cybernetic Cloud Inc.
 *
 * @license: Proprietary License.
 * @author Philippe Ribeiro
 * @date: 1/6/2017
 */
@Log4j2
public class CloudWatch {

  private AmazonCloudWatch cloudWatchClient;

  /**
   * Allow the user to create a CloudWatch client from a specific region
   *
   * @param region: The region as a String
   */
  public CloudWatch(String region, AWSStaticCredentialsProvider provider) {
    this.cloudWatchClient =
        AmazonCloudWatchClientBuilder.standard()
            .withCredentials(provider)
            .withRegion(region)
            .build();
  }

  /**
   * Create a new request for the particular instance. It takes the instanceId, the measure and the
   * statistics being used.
   *
   * @param instances: A list of all the instances to be fetched.
   * @param metric: The metric being obtained.
   * @param statistics: The list of statistics being fetched.
   * @return GetMetricStatisticsRequest: A new metrics request
   */
  private GetMetricStatisticsRequest request(
      final List<String> instances,
      CloudWatchDimensions dimension,
      CloudWatchMetrics metric,
      List<String> statistics,
      final int period,
      final long startTime) {

    /* Generalize the code, and allow it to pass any number of dimensions */
    List<Dimension> dimensions = new ArrayList<>();
    instances.forEach(
        instance -> {
          Dimension dim = new Dimension().withName(dimension.toString()).withValue(instance);
          dimensions.add(dim);
        });

    log.info(dimensions);

    return new GetMetricStatisticsRequest()
        .withStartTime(new Date(new Date().getTime() - startTime))
        .withNamespace(AwsNamespace.AMAZON_EC2.toString())
        .withPeriod(period)
        .withDimensions(dimensions)
        .withMetricName(metric.toString())
        .withStatistics(statistics)
        .withEndTime(new Date());
  }

  /**
   * Get the metric static result from the request
   *
   * @param request: The statistic request
   * @return GetMetricStatisticsResult: The result of the request.
   */
  private GetMetricStatisticsResult result(final GetMetricStatisticsRequest request) {
    return this.cloudWatchClient.getMetricStatistics(request);
  }

  /**
   * @param instances: A list of all the instances to be fetched.
   * @param metric : The measure being obtained.
   * @param statistics : The list of statistics being fetched.
   * @param period: The time interval.
   * @param startTime: The time history, since when we want to start fetching data.
   * @return List: A list of data points
   */
  public List<Datapoint> getDatapoints(
      final List<String> instances,
      CloudWatchDimensions dimension,
      CloudWatchMetrics metric,
      List<String> statistics,
      int period,
      long startTime) {

    GetMetricStatisticsRequest getMetricRequest =
        this.request(instances, dimension, metric, statistics, period, startTime);
    if (getMetricRequest == null) {
      return null;
    }
    GetMetricStatisticsResult getMetricResult = this.result(getMetricRequest);
    return getMetricResult.getDatapoints();
  }

  /**
   * List all of CloudWatch's metrics, allowing the user to filter by name and namespace.
   *
   * @param name: The name of the metric
   * @param namespace: The name of the CloudWatch's namespace service.
   * @return Set: A set of metric objects.
   */
  public Set<Metric> listMetrics(String name, String namespace) {
    Set<Metric> metrics = new HashSet<>();

    boolean done = false;
    while (!done) {
      ListMetricsRequest request =
          new ListMetricsRequest().withMetricName(name).withNamespace(namespace);

      ListMetricsResult response = this.cloudWatchClient.listMetrics(request);
      // add all the metrics
      metrics.addAll(response.getMetrics());
      // get the next token
      request.setNextToken(response.getNextToken());

      if (response.getNextToken() == null) {
        done = true;
      }
    }

    return metrics;
  }

  /**
   * Allow the user to create his own custom metric, independently of the services offered by
   * Amazon's AWS.
   *
   * @param dimensionName: The name of the dimension to be added.
   * @param dimensionValue: The value of the dimension to be added.
   * @param metricName: The name of the metric
   * @param metricValue: The value of the metric
   * @param namespace: The newly created namespace.
   * @return boolean
   */
  public boolean putCustomMetric(
      String dimensionName,
      String dimensionValue,
      String metricName,
      Double metricValue,
      String namespace) {
    // create a new dimension
    Dimension dimension = new Dimension().withName(dimensionName).withValue(dimensionValue);

    // create a new metric data type
    MetricDatum datum =
        new MetricDatum()
            .withMetricName(metricName)
            .withUnit(StandardUnit.None)
            .withValue(metricValue)
            .withDimensions(dimension);

    PutMetricDataRequest request =
        new PutMetricDataRequest().withNamespace(namespace).withMetricData(datum);

    return this.cloudWatchClient.putMetricData(request) != null;
  }

  /**
   * Allow the user to create a new alarm. The alarm will be set of whenever the threshold is met,
   * triggering a sequence of events that will notify the user.
   *
   * @param dimensionName: The name of the dimension
   * @param dimensionValue: The value of the dimension
   * @param alarmName: The name of the alarm
   * @param comparator: The comparator method
   * @param evalPeriod: The evalution period
   * @param metricName: The name of the metric
   * @param namespace: The name of the namespace
   * @param enable: Whether to enable the alarm or not
   * @param threshold: The threshold value
   * @param period: The period the threshold will be checked
   * @param description: The alarm's description
   * @return boolean
   */
  public boolean createAlarm(
      String dimensionName,
      String dimensionValue,
      String alarmName,
      ComparisonOperator comparator,
      Integer evalPeriod,
      String metricName,
      String namespace,
      Boolean enable,
      Double threshold,
      Integer period,
      String description) {
    // create a new dimension
    Dimension dimension = new Dimension().withName(dimensionName).withValue(dimensionValue);
    // create the request for the alarm
    PutMetricAlarmRequest request =
        new PutMetricAlarmRequest()
            .withAlarmName(alarmName)
            .withComparisonOperator(comparator)
            .withEvaluationPeriods(evalPeriod)
            .withMetricName(metricName)
            .withNamespace(namespace)
            .withPeriod(period)
            .withStatistic(Statistic.Average)
            .withThreshold(threshold)
            .withActionsEnabled(enable)
            .withAlarmDescription(description)
            .withUnit(StandardUnit.Seconds)
            .withDimensions(dimension);

    return this.cloudWatchClient.putMetricAlarm(request) != null;
  }

  /**
   * List all the existing alarms in AWS.
   *
   * @return Set: A set of the metric alarms created.
   */
  public Set<MetricAlarm> listAlarms() {
    Set<MetricAlarm> alarms = new HashSet<>();

    boolean done = false;

    while (!done) {
      DescribeAlarmsRequest request = new DescribeAlarmsRequest();

      DescribeAlarmsResult response = this.cloudWatchClient.describeAlarms(request);
      // add all the alarms to your set
      alarms.addAll(response.getMetricAlarms());

      request.setNextToken(response.getNextToken());

      if (response.getNextToken() == null) {
        done = true;
      }
    }

    return alarms;
  }

  /**
   * Delete an alarm.
   *
   * @param name: The name of the alarm.
   * @return boolean: The status of the action.
   */
  public boolean deleteAlarms(String name) {
    DeleteAlarmsRequest request = new DeleteAlarmsRequest().withAlarmNames(name);

    return this.cloudWatchClient.deleteAlarms(request) != null;
  }

  /**
   * @param alarm
   * @return
   */
  public boolean enableAlarmAction(String alarm) {
    EnableAlarmActionsRequest request = new EnableAlarmActionsRequest().withAlarmNames(alarm);

    return this.cloudWatchClient.enableAlarmActions(request) != null;
  }

  /**
   * @param alarm
   * @return
   */
  public boolean disableAlarmAction(String alarm) {
    DisableAlarmActionsRequest request = new DisableAlarmActionsRequest().withAlarmNames(alarm);

    return this.cloudWatchClient.disableAlarmActions(request) != null;
  }
}
