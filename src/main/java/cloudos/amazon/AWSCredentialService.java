package cloudos.amazon;
import cloudos.Providers;
import cloudos.credentials.AmazonCredentialManagement;
import cloudos.models.AwsCredential;
import cloudos.models.CloudosCredential;
import cloudos.models.CloudosCredentialRepository;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class keeping the AWSCredentials for the services
 *
 * @author flavio
 */
@Service
public class AWSCredentialService {

  private AWSStaticCredentialsProvider credentialProvider;

  @Autowired 
  private CloudosCredentialRepository cloudosCredentialRepository;
  
  @Autowired
  private AmazonCredentialManagement amazonCredentialManagement;
  
  private AwsCredential awsCredentials;

  /**
   * Return the credential.
   *
   * @return a new set of AWS credentials
   */
  private AwsCredential getAWSCredential() {
    if (awsCredentials == null) {
      List<CloudosCredential> findByProvider =
          cloudosCredentialRepository.findByProvider(Providers.AMAZON_AWS);
      if (CollectionUtils.isNotEmpty(findByProvider)) {
        awsCredentials = (AwsCredential) findByProvider.get(0);
        amazonCredentialManagement.decryptSensitiveData(awsCredentials);
      }
    }
    return awsCredentials;
  }

  /**
   * Get credentials for the current AWS provider.
   *
   * @return the AWS credential provider
   */
  public AWSStaticCredentialsProvider getAWSCredentials() {
    if (credentialProvider != null) {
      return credentialProvider;
    }
    AwsCredential awsCredentials = getAWSCredential();
    if (awsCredentials != null) {
      BasicAWSCredentials credentials =
          new BasicAWSCredentials(
              awsCredentials.getAccessKeyId(), awsCredentials.getSecretAccessKey());
      credentialProvider = new AWSStaticCredentialsProvider(credentials);
    }
    return credentialProvider;
  }

  /**
   * Update the credentials.
   *
   * @param savedCredential the current saved credentials
   */
  public void changeCredentials(AwsCredential savedCredential) {
    this.awsCredentials = null;
    this.credentialProvider = null;
    // force reload
    getAWSCredentials();
  }
}
