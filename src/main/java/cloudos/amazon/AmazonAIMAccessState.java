package cloudos.amazon;

/** Created by philipperibeiro on 5/9/17. */
public enum AmazonAIMAccessState {
  ACTIVE_ACCESS_KEY("Activate"),
  DEACTIVATE_ACCESS_KEY("Inactive");

  private String accessStatus;

  AmazonAIMAccessState(String accessStatus) {
    this.accessStatus = accessStatus;
  }

  @Override
  public String toString() {
    return this.accessStatus;
  }
}
