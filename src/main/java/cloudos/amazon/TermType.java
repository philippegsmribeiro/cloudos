package cloudos.amazon;

/** @author Philippe Ribeiro */
public enum TermType {

  // On-Demand instances
  ON_DEMAND("OnDemand"),
  // Reserved instances
  RESERVED("Reserved"),
  // Dedicated instances
  DEDICATED("Dedicated"),
  // Spot instances
  SPOT("SPOT");


  private final String term;

  /** @param term The Aws Term type; */
  private TermType(final String term) {
    this.term = term;
  }

  /** Return the Default string definition for the TermType; */
  @Override
  public String toString() {
    return this.term;
  }

  /**
   * Method that return the TermType by term.
   * 
   * @param term term of Aws Term type.
   * @return TermType associated with the term.
   */
  public static final TermType getTermTypeByTerm(String term) {
    for (TermType termType : TermType.values()) {
      if (termType.toString().equals(term))
        return termType;
    }
    return null;
  }
}
