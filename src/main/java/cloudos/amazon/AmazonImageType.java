package cloudos.amazon;

import cloudos.Providers;
import cloudos.provider.ProviderImageType;

public class AmazonImageType extends ProviderImageType {

  public AmazonImageType() {
    setProvider(Providers.AMAZON_AWS);
  }
}
