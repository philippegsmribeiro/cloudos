package cloudos.amazon;

import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by philipperibeiro on 12/23/16. Handle all the Amazon S3 related operations as a service.
 */
@Service
@Log4j2
public class AmazonS3Service {

  @Autowired AWSCredentialService awsCredentialService;

  private ConcurrentHashMap<String, S3> map;

  /** Default constructor for the AmazonS3Service class. */
  public AmazonS3Service() {
    this.map = new ConcurrentHashMap<>();
  }

  /**
   * Delete a folder inside a S3 bucket.
   *
   * @param bucketname The name of the bucket
   * @param region The region the bucket is located.
   * @param foldername The name of the folder to be deleted.
   * @return boolean: true if successful, false otherwise.
   */
  public synchronized boolean deleteFolder(String bucketname, String region, String foldername) {
    try {
      S3 s3 = this.getClient(region);
      s3.deleteFolder(bucketname, foldername);
      return true;
    } catch (Exception ex) {
      log.info(ex.getMessage());
    }
    return false;
  }

  /**
   * Make a S3 object public.
   *
   * @param bucketname The name of the bucket
   * @param region The region the bucket is located.
   * @param folderpath The name of the folder to be deleted.
   * @return boolean: true if successful, false otherwise.
   */
  public synchronized boolean uploadFolder(String bucketname, String region, String folderpath) {
    try {
      S3 s3 = this.getClient(region);
      s3.uploadFolder(bucketname, folderpath);
      return true;
    } catch (Exception ex) {
      log.info(ex.getMessage());
    }
    return false;
  }

  /**
   * Make a S3 object public.
   *
   * @param bucketname The name of the bucket
   * @param region The region the bucket is located.
   * @param filename The name of the folder to be deleted.
   * @return boolean: true if successful, false otherwise.
   */
  public synchronized boolean makeObjectPublic(String bucketname, String region, String filename) {
    try {
      S3 s3 = this.getClient(region);
      s3.makeObjectPublic(bucketname, filename);
      return true;
    } catch (Exception ex) {
      log.info(ex.getMessage());
    }
    return false;
  }

  /**
   * Make a S3 object private.
   *
   * @param bucketname The name of the bucket
   * @param region The region the bucket is located.
   * @param filename The name of the folder to be deleted.
   * @return boolean: true if successful, false otherwise.
   */
  public synchronized boolean makeObjectPrivate(String bucketname, String region, String filename) {
    try {
      S3 s3 = this.getClient(region);
      s3.makeObjectPrivate(bucketname, filename);
      return true;
    } catch (Exception ex) {
      log.info(ex.getMessage());
    }
    return false;
  }

  /**
   * Download a folder.
   * @param bucketname The name of the bucket
   * @param region The region the bucket is located.
   * @param folderpath The name of the folder to be deleted.
   * @param folderPrefix The folder prefix in the S3 bucket.
   * @return boolean: true if successful, false otherwise.
   */
  public synchronized boolean downloadFolder(
      String bucketname, String region, String folderPrefix, String folderpath) {
    try {
      S3 s3 = this.getClient(region);
      s3.downloadFolder(bucketname, folderPrefix, folderpath);
      return true;
    } catch (Exception ex) {
      log.info(ex.getMessage());
    }
    return false;
  }

  /**
   * Create a folder inside a S3 bucket.
   *
   * @param bucketname The name of the bucket
   * @param region The region the bucket is located.
   * @param foldername The name of the folder to be deleted.
   * @return boolean: true if successful, false otherwise.
   */
  public synchronized String createFolder(String bucketname, String region, String foldername) {
    try {
      S3 s3 = this.getClient(region);
      return s3.createFolder(bucketname, foldername);
    } catch (Exception ex) {
      log.info(ex.getMessage());
      return null;
    }
  }

  /**
   * Create a new S3 bucket in the given region.
   *
   * @param bucketname The name of the bucket
   * @param region The region the bucket is located.
   * @return Bucket: The created object or null
   */
  public synchronized Bucket createBucket(String bucketname, String region) {
    try {
      S3 s3 = this.getClient(region);
      return s3.createBucket(bucketname);
    } catch (Exception ex) {
      log.info(ex.getMessage());
      return null;
    }
  }

  /**
   * Get all the keys stored in the bucket.
   *
   * @param bucketname The name of the bucket
   * @param region The region the bucket is located.
   * @return Set: A set of object summary
   */
  public Set<S3ObjectSummary> listBucketContent(String bucketname, String region) {
    try {
      S3 s3 = this.getClient(region);
      return s3.listBucketContent(bucketname);
    } catch (Exception ex) {
      log.info(ex.getMessage());
      return null;
    }
  }

  /**
   * Get a lit of of all buckets in a given region.
   *
   * @param region The region the bucket is located.
   * @return List: A list of buckets in the given region
   */
  public List<Bucket> listBucket(String region) {
    List<Bucket> buckets = new ArrayList<>();
    try {
      S3 s3 = this.getClient(region);
      buckets = s3.listBuckets();
    } catch (Exception ex) {
      log.error(ex.getMessage());
    }
    return buckets;
  }

  /**
   * Delete a bucket, including all of its contents.
   *
   * @param bucketname The name of the bucket
   * @param region The region the bucket is located.
   * @return boolean: true if successfully uploaded, false otherwise.
   */
  public synchronized boolean deleteBucket(String bucketname, String region) {
    try {
      S3 s3 = this.getClient(region);
      s3.deleteBucket(bucketname);
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return false;
    }
    return true;
  }

  /**
   * Upload a file to a given bucket in S3.
   *
   * @param bucketname The name of the bucket
   * @param region The region the bucket is located.
   * @param key The name of the file in S3.
   * @param filepath The local path of the file
   * @return boolean: true if successfully uploaded, false otherwise.
   */
  public synchronized boolean uploadFile(
      String bucketname, String region, String key, String filepath) {
    try {
      S3 s3 = this.getClient(region);
      s3.uploadFile(bucketname, key, filepath);
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return false;
    }
    return true;
  }

  /**
   * Check if a bucket exists.
   *
   * @param bucketname The name of the bucket.
   * @param region The region the bucket is located.
   * @return boolean: True if the bucket exists, false otherwise.
   */
  public boolean bucketExists(String bucketname, String region) {
    S3 s3 = this.getClient(region);
    return s3.bucketExists(bucketname);
  }

  /**
   * Download a single file stored in the specified S3 bucket.
   *
   * @param bucketname The name of the bucket.
   * @param region The region the bucket is located.
   * @param key The key file name in bucket.
   * @param filepath The filepath where the file will be downloaded to.
   * @param waitForCompletion if wait the thread for completion
   * @param logProgress if log the progress
   * @return boolean: true if successful, false otherwise.
   */
  public synchronized boolean downloadFile(String bucketname, String region, String key,
      String filepath, boolean waitForCompletion, boolean logProgress) {
    try {
      S3 s3 = this.getClient(region);
      s3.downloadFile(bucketname, key, filepath, waitForCompletion, logProgress);
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return false;
    }
    return true;
  }

  /**
   * Get a S3 client if it exists, otherwise create a new S3 object.
   *
   * @param region The AWS region the object belongs to.
   * @return S3: A S3 object.
   */
  private synchronized S3 getClient(String region) {
    if (this.map.containsKey(region)) {
      return this.map.get(region);
    }
    S3 s3 = new S3(region, awsCredentialService.getAWSCredentials());
    this.map.put(region, s3);
    return s3;
  }

  public void resetMaps() {
    map.clear();
  }
}
