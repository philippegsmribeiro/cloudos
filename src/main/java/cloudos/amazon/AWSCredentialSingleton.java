package cloudos.amazon;
/**
 * @Copyright CyberTextron Inc. 2016
 *
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
import cloudos.models.AwsCredential;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import org.springframework.beans.factory.annotation.Value;

public class AWSCredentialSingleton {

  private static AWSCredentials credentials;

  private static AWSCredentialsProvider provider;

  @Value("${aws_access_key_id}")
  public static String accessKeyId;

  @Value("${aws_secret_key}")
  public static String secretAccessKey;

  protected AWSCredentialSingleton() {
    /* Exists only to defeat instantiation */

  }

  /** @return */
  public static AWSCredentials getCredentials() {
    if (credentials == null) {
      if (accessKeyId == null || secretAccessKey == null) {
        init();
      }
      credentials = new BasicAWSCredentials(accessKeyId, secretAccessKey);
    }
    return credentials;
  }

  private static void init() {
    accessKeyId = accessKeyId != null ? accessKeyId : "AKIAIUBBD5QOWZIVCYBA";
    secretAccessKey =
        secretAccessKey != null ? secretAccessKey : "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";
  }

  /** @return */
  public static AWSCredentialsProvider getProvider() {
    if (provider == null) {
      if (accessKeyId == null || secretAccessKey == null) {
        init();
      }
      credentials = new BasicAWSCredentials(accessKeyId, secretAccessKey);
      provider = new AWSStaticCredentialsProvider(credentials);
    }
    return provider;
  }

  public static void changeCredentials(AwsCredential credential) {
    accessKeyId = credential.getAccessKeyId();
    secretAccessKey = credential.getSecretAccessKey();
    provider = null;
  }
}
