package cloudos.amazon;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.inspector.AmazonInspector;
import com.amazonaws.services.inspector.AmazonInspectorClientBuilder;
import com.amazonaws.services.inspector.model.CreateResourceGroupRequest;
import com.amazonaws.services.inspector.model.CreateResourceGroupResult;
import com.amazonaws.services.inspector.model.DescribeResourceGroupsRequest;
import com.amazonaws.services.inspector.model.DescribeResourceGroupsResult;
import com.amazonaws.services.inspector.model.ResourceGroup;
import com.amazonaws.services.inspector.model.ResourceGroupTag;

import java.util.Collection;
import java.util.List;

/**
 * Created by philipperibeiro on 5/20/17.
 *
 * <p>Allow the user to create Resource Groups in AWS
 */
public class AwsResourceGroup {

  private AmazonInspector client;

  /**
   * Constructor for the AwsResourceGroup
   *
   * @param region: The specific region.
   */
  public AwsResourceGroup(String region, AWSCredentialsProvider provider) {
    this.client =
        AmazonInspectorClientBuilder.standard()
            .withCredentials(provider)
            .withRegion(region)
            .build();
  }

  /**
   * Create a new Resource Group
   *
   * @param resourceGroupTags: A collection of Resource Group Tags
   * @return String: The Arn of the resource group created.
   */
  public String createResourceGroup(Collection<ResourceGroupTag> resourceGroupTags) {
    CreateResourceGroupRequest resourceGroupRequest =
        new CreateResourceGroupRequest().withResourceGroupTags(resourceGroupTags);
    CreateResourceGroupResult resourceGroupResult =
        this.client.createResourceGroup(resourceGroupRequest);
    return resourceGroupResult.getResourceGroupArn();
  }

  /**
   * Get a list of of resource groups that match the arn
   *
   * @param resourceGroupArns: A number of arns parameters
   * @return List: A list of resource groups.
   */
  public List<ResourceGroup> getResourceGroups(Collection<String> resourceGroupArns) {
    DescribeResourceGroupsRequest resourceGroupsRequest =
        new DescribeResourceGroupsRequest().withResourceGroupArns(resourceGroupArns);
    DescribeResourceGroupsResult resourceGroupsResult =
        this.client.describeResourceGroups(resourceGroupsRequest);
    return resourceGroupsResult.getResourceGroups();
  }
}
