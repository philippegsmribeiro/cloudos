package cloudos.amazon;

import com.amazonaws.services.redshift.AmazonRedshiftClient;
import com.amazonaws.services.redshift.model.AuthorizeClusterSecurityGroupIngressRequest;
import com.amazonaws.services.redshift.model.Cluster;
import com.amazonaws.services.redshift.model.ClusterSecurityGroup;
import com.amazonaws.services.redshift.model.ClusterSecurityGroupMembership;
import com.amazonaws.services.redshift.model.CreateClusterSecurityGroupRequest;
import com.amazonaws.services.redshift.model.DescribeClusterSecurityGroupsRequest;
import com.amazonaws.services.redshift.model.DescribeClusterSecurityGroupsResult;
import com.amazonaws.services.redshift.model.DescribeClustersRequest;
import com.amazonaws.services.redshift.model.DescribeClustersResult;
import com.amazonaws.services.redshift.model.EC2SecurityGroup;
import com.amazonaws.services.redshift.model.IPRange;
import com.amazonaws.services.redshift.model.ModifyClusterRequest;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.log4j.Log4j2;

/**
 * @Copyright CyberTextron Inc. 2016
 *
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
/** @author philipperibeiro */
@Log4j2
public class CreateAndModifyClusterSecurityGroup {

  private AmazonRedshiftClient client;
  private String clusterSecurityGroupName;
  private String clusterIdentifier;
  private String ownerID;

  /**
   * @param clusterSecurityGroupName
   * @param clusterIdentifier
   * @param ownerID
   */
  public CreateAndModifyClusterSecurityGroup(String clusterSecurityGroupName,
      String clusterIdentifier, String ownerID) {

    this.client = new AmazonRedshiftClient(AWSCredentialSingleton.getCredentials());
    try {
      this.clusterSecurityGroupName = clusterSecurityGroupName;
      this.clusterIdentifier = clusterIdentifier;
      this.ownerID = ownerID;
      this.createClusterSecurityGroup();
      this.describeClusterSecurityGroups();
      this.addIngressRules();
      this.associateSecurityGroupWithCluster();
    } catch (Exception e) {
      log.error("Operation failed: " + e.getMessage());
    }
  }

  /** */
  public void associateSecurityGroupWithCluster() {
    // Get existing security groups used by the cluster.
    DescribeClustersRequest request =
        new DescribeClustersRequest().withClusterIdentifier(this.clusterIdentifier);

    DescribeClustersResult result = client.describeClusters(request);
    List<ClusterSecurityGroupMembership> membershipList =
        result.getClusters().get(0).getClusterSecurityGroups();

    List<String> secGroupNames = new ArrayList<>();
    for (ClusterSecurityGroupMembership mem : membershipList) {
      secGroupNames.add(mem.getClusterSecurityGroupName());
    }
    // Add new security group to the list.
    secGroupNames.add(clusterSecurityGroupName);

    // Apply the change to the cluster.
    ModifyClusterRequest request2 = new ModifyClusterRequest()
        .withClusterIdentifier(this.clusterIdentifier).withClusterSecurityGroups(secGroupNames);

    Cluster result2 = client.modifyCluster(request2);
    log.info(result2);
    log.info(String.format("\nAssociated security group '%s' to cluster '%s'.",
        clusterSecurityGroupName, clusterIdentifier));
  }

  /** */
  public void addIngressRules() {
    // TODO Auto-generated method stub
    AuthorizeClusterSecurityGroupIngressRequest request =
        new AuthorizeClusterSecurityGroupIngressRequest()
            .withClusterSecurityGroupName(this.clusterSecurityGroupName)
            .withCIDRIP("192.168.40.5/32");

    ClusterSecurityGroup result = this.client.authorizeClusterSecurityGroupIngress(request);

    request = new AuthorizeClusterSecurityGroupIngressRequest()
        .withClusterSecurityGroupName(this.clusterSecurityGroupName)
        .withEC2SecurityGroupName("default").withEC2SecurityGroupOwnerId(this.ownerID);
    result = this.client.authorizeClusterSecurityGroupIngress(request);
    log.info(
        String.format("\nAdded ingress rules to security group '%s'\n", clusterSecurityGroupName));
    this.printResultSecurityGroup(result);
  }

  /** */
  public void describeClusterSecurityGroups() {
    // TODO Auto-generated method stub
    DescribeClusterSecurityGroupsRequest request = new DescribeClusterSecurityGroupsRequest();

    DescribeClusterSecurityGroupsResult result = client.describeClusterSecurityGroups(request);
    this.printResultSecurityGroup(result.getClusterSecurityGroups());
  }

  /** */
  public void createClusterSecurityGroup() {
    // TODO Auto-generated method stub
    CreateClusterSecurityGroupRequest request =
        new CreateClusterSecurityGroupRequest().withDescription("my cluster security group")
            .withClusterSecurityGroupName(this.clusterSecurityGroupName);

    client.createClusterSecurityGroup(request);
    log.info(
        String.format("Created cluster security group: '%s'\n", this.clusterSecurityGroupName));
  }

  /** @param group */
  public void printResultSecurityGroup(ClusterSecurityGroup group) {
    log.info(String.format("\nName: '%s', Description: '%s'\n", group.getClusterSecurityGroupName(),
        group.getDescription()));
    for (EC2SecurityGroup g : group.getEC2SecurityGroups()) {
      String.format("EC2group: '%s', '%s', '%s'\n", g.getEC2SecurityGroupName(),
          g.getEC2SecurityGroupOwnerId(), g.getStatus());
    }
    for (IPRange range : group.getIPRanges()) {
      String.format("IPRanges: '%s', '%s'\n", range.getCIDRIP(), range.getStatus());
    }
  }

  /** @param groups */
  public void printResultSecurityGroup(List<ClusterSecurityGroup> groups) {
    if (groups == null) {
      log.info("\nDescribe cluster security groups result is null.");
      return;
    }

    log.info("\nPrinting security group results:");
    for (ClusterSecurityGroup group : groups) {
      printResultSecurityGroup(group);
    }
  }
}
