package cloudos.amazon;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.logs.AWSLogsClient;
import com.amazonaws.services.logs.AWSLogsClientBuilder;
import com.amazonaws.services.logs.model.CreateLogGroupRequest;
import com.amazonaws.services.logs.model.CreateLogGroupResult;
import com.amazonaws.services.logs.model.CreateLogStreamRequest;
import com.amazonaws.services.logs.model.CreateLogStreamResult;
import com.amazonaws.services.logs.model.DescribeLogStreamsRequest;
import com.amazonaws.services.logs.model.InputLogEvent;
import com.amazonaws.services.logs.model.LogStream;
import com.amazonaws.services.logs.model.PutLogEventsRequest;
import com.amazonaws.services.logs.model.PutLogEventsResult;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import lombok.extern.log4j.Log4j2;

/** Created by philipperibeiro on 2/18/17. */
@Log4j2
public class CloudWatchLogs {

  private AWSLogsClient awsLogsClient;
  private Calendar calendar;

  public CloudWatchLogs(String region, AWSCredentialsProvider provider) {
    this.awsLogsClient =
        (AWSLogsClient)
            AWSLogsClientBuilder.standard().withCredentials(provider).withRegion(region).build();
    this.calendar = Calendar.getInstance();
  }

  public void createLogGroup(String logGroupName) {
    CreateLogGroupRequest createLogGroupRequest = new CreateLogGroupRequest();
    createLogGroupRequest.withLogGroupName(logGroupName);

    CreateLogGroupResult createLogGroupResult =
        this.awsLogsClient.createLogGroup(createLogGroupRequest);
    log.info(createLogGroupResult.getSdkResponseMetadata());
  }

  public void createLogStream(String logGroupName, String logStreamName) {
    CreateLogStreamRequest createLogStreamRequest = new CreateLogStreamRequest();
    createLogStreamRequest.setLogGroupName(logGroupName);
    createLogStreamRequest.setLogStreamName(logStreamName);

    CreateLogStreamResult createLogStreamResult =
        this.awsLogsClient.createLogStream(createLogStreamRequest);
    log.info(createLogStreamResult.getSdkResponseMetadata());
  }

  public void putLogEvent(String logGroupName, String logStreamName) {
    PutLogEventsRequest putLogEventsRequest = new PutLogEventsRequest();
    putLogEventsRequest.setLogGroupName(logGroupName);
    putLogEventsRequest.setLogStreamName(logStreamName);

    String token = null;
    DescribeLogStreamsRequest logStreamsRequest =
        new DescribeLogStreamsRequest().withLogGroupName(logGroupName);
    List<LogStream> logStreamList =
        this.awsLogsClient.describeLogStreams(logStreamsRequest).getLogStreams();

    for (LogStream logStream : logStreamList) {
      token = logStream.getUploadSequenceToken();
    }
    if (token != null) {
      putLogEventsRequest.setSequenceToken(token);
    }
    InputLogEvent testEvent = new InputLogEvent();
    testEvent.setMessage("let's test this log!");
    testEvent.setTimestamp(calendar.getTimeInMillis());

    ArrayList<InputLogEvent> logEvents = new ArrayList<>();
    logEvents.add(testEvent);
    putLogEventsRequest.setLogEvents(logEvents);

    // this will return the next token if you want to hold onto it
    PutLogEventsResult putLogEventsResult = this.awsLogsClient.putLogEvents(putLogEventsRequest);
    log.info(putLogEventsResult.getSdkResponseMetadata());
  }
}
