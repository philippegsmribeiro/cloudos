package cloudos.amazon;

public enum AmazonInstanceDistros {
  RED_HAT("rhel", "ec2-user"),
  AMAZON("amazon", "ec2-user"),
  SUSE("suse", "ec2-user"),
  FEDORA("fedora", "ec2-user"),
  CENTOS("centos", "centos"),
  UBUNTU("ubuntu", "ubuntu"), 
  CANONICAL("canonical", "ubuntu");

  private String user;
  private String type;

  private AmazonInstanceDistros(String type, String user) {
    this.type = type;
    this.user = user;
  }

  public String getType() {
    return type;
  }

  public String getUser() {
    return user;
  }

  /**
   * Return by the type.
   *
   * @param type from description
   * @return Distro info
   */
  public static AmazonInstanceDistros findByType(String type) {
    for (AmazonInstanceDistros distro : AmazonInstanceDistros.values()) {
      System.out.println(type + "" + distro.getType());
      if (type.toLowerCase().trim().contains(distro.getType().toLowerCase().trim())) {
        return distro;
      }
    }
    return null;
  }
}
