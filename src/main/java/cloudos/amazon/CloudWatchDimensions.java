package cloudos.amazon;

/**
 * Created by philipperibeiro on 3/29/17.
 *
 * <p>Define all the dimensions used by AWS CloudWatch. @Copyright 2017 Cybernetic Cloud Inc.
 *
 * @license: Proprietary License.
 * @author Philippe Ribeiro
 * @date: 1/6/2017
 */
public enum CloudWatchDimensions {
  AUTO_SCALING_GROUP_NAME("AutoScalingGroupName"),
  IMAGE_ID("ImageId"),
  INSTANCE_ID("InstanceId"),
  INSTANCE_TYPE("InstanceType"),
  ESTIMATED_CHARGES("EstimatedCharges");

  private String dimension;

  /** @param dimension */
  CloudWatchDimensions(String dimension) {
    this.dimension = dimension;
  }

  /**
   * Find the matching value for the CloudWatch dimension
   *
   * @param value: The matching string value
   * @return: CloudWatchDimensions that matches thevalue
   */
  public static final CloudWatchDimensions getDimensionByValue(String value) {
    for (CloudWatchDimensions dimension : CloudWatchDimensions.values()) {
      if (dimension.getDimension().equals(value)) return dimension;
    }

    return null;
  }

  /** @return */
  public String getDimension() {
    return this.dimension;
  }

  @Override
  public String toString() {
    return this.dimension;
  }
}
