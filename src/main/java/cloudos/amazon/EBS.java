package cloudos.amazon;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.AttachVolumeRequest;
import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.CreateVolumeRequest;
import com.amazonaws.services.ec2.model.CreateVolumeResult;
import com.amazonaws.services.ec2.model.Tag;

import java.util.ArrayList;

import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;

/**
 * @Copyright CyberTextron Inc. 2016
 *
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
@Log4j2
@ToString
@NoArgsConstructor
public class EBS {

  /* Make the AmazonEC2 client static, so it can be used by other objects */
  private static AmazonEC2 client;

  /** @param client */
  public EBS(AmazonEC2 client) {
    EBS.client = client;
  }

  /**
   * @param instanceId
   * @param size
   * @param region
   * @param device
   * @param name
   * @param value
   * @return
   */
  public String createVolume(
      String instanceId, Integer size, String region, String device, String name, String value) {
    try {
      log.info("Volume creating begins...");
      CreateVolumeRequest request = new CreateVolumeRequest(size, region);
      CreateVolumeResult result = client.createVolume(request);

      /* Create the list of tags we want to create */
      log.info("Settings the tags to the volume...");
      ArrayList<Tag> instanceTags = new ArrayList<>();
      instanceTags.add(new Tag(name, value));
      /* Create a new tag request */
      CreateTagsRequest tagsRequest =
          new CreateTagsRequest()
              .withTags(instanceTags)
              .withResources(result.getVolume().getVolumeId());
      client.createTags(tagsRequest);

      String volumeId = result.getVolume().getVolumeId();
      this.attachVolume(volumeId, instanceId, device);
      log.info("Creating the volume ends...");
      return volumeId;
    } catch (AmazonServiceException ase) {
      log.error(ase.getMessage());
      return null;
    }
  }

  /**
   * Attaches a volume to an EC2 instance. The volume must has been created before being used, and
   * not attached to other EC2 instance.
   *
   * @param device
   * @param instanceId
   * @param volumeId
   */
  public void attachVolume(String volumeId, String instanceId, String device) {
    try {
      AttachVolumeRequest attachRequest = new AttachVolumeRequest(volumeId, instanceId, device);
      client.attachVolume(attachRequest);
      log.info("Volume " + volumeId + " correctly attached.");
    } catch (AmazonServiceException ase) {
      log.error(ase.getMessage());
    }
  }
}
