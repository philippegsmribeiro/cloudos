package cloudos.amazon.models;

import cloudos.amazon.AmazonInstance;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The AmazonSpotInstance extends the AmazonInstance type in order to handle spot instances.
 * <p>
 * The spot instances have the spot instance request id, which can be used to track and end spot
 * request that created the instance.
 * </p>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AmazonSpotInstance extends AmazonInstance {

  private String spotInstanceRequestId;

  public AmazonSpotInstance(AmazonInstance amazonInstance, String spotInstanceRequestId) {
    super(amazonInstance);
    this.setSpotInstanceRequestId(spotInstanceRequestId);
  }
}
