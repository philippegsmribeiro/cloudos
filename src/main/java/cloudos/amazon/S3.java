package cloudos.amazon;

import cloudos.security.LogFilter;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.CompleteMultipartUploadRequest;
import com.amazonaws.services.s3.model.CompleteMultipartUploadResult;
import com.amazonaws.services.s3.model.CopyPartRequest;
import com.amazonaws.services.s3.model.CopyPartResult;
import com.amazonaws.services.s3.model.CreateBucketRequest;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.DeleteObjectsResult;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.GetObjectMetadataRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadResult;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.ListVersionsRequest;
import com.amazonaws.services.s3.model.MultiObjectDeleteException;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PartETag;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.RestoreObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.model.S3VersionSummary;
import com.amazonaws.services.s3.model.VersionListing;
import com.amazonaws.services.s3.transfer.Download;
import com.amazonaws.services.s3.transfer.MultipleFileDownload;
import com.amazonaws.services.s3.transfer.MultipleFileUpload;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.amazonaws.services.s3.transfer.Upload;
/**
 * @Copyright CyberTextron Inc. 2016
 *
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.ThreadContext;

@Log4j2
public class S3 {

  private AmazonS3 client;
  private final String SUFFIX;

  /**
   * Create a S3 object specifying which region should the object belongs to.
   *
   * @param region The name of the region
   */
  public S3(String region, AWSCredentialsProvider provider) {
    ClientConfiguration clientConfig = new ClientConfiguration();
    clientConfig.setProtocol(Protocol.HTTP);
    this.client =
        AmazonS3ClientBuilder.standard().withCredentials(provider).withRegion(region).build();
    this.SUFFIX = "/";
  }

  /**
   * Create a new bucket in S3, given the bucket name.
   *
   * @param bucketname the name of the bucket
   */
  public Bucket createBucket(String bucketname) {
    try {
      if (bucketExists(bucketname)) {
        log.warn("Bucket {} in the default already exists... nothing to do", bucketname);
        return null;
      }
      log.info("Creating bucket: {}", bucketname);
      return this.client.createBucket(bucketname);
    } catch (AmazonServiceException ase) {
      log.error(
          "Caught an AmazonServiceException, which means your request made it "
              + "to Amazon S3, but was rejected with an error response for some reason.");
      log.error("Error Message:    " + ase.getMessage());
      log.error("HTTP Status Code: " + ase.getStatusCode());
      log.error("AWS Error Code:   " + ase.getErrorCode());
      log.error("Error Type:       " + ase.getErrorType());
      log.error("Request ID:       " + ase.getRequestId());
    } catch (AmazonClientException ace) {
      log.error(
          "Caught an AmazonClientException, which means the client encountered "
              + "a serious internal problem while trying to communicate with S3, "
              + "such as not being able to access the network.");
      log.error("Error Message: {}", ace.getMessage());
    }
    return null;
  }

  /**
   * Create a new bucket in S3, given the bucket name and its region.
   *
   * @param bucketname the name of the bucket
   * @param region The region we want to create the bucket on.
   */
  public Bucket createBucket(String bucketname, String region) {
    // bucket already exists ... nothing to do
    if (bucketExists(bucketname)) {
      log.warn("Bucket {} in region {} already exists... nothing to do", bucketname, region);
      return null;
    }
    try {
      log.info("Creating bucket '{}' in region '{}'", bucketname, region);
      return this.client.createBucket(new CreateBucketRequest(bucketname, region));
    } catch (AmazonServiceException ase) {
      log.error(
          "Caught an AmazonServiceException, which means your request made it "
              + "to Amazon S3, but was rejected with an error response for some reason.");
      log.error("Error Message:    {}", ase.getMessage());
      log.error("HTTP Status Code: {}", ase.getStatusCode());
      log.error("AWS Error Code:   {}", ase.getErrorCode());
      log.error("Error Type:       {}", ase.getErrorType());
      log.error("Request ID:       {}", ase.getRequestId());
    } catch (AmazonClientException ace) {
      log.error(
          "Caught an AmazonClientException, which means the client encountered "
              + "a serious internal problem while trying to communicate with S3, "
              + "such as not being able to access the network.");
      log.error("Error Message: {}", ace.getMessage());
    }
    return null;
  }

  /**
   * Return a list of all the buckets.
   *
   * @return List: A list of all the buckets.
   */
  public List<Bucket> listBuckets() {
    List<Bucket> buckets = new ArrayList<>();
    try {
      buckets = this.client.listBuckets();
    } catch (AmazonClientException ace) {
      log.error(ace.getMessage());
    }
    return buckets;
  }

  /**
   * Make an existing object in S3 public.
   *
   * @param bucketname The name of the bucket
   * @param filename The path of the file.
   */
  public void makeObjectPublic(String bucketname, String filename) {
    try {
      this.client.setObjectAcl(bucketname, filename, CannedAccessControlList.PublicRead);
    } catch (AmazonClientException ace) {
      log.error(ace.getMessage());
    }
  }

  /**
   * Get a S3 object from the bucket, based on its file name.
   *
   * @param bucketname the name of the bucket
   * @param filename the name of the file
   * @return a S3Object
   */
  public S3Object getObjectSummary(String bucketname, String filename) {
    try {
      GetObjectRequest request = new GetObjectRequest(bucketname, filename.replace("\\", "/"));
      return this.client.getObject(request);
    } catch (AmazonClientException ace) {
      log.error(ace.getMessage());
    }
    return null;
  }

  /**
   * Get the MD5 hash value of the file in the given bucket.
   *
   * @param bucketname the name of the bucket
   * @param filename the name of the file
   * @return the MD5 value of the object
   */
  public String getObjectMd5(String bucketname, String filename) {
    S3Object s3Object = this.getObjectSummary(bucketname, filename);
    log.info("S3Object: {}", s3Object);
    if (s3Object != null) {
      // return the MD5
      return s3Object.getObjectMetadata().getETag();
    }

    return null;
  }

  /**
   * Make an existing object in S3 private.
   *
   * @param bucketname The name of the bucket
   * @param filename The path of the file.
   */
  public void makeObjectPrivate(String bucketname, String filename) {
    try {
      this.client.setObjectAcl(bucketname, filename, CannedAccessControlList.Private);
    } catch (AmazonClientException ace) {
      log.error(ace.getMessage());
    }
  }

  /**
   * Download a file in S3 to the local path.
   *
   * @param bucketname The name of the bucket
   * @param key the file key
   * @param filepath The path to download the file to
   * @param waitForCompletion if wait the thread for completion
   * @param logProgress if log the progress
   * @throws Exception if something goes wrong
   */
  public void downloadFile(String bucketname, String key, String filepath,
      boolean waitForCompletion, boolean logProgress) throws Exception {
    TransferManager tm = TransferManagerBuilder.standard().withS3Client(this.client).build();

    DecimalFormat df = new DecimalFormat("#.00");
    try {
      GetObjectRequest request = new GetObjectRequest(bucketname, key.replace("\\", "/"));
      // Download the Amazon S3 object to a file.
      Download download = tm.download(request, new File(filepath));
      String fileS3name = bucketname + key;

      String threadName = ThreadContext.get(LogFilter.LOG_PATH);

      download.addProgressListener((ProgressListener) progressEvent -> {

        ThreadContext.put(LogFilter.LOG_PATH, threadName);

        double percentTransferred = download.getProgress().getPercentTransferred();
        if (logProgress) {
          log.info("### {} % {} ###", df.format(percentTransferred), fileS3name);
        }

        if (percentTransferred == 100) {
          double size = download.getProgress().getBytesTransferred();
          log.info("### Download complete!!! {} MB {} ###", df.format((size / 1024) / 1024),
              fileS3name);
        }

        ThreadContext.remove(LogFilter.LOG_PATH);
      });
      if (waitForCompletion) {
        // Blocking call to wait until the download finishes.
        download.waitForCompletion();
      }
      // If transfer manager will not be used anymore, shut it down.
    } catch (AmazonClientException | InterruptedException e) {
      log.error(e.getMessage());
      throw e;
    } finally {
      tm.shutdownNow(false);
    }
  }

  /**
   * @param sourceBucketName
   * @param sourceObjectKey
   * @param targetBucketName
   * @param targetObjectKey
   */
  public void copyObjects(
      String sourceBucketName,
      String sourceObjectKey,
      String targetBucketName,
      String targetObjectKey) {
    // List to store copy part responses.

    List<CopyPartResult> copyResponses = new ArrayList<>();

    InitiateMultipartUploadRequest initiateRequest =
        new InitiateMultipartUploadRequest(targetBucketName, targetObjectKey);

    InitiateMultipartUploadResult initResult = this.client.initiateMultipartUpload(initiateRequest);

    try {
      // Get object size.
      GetObjectMetadataRequest metadataRequest =
          new GetObjectMetadataRequest(sourceBucketName, sourceObjectKey);

      ObjectMetadata metadataResult = this.client.getObjectMetadata(metadataRequest);
      long objectSize = metadataResult.getContentLength(); // in bytes

      // Copy parts.
      long partSize = 5 * (long) Math.pow(2.0, 20.0); // 5 MB

      long bytePosition = 0;
      for (int i = 1; bytePosition < objectSize; i++) {
        CopyPartRequest copyRequest =
            new CopyPartRequest()
                .withDestinationBucketName(targetBucketName)
                .withDestinationKey(targetObjectKey)
                .withSourceBucketName(sourceBucketName)
                .withSourceKey(sourceObjectKey)
                .withUploadId(initResult.getUploadId())
                .withFirstByte(bytePosition)
                .withLastByte(
                    bytePosition + partSize - 1 >= objectSize
                        ? objectSize - 1
                        : bytePosition + partSize - 1)
                .withPartNumber(i);

        copyResponses.add(this.client.copyPart(copyRequest));
        bytePosition += partSize;
      }
      CompleteMultipartUploadRequest completeRequest =
          new CompleteMultipartUploadRequest(
              targetBucketName, targetObjectKey, initResult.getUploadId(), GetETags(copyResponses));

      CompleteMultipartUploadResult completeUploadResponse =
          this.client.completeMultipartUpload(completeRequest);
    } catch (Exception e) {
      log.error(e.getMessage());
    }
  }

  /**
   * Check if the bucket exists before attempting to delete / create it.
   *
   * @param bucketname The name of the bucket.
   * @return boolean true if successful, false otherwise.
   */
  public boolean bucketExists(String bucketname) {
    try {
      /*
       *
       * Notice that we supply the bucket name in the request and specify that we want 0 keys
       * returned since we don't actually care about the data.
       */
      List<Bucket> buckets = this.client.listBuckets();
      for (Bucket bucket : buckets) {
        if (bucket.getName().equals(bucketname)) {
          return true;
        }
      }
    } catch (AmazonServiceException ase) {
      log.error("AccessDenied: {}", ase.getErrorCode());
      log.error(ase.getErrorMessage());
    }
    return false;
  }

  /**
   * Upload a file of any size to the S3 bucket.
   *
   * @param bucketname The name of the bucket
   * @param keyname The name of the key in the bucket
   * @param filepath The location of the file.
   */
  public void uploadFile(String bucketname, String keyname, String filepath) {
    TransferManager tm = TransferManagerBuilder.standard().withS3Client(this.client).build();
    // TransferManager processes all transfers asynchronously,
    // so this call will return immediately.
    Upload upload = tm.upload(bucketname, keyname, new File(filepath));
    // You can set a progress listener directly on a transfer, or you can pass one into
    // the upload object to have it attached to the transfer as soon as it starts
    // This method is called periodically as your transfer progresses
    upload.addProgressListener(
        (ProgressListener)
            progressEvent -> {
              log.info("### {} %", upload.getProgress().getPercentTransferred());

              if (upload.getProgress().getPercentTransferred() == 100) {
                log.info("##### Upload complete!!! #######");
              }
            });
    try {
      // block and wait for the upload to finish
      upload.waitForCompletion();
      log.info("Upload complete");
    } catch (InterruptedException e) {
      log.error("Unable to upload file, upload was aborted.");
      log.error(e.getMessage());
    } finally {
      tm.shutdownNow(false);
    }
  }

  /**
   * Upload a stream instead of a file to a S3 bucket.
   *
   * @param stream The FileInputStream containing the bytes if the file or folder.
   * @param bucket The name of the bucket to be uploaded.
   * @param key The name of the resource in S3.
   * @return PutObjectResult: the result of the put request, or null.
   */
  public synchronized PutObjectResult uploadFileStream(
      FileInputStream stream, String bucket, String key) {
    try {
      ObjectMetadata objectMetadata = new ObjectMetadata();
      PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, key, stream, objectMetadata);
      PutObjectResult result = this.client.putObject(putObjectRequest);
      log.info("Etag: {} ---> {}", result.getETag(), result);
      return result;
    } catch (AmazonServiceException ase) {
      log.error(ase.getMessage());
      return null;
    }
  }

  /**
   * Download an S3 object as an InputStream.
   *
   * @param bucketname The name of the bucket
   * @param key The name of the resource to be downloaded to
   * @return S3ObjectInputStream: the input stream if successful, null otherwise.
   */
  public synchronized S3ObjectInputStream downloadFileStream(String bucketname, String key) {
    try {
      GetObjectRequest request = new GetObjectRequest(bucketname, key);
      S3Object object = this.client.getObject(request);
      return object.getObjectContent();
    } catch (AmazonServiceException ase) {
      log.error(ase.getMessage());
      return null;
    }
  }

  /**
   * Download an entire folder from an S3 bucket to a local directory.
   *
   * @param bucket The name of the bucket
   * @param folderPrefix The path of the folder being download from S3
   * @param folderpath The final location of the downloaded folder.
   */
  public void downloadFolder(String bucket, String folderPrefix, String folderpath) {
    if (!folderpath.endsWith(File.separator)) {
      folderpath = folderpath + this.SUFFIX;
    }
    if (!folderPrefix.endsWith(File.separator)) {
      folderPrefix = folderPrefix + this.SUFFIX;
    }

    File outputDirectory = new File(folderpath);
    TransferManager tm = TransferManagerBuilder.standard().withS3Client(this.client).build();
    MultipleFileDownload multipleFileDownload =
        tm.downloadDirectory(bucket, folderPrefix, outputDirectory, true);

    try {
      log.info("Download directory '{}' from bucket '{}'", folderPrefix, bucket);
      multipleFileDownload.waitForCompletion();
    } catch (InterruptedException e) {
      log.error(e.getMessage());
    } finally {
      tm.shutdownNow(false);
    }
  }

  /**
   * Upload an entire folder to a S3 bucket.
   *
   * @param bucket The name of the bucket
   * @param folderpath The path of the folder being uploaded.
   */
  public void uploadFolder(String bucket, String folderpath) {
    // add the separator at the end of the file
    if (!folderpath.endsWith(File.separator)) {
      folderpath = folderpath + this.SUFFIX;
    }

    File folder = new File(folderpath);
    TransferManager tm = TransferManagerBuilder.standard().withS3Client(this.client).build();
    MultipleFileUpload upload = tm.uploadDirectory(bucket, folder.getName(), folder, true);

    try {
      upload.waitForCompletion();
    } catch (InterruptedException e) {
      log.error(e.getMessage());
    } finally {
      tm.shutdownNow(false);
    }
  }

  /**
   * Upload an object to S3.
   *
   * @param object
   * @param bucketname The name of the bucket
   * @param filename
   */
  public void createObject(String object, String bucketname, String filename) {
    try {
      ByteArrayInputStream input = new ByteArrayInputStream(object.getBytes());
      this.client.putObject(bucketname, filename, input, new ObjectMetadata());
    } catch (AmazonClientException ace) {
      log.error(ace.getMessage());
    }
  }

  /**
   * Get a list of all the elements in a bucket.
   *
   * @param bucketname The name of the bucket
   * @return List A list of S3ObjectSummary
   */
  public Set<S3ObjectSummary> listBucketContent(String bucketname) {
    Set<S3ObjectSummary> bucketContent = new HashSet<>();
    try {
      log.info("Listing objects");
      final ListObjectsV2Request req =
          new ListObjectsV2Request().withBucketName(bucketname).withMaxKeys(50);
      ListObjectsV2Result result;
      do {
        result = this.client.listObjectsV2(req);

        bucketContent.addAll(result.getObjectSummaries());
        log.info("Next Continuation Token : {}", result.getNextContinuationToken());
        req.setContinuationToken(result.getNextContinuationToken());
      } while (result.isTruncated());

    } catch (AmazonServiceException ase) {
      log.info(
          "Caught an AmazonServiceException, "
              + "which means your request made it "
              + "to Amazon S3, but was rejected with an error response "
              + "for some reason.");
      log.info("Error Message:    {}", ase.getMessage());
      log.info("HTTP Status Code: {}", ase.getStatusCode());
      log.info("AWS Error Code:   {}", ase.getErrorCode());
      log.info("Error Type:       {}", ase.getErrorType());
      log.info("Request ID:       {}", ase.getRequestId());
    } catch (AmazonClientException ace) {
      log.info(
          "Caught an AmazonClientException, "
              + "which means the client encountered "
              + "an internal error while trying to communicate"
              + " with S3, "
              + "such as not being able to access the network.");
      log.info("Error Message: {}", ace.getMessage());
    }
    return bucketContent;
  }

  /**
   * Restore archived objects in S3.
   *
   * @param bucketName The name of the bucket
   * @param objectKey the object key to be restored
   * @param expirationInDays How many days till it expires
   */
  public void restoreArchivedObject(String bucketName, String objectKey, Integer expirationInDays) {
    try {
      RestoreObjectRequest requestRestore =
          new RestoreObjectRequest(bucketName, objectKey, expirationInDays);
      this.client.restoreObject(requestRestore);

      GetObjectMetadataRequest requestCheck = new GetObjectMetadataRequest(bucketName, objectKey);
      ObjectMetadata response = this.client.getObjectMetadata(requestCheck);

      Boolean restoreFlag = response.getOngoingRestore();
      log.info("Restoration status: {}", (restoreFlag) ? "in progress" : "finished");

    } catch (AmazonS3Exception amazonS3Exception) {
      log.error("An Amazon S3 error occurred. Exception: {}", amazonS3Exception.toString());
    } catch (Exception ex) {
      log.error(ex.getMessage());
    }
  }

  /**
   * Create a folder inside a bucket in S3.
   *
   * @param bucketname
   * @param foldername
   */
  public String createFolder(String bucketname, String foldername) {
    try {
      /* create metadata for the folder and set content-length to 0. */
      ObjectMetadata metadata = new ObjectMetadata();
      metadata.setContentLength(0);
      /* create empty content */
      InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
      /* Create a PutObjectRequest passing the folder name suffixed by / */
      String key = foldername + this.SUFFIX;
      PutObjectRequest putObjectRequest =
          new PutObjectRequest(bucketname, key, emptyContent, metadata);
      /* send request to S3 to create folder */
      this.client.putObject(putObjectRequest);
      return key;
    } catch (AmazonClientException ace) {
      log.error(ace.getMessage());
    }
    return null;
  }

  /**
   * Delete a bucket, if it exists.
   *
   * @param bucketname The name of the bucket to be deleted.
   */
  public void deleteBucket(String bucketname) {
    // bucket does not exist ... nothing to do
    if (!bucketExists(bucketname)) {
      return;
    }
    try {
      log.info("Removing objects from bucket");
      ObjectListing object_listing = this.client.listObjects(bucketname);
      while (true) {
        for (Iterator<?> iterator = object_listing.getObjectSummaries().iterator(); iterator
            .hasNext();) {
          S3ObjectSummary summary = (S3ObjectSummary) iterator.next();
          this.client.deleteObject(bucketname, summary.getKey());
        }

        // more object_listing to retrieve?
        if (object_listing.isTruncated()) {
          object_listing = this.client.listNextBatchOfObjects(object_listing);
        } else {
          break;
        }
      }

      log.info(" - removing versions from bucket");
      VersionListing version_listing =
          this.client.listVersions(new ListVersionsRequest().withBucketName(bucketname));
      while (true) {
        for (S3VersionSummary vs : version_listing.getVersionSummaries()) {
          this.client.deleteVersion(bucketname, vs.getKey(), vs.getVersionId());
        }
        if (version_listing.isTruncated()) {
          version_listing = this.client.listNextBatchOfVersions(version_listing);
        } else {
          break;
        }
      }

      log.info(" OK, bucket ready to delete!");
      this.client.deleteBucket(bucketname);
    } catch (AmazonServiceException ace) {
      log.error(ace.getMessage());
    }
  }

  /**
   * Delete all the files in a bucket.
   *
   * @param bucketName
   * @param keyName
   */
  public void deleteObject(String bucketName, String keyName) {
    try {
      this.client.deleteObject(new DeleteObjectRequest(bucketName, keyName));
    } catch (AmazonClientException ace) {
      log.error(ace.getMessage());
    }
  }

  /**
   * @param bucketName
   * @param keys
   */
  public void multiObjectDelete(String bucketName, List<DeleteObjectsRequest.KeyVersion> keys) {
    // Multi-object delete by specifying only keys (no version ID).
    DeleteObjectsRequest multiObjectDeleteRequest =
        new DeleteObjectsRequest(bucketName).withQuiet(false);

    // Create request that include only object key names.
    List<DeleteObjectsRequest.KeyVersion> justKeys = new ArrayList<>();
    for (DeleteObjectsRequest.KeyVersion key : keys) {
      justKeys.add(new DeleteObjectsRequest.KeyVersion(key.getKey()));
    }

    multiObjectDeleteRequest.setKeys(justKeys);
    // Execute DeleteObjects - Amazon S3 add delete marker for each object
    // deletion. The objects no disappear from your bucket (verify).
    DeleteObjectsResult delObjRes;

    try {
      delObjRes = this.client.deleteObjects(multiObjectDeleteRequest);
      log.info("Successfully deleted all the {} items.", delObjRes.getDeletedObjects().size());
    } catch (MultiObjectDeleteException mode) {
      log.error(mode.getMessage());
    }
  }

  /**
   * Delete all a folder in a bucket. In order to delete the folder, all the files in that folder
   * have to be deleted first.
   *
   * @param bucketname
   * @param foldername
   */
  public void deleteFolder(String bucketname, String foldername) {
    try {
      List<S3ObjectSummary> fileList =
          this.client.listObjects(bucketname, foldername).getObjectSummaries();
      for (S3ObjectSummary file : fileList) {
        this.client.deleteObject(bucketname, file.getKey());
      }
      this.client.deleteObject(bucketname, foldername);
    } catch (AmazonClientException ace) {
      log.error(ace.getMessage());
    }
  }

  /**
   * Produce a URL object, which can be fetched for download.
   *
   * @param bucketname
   * @param filename
   * @return
   */
  public URL generateDownloadURL(String bucketname, String filename) {
    GeneratePresignedUrlRequest request = new GeneratePresignedUrlRequest(bucketname, filename);
    return this.client.generatePresignedUrl(request);
  }

  /**
   * @param responses
   * @return
   */
  private static List<PartETag> GetETags(List<CopyPartResult> responses) {
    // Helper function that constructs ETags.
    List<PartETag> etags = new ArrayList<>();
    for (CopyPartResult response : responses) {
      etags.add(new PartETag(response.getPartNumber(), response.getETag()));
    }
    return etags;
  }
}
