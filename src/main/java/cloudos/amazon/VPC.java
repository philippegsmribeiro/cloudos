package cloudos.amazon;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.AssociateRouteTableRequest;
import com.amazonaws.services.ec2.model.CreateRouteTableRequest;
import com.amazonaws.services.ec2.model.CreateRouteTableResult;
import com.amazonaws.services.ec2.model.CreateSubnetRequest;
import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.CreateVpcRequest;
import com.amazonaws.services.ec2.model.CreateVpcResult;
import com.amazonaws.services.ec2.model.DeleteSubnetRequest;
import com.amazonaws.services.ec2.model.DeleteVpcRequest;
import com.amazonaws.services.ec2.model.DescribeSubnetsRequest;
import com.amazonaws.services.ec2.model.DescribeSubnetsResult;
import com.amazonaws.services.ec2.model.DescribeVpcsRequest;
import com.amazonaws.services.ec2.model.DescribeVpcsResult;
import com.amazonaws.services.ec2.model.Subnet;
import com.amazonaws.services.ec2.model.Tag;
import com.amazonaws.services.ec2.model.Vpc;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.log4j.Log4j2;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by philipperibeiro on 6/26/17.
 *
 * <p>Allow the user to create Virtual Private Clouds in Amazon AWS.
 */
@Log4j2
public class VPC {

  private static final String NAME = "Name";

  private AmazonEC2 client;

  /**
   * VPC constructor that takes another client as parameter.
   *
   * @param client an existing AmazonEC2 client
   */
  public VPC(AmazonEC2 client) {
    this.client = client;
  }

  /**
   * VPC constructor that takes a String region as parameter.
   *
   * @param region the region the EC2 client will be created
   */
  public VPC(String region, AWSCredentialsProvider provider) {
    client = AmazonEC2ClientBuilder.standard().withRegion(region).withCredentials(provider).build();
  }

  /**
   * Create a VPC by giving the desired CIDR block.
   *
   * @param cidrBlock the CIDR block of addresses in this VPC
   * @return a newly created VPC
   */
  public Vpc createVpc(String cidrBlock) {
    log.info("Creating a new VPC with CIDR block {}", cidrBlock);
    CreateVpcRequest vpc = new CreateVpcRequest().withCidrBlock(cidrBlock);
    return this.createVpc(vpc);
  }

  /**
   * Create a VPC by giving the desired CIDR block.
   *
   * @param cidrBlock the CIDR block of addresses in this VPC
   * @param tags the name of the Vpc
   * @return a newly created VPC
   */
  public Vpc createVpc(String cidrBlock, List<Tag> tags) {
    log.info("Creating a new VPC with CIDR block {}", cidrBlock);
    CreateVpcRequest vpcRequest = new CreateVpcRequest().withCidrBlock(cidrBlock);

    Vpc vpc = this.createVpc(vpcRequest);

    if (vpc != null) {
      // assign tags to the newly created VPC
      CreateTagsRequest createTagsRequest = new CreateTagsRequest();
      createTagsRequest.setTags(tags);
      createTagsRequest.withResources(vpc.getVpcId());
      this.client.createTags(createTagsRequest);
    }

    return null;
  }

  /**
   * Create a new VPC with IpV6.
   *
   * @param cidrBlock the CIDR block of addresses in this VPC
   * @param ipv6CidrBlock create VPC as IPv6
   * @return a newly created VPC
   */
  public Vpc createVpc(String cidrBlock, Boolean ipv6CidrBlock) {
    log.info("Creating a new VPC with CIDR block {} for IPv6", cidrBlock);
    CreateVpcRequest vpc =
        new CreateVpcRequest()
            .withCidrBlock(cidrBlock)
            .withAmazonProvidedIpv6CidrBlock(ipv6CidrBlock);
    return this.createVpc(vpc);
  }

  /**
   * Create a new Amazon Vpc, given the CIDR block and the instance tenancy.
   *
   * @param cidrBlock the CIDR block of addresses in this VPC
   * @param instanceTenancy the current instance tenancy for this VPC.
   * @return a newly created VPC
   */
  public Vpc createVpc(String cidrBlock, String instanceTenancy) {
    log.info("Creating a new VPC with CIDR block {} in tenancy {}", cidrBlock, instanceTenancy);
    CreateVpcRequest vpc =
        new CreateVpcRequest().withCidrBlock(cidrBlock).withInstanceTenancy(instanceTenancy);
    return this.createVpc(vpc);
  }

  /**
   * Create a new Amazon Vpc, given the CIDR block, the instance tenancy, and the ipv6CidrBlock.
   *
   * @param cidrBlock the CIDR block of addresses in this VPC
   * @param instanceTenancy the current instance tenancy for this VPC
   * @param ipv6CidrBlock create VPC as IPv6
   * @param name then ame of the VPC
   * @param tags a list of tags
   * @return a newly created VPC
   */
  @SuppressWarnings("Duplicates")
  public Vpc createVpc(String cidrBlock, String instanceTenancy, Boolean ipv6CidrBlock,
      String name, List<Tag> tags) {

    log.info("Creating a new VPC with CIDR block {}", cidrBlock);
    CreateVpcRequest vpcRequest = new CreateVpcRequest()
                                        .withCidrBlock(cidrBlock)
                                        .withInstanceTenancy(instanceTenancy)
                                        .withAmazonProvidedIpv6CidrBlock(ipv6CidrBlock);

    Vpc vpc = this.createVpc(vpcRequest);

    if (vpc != null) {
      // create a new tag
      Tag tag = new Tag().withKey(NAME).withValue(name);
      tags.add(tag);

      // assign tags to the newly created VPC
      CreateTagsRequest createTagsRequest = new CreateTagsRequest();
      createTagsRequest.setTags(tags);
      createTagsRequest.withResources(vpc.getVpcId());
      this.client.createTags(createTagsRequest);
    }

    return vpc;
  }

  /**
   * Create a new Subnet inside an existing VPC.
   *
   * @param vpcId the id of the VPC
   * @param subnetCidrBlock the CIDR block of addresses for the subnet
   * @param availabilityZone specify which availability zone to use.
   * @param isIpv6 if the CIDR block is to be IPv6
   * @param isPrivateSubnet if we should make the subnet private
   * @return a newly created AWS subnet
   */
  public Subnet createSubnet(
      String vpcId,
      String subnetCidrBlock,
      String availabilityZone,
      Boolean isIpv6,
      Boolean isPrivateSubnet,
      String name) {
    // create request
    CreateSubnetRequest csr = new CreateSubnetRequest()
                                    .withAvailabilityZone(availabilityZone)
                                    .withVpcId(vpcId);
    // define if the subnet is an IPv6 or IPv4
    if (isIpv6) {
      csr.withIpv6CidrBlock(subnetCidrBlock);
    } else {
      csr.withCidrBlock(subnetCidrBlock);
    }

    // send the request
    Subnet subnet = this.client.createSubnet(csr).getSubnet();
    log.info("Subnet with id {} created", subnet.getSubnetId());

    // make this Subnet private
    if (isPrivateSubnet) {
      // create a new route table
      CreateRouteTableRequest routeTableRequest = new CreateRouteTableRequest().withVpcId(vpcId);

      CreateRouteTableResult routeTableResult = this.client.createRouteTable(routeTableRequest);

      if (routeTableResult == null || routeTableResult.getRouteTable() == null) {
          log.warn("Failure creating new Route Table for VPC {}", vpcId);
        return null;
      }

      AssociateRouteTableRequest associateRouteTableRequest = new AssociateRouteTableRequest()
                          .withRouteTableId(routeTableResult.getRouteTable().getRouteTableId())
                          .withSubnetId(subnet.getSubnetId());
      this.client.associateRouteTable(associateRouteTableRequest);
    }

    // set the name of the subnet
    if (StringUtils.isNotEmpty(name)) {
      this.setResourceName(name, subnet.getSubnetId());
    }

    return subnet;
  }

  /**
   * List all the Vpcs in the current AWS account.
   *
   * @return a List of VPCs
   */
  public List<Vpc> listVpcs() {
    // create a new request
    DescribeVpcsResult result = this.client.describeVpcs();
    List<Vpc> vpcList = result.getVpcs();

    // avoid null pointer exceptions
    if (vpcList != null) {
      return vpcList;
    }
    return new ArrayList<>();
  }

  /**
   * List all the subnets in the surrent AWS account.
   *
   * @return {@link List<Subnet>} a list of subnets
   */
  public List<Subnet> listSubnets() {
    DescribeSubnetsResult result = this.client.describeSubnets();
    List<Subnet> subnets = result.getSubnets();

    // avoid null pointer exceptions
    if (subnets != null) {
      return subnets;
    }

    return new ArrayList<>();
  }

  /**
   * Describe one or more VPCs by their id.
   *
   * @param vpcIds a number of VPC ids
   * @return a List of VPC
   */
  public List<Vpc> describeVpc(String... vpcIds) {
    DescribeVpcsRequest request = new DescribeVpcsRequest().withVpcIds(vpcIds);

    DescribeVpcsResult result = this.client.describeVpcs(request);
    List<Vpc> vpcList = result.getVpcs();

    // avoid null pointer exceptions
    if (vpcList != null) {
      return vpcList;
    }
    return new ArrayList<>();
  }

  /**
   * Describe the subnet ids given in the arguments.
   *
   * @param subnetIds a number of subnet ids
   * @return a list of subnets
   */
  public List<Subnet> describeSubnets(String... subnetIds) {
    DescribeSubnetsRequest request = new DescribeSubnetsRequest().withSubnetIds(subnetIds);

    DescribeSubnetsResult result = this.client.describeSubnets(request);
    List<Subnet> subnets = result.getSubnets();

    // avoid null pointer exceptions
    if (subnets != null) {
      return subnets;
    }

    return new ArrayList<>();
  }

  /**
   * Delete an existing VPC.
   *
   * @param vpcId the VPC id
   */
  public void deleteVpc(String vpcId) {
    // check if the the VPC exists before attempting to delete it
    if (this.describeVpc(vpcId) == null) {
      log.warn("The VPC {} does not exist ... nothing to do");
      return;
    }
    // we need to delete the subnets in the VPC first
    List<Subnet> subnets = this.listSubnets();
    if (CollectionUtils.isNotEmpty(subnets)) {
      subnets.forEach(subnet -> {
        // the subnet belongs to this vpc ... delete it
        if (subnet.getVpcId().equals(vpcId)) {
          this.deleteSubnet(subnet.getSubnetId());
        }
      });
    }

    // now delete the subnet
    DeleteVpcRequest deleteVPC = new DeleteVpcRequest().withVpcId(vpcId);

    this.client.deleteVpc(deleteVPC);
    log.info("Deleted VPC {}", vpcId);
  }

  /**
   * Delete an existing subnet.
   *
   * @param subnetId the id of the subnet to be deleted
   */
  public void deleteSubnet(String subnetId) {
    if (this.describeSubnets(subnetId) == null) {
      log.warn("The subnet {} does not exist ... nothing to do");
      return;
    }

    DeleteSubnetRequest deleteSubnet = new DeleteSubnetRequest().withSubnetId(subnetId);
    this.client.deleteSubnet(deleteSubnet);
    log.info("Deleted subnet {} successfully", subnetId);
  }

  /**
   * Create a VPC by giving the configured request.
   *
   * @param vpc a CreateVpcRequest
   * @return a newly created VPC
   */
  private Vpc createVpc(CreateVpcRequest vpc) {
    CreateVpcResult res = this.client.createVpc(vpc);
    Vpc vp = res.getVpc();
    if (vp != null) {
      log.info("Created VPC with id {}", vp.getVpcId());
      return vp;
    } else {
      log.warn("Newly created VPC is invalid");
      return null;
    }
  }

  /**
   * Set the name of the resource, in this case, a subnet or a vpc.
   *
   * @param name the name of the resource
   * @param resource the resource id
   */
  private void setResourceName(String name, String resource) {
    List<Tag> tags = new ArrayList<>();
    Tag tag = new Tag().withKey(NAME).withValue(name);
    tags.add(tag);

    // Create a new tag request
    CreateTagsRequest tagsRequest = new CreateTagsRequest()
                                          .withTags(tags)
                                          .withResources(resource);

    this.client.createTags(tagsRequest);
  }
}
