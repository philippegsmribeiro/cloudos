package cloudos.amazon;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.cloudwatchevents.AmazonCloudWatchEvents;
import com.amazonaws.services.cloudwatchevents.AmazonCloudWatchEventsClientBuilder;
import com.amazonaws.services.cloudwatchevents.model.PutEventsRequest;
import com.amazonaws.services.cloudwatchevents.model.PutEventsRequestEntry;
import com.amazonaws.services.cloudwatchevents.model.PutRuleRequest;
import com.amazonaws.services.cloudwatchevents.model.PutTargetsRequest;
import com.amazonaws.services.cloudwatchevents.model.RuleState;
import com.amazonaws.services.cloudwatchevents.model.Target;

import lombok.extern.log4j.Log4j2;

/**
 * Created by philipperibeiro on 4/30/17.
 *
 * <p>CloudWatch Events delivers a near real-time stream of system events that describe changes in
 * AWS resources to Amazon EC2 instances, Lambda functions, Amazon Kinesis streams, Amazon ECS
 * tasks, Step Functions state machines, Amazon SNS topics, Amazon SQS queues, or built-in targets.
 * You can match events and route them to one or more target functions or streams by using simple
 * rules. @Copyright 2017 Cybernetic Cloud Inc.
 *
 * @license: Proprietary License.
 * @author Philippe Ribeiro
 * @date: 4/30/2017
 */
@Log4j2
public class CloudWatchEvents {

  // define the CloudWatchEvents client
  private AmazonCloudWatchEvents cloudWatchEvents;

  /**
   * Allow the user to specify the region as a Regions object.
   *
   * @param region: The region where the client should operate
   */
  public CloudWatchEvents(String region, AWSCredentialsProvider provider) {
    this.cloudWatchEvents =
        AmazonCloudWatchEventsClientBuilder.standard()
            .withCredentials(provider)
            .withRegion(region)
            .build();
  }

  /**
   * Add a new event to CloudWatch
   *
   * @param event: The name of the event
   * @param detail: The details
   * @param resource: The name of the resource
   * @param source: The source
   * @return boolean
   */
  public boolean addEvent(String event, String detail, String resource, String source) {
    PutEventsRequestEntry request_entry =
        new PutEventsRequestEntry()
            .withDetail(event)
            .withDetailType(detail)
            .withResources(resource)
            .withSource(source);

    PutEventsRequest request = new PutEventsRequest().withEntries(request_entry);

    return this.cloudWatchEvents.putEvents(request) != null;
  }

  /**
   * Add a new rule to CloudWatch
   *
   * @param rule: The rule's name
   * @param role: The role's name
   * @param schedule: The schedule's name
   * @return boolean
   */
  public boolean addRule(String rule, String role, String schedule) {
    PutRuleRequest request =
        new PutRuleRequest()
            .withName(rule)
            .withRoleArn(role)
            .withScheduleExpression(schedule)
            .withState(RuleState.ENABLED);

    return this.cloudWatchEvents.putRule(request) != null;
  }

  /**
   * Add a new target to CloudWatch
   *
   * @param function_arn: the function_arn
   * @param target_id: The target id
   * @param rule_name: The rule name
   * @return boolean
   */
  public boolean addTarget(String function_arn, String target_id, String rule_name) {
    Target target = new Target().withArn(function_arn).withId(target_id);

    PutTargetsRequest request = new PutTargetsRequest().withTargets(target).withRule(rule_name);

    return this.cloudWatchEvents.putTargets(request) != null;
  }
}
