package cloudos.amazon;

import cloudos.Providers;
import cloudos.dashboard.DashboardRegion;
import cloudos.dashboard.DashboardService;
import cloudos.pricing.PricingInfo;
import cloudos.pricing.PricingInfoRepository;
import cloudos.pricing.PricingType;
import cloudos.pricing.PricingValue;
import cloudos.pricing.PricingValueUnit;
import cloudos.pricing.PricingVersion;
import cloudos.pricing.PricingVersionRepository;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * AWSPricing is responsible for reading data from Amazon's AWS and insert in the appropriate
 * database. The information is correctly parsed and assigned to the respective collection in Mongo.
 *
 * @author Philippe Ribeiro
 */
@Service
@Log4j2
public class AmazonPricing extends AmazonPricingList {

  private static final String IMAGE_WINDOWS = "Windows";
  private static final String URL_TEMPLATE = "https://pricing.us-east-1.amazonaws.com%s";
  public static final String AMAZON_RDS = "AmazonRDS";
  public static final String AMAZON_S3 = "AmazonS3";
  public static final String AMAZON_REDSHIFT = "AmazonRedshift";
  public static final String AMAZON_EC2 = "AmazonEC2";
  public static final String AMAZON_GLACIER = "AmazonGlacier";
  public static final String AMAZON_SES = "AmazonSES";
  public static final String AMAZON_SIMPLE_DB = "AmazonSimpleDB";
  public static final String AMAZON_COGNITO = "AmazonCognito";
  public static final String AMAZON_DYNAMO_DB = "AmazonDynamoDB";
  public static final String AMAZON_ECR = "AmazonECR";
  public static final String AMAZON_ROUTE_53 = "AmazonRoute53";
  public static final String AMAZON_AWS_CONFIG = "AWSConfig";
  public static final String AMAZON_COGNITO_SYNC = "AmazonCognitoSync";
  public static final String AMAZON_EFS = "AmazonEFS";
  public static final String AMAZON_ELASTIC_CACHE = "AmazonElastiCache";
  public static final String AMAZON_CLOUD_FRONT = "AmazonCloudFront";
  public static final String AMAZON_KMS = "awskms";
  public static final String AMAZON_WORK_DOCS = "AmazonWorkDocs";
  public static final String AMAZON_VPC = "AmazonVPC";
  public static final String AMAZON_QUEUE_SERVICE = "AWSQueueService";
  public static final String AMAZON_ALEXA = "AlexaTopSites";
  public static final String AMAZON_CLOUD_WATCH = "AmazonCloudWatch";

  @Autowired
  PricingInfoRepository pricingVersionInfoRepository;
  @Autowired
  PricingVersionRepository pricingVersionRepository;
  @Autowired
  DashboardService dashboardService;

  @Autowired
  private cloudos.utils.ProfileUtils profileUtils;

  public AmazonPricing() {
    super();
  }

  /**
   * The getAWSPricing method first fetches all the URLs where the pricing info is located. Then for
   * each one of the URLS, which represent a different AWS service, fetch the data associated with
   * the URL and invoke the appropriate method.
   *
   * @throws Exception if something goes wrong
   */
  public void getAwsPricing() throws Exception {
    getAwsPricing(null);
  }

  /**
   * Fetch and save a pricing information about a service type.
   * 
   * @param serviceType - If null, the routine will fetch all amazon offers (ec2,s3,etc). Otherwise,
   *        will fetch only the filtered offer.
   * @throws Exception if something goes wrong
   */
  @SuppressWarnings("unchecked")
  public void getAwsPricing(String serviceType) throws Exception {
    Map<String, Object> offers = this.getOffers();
    for (Map.Entry<String, Object> entry : offers.entrySet()) {
      if (entry.getKey().equals("offers")) {
        Map<String, Object> map = (Map<String, Object>) entry.getValue();

        for (Map.Entry<String, Object> service : map.entrySet()) {
          Map<String, Object> map2 = (Map<String, Object>) service.getValue();

          if (StringUtils.isNotBlank(serviceType) && !service.getKey().equals(serviceType)) {
            continue;
          }

          // just get EC2 for now
          if (map2.get("offerCode").equals(AMAZON_EC2)) {
            String serviceUrl = (String) map2.get("currentVersionUrl");
            String url = String.format(URL_TEMPLATE, serviceUrl);
            Map<String, Object> servicePricing = this.sendGet(url);
            this.parsePricingResult(servicePricing, url);
          }
        }
      }
    }
  }

  /**
   * Return the version.
   *
   * @param newDocumentsActive - Indicates that the new workload of pricing data will be active or
   *        not.
   * @param filteredOffer - If null, the routine will fetch all amazon offers (ec2,s3,etc).
   *        Otherwise, will fetch only the filtered offer.
   * @throws Exception if something goes wrong
   */
  @SuppressWarnings("unchecked")
  public String getAwsPricingVersion(boolean newDocumentsActive, String filteredOffer)
      throws Exception {
    Map<String, Object> offers = this.getOffers();
    for (Map.Entry<String, Object> entry : offers.entrySet()) {
      if (entry.getKey().equals("offers")) {
        Map<String, Object> map = (Map<String, Object>) entry.getValue();

        for (Map.Entry<String, Object> service : map.entrySet()) {
          Map<String, Object> map2 = (Map<String, Object>) service.getValue();

          if (StringUtils.isNotBlank(filteredOffer) && !service.getKey().equals(filteredOffer)) {
            continue;
          }

          String serviceUrl = (String) map2.get("currentVersionUrl");
          String url = String.format(URL_TEMPLATE, serviceUrl);
          return this.getVersion(url);
        }
      }
    }
    return null;
  }

  /**
   * Parse the url result.
   * 
   * @param servicePricing json mapping
   * @param newDocumentsActive - Indicates that the new workload of pricing data will be active or
   *        not.
   * @throws Exception if something goes wrong
   */
  @SuppressWarnings("unchecked")
  private void parsePricingResult(Map<String, Object> servicePricing, String url) throws Exception {
    if (servicePricing.containsKey("offerCode") && servicePricing.containsKey("terms")) {
      String version = (String) servicePricing.get("version");
      Map<String, Object> products = (Map<String, Object>) servicePricing.get("products");
      Map<String, Object> terms = (Map<String, Object>) servicePricing.get("terms");
      if (servicePricing.get("offerCode").equals(AMAZON_EC2)) {
        this.insertEC2Pricing(products, terms, url, version);
      }
    }
  }

  /**
   * Insert the EC2 pricing schema into the Mongo Database.
   *
   * @param products A map containing the instance information
   * @param terms A map containing the pricing schema.
   * @param url fetched
   * @param version of the pricing
   * @throws Exception if something goes wrong
   */
  @SuppressWarnings({"unchecked"})
  private void insertEC2Pricing(Map<String, Object> products, Map<String, Object> terms, String url,
      String version) throws Exception {

    Map<String, List<DashboardRegion>> mapRegion =
        dashboardService.findRegionsByProviders(Providers.AMAZON_AWS).stream()
            .collect(Collectors.groupingBy(DashboardRegion::getName));

    if (MapUtils.isEmpty(mapRegion)) {
      throw new Exception("No regions to map!");
    }

    PricingVersion pricingVersion = PricingVersion.builder()
                                                  .url(url)
                                                  .provider(Providers.AMAZON_AWS)
                                                  .type(PricingType.INSTANCE)
                                                  .version(version)
                                                  .build();
    pricingVersion = pricingVersionRepository.save(pricingVersion);

    Date today = new Date();
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    Map<String, String> mapSkuRegion = new HashMap<>();
    Map<String, String> mapSkuMachineType = new HashMap<>();
    Map<String, String> mapSkuImage = new HashMap<>();
    /* First insert the Instance typing */
    for (Map.Entry<String, Object> product : products.entrySet()) {
      Map<String, Object> document = (Map<String, Object>) product.getValue();
      Map<String, Object> attMap = (Map<String, Object>) document.get("attributes");
      String instanceType = (String) attMap.get("instanceType");
      String instanceLocation = (String) attMap.get("location");
      List<DashboardRegion> list = mapRegion.get(instanceLocation);
      if (CollectionUtils.isNotEmpty(list)) {
        instanceLocation = list.get(0).getRegion();
      }
      String instanceOs = (String) attMap.get("operatingSystem");
      mapSkuRegion.put((String) document.get("sku"), instanceLocation);
      mapSkuMachineType.put((String) document.get("sku"), instanceType);
      mapSkuImage.put((String) document.get("sku"), instanceOs);
    }

    /* Then insert the pricing schema */
    List<PricingInfo> infos = new ArrayList<>();
    for (Map.Entry<String, Object> term : terms.entrySet()) {
      
      Map<String, Object> sku = (Map<String, Object>) term.getValue();
      for (Map.Entry<String, Object> entry : sku.entrySet()) {
        Map<String, Object> termCode = (Map<String, Object>) entry.getValue();
        for (Map.Entry<String, Object> code : termCode.entrySet()) {

          Map<String, Object> current = (Map<String, Object>) code.getValue();
          try {
            String skuValue = (String) current.get("sku");
            String instanceType = mapSkuMachineType.get(skuValue);
            String instanceLocation = mapSkuRegion.get(skuValue);
            String instanceOs = mapSkuImage.get(skuValue);

            PricingInfo info = new PricingInfo();
            info.setMachineType(instanceType);
            info.setRegion(instanceLocation);
            info.setImage(instanceOs);
            info.setName(String.format("%s-%s-%s", instanceType, instanceLocation, instanceOs));
            info.setProvider(Providers.AMAZON_AWS);
            info.setContent(gson.toJson(current));
            info.setPricingVersion(pricingVersion.getId());
            info.setLoadingDate(today);
            info.setReferenceDate(formatter.parse((String) current.get("effectiveDate")));

            if (profileUtils.isTest()) {
              // if test - ignoring windows
              if (IMAGE_WINDOWS.equals(instanceOs)) {
                log.debug("IGNORING: {}", info);
                continue;
              }
            }
            log.debug(info);
            infos.add(info);
          } catch (ParseException e) {
            log.error("Error getting pricing for {}", current);
          }
        }
      }
    }
    log.info("Number of pricing info: {}", infos.size());
    pricingVersionInfoRepository.save(infos);
  }

  /**
   * Get a pricing value for an instance.
   *
   * @param machineType type of instance
   * @param region of instance
   * @param imageProject imageProject (OS)
   * @return PricingValue
   */
  @SuppressWarnings("unchecked")
  public PricingValue getPricingForInstance(String machineType, String region,
      String imageProject) {
    List<PricingVersion> versions =
        pricingVersionRepository.findByProviderAndType(Providers.AMAZON_AWS, PricingType.INSTANCE);
    if (CollectionUtils.isNotEmpty(versions)) {
      // get the latest
      PricingVersion pricingVersion =
          versions.stream().sorted(Comparator.comparing(PricingVersion::getVersion))
              .collect(Collectors.toList()).get(0);
      PricingInfo pricingInfo =
          pricingVersionInfoRepository.findByPricingVersionAndMachineTypeAndRegionAndImage(
              pricingVersion.getId(), machineType, region, "Linux");
      if (pricingInfo != null) {
        Map<String, Object> json = gson.fromJson(pricingInfo.getContent(), Map.class);
        Map<String, Object> pricing =
            (Map<String, Object>) ((Map<String, Object>) json.get("priceDimensions")).values()
                .iterator().next();

        String unit = (String) pricing.get("unit");
        Map<String, Object> pricePerUnit = (Map<String, Object>) pricing.get("pricePerUnit");
        Entry<String, Object> next = pricePerUnit.entrySet().iterator().next();
        String currency = next.getKey();
        Double price = Double.parseDouble((String) next.getValue());
        return new PricingValue(currency, price, mapUnit(unit));
      }
    }
    return null;
  }

  /**
   * Map the string unit to the PricingValueUnit.
   *
   * @param unit string
   * @return PricingValueUnit
   */
  private PricingValueUnit mapUnit(String unit) {
    switch (unit) {
      case "Hrs":
        return PricingValueUnit.HRS;
      default:
        break;
    }
    return null;
  }
}
