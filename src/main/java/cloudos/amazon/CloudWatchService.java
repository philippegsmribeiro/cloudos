package cloudos.amazon;

import com.amazonaws.regions.Regions;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.validation.constraints.NotNull;

import lombok.extern.log4j.Log4j2;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CloudWatchService {

  @Autowired AWSCredentialService awsCredentialService;

  private static ConcurrentMap<String, CloudWatch> map = new ConcurrentHashMap<>();

  /**
   * Get the cloudwatch for region
   *
   * @param region
   * @return
   */
  public synchronized CloudWatch getClient(@NotNull String region) {
    try {
      // check if region is null.. if it is, use the default AWS Region.
      if (StringUtils.isBlank(region)) {
        region = Regions.US_EAST_1.getName();
      }

      // There should be only one CloudWatch client per region
      if (map.containsKey(region)) {
        log.info("Returning CloudWatch for region: {}", region);
        return map.get(region);
      } else {
        // Create a new CloudWatch client by region
        CloudWatch cloudWatch = new CloudWatch(region, awsCredentialService.getAWSCredentials());
        log.info("Creating CloudWatch for region: {}", region);
        // adding in the map
        map.put(region, cloudWatch);
        return cloudWatch;
      }
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return null;
    }
  }

  public void resetMaps() {
    map.clear();
  }
}
