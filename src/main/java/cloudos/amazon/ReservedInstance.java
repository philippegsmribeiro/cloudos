package cloudos.amazon;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;

import lombok.ToString;
import lombok.extern.log4j.Log4j2;

/**
 * Define an object for the Reserved Instances in AWS. This object is composed of a GroupName, which
 * must exists before the object is instantiated, and a keyPairName. @Copyright CyberTextron Inc.
 * 2016
 *
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
@Log4j2
@ToString
public class ReservedInstance extends EC2 {

  public ReservedInstance() {
    super();
  }

  public ReservedInstance(String groupName, AmazonEC2 client) {
    super(groupName, client);
  }

  public ReservedInstance(String groupName, String region, AWSCredentialsProvider provider) {
    super(groupName, region, provider);
  }

  /**
   * @param ami
   * @param instance
   * @param minCount
   * @param maxCount
   * @param keyPairName
   * @param securityGroup
   */
  public void createInstance(
      String ami,
      String instance,
      Integer minCount,
      Integer maxCount,
      String keyPairName,
      String securityGroup) {
    assert client != null;
    /* add boundaries, e.g max number of instances allowed to be created */
    assert minCount >= 1 && minCount <= maxCount;

    RunInstancesRequest runInstancesRequest = new RunInstancesRequest();
    runInstancesRequest
        .withImageId(ami)
        .withInstanceType(instance)
        .withMinCount(minCount)
        .withMaxCount(maxCount)
        .withKeyName(keyPairName)
        .withSecurityGroups(securityGroup);
    RunInstancesResult runInstancesResult = client.runInstances(runInstancesRequest);
    log.info(runInstancesResult);
  }

}
