package cloudos.amazon;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;
import com.amazonaws.services.sqs.buffered.AmazonSQSBufferedAsyncClient;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.DeleteMessageBatchRequest;
import com.amazonaws.services.sqs.model.DeleteMessageBatchRequestEntry;
import com.amazonaws.services.sqs.model.DeleteMessageBatchResult;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.DeleteQueueRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesResult;
import com.amazonaws.services.sqs.model.ListQueuesRequest;
import com.amazonaws.services.sqs.model.ListQueuesResult;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.PurgeQueueInProgressException;
import com.amazonaws.services.sqs.model.PurgeQueueRequest;
import com.amazonaws.services.sqs.model.QueueAttributeName;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageBatchRequest;
import com.amazonaws.services.sqs.model.SendMessageBatchRequestEntry;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SetQueueAttributesRequest;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import lombok.extern.log4j.Log4j2;

import org.apache.commons.collections.CollectionUtils;
import org.bson.types.ObjectId;

/**
 * @Copyright Cybernetic Cloud Inc. 2017.
 *
 * @license: private
 * @author Philippe Ribeiro
 * @date: 6/15/2017
 */
@Log4j2
public class SQS {

  /* Make the AmazonEC2 client static, so it can be used by other objects */
  private AmazonSQSAsync sqs;

  private static final String DEFAULT_DELAY_PERIOD = "60";
  private static final String DEFAULT_RETENTION_PERIOD = "86400";
  private static final int DEFAULT_WAIT_TIME_SECONDS = 1;
  private static final int DEFAULT_MAX_NUMBER_OF_MESSAGES = 10;
  private static final String QUEUE_ARN = "QueueArn";
  private static final String DEAD_LETTER_REDRIVE_POLICY = "RedrivePolicy";
  private static final String DEAD_LETTER_MAX_RECEIVE_COUNT = "maxReceiveCount";
  private static final String DEAD_LETTER_MAX_RECEIVE_COUNT_VALUE = "5";
  private static final String DEAD_LETTER_TARGET_ARN = "deadLetterTargetArn";

  /**
   * Another constructor, that takes the Regions object and the Credentials.
   *
   * @param region - The AWS region the queues will be created.
   * @param credentials - The credentials
   */
  public SQS(String region, AWSCredentialsProvider credentials) {
    // Create the basic Amazon SQS async client
    AmazonSQSAsync sqsAsync = AmazonSQSAsyncClientBuilder.standard().withCredentials(credentials)
        .withRegion(region).build();
    // Create the buffered client
    this.sqs = new AmazonSQSBufferedAsyncClient(sqsAsync);
    log.info("Starting AWS SQS");
  }

  /**
   * Create a new queue by only specifying the name and using the default periods.
   *
   * @param name - The name of the queue
   * @return String - The queue URL.
   */
  public String createQueue(String name) {
    return this.createQueue(name, DEFAULT_DELAY_PERIOD, DEFAULT_RETENTION_PERIOD);
  }

  /**
   * Create a new queue by specifying the delay and retention period.
   *
   * @param name - The name of the queue
   * @param delay - The delay period of the queue
   * @param retentionPeriod - The retention period of the queue.
   * @return String - The queue URL.
   */
  public String createQueue(String name, String delay, String retentionPeriod) {
    try {
      log.info("Creating a new SQS queue called {}", name);
      // @TODO: Check if the queue already exists
      CreateQueueRequest request =
          new CreateQueueRequest(name).addAttributesEntry("DelaySeconds", delay)
              .addAttributesEntry("MessageRetentionPeriod", retentionPeriod);

      return this.sqs.createQueue(request).getQueueUrl();
    } catch (AmazonServiceException ase) {
      log.error("Caught an AmazonServiceException, which means your request made it "
          + "to Amazon SQS, but was rejected with an error response for some reason.");
      log.error("Error Message:    {}", ase.getMessage());
      log.error("HTTP Status Code: {}", ase.getStatusCode());
      log.error("AWS Error Code:   {}", ase.getErrorCode());
      log.error("Error Type:       {}", ase.getErrorType());
      log.error("Request ID:       {}", ase.getRequestId());
      throw ase;
    } catch (AmazonClientException ace) {
      log.error("Caught an AmazonClientException, which means the client encountered "
          + "a serious internal problem while trying to communicate with SQS, such as not "
          + "being able to access the network.");
      log.error("Error Message: {}", ace.getMessage());
      throw ace;
    }
  }

  /**
   * Return a list of all the queues.
   *
   * @return List - A list of all the queues in this region
   */
  public List<String> listQueues() {
    List<String> queues = this.sqs.listQueues().getQueueUrls();
    if (queues != null) {
      return queues;
    }
    return new ArrayList<>();
  }

  /**
   * Filter a queue by its prefix.
   *
   * @param namePrefix - The prefix of the queue name.
   * @return List - A list of queues that match the prefix.
   */
  public List<String> filterQueues(String namePrefix) {
    log.info("Queue URLs with prefix: {}", namePrefix);
    ListQueuesResult lqResult = this.sqs.listQueues(new ListQueuesRequest(namePrefix));
    return lqResult.getQueueUrls();
  }

  /**
   * Delete an existing SQS Queue.
   *
   * @param queueUrl - The Queue URL
   * @return boolean: If deleting the queue was successfull or not.
   */
  public boolean deleteQueue(String queueUrl) {
    try {
      log.info("Deleting the queue {}", queueUrl);
      this.sqs.deleteQueue(new DeleteQueueRequest(queueUrl));
      return true;
    } catch (AmazonServiceException ase) {
      log.error("Exception while attempting to delete the SQS queue {}", queueUrl);
      log.error("Error Message:    {}", ase.getMessage());
      log.error("HTTP Status Code: {}", ase.getStatusCode());
      log.error("AWS Error Code:   {}", ase.getErrorCode());
      log.error("Error Type:       {}", ase.getErrorType());
      log.error("Request ID:       {}", ase.getRequestId());
      throw ase;
    } catch (AmazonClientException ace) {
      log.error("Caught an AmazonClientException, which means the client encountered "
          + "a serious internal problem while trying to communicate with SQS, such as not "
          + "being able to access the network.");
      log.error("Error Message: {}", ace.getMessage());
      throw ace;
    }
  }

  /**
   * Read all the messages in a queue.
   *
   * @param queueUrl - The Queue URL
   * @return List - A list of Message
   */
  public List<Message> readMessages(String queueUrl) {
    return this.readMessages(queueUrl, DEFAULT_WAIT_TIME_SECONDS, DEFAULT_MAX_NUMBER_OF_MESSAGES);
  }

  /**
   * Read all the messages in a queue.
   *
   * @param queueUrl - The Queue URL
   * @param waitTimeSeconds - waiting time
   * @param maxNumberOfMessages - max number of messages
   * @return List - A list of Message
   */
  public List<Message> readMessages(String queueUrl, int waitTimeSeconds, int maxNumberOfMessages) {
    log.debug("reading messages from {}", queueUrl);
    ReceiveMessageRequest request = new ReceiveMessageRequest().withQueueUrl(queueUrl)
        .withWaitTimeSeconds(waitTimeSeconds).withMaxNumberOfMessages(maxNumberOfMessages)
        .withAttributeNames(QueueAttributeName.CreatedTimestamp);
    try {
      return this.sqs.receiveMessage(request).getMessages();
    } catch (Exception e) {
      log.error("Error reading the queue", e);
      return Collections.emptyList();
    }
  }

  /**
   * Delete a list of messages from the queue.
   *
   * @param queueUrl - The queue the messages will be deleted from
   * @param messages - A list of messages
   */
  public void deleteMessages(String queueUrl, List<Message> messages) {
    log.info("deleting messages from queue {}", queueUrl);
    for (Message message : messages) {
      this.deleteMessage(queueUrl, message.getReceiptHandle());
    }
  }

  /**
   * Delete a single message in the queue url.
   *
   * @param message - The message to be deleted.
   * @param queueUrl - The Queue URL
   * @return boolean - If the message was correctly deleted.
   */
  public boolean deleteMessage(String queueUrl, Message message) {
    log.info("Deleting message {} from Queue {}", message, queueUrl);
    return this.deleteMessage(queueUrl, message.getReceiptHandle());
  }

  /**
   * Delete a single message in the queue url.
   *
   * @param messageReceiptHandle - The message receipt to be deleted.
   * @param queueUrl - The Queue URL
   * @return boolean - If the message was correctly deleted.
   */
  public boolean deleteMessage(String queueUrl, String messageReceiptHandle) {
    try {
      log.info("Deleting message of {} from Queue {}", messageReceiptHandle, queueUrl);
      this.sqs.deleteMessage(new DeleteMessageRequest(queueUrl, messageReceiptHandle));
      return true;
    } catch (AmazonServiceException ase) {
      log.error("Exception thrown while attempting to delete message of {} from queue {}",
          messageReceiptHandle, queueUrl);
      log.error("Error Message:    {}", ase.getMessage());
      log.error("HTTP Status Code: {}", ase.getStatusCode());
      log.error("AWS Error Code:   {}", ase.getErrorCode());
      log.error("Error Type:       {}", ase.getErrorType());
      log.error("Request ID:       {}", ase.getRequestId());
      throw ase;
    } catch (AmazonClientException ace) {
      log.error("Caught an AmazonClientException, which means the client encountered "
          + "a serious internal problem while trying to communicate with SQS, such as not "
          + "being able to access the network.");
      log.error("Error Message: {}", ace.getMessage());
      throw ace;
    }
  }

  /**
   * Read the content.
   * 
   * @param messages - list of messages
   */
  public void readContent(List<Message> messages) {
    for (Message message : messages) {
      log.info("Message {}", message);
      log.info("Message ID: {}", message.getMessageId());
      log.info("Receipt Handler: {}", message.getReceiptHandle());
      log.info("MD5 of body: {}", message.getMD5OfBody());
      log.info("Body: {}", message.getBody());
      for (Entry<String, String> entry : message.getAttributes().entrySet()) {
        log.info("Name: {}", entry.getKey());
        log.info("Value: {}", entry.getValue());
      }
    }
  }

  /**
   * Send a single message to the SQS queue specified.
   *
   * @param queueUrl - The queue url
   * @param message - The message to be sent
   * @param delay - How long to delay the message for
   * @return boolean - If the message as correctly sent.
   */
  public boolean sendMessage(String queueUrl, String message, Integer delay) {
    try {
      SendMessageRequest sendMsgRequest = new SendMessageRequest().withQueueUrl(queueUrl)
          .withMessageBody(message).withDelaySeconds(delay);
      sqs.sendMessage(sendMsgRequest);
      return true;
    } catch (AmazonServiceException ase) {
      log.error("Exception while attempting to send message {} to queue {}", message, queueUrl);
      log.error("Error Message:    {}", ase.getMessage());
      log.error("HTTP Status Code: {}", ase.getStatusCode());
      log.error("AWS Error Code:   {}", ase.getErrorCode());
      log.error("Error Type:       {}", ase.getErrorType());
      log.error("Request ID:       {}", ase.getRequestId());
      throw ase;
    } catch (AmazonClientException ace) {
      log.error("Caught an AmazonClientException, which means the client encountered "
          + "a serious internal problem while trying to communicate with SQS, such as not "
          + "being able to access the network.");
      log.error("Error Message: {}", ace.getMessage());
      throw ace;
    }
  }

  /**
   * Send a message batch instead of a single message.
   *
   * @param queueUrl - The queue url
   * @param entries - The messages to be sent
   * @param delay - How long to delay the message for
   */
  public void sendMessage(String queueUrl, Map<String, String> entries, Integer delay) {
    // Send multiple messages to the queue
    List<SendMessageBatchRequestEntry> batchRequestEntries = new ArrayList<>();

    for (Map.Entry<String, String> entry : entries.entrySet()) {
      batchRequestEntries.add(new SendMessageBatchRequestEntry(entry.getKey(), entry.getValue())
          .withDelaySeconds(delay));
    }

    SendMessageBatchRequest sendBatchRequest =
        new SendMessageBatchRequest().withQueueUrl(queueUrl).withEntries(batchRequestEntries);
    this.sqs.sendMessageBatch(sendBatchRequest);
  }

  /**
   * Set a dead letter queue for a queue.
   *
   * @param queueUrl - the queue url
   * @param deadLetterQueueUrl - the dead queue url
   */
  @SuppressWarnings("Duplicates")
  public void setDeadLetterQueueForQueue(String queueUrl, String deadLetterQueueUrl) {
    try {
      GetQueueAttributesResult queueAttrs = sqs.getQueueAttributes(
          new GetQueueAttributesRequest(deadLetterQueueUrl).withAttributeNames(QUEUE_ARN));
      String dlQueueArn = queueAttrs.getAttributes().get(QUEUE_ARN);
      // Set dead letter queue with redrive policy on source queue.
      Map<String, String> redrivePolicy = new HashMap<>();
      redrivePolicy.put(DEAD_LETTER_MAX_RECEIVE_COUNT, DEAD_LETTER_MAX_RECEIVE_COUNT_VALUE);
      redrivePolicy.put(DEAD_LETTER_TARGET_ARN, dlQueueArn);
      SetQueueAttributesRequest request = new SetQueueAttributesRequest().withQueueUrl(queueUrl)
          .addAttributesEntry(DEAD_LETTER_REDRIVE_POLICY, new Gson().toJson(redrivePolicy));
      sqs.setQueueAttributes(request);
    } catch (AmazonServiceException ase) {
      log.error("Error Message:    {}", ase.getMessage());
      log.error("HTTP Status Code: {}", ase.getStatusCode());
      log.error("AWS Error Code:   {}", ase.getErrorCode());
      log.error("Error Type:       {}", ase.getErrorType());
      log.error("Request ID:       {}", ase.getRequestId());
      throw ase;
    } catch (AmazonClientException ace) {
      log.error("Caught an AmazonClientException, which means the client encountered "
          + "a serious internal problem while trying to communicate with SQS, such as not "
          + "being able to access the network.");
      log.error("Error Message: {}", ace.getMessage());
      throw ace;
    }
  }

  /**
   * Delete the message in batch.
   *
   * @param queueUrl - the queue url
   * @param deletes - delete messages
   * @return true if the messages get deleted, false otherwise
   */
  public boolean deleteMessageBatch(String queueUrl, List<Message> deletes) {
    if (CollectionUtils.isEmpty(deletes)) {
      return true;
    }
    DeleteMessageBatchRequest deleteMessageBatchRequest = new DeleteMessageBatchRequest(queueUrl);
    deleteMessageBatchRequest.setEntries(new ArrayList<>());
    for (Message message : deletes) {
      deleteMessageBatchRequest.getEntries().add(new DeleteMessageBatchRequestEntry()
          .withId(new ObjectId().toHexString()).withReceiptHandle(message.getReceiptHandle()));
    }
    try {
      DeleteMessageBatchResult deleteMessageBatch =
          this.sqs.deleteMessageBatch(deleteMessageBatchRequest);
      return deleteMessageBatch.getFailed().isEmpty();
    } catch (AmazonServiceException ase) {
      log.error("Error Message:    {}", ase.getMessage());
      log.error("HTTP Status Code: {}", ase.getStatusCode());
      log.error("AWS Error Code:   {}", ase.getErrorCode());
      log.error("Error Type:       {}", ase.getErrorType());
      log.error("Request ID:       {}", ase.getRequestId());
      throw ase;
    } catch (AmazonClientException ace) {
      log.error("Caught an AmazonClientException, which means the client encountered "
          + "a serious internal problem while trying to communicate with SQS, such as not "
          + "being able to access the network.");
      log.error("Error Message: {}", ace.getMessage());
      throw ace;
    }
  }

  /**
   * Purge a list.
   *
   * @param queueUrl - the queue url
   * @return true if the queue get purged, false otherwise
   */
  @SuppressWarnings("Duplicates")
  public boolean purgeQueue(String queueUrl) {
    try {
      this.sqs.purgeQueue(new PurgeQueueRequest(queueUrl));
    } catch (PurgeQueueInProgressException e) {
      // return true because it is already in progress of purging
      return true;
    } catch (AmazonServiceException ase) {
      log.error("Error Message:    {}", ase.getMessage());
      log.error("HTTP Status Code: {}", ase.getStatusCode());
      log.error("AWS Error Code:   {}", ase.getErrorCode());
      log.error("Error Type:       {}", ase.getErrorType());
      log.error("Request ID:       {}", ase.getRequestId());
      throw ase;
    } catch (AmazonClientException ace) {
      log.error("Caught an AmazonClientException, which means the client encountered "
          + "a serious internal problem while trying to communicate with SQS, such as not "
          + "being able to access the network.");
      log.error("Error Message: {}", ace.getMessage());
      throw ace;
    }
    return true;
  }

  /**
   * Count (approximately) the number of messages on the queue.
   * 
   * @param queueUrl url for the queue
   * @return number approximate of messages
   */
  public long countMessages(String queueUrl) {
    try {
      GetQueueAttributesRequest getQueueAttributesRequest =
          new GetQueueAttributesRequest().withQueueUrl(queueUrl)
              .withAttributeNames(QueueAttributeName.ApproximateNumberOfMessages);
      GetQueueAttributesResult queueAttributes =
          this.sqs.getQueueAttributes(getQueueAttributesRequest);
      return Long.parseLong(
          queueAttributes.getAttributes().get(QueueAttributeName.ApproximateNumberOfMessages.name()));
    } catch (AmazonServiceException ase) {
      log.error("Error Message:    {}", ase.getMessage());
      log.error("HTTP Status Code: {}", ase.getStatusCode());
      log.error("AWS Error Code:   {}", ase.getErrorCode());
      log.error("Error Type:       {}", ase.getErrorType());
      log.error("Request ID:       {}", ase.getRequestId());
      throw ase;
    } catch (AmazonClientException ace) {
      log.error("Caught an AmazonClientException, which means the client encountered "
          + "a serious internal problem while trying to communicate with SQS, such as not "
          + "being able to access the network.");
      log.error("Error Message: {}", ace.getMessage());
      throw ace;
    }
  }
}
