package cloudos.amazon;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.AuthorizeSecurityGroupEgressRequest;
import com.amazonaws.services.ec2.model.AuthorizeSecurityGroupIngressRequest;
import com.amazonaws.services.ec2.model.AvailabilityZone;
import com.amazonaws.services.ec2.model.CreateSecurityGroupRequest;
import com.amazonaws.services.ec2.model.CreateSecurityGroupResult;
import com.amazonaws.services.ec2.model.DeleteSecurityGroupRequest;
import com.amazonaws.services.ec2.model.DescribeAvailabilityZonesResult;
import com.amazonaws.services.ec2.model.DescribeImagesResult;
import com.amazonaws.services.ec2.model.DescribeInstanceStatusRequest;
import com.amazonaws.services.ec2.model.DescribeInstanceStatusResult;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.DescribeKeyPairsRequest;
import com.amazonaws.services.ec2.model.DescribeKeyPairsResult;
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsRequest;
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsResult;
import com.amazonaws.services.ec2.model.DescribeTagsRequest;
import com.amazonaws.services.ec2.model.DescribeTagsResult;
import com.amazonaws.services.ec2.model.Image;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.InstanceStateName;
import com.amazonaws.services.ec2.model.InstanceStatus;
import com.amazonaws.services.ec2.model.IpPermission;
import com.amazonaws.services.ec2.model.KeyPairInfo;
import com.amazonaws.services.ec2.model.RebootInstancesRequest;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.RevokeSecurityGroupEgressRequest;
import com.amazonaws.services.ec2.model.RevokeSecurityGroupIngressRequest;
import com.amazonaws.services.ec2.model.SecurityGroup;
import com.amazonaws.services.ec2.model.StartInstancesRequest;
import com.amazonaws.services.ec2.model.StopInstancesRequest;
import com.amazonaws.services.ec2.model.Tag;
import com.amazonaws.services.ec2.model.TagDescription;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;

import org.apache.commons.lang.StringUtils;

/**
 * @Copyright Cybernetic Cloud Inc. 2016
 *
 * @license: private
 * @author Philippe Ribeiro
 * @date: 12/18/2016
 */
@Log4j2
@ToString
@NoArgsConstructor
public abstract class EC2 {

  /* define the name of the security group */
  protected String groupName;
  /*
   * Make the client static, since the connection can be shared across all the objects.
   */
  protected AmazonEC2 client;
  /* Allow to handle and manipulate Elastic Block Storage */
  protected EBS ebs;
  protected ArrayList<String> ipRanges;
  protected ArrayList<IpPermission> ipPermissions;
  /* Contain a list of all the instances created */
  protected List<String> instanceIds;
  /* Maintain a Map between the instance and its IP address */
  protected Map<String, String> addresses;

  protected volatile List<String> availabilityZones;

  /**
   * @param groupName
   * @param region
   */
  public EC2(String groupName, String region, AWSCredentialsProvider provider) {
    try {
      /* Create the AmazonEC2Client object so we can call various APIs */
      client =
          AmazonEC2ClientBuilder.standard().withRegion(region).withCredentials(provider).build();
      this.groupName = groupName;
      this.ebs = new EBS(client);
      /* Store all the instances created */
      this.instanceIds = new LinkedList<>();
      this.addresses = new HashMap<>();
    } catch (Exception e) {
      log.error(e.getMessage());
      throw new AmazonClientException(
          "Cannot load the credentials from the"
              + "credential profiles file. Please make sure that your credentials file is at"
              + " the correct location (~/.aws/credentials), and is in valid format.");
    }
  }

  /**
   * Constructor for the EC2 class. Since EC2 is an abstract class, it cannot be instantiated. The
   * children class can take advantage of this constructor for simplification.
   *
   * @param groupName The name of the Security Group the instances will belong to.
   * @param client: An AmazonEC2 client, which establishes connection with Amazon
   */
  public EC2(String groupName, AmazonEC2 client) {
    try {
      /* Create the AmazonEC2Client object so we can call various APIs */
      this.client = client;
      this.groupName = groupName;
      this.ebs = new EBS(client);
      /* Store all the instances created */
      this.instanceIds = new LinkedList<>();
      this.addresses = new HashMap<>();
    } catch (Exception e) {
      log.error(e.getMessage());
      throw new AmazonClientException(
          "Cannot load the credentials from the"
              + "credential profiles file. Please make sure that your credentials file is at"
              + " the correct location (~/.aws/credentials), and is in valid format.");
    }
  }

  /**
   * @param request
   * @return
   */
  public List<TagDescription> describeTags(DescribeTagsRequest request) {
    log.info("describe tags, request={}", request);
    DescribeTagsResult result = client.describeTags(request);
    return result.getTags();
  }

  /** @return */
  public synchronized List<String> availabilityZones() {
    if (this.availabilityZones == null) {
      DescribeAvailabilityZonesResult result = client.describeAvailabilityZones();
      this.availabilityZones =
          result
              .getAvailabilityZones()
              .stream()
              .filter(zone -> "available".equals(zone.getState()))
              .map(AvailabilityZone::getZoneName)
              .collect(Collectors.toList());
      log.info("Availability zones => {}", this.availabilityZones);
    }
    return this.availabilityZones;
  }

  /**
   * Create a new Security Group in AWS. If Security Group already exists, then it will throw an
   * exception
   *
   * @param groupName: The name of the Security Group the instances will belong to.
   */
  public String createSecurityGroup(String groupName, String description) {
    try {
      CreateSecurityGroupRequest csgr = new CreateSecurityGroupRequest();
      csgr.withGroupName(groupName).withDescription(description);
      CreateSecurityGroupResult createSecurityGroupResult = this.client.createSecurityGroup(csgr);
      log.info("Created cluster security group: {}", groupName);
      return createSecurityGroupResult.getGroupId();
    } catch (AmazonServiceException ase) {
      /*
       * Likely this means that the group is already created, so ignore.
       */
      log.error(ase.getMessage());
      return null;
    }
  }

  /**
   * @param groupName
   * @param rules
   */
  public void createSGIngressRules(
      String groupName, String ipPermission, List<IpPermission> rules) {
    try {
      AuthorizeSecurityGroupIngressRequest asgir = new AuthorizeSecurityGroupIngressRequest();
      asgir.withGroupName("JavaSecurityGroup").withIpPermissions(rules);
      this.client.authorizeSecurityGroupIngress(asgir);
    } catch (AmazonServiceException ase) {
      log.error(ase);
    }
  }

  /**
   * @param groupName
   * @param ipPermission
   * @param rules
   */
  public void createSGEngressRules(
      String groupName, String ipPermission, List<IpPermission> rules) {
    try {
      AuthorizeSecurityGroupEgressRequest asger = new AuthorizeSecurityGroupEgressRequest();
      asger.withSourceSecurityGroupName(groupName).withIpPermissions(rules);
      this.client.authorizeSecurityGroupEgress(asger);
    } catch (AmazonServiceException ase) {
      log.error(ase);
    }
  }

  /**
   * @param securityGroupName
   * @return
   */
  public SecurityGroup getSecurityGroup(String securityGroupName) {
    DescribeSecurityGroupsRequest securityRequest = new DescribeSecurityGroupsRequest();
    securityRequest.setGroupNames(Arrays.asList(securityGroupName));
    DescribeSecurityGroupsResult securityDescription =
        this.client.describeSecurityGroups(securityRequest);
    return securityDescription.getSecurityGroups().get(0);
  }

  /**
   * @param groupName
   * @param rules
   */
  public void deleteSGEgressRules(String groupName, List<IpPermission> rules) {
    try {
      log.info("delete ingress sg rules, groupName={}, rules={}", groupName, rules);
      client.revokeSecurityGroupEgress(
          new RevokeSecurityGroupEgressRequest()
              .withSourceSecurityGroupName(groupName)
              .withIpPermissions(rules));
    } catch (AmazonServiceException ase) {
      log.error(ase);
    }
  }

  /**
   * @param securityGroupId
   * @param rules
   */
  public void deleteSGIngressRules(String securityGroupId, List<IpPermission> rules) {
    try {
      log.info("delete ingress sg rules, sgId={}, rules={}", securityGroupId, rules);

      client.revokeSecurityGroupIngress(
          new RevokeSecurityGroupIngressRequest()
              .withGroupId(securityGroupId)
              .withIpPermissions(rules));
    } catch (AmazonServiceException ase) {
      log.error(ase);
    }
  }

  /**
   * Get a list of all the security groups in this particular region
   *
   * @return List: A List of Security Groups
   */
  public List<SecurityGroup> listSecurityGroup() {
    List<SecurityGroup> groups = new ArrayList<>();
    try {
      DescribeSecurityGroupsRequest securityRequest = new DescribeSecurityGroupsRequest();
      DescribeSecurityGroupsResult securityDescription =
          this.client.describeSecurityGroups(securityRequest);
      return securityDescription.getSecurityGroups();
    } catch (AmazonServiceException ase) {
      log.error(ase.getMessage());
    }
    return groups;
  }

  /**
   * Delete an existing Security Group
   *
   * @param groupName: The name of the security group being created.
   */
  public void deleteSecurityGroup(String groupName) throws AmazonServiceException {
    log.info("delete security group, groupName={}", groupName);
    client.deleteSecurityGroup(new DeleteSecurityGroupRequest().withGroupName(groupName));
  }

  /**
   * Get a list of all the available images in AWS.
   *
   * @return A list of all the available Images.
   */
  public List<Image> getAvailableImages() {
    try {
      DescribeImagesResult directory = client.describeImages();
      return directory.getImages();
    } catch (AmazonServiceException ase) {
      // just log the error.
      log.error(ase.getMessage());
      return null;
    }
  }

  /**
   * Get the number of instances in the list.
   *
   * @return the number of instances in the instanceIds list.
   */
  public int getInstanceCount() {
    return this.instanceIds.size();
  }

  /**
   * Get the IP of the current host, so that we can limit the Security Group by default to the ip
   * range associated with your subnet.
   *
   * @return String the IP address of the current host.
   */
  public String getLocalHostIpAddress() {
    try {
      String ipaddress = "0.0.0.0/0";
      InetAddress addr = InetAddress.getLocalHost();
      /* Get IP Address */
      ipaddress = addr.getHostAddress() + "/10";
      this.ipRanges.add(ipaddress);
      return ipaddress;
    } catch (UnknownHostException e) {
      log.error(e.getMessage());
      return null;
    }
  }

  /**
   * Open up port 'fromPort' for protocol traffic to the associate IP from above (e.g. SSH traffic )
   *
   * @param protocol
   * @param fromPort
   * @param toPort
   */
  public void addIpRanges(String protocol, Integer fromPort, Integer toPort) {

    IpPermission permission = new IpPermission();
    permission.setIpProtocol(protocol);
    permission.setFromPort(fromPort);
    permission.setToPort(toPort);
    // @TODO: Refactor this.
    permission.withIpRanges(this.ipRanges);
    this.ipPermissions.add(permission);

    try {
      /* Authorize the ports to be used. */
      AuthorizeSecurityGroupIngressRequest ingressRequest =
          new AuthorizeSecurityGroupIngressRequest(this.groupName, this.ipPermissions);
      client.authorizeSecurityGroupIngress(ingressRequest);
    } catch (AmazonServiceException ase) {
      /*
       * Ignore because this likely means the zone has already been authorized
       */
      log.warn(ase.getErrorMessage());
    }
  }

  /**
   * Get a list of all the current reservations in AWS.
   *
   * @return List<Reservation>
   */
  public List<Reservation> getReservations() {
    DescribeInstancesResult describeInstancesRequest = client.describeInstances();
    return describeInstancesRequest.getReservations();
  }

  /**
   * Get a set of all the current instances in use.
   *
   * @return Set<Instance> the set of instances.
   */
  public Set<Instance> getInstances() {
    Set<Instance> instances = new HashSet<>();
    List<Reservation> reservations = this.getReservations();
    for (Reservation reservation : reservations) {
      instances.addAll(reservation.getInstances());
    }

    return instances;
  }

  /**
   * Get the status (running, stopped, terminated) of a single EC2 instance, given its instanceId.
   *
   * @param instanceId the instance ID.
   * @return The instance status.
   */
  public String getInstanceStatus(String instanceId) {
    try {
      DescribeInstanceStatusRequest request =
          new DescribeInstanceStatusRequest().withInstanceIds(instanceId);
      DescribeInstanceStatusResult result = client.describeInstanceStatus(request);
      List<InstanceStatus> state = result.getInstanceStatuses();
      while (state.size() < 1) {
        // Do Nothing, just wait, have thread sleep if needed.
        result = client.describeInstanceStatus(request);
        state = result.getInstanceStatuses();
      }
      String status = state.get(0).getInstanceState().getName();
      return status;

    } catch (AmazonServiceException ase) {
      // log the error and return null
      log.error(ase.getErrorMessage());
      return null;
    }
  }

  /**
   * Return all the current instances Id.
   *
   * @return A list of instance id.
   */
  public List<String> getInstancesId() {
    return this.instanceIds;
  }

  /**
   * Get the status (running, stopped, terminated) of all EC2 instance
   *
   * @return A list of instance status.
   */
  public List<InstanceStatus> getInstanceStatus() {
    try {
      DescribeInstanceStatusRequest request = new DescribeInstanceStatusRequest();
      DescribeInstanceStatusResult result = client.describeInstanceStatus(request);
      List<InstanceStatus> state = result.getInstanceStatuses();
      while (state.size() < 1) {
        // Do Nothing, just wait, have thread sleep if needed.
        result = client.describeInstanceStatus(request);
        state = result.getInstanceStatuses();
      }
      return state;

    } catch (AmazonServiceException ase) {
      // log the error and return null
      log.error(ase.getErrorMessage());
      return new ArrayList<>();
    }
  }

  /**
   * @param instanceIds
   * @return
   */
  public List<InstanceStatus> getInstancesStatus(List<String> instanceIds)
      throws AmazonServiceException {
    List<InstanceStatus> state;
    try {
      DescribeInstanceStatusRequest request = new DescribeInstanceStatusRequest()
          .withIncludeAllInstances(true).withInstanceIds(instanceIds.stream()
              .filter(i -> StringUtils.isNotBlank(i)).collect(Collectors.toList()));
      DescribeInstanceStatusResult result = client.describeInstanceStatus(request);
      state = result.getInstanceStatuses();
      while (state.size() < 1) {
        // Do Nothing, just wait, have thread sleep if needed.
        result = client.describeInstanceStatus(request);
        state = result.getInstanceStatuses();
      }
    } catch (AmazonServiceException ase) {
      // log the error and return null
      log.error(ase.getErrorMessage());
      throw ase;
    }
    return state;
  }

  /** Start all the instances in the instanceIds list. */
  public void start() {
    try {
      log.info("Attempting to start all instances");
      StartInstancesRequest request = new StartInstancesRequest(this.instanceIds);
      client.startInstances(request);
    } catch (AmazonServiceException ase) {
      log.error(ase.getMessage());
    }
  }

  /**
   * Start a particular set of EC2 instance, given its ID
   *
   * @param instances: A list of instance IDs
   */
  public void start(List<String> instances) {
    try {
      log.info(
          "Attempting to start {} instances {}",
          instances.size(),
          Arrays.toString(instances.toArray()));
      StartInstancesRequest request = new StartInstancesRequest(instances);
      client.startInstances(request);
    } catch (AmazonServiceException ase) {
      log.error(ase.getMessage());
    }
  }

  /** Stop all the instances in the instanceIds list. */
  public void stop() {
    try {
      log.info("Attempting to stop all the instances");
      StopInstancesRequest request = new StopInstancesRequest(this.instanceIds);
      client.stopInstances(request);
    } catch (AmazonServiceException ase) {
      log.error(ase.getMessage());
    }
  }

  /**
   * Stop all the instances in a particular list of IDs.
   *
   * @param instances A list of instance IDs
   */
  public void stop(List<String> instances) {
    try {
      log.info(
          "Attempting to stop {} instances {}",
          instances.size(),
          Arrays.toString(instances.toArray()));
      StopInstancesRequest request = new StopInstancesRequest(instances);
      client.stopInstances(request);
    } catch (AmazonServiceException ase) {
      log.error(ase.getMessage());
    }
  }

  /** Reboot all the instances */
  public void reboot() {
    try {
      log.info("Reboot");
      RebootInstancesRequest request = new RebootInstancesRequest(this.instanceIds);
      client.rebootInstances(request);
    } catch (AmazonServiceException ase) {
      log.error(ase.getMessage());
    }
  }

  /**
   * Reboot all the instances in a particular list of IDs.
   *
   * @param instances A list of instance IDs
   */
  public void reboot(List<String> instances) {
    try {
      log.info(
          "Attempting rebooting {} instances {}",
          instances.size(),
          Arrays.toString(instances.toArray()));
      RebootInstancesRequest request = new RebootInstancesRequest(instances);
      client.rebootInstances(request);
    } catch (AmazonServiceException ase) {
      log.error(ase.getMessage());
    }
  }

  /** Terminate all the instances in the instanceIds list. */
  public void terminate() {
    try {
      log.info("Attempting to terminate all instances");
      TerminateInstancesRequest request = new TerminateInstancesRequest(this.instanceIds);
      client.terminateInstances(request);
    } catch (AmazonServiceException ase) {
      log.error(ase.getMessage());
    }
  }

  /**
   * Terminate all the instances in a particular list of IDs.
   *
   * @param instances A list of instance IDs
   */
  public void terminate(List<String> instances) {
    try {
      if (instances == null || instances.size() == 0) {
        return;
      }
      log.info(
          "Attempting to terminate {} instances {}",
          instances.size(),
          Arrays.toString(instances.toArray()));
      TerminateInstancesRequest request = new TerminateInstancesRequest(instances);
      client.terminateInstances(request);
    } catch (AmazonServiceException ase) {
      log.error(ase.getMessage());
    }
  }

  /**
   * Get all the currently running instances based on their common tag name.
   *
   * @param tagname the tag name of the instance.
   * @param value the value name of the instance.
   * @return a List of instance IDs.
   */
  public List<String> getRunningInstancesByTags(String tagname, String value) {
    List<String> instances = new ArrayList<>();
    for (Reservation reservation : this.getReservations()) {
      for (Instance instance : reservation.getInstances()) {
        // if the instance is not running, skip
        if (!instance.getState().getName().equals(InstanceStateName.Running.toString())) {
          continue;
        }
        for (Tag tag : instance.getTags()) {
          if (tag.getKey().equals(tagname) && tag.getValue().equals(value)) {
            instances.add(instance.getInstanceId());
          }
        }
      }
    }
    return instances;
  }

  /**
   * List all the keys in a particular region
   *
   * @return List<KeyPairInfo>: A list of KeyPairs
   */
  public List<KeyPairInfo> listAllKeys() {
    DescribeKeyPairsRequest request = new DescribeKeyPairsRequest();
    DescribeKeyPairsResult result = this.client.describeKeyPairs(request);
    return result.getKeyPairs();
  }

  /**
   * List all the keys in a particular region
   *
   * @return List<KeyPairInfo>: A list of KeyPairs
   */
  public List<KeyPairInfo> listAllKeys(String... keynames) {
    DescribeKeyPairsRequest request = new DescribeKeyPairsRequest().withKeyNames(keynames);
    DescribeKeyPairsResult result = this.client.describeKeyPairs(request);
    return result.getKeyPairs();
  }

  /** Shutdown the EC2 client connection. */
  @Override
  public void finalize() {
    if (this.client != null) {
      log.info("Shutting down the EC2 client");
      this.client.shutdown();
    } else {
      log.warn("EC2 client is null... nothing to do.");
    }
  }
}
