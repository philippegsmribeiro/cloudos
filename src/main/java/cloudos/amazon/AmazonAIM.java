package cloudos.amazon;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClientBuilder;
import com.amazonaws.services.identitymanagement.model.AccessKey;
import com.amazonaws.services.identitymanagement.model.AccessKeyLastUsed;
import com.amazonaws.services.identitymanagement.model.AccessKeyMetadata;
import com.amazonaws.services.identitymanagement.model.CreateAccessKeyRequest;
import com.amazonaws.services.identitymanagement.model.CreateAccessKeyResult;
import com.amazonaws.services.identitymanagement.model.DeleteAccessKeyRequest;
import com.amazonaws.services.identitymanagement.model.DeleteAccessKeyResult;
import com.amazonaws.services.identitymanagement.model.GetAccessKeyLastUsedRequest;
import com.amazonaws.services.identitymanagement.model.GetAccessKeyLastUsedResult;
import com.amazonaws.services.identitymanagement.model.ListAccessKeysRequest;
import com.amazonaws.services.identitymanagement.model.ListAccessKeysResult;
import com.amazonaws.services.identitymanagement.model.UpdateAccessKeyRequest;
import com.amazonaws.services.identitymanagement.model.UpdateAccessKeyResult;

import java.util.HashSet;
import java.util.Set;

/**
 * Manage Amazon AWS AMI credentials @Copyright CyberTextron Inc. 2017
 *
 * @license private
 * @author Philippe Ribeiro
 * @date 5/4/2017
 */
public class AmazonAIM {

  private AmazonIdentityManagement iam;

  /**
   * Constructor with the specific region
   *
   * @param region: The AWS region the AMI should be used.
   */
  public AmazonAIM(String region, AWSCredentialsProvider provider) {
    this.iam =
        AmazonIdentityManagementClientBuilder.standard()
            .withRegion(region)
            .withCredentials(provider)
            .build();
  }

  /**
   * Create a new access key for a given user.
   *
   * @param user: The name of the user
   * @return AccessKey: The newly created key
   */
  public AccessKey createAccessKey(String user) {
    CreateAccessKeyRequest request = new CreateAccessKeyRequest().withUserName(user);

    CreateAccessKeyResult response = this.iam.createAccessKey(request);
    return response.getAccessKey();
  }

  /**
   * List all the access keys in the given region that belongs to a particular username.
   *
   * @param username: The user name
   * @return Set: A set of AccessKeyMetadata
   */
  public Set<AccessKeyMetadata> listAccessKeys(String username) {

    boolean done = false;
    Set<AccessKeyMetadata> accessKeyMetadata = new HashSet<>();
    ListAccessKeysRequest request;

    while (!done) {
      request = new ListAccessKeysRequest().withUserName(username);

      ListAccessKeysResult response = this.iam.listAccessKeys(request);
      if (response != null) {
        accessKeyMetadata.addAll(response.getAccessKeyMetadata());
      }

      request.setMarker(response.getMarker());

      if (!response.getIsTruncated()) {
        done = true;
      }
    }
    return accessKeyMetadata;
  }

  /**
   * Return the last used Access Key
   *
   * @param access_id: THe access key id
   * @return AccessKeyLastUsed: the last used Access Key
   */
  public AccessKeyLastUsed getLastUsedAccessKey(String access_id) {
    GetAccessKeyLastUsedRequest request =
        new GetAccessKeyLastUsedRequest().withAccessKeyId(access_id);

    GetAccessKeyLastUsedResult response = this.iam.getAccessKeyLastUsed(request);
    return response.getAccessKeyLastUsed();
  }

  /**
   * Allow the user to active or deactivate an access key. The status should be Activate or
   * Inactive.
   *
   * @param access_id: The access id
   * @param username: The user name
   * @param status: The status should be Activate or Inactive.
   * @return boolean
   */
  public boolean updateAccessKey(String access_id, String username, AmazonAIMAccessState status) {

    UpdateAccessKeyRequest request =
        new UpdateAccessKeyRequest()
            .withAccessKeyId(access_id)
            .withUserName(username)
            .withStatus(status.toString());

    UpdateAccessKeyResult response = iam.updateAccessKey(request);
    return response != null;
  }

  /**
   * Delete the created access key.
   *
   * @param access_key: The access_key to be deleted
   * @param username: The user name that owns the key
   * @return boolean
   */
  public boolean deleteAccessKey(String access_key, String username) {

    DeleteAccessKeyRequest request =
        new DeleteAccessKeyRequest().withAccessKeyId(access_key).withUserName(username);

    DeleteAccessKeyResult response = iam.deleteAccessKey(request);
    return response != null;
  }
}
