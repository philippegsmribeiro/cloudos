package cloudos.amazon;

/**
 * Define all the Namespaces used in AWS. @Copyright 2017 Cybernetic Cloud Inc.
 *
 * @license: Proprietary License.
 * @author Philippe Ribeiro
 * @date: 1/6/2017
 */
public enum AwsNamespace {
  AMAZON_API_GATEWAY("AWS/ApiGateway"),
  AUTO_SCALING("AWS/AutoScaling"),
  AWS_BILLING("AWS/Billing"),
  AMAZON_CLOUD_FRONT("AWS/CloudFront"),
  AMAZON_CLOUD_SEARCH("AWS/CloudSearch"),
  AMAZON_CLOUD_WATCH_EVENTS("AWS/Events"),
  AMAZON_CLOUD_WATCH_LOGS("AWS/Logs"),
  AMAZON_DYNAMO_DB("AWS/DynamoDB"),
  AMAZON_EC2("AWS/EC2"),
  AMAZON_EC2_SPOT("AWS/EC2Spot"),
  AMAZON_EC2_CONTAINER_SERVICE("AWS/ECS"),
  AWS_ELASTIC_BEANSTALK("AWS/ElasticBeanstalk"),
  AMAZON_ELASTIC_BLOCK_STORE("AWS/EBS"),
  AMAZON_ELASTIC_FILE_SYSTEM("AWS/EFS"),
  CLASSIC_ELASTIC_LOAD_BALACING("AWS/ELB"),
  APPLICATION_ELASTIC_LOAD_BALACING("AWS/ApplicationELB"),
  AMAZON_ELASTIC_TRANSCODER("AWS/ElasticTranscoder"),
  AMAZON_ELASTICACHE("AWS/ElastiCache"),
  AMAZON_ELASTICSEARCH_SERVICE("AWS/ES"),
  AMAZON_EMR("AWS/ElasticMapReduce"),
  AWS_IOT("AWS/IoT"),
  AMS_KEY_MANAGEMENT_SERVICE("AWS/KMS"),
  AMAZON_KINESIS_ANALYTICS("AWS/KinesisAnalytics"),
  AMAZON_KINESIS_FIREHOSE("AWS/Firehose"),
  AMAZON_KINESIS_STREAM("AWS/Kinesis"),
  AWS_LAMBDA("AWS/Lambda"),
  AMAZON_MACHINE_LEARNING("AWS/ML"),
  AWS_OPSWORKS("AWS/OpsWorks"),
  AMAZON_POLLY("AWS/Polly"),
  AMAZON_REDSHIFT("AWS/Redshift"),
  AMAZON_RELATIONAL_DATABASE_SERVICE("AWS/RDS"),
  AMAZON_ROUTE_53("AWS/Route53"),
  AMAZON_SIMPLE_EMAIL_SERVICE("AWS/SES"),
  AMAZON_SIMPLE_NOTIFICATION_SERVICE("AWS/SNS"),
  AMAZON_SIMPLE_QUEUE_SERVICE("AWS/SQS"),
  AMAZON_SIMPLE_STORAGE_SERVICE("AWS/S3"),
  AMAZON_SIMPLE_WORKFLOW_SERVICE("AWS/SWF"),
  AWS_STORAGE_GATEWAY("AWS/StorageGateway"),
  AWS_WAF("AWS/WAF"),
  AMAZON_WORKSPACE("AWS/WorkSpaces");

  private final String namespace;

  AwsNamespace(String namespace) {
    this.namespace = namespace;
  }

  @Override
  public String toString() {
    return this.namespace;
  }
}
