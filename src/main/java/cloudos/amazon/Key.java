package cloudos.amazon;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.CreateKeyPairRequest;
import com.amazonaws.services.ec2.model.CreateKeyPairResult;
import com.amazonaws.services.ec2.model.DeleteKeyPairRequest;
import com.amazonaws.services.ec2.model.DescribeKeyPairsRequest;
import com.amazonaws.services.ec2.model.ImportKeyPairRequest;
import com.amazonaws.services.ec2.model.KeyPair;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;

/**
 * Create a KeyPair for AWS. @Copyright CyberTextron Inc. 2016
 *
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
@Log4j2
@ToString
@NoArgsConstructor
public class Key {

  /**
   * Create KeyPair Request for AWS. This will be used to access the AWS machines.
   *
   * @param keyname the keyname to be used. This must be a unique keyname, otherwise it will throw
   *     an exception
   * @param client An AmazonEC2 client.
   * @return keypair the new CreateKeyPairRequest object.
   */
  public String getPrivateKey(String keyname, AmazonEC2 client) {

    assert client != null;
    CreateKeyPairRequest keypair = new CreateKeyPairRequest();
    keypair.withKeyName(keyname);
    CreateKeyPairResult result = client.createKeyPair(keypair);
    // call the result object's getKeyPair method to obtain a KeyPair
    // object.
    KeyPair keyPair = result.getKeyPair();

    return keyPair.getKeyMaterial();
  }

  /**
   * @param keyname
   * @param client
   */
  public String saveKeyPair(String keyname, AmazonEC2 client) {
    try {
      assert client != null;
      String filename =
          System.getProperty("user.home")
              + File.separator
              + ".ssh"
              + File.separator
              + keyname
              + ".pub";
      Path path = new File(filename).toPath();
      if (path.toFile().exists()) {
        // if file exists - try to import it
        client.importKeyPair(
            new ImportKeyPairRequest(keyname, new String(Files.readAllBytes(path))));
      } else {
        // create it
        CreateKeyPairRequest keypair = new CreateKeyPairRequest();
        keypair.withKeyName(keyname);
        CreateKeyPairResult result = client.createKeyPair(keypair);
        // call the result object's getKeyPair method to obtain a KeyPair
        // object.
        KeyPair keyPair = result.getKeyPair();
        log.info("Saving a new Pemfile: " + filename);
        this.savePem(filename, keyPair);
        this.changePemPermission(filename);
      }
      return filename;
    } catch (Exception e) {
      log.error(e.getMessage());
      return null;
    }
  }

  /**
   * @param keyname
   * @param client
   */
  public void deleteKey(String keyname, AmazonEC2 client) {
    try {
      assert client != null;
      DeleteKeyPairRequest deleteRequest = new DeleteKeyPairRequest(keyname);
      client.deleteKeyPair(deleteRequest);
    } catch (AmazonServiceException e) {
      log.error(e.getMessage());
    }
  }

  /**
   * @param name
   * @param client
   * @return
   */
  public boolean keyPairExists(String name, AmazonEC2 client) {
    try {
      return !client
          .describeKeyPairs(new DescribeKeyPairsRequest().withKeyNames(name))
          .getKeyPairs()
          .isEmpty();
    } catch (AmazonServiceException e) {
      if ("InvalidKeyPair.NotFound".equals(e.getErrorCode())) {
        return false;
      }
      throw e;
    }
  }

  /** @param filename */
  public void changePemPermission(String filename) {
    String path = System.getProperty("user.home") + "/.ssh/";
    try {
      String command = "chmod 0400 test_deploy.pem";
      String[] tokens = command.split("\\s+");
      ProcessBuilder builder = new ProcessBuilder(tokens);
      System.out.println("------------------------------------------------");
      System.out.println("Executing " + command + " in directory " + path);
      System.out.println("Current path: " + Paths.get(".").toAbsolutePath().normalize().toString());
      builder.directory(new File(path));
      Process child = builder.start();
      // watch(child);
      System.out.println("------------------------------------------------");
    } catch (Exception ex) {
      System.out.println(ex.getMessage());
    }
  }

  /**
   * @param filename
   * @param keyPair
   */
  public void savePem(String filename, KeyPair keyPair) {
    String name;
    // if there's a pem extention
    if (!filename.endsWith(".pem")) {
      name = filename + ".pem";
    } else {
      name = filename;
    }
    File file = new File(name);
    if (file.exists()) {
      log.info(String.format("File %s already exists...deleting", name));
      if (file.delete()) {
        log.info("File " + name + " was successfully deleted.");
      }
    }
    // set the file permission to 0600
    file.setExecutable(false);
    file.setReadable(false);
    file.setWritable(false);

    /*
     * Apply a try-with-resources statement
     */
    try (BufferedReader bufferedReader =
        new BufferedReader(new StringReader(keyPair.getKeyMaterial()))) {
      try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file))) {
        char buffer[] = new char[1024];
        int len;
        while ((len = bufferedReader.read(buffer)) != -1) {
          bufferedWriter.write(buffer, 0, len);
        }
        bufferedWriter.flush();
      }
      bufferedReader.close();
      this.changePemPermission(filename);
    } catch (IOException ex) {
      log.error(ex.getMessage());
    }
  }
}
