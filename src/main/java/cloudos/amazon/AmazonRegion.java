package cloudos.amazon;

import cloudos.Providers;
import cloudos.provider.ProviderRegion;

public class AmazonRegion extends ProviderRegion {

  public AmazonRegion() {
    setProvider(Providers.AMAZON_AWS);
  }
}
