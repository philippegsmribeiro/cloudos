package cloudos.amazon;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.CreateImageRequest;
import com.amazonaws.services.ec2.model.CreateImageResult;
import com.amazonaws.services.ec2.model.CreateSnapshotRequest;
import com.amazonaws.services.ec2.model.CreateSnapshotResult;
import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.DescribeImagesRequest;
import com.amazonaws.services.ec2.model.DescribeImagesResult;
import com.amazonaws.services.ec2.model.DescribeInstanceStatusRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.DescribeSnapshotsRequest;
import com.amazonaws.services.ec2.model.DescribeSnapshotsResult;
import com.amazonaws.services.ec2.model.DescribeVolumesRequest;
import com.amazonaws.services.ec2.model.DescribeVolumesResult;
import com.amazonaws.services.ec2.model.Filter;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.InstanceMonitoring;
import com.amazonaws.services.ec2.model.InstanceStatus;
import com.amazonaws.services.ec2.model.MonitorInstancesRequest;
import com.amazonaws.services.ec2.model.MonitorInstancesResult;
import com.amazonaws.services.ec2.model.Placement;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.ResourceType;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.Tag;
import com.amazonaws.services.ec2.model.TagSpecification;
import com.amazonaws.services.ec2.model.Volume;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Define an object for the Reserved Instances in AWS. This object is composed of a GroupName, which
 * must exists before the object is instantiated, and a keyPairName. @Copyright CyberTextron Inc.
 * 2016
 *
 * @license: private
 * @author Philippe Ribeiro
 * @date: 4/2/2016
 */
@Log4j2
@ToString
public class AmazonEC2OnDemand extends EC2 {

  private static final int SLEEP_TIME_10 = 10 * 1000;

  /* Define constants to be used throughout the program */
  public static final int MAX_ATTEMPTS = 16;

  public static final long SLEEP_TIME = 1000 * 30;

  public static final String RUNNING = "running";

  public static final String OK = "ok";

  public static final String TAG_NAME = "Name";

  public AmazonEC2OnDemand(String securityGroup, AmazonEC2 client) {
    super(securityGroup, client);
    this.instanceIds = new LinkedList<>();
  }

  /**
   * Create a new On-Demand instance request. The user has the option to specify how many instances
   * must be created. The maxCount bound is 10,000 instances.
   *
   * @param ami The AMI ID.
   * @param instanceType The type of instance. (e.g 'm1.small')
   * @param minCount The minimum number of instances to be created.
   * @param maxCount The maximum number of instances to be created. Currently bounded at 10,000.
   * @param keyPairName The name of the key pair for AWS.
   * @param securityGroup The name of the security group.
   * @param cloudGroup The cloudGroupId that the instance will belong
   * @throws Exception if something goes wrong
   */
  public List<Instance> createInstance(String ami, String instanceType, Integer minCount,
      Integer maxCount, String keyPairName, String securityGroup, String zone, String name,
      Map<String, String> tags, String cloudGroup) throws Exception {

    if (this.client == null || minCount < 0 || maxCount < 0 || minCount > maxCount
        || maxCount > 1000) {
      return new ArrayList<>();
    }
    try {
      // verify if key exists
      this.verifyAndImportKey(keyPairName);
      // Create a new request to create AWS instances.
      RunInstancesRequest runInstancesRequest = new RunInstancesRequest();
      runInstancesRequest.withImageId(ami)
                         .withInstanceType(instanceType)
                         .withMinCount(minCount)
                         .withMaxCount(maxCount)
                         .withKeyName(keyPairName) 
                         .withMonitoring(true);
      
      // TODO: review security group logic
      boolean ids = false;
      if (securityGroup != null && securityGroup.startsWith("sg-")) {
        ids = true;
      }
      if (ids) {
        runInstancesRequest.withSecurityGroupIds(securityGroup);
      } else {
        runInstancesRequest.withSecurityGroups(securityGroup);
      }

      List<Tag> createTags = new ArrayList<>();
      if (StringUtils.isNotBlank(cloudGroup)) {
        createTags.add(new Tag(cloudos.models.Instance.CLOUDGROUP_ID_TAG, cloudGroup));
      }
      if (MapUtils.isNotEmpty(tags)) {
        tags.forEach((mapKey, mapValue) -> {
          createTags.add(new Tag(mapKey, mapValue));
        });
      }

      if (CollectionUtils.isNotEmpty(createTags)) {
        runInstancesRequest.withTagSpecifications(new TagSpecification()
            .withResourceType(ResourceType.Instance).withTags(createTags.toArray(new Tag[] {})));
      }
      if (StringUtils.isNotEmpty(zone)) {
        runInstancesRequest.withPlacement(new Placement(zone));
      }
      RunInstancesResult runInstancesResult = this.client.runInstances(runInstancesRequest);
      List<Instance> instances = runInstancesResult.getReservation().getInstances();
      // add the names
      if (StringUtils.isNotEmpty(name)) {
        this.tagEC2Instances(instances, name);
      }
      this.waitUntilRunning(instances);
      return this.waitForPublicIpAddresses(instances);
    } catch (

    AmazonServiceException ase) {
      StringBuffer error = new StringBuffer();
      error.append(String.format("Error Message:    %s\n", ase.getMessage()));
      error.append(String.format("HTTP Status Code: %s\n", ase.getStatusCode()));
      error.append(String.format("AWS Error Code:   %s\n", ase.getErrorCode()));
      error.append(String.format("Error Type:       %s\n", ase.getErrorType()));
      error.append(String.format("Request ID:       %s\n", ase.getRequestId()));
      log.error("Exception while attempting to create instances.\n {}", error.toString(), ase);
      throw new Exception(String.format("Error creating instances! \n%s", error.toString()), ase);
    } catch (AmazonClientException ace) {
      log.error("Caught an AmazonClientException, which means the client encountered "
          + "a serious internal problem while trying to communicate with EC2, such as not "
          + "being able to access the network.");
      log.error("Error Message: {}", ace.getMessage());
      throw new Exception(ace);
    } catch (InterruptedException e) {
      log.error("Error waiting for returning from the client.", e);
      throw new Exception(e);
    }
  }

  /**
   * Get a list of instances containing their public IP Address.
   *
   * @param instances A list of AWS Instances
   * @return List: A list of Instances with their public IP address set.
   */
  private List<Instance> waitForPublicIpAddresses(List<Instance> instances) {
    List<String> instanceIds =
        instances.stream().map(Instance::getInstanceId).collect(Collectors.toList());
    List<Instance> publicIpInstances = new ArrayList<>();
    DescribeInstancesRequest request = new DescribeInstancesRequest().withInstanceIds(instanceIds);
    DescribeInstancesResult result = this.client.describeInstances(request);
    List<Reservation> list = result.getReservations();

    for (Reservation res : list) {
      List<Instance> instanceList = res.getInstances();
      for (Instance instance : instanceList) {
        // Select only the instances marked as running.
        if (RUNNING.equalsIgnoreCase(instance.getState().getName())) {
          publicIpInstances.add(instance);
        }
      }
    }
    return publicIpInstances;
  }

  /**
   * Return a map between the instances and its public IP address.
   *
   * @param instances - A list of instances.
   * @return A Map between the instance and it's public IP address.
   */
  public Map<Instance, String> getInstancesPublicIp(List<Instance> instances) {
    Map<Instance, String> map = new HashMap<>();
    List<String> instanceIds =
        instances.stream().map(Instance::getInstanceId).collect(Collectors.toList());
    DescribeInstancesRequest request = new DescribeInstancesRequest().withInstanceIds(instanceIds);
    DescribeInstancesResult result = this.client.describeInstances(request);
    List<Reservation> list = result.getReservations();

    for (Reservation res : list) {
      List<Instance> instanceList = res.getInstances();
      for (Instance instance : instanceList) {
        // Select only the instances marked as running.
        if (RUNNING.equalsIgnoreCase(instance.getState().getName())) {
          map.put(instance, instance.getPublicIpAddress());
        }
      }
    }
    return map;
  }

  /**
   * Wait until a request is executed for the instance creation.
   *
   * @param instances A list of instances that we should wait until their status is 'running'
   * @throws InterruptedException If the thread is interrupted for whatever reason.
   */
  private void waitUntilRunning(List<Instance> instances) throws InterruptedException {

    List<String> instanceIds =
        instances.stream().map(Instance::getInstanceId).collect(Collectors.toList());
    int attempts = 0;

    while (true) {

      attempts++;
      // Sleep for 30 seconds.
      Thread.sleep(SLEEP_TIME);

      List<InstanceStatus> statuses = this.client
          .describeInstanceStatus(new DescribeInstanceStatusRequest().withInstanceIds(instanceIds))
          .getInstanceStatuses();

      if (statuses.size() < instanceIds.size()) {
        if (attempts > MAX_ATTEMPTS) { // roughly after 8 minutes
          throw new InterruptedException("waited too long to get instance status");
        }
        log.info("status is not synced, continue to wait");
        continue;
      }

      for (InstanceStatus status : statuses) {
        log.info(String.format("instance id: %s => %s, checks => %s, %s", status.getInstanceId(),
            status.getInstanceState().getName(), status.getSystemStatus().getStatus(),
            status.getInstanceStatus().getStatus()));
      }

      boolean allOk = statuses.stream()
          .allMatch(status -> RUNNING.equalsIgnoreCase(status.getInstanceState().getName())
              && OK.equalsIgnoreCase(status.getSystemStatus().getStatus())
              && OK.equalsIgnoreCase(status.getInstanceStatus().getStatus()));

      if (allOk) {
        break;
      }

      if (attempts > MAX_ATTEMPTS) { // roughly after 8 minutes
        throw new InterruptedException("waited too long to get instance status");
      }
      log.info("Waiting at attempt " + attempts);
    }
  }

  /**
   * Tag EC2 instances based on the tag name given by the user.
   *
   * @param instances - A list instances to be tagged.
   * @param tagname - The tag name to be added to the instances.
   */
  public void tagEC2Instances(List<Instance> instances, String tagname) {
    if (CollectionUtils.isNotEmpty(instances)) {
      if (instances.size() == 1) {
        CreateTagsRequest createTagsRequest = new CreateTagsRequest();
        createTagsRequest.withResources(instances.get(0).getInstanceId())
            .withTags(new Tag(TAG_NAME, tagname));
        this.client.createTags(createTagsRequest);
      } else {
        int index = 1;
        for (Instance instance : instances) {
          CreateTagsRequest createTagsRequest = new CreateTagsRequest();
          createTagsRequest.withResources(instance.getInstanceId())
              .withTags(new Tag(TAG_NAME, tagname + index));
          this.client.createTags(createTagsRequest);
          index++;
        }
      }
    }
  }

  /**
   * Verify if cloudos key is already imported to the region and import it if is not there yet.
   *
   * @param keyPairName - key name
   */
  private void verifyAndImportKey(String keyPairName) {
    Key key = new Key();
    if (!key.keyPairExists(keyPairName, client)) {
      key.saveKeyPair(keyPairName, client);
    }
  }

  /**
   * Enable monitoring for instances.
   *
   * @param instanceIds instances
   * @return List of InstanceMonitoring
   */
  public List<InstanceMonitoring> enableMonitoring(String... instanceIds) {
    MonitorInstancesRequest monitorInstancesRequest = new MonitorInstancesRequest();
    monitorInstancesRequest.withInstanceIds(instanceIds);
    MonitorInstancesResult result = this.client.monitorInstances(monitorInstancesRequest);
    return result.getInstanceMonitorings();
  }

  /**
   * Retrieve instance by provider id.
   *
   * @param providerId of instance
   * @return Instance object
   */
  public Instance getInstanceById(String providerId) {
    DescribeInstancesRequest describeInstancesRequest = new DescribeInstancesRequest();
    describeInstancesRequest.setInstanceIds(Arrays.asList(providerId));
    DescribeInstancesResult describeInstances = client.describeInstances(describeInstancesRequest);
    if (CollectionUtils.isNotEmpty(describeInstances.getReservations())) {
      return describeInstances.getReservations().get(0).getInstances().get(0);
    }
    return null;
  }

  /**
   * Clone an instance, copying the disk (creating snapshot).
   * 
   * @param instanceClonedName name of the new instance
   * @param instance to be cloned
   * @param machineType type of instance
   * @return new cloned instance
   * @throws Exception if something goes wrong
   */
  public Instance cloneInstance(String instanceClonedName, Instance instance, String machineType)
      throws Exception {

    DescribeVolumesRequest describeVolumesRequest = new DescribeVolumesRequest();
    describeVolumesRequest
        .withFilters(new Filter("attachment.instance-id").withValues(instance.getInstanceId()));
    DescribeVolumesResult describeVolumes = this.client.describeVolumes(describeVolumesRequest);
    List<Volume> volumes = describeVolumes.getVolumes();

    CreateSnapshotRequest createSnapshotRequest = new CreateSnapshotRequest();
    createSnapshotRequest.withVolumeId(volumes.get(0).getVolumeId());
    CreateSnapshotResult createSnapshot = this.client.createSnapshot(createSnapshotRequest);


    Boolean snapshotAvailable = false;
    while (!snapshotAvailable) {
      DescribeSnapshotsResult result = client.describeSnapshots(new DescribeSnapshotsRequest()
          .withSnapshotIds(createSnapshot.getSnapshot().getSnapshotId()));
      String status = (result.getSnapshots()).get(0).getState();
      if (status.equalsIgnoreCase("completed")) {
        snapshotAvailable = true;
      } else {
        log.info("Wating for snapshot to become available.");
        Thread.sleep(SLEEP_TIME_10);
      }
    }

    CreateImageRequest createImageRequest = new CreateImageRequest();
    createImageRequest.withInstanceId(instance.getInstanceId());
    createImageRequest.withName(instance.getInstanceId() + "clone");
    CreateImageResult createImage = this.client.createImage(createImageRequest);

    Boolean imageAvailable = false;
    while (!imageAvailable) {
      DescribeImagesResult describeImages = this.client
          .describeImages(new DescribeImagesRequest().withImageIds(createImage.getImageId()));
      String state = describeImages.getImages().get(0).getState();
      if (state.equalsIgnoreCase("available")) {
        imageAvailable = true;
      } else {
        log.info("Wating for AMI to become available.");
        Thread.sleep(SLEEP_TIME_10);
      }
    }

    RunInstancesRequest runInstancesRequest = new RunInstancesRequest();

    List<Tag> tags = instance.getTags();
    for (Tag tag : tags) {
      log.info("tags: {}-{}", tag.getKey(), tag.getValue());
      if (tag.getKey().equals(TAG_NAME)) {
        tag.setValue(instanceClonedName);
      }
      log.info("tags: {}-{}", tag.getKey(), tag.getValue());
    }
    runInstancesRequest
        .withImageId(createImage.getImageId()).withInstanceType(machineType).withMinCount(1)
        .withMaxCount(1).withKeyName(instance.getKeyName()).withSecurityGroupIds(instance
            .getSecurityGroups().stream().map(s -> s.getGroupId()).collect(Collectors.toList()))
        .withMonitoring(true)
        // withNetworkInterfaces(instance.getNetworkInterfaces());
        .withTagSpecifications(
            new TagSpecification().withResourceType(ResourceType.Instance).withTags(tags))
        .withPlacement(instance.getPlacement());

    RunInstancesResult runInstancesResult = this.client.runInstances(runInstancesRequest);

    List<Instance> instances = runInstancesResult.getReservation().getInstances();

    this.waitUntilRunning(instances);
    return this.waitForPublicIpAddresses(instances).get(0);
  }
}
