package cloudos.amazon;

import cloudos.Providers;
import cloudos.provider.ProviderMachineType;

public class AmazonMachineType extends ProviderMachineType {

  public AmazonMachineType() {
    setProvider(Providers.AMAZON_AWS);
  }
}
