package cloudos.amazon;

import cloudos.utils.FileUtils;
import com.google.gson.Gson;
import java.io.InputStreamReader;
import java.util.Map;
import lombok.extern.log4j.Log4j2;

/**
 * Fetch the URL for each one of the AWS Services. Mainly, it reads the data from the
 * PRICING_LIST_URL, populates the offers map and make those URLs available for a client to use.
 *
 * @author Philippe Ribeiro
 */
@Log4j2
public class AmazonPricingList {

  private static final String VERSION = "\"version\" :";
  private static final String PRICING_LIST_URL =
      "https://pricing.us-east-1.amazonaws.com/offers/v1.0/aws/index.json";
  private Map<String, Object> offers;
  protected Gson gson = new Gson();

  /**
   * Set a GET request to the given URl.
   *
   * @param url The url to be fetched
   * @throws Exception if something goes wrong
   */
  @SuppressWarnings({"cast", "unchecked"})
  protected Map<String, Object> sendGet(String url) throws Exception {
    InputStreamReader json = FileUtils.readUrlContentToReader(url);
    // I may want to refactor this, to return a Map
    return gson.fromJson(json, Map.class);
  }

  /**
   * Get version of the file without download all file.
   * 
   * @param url of the file
   * @return version
   * @throws Exception if something goes wrong
   */
  protected String getVersion(String url) throws Exception {
    String json = FileUtils.readPartialUrlContent(url, VERSION);
    return json.replaceAll(VERSION, "").replace('"', ' ').replace(',', ' ').trim();
  }


  /**
   * Returns a reference to the offers map.
   *
   * @return a Map containing the AWS offers
   * @throws Exception if something goes wrong
   */
  public Map<String, Object> getOffers() throws Exception {
    if (offers == null) {
      this.offers = this.sendGet(PRICING_LIST_URL);
    }
    return offers;
  }
}
