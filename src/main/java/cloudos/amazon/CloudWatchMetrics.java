package cloudos.amazon;

/**
 * Created by philipperibeiro on 3/28/17.
 *
 * <p>Define all the metrics currently supported by AWS's CloduWatch.
 */
public enum CloudWatchMetrics {
  CPU_UTILIZATION("CPUUtilization"),
  NETWORK_IN("NetworkIn"),
  NETWORK_OUT("NetworkOut"),
  DISK_READ_OPS("DiskReadOps"),
  DISK_WRITE_OPS("DiskWriteOps"),
  DISK_READ_BYTES("DiskReadBytes"),
  DISK_WRITE_BYTES("DiskWriteBytes"),
  NETWORK_PACKETS_IN("NetworkPacketsIn"),
  NETWORK_PACKETS_OUT("NetworkPacketsOut"),
  STATUS_CHECK_FAILED("StatusCheckFailed"),
  STATUS_CHECK_FAILED_INSTANCE("StatusCheckFailed_Instance"),
  STATUS_CHECK_FAILED_SYSTEM("StatusCheckFailed_System");

  private final String metric;

  /** @param metric */
  CloudWatchMetrics(String metric) {
    this.metric = metric;
  }

  /**
   * Find all the metrics by value
   *
   * @param value: Any metric value.
   * @return CloudWatchMetrics matching the value
   */
  public static CloudWatchMetrics getMetricByValue(String value) {
    for (CloudWatchMetrics metric : CloudWatchMetrics.values()) {
      if (metric.getMetric().equals(value)) return metric;
    }

    return null;
  }

  public String getMetric() {
    return this.metric;
  }

  @Override
  public String toString() {
    return this.metric;
  }
}
