package cloudos.amazon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import cloudos.machinelearning.MLUtils;
import cloudos.models.AbstractInstance;
import cloudos.models.Instance;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * Declare the general Instance class.
 * 
 * @Copyright CyberTextron Inc. 2016
 *
 * @license: private
 * @author Philippe Ribeiro
 * @date: 11/5/2016
 */
@Getter
@Setter
@AllArgsConstructor
@Builder
@Document(collection = "aws_instances")
@Log4j2
public class AmazonInstance extends AbstractInstance {

  @JsonSerialize(using = ToStringSerializer.class)
  private String sku;
  private String productFamily;
  private Map<String, Object> attributes;
  private boolean active;

  public static final ArrayList<String> features =
      new ArrayList<>(Arrays.asList("vcpu", "clockSpeed", "memory", "storage"));

  /**
   * Default constructor.
   *
   */
  public AmazonInstance() {
    super();
    this.sku = "";
    this.productFamily = "";
    this.attributes = new HashMap<>();
  }
  
  /**
   * Constructor for use @Builder and inheritance.
   */
  @Builder
  public AmazonInstance(String sku, String productFamily, Map<String, Object> attributes,
      boolean active, String id, Instance instance) {
    super(id, instance);
    this.sku = sku;
    this.productFamily = productFamily;
    this.attributes = attributes;
    this.active = active;
  }

  /**
   * Copy constructor.
   *
   * @param amazonInstance a AmazonInstance object to be copied
   */
  public AmazonInstance(final AmazonInstance amazonInstance) {
    super();
    this.setSku(amazonInstance.getSku());
    this.setProductFamily(amazonInstance.getProductFamily());
    this.setAttributes(amazonInstance.getAttributes());
    this.setId(amazonInstance.getId());
    this.setInstance(amazonInstance.getInstance());
  }

  /**
   * Given a AmazonInstance object, extract the feature vector from the object.
   *
   * @return a vector containing the value of each feature
   */
  // @Override
  public List<Double> getFeatureVector() {
    List<Double> featureVector = new ArrayList<>();

    for (String feature : features) {
      if (this.attributes.containsKey(feature)) {
        String value = (String) this.attributes.get(feature);
        double featureValue = this.extractFeatureValue(feature, value);
        featureVector.add(featureValue);
      } else {
        // add the default value
        featureVector.add(0.0);
      }
    }
    return featureVector;
  }

  /**
   * Given the features specified for the AmazonInstance, converts the string value of the feature
   * into a double value.
   *
   * @param feature the name of the feature
   * @param value the value of feature as a String
   * @return a double value representation of the feature
   */
  public double extractFeatureValue(String feature, String value) {
    double featureValue = 0;
    try {
      switch (feature) {
        case "vcpu":
        case "clockSpeed":
        case "memory":
          // @TODO: We need to add a provision for MiB, GiB and other expression
          featureValue = Double.parseDouble(value.replaceAll("[^0-9]", "").trim());
          break;
        case "storage":
          featureValue = MLUtils.getStorage(value);
          break;
        default:
          featureValue = 0;
      }
    } catch (NumberFormatException ex) {
      log.error(String.format("Exception for feature '%s' and value '%s'", feature, value));
      log.error(ex);
      featureValue = 0;
    }
    return featureValue;
  }
}
