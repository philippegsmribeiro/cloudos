package cloudos.amazon;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface AmazonInstanceRepository extends MongoRepository<AmazonInstance, String> {

  AmazonInstance findByInstance(cloudos.models.Instance instance);

  List<AmazonInstance> findBySku(String sku);

  List<AmazonInstance> findByProductFamily(String productFamily);

  /* Sometimes we want only to fetch a sample for testing */
  List<AmazonInstance> findTop100ByProductFamily(String productFamily, Pageable pageable);

  Long countByActive(boolean active);

  Long deleteByActive(boolean active);

  /* Attempt to find the instances by Attribute */
  @Query(value = "{ 'attributes.instanceType' : ?0 }")
  List<AmazonInstance> findByAttributesInstanceType(String instanceType);

  @Query(value = "{ 'attributes.instanceType' : ?0 }")
  AmazonInstance findOneByAttributesInstanceType(String instanceType);
}
