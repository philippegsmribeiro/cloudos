package cloudos.storage;

import cloudos.utils.TarArchive;
import cloudos.utils.ZipArchive;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
@Log4j2
public class FileSystemStorageService implements StorageService {

  @Value("${storage.file_upload_dir}")
  private String uploadDir;

  private Path rootLocation;
  private ZipArchive zipArchive;
  private TarArchive tarArchive;

  public FileSystemStorageService() {}

  @PostConstruct
  private void load() {
    this.rootLocation = Paths.get(uploadDir);
    this.zipArchive = new ZipArchive();
    this.tarArchive = new TarArchive();
  }

  @Override
  public Path load(String filename) {
    return rootLocation.resolve(filename);
  }

  @Override
  public void store(MultipartFile file, boolean overrideIfExists) {
    try {
      if (file.isEmpty()) {
        throw new StorageException("Failed to store empty file " + file.getOriginalFilename());
      }
      Path filePath = this.rootLocation.resolve(file.getOriginalFilename());
      if (Files.exists(filePath) && overrideIfExists) {
        Files.delete(filePath);
      }
      Files.copy(file.getInputStream(), filePath);
      /* Check if they are .tar or .zip format, and extract the */
      Path current = filePath.toAbsolutePath();
      String filename = current.getFileName().toString();
      String parent = current.getParent().toString();
      String fullpath = this.joinPaths(parent, filename);

      if (this.zipArchive.isZipFile(new File(fullpath))) {
        this.zipArchive.unzip(fullpath, parent);
      } else if (fullpath.endsWith(".tar") || fullpath.endsWith(".tgz")) {
        this.tarArchive.untarArchive(fullpath);
      }
    } catch (IOException e) {
      throw new StorageException("Failed to store file " + file.getOriginalFilename(), e);
    }
  }

  /**
   * Join paths.
   *
   * @param folder path
   * @param filename path
   * @return final path
   */
  private String joinPaths(String folder, String filename) {
    Path currentPath = Paths.get(folder);
    Path filePath = Paths.get(currentPath.toString(), filename);
    return filePath.toString();
  }

  @Override
  public Stream<Path> loadAll() {
    try {
      return Files.walk(this.rootLocation, 1)
          .filter(path -> !path.equals(this.rootLocation))
          .map(path -> this.rootLocation.relativize(path));
    } catch (IOException e) {
      throw new StorageException("Failed to read stored files", e);
    }
  }

  @Override
  public Resource loadAsResource(String filename) {
    try {
      Path file = load(filename);
      Resource resource = new UrlResource(file.toUri());
      if (resource.exists() || resource.isReadable()) {
        return resource;
      }
      throw new StorageFileNotFoundException("Could not read file: " + filename);
    } catch (MalformedURLException e) {
      throw new StorageFileNotFoundException("Could not read file: " + filename, e);
    }
  }

  @Override
  public void deleteAll() {
    FileSystemUtils.deleteRecursively(rootLocation.toFile());
  }

  @Override
  public void init() {
    if (!Files.exists(rootLocation)) {
      rootLocation.toFile().mkdirs();
    }
  }
}
