package cloudos.storage;

public class StorageFileNotFoundException extends StorageException {

  private static final long serialVersionUID = 2706403013815597695L;

  public StorageFileNotFoundException(String message) {
    super(message);
  }

  public StorageFileNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }
}
