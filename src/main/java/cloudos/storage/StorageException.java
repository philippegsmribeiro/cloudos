package cloudos.storage;

public class StorageException extends RuntimeException {

  private static final long serialVersionUID = 6395371127116183420L;

  public StorageException(String message) {
    super(message);
  }

  public StorageException(String message, Throwable cause) {
    super(message, cause);
  }
}
