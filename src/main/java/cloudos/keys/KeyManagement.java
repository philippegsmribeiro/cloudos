package cloudos.keys;

import cloudos.keys.cloudoskey.CloudosKey;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import org.apache.log4j.Logger;

/** Created by gleimar on 23/04/2017. */
public interface KeyManagement<T extends CloudosKey> {

  static final Logger logger = Logger.getLogger(KeyManagement.class);

  T generateKey(String keyName, String region) throws KeyManagementException;

  void deleteKey(String keyName, String region) throws KeyManagementException;

  /**
   * Save a key to a file.
   * 
   * @param keyName name
   * @param directoryPath path to be saved
   * @param fileExtension extension of the file
   * @param keyContent key content
   * @return Path for the key file
   */
  default Path generateKeyFile(String keyName, String directoryPath, String fileExtension,
      String keyContent) {

    Path directorySshKey = Paths.get(directoryPath);
    Path keyPath = null;

    try {
      if (!Files.exists(directorySshKey, LinkOption.NOFOLLOW_LINKS)) {
        Files.createDirectories(directorySshKey);
      }

      // delete the keys if they exists
      keyPath = Paths.get(directoryPath + File.separator + keyName + fileExtension);
      Files.deleteIfExists(keyPath);

      // recreate
      File sshFile = keyPath.toFile();

      if (!sshFile.exists()) {
        Files.write(sshFile.toPath(), keyContent.getBytes());
      }
    } catch (IOException e) {
      logger.error(e);
    }

    return keyPath;
  }

  Map.Entry<Path, Path> generateKeyPairFiles(T cloudosKey);

  CloudosKey findKey(String keyName, String region) throws KeyManagementException;
}
