package cloudos.keys;

import cloudos.Providers;
import cloudos.keys.cloudoskey.CloudosKey;
import cloudos.keys.cloudoskey.CloudosKeyService;
import cloudos.keys.cloudoskey.GoogleKey;
import cloudos.utils.KeyPairUtil;

import com.jcraft.jsch.KeyPair;

import java.nio.file.Path;
import java.util.AbstractMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/** Created by gleimar on 01/05/2017. */
@Service
public class GoogleKeyManagement implements KeyManagement<GoogleKey> {

  private static final int TYPE_RSA = KeyPair.RSA;
  private static final int LENGTH_RSA = 2048;

  @Resource
  CloudosKeyService cloudosKeyService;

  @Value("${google.sshDir}")
  private String dir;

  @Override
  public GoogleKey generateKey(String keyName, String region) {
    Map.Entry<String, String> keyPair =
        KeyPairUtil.generateKeyPair(TYPE_RSA, LENGTH_RSA, "google key by cloudos");
    GoogleKey googleKey = new GoogleKey();
    googleKey.setKeyName(keyName);
    googleKey.setRegion(region);
    googleKey.setPrivateKeyContent(keyPair.getKey());
    googleKey.setPublicKeyContent(keyPair.getValue());

    return googleKey;
  }

  @Override
  public void deleteKey(String keyName, String region) {}

  @Override
  public Map.Entry<Path, Path> generateKeyPairFiles(GoogleKey cloudosKey) {

    Path pathPrivateKey =
        this.generateKeyFile(cloudosKey.getKeyName(), dir, "", cloudosKey.getPrivateKeyContent());
    Path pathPublicKey = this.generateKeyFile(cloudosKey.getKeyName(), dir, ".pub",
        cloudosKey.getPublicKeyContent());

    return new AbstractMap.SimpleEntry<>(pathPrivateKey, pathPublicKey);
  }

  @Override
  public CloudosKey findKey(String keyName, String region) {
    CloudosKey key = this.cloudosKeyService.findActiveKeysByKeyNameAndProviderAndRegion(keyName,
        Providers.GOOGLE_COMPUTE_ENGINE, region);
    if (key == null) {
      key = this.cloudosKeyService.findActiveKeysByKeyNameAndProvider(keyName,
          Providers.GOOGLE_COMPUTE_ENGINE);
    }
    return key;
  }
}
