package cloudos.keys;

/** Created by gleimar on 01/05/2017. */
public class KeyManagerServiceException extends Exception {

  private static final long serialVersionUID = 3974416993894042911L;

  public KeyManagerServiceException(String message) {
    super(message);
  }

  public KeyManagerServiceException(String message, Throwable cause) {
    super(message, cause);
  }
}
