package cloudos.keys;

import cloudos.AmazonService;
import cloudos.Providers;
import cloudos.amazon.Key;
import cloudos.keys.cloudoskey.AmazonKey;
import cloudos.keys.cloudoskey.CloudosKey;
import cloudos.keys.cloudoskey.CloudosKeyService;
import cloudos.provider.ProviderKey;
import cloudos.provider.ProvidersService;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.ec2.AmazonEC2;
import java.io.File;
import java.nio.file.Path;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Created by gleimar on 23/04/2017. */
@Service
@Log4j2
public class AmazonKeyManagement implements KeyManagement<AmazonKey> {

  private static final String REGION_KEY_MAP = "%s$%s";

  @Autowired
  private AmazonService amazonService;

  @Autowired
  private CloudosKeyService cloudosKeyService;

  @Autowired
  private ProvidersService providersService;

  private Key key;

  public AmazonKeyManagement() {
    this.key = new Key();
  }

  @Override
  public AmazonKey generateKey(String keyName, String region) throws KeyManagementException {

    AmazonEC2 amazonClient = this.getAmazonClient(region);

    String privateKey;

    try {
      privateKey = key.getPrivateKey(keyName, amazonClient);
    } catch (AmazonServiceException e) {
      throw new KeyManagementException(
          String.format(
              "There is a problem in Amazon when creates a new key. Details:%s",
              e.getLocalizedMessage()), e);
    }

    if (StringUtils.isBlank(privateKey)) {
      return null;
    }

    CloudosKey amazonKey = new AmazonKey();
    amazonKey.setRegion(region);
    amazonKey.setKeyName(keyName);
    amazonKey.setPrivateKeyContent(privateKey);

    CloudosKey savedKey =  cloudosKeyService.save(amazonKey);
    
    try {
      // updates the provider key list
      providersService.reloadKeys(Providers.AMAZON_AWS, savedKey);
    } catch (Exception e) {
      log.error("Error updating provider key list." , e);
    }
    
    return (AmazonKey) savedKey;
  }

  private AmazonEC2 getAmazonClient(String region) throws KeyManagementException {
    AmazonEC2 awsClient = amazonService.getClient(region);

    if (awsClient == null) {
      throw new KeyManagementException(
          String.format("No Amazon Client obtained by the region informed. Region: %s", region));
    }

    return awsClient;
  }

  @Override
  public void deleteKey(String keyName, String region) throws KeyManagementException {

    AmazonEC2 amazonClient = this.getAmazonClient(region);

    try {
      key.deleteKey(keyName, amazonClient);
    } catch (Throwable e) {
      throw new KeyManagementException("Error delete key", e);
    }
  }

  @Override
  public Map.Entry<Path, Path> generateKeyPairFiles(AmazonKey cloudosKey) {
    String userHome = System.getProperty("user.home");

    Path pathPrivateKey =
        this.generateKeyFile(
            cloudosKey.getKeyName(),
            userHome + File.separator,
            "",
            cloudosKey.getPrivateKeyContent());

    return new AbstractMap.SimpleEntry<>(pathPrivateKey, null);
  }

  @Override
  public CloudosKey findKey(String keyName, String region) throws KeyManagementException {
    try {
      CloudosKey key = this.cloudosKeyService.findActiveKeysByKeyNameAndProviderAndRegion(keyName,
          Providers.AMAZON_AWS, region);
      if (key == null) {
        key = this.cloudosKeyService.findActiveKeysByKeyNameAndProvider(keyName,
            Providers.AMAZON_AWS);
      }
      if (key == null && this.key.keyPairExists(keyName, getAmazonClient(region))) {
        // if key exist on the provider but not in the cloudos - save it
        CloudosKey amazonKey = new AmazonKey();
        amazonKey.setRegion(region);
        amazonKey.setKeyName(keyName);
        amazonKey.setAddedBySync(true);
        key = this.cloudosKeyService.save(amazonKey);
      }
      return key;
    } catch (Throwable e) {
      throw new KeyManagementException("Error finding key", e);
    }
  }

  /**
   * Verify on the provider if key exists.
   * @param keyName name of the key
   * @param region region of the key
   * @return true if exists, false otherwise
   * @throws KeyManagementException if something goes wrong
   */
  public boolean verifyKeyExists(String keyName, String region) throws KeyManagementException {
    return key.keyPairExists(keyName, getAmazonClient(region));
  }

  /**
   * Classify the provider key elements (to be used on the deployments, etc).
   * Set enable/disable according to private key presence.
   * @param providerKeys list of provider key elements
   * @return updated list
   */
  public List<ProviderKey> classifyAccordingPrivateKey(List<ProviderKey> providerKeys) {
    final List<CloudosKey> cloudosKeys =
        cloudosKeyService.findActiveKeysByProvider(Providers.AMAZON_AWS);
    Map<String, CloudosKey> table = new HashMap<>();
    cloudosKeys.forEach(cloudosKey -> {
      table.put(String.format(REGION_KEY_MAP, cloudosKey.getRegion(), cloudosKey.getKeyName()),
          cloudosKey);
    });

    for (ProviderKey providerKey : providerKeys) {
      providerKey.setEnabled(false);
      // verify if cloudos has the key
      CloudosKey key =
          table.get(String.format(REGION_KEY_MAP, providerKey.getRegion(), providerKey.getKey()));
      if (key != null) {
        // if contains
        // set it enable if key is registred on the cloudos - private key is needed to run
        // the deploy
        if (StringUtils.isNotBlank(key.getPrivateKeyContent())) {
          providerKey.setDescription(providerKey.getKey());
          providerKey.setEnabled(true);
        } else {
          providerKey.setDescription(
              providerKey.getKey() + " [key on cloudOS, but not private key found!]");
        }
      } else {
        providerKey.setDescription(providerKey.getKey() + " [key not imported to cloudOS!]");
      }
    }
    return providerKeys;
  }

  /**
   * Update the list of cloudos key with the list on the provider.
   * 
   * @param providerKeyList list from the provider
   */
  public void updateKeys(List<ProviderKey> providerKeyList) {
    providerKeyList.forEach(key -> {
      try {
        this.findKey(key.getKey(), key.getRegion());
      } catch (KeyManagementException e) {
        log.error("Error syncing keys!", e);
      }
    });
  }

}
