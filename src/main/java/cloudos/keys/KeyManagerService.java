package cloudos.keys;

import cloudos.ItemKeyRegionGrouped;
import cloudos.Providers;
import cloudos.exceptions.NotImplementedException;
import cloudos.keys.cloudoskey.CloudosKey;
import cloudos.keys.cloudoskey.CloudosKeyService;
import cloudos.queue.QueueManagerService;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Created by gleimar on 23/04/2017. */
@Service
public class KeyManagerService {

  @Autowired
  private CloudosKeyService cloudosKeyService;

  @Autowired
  private AmazonKeyManagement amazonKeyManagement;

  @Autowired
  private GoogleKeyManagement googleKeyManagement;

  @Autowired
  private QueueManagerService queueManagerService;

  @SuppressWarnings("rawtypes")
  private KeyManagement getManagement(Providers providers) throws NotImplementedException {
    switch (providers) {
      case AMAZON_AWS:
        return amazonKeyManagement;
      case GOOGLE_COMPUTE_ENGINE:
        return googleKeyManagement;
      default:
        break;
    }
    throw new NotImplementedException();
  }

  /**
   * Checks if the keyName exists in cloudos database.
   * @param keyName name of key
   * @param provider key provider
   * @param region key region
   * @return cloudosKey
   * @throws NotImplementedException if provider not implemented yet
   * @throws KeyManagementException if something goes wrong
   */
  public CloudosKey findKey(String keyName, Providers provider, String region)
      throws NotImplementedException, KeyManagementException {
    return getManagement(provider).findKey(keyName, region);
  }
  
  /**
   * Retrieve all key by region.
   * @param region to filter
   * @return list of cloudoskey
   */
  public List<CloudosKey> findKey(String region) {
    return this.cloudosKeyService.findActiveKeysByRegion(region);
  }

  /**
   * Retrieve all keys from provider.
   *
   * @param provider to filter
   * @return list of cloudoskey
   */
  public List<CloudosKey> findKeyByProvider(Providers provider) {
    return this.cloudosKeyService.findActiveKeysByProvider(provider);
  }

  /**
   * Gets a list of keys per region.
   * 
   * @param provider to filter
   * @return List of (name,keys), where name-> is the region and keys-> is a list of key in this
   *         region
   */
  public List<ItemKeyRegionGrouped> findKeyByProviderGroupedByRegion(Providers provider) {

    List<CloudosKey> cloudosKeyList =
        this.cloudosKeyService.findActiveKeysByProvider(provider);

    List<ItemKeyRegionGrouped> keysGrouped = new ArrayList<>();

    for (CloudosKey cloudosKey : cloudosKeyList) {

      ItemKeyRegionGrouped keyGrouped = new ItemKeyRegionGrouped(cloudosKey.getRegion());

      int indexOf = keysGrouped.indexOf(keyGrouped);

      if (indexOf != -1) {
        keyGrouped = keysGrouped.get(indexOf);
      }

      keyGrouped.add(cloudosKey);

      if (indexOf == -1) {
        keysGrouped.add(keyGrouped);
      }
    }

    return keysGrouped;
  }

  /**
   * Deletes the key from cloudos db and cloud provider if necessary.
   * If the key entered does not exists, returns null. Otherwise, return the cloudosKey deleted.
   * @param keyName name
   * @param provider of key
   * @param region of key
   * @return cloudoskey
   * @throws NotImplementedException if provider not implemented yet
   * @throws KeyManagementException if something goes wrong
   */
  public CloudosKey deleteKey(String keyName, Providers provider, String region)
      throws NotImplementedException, KeyManagerServiceException, KeyManagementException {

    CloudosKey cloudosKey = this.findKey(keyName, provider, region);

    if (cloudosKey == null) {
      return null;
    }

    try {
      this.getManagement(provider).deleteKey(keyName, region);
    } catch (KeyManagementException e) {
      throw new KeyManagerServiceException(e.getLocalizedMessage(), e);
    }

    this.cloudosKeyService.delete(cloudosKey);

    return cloudosKey;
  }

  /**
   * Delete a key by the id of the cloudosKey.
   * 
   * @param cloudosKeyId id
   * @return cloudoskey
   * @throws NotImplementedException if provider not implemented yet
   * @throws KeyManagementException if something goes wrong
   */
  public CloudosKey deleteKey(String cloudosKeyId)
      throws NotImplementedException, KeyManagerServiceException {

    CloudosKey cloudosKey = this.cloudosKeyService.findOne(cloudosKeyId);

    if (cloudosKey == null) {
      throw new KeyManagerServiceException("The entered key does not exist");
    }

    try {
      this.getManagement(cloudosKey.getProvider()).deleteKey(cloudosKey.getKeyName(),
          cloudosKey.getRegion());
    } catch (KeyManagementException e) {
      throw new KeyManagerServiceException(e.getLocalizedMessage(), e);
    }

    this.cloudosKeyService.delete(cloudosKey);

    return cloudosKey;
  }

  /**
   * Create the key in the cloud provider and then stores the key in database.
   * If the key exits, throw a exception.
   * @param keyName - The key name
   * @param provider - The cloud provider (aws, azure, google, etc)
   * @param region - The region by the cloud provider
   * @return cloudoskey
   * @throws NotImplementedException if provider not implemented yet
   * @throws KeyManagementException if something goes wrong
   */
  public CloudosKey generateKey(String keyName, Providers provider, String region)
      throws NotImplementedException, KeyManagerServiceException {

    CloudosKey cloudosKey = this.cloudosKeyService
        .findActiveKeysByKeyNameAndProviderAndRegion(keyName, provider, region);

    if (cloudosKey != null) {
      throw new KeyManagerServiceException("The key already exists.");
    }

    try {
      cloudosKey = this.getManagement(provider).generateKey(keyName, region);
    } catch (KeyManagementException e) {
      throw new KeyManagerServiceException(e.getLocalizedMessage(), e);
    }

    if (cloudosKey == null) {
      return null;
    }

    return this.cloudosKeyService.save(cloudosKey);
  }

  /**
   * Generate key pair with the private and public key.
   *
   * @param cloudosKey key
   * @return
   * @throws NotImplementedException if provider not implemented 
   */
  public Map.Entry<Path, Path> generateKeyPairFiles(CloudosKey cloudosKey)
      throws NotImplementedException {

    Map.Entry<Path, Path> keyPairFiles =
        this.getManagement(cloudosKey.getProvider()).generateKeyPairFiles(cloudosKey);

    return keyPairFiles;
  }

  /**
   * Save a private key for a cloudOs key.
   * 
   * @param cloudosKey - with the private key
   * @return saved cloudoskey
   */
  public CloudosKey savePrivateKey(CloudosKey cloudosKey) {
    CloudosKey savedKey = cloudosKeyService.findOne(cloudosKey.getId());
    if (savedKey != null) {
      if (StringUtils.isNotEmpty(cloudosKey.getPublicKeyContent())) {
        savedKey.setPublicKeyContent(cloudosKey.getPublicKeyContent());
      }
      if (StringUtils.isNotEmpty(cloudosKey.getPrivateKeyContent())) {
        savedKey.setPrivateKeyContent(cloudosKey.getPrivateKeyContent());
      }
      savedKey = cloudosKeyService.save(savedKey);
      queueManagerService.sendInstanceKeySaved(savedKey);
      return savedKey;
    }
    return null;
  }
}
