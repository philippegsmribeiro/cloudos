package cloudos.keys.cloudoskey;

import cloudos.Providers;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/** Created by gleimar on 29/04/2017. */
interface CloudosKeyRepository extends MongoRepository<CloudosKey, String> {

  @Query(value = "{ keyName : ?0 , provider : ?1 , region : ?2 , deleted: {$exists : ?3} }")
  CloudosKey findByKeyNameAndProviderAndRegionAndDeleted(
      String keyName, Providers provider, String region, boolean deleted);

  @Query(value = "{ provider : ?0 , deleted: {$exists : ?1} }")
  List<CloudosKey> findByProviderAndDeleted(Providers provider, boolean deleted);

  @Query(value = "{ keyName : ?0 , provider : ?1 , deleted: {$exists : ?2} }")
  CloudosKey findByKeyNameAndProviderAndDeleted(
      String keyName, Providers provider, boolean deleted);

  @Query(value = "{ region : ?0 , deleted: {$exists : ?1} }")
  List<CloudosKey> findByRegionAndDeleted(String region, boolean deleted);
}
