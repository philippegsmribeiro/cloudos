package cloudos.keys.cloudoskey;

import cloudos.Providers;

/** Created by gleimar on 01/05/2017. */
public class GoogleKey extends CloudosKey {

  public GoogleKey() {
    this.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
  }
}
