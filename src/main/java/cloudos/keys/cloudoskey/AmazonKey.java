package cloudos.keys.cloudoskey;

import cloudos.Providers;

/** Created by gleimar on 01/05/2017. */
public class AmazonKey extends CloudosKey {
  public AmazonKey() {
    this.setProvider(Providers.AMAZON_AWS);
  }
}
