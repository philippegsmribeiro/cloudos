package cloudos.keys.cloudoskey;

import cloudos.Providers;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/** Created by gleimar on 29/04/2017. */
@Document(collection = "cloudos_key")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "type",
    defaultImpl = AmazonKey.class
)
@JsonSubTypes({
    @JsonSubTypes.Type(value = AmazonKey.class, name = "AmazonKey"),
    @JsonSubTypes.Type(value = GoogleKey.class, name = "GoogleKey")
})
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CloudosKey implements Serializable {
  private static final long serialVersionUID = -3849386139594342706L;
  @Id
  private String id;
  private String keyName;
  private String privateKeyContent;
  private Providers provider;
  private String region;
  private Date created;
  private Date deleted;
  private Boolean addedBySync;
  private String publicKeyContent;

}
