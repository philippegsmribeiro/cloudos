package cloudos.keys.cloudoskey;

import cloudos.Providers;
import cloudos.google.GoogleKeyGenerator;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CloudosKeyService {

  @Autowired
  private CloudosKeyRepository cloudosKeyRepository;

  @Value("${google.sshkey}")
  private String key;

  @Value("${google.sshPubKey}")
  private String pubkey;

  /**
   * Generate default keys to be used on the application.
   */
  public void setUpDefaultKeys() {
    log.info("# Validating default keys:");
    CloudosKey googleKey = cloudosKeyRepository.findByKeyNameAndProviderAndDeleted(
        GoogleKeyGenerator.keyName, Providers.GOOGLE_COMPUTE_ENGINE, false);
    if (googleKey == null) {
      googleKey = new GoogleKey();
      googleKey.setKeyName(GoogleKeyGenerator.keyName);
      googleKey.setRegion("");
      googleKey.setPrivateKeyContent(key);
      googleKey.setPublicKeyContent(pubkey);
      log.info(" - Creating googlekey: {}.", googleKey.getKeyName());
      cloudosKeyRepository.save(googleKey);
    } else {
      log.info(" - Googlekey: {} exists.", googleKey.getKeyName());
    }
  }

  /**
   * Find an active key by name and provider.
   * 
   * @param keyname name
   * @param provider of the key
   * @return key
   */
  public CloudosKey findActiveKeysByKeyNameAndProvider(String keyname, Providers provider) {
    return cloudosKeyRepository.findByKeyNameAndProviderAndDeleted(keyname, provider, false);
  }

  /**
   * Save a key.
   * 
   * @param cloudosKey to be saved
   * @return saved key
   */
  public CloudosKey save(CloudosKey cloudosKey) {
    return cloudosKeyRepository.save(cloudosKey);
  }

  /**
   * Retrieve all active by provider.
   * 
   * @param provider to filter
   * @return list of keys
   */
  public List<CloudosKey> findActiveKeysByProvider(Providers provider) {
    return cloudosKeyRepository.findByProviderAndDeleted(provider, false);
  }

  /**
   * Delete a key.
   * 
   * @param cloudosKey to be deleted
   */
  public void delete(CloudosKey cloudosKey) {
    cloudosKeyRepository.delete(cloudosKey);
  }

  /**
   * Retrieve by Id.
   * 
   * @param id of the key
   * @return cloudosKey
   */
  public CloudosKey findOne(String id) {
    return cloudosKeyRepository.findOne(id);
  }

  /**
   * Retrieve a active keys by name, provider and region.
   * 
   * @param keyName name of the key
   * @param provider of the key
   * @param region of the key
   * @return CloudosKey
   */
  public CloudosKey findActiveKeysByKeyNameAndProviderAndRegion(String keyName, Providers provider,
      String region) {
    return cloudosKeyRepository.findByKeyNameAndProviderAndRegionAndDeleted(keyName, provider,
        region, false);
  }

  /**
   * Retrieve all active keys by region.
   * 
   * @param region of the key
   * @return List of CloudosKey
   */
  public List<CloudosKey> findActiveKeysByRegion(String region) {
    return cloudosKeyRepository.findByRegionAndDeleted(region, false);
  }

  /**
   * Retrieve a deleted keys by name, provider and region.
   * 
   * @param keyName name of the key
   * @param provider of the key
   * @param region of the key
   * @return CloudosKey
   */
  public CloudosKey findDeletedKeyByKeyNameAndProviderAndRegion(String keyName, Providers provider,
      String region) {
    return cloudosKeyRepository.findByKeyNameAndProviderAndRegionAndDeleted(keyName, provider,
        region, true);
  }

}
