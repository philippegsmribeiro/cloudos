package cloudos.keys;

/** Created by gleimar on 27/05/2017. */
public class KeyManagementException extends Exception {

  private static final long serialVersionUID = -2632676855590136961L;

  public KeyManagementException(String message) {
    super(message);
  }

  public KeyManagementException(String message, Throwable cause) {
    super(message, cause);
  }
}
