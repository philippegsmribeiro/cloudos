package cloudos;

import cloudos.credentials.AmazonCredentialManagement;
import cloudos.credentials.AzureCredentialManagement;
import cloudos.credentials.CloudCredentialActionException;
import cloudos.credentials.CloudCredentialActionRequest;
import cloudos.credentials.CredentialManagement;
import cloudos.credentials.GoogleCredentialManagement;
import cloudos.encryptionKeys.AmazonKMSServiceException;
import cloudos.exceptions.NotImplementedException;
import cloudos.google.GoogleCredential;
import cloudos.models.AwsCredential;
import cloudos.models.AzureCredential;
import cloudos.models.CloudosCredential;
import cloudos.models.CloudosCredentialRepository;
import cloudos.queue.QueueManagerService;
import cloudos.storage.StorageService;
import cloudos.utils.FileUtils;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/** Created by gleimar on 20/05/2017. */
@Log4j2
@Service
public class CredentialManagerService {

  @Value("${google.credentials.project}")
  private String googleProject;

  @Value("${google.credentials.json}")
  private String json;

  @Value("${aws_access_key_id}")
  private String awsAccessKey;

  @Value("${aws_secret_key}")
  private String awsSecretKey;

  @Value("${azure.client}")
  private String azureClient;

  @Value("${azure.tenant}")
  private String azureTenant;

  @Value("${azure.key}")
  private String azureKey;
  
  @Autowired
  private AmazonCredentialManagement amazonCredentialManagement;

  @Autowired
  private AzureCredentialManagement azureCredentialManagement;

  @Autowired
  private GoogleCredentialManagement googleCredentialManagement;

  @Autowired
  private CloudosCredentialRepository cloudosCredentialRepository;

  @Autowired
  private QueueManagerService queueManagerService;
  
  @Autowired
  private StorageService storageService;

  private CredentialManagement<?> getManagement(Providers providers)
      throws NotImplementedException {
    switch (providers) {
      case AMAZON_AWS:
        return amazonCredentialManagement;
      case GOOGLE_COMPUTE_ENGINE:
        return googleCredentialManagement;
      case MICROSOFT_AZURE:
        return azureCredentialManagement;
      default:
        break;
    }
    throw new NotImplementedException();
  }

  /**
   * Retrieve a list of credentials
   *
   * @param providers
   * @return
   * @throws NotImplementedException
   */
  public List<CloudosCredential> list(Providers providers) throws NotImplementedException {
    if (providers != null) {
      return cloudosCredentialRepository.findByProvider(providers);
    }
    return cloudosCredentialRepository.findAll();
  }

  /**
   * Save a credential
   *
   * @param cloudCreateCredentialRequest
   * @return
   * @throws NotImplementedException
   * @throws CloudCredentialActionException
   */
  @SuppressWarnings("unchecked")
  public <T extends CloudosCredential> T save(
      CloudCredentialActionRequest cloudCreateCredentialRequest)
      throws NotImplementedException, CloudCredentialActionException {
    T cloudosCredential = (T) cloudCreateCredentialRequest.getCloudosCredential();
    if (cloudosCredential == null) {
      throw new CloudCredentialActionException("No cloudos credential entered.");
    }
    
    // That is considering that we have just one credential for provider
    // TODO: allow multiple credentials

    CredentialManagement<T> management =
        (CredentialManagement<T>) this.getManagement(cloudosCredential.getProvider());
    
    management.loadConfigFile(cloudosCredential);

    if (!management.validateCredential(cloudosCredential)) {
      throw new CloudCredentialActionException("Bad credentials!");
    }

    // saved credential
    T savedCredential = null;

    // get the credential
    List<CloudosCredential> findByProvider =
        cloudosCredentialRepository.findByProvider(cloudosCredential.getProvider());
    if (CollectionUtils.isNotEmpty(findByProvider)) {
      // if credential already exists

      boolean same = false;
      if (findByProvider.get(0).getId().equals(cloudosCredential.getId())) {
        // if is the same, just update
        same = true;
      }

      // save the new one
      savedCredential = (T) management.save(cloudosCredential);

      if (!same) {
        // delete the other one
        cloudosCredentialRepository.delete(findByProvider);
      }

    } else {

      // there is no credential for the provider

      // save the new one
      savedCredential = (T) management.save(cloudosCredential);
    }

    if (savedCredential != null) {

      decryptSensitiveData(savedCredential);

      // first credential setup
      management.updateActiveCredential(savedCredential);
      // fire a queue message
      queueManagerService.sendCredentialChangeMessage(savedCredential);

      encryptSensitiveData(savedCredential);
    }
    return savedCredential;
  }

  /**
   * Auxilary method to re-set the credentials base on the properties files. It should be call just
   * in tests
   *
   * @throws Exception if something goes wrong
   */
  public void setDefaultCredentials() throws Exception {
    // delete the credentials and set the ones from the properties
    this.cloudosCredentialRepository.deleteAll();

    log.info("# AWS credential:");
    AwsCredential awsCredentials = new AwsCredential(awsAccessKey, awsSecretKey);
    awsCredentials = cloudosCredentialRepository.save(awsCredentials);
    log.info("Adding credential: {}.", awsAccessKey);

    log.info("# Azure credential:");
    AzureCredential azureCredential = new AzureCredential(azureClient, azureTenant, azureKey);
    azureCredential = cloudosCredentialRepository.save(azureCredential);
    log.info("Adding credential: {}.", azureClient);

    log.info("# Gcloud credential:");
    byte[] readAllBytes = Files.readAllBytes(Paths
        .get(FileUtils.getFileFromResourcesFile(json, this.getClass().getClassLoader()).toURI()));
    GoogleCredential googleCredential =
        new GoogleCredential(googleProject, new String(readAllBytes));

    googleCredential = cloudosCredentialRepository.save(googleCredential);
    log.info("Adding credential: {}.", googleProject);


    // validate them
    List<String> credentialsOnError = new ArrayList<>();
    decryptSensitiveData(awsCredentials);
    if (!amazonCredentialManagement.validateCredential(awsCredentials)) {
      credentialsOnError.add(String.format("AWS[%s]", awsCredentials.getAccessKeyId()));
    }

    decryptSensitiveData(azureCredential);
    if (!azureCredentialManagement.validateCredential(azureCredential)) {
      credentialsOnError.add(String.format("AZURE[%s]", azureCredential.getClient()));
    }

    decryptSensitiveData(googleCredential);
    if (!googleCredentialManagement.validateCredential(googleCredential)) {
      credentialsOnError.add(String.format("GCLOUD[%s]", googleCredential.getProject()));
    }
    if (CollectionUtils.isNotEmpty(credentialsOnError)) {
      throw new CloudCredentialActionException(String.format("Credentials %s not valid!",
          (Arrays.toString(credentialsOnError.toArray()))));
    }
  }

  /**
   * Delete a credential
   *
   * @param request
   * @return
   * @throws NotImplementedException
   * @throws CloudCredentialActionException
   * @throws AmazonKMSServiceException
   */
  @SuppressWarnings("unchecked")
  public <T extends CloudosCredential> boolean delete(CloudCredentialActionRequest request)
      throws NotImplementedException, CloudCredentialActionException, AmazonKMSServiceException {
    T cloudosCredential = (T) request.getCloudosCredential();
    if (cloudosCredential == null || StringUtils.isBlank(cloudosCredential.getId())) {
      throw new CloudCredentialActionException("No cloudos credential entered.");
    }

    // That is considering that we have just one credential for provider
    // TODO: allow multiple credentials

    CloudosCredential findOne = cloudosCredentialRepository.findOne(cloudosCredential.getId());
    if (findOne == null) {
      throw new CloudCredentialActionException("Credential not found!");
    }

    // delete the other one
    cloudosCredentialRepository.delete(findOne);

    CredentialManagement<T> management =
        (CredentialManagement<T>) this.getManagement(cloudosCredential.getProvider());

    // update the active to null - disable the provider
    management.updateActiveCredential(null);

    return true;
  }

  @SuppressWarnings("unchecked")
  public <T extends CloudosCredential> T decryptSensitiveData(CloudosCredential genericCredential)
      throws NotImplementedException, CloudCredentialActionException {
    T cloudosCredential = (T) genericCredential;
    if (cloudosCredential == null) {
      throw new CloudCredentialActionException("No cloudos credential entered.");
    }

    CredentialManagement<T> management =
        (CredentialManagement<T>) this.getManagement(cloudosCredential.getProvider());

    return management.decryptSensitiveData(cloudosCredential);
  }

  @SuppressWarnings("unchecked")
  public <T extends CloudosCredential> T encryptSensitiveData(CloudosCredential genericCredential)
      throws NotImplementedException, CloudCredentialActionException {
    T cloudosCredential = (T) genericCredential;
    if (cloudosCredential == null) {
      throw new CloudCredentialActionException("No cloudos credential entered.");
    }

    CredentialManagement<T> management =
        (CredentialManagement<T>) this.getManagement(cloudosCredential.getProvider());

    return management.encryptSensitiveData(cloudosCredential);
  }
  
  /**
   * Checks if there are credentials on database, returning true or false.
   * 
   * @return true if it has credentials, false otherwise.
   */
  public boolean hasCredentials(){
    return !cloudosCredentialRepository.findAll().isEmpty();
  }
}
