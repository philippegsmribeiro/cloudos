package cloudos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Collection to store the encrypted Amazon data keys generated when encrypting sensitive
 * user/credential data.
 * 
 * @author Rogério Souza
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "amazon_data_key")
public class AmazonDataKey {

  @Id
  private String id;

  @NotEmpty
  private String alias;

  @NotEmpty
  private byte[] encryptedKey;

}
