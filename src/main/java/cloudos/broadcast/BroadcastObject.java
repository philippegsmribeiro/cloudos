package cloudos.broadcast;

import cloudos.config.CloudOSJson;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Object sent on a server broadcast. */
@CloudOSJson
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BroadcastObject {

  private BroadcastType type;
  private String message;
  private Object object;
  private String objectClassName;
  private String sqsReceipt;

  /**
   * Constructor.
   *
   * @param type of broadcasting
   * @param message to be broadcasted
   * @param object to be broadcasted
   */
  public BroadcastObject(BroadcastType type, String message, Object object) {
    super();
    this.setType(type);
    this.setMessage(message);
    this.setObject(object);
  }

  /**
   * Set the object.
   *
   * @param object to be broadcasted
   */
  public void setObject(Object object) {
    this.object = object;
    if (object != null) {
      this.setObjectClassName(object.getClass().getName());
    }
  }
}
