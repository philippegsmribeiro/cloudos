package cloudos.broadcast;

public enum BroadcastType {

  /** */
  DEPLOYMENTS,
  /** */
  NOTIFICATION,
  /** */
  INSTANCE_UPDATED,
  /** */
  INSTANCE_CREATED,
  /** */
  INSTANCE_DELETED,
  /** */
  BEAT
}
