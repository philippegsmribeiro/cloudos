package cloudos.discovery;

import cloudos.Providers;
import cloudos.exceptions.NotImplementedException;
import cloudos.instances.AmazonInstanceManagement;
import cloudos.instances.GoogleInstanceManagement;
import cloudos.instances.InstanceManagement;
import cloudos.models.CloudosCredential;
import cloudos.provider.AmazonProviderManagement;
import cloudos.provider.GoogleProviderManagement;
import cloudos.provider.ProviderManagement;
import cloudos.queue.QueueListener;
import cloudos.queue.message.CredentialChangeMessage.CredentialChangeMessageConsumer;
import javax.annotation.PostConstruct;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Discovery Service.
 *
 * @author flavio
 */
@Service
@Log4j2
public class DiscoveryService {

  @Autowired
  QueueListener queueListener;
  @Autowired
  GoogleInstanceManagement googleInstanceManagement;
  @Autowired
  AmazonInstanceManagement amazonInstanceManagement;
  @Autowired
  GoogleProviderManagement googleProviderManagement;
  @Autowired
  AmazonProviderManagement amazonProviderManagement;

  @PostConstruct
  private void setUp() {
    // add the consumer for credential changes
    queueListener.addConsumer(
        (CredentialChangeMessageConsumer) message -> executeDiscovery(message.getCredential()));
  }

  /**
   * Execute the discovery of the resources on the account.
   *
   * @param cloudosCredential credential
   */
  public void executeDiscovery(CloudosCredential cloudosCredential) {
    try {
      // provider elements (keys, etcs)
      getProviderManagement(cloudosCredential.getProvider()).discovery();
      // instances
      getInstanceManagement(cloudosCredential.getProvider()).discovery();
      //
    } catch (Exception e) {
      log.error("Error discovery instances!");
    }
  }

  /**
   * Get management class.
   *
   * @param providers provider
   * @return Instance Management
   * @throws NotImplementedException if not implemented yet
   */
  private InstanceManagement<?> getInstanceManagement(Providers providers)
      throws NotImplementedException {
    switch (providers) {
      case AMAZON_AWS:
        return amazonInstanceManagement;
      case GOOGLE_COMPUTE_ENGINE:
        return googleInstanceManagement;
      default:
        break;
    }
    throw new NotImplementedException();
  }

  /**
   * Get management class.
   *
   * @param providers provider
   * @return Provider Management
   * @throws NotImplementedException if not implemented
   */
  private ProviderManagement getProviderManagement(Providers providers)
      throws NotImplementedException {
    switch (providers) {
      case AMAZON_AWS:
        return amazonProviderManagement;
      case GOOGLE_COMPUTE_ENGINE:
        return googleProviderManagement;
      default:
        break;
    }
    throw new NotImplementedException();
  }
}
