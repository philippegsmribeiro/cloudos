package cloudos;

import cloudos.models.CloudosResponse;
import cloudos.models.services.CloudosService;
import cloudos.models.services.ServiceException;
import cloudos.models.services.ServiceRequest;
import cloudos.models.services.ServiceResponse;
import cloudos.security.WebSecurityConfig;
import cloudos.services.CloudosServiceService;
import java.util.List;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = CloudosServiceController.SERVICES)
@Log4j
public class CloudosServiceController {

  public static final String SERVICES = WebSecurityConfig.API_PATH + "/services";

  @Autowired
  private CloudosServiceService cloudosServiceService;

  /**
   * Create Service.
   */
  @RequestMapping(method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> createService(@RequestBody ServiceRequest serviceRequest) {
    try {
      if (serviceRequest == null) {
        return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.PRECONDITION_FAILED)
            .message("Service request is invalid").build(), HttpStatus.PRECONDITION_FAILED);
      }
      ServiceResponse response = cloudosServiceService.createService(serviceRequest);
      if (response != null && response.getService() != null) {
        return new ResponseEntity<>(response, HttpStatus.OK);
      }
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.INTERNAL_SERVER_ERROR)
          .message("Response invalid").build(), HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (ServiceException e) {
      log.error("Error - Validation", e);
      return new ResponseEntity<>(
          CloudosResponse.builder().status(HttpStatus.BAD_REQUEST).message(e.getMessage()).build(),
          HttpStatus.BAD_REQUEST);
    } catch (Exception e) {
      log.error("Error", e);
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.INTERNAL_SERVER_ERROR)
          .message(e.getMessage()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Get Services.
   */
  @RequestMapping(value = "/", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> findServices(ServiceRequest serviceRequest) {
    try {
      if (serviceRequest == null) {
        return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.PRECONDITION_FAILED)
            .message("Service request is invalid").build(), HttpStatus.PRECONDITION_FAILED);
      }
      List<CloudosService> response = cloudosServiceService.getServicesByFilter(serviceRequest);
      if (response != null) {
        return new ResponseEntity<>(response, HttpStatus.OK);
      } else {
        return new ResponseEntity<>(CloudosResponse.builder()
            .status(HttpStatus.INTERNAL_SERVER_ERROR).message("Response invalid").build(),
            HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } catch (Exception e) {
      log.error("Error", e);
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.INTERNAL_SERVER_ERROR)
          .message(e.getMessage()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Update Service.
   */
  @RequestMapping(method = RequestMethod.PUT)
  @ResponseBody
  public ResponseEntity<?> updateService(@RequestBody ServiceRequest serviceRequest) {
    try {
      if (serviceRequest == null) {
        return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.PRECONDITION_FAILED)
            .message("Service request is invalid").build(), HttpStatus.PRECONDITION_FAILED);
      }
      ServiceResponse response = cloudosServiceService.updateService(serviceRequest);
      if (response != null && response.getService() != null) {
        return new ResponseEntity<>(response, HttpStatus.OK);
      }
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.INTERNAL_SERVER_ERROR)
          .message("Response invalid").build(), HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (ServiceException e) {
      log.error("Error", e);
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.INTERNAL_SERVER_ERROR)
          .message(e.getMessage()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Delete Service.
   */
  @RequestMapping(method = RequestMethod.DELETE)
  @ResponseBody
  public ResponseEntity<?> deleteService(@RequestParam String id) {
    try {
      if (id == null) {
        return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.PRECONDITION_FAILED)
            .message("Service request is invalid").build(), HttpStatus.PRECONDITION_FAILED);
      }
      ServiceResponse response = cloudosServiceService.deleteService(id);
      if (response != null && response.getService() != null) {
        return new ResponseEntity<>(response, HttpStatus.OK);
      }
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.INTERNAL_SERVER_ERROR)
          .message("Response invalid").build(), HttpStatus.INTERNAL_SERVER_ERROR);

    } catch (ServiceException e) {
      log.error("Error", e);
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.INTERNAL_SERVER_ERROR)
          .message(e.getMessage()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
