package cloudos;

import cloudos.security.WebSecurityConfig;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = MapsController.MAPS)
public class MapsController {

  public static final String MAPS = WebSecurityConfig.API_PATH + "/maps";
}
