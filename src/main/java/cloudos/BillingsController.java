package cloudos;

import cloudos.billings.BillingsService;
import cloudos.billings.CloudosReportBucket;
import cloudos.models.CloudosReportBucketResponse;
import cloudos.security.WebSecurityConfig;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/** Created by philipperibeiro on 5/6/17. */
@RestController
@RequestMapping(value = BillingsController.BILLINGS)
@Log4j2
public class BillingsController {

  public static final String BILLINGS = WebSecurityConfig.API_PATH + "/billings";


  @Autowired private BillingsService billingsService;

  /** Default controller. */
  public BillingsController() {}

  /**
   * Initialize the CloudosReportBucketRepository.
   *
   * @param billingsService The repository containing the list of buckets.
   */
  public BillingsController(BillingsService billingsService) {
    this.billingsService = billingsService;
  }

  /**
   * Return a list of all the billings buckets.
   *
   * @return ResponseEntity: A response containing all the existing billing report buckets
   */
  @RequestMapping(value = "/buckets", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> listBuckets() {
    List<CloudosReportBucket> buckets = this.billingsService.getBuckets();
    if (buckets == null) {
      // return a default empty list
      buckets = new ArrayList<>();
    }
    return new ResponseEntity<>(buckets, HttpStatus.OK);
  }

  /**
   * Get the information associated with a single bucket.
   *
   * @param id The bucket id
   * @return ResponseEntity: A response containing a single bucket report.
   */
  @RequestMapping(value = "/buckets/{id}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> describeBucket(@PathVariable String id) {
    log.info("Describring bucket id {}", id);
    CloudosReportBucket reportBucket = this.billingsService.describeBucket(id);

    if (reportBucket != null) {
      log.info("Found bucket {} for id {}", reportBucket, id);
      return new ResponseEntity<>(reportBucket, HttpStatus.OK);
    } else {
      log.info("Bucket with id {} not found", id);
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
  }

  /**
   * Adds a new billings report bucket.
   *
   * @param reportBucket The report bucket
   * @return ResponseEntity: OK if successful, not found otherwise
   */
  @RequestMapping(value = "/buckets", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> addBucket(@RequestBody CloudosReportBucket reportBucket) {
    log.info("=====================================");
    log.info("Adding Report Bucket {}", reportBucket);
    log.info("=====================================");
    // define the CloudosReportBucketResponse response to be returned
    CloudosReportBucketResponse response;
    if (reportBucket == null) {
      log.info("Bucket is null");
      response =
          new CloudosReportBucketResponse(HttpStatus.NOT_FOUND, null, "Report bucket not found");
      return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    } else if (!this.billingsService.bucketExists(reportBucket)) {
      log.info("Bucket {} does not exist.... adding it", reportBucket);
      CloudosReportBucket cloudosReportBucket = this.billingsService.createBucket(reportBucket);
      response =
          new CloudosReportBucketResponse(
              HttpStatus.CREATED, cloudosReportBucket, "Report bucket successfully created");

      // triggers billings monitor
      billingsService.triggerBillingsFileProcessor(cloudosReportBucket.getProvider());

      return new ResponseEntity<>(response, HttpStatus.CREATED);
    } else {
      log.info("Bucket {} already exist.... returning error", reportBucket);
      response =
          new CloudosReportBucketResponse(
              HttpStatus.INTERNAL_SERVER_ERROR,
              null,
              "Failed to create bucket: Bucket already exists.");
      return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Delete an existing billings report bucket.
   *
   * @param reportBucket The report bucket
   * @return ResponseEntity: OK if successful, not found otherwise
   */
  @RequestMapping(value = "/buckets", method = RequestMethod.DELETE)
  @ResponseBody
  public ResponseEntity<?> deleteBucket(@RequestBody CloudosReportBucket reportBucket) {
    log.info("Attempting to delete bucket {}", reportBucket);
    // define the CloudosReportBucketResponse response to be returned
    CloudosReportBucketResponse response;
    if (reportBucket == null || reportBucket.getId() == null) {
      log.info("Bucket is null");
      response =
          new CloudosReportBucketResponse(HttpStatus.NOT_FOUND, null, "Report bucket not found");
      return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    } else if (this.billingsService.bucketExists(reportBucket)) {
      // attempt to delete the bucket
      this.billingsService.deleteBucket(reportBucket);
      response =
          new CloudosReportBucketResponse(
              HttpStatus.OK, reportBucket, "Report bucket successfully deleted");
      return new ResponseEntity<>(response, HttpStatus.OK);
    } else {
      log.info("Bucket {} was not found ... returning error", reportBucket);
      response =
          new CloudosReportBucketResponse(HttpStatus.NOT_FOUND, null, "Report bucket not found");
      return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }
  }
}
