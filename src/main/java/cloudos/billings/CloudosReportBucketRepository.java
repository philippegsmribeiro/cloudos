package cloudos.billings;

import cloudos.Providers;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by philipperibeiro on 5/6/17.
 *
 * <p>Create a repository for the billings report buckets
 */
public interface CloudosReportBucketRepository
    extends MongoRepository<CloudosReportBucket, String> {

  CloudosReportBucket findByName(String name);

  List<CloudosReportBucket> findByProvider(Providers provider);

  CloudosReportBucket findByNameAndProvider(String name, Providers provider);

  /**
   * Check if the report bucket already exists. Two buckets cannot exist in the same provider with
   * the same name.
   *
   * @param reportBucket: The report bucket object
   * @return boolean
   */
  default boolean exists(CloudosReportBucket reportBucket) {
    return this.findByNameAndProvider(reportBucket.getName(), reportBucket.getProvider()) != null;
  }
}
