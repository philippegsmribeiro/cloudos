package cloudos.billings;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Credits entity for {@link GoogleBillingReport}.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoogleBillingReportCredits extends AbstractBillingReport implements Serializable {

  private String creditId;
  private Double amount;
  private String currency;

}
