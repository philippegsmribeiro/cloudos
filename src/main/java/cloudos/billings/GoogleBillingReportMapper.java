package cloudos.billings;

import cloudos.Providers;
import cloudos.costanalysis.BillingAccountRepository;
import cloudos.models.CallResult;
import cloudos.models.CallStatus;
import cloudos.models.services.ServiceCategory;
import cloudos.models.services.ServiceException;
import cloudos.models.services.ServiceRequest;
import cloudos.models.services.ServiceStatus;
import cloudos.services.CloudosServiceService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Billing report mapper implementation for handling GOOGLE_COMPUTE_ENGINE billing reports.
 */
@Component("googleBillingReportMapper")
@Log4j2
public class GoogleBillingReportMapper extends AbstractBillingReportMapper
    implements BillingReportMapper {

  private static final Providers PROVIDER = Providers.GOOGLE_COMPUTE_ENGINE;

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private BillingAccountRepository billingAccountRepository;

  @Autowired
  private CloudosServiceService cloudosServiceService;

  @Override
  public CallResult<Void> getBillingReport(String fileName,
      BillingReportRepository<?, ?> billingReportRepository) {

    CallResult<Void> result = new CallResult<>(CallStatus.SUCCESS);

    log.info("Processing {} file {}", PROVIDER, fileName);

    try {
      Instant start = Instant.now();
      List<GoogleBillingReport> report = objectMapper.readValue(FileUtils.getFile(fileName),
          new TypeReference<List<GoogleBillingReport>>() {});
      if (CollectionUtils.isNotEmpty(report)) {
        for (GoogleBillingReport item : report) {

          // saving billing account
          saveBillingAccount(billingAccountRepository, item.getAccountId(), PROVIDER);

          // saving provider specific billing report
          ((GoogleBillingReportRepository) billingReportRepository).save(item);
          this.saveServices(item);
        }

        Instant stop = Instant.now();
        Duration d = Duration.between(start, stop);
        log.info("Completed inserting {} {} records in {} seconds", PROVIDER, report.size(),
            d.getSeconds());
      }
    } catch (Exception e) {
      log.error("Error caught while parsing {} JSON: '{}': {}", PROVIDER, fileName, e.getMessage());
      result.fail(String.format("Error caught while parsing %s file '%s': %s", PROVIDER.toString(),
          fileName, e.getMessage()));
    }
    log.info("Finished processing {} file {}", PROVIDER, fileName);

    return result;
  }
  
  /**
   * Parses the a billing report file and save into the billing report repository.
   *
   * @param file the billing report file
   * @param billingReportRepository the google billing report repository instance
   * @return the {@link CallResult}
   */
  public CallResult<Void> getBillingReport(File file,
      GoogleBillingReportRepository billingReportRepository) {
    return this.getBillingReport(file.getAbsolutePath(), billingReportRepository);
  }

  /**
   * Method that save service the billing report.
   * 
   * @param report the Google billing report
   * @throws ServiceException if something goes wrong
   */
  private void saveServices(GoogleBillingReport report) throws ServiceException {
    cloudosServiceService.createService(ServiceRequest.builder().code(report.getProductCode())
        .name(report.getProductCode()).providers(Providers.GOOGLE_COMPUTE_ENGINE)
        .serviceStatus(ServiceStatus.ACTIVE).serviceCategory(ServiceCategory.OTHER).build(), false);
  }

}
