package cloudos.billings;

import cloudos.Providers;
import cloudos.costanalysis.BillingAccountRepository;
import cloudos.models.costanalysis.BillingAccount;
import org.apache.commons.lang3.StringUtils;

/**
 * Common features for billing report mappers.
 */
public abstract class AbstractBillingReportMapper {

  /**
   * Save or update a billing account.
   */
  public void saveBillingAccount(BillingAccountRepository repository, String accountId,
      Providers provider) {
    if (StringUtils.isNotBlank(accountId) && provider != null) {
      repository.save(new BillingAccount(accountId, provider));
    }
  }

}
