package cloudos.billings;

import cloudos.Providers;
import cloudos.config.ThreadingConfig;
import cloudos.costanalysis.BillingAccountRepository;
import cloudos.costanalysis.GenericBillingReportRepository;
import cloudos.models.CallResult;
import cloudos.models.CallStatus;
import cloudos.models.costanalysis.BillingAccount;
import cloudos.models.services.ServiceCategory;
import cloudos.models.services.ServiceException;
import cloudos.models.services.ServiceRequest;
import cloudos.models.services.ServiceStatus;
import cloudos.security.LogFilter;
import cloudos.services.CloudosServiceService;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.stereotype.Component;

/**
 * Billing report mapper implementation for handling AWS billing reports.
 */
@Component("awsBillingReportMapper")
@Log4j2
public class AwsBillingReportMapper extends AbstractBillingReportMapper
    implements BillingReportMapper {

  // batch size for threading
  private static final int BATCH_SIZE = 1000;

  @Autowired
  private BillingAccountRepository billingAccountRepository;

  @Autowired
  private GenericBillingReportRepository genericBillingReportRepository;

  @Autowired
  private CloudosServiceService cloudosServiceService;

  @Autowired
  @Qualifier(ThreadingConfig.BILLINGS)
  private AsyncTaskExecutor taskExecutor;

  @Override
  public CallResult<Void> getBillingReport(String fileName,
      BillingReportRepository<?, ?> billingReportRepository) {

    log.info("Mapping AWS report {} ", fileName);
    CallResult<Void> result = new CallResult<>(CallStatus.SUCCESS);

    try {

      List<CSVRecord> records = getCsvRecords(fileName);
      final Instant start = Instant.now();

      List<Future<?>> threads = new ArrayList<>();
      List<AwsBillingReport> batchList = new ArrayList<>();

      int batch = 0;
      // Read the CSV file records starting from the second record to skip the header
      for (CSVRecord record : records) {

        AwsBillingReport report = mapRow(record);

        if (StringUtils.isEmpty(report.getLineItemId())) {
          log.info("Ignored line: {}", record);
          continue;
        }

        batch++;

        batchList.add(report);

        if (batch == BATCH_SIZE) {

          // save in batch for performance
          final List<AwsBillingReport> batchListToSave = new ArrayList<>(batchList);
          Future<Boolean> submit = taskExecutor.submit(() -> {
            ThreadContext.put(LogFilter.LOG_PATH, fileName);
            saveBatch(billingReportRepository, batchListToSave);
            ThreadContext.remove(LogFilter.LOG_PATH);
            return true;
          });
          threads.add(submit);

          // reset batch count
          batch = 0;
          batchList.clear();
        }
      }

      // save the last items
      final List<AwsBillingReport> batchListToSave = new ArrayList<>(batchList);
      Future<Boolean> submit = taskExecutor.submit(() -> {
        // save in batch for performance
        ThreadContext.put(LogFilter.LOG_PATH, fileName);
        saveBatch(billingReportRepository, batchListToSave);
        ThreadContext.remove(LogFilter.LOG_PATH);
        return true;
      });
      threads.add(submit);

      // wait for all threads to finish
      long count = 0;
      do {
        count = threads.stream().filter(t -> !t.isDone()).count();
        log.info("Processing File: {} Waiting threads {}/{}", fileName, count, threads.size());
        Thread.sleep(1000);
      } while (count > 0);

      Instant stop = Instant.now();
      Duration d = Duration.between(start, stop);
      log.info("Completed mapping {} records in {} seconds.", records.size(), d.getSeconds());
    } catch (Exception e) {
      log.error("Error while mapping file {}: {}", fileName, e.getMessage());
      result.fail(e.getMessage());
    }

    return result;
  }

  /**
   * Save a batch of records.
   * 
   * @param billingReportRepository repository
   * @param batchList batch to be save
   */
  private void saveBatch(BillingReportRepository<?, ?> billingReportRepository,
      List<AwsBillingReport> batchList) {

    log.info("Saving batch!");
    if (CollectionUtils.isNotEmpty(batchList)) {

      try {
        log.info("saving generic billing report first: {}", batchList.size());
        genericBillingReportRepository.saveAll(new ArrayList<>(batchList));

        log.info("saving provider specific billing report: {}", batchList.size());
        ((AwsBillingReportRepository) billingReportRepository).save(new ArrayList<>(batchList));

        try {
          this.saveServices(batchList);
        } catch (ServiceException e) {
          log.error("Error saving services", e);
        }

        // billing account
        Set<BillingAccount> accounts =
            batchList.stream().filter(t -> StringUtils.isNotEmpty(t.getAccountId()))
                .map(t -> new BillingAccount(t.getAccountId(), Providers.AMAZON_AWS))
                .collect(Collectors.toSet());
        List<BillingAccount> billingAccounts = billingAccountRepository.findAll();
        if (!billingAccounts.containsAll(accounts)) {
          log.info("Saving {} accounts!", accounts.size());
          billingAccountRepository.save(accounts);
        }
      } catch (Exception e) {
        log.error("Error saving batch!", e);
      }

    }
  }

  /**
   * Method that save services of the billing report.
   * 
   * @param report the AWS billing report
   * @throws ServiceException if something goes wrong
   */
  private void saveServices(List<AwsBillingReport> reports) throws ServiceException {
    Set<ServiceRequest> servicesRequests = new HashSet<ServiceRequest>();
    for (AwsBillingReport report : reports) {
      ServiceRequest build = ServiceRequest.builder().code(report.getProductCode())
          .name(report.getProductCode()).providers(Providers.AMAZON_AWS)
          .serviceStatus(ServiceStatus.ACTIVE).serviceCategory(ServiceCategory.OTHER).build();
      servicesRequests.add(build);
    }
    cloudosServiceService.createServices(new ArrayList<>(servicesRequests), false);
  }

  /**
   * Open the file as a {@link CSVRecord} iterable.
   *
   * @param fileName the file to be opened
   * @return a {@link CSVRecord} iterable containg the records
   */
  public List<CSVRecord> getCsvRecords(String fileName) throws IOException {
    // initialize FileReader object
    Reader fileReader = new FileReader(fileName);

    // use an iterator for faster and safer data iteraction
    CSVParser parse = CSVFormat.RFC4180.withFirstRecordAsHeader().withAllowMissingColumnNames(true)
        .withIgnoreEmptyLines().parse(fileReader);

    Instant start = Instant.now();
    List<CSVRecord> records = parse.getRecords();
    // sort them
    Collections.sort(records, new Comparator<CSVRecord>() {
      @Override
      public int compare(CSVRecord o1, CSVRecord o2) {

        String string1 = o1.get(AwsBillingHeader.LINE_ITEM_ID);
        String string2 = o2.get(AwsBillingHeader.LINE_ITEM_ID);

        int compareName = string1.compareTo(string2);

        if (compareName != 0) {
          return compareName;
        }

        String time1 = o1.get(AwsBillingHeader.TIME_INTERVAL);
        String time2 = o2.get(AwsBillingHeader.TIME_INTERVAL);

        int compareDate = time1.compareTo(time2);
        return compareDate;
      }
    });
    Instant stop = Instant.now();
    Duration d = Duration.between(start, stop);
    log.info("Duration for parsing the csv file: {} seconds", d.getSeconds());
    return records;
  }

  /**
   * Maps a single row into a {@link AwsBillingReport} object.
   *
   * @param record the record
   * @return a {@link AwsBillingReport} object
   */
  public AwsBillingReport mapRow(CSVRecord record) {
    Map<String, String> inputMap = record.toMap();
    Map<String, String> headerMap = AwsBillingReport.getHeaders();
    for (Map.Entry<String, String> header : inputMap.entrySet()) {
      if (headerMap.containsKey(header.getKey())) {
        headerMap.put(header.getKey(), record.get(header.getKey()));
      }
    }
    return new AwsBillingReport(headerMap);
  }
}
