package cloudos.billings;

import cloudos.Providers;
import cloudos.models.CallResult;

/**
 * Interface for Billing File Processor.
 */
public interface BillingFileProcessor {

  /**
   * Poll all the provider buckets that contain billings reports to be processes. All the buckets
   * are then scanned and polled. If the bucket entry hasn't been processed yet, it will then be
   * forwarded to be parsed and stored in the billings collection.
   */
  void poll() throws Exception;

  /**
   * Returns the provider of the {@link BillingFileProcessor} implementation.
   */
  Providers getProvider();

  /**
   * Maps a previously downloaded billing file.
   *
   * @param billingFile the downloaded billing file instance
   * @return the result status.
   */
  CallResult<Void> processBillingFile(BillingFile billingFile);
}
