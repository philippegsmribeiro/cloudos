package cloudos.billings;

import cloudos.Providers;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.ser.std.ToStringSerializer;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by philipperibeiro on 5/29/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "cloudos_billing_files")
public class BillingFile {

  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  private String id;

  private String name;
  private String prefix;
  private String bucket;
  private String tag;
  private Providers provider;
  private Date created;
  /** Processed: if file was processed by the processor.   */
  private boolean processed;
  /** Ignored: if file was ignored by the processor.   */
  private boolean ignored;

}
