package cloudos.billings;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Cost entity for {@link GoogleBillingReport}. */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoogleBillingReportCost extends AbstractBillingReport implements Serializable {

  private Double amount;
  private String currency;

}
