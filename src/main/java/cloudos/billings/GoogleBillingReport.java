package cloudos.billings;

import cloudos.Providers;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Class representation of Google billing report.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Document(collection = "cloudos_google_billing_report")
public class GoogleBillingReport extends AbstractBillingReport
    implements Serializable, BillingReport {

  private static final Providers PROVIDER = Providers.GOOGLE_COMPUTE_ENGINE;

  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  private String id;
  private String lineItemId;
  private String accountId;
  private String description;
  private Date startTime;
  private Date endTime;
  private Long projectNumber;
  private String projectId;
  private String projectName;
  private List<GoogleBillingReportMeasurement> measurements;
  private List<GoogleBillingReportCredits> credits;
  private GoogleBillingReportCost cost;
  private String resourceId;

  /**
   * Default constructor.
   *
   * <p>Initializes collections
   */
  public GoogleBillingReport() {
    measurements = new ArrayList<>();
    credits = new ArrayList<>();
  }

  @Override
  public Double getUsageCost() {
    double credits = getCredits().stream().mapToDouble(GoogleBillingReportCredits::getAmount).sum();
    return (this.cost != null ? this.cost.getAmount() : Double.valueOf(0.0)) - credits;
  }

  @Override
  public Date getUsageStartDate() {
    return getStartTime();
  }

  @Override
  public Date getUsageEndDate() {
    return getEndTime();
  }

  @Override
  public String getResourceId() {
    return resourceId;
  }

  @Override
  public Providers getProvider() {
    return PROVIDER;
  }

  @Override
  public String getProductCode() {
    return getLineItemId();
  }

}
