package cloudos.billings;

import cloudos.Application;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import lombok.Data;

/**
 * Created by philipperibeiro on 6/2/17.
 *
 * <p>Implement an interface for the billing reports.
 */
@Data
public abstract class AbstractBillingReport {

  protected static final String AWS_BILLING_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";

  protected String billingReport;

  public String getBillingReport() {
    return billingReport;
  }

  public void setBillingReport(String billingReport) {
    this.billingReport = billingReport;
  }

  /**
   * Parse a String representing a double value.
   *
   * @param s The double value string representation.
   * @return Double: A Double value.
   */
  protected static Double parseDouble(String s) {
    if (s == null || s.isEmpty()) {
      return 0.0;
    } else {
      NumberFormat nf = NumberFormat.getInstance(Application.LOCALE_DEFAULT);
      try {
        return nf.parse(s).doubleValue();
      } catch (ParseException e) {
        return 0.0;
      }
    }
  }

  /**
   * Parse a Date represented by a string.
   *
   * @param inputString The date string representation.
   * @return Date: The date as defined.
   */
  protected static Date format(String inputString) {
    DateFormat dateFormat = new SimpleDateFormat(AWS_BILLING_FORMAT);
    try {
      return dateFormat.parse(inputString);
    } catch (ParseException e) {
      return new Date();
    }
  }

}
