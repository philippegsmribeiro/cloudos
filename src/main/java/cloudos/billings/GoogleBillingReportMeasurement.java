package cloudos.billings;

import java.io.Serializable;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Measurement entity for {@link GoogleBillingReport}. */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoogleBillingReportMeasurement extends AbstractBillingReport implements Serializable {

  private String measurementId;
  private BigDecimal sum;
  private String unit;

}
