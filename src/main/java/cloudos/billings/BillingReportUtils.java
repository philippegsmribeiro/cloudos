package cloudos.billings;

import cloudos.Providers;
import cloudos.models.costanalysis.GenericBillingReport;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Utility class to handle {@link BillingReport}.
 */
public class BillingReportUtils {

  /**
   * Private constructor to prevent new instances.
   */
  private BillingReportUtils() {
  }

  /**
   * Group a {@link GenericBillingReport} list by {@link Providers}.
   *
   * @param reports the billing reports list
   * @return the billing reports grouped by providers
   */
  public static Map<Providers, List<GenericBillingReport>> groupBillingReportsByProvider(
      List<GenericBillingReport> reports) {
    if (reports == null) {
      return new HashMap<>();
    }

    return reports.stream().collect(Collectors.groupingBy(GenericBillingReport::getProvider));
  }
}
