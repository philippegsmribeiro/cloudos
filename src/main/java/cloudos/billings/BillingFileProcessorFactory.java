package cloudos.billings;

import cloudos.Providers;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/** Factory for billing file processors. */
@Component
@Log4j2
public class BillingFileProcessorFactory {

  @Autowired private AwsBillingFileProcessor awsBillingFileProcessor;

  @Autowired private GoogleBillingFileProcessor googleBillingFileProcessor;

  /**
   * Returns the provider billing file processor.
   *
   * @param provider the provider
   * @return the billing file processor instance or @{code null} if no implementation was found
   */
  public BillingFileProcessor getBillingFileProcessor(Providers provider) {

    switch (provider) {
      case AMAZON_AWS:
        return this.awsBillingFileProcessor;
      case GOOGLE_COMPUTE_ENGINE:
        return this.googleBillingFileProcessor;
      default:
        return null;
    }
  }
}
