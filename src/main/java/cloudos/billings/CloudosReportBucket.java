package cloudos.billings;

import cloudos.JsonDateSerializer;
import cloudos.Providers;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.ser.std.ToStringSerializer;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by philipperibeiro on 5/6/17.
 *
 * <p>Define the model to be used for the billings report bucket.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "cloudos_report_buckets")
public class CloudosReportBucket {

  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  private String id;

  private String name;
  private Providers provider;

  private String region;
  private Date timestamp;

  @JsonSerialize(using = JsonDateSerializer.class)
  public Date getTimestamp() {
    return this.timestamp;
  }

}
