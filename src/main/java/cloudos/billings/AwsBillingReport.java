package cloudos.billings;

import cloudos.Providers;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.ser.std.ToStringSerializer;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by philipperibeiro on 6/2/17.
 */
@SuppressWarnings("serial")
@Document(collection = "cloudos_aws_billing_report")
@Data
@AllArgsConstructor
public class AwsBillingReport extends AbstractBillingReport implements Serializable, BillingReport {

  /**
   * the billing provider.
   */
  private static final Providers PROVIDER = Providers.AMAZON_AWS;

  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  private String id;

  private String lineItemId;

  private String timeInterval;
  private String invoiceId;

  private String billingEntity;
  private String billType;
  private String payerAccountId;

  private Date billingPeriodStartDate;

  private Date billingPeriodEndDate;

  private String usageAccountId;
  private String lineItemType;

  private Date usageStartDate;

  private Date usageEndDate;

  private String productCode;
  private String lineUsageType;
  private String operationType;
  private String availabilityZone;
  private String resourceId;
  private Double usageAmount;
  private String normalizationFactor;
  private String normalizedUsageAmount;
  private String currencyCode;
  private Double unblendedRate;
  private Double unblendedCost;
  private Double blendedRate;
  private Double blendedCost;
  private String lineItemDescription;
  private String taxType;
  private String productName;
  private String availability;
  private String clockSpeed;
  // file has no current Generation
  private String currentGeneration;
  private String durability;
  private String ecu;
  private String fromLocation;
  private String fromLocationType;
  private String group;
  private String groupDescription;
  private String instanceFamily;
  private String instanceType;
  private String licenseModel;
  private String location;
  private String locationType;
  private String maxIopsBurstPerformance;
  private String maxIopsvolume;
  private String maxThroughputvolume;
  private String maxVolumeSize;
  private String memory;
  private String networkPerformance;
  private String operatingSystem;
  private String operation;
  private String physicalProcessor;
  private String preInstalledSw;
  private String processorArchitecture;
  private String processorFeatures;
  private String productFamily;
  private String servicecode;
  private String sku;
  private String storage;
  private String storageClass;
  private String storageMedia;
  private String tenancy;
  private String toLocation;
  private String toLocationType;
  private String transferType;

  private String productUsagetype;
  private String vcpu;
  private String volumeType;
  private Double publicOnDemandCost;
  private Double publicOnDemandRate;
  private String term;
  private String unit;
  private String normalizedUnitsPerReservation;
  private String totalReservedNormalizedUnits;
  private String totalReservedUnits;
  private String unitsPerReservation;
  private String region;

  public AwsBillingReport() {
    super();
  }

  /**
   * Constructor.
   *
   * @param headerMap the header map
   */
  public AwsBillingReport(Map<String, String> headerMap) {
    super();

    this.lineItemId = headerMap.get(AwsBillingHeader.LINE_ITEM_ID.toString());
    this.timeInterval = headerMap.get(AwsBillingHeader.TIME_INTERVAL.toString());
    this.invoiceId = headerMap.get(AwsBillingHeader.INVOICE_ID.toString());
    this.billingEntity = headerMap.get(AwsBillingHeader.BILLING_ENTITY.toString());
    this.billType = headerMap.get(AwsBillingHeader.BILL_TYPE.toString());
    this.payerAccountId = headerMap.get(AwsBillingHeader.PAYER_ACCOUNT_ID.toString());
    this.billingPeriodStartDate =
        format(headerMap.get(AwsBillingHeader.BILLING_PERIOD_START_DATE.toString()));
    this.billingPeriodEndDate =
        format(headerMap.get(AwsBillingHeader.BILLING_PERIOD_END_DATE.toString()));
    this.usageAccountId = headerMap.get(AwsBillingHeader.USAGE_ACCOUNT_ID.toString());
    this.lineItemType = headerMap.get(AwsBillingHeader.LINE_ITEM_TYPE.toString());
    this.usageStartDate = format(headerMap.get(AwsBillingHeader.USAGE_START_DATE.toString()));
    this.usageEndDate = format(headerMap.get(AwsBillingHeader.USAGE_END_DATE.toString()));
    this.productCode = headerMap.get(AwsBillingHeader.PRODUCT_CODE.toString());
    this.lineUsageType = headerMap.get(AwsBillingHeader.LINE_USAGE_TYPE.toString());
    this.operationType = headerMap.get(AwsBillingHeader.OPERATION_TYPE.toString());
    this.availabilityZone = headerMap.get(AwsBillingHeader.AVAILABILITY_ZONE.toString());
    this.resourceId = headerMap.get(AwsBillingHeader.RESOURCE_ID.toString());
    this.usageAmount = parseDouble(headerMap.get(AwsBillingHeader.USAGE_AMOUNT.toString()));
    this.normalizationFactor = headerMap.get(AwsBillingHeader.NORMALIZATION_FACTOR.toString());
    this.normalizedUsageAmount = headerMap.get(AwsBillingHeader.NORMALIZED_USAGE_AMOUNT.toString());
    this.currencyCode = headerMap.get(AwsBillingHeader.CURRENCY_CODE.toString());
    this.unblendedRate = parseDouble(headerMap.get(AwsBillingHeader.UNBLENDED_RATE.toString()));
    this.unblendedCost = parseDouble(headerMap.get(AwsBillingHeader.UNBLENDED_COST.toString()));
    this.blendedRate = parseDouble(headerMap.get(AwsBillingHeader.BLENDED_RATE.toString()));
    this.blendedCost = parseDouble(headerMap.get(AwsBillingHeader.BLENDED_COST.toString()));
    this.lineItemDescription = headerMap.get(AwsBillingHeader.LINE_ITEM_DESCRIPTION.toString());
    this.taxType = headerMap.get(AwsBillingHeader.TAX_TYPE.toString());
    this.productName = headerMap.get(AwsBillingHeader.PRODUCT_NAME.toString());
    this.availability = headerMap.get(AwsBillingHeader.AVAILABILITY.toString());
    this.clockSpeed = headerMap.get(AwsBillingHeader.CLOCK_SPEED.toString());

    this.currentGeneration = headerMap.get(AwsBillingHeader.CURRENT_GENERATION.toString());
    this.durability = headerMap.get(AwsBillingHeader.DURABILITY.toString());
    this.ecu = headerMap.get(AwsBillingHeader.ECU.toString());
    this.fromLocation = headerMap.get(AwsBillingHeader.FROM_LOCATION.toString());
    this.fromLocationType = headerMap.get(AwsBillingHeader.FROM_LOCATION_TYPE.toString());
    this.group = headerMap.get(AwsBillingHeader.GROUP.toString());
    this.groupDescription = headerMap.get(AwsBillingHeader.GROUP_DESCRIPTION.toString());
    this.instanceFamily = headerMap.get(AwsBillingHeader.INSTANCE_FAMILY.toString());
    this.instanceType = headerMap.get(AwsBillingHeader.INSTANCE_TYPE.toString());
    this.licenseModel = headerMap.get(AwsBillingHeader.LICENSE_MODEL.toString());
    this.locationType = headerMap.get(AwsBillingHeader.LOCATION_TYPE.toString());
    this.maxIopsBurstPerformance =
        headerMap.get(AwsBillingHeader.MAX_IOPS_BURST_PERFORMANCE.toString());
    this.maxIopsvolume = headerMap.get(AwsBillingHeader.MAX_IOPS_VOLUME.toString());
    this.maxVolumeSize = headerMap.get(AwsBillingHeader.MAX_VOLUME_SIZE.toString());
    this.memory = headerMap.get(AwsBillingHeader.MEMORY.toString());
    this.networkPerformance = headerMap.get(AwsBillingHeader.NETWORK_PERFORMANCE.toString());
    this.operatingSystem = headerMap.get(AwsBillingHeader.OPERATING_SYSTEM.toString());
    this.operation = headerMap.get(AwsBillingHeader.OPERATION.toString());
    this.physicalProcessor = headerMap.get(AwsBillingHeader.PHYSICAL_PROCESSOR.toString());
    this.preInstalledSw = headerMap.get(AwsBillingHeader.PRE_INSTALLED_SW.toString());
    this.processorArchitecture = headerMap.get(AwsBillingHeader.PROCESSOR_ARCHITECTURE.toString());
    this.processorFeatures = headerMap.get(AwsBillingHeader.PROCESSOR_FEATURES.toString());
    this.productFamily = headerMap.get(AwsBillingHeader.PRODUCT_FAMILY.toString());
    this.servicecode = headerMap.get(AwsBillingHeader.SERVICE_CODE.toString());
    this.sku = headerMap.get(AwsBillingHeader.SKU.toString());
    this.storage = headerMap.get(AwsBillingHeader.STORAGE.toString());
    this.storageClass = headerMap.get(AwsBillingHeader.STORAGE_CLASS.toString());
    this.storageMedia = headerMap.get(AwsBillingHeader.STORAGE_MEDIA.toString());
    this.tenancy = headerMap.get(AwsBillingHeader.TENANCY.toString());
    this.toLocation = headerMap.get(AwsBillingHeader.TO_LOCATION.toString());
    this.toLocationType = headerMap.get(AwsBillingHeader.TO_LOCATION_TYPE.toString());
    this.transferType = headerMap.get(AwsBillingHeader.TRANSFER_TYPE.toString());
    this.productUsagetype = headerMap.get(AwsBillingHeader.PRODUCT_USAGE_TYPE.toString());
    this.vcpu = headerMap.get(AwsBillingHeader.VCPU.toString());
    this.volumeType = headerMap.get(AwsBillingHeader.VOLUME_TYPE.toString());
    this.publicOnDemandCost =
        parseDouble(headerMap.get(AwsBillingHeader.PUBLIC_ON_DEMAND_COST.toString()));
    this.publicOnDemandRate =
        parseDouble(headerMap.get(AwsBillingHeader.PUBLIC_ON_DEMAND_RATE.toString()));
    this.term = headerMap.get(AwsBillingHeader.TERM.toString());
    this.unit = headerMap.get(AwsBillingHeader.UNIT.toString());
    this.normalizedUnitsPerReservation =
        headerMap.get(AwsBillingHeader.NORMALIZED_UNITS_PER_RESERVATION.toString());
    this.totalReservedNormalizedUnits =
        headerMap.get(AwsBillingHeader.TOTAL_RESERVED_NORMALIZED_UNITS.toString());
    this.totalReservedUnits = headerMap.get(AwsBillingHeader.TOTAL_RESERVED_UNITS.toString());
    this.unitsPerReservation = headerMap.get(AwsBillingHeader.UNITS_PER_RESERVATION.toString());
    this.region = headerMap.get(AwsBillingHeader.REGION.toString());

    id = String.format("%s_%s_%s", this.lineItemId, this.usageStartDate, this.usageEndDate);
  }

  /**
   * Get all the headers of this Billing Report.
   *
   * @return A Map between the header name and its string value
   */
  public static Map<String, String> getHeaders() {
    List<AwsBillingHeader> enumList = Arrays.asList(AwsBillingHeader.values());
    Map<String, String> headers = new HashMap<>();
    for (AwsBillingHeader header : enumList) {
      headers.put(header.toString(), "");
    }

    return headers;
  }
  
    @Override
  public String getResourceId() {
    return this.resourceId;
  }

  @Override
  public Providers getProvider() {
    return PROVIDER;
  }

  @Override
  public String getAccountId() {
    return getPayerAccountId();
  }


  @Override
  public String getProductCode() {
    return productCode;
  }


  @Override
  public Date getUsageEndDate() {
    return this.usageEndDate != null ? (Date) this.usageEndDate.clone() : null;
  }

  @Override
  public Date getUsageStartDate() {
    return this.usageStartDate != null ? (Date) this.usageStartDate.clone() : null;
  }

  @Override
  public Double getUsageCost() {
    return getBlendedCost();
  }

}
