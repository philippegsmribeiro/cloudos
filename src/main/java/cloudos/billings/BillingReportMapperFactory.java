package cloudos.billings;

import cloudos.Providers;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by philipperibeiro on 6/2/17.
 *
 * <p>
 * Factory method for the BillingReportMapper implementations.
 */
@Component("billingReportMapperFactory")
@Log4j2
public class BillingReportMapperFactory {

  @Autowired
  private AwsBillingReportMapper awsBillingReportMapper;

  @Autowired
  private GoogleBillingReportMapper googleBillingReportMapper;

  /**
   * Gets the {@link BillingReportMapper} implementation by the provider.
   *
   * <p>
   * Returns the {@link BillingReportMapper} implementation or @{code null} if the mapper is not
   * implemented for the current provider.
   *
   * @param provider the {@link Providers} instance
   * @return the {@link BillingReportMapper} implementation or @{code null}
   */
  public BillingReportMapper getMapper(Providers provider) {

    if (provider == null) {
      return null;
    }

    switch (provider) {
      case AMAZON_AWS:
        return awsBillingReportMapper;
      case GOOGLE_COMPUTE_ENGINE:
        return googleBillingReportMapper;
      case IBM_SMART_CLOUD:
      case ORACLE_CLOUD:
      case ALIBABA_ALIYUN:
      case MICROSOFT_AZURE:
      default:
        log.warn("No billing report mapper implemented for '{}', returning null ", provider);
        return null;
    }
  }
}
