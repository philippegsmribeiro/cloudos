package cloudos.billings;

import cloudos.queue.QueueManagerService;
import java.io.File;
import java.nio.channels.ClosedByInterruptException;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.time.DateUtils;

/**
 * Created by philipperibeiro on 5/27/17.
 */
@Log4j2
public abstract class Poller {

  private static final int DEFAULT_LIMIT_IN_DAYS = 90;
  protected static final String JSON_FILE_EXTENTION = ".json";
  protected static final String CSV_FILE_EXTENSION = ".csv";
  protected static final String GZ_FILE_EXTENSION = ".gz";
  protected static final String ZIP_FILE_EXTENSION = ".zip";
  protected static final long SLEEP_CONSTAST = 1000L;
  private Thread pollerThread;
  private CountDownLatch threadDoneSignal;

  /**
   * do work.
   */
  private void doWork(int initialDelaySec, int delaySec, boolean fixedRate) {
    log.info("poller starting....");
    boolean first = true;
    long sleepTime = delaySec * SLEEP_CONSTAST;

    while (true) {
      try {
        if (first) {
          if (initialDelaySec > 0) {
            Thread.sleep(initialDelaySec * SLEEP_CONSTAST);
          }
          first = false;
        } else if (sleepTime > 0) {
          Thread.sleep(sleepTime);
        }

        long startMillis = System.currentTimeMillis();
        this.poll();
        sleepTime =
            fixedRate
                ? delaySec * SLEEP_CONSTAST - (System.currentTimeMillis() - startMillis)
                : delaySec * SLEEP_CONSTAST;
      } catch (ClosedByInterruptException e) {
        break;
      } catch (Exception e) {
        log.error("Error polling", e);
      }
    }

    threadDoneSignal.countDown();
    log.info("poller stopping.");
  }

  public abstract void poll() throws Exception;

  protected String getThreadName() {
    return getClass().getName();
  }

  public void start() {
    start(0, 3600, false);
  }

  public void start(final int delaySec) {
    start(0, delaySec, false);
  }

  /**
   * Starts the polling process.
   */
  public void start(final int initialDelaySec, final int delaySec, final boolean fixedRate) {
    threadDoneSignal = new CountDownLatch(1);
    pollerThread = new Thread(() -> doWork(initialDelaySec, delaySec, fixedRate), getThreadName());
    pollerThread.start();
    log.info("poller thread for {} started...", getThreadName());
  }

  /**
   * Interrupts the poller thread before when shutting down.
   */
  public void shutdown() {
    log.info("shutting down... trying to interrupt poller thread...");
    boolean done = false;
    int numTries = 0;
    while (!done) {
      pollerThread.interrupt();
      try {
        done = threadDoneSignal.await(10, TimeUnit.SECONDS);
      } catch (InterruptedException e) {
        // ignore InterruptedException here
      }
      if (!done) {
        numTries = numTries + 1;
        log.warn("trying to interrupt write thread again {}", numTries);
      } else {
        log.info("shutted down successfully.");
      }
    }
  }

  /**
   * Submits the billing file to the queue manager for later processing.
   *
   * <p>Billings files older than 90 days will not be enqueued for processing</p>
   *
   * @param billingFile the billing file instance
   * @param queueManagerService the queue manager service instance
   */
  protected void sendBillingFileForProcessing(BillingFile billingFile,
      QueueManagerService queueManagerService) {
    if (shouldFileBeProcessed(billingFile.getCreated())) {
      log.info("Sending report file {} for mapping queue", billingFile.getName());
      queueManagerService.sendBillingFileDownloadedMessage(billingFile);
    } else if (log.isDebugEnabled()) {
      log.debug("skipped billing file older than 90 days: {}", billingFile);
    }

  }

  // keep the date, avoid extra calculations
  private Date dateLimit = DateUtils
      .truncate(DateUtils.addDays(new Date(), DEFAULT_LIMIT_IN_DAYS * -1), Calendar.DAY_OF_MONTH);

  /**
   * Calculate the dateLimit based on the properties.
   * @param limitInDays days
   */
  protected void calculateDateLimit(Integer limitInDays) {
    dateLimit =
        DateUtils.truncate(DateUtils.addDays(new Date(), limitInDays * -1), Calendar.DAY_OF_MONTH);
  }

  /**
   * Returns whether report file should be processed or not.
   *
   * @param createdDate the file created date
   * @return true if file should be processed
   */
  protected boolean shouldFileBeProcessed(Date createdDate) {
    return dateLimit.before(createdDate);
  }

  /**
   * Attempts to delete a file and does not throw exceptions.
   *
   * @param reportPath the file to be deleted
   * @return if the delete action was successfull
   */
  protected boolean deleteBillingReportFile(String reportPath) {
    log.info("Going to delete file {}", reportPath);
    return FileUtils.deleteQuietly(new File(reportPath));
  }
}
