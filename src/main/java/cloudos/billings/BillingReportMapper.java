package cloudos.billings;

import cloudos.models.CallResult;

/** Billing report mapper interface. */
public interface BillingReportMapper {

  /**
   * Parses the a billing report file and save into the billing report repository.
   *
   * @param fileName the billing report file path
   * @param billingReportRepository the provider specific billing report repository instance
   * @return the {@link CallResult}
   */
  CallResult<Void> getBillingReport(String fileName,
      BillingReportRepository<?,?> billingReportRepository);
}
