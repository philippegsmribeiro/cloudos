package cloudos.billings;

/** Created by philipperibeiro on 6/17/17. */
public enum AwsBillingHeader {
  LINE_ITEM_ID("identity/LineItemId"),
  TIME_INTERVAL("identity/TimeInterval"),
  INVOICE_ID("invoiceId"),
  BILLING_ENTITY("bill/BillingEntity"),
  BILL_TYPE("bill/BillType"),
  PAYER_ACCOUNT_ID("bill/PayerAccountId"),
  BILLING_PERIOD_START_DATE("bill/BillingPeriodStartDate"),
  BILLING_PERIOD_END_DATE("bill/BillingPeriodEndDate"),
  USAGE_ACCOUNT_ID("lineItem/UsageAccountId"),
  LINE_ITEM_TYPE("lineItem/LineItemType"),
  USAGE_START_DATE("lineItem/UsageStartDate"),
  USAGE_END_DATE("lineItem/UsageEndDate"),
  PRODUCT_CODE("lineItem/ProductCode"),
  LINE_USAGE_TYPE("lineItem/UsageType"),
  OPERATION_TYPE("lineItem/Operation"),
  AVAILABILITY_ZONE("lineItem/AvailabilityZone"),
  RESOURCE_ID("lineItem/ResourceId"),
  USAGE_AMOUNT("lineItem/UsageAmount"),
  NORMALIZATION_FACTOR("lineItem/NormalizationFactor"),
  NORMALIZED_USAGE_AMOUNT("lineItem/NormalizedUsageAmount"),
  CURRENCY_CODE("lineItem/CurrencyCode"),
  UNBLENDED_RATE("lineItem/UnblendedRate"),
  UNBLENDED_COST("lineItem/UnblendedCost"),
  BLENDED_RATE("lineItem/BlendedRate"),
  BLENDED_COST("lineItem/BlendedCost"),
  LINE_ITEM_DESCRIPTION("lineItem/LineItemDescription"),
  TAX_TYPE("lineItem/TaxType"),
  PRODUCT_NAME("product/ProductName"),
  AVAILABILITY("product/availability"),
  CLOCK_SPEED("product/clockSpeed"),
  CURRENT_GENERATION("product/currentGeneration"),
  DURABILITY("product/durability"),
  ECU("product/ecu"),
  FROM_LOCATION("product/fromLocation"),
  FROM_LOCATION_TYPE("product/fromLocationType"),
  GROUP("product/group"),
  GROUP_DESCRIPTION("product/groupDescription"),
  INSTANCE_FAMILY("product/instanceFamily"),
  INSTANCE_TYPE("product/instanceType"),
  LICENSE_MODEL("product/licenseModel"),
  LOCATION("product/location"),
  LOCATION_TYPE("product/locationType"),
  MAX_IOPS_BURST_PERFORMANCE("product/maxIopsBurstPerformance"),
  MAX_IOPS_VOLUME("product/maxIopsvolume"),
  MAX_THROUGHPUT_VOLUME("product/maxThroughputvolume"),
  MAX_VOLUME_SIZE("product/maxVolumeSize"),
  MEMORY("product/memory"),
  NETWORK_PERFORMANCE("product/networkPerformance"),
  OPERATING_SYSTEM("product/operatingSystem"),
  OPERATION("product/operation"),
  PHYSICAL_PROCESSOR("product/physicalProcessor"),
  PRE_INSTALLED_SW("product/preInstalledSw"),
  PROCESSOR_ARCHITECTURE("product/processorArchitecture"),
  PROCESSOR_FEATURES("product/processorFeatures"),
  PRODUCT_FAMILY("product/productFamily"),
  SERVICE_CODE("product/servicecode"),
  SKU("product/sku"),
  STORAGE("product/storage"),
  STORAGE_CLASS("product/storageClass"),
  STORAGE_MEDIA("product/storageMedia"),
  TENANCY("product/tenancy"),
  REGION("product/region"),
  TO_LOCATION("product/toLocation"),
  TO_LOCATION_TYPE("product/toLocationType"),
  TRANSFER_TYPE("product/transferType"),
  PRODUCT_USAGE_TYPE("product/usagetype"),
  VCPU("product/vcpu"),
  VOLUME_TYPE("product/volumeType"),
  PUBLIC_ON_DEMAND_COST("pricing/publicOnDemandCost"),
  PUBLIC_ON_DEMAND_RATE("pricing/publicOnDemandRate"),
  TERM("pricing/term"),
  UNIT("pricing/unit"),
  NORMALIZED_UNITS_PER_RESERVATION("reservation/NormalizedUnitsPerReservation"),
  TOTAL_RESERVED_NORMALIZED_UNITS("reservation/TotalReservedNormalizedUnits"),
  TOTAL_RESERVED_UNITS("reservation/TotalReservedUnits"),
  UNITS_PER_RESERVATION("reservation/UnitsPerReservation");

  private String value;

  AwsBillingHeader(String value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return this.value;
  }
}
