package cloudos.billings;

import cloudos.Providers;
import java.util.Date;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.util.Assert;

/** Created by philipperibeiro on 5/29/17. */
public interface BillingFileRepository extends MongoRepository<BillingFile, String> {

  List<BillingFile> getByProcessed(boolean processed);

  List<BillingFile> getByCreated(Date created);

  BillingFile getByName(String name);

  BillingFile getByTag(String tag);

  BillingFile getByNameAndProcessed(String name, boolean processed);

  List<BillingFile> getByPrefix(String prefix);

  List<BillingFile> getByBucket(String bucket);

  List<BillingFile> getByProvider(Providers provider);

  /**
   * Check if an entry exists by looking to its tag
   *
   * @param tag: The tag (hash) value of the file
   * @return boolean: true if the entry exists, false otherwise.
   */
  default boolean processed(String tag) {
    Assert.notNull(tag, "The given tag must not be null!");
    BillingFile billingFile = this.getByTag(tag);
    return billingFile != null && billingFile.isProcessed();
  }
}
