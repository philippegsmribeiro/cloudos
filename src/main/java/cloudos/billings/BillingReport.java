package cloudos.billings;

import cloudos.Providers;
import java.util.Date;

/**
 * Base interface for all billing reports.
 */
public interface BillingReport {

  /**
   * Returns the usage amount.
   */
  Double getUsageCost();

  /**
   * Returns the usage start date (copy).
   */
  Date getUsageStartDate();

  /**
   * Returns the usage end date (copy).
   */
  Date getUsageEndDate();

  /**
   * Returns the resource ID.
   */
  String getResourceId();

  /**
   * Returns the billing provider.
   */
  Providers getProvider();

  /**
   * Returns the billing account id.
   */
  String getAccountId();

  /**
   * Returns the service name identifier.
   */
  String getProductCode();
}
