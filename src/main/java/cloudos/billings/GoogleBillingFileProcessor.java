package cloudos.billings;

import cloudos.Application;
import cloudos.Providers;
import cloudos.config.ThreadingConfig;
import cloudos.costanalysis.GoogleBillingsReconciliationService;
import cloudos.costanalysis.GoogleUsageReportMapper;
import cloudos.google.integration.GoogleStorageIntegration;
import cloudos.models.CallResult;
import cloudos.models.CallStatus;
import cloudos.queue.QueueManagerService;

import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Bucket;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;
import lombok.extern.log4j.Log4j2;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.stereotype.Service;

/**
 * Google billing file processor.
 */
@Service
@Log4j2
public class GoogleBillingFileProcessor extends Poller implements BillingFileProcessor {

  public static final String GOOGLE_BILLINGS_HOME = "google";

  @Value("${cloudos.billings.directory}")
  private String directory;
  
  @Autowired
  @Qualifier(ThreadingConfig.BILLINGS)
  private AsyncTaskExecutor taskExecutor;

  @Autowired
  private BillingFileRepository fileRepository;

  @Autowired
  private CloudosReportBucketRepository bucketRepository;

  @Autowired
  private GoogleBillingReportRepository billingReportRepository;

  @Autowired
  private GoogleStorageIntegration googleStorageIntegration;

  @Autowired
  private BillingReportMapperFactory billingReportMapperFactory;

  @Autowired
  private QueueManagerService queueManagerService;

  @Autowired
  private GoogleUsageReportMapper googleUsageReportMapper;

  @Autowired
  private GoogleBillingsReconciliationService googleBillingsReconciliationService;

  public GoogleBillingFileProcessor() throws IOException {
    this.init();
  }

  /**
   * Initializes the directories.
   */
  public void init() throws IOException {
    // check if the directory is null
    if (this.directory == null) {
      return;
    }
    log.info("Checking Google billings directory");
    Files.createDirectories(Paths.get(this.directory, GOOGLE_BILLINGS_HOME));
    log.info("Google billings directory successfully created");
  }

  @Override
  public void poll() throws Exception {

    /* Get a list of all the AWS buckets to keep track of the reports */
    List<CloudosReportBucket> reportBuckets = this.bucketRepository.findByProvider(getProvider());

    // nothing to do
    if (CollectionUtils.isEmpty(reportBuckets)) {
      return;
    }

    /* go over all the report buckets and find all the reports */
    reportBuckets.forEach(reportBucket -> pollBucket(reportBucket));
  }

  @Override
  public Providers getProvider() {
    return Providers.GOOGLE_COMPUTE_ENGINE;
  }

  @Override
  public CallResult<Void> processBillingFile(BillingFile billingFile) {

    // Google Billing files have a number at the end of its name, so we retrieve its parent
    String reportPath = Paths.get(billingFile.getPrefix(), billingFile.getName())
        .getParent().toAbsolutePath().toString();

    // initializes the call result
    CallResult<Void> result = new CallResult<>(null, CallStatus.FAILURE,
        Collections.singleton("not processed"));

    // checks what kind of report it is
    if (reportPath.endsWith(Poller.CSV_FILE_EXTENSION)) {

      // GOOGLE CLOUD ENGINE USAGE REPORTS CSV
      File usageReportFile = Paths.get(reportPath).toFile();
      result = googleUsageReportMapper.processReport(usageReportFile);
      if (CallStatus.SUCCESS == result.getStatus()) {
        /* Call the reconciliation process to try populate the billing reports. using
         *  yesterday's date (billing files are generated in the day after
         */
        result = googleBillingsReconciliationService.reconcile(billingFile.getCreated().toInstant()
            .atZone(Application.ZONE_ID_DEFAULT).toLocalDate().minusDays(1));
      }
    } else if (reportPath.endsWith(Poller.JSON_FILE_EXTENTION)) {

      // GOOGLE CLOUD ENGINE JSON BILLING FILES
      BillingReportMapper mapper = billingReportMapperFactory.getMapper(getProvider());
      result = mapper.getBillingReport(reportPath, billingReportRepository);
    }

    if (CallStatus.SUCCESS == result.getStatus()) {
      billingFile.setProcessed(true);
      fileRepository.save(billingFile);
      deleteBillingReportFile(reportPath);
    }
    return result;
  }

  /**
   * Retrieve the buckets from the provider and initiates the process.
   */
  public void pollBucket(CloudosReportBucket reportBucket) {

    String bucketName = reportBucket.getName();

    log.info("Processing reports for bucket {}", bucketName);
    Bucket bucket = googleStorageIntegration.retrieveBucket(bucketName);

    if (bucket != null) {

      Iterable<Blob> reports = googleStorageIntegration.listBlobs(bucketName);

      // process the billing reports in a separate thread
      sendReportsForProcessing(reports,
          Paths.get(this.directory, GOOGLE_BILLINGS_HOME).toAbsolutePath().toString());

    } else {
      log.warn("Bucket {} does not exist... skipping", bucketName);
    }
  }

  /**
   * Processes bucket reports using custom destination directory.
   */
  public void sendReportsForProcessing(Iterable<Blob> blobs, String destinationDirectory) {

    /* check if the reports are null */
    if (blobs == null) {
      log.info("Set of report objects is null... nothing to do.");
      return;
    }
    
    List<Future<?>> threads = new ArrayList<>();

    // downloads all reports
    blobs.forEach(
        report -> {
          // check if file created time is not old enough to be processed
          if (shouldFileBeProcessed(new Date(report.getCreateTime()))) {

            String reportKey = report.getGeneratedId();
            log.info("Checking report file {}", reportKey);
            
            BillingFile billingFile = this.fileRepository.getByName(reportKey);
            if (billingFile != null && billingFile.isProcessed()) {
              log.warn("File {} already previously processed", reportKey);
              return;
            }

            if (billingFile != null) {
              // delete it, as it will create again
              fileRepository.delete(billingFile);
            }
            
            String id = billingFile != null ? billingFile.getId() : null;
            billingFile = createBillingFile(id, report);
            final BillingFile savedBillingFile = fileRepository.save(billingFile);

            
            // add billing file to be processed by queue listener
            Future<?> submit = taskExecutor.submit(() -> {
              
              // send o another thread - performance
              String dumpedReportPath = dumpBlobContentsToFile(report, destinationDirectory);
              if (dumpedReportPath != null) {
                sendBillingFileForProcessing(savedBillingFile, queueManagerService);
              } else {
                log.warn("Skipped report file {} due to an error...", reportKey);
              }
            });
            threads.add(submit);

          }
        });
    
    // wait for all threads to finish
    Instant time = Instant.now();
    while (threads.stream().filter(t -> !t.isDone()).count() > 0) {
      if (Duration.between(time, Instant.now()).toMillis() > 2000) {
        time = Instant.now();
        log.info(
            "Waiting threads to finish! Threads: {}(waiting) / {}(total)",
            threads.stream().filter(t -> !t.isDone()).count(),
            threads.size());
      }
    }
  }

  /**
   * Creates a new instance of {@link BillingFile}.
   */
  public BillingFile createBillingFile(String id, Blob blobReport) {
    String name = blobReport.getGeneratedId();
    String prefix = Paths.get(directory, GOOGLE_BILLINGS_HOME).toAbsolutePath().toString();
    String bucket = blobReport.getBlobId().getBucket();
    String tag = blobReport.getEtag();
    Date created = new Date(blobReport.getCreateTime());
    return new BillingFile(id, name, prefix, bucket, tag, getProvider(), created, false, false);
  }

  /**
   * Saves blob contents to file.
   *
   * @param blob the blob
   * @param directory the output directory
   * @return the absolute file path if no error occurs, otherwise null
   */
  public String dumpBlobContentsToFile(Blob blob, String directory) {
    Path path = Paths.get(directory, blob.getBlobId().getBucket());
    try {
      Path parentDir = Files.createDirectories(path);
      File file = new File(parentDir.toString(), blob.getName());
      FileUtils.writeByteArrayToFile(file, blob.getContent());
      log.info("File {} written successfully", file.toString());
      return file.getAbsolutePath();
    } catch (IOException e) {
      log.error("Error while writing file {}: {}", path, e.getMessage());
    }
    return null;
  }

}
