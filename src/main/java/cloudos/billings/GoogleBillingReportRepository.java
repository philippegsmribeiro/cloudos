package cloudos.billings;

import java.util.Date;
import java.util.List;
import org.springframework.data.mongodb.repository.Query;

/** Store all the Google Billing Reports in a Mongo collection. */
public interface GoogleBillingReportRepository
    extends BillingReportRepository<GoogleBillingReport, String> {

  List<GoogleBillingReport> findByAccountId(String accountId);

  List<GoogleBillingReport> findByLineItemId(String lineItemId);

  List<GoogleBillingReport> findByDescription(String description);

  List<GoogleBillingReport> findByStartTime(Date startTime);

  List<GoogleBillingReport> findByEndTime(Date endTime);

  List<GoogleBillingReport> findByProjectId(String projectId);

  List<GoogleBillingReport> findByProjectName(String projectName);

  List<GoogleBillingReport> findByStartTimeBetween(Date startTime, Date endTime);

  @Query(value = "{'startTime':{ $gte: ?0, $lte: ?1}}")
  List<GoogleBillingReport> findByBillingsReportStartTimeBetween(Date start, Date end);

  @Query(value = "{'startTime':{ $gte: ?0, $lte: ?1}, 'lineItemId': ?2, 'projectNumber': ?3}")
  List<GoogleBillingReport> findByStartTimeBetweenAndLineItemIdAndProjectNumber(Date start,
      Date end, String lineItemId, Long projectNumber);
}
