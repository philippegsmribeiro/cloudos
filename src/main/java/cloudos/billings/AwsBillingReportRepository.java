package cloudos.billings;

import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;

/**
 * Created by philipperibeiro on 6/2/17.
 *
 *
 * <p>Store all the AWS Billing Reports in a Mongo collection
 */
public interface AwsBillingReportRepository
    extends BillingReportRepository<AwsBillingReport, String> {
  /* Implement methods that will perform the appropriate query */

  List<AwsBillingReport> findByResourceId(String resourceId);

  List<AwsBillingReport> findByResourceId(String resourceId, Pageable pageable);

  List<AwsBillingReport> findByResourceIdAfter(String resourceId, Date date);

  List<AwsBillingReport> findByUsageAccountId(String usageAccountId);

  List<AwsBillingReport> findByProductName(String productName);

  List<AwsBillingReport> findByBillingPeriodStartDate(Date start);

  List<AwsBillingReport> findByBillingPeriodEndDate(Date end);

  List<AwsBillingReport> findByProductCode(String productCode);

  List<AwsBillingReport> findByFromLocation(String location);

  List<AwsBillingReport> findByUsageStartDateBetween(Date start, Date end);

  List<AwsBillingReport> findByUsageStartDateBetween(Date start, Date end, Pageable pageable);

  @Query(value = "{'usageStartDate':{ $gte: ?0, $lte: ?1}}")
  List<AwsBillingReport> findBillingsReportUsageStartDateBetween(Date start, Date end);

  List<AwsBillingReport> findByPayerAccountId(String payerAccountId);

  List<AwsBillingReport> findByPayerAccountId(String payerAccountId, Pageable pageable);

  @Query("{'$and':[ {'usageStartDate':{ $gte: ?0, $lte: ?1}}, {'term': ?2 } ] }")
  List<AwsBillingReport> findBillingsReportUsageStartDateBetweenAndTerm(Date start, Date end,
      String term);

}
