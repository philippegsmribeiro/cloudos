package cloudos.billings;

import java.math.MathContext;
import java.math.RoundingMode;

/**
 * Billings process constants.
 */
public final class BillingsConstants {

  public static final MathContext MATH_CONTEXT = new MathContext(6, RoundingMode.HALF_UP);

  private BillingsConstants() {
  }

}
