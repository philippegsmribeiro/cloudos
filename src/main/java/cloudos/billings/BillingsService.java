package cloudos.billings;

import cloudos.Providers;
import cloudos.queue.QueueListener;
import cloudos.queue.message.BillingFileDownloadedMessage.BillingFileDownloadedMessageConsumer;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;

import lombok.extern.log4j.Log4j2;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.stereotype.Service;

/**
 * Created by philipperibeiro on 6/2/17.
 */
@Service
@Log4j2
public class BillingsService {

  @Autowired
  private CloudosReportBucketRepository bucketRepository;

  @Autowired
  private BillingFileRepository billingFileRepository;

  @Autowired
  private AsyncTaskExecutor taskExecutor;

  @Autowired
  private BillingFileProcessorFactory billingFileProcessorFactory;

  @Autowired
  private QueueListener queueListener;

  /**
   * Constructor.
   */
  public BillingsService(
      CloudosReportBucketRepository bucketRepository, BillingFileRepository billingFileRepository) {
    this.bucketRepository = bucketRepository;
    this.billingFileRepository = billingFileRepository;
  }

  @PostConstruct
  public void setUpListener() {
    // add listener to queue
    queueListener.addConsumer((BillingFileDownloadedMessageConsumer) message -> {
      BillingFile billingFile = message.getBillingFile();
      BillingFileProcessor processor = billingFileProcessorFactory
          .getBillingFileProcessor(billingFile.getProvider());
      if (processor != null) {
        processor.processBillingFile(billingFile);
      }
    });
  }

  /**
   * Insert a new bucket into the collection.
   */
  public CloudosReportBucket createBucket(@NotNull CloudosReportBucket reportBucket) {
    // if there's no timestamp, add one
    if (reportBucket.getTimestamp() == null) {
      reportBucket.setTimestamp(new Date());
    }

    if (reportBucket.getId() == null) {
      reportBucket.setId(new ObjectId().toString());
    }
    return this.bucketRepository.save(reportBucket);
  }

  /**
   * Deletes a bucket.
   */
  public void deleteBucket(CloudosReportBucket reportBucket) {
    this.bucketRepository.delete(reportBucket);
  }

  /**
   * Returns whether a bucket exists.
   */
  public boolean bucketExists(@NotNull CloudosReportBucket reportBucket) {
    return this.bucketRepository.exists(reportBucket);
  }

  /**
   * Return a list of all Billing Reports.
   */
  public List<BillingFile> getBillingReports() {
    return this.billingFileRepository.findAll();
  }

  /**
   * Returns all buckets.
   */
  public List<CloudosReportBucket> getBuckets() {
    return this.bucketRepository.findAll();
  }

  /**
   * Attempt to find a single cloud report bucket.
   *
   * @param id The id of the cloud report bucket.
   * @return CloudosReportBucket: An CloudosReportBucket object or null if it does not exist.
   */
  public CloudosReportBucket describeBucket(String id) {
    return this.bucketRepository.findOne(id);
  }

  /**
   * Executes the billings poller for the @{code provider}.
   */
  public void triggerBillingsFileProcessor(Providers provider) {
    taskExecutor.execute(
        () -> {
          BillingFileProcessor fileProcessor =
              billingFileProcessorFactory.getBillingFileProcessor(provider);
          try {
            fileProcessor.poll();
          } catch (Exception e) {
            log.error(
                "Unexpected exception while processing billing files for provider {}: {}",
                provider.toString(),
                e.getMessage());
          }
        });
  }
}
