package cloudos.billings;

import java.io.Serializable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.NoRepositoryBean;

/** Generic interface for billing reports repositories. All billing providers should extend it. */
@NoRepositoryBean
public interface BillingReportRepository<T extends BillingReport, ID extends Serializable>
    extends MongoRepository<T, ID> {}
