package cloudos.billings;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Created by philipperibeiro on 6/2/17. */
@Service
@Log4j2
public class BillingsManager implements Runnable {

  @Autowired private AwsBillingFileProcessor awsBillingFileProcessor;

  // @TODO: Add the BillingProcessor for the other providers

  /* The number of threads in the pool - one for each provider */
  private static final int NUMBER_OF_THREADS = 3;

  /* How long should the scheduler sleep for, in minutes */
  private static final int SLEEP_TIME = 60;

  /* Create a final executor, to schedule when the billing processors should be invoked */
  private final ScheduledExecutorService executor =
      Executors.newScheduledThreadPool(NUMBER_OF_THREADS);

  /**
   * Use this constructor to initialize the AwsBillingFileProcessor
   *
   * @param awsBillingFileProcessor: A AwsBilingFileProcessor
   */
  public BillingsManager(AwsBillingFileProcessor awsBillingFileProcessor) {
    this.awsBillingFileProcessor = awsBillingFileProcessor;
  }

  @Override
  public void run() {
    // @TODO: Invoke the AwsBillingFileProcessor, and make the scheduler runs hourly
    Runnable task =
        () -> {
          try {
            TimeUnit.SECONDS.sleep(2);
            log.info("Scheduling: " + System.nanoTime());
          } catch (InterruptedException e) {
            log.info("task interrupted");
          }
        };

    executor.scheduleAtFixedRate(task, 0, SLEEP_TIME, TimeUnit.SECONDS);
  }
}
