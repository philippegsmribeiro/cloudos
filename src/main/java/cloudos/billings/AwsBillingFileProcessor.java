package cloudos.billings;

import cloudos.Providers;
import cloudos.amazon.AmazonS3Service;
import cloudos.config.ThreadingConfig;
import cloudos.models.CallResult;
import cloudos.models.CallStatus;
import cloudos.queue.QueueManagerService;
import cloudos.security.LogFilter;
import cloudos.utils.GzipArchive;
import cloudos.utils.ZipArchive;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Future;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.stereotype.Service;

/**
 * Created by philipperibeiro on 5/27/17.
 */
@Service
@Log4j2
public class AwsBillingFileProcessor extends Poller implements BillingFileProcessor {

  /* number max of attempts of downloading the file */
  private static final int MAX_ATTEMPTS = 3;

  /* Define the default provider to be AWS */
  private static final Providers AWS_PROVIDER = Providers.AMAZON_AWS;

  private static final String AWS_BILLINGS_HOME = "aws";

  @Autowired
  private AmazonS3Service amazonS3Service;

  @Autowired
  @Qualifier(ThreadingConfig.BILLINGS)
  private AsyncTaskExecutor taskExecutor;

  @Autowired
  private BillingFileRepository fileRepository;

  @Autowired
  private CloudosReportBucketRepository bucketRepository;

  @Autowired
  private AwsBillingReportRepository billingReportRepository;

  @Autowired
  private BillingReportMapperFactory billingReportMapperFactory;

  @Autowired
  private BillingFileRepository billingFileRepository;

  @Autowired
  private QueueManagerService queueManagerService;

  @Value("${cloudos.billings.directory}")
  private String directory;

  @Value("${cloudos.billings.limit_in_days:90}")
  private Integer limitInDays;

  private ZipArchive zipArchive;

  private GzipArchive gzipArchive;

  /**
   * Default constructor for the AwsBillingFileProcessor.
   */
  public AwsBillingFileProcessor() throws IOException {
    this.zipArchive = new ZipArchive();
    this.gzipArchive = new GzipArchive();
    this.init();
  }

  /**
   * Initializes any of the variables needed for the AWS Billings File Processor.
   */
  public void init() throws IOException {
    // check if the directory is null
    if (this.directory == null) {
      return;
    }

    // check if the Billings Directory
    File billingsHome = new File(this.directory);
    if (!billingsHome.exists()) {
      log.info("Creating billings home at {}", billingsHome.getAbsolutePath());
      if (!billingsHome.mkdirs()) {
        throw new IOException(
            String.format("Could not create billings root directory at '%s'", this.directory));
      }
    }
    // now attempt to create the aws directory
    File awsHome = new File(this.directory, AWS_BILLINGS_HOME);
    if (!awsHome.exists()) {
      log.info("Creating AWS billings directory at {}", awsHome.getAbsolutePath());
      if (!awsHome.mkdirs()) {
        throw new IOException(String.format("Could not create AWS billings directory at '%s'",
            awsHome.getAbsolutePath()));
      }
    }
    log.info("AWS billings directory successfully created");
  }

  @Override
  public void poll() throws Exception {

    /* Get a list of all the AWS buckets to keep track of the reports */
    List<CloudosReportBucket> reportBuckets = this.bucketRepository.findByProvider(AWS_PROVIDER);

    // nothing to do
    if (CollectionUtils.isEmpty(reportBuckets)) {
      return;
    }

    /* go over all the report buckets and find all the reports */
    reportBuckets.forEach(reportBucket -> {
      log.info("Processing reports for bucket {}", reportBucket.getName());

      // create a new bucket from the given region
      log.info("Region {} ", reportBucket.getRegion());
      String region = StringUtils.isNoneEmpty(reportBucket.getRegion()) ? reportBucket.getRegion()
          : Regions.US_EAST_1.getName();

      // if the bucket exists, list all the files in the bucket
      log.info("Checking if the bucket {} exists", reportBucket.getName());
      if (amazonS3Service.bucketExists(reportBucket.getName(), region)) {

        Set<S3ObjectSummary> reports =
            amazonS3Service.listBucketContent(reportBucket.getName(), region);
        this.process(reports, region);

      } else {
        log.warn("Bucket {} does not exist... skipping", reportBucket.getName());
      }
    });

    log.info("AWS usage processed.");
  }

  @Override
  public Providers getProvider() {
    return Providers.AMAZON_AWS;
  }

  /**
   * Downloads the billing report and enqueues it for later processing.
   *
   * @param reports A set of AWS Billing reports
   */
  public void process(Set<S3ObjectSummary> reports, String region) {

    /* check if the reports are null */
    if (reports == null) {
      log.warn("Set of report objects is null... nothing to do.");
      return;
    }

    calculateDateLimit(limitInDays);

    List<Future<?>> threads = new ArrayList<>();

    // Get the file destination
    String root = new File(this.directory, AWS_BILLINGS_HOME).getAbsolutePath();
    // process all the file reports currently in the bucket
    for (S3ObjectSummary summary : reports) {
      log.info("Processing summary object file: {}", summary.getKey());
      // check if the current object summary does not exist before attempting to process it
      if (!isReportSupportedForMapping(summary.getKey())) {
        log.info("Skipping unsupported file {}", summary.getKey());
        continue;
      }

      // check if file created time is not old enough to be processed
      if (shouldFileBeProcessed(summary.getLastModified())) {

        BillingFile billingFile = this.fileRepository.getByName(summary.getKey());
        if (billingFile != null && billingFile.isProcessed()) {
          log.warn("Skipping already processed file {}", summary.getKey());
          continue;
        }
        if (billingFile != null && billingFile.isIgnored()) {
          log.warn("Skipping ignored file {}", summary.getKey());
          continue;
        }

        if (billingFile != null) {
          // delete it, as it will create again
          fileRepository.delete(billingFile);
        }

        billingFile = new BillingFile(null, summary.getKey(), root, summary.getBucketName(),
            summary.getETag(), getProvider(), summary.getLastModified(), false, false);

        Path path = Paths.get(summary.getKey());
        String lastSegment = path.getFileName().toString();


        // save/update billing file in DB
        final BillingFile savedBillingFile = billingFileRepository.save(billingFile);

        // add billing file to be processed by queue listener
        Future<?> submit = taskExecutor.submit(() -> {

          ThreadContext.put(LogFilter.LOG_PATH, savedBillingFile.getName());

          boolean downloaded = false;

          Path filePath = Paths.get(root, summary.getKey());
          if (Files.exists(filePath, LinkOption.NOFOLLOW_LINKS)) {

            try {
              // verify if file is already downloaded by size
              if (Files.size(filePath) == summary.getSize()) {
                log.info("File already downloaded. Skiping new download.");
                downloaded = true;
              }
            } catch (Exception e) {
              log.error("Failing verifying file size. Trying to download it again!", e);
            }
          }

          int tries = 1;
          while (!downloaded) {

            try {
              log.info("Downloading summary object file {} from modified date {}", lastSegment,
                  summary.getLastModified());
              amazonS3Service.downloadFile(summary.getBucketName(), region, summary.getKey(),
                  filePath.toString(), true, false);
              // s3.downloadFile(summary.getBucketName(), summary.getKey(), filePath, true, false);
              log.info("File downloaded.");
              downloaded = true;

            } catch (Exception e) {
              // error - try to download again
              log.error("Error downloading the file {}! Try: {}", savedBillingFile.getName(),
                  tries);
              if (tries <= MAX_ATTEMPTS) {
                log.error("Not possible to download the file {} from S3!",
                    savedBillingFile.getName());
                break;
              }
              tries++;
            }
          }

          if (downloaded) {
            // send o another thread - performance
            sendBillingFileForProcessing(savedBillingFile, queueManagerService);
          }

          ThreadContext.remove(LogFilter.LOG_PATH);
        });
        threads.add(submit);
      } else {
        log.info("Skipping by date limit - file {}", summary.getKey());
        continue;
      }

    }

    // wait all threads
    Instant time = Instant.now();
    while (threads.stream().filter(t -> !t.isDone()).count() > 0) {
      if (Duration.between(time, Instant.now()).toMillis() > 2000) {
        time = Instant.now();
        log.info(
            "Waiting threads to finish! Threads: {}(waiting) / {}(total)",
            threads.stream().filter(t -> !t.isDone()).count(),
            threads.size());
      }
    }
  }

  /**
   * Checks report file name and return whether it is supported.
   *
   * @param fileName the file name
   */
  private boolean isReportSupportedForMapping(String fileName) {

    boolean supported = true;

    if (fileName.endsWith(JSON_FILE_EXTENTION)) {
      supported = false;
    } else if (fileName.endsWith(CSV_FILE_EXTENSION)) {
      supported = false;
    }

    return supported;
  }

  @Override
  public CallResult<Void> processBillingFile(BillingFile billingFile) {

    log.info("Processing AWS billing file {}", billingFile.getPrefix());

    String filePath = Paths.get(billingFile.getPrefix(), billingFile.getName()).toString();

    boolean ignored = false;
    try {
      /* Check the type of the file ... if the file is gunzip or zip, invoke for further
      processing */
      if (this.zipArchive.isZipFile(filePath)) {
        // @TODO: fix AWS ZIP files processing (different header)
        log.warn("AWS ZIP file not supported yet");
        // set it as ignored for now - avoiding trying to process again
        ignored = true;
      } else if (this.gzipArchive.isGZipped(filePath)) {
        String output = filePath.replace(GZ_FILE_EXTENSION, "");
        this.gzipArchive.gunzipIt(filePath, output);
        filePath = output;
      } else if (StringUtils.endsWith(filePath, CSV_FILE_EXTENSION)) {
        // @TODO: fix AWS CSV non compacted files (different header)
        log.warn("AWS CSV file not supported yet");
        // set it as ignored for now - avoiding trying to process again
        ignored = true;
      } else {
        log.warn("Skipping unsupported file {}", filePath);
        // set it as ignored for now - avoiding trying to process again
        ignored = true;
      }
    } catch (IOException e) {
      // an error occurred processing this billing file
      log.error("Report {} was not processed correctly", filePath);
      return new CallResult<>(CallStatus.FAILURE);
    }

    if (ignored) {
      // if ignored
      billingFile.setIgnored(true);
      this.fileRepository.save(billingFile);
      return new CallResult<>(CallStatus.FAILURE);
    }
    
    if (filePath == null) {
      log.error("Report file {} was not processed correctly, filePath is null!",
          billingFile.getId());
      return new CallResult<>(CallStatus.FAILURE);
    }
    
    BillingReportMapper mapper = billingReportMapperFactory.getMapper(billingFile.getProvider());
    CallResult<Void> processResult = mapper.getBillingReport(filePath, billingReportRepository);

    if (CallStatus.FAILURE == processResult.getStatus()) {
      // an error occurred processing this billing file
      log.error("Report {} was not processed correctly", filePath);
      
    } else {

      // set the billing file as processed
      billingFile.setProcessed(true);
      this.fileRepository.save(billingFile);
    }
    return processResult;
  }

  /**
   * Attempts to delete a set of files.
   *
   * @param filesToBeDeleted the files to be deleted
   */
  private void deleteFiles(Set<String> filesToBeDeleted) {
    for (String file : filesToBeDeleted) {
      deleteBillingReportFile(file);
    }
  }

}
