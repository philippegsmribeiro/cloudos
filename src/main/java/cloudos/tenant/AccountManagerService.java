package cloudos.tenant;

import cloudos.exceptions.InvalidRequestException;
import cloudos.models.CloudosAccount;
import cloudos.models.CloudosAccountRepository;
import cloudos.utils.BeanValidationUtil;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountManagerService {

  private final Logger logger = LoggerFactory.getLogger(AccountManagerService.class);

  @Autowired
  private CloudosAccountRepository cloudosAccountRepository;
  
  @Autowired
  private BeanValidationUtil beanValidationUtil;  

  /**
   * Create a new CloudosAccount database entry.
   *
   * @param request the request containing the CloudosAccount information
   * @return a newly created CloudosAccount or null
   * @throws InvalidRequestException if failed to create the CloudosAccount
   */
  public CloudosAccount create(CloudosAccount cloudosAccount) throws InvalidRequestException {

    if (cloudosAccount == null) {
      throw new InvalidRequestException("CloudosAccount object must not be null.");
    }
    
    beanValidationUtil.validate(cloudosAccount);

    return (CloudosAccount) this.cloudosAccountRepository.save(cloudosAccount);
  }

  /**
   * Remove from database the CloudosAccount.
   *
   * @param id the CloudosAccount id
   * @throws InvalidRequestException if failed to delete the CloudosAccount
   */
  public void delete(String id) throws InvalidRequestException {

    if (id == null) {
      throw new InvalidRequestException("CloudosAccount ID must not be null.");
    }

    this.cloudosAccountRepository.delete(id);
  }

  /**
   * List all CloudosAccount objects.
   *
   * @return a list of CloudosAccount objects.
   */
  public List<CloudosAccount> list() {
    return this.cloudosAccountRepository.findAll();
  }

  /**
   * List CloudosAccount objects filtered by something.
   *
   * @return a list of CloudosAccount objects.
   */
  public List<CloudosAccount> listBySomething() {
    return this.cloudosAccountRepository.findAll();
  }


  /**
   * Get a CloudosAccount based on its id.
   *
   * @param id the CloudosAccount id
   * @return a CloudosAccount if found, null otherwise
   */
  public CloudosAccount getAccount(String id) {
    return this.cloudosAccountRepository.findOne(id);
  }

}
