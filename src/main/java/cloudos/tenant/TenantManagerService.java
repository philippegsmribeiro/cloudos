package cloudos.tenant;

import cloudos.CredentialManagerService;
import cloudos.credentials.CloudCredentialActionException;
import cloudos.credentials.CloudCredentialActionRequest;
import cloudos.encryptionKeys.AmazonKMSServiceException;
import cloudos.exceptions.InvalidRequestException;
import cloudos.exceptions.NotImplementedException;
import cloudos.models.CloudosAccount;
import cloudos.models.CloudosAccountRepository;
import cloudos.models.CloudosCredential;
import cloudos.models.CloudosCredentialRepository;
import cloudos.models.CloudosTenant;
import cloudos.models.CloudosTenantRepository;
import cloudos.securitygroup.CloudSecurityGroupException;
import cloudos.user.User;
import cloudos.user.UserRole;
import cloudos.user.UserService;
import cloudos.utils.BeanValidationUtil;
import io.jsonwebtoken.lang.Collections;
import java.util.Arrays;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class TenantManagerService {

  private static final String regionsByCountryFlagPropertyPrefix = "cloudos.regions.by.country.";

  @Autowired
  private Environment environment;

  @Autowired
  private CloudosAccountRepository cloudosAccountRepository;

  @Autowired
  private CredentialManagerService credentialManagerService;

  @Autowired
  private CloudosTenantRepository cloudosTenantRepository;

  @Autowired
  private BeanValidationUtil beanValidationUtil;

  @Autowired
  private UserService userService;

  @Autowired
  private CloudosCredentialRepository cloudosCredentialRepository;


  /**
   * Checks if a given domainName already exist on database for Tenants.
   *
   * @param domainName - the Tenant domain name
   * @return true if any tenant was found, false otherwise
   */
  public boolean tenantExists(String domainName) {
    return cloudosTenantRepository.findByDomainName(domainName) != null;
  }

  /**
   * Create and save a new CloudosTenant.
   *
   * @param cloudosTenant - the request containing the CloudosTenant information.
   * @return a newly created CloudosTenant or null.
   * @throws InvalidRequestException - when the cloudosTenant is null or user validation raises an
   *         error.
   * @throws CloudCredentialActionException - when credential is null or it has bad credential
   *         values.
   * @throws NotImplementedException - when the credential management is not implemented.
   */
  public CloudosTenant save(CloudosTenant cloudosTenant) throws InvalidRequestException,
      CloudCredentialActionException, NotImplementedException, AmazonKMSServiceException {

    if (cloudosTenant == null) {
      throw new InvalidRequestException("CloudosTenant object must not be null.");
    }

    beanValidationUtil.validate(cloudosTenant);

    saveCloudAccounts(cloudosTenant);

    saveAdminUser(cloudosTenant);

    return (CloudosTenant) this.cloudosTenantRepository.save(cloudosTenant);
  }

  /**
   * Saves the first admin user when the tenant is being created.
   * 
   * @param cloudosTenant - The tenant with the admin user in the users list.
   * @throws InvalidRequestException - When validations raise an error.
   * @throws AmazonKMSServiceException - When an error occurs when encrypting/decrypting
   */
  private void saveAdminUser(CloudosTenant cloudosTenant)
      throws InvalidRequestException, AmazonKMSServiceException {
    if (Collections.isEmpty(cloudosTenant.getUsers())) {
      throw new InvalidRequestException("CloudosTenant must have an Admin user.");
    }

    if (cloudosTenant.getUsers().size() > 1) {
      throw new InvalidRequestException("CloudosTenant must have only one Admin user.");
    }

    User adminUser = cloudosTenant.getUsers().get(0);
    adminUser.setRole(UserRole.ADMIN);

    userService.save(adminUser);
  }


  /**
   * Saves all accounts from a tenant.
   * 
   * @param cloudosTenant - The tenant with the accounts list to be saved.
   * @throws InvalidRequestException - When accounts validation raises an error.
   * @throws NotImplementedException
   * @throws CloudCredentialActionException
   */
  private void saveCloudAccounts(CloudosTenant cloudosTenant)
      throws InvalidRequestException, NotImplementedException, CloudCredentialActionException {
    for (CloudosAccount account : cloudosTenant.getAccounts()) {
      beanValidationUtil.validate(account);
      account = cloudosAccountRepository.save(account);

      for (CloudosCredential credential : account.getCredentials()) {
        if (StringUtils.isBlank(credential.getId())) {
          CloudCredentialActionRequest request = new CloudCredentialActionRequest();
          request.setCloudosCredential(credential);
          credentialManagerService.save(request);
        }
      }

    }

  }

  /**
   * Update a CloudosTenant or update an existing database entry.
   *
   * @param request the request containing the CloudosTenant information.
   * @return an updated CloudosTenant or null.
   * @throws InvalidRequestException - when the cloudosTenant is null or user validation raises an
   *         error.
   * @throws CloudCredentialActionException - when credential is null or it has bad credential
   *         values.
   * @throws NotImplementedException - when the credential management is not implemented.
   * @throws AmazonKMSServiceException - When an error occurs when encrypting/decrypting
   */
  public CloudosTenant update(CloudosTenant cloudosTenant) throws InvalidRequestException,
      CloudCredentialActionException, NotImplementedException, AmazonKMSServiceException {

    if (cloudosTenant == null) {
      throw new InvalidRequestException("CloudosTenant object must not be null.");
    }

    beanValidationUtil.validate(cloudosTenant);

    saveCloudAccounts(cloudosTenant);

    saveUsers(cloudosTenant);

    return (CloudosTenant) this.cloudosTenantRepository.save(cloudosTenant);
  }

  /**
   * Save all users from a tenant.
   * 
   * @param cloudosTenant - The tenant with the users list.
   * @throws InvalidRequestException - When users validation raises an error.
   * @throws AmazonKMSServiceException- When an error occurs when encrypting/decrypting
   */
  private void saveUsers(CloudosTenant cloudosTenant)
      throws InvalidRequestException, AmazonKMSServiceException {
    boolean userAdminFound = false;

    for (User user : cloudosTenant.getUsers()) {
      if (user.getRole().equals(UserRole.ADMIN)) {
        userAdminFound = true;
      }
      if (StringUtils.isEmpty(user.getId())) {
        userService.save(user);
      }
    }

    if (!userAdminFound) {
      throw new InvalidRequestException("CloudosTenant must have an Admin user.");
    }
  }

  /**
   * Remove from database the CloudosTenant.
   *
   * @param id the CloudosTenant id
   * @throws CloudCredentialActionException
   * @throws NotImplementedException
   * @throws AmazonKMSServiceException - When an error occurs when encrypting/decrypting
   * @throws CloudSecurityGroupException if failed to delete the CloudosTenant
   */
  public void delete(String id) throws InvalidRequestException, NotImplementedException,
      CloudCredentialActionException, AmazonKMSServiceException {

    if (id == null) {
      throw new InvalidRequestException("ID must not be null.");
    }

    CloudosTenant tenant = getTenant(id);

    deleteUsers(tenant);
    deleteAccountsAndCredentials(tenant);

    this.cloudosTenantRepository.delete(id);

  }

  /**
   * Cascade delete - Remove from database all accounts and credentials related to a given tenant.
   * 
   * @param tenant - The tenant with the accounst and credentials list to be deleted.
   */
  private void deleteAccountsAndCredentials(CloudosTenant tenant) {
    for (CloudosAccount account : tenant.getAccounts()) {
      for (CloudosCredential credential : account.getCredentials()) {
        cloudosCredentialRepository.delete(credential);
      }
      cloudosAccountRepository.delete(account);
    }
  }

  /**
   * Cascade delete - Remove from database all users related to a given tenant.
   * 
   * @param tenant - The tenant with the users list to be deleted.
   */
  private void deleteUsers(CloudosTenant tenant) {
    for (User user : tenant.getUsers()) {
      userService.delete(user);
    }
  }

  /**
   * List all CloudosTenant objects.
   *
   * @return a list of CloudosTenant objects.
   */
  public List<CloudosTenant> list() {
    return this.cloudosTenantRepository.findAll();
  }

  /**
   * Get a CloudosTenant based on its id.
   *
   * @param id the CloudosTenant id
   * @return a CloudosTenant if found, null otherwise
   * @throws AmazonKMSServiceException - When an error occurs when encrypting/decrypting
   * @throws CloudCredentialActionException
   * @throws NotImplementedException
   */
  public CloudosTenant getTenant(String id) throws InvalidRequestException,
      AmazonKMSServiceException, NotImplementedException, CloudCredentialActionException {

    if (id == null) {
      throw new InvalidRequestException("ID must not be null.");
    }

    CloudosTenant cloudosTenant = cloudosTenantRepository.findOne(id);
    decryptTenantSensitiveData(cloudosTenant);
    return cloudosTenant;
  }

  /**
   * Get a CloudosTenant based on a given username.
   *
   * @param username the CloudosTenant user's username.
   * @return a CloudosTenant if found, null otherwise
   * @throws CloudCredentialActionException
   * @throws NotImplementedException
   * @throws AmazonKMSServiceException - When an error occurs when encrypting/decrypting
   */
  public CloudosTenant getTenantByUsername(String username) throws InvalidRequestException,
      NotImplementedException, CloudCredentialActionException, AmazonKMSServiceException {

    if (StringUtils.isEmpty(username)) {
      throw new InvalidRequestException("Username must not be null.");
    }

    User user = userService.retrieveByUsername(username);

    if (user == null) {
      throw new InvalidRequestException("User not found.");
    }

    CloudosTenant cloudosTenant = cloudosTenantRepository.findByUsersId(user.getId());
    decryptTenantSensitiveData(cloudosTenant);
    return cloudosTenant;
  }

  /**
   * Decrypts tenants's sensitive data based on Amazon customer master key (CMK) using AES algorithm.
   *
   * @param CloudosTenant - The tenant to have sensitive data decrypted.
   * @throws AmazonKMSServiceException when an error occurs when encrypting/decrypting
   */  
  private void decryptTenantSensitiveData(CloudosTenant cloudosTenant)
      throws NotImplementedException, CloudCredentialActionException, AmazonKMSServiceException {
    if (cloudosTenant != null) {
      for (CloudosAccount cloudAccount : cloudosTenant.getAccounts()) {
        for (CloudosCredential credential : cloudAccount.getCredentials()) {
          credentialManagerService.decryptSensitiveData(credential);
        }
      }

      if (cloudosTenant != null) {
        for (User user : cloudosTenant.getUsers()) {
          userService.decryptUserSensitiveData(user);
        }
      }
    }
  }

  /**
   * Returns the regions based on the country flag.
   * 
   * @param countryFlag - the country flag value.
   * @return a string list with the found regions.
   * @throws InvalidRequestException - when countryFlag is empty/null or no region was found by the
   *         informed country flag
   */
  public List<String> getRegionsByCountryFlag(String countryFlag) throws InvalidRequestException {

    if (StringUtils.isEmpty(countryFlag)) {
      throw new InvalidRequestException("The country flag must not be null.");
    }

    String regions = environment.getProperty(regionsByCountryFlagPropertyPrefix + countryFlag);

    if (StringUtils.isEmpty(regions)) {
      throw new InvalidRequestException(
          "No regions by country flag property found for parameter: " + countryFlag);
    }

    if (!regions.contains(",")) {
      return Arrays.asList(regions);
    }

    return Arrays.asList(regions.split(","));
  }
  
}
