package cloudos.tenant;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Class representing the request to check existing user by email.
 *
 * @author Rogério Souza
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmailRequest implements Serializable {

  private static final long serialVersionUID = 3403737460128943381L;
  
  private String email;

}
