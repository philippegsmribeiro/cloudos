package cloudos.tenant;

import cloudos.credentials.CloudCredentialActionException;
import cloudos.encryptionKeys.AmazonKMSServiceException;
import cloudos.exceptions.InvalidRequestException;
import cloudos.exceptions.NotImplementedException;
import cloudos.models.CloudosTenant;
import cloudos.security.WebSecurityConfig;
import cloudos.user.UserService;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller class to handle Tenants creation related operations, also retrieve regions by country
 * flag and generates random CIDR subnet.
 *
 * @author Rogério Souza
 *
 */
@RestController
@RequestMapping(value = TenantController.TENANT)
@Log4j2
public class TenantController {

  public static final String TENANT = WebSecurityConfig.API_PATH + "/tenant";

  @Autowired
  private TenantManagerService tenantManagerService;

  @Autowired
  private UserService userService;

  /**
   * Create a new CloudosTenant.
   *
   * @param cloudosTenant - A CloudosTenant, containing most of the information needed.
   * @return HttpStatus.CREATED is successful.
   */
  @RequestMapping(value = "/create", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> create(@RequestBody CloudosTenant cloudosTenant) {

    if (cloudosTenant == null) {
      return new ResponseEntity<>(new InvalidRequestException("Request object is invalid"),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }

    try {

      cloudosTenant = tenantManagerService.save(cloudosTenant);

      if (cloudosTenant != null) {
        return new ResponseEntity<>(cloudosTenant, HttpStatus.CREATED);
      }

      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (InvalidRequestException | CloudCredentialActionException | NotImplementedException
        | AmazonKMSServiceException e) {
      log.error("Error", e);
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Verifies if the given email/account already exists on database.
   *
   * @param domainName - the Tenant domain name.
   */
  @RequestMapping(value = "/check_existing_tenant_by_domain/{domainName:.+}",
      method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> tenantExists(@PathVariable String domainName) {

    if (StringUtils.isEmpty(domainName)) {
      return new ResponseEntity<>(new InvalidRequestException("Domain Name is invalid"),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return new ResponseEntity<>(tenantManagerService.tenantExists(domainName), HttpStatus.OK);
  }

  /**
   * Verifies if the given user email already exists on database.
   *
   * @param emailRequest - User's email.
   */
  @RequestMapping(value = "/check_existing_user_by_email", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> userExistsByEmail(@RequestBody EmailRequest emailRequest) {

    if (emailRequest == null || StringUtils.isEmpty(emailRequest.getEmail())) {
      return new ResponseEntity<>(new InvalidRequestException("Request object is invalid"),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }

    try {
      return new ResponseEntity<>(userService.loadUserByEmail(emailRequest.getEmail()) != null,
          HttpStatus.OK);
    } catch (UsernameNotFoundException e1) {
      return new ResponseEntity<>(false, HttpStatus.OK);
    } catch (AmazonKMSServiceException e) {
      log.error("Error", e);
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  /**
   * Verifies if the given user username already exists on database.
   *
   * @param username - User's username.
   */
  @RequestMapping(value = "/check_existing_user_by_username/{username}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> userExistsByUSername(@PathVariable String username) {

    if (StringUtils.isEmpty(username)) {
      return new ResponseEntity<>(new InvalidRequestException("Request object is invalid"),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }

    try {
      return new ResponseEntity<>(userService.loadUserByUsername(username) != null, HttpStatus.OK);
    } catch (UsernameNotFoundException e1) {
      return new ResponseEntity<>(false, HttpStatus.OK);
    }

  }

  /**
   * Returns the regions based on the country flag.
   *
   * @param countryFlag - the country flag value.
   * @return a string list with the found regions.
   */
  @RequestMapping(value = "/get_regions_by_country_flag/{countryFlag}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getRegionsByCountryFlag(@PathVariable String countryFlag) {

    if (StringUtils.isEmpty(countryFlag)) {
      return new ResponseEntity<>(new InvalidRequestException("Request object is invalid"),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }

    try {
      return new ResponseEntity<>(tenantManagerService.getRegionsByCountryFlag(countryFlag),
          HttpStatus.OK);
    } catch (InvalidRequestException e) {
      log.error("Error", e);
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

}
