package cloudos;

import cloudos.costanalysis.costsummary.CostSummary;
import cloudos.dashboard.CostEntry;
import cloudos.dashboard.CostEstimatedEntry;
import cloudos.dashboard.DashboardService;
import cloudos.dashboard.Event;
import cloudos.dashboard.InstanceInfo;
import cloudos.dashboard.Provider;
import cloudos.dashboard.RegionEntry;
import cloudos.dashboard.ResourceEntry;
import cloudos.elasticsearch.model.charts.MemoryUsageBytesChart;
import cloudos.elasticsearch.services.MetricbeatService;
import cloudos.models.CloudosDatapointAggregated;
import cloudos.security.WebSecurityConfig;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = DashboardController.DASHBOARD)
public class DashboardController {

  public static final String DASHBOARD = WebSecurityConfig.API_PATH + "/dashboard";

  @Autowired
  private DashboardService dashboardService;

  @Autowired
  private MetricbeatService metricbeatService;

  public DashboardController() {}

  /**
   * Initialize a the DashboardService.
   *
   * @param dashboardService A new DashboardService
   */
  public DashboardController(DashboardService dashboardService) {
    this.dashboardService = dashboardService;
  }

  /**
   * Returns the list of regions.
   */
  @RequestMapping(value = "list/regions", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getRegions() {
    RegionEntry regions = this.dashboardService.getRegions();
    if (regions != null) {
      return new ResponseEntity<>(regions, HttpStatus.OK);
    }
    // in case no entry was found, return an error.
    return new ResponseEntity<>(new RegionEntry(), HttpStatus.OK);
  }

  /**
   * Returns the cost.
   */
  @RequestMapping(value = "list/cost", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getCost() {
    CostEntry entry = dashboardService.getCost();
    if (entry != null) {
      return new ResponseEntity<>(entry, HttpStatus.OK);
    }
    return new ResponseEntity<>(new CostEntry(), HttpStatus.OK);
  }
  
  /**
   * Returns the cost summary.
   */
  @RequestMapping(value = "list/cost_summary", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<Map<String, CostSummary>> getCostSummary() {
    Map<String, CostSummary> entry = dashboardService.getCostSummary();
    if (entry != null) {
      return ResponseEntity.ok(entry);
    }
    return ResponseEntity.ok(new HashMap());
  }

  /** Returns the estimated cost. */
  @RequestMapping(value = "list/cost_estimated", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getCostEstimated() {
    CostEstimatedEntry entry = dashboardService.getCostEstimated();
    if (entry != null) {
      return new ResponseEntity<>(entry, HttpStatus.OK);
    }
    return new ResponseEntity<>(new CostEstimatedEntry(), HttpStatus.OK);
  }


  /** Returns the resources list. */
  @RequestMapping(value = "list/resources", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getResources() {
    ResourceEntry entry = this.dashboardService.getResources();
    if (entry != null) {
      return new ResponseEntity<>(entry, HttpStatus.OK);
    }
    return new ResponseEntity<>(new ResourceEntry(), HttpStatus.OK);
  }

  /** Returns the instances list. */
  @RequestMapping(value = "list/instances", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getInstances() {
    List<InstanceInfo> instances = this.dashboardService.getInstances();
    return new ResponseEntity<>(instances, HttpStatus.OK);
  }

  /** Returns the providers list. */
  @RequestMapping(value = "list/providers", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getProviders() {
    List<Provider> providers = this.dashboardService.getProviders();
    return new ResponseEntity<>(providers, HttpStatus.OK);
  }

  /** Returns the events list. */
  @RequestMapping(value = "list/events", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getEvents() {
    List<Event> events = this.dashboardService.getEvents();
    return new ResponseEntity<>(events, HttpStatus.OK);
  }

  @RequestMapping(value = "list/cpu_usage", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getCpuUsage() {
    return this.getCpuUsage(null);
  }

  /**
   * Returns the cpu usage list.
   */
  @RequestMapping(value = "list/cpu_usage/{provider}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getCpuUsage(@PathVariable Providers provider) {
    Map<Providers, List<CloudosDatapointAggregated>> datapoints =
        this.dashboardService.getCloudCpuUsage(provider);
    if (datapoints != null) {
      return new ResponseEntity<>(datapoints, HttpStatus.OK);
    }
    return new ResponseEntity<HttpStatus>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @RequestMapping(value = "list/memory_usage", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getMemoryUsage() {
    return this.getMemoryUsage(null);
  }

  /**
   * Returns the memory usage list.
   */
  @RequestMapping(value = "list/memory_usage/{provider}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getMemoryUsage(@PathVariable Providers provider) {
    Map<Providers, List<CloudosDatapointAggregated>> datapoints =
        this.dashboardService.getCloudMemoryUsage(provider);
    if (datapoints != null) {
      return new ResponseEntity<>(datapoints, HttpStatus.OK);
    }
    return new ResponseEntity<HttpStatus>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @RequestMapping(value = "list/disk_usage", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getDiskusage() {
    return this.getDiskusage(null);
  }

  /**
   * Returns the disk usage list by provider.
   */
  @RequestMapping(value = "list/disk_usage/{provider}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getDiskusage(@PathVariable Providers provider) {
    Map<Providers, List<CloudosDatapointAggregated>> datapoints =
        this.dashboardService.getCloudDiskUsage(provider);
    if (datapoints != null) {
      return new ResponseEntity<>(datapoints, HttpStatus.OK);
    }
    return new ResponseEntity<HttpStatus>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @RequestMapping(value = "list/network_usage", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getNetworkUsage() {
    return getNetworkUsage(null);
  }

  /**
   * Returns the network usage by provider.
   */
  @RequestMapping(value = "list/network_usage/{provider}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getNetworkUsage(@PathVariable Providers provider) {
    Map<Providers, List<CloudosDatapointAggregated>> datapoints =
        this.dashboardService.getCloudNetworkUsage(provider);
    if (datapoints != null) {
      return new ResponseEntity<>(datapoints, HttpStatus.OK);
    }
    return new ResponseEntity<HttpStatus>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * Retrieve running instances by provider data from Elasticsearch to build the Memory Usage Bytes
   * chart.
   */
  @RequestMapping(value = "chart/memory_usage/{provider}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getMemoryUsageChart(@PathVariable Providers provider) {
    try {
      MemoryUsageBytesChart memoryUsageBytesChart =
          this.metricbeatService.getMemoryUsageBytesChartDataByProvider(provider, null, null);
      return new ResponseEntity<>(memoryUsageBytesChart, HttpStatus.OK);
    } catch (UnknownHostException e) {
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }
}
