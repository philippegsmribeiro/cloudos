package cloudos;

import cloudos.keys.cloudoskey.CloudosKey;
import java.util.ArrayList;
import java.util.List;

/** Created by gleimar on 24/06/2017. */
public class ItemKeyRegionGrouped {

  private String name;
  private List<CloudosKey> keys;

  public ItemKeyRegionGrouped(String name) {
    this.name = name;
    keys = new ArrayList<>();
  }

  public String getName() {
    return name;
  }

  public boolean add(CloudosKey cloudosKey) {
    return keys.add(cloudosKey);
  }

  public List<CloudosKey> getKeys() {
    return keys;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    ItemKeyRegionGrouped that = (ItemKeyRegionGrouped) o;

    return name != null ? name.equals(that.name) : that.name == null;
  }

  @Override
  public int hashCode() {
    return name != null ? name.hashCode() : 0;
  }
}
