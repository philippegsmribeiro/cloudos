package cloudos;

import cloudos.security.WebSecurityConfig;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = SocialController.SOCIAL)
public class SocialController {

  public static final String SOCIAL = WebSecurityConfig.API_PATH + "/social";
}
