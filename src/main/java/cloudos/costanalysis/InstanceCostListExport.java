package cloudos.costanalysis;

import cloudos.models.costanalysis.InstanceCost;
import cloudos.utils.export.FileNameExport;
import cloudos.utils.export.ListExport;

/**
 * Entity that serves to store a list of entity {@link InstanceCost} to be exported.
 * 
 * @author Alex Calagua
 *
 */
@FileNameExport(fileNameExport = "costReport")
public class InstanceCostListExport extends ListExport<InstanceCost> {

}
