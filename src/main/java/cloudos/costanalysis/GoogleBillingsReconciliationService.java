package cloudos.costanalysis;

import cloudos.Providers;
import cloudos.billings.GoogleBillingReport;
import cloudos.billings.GoogleBillingReportMeasurement;
import cloudos.billings.GoogleBillingReportRepository;
import cloudos.models.CallResult;
import cloudos.models.CallStatus;
import cloudos.models.costanalysis.GoogleBillingReconciliationEntry;
import cloudos.models.costanalysis.GoogleBillingReconciliationEntryId;
import cloudos.models.costanalysis.GoogleUsageReportEntry;
import cloudos.utils.JavaSparkContextBuilder;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import scala.Tuple3;

/**
 * Service bean for Google billings reconciliation. It is responsible for conciliating the google
 * usage reports agains the billings reports and enrich the google billings process with the its
 * missing data.
 */
@Service
@Log4j2
public class GoogleBillingsReconciliationService {

  private static final String RESOURCE_URI_SEPARATOR = "/";
  private static final String DEFAULT_CURRENCY = "USD";
  
  @Value("${cloudos.billings.limit_in_days:90}")
  private Integer limitInDays;

  @Autowired
  private GoogleUsageReportEntryRepository googleUsageReportEntryRepository;

  @Autowired
  private GoogleBillingReportRepository googleBillingReportRepository;

  @Autowired
  private GoogleBillingsReconciliationEntryRepository googleBillingReconciliationEntryRepository;

  @Autowired
  private GenericBillingReportRepository genericBillingReportRepository;

  /** MATH CONTEXT FROM GOOGLE BILLING REPORTS, IT MUST MATCH THE PRECISION OF THE COST ENTRIES. */
  public static final MathContext MATH_CONTEXT = new MathContext(6, RoundingMode.HALF_UP);

  /**
   * Conciliates the google usage reports against the billings reports and enrich the google
   * billings process with the its missing data.
   */
  public CallResult<Void> reconcile(@NotNull LocalDate reportDate, @NotNull String measurementId,
      @NotNull Long projectNumber) {

    CallResult<Void> result = new CallResult<>(null, CallStatus.SUCCESS, null);

    try {
      // retrieve the usage reports
      List<GoogleUsageReportEntry> usageReports = googleUsageReportEntryRepository
          .findByReportDateAndMeasurementIdAndNumericProjectId(reportDate, measurementId,
              projectNumber);

      if (!usageReports.isEmpty()) {

        /* for each measurement id kind, get the equivalent billing report to calculate the amount
        value */
        ZoneId zone = ZoneId.systemDefault();

        Date dateFrom = Date
            .from(reportDate.atStartOfDay(zone).toInstant());

        Date dateTo = Date
            .from(reportDate.plus(1, ChronoUnit.DAYS).atStartOfDay(zone)
                .toInstant());

        List<GoogleBillingReport> billingReports = googleBillingReportRepository
            .findByStartTimeBetweenAndLineItemIdAndProjectNumber(dateFrom, dateTo, measurementId,
                projectNumber);

        // process usage reports only if there are billing reports available
        if (!billingReports.isEmpty()) {

          // retrieves the costSum and usage sum to calculate each usage entry cost
          BigDecimal totalCost = BigDecimal.ZERO;
          BigDecimal totalUsageAmount = BigDecimal.ZERO;
          String costCurrency = DEFAULT_CURRENCY;

          for (GoogleBillingReport billingReport : billingReports) {
            // @TODO: Billing reports should use BigDecimal instead of Double to improve precision
            totalCost = totalCost.add(BigDecimal.valueOf(billingReport.getUsageCost()));
            for (GoogleBillingReportMeasurement measurement : billingReport.getMeasurements()) {
              totalUsageAmount = totalUsageAmount.add(measurement.getSum());
            }
          }

          // generate 1 reconciliation entry per usage report
          for (GoogleUsageReportEntry usageReport : usageReports) {

            GoogleBillingReconciliationEntry entry = getGoogleBillingReconciliationEntry(reportDate,
                measurementId, projectNumber, billingReports,
                totalCost, totalUsageAmount, costCurrency, usageReport);

            // Saves the reconciliation entry
            googleBillingReconciliationEntryRepository.save(entry);

            // Saves the billing report entry
            genericBillingReportRepository.save(entry);
          }
        }
      }
    } catch (Exception e) {
      result.fail(e.getMessage());
    }

    return result;
  }

  /**
   * Conciliates the google usage reports against the billings reports and enrich the google
   * billings process with the its missing data.
   */
  public CallResult<Void> reconcile(@NotNull LocalDate reportDate) {

    log.info("Reconciling {} reports using date {}", Providers.GOOGLE_COMPUTE_ENGINE, reportDate);
    Instant start = Instant.now();
    CallResult<Void> result = new CallResult<>(null, CallStatus.SUCCESS, new HashSet<>());
    List<Tuple3<LocalDate, String, Long>> reconcileParams = null;

    try {
      reconcileParams = getReconcileParams(reportDate);
    } catch (Exception e) {
      result.fail(e.getMessage());
      // FAILURE
      return result;
    }

    for (Tuple3<LocalDate, String, Long> params : reconcileParams) {
      CallResult<Void> reconcile = reconcile(params._1(), params._2(), params._3());
      if (CallStatus.FAILURE == reconcile.getStatus()) {
        // FAILURE // PARTIAL FAILURE
        result.fail(reconcile.getMessages());
      }
    }

    Duration duration = Duration.between(start, Instant.now());
    log.info("Finished reconciling {} reports using date {} with status {} in {} seconds",
        Providers.GOOGLE_COMPUTE_ENGINE, reportDate, result.getStatus(), duration.getSeconds());
    return result;
  }

  /**
   * Conciliates the google usage reports against the billings reports and enrich the google
   * billings process with the its missing data for a period of 90 days.
   */
  public CallResult<Void> reconcile() {
    CallResult<Void> result = new CallResult<>(null, CallStatus.SUCCESS, null);
    for (int i = 0; i <= limitInDays; i++) {
      LocalDate reportDate = LocalDate.now().minusDays(i);
      CallResult<Void> reconcileResult = reconcile(reportDate);
      if (CallStatus.FAILURE == reconcileResult.getStatus()) {
        // PARTIAL FAILURE
        result.fail(reconcileResult.getMessages());
      }
    }
    return result;
  }

  /**
   * Get the distinct reconcile params by report date (report date, measurement ID and project
   * number).
   */
  private List<Tuple3<LocalDate, String, Long>> getReconcileParams(@NotNull LocalDate reportDate) {
    List<GoogleUsageReportEntry> usageReports = googleUsageReportEntryRepository
        .findByReportDate(reportDate);

    JavaSparkContext context = JavaSparkContextBuilder.defaultContext();

    JavaRDD<GoogleUsageReportEntry> reportsRdd = context.parallelize(usageReports);

    JavaRDD<Tuple3<LocalDate, String, Long>> params = reportsRdd
        .map(usageReportEntry -> new Tuple3<>(usageReportEntry.getReportDate(),
            usageReportEntry.getMeasurementId(), usageReportEntry.getNumericProjectId()));

    // retrieve the params to call the reconcile process properly
    List<Tuple3<LocalDate, String, Long>> reconcileParams = params.distinct().collect();

    return reconcileParams;
  }

  /**
   * Creates a {@link GoogleBillingReconciliationEntry} instance.
   */
  private GoogleBillingReconciliationEntry getGoogleBillingReconciliationEntry(
      @NotNull LocalDate reportDate, @NotNull String measurementId, @NotNull Long projectNumber,
      List<GoogleBillingReport> billingReports, BigDecimal totalCost, BigDecimal totalUsageAmount,
      String costCurrency, GoogleUsageReportEntry usageReport) {

    String resourceId = getResourceIdFromResourceUri(usageReport.getResourceUri());

    GoogleBillingReport billingReport = billingReports.iterator().next();
    String accountId = billingReport.getAccountId();
    Date startTime = billingReport.getStartTime();
    Date endTime = billingReport.getEndTime();

    GoogleBillingReconciliationEntryId id = new GoogleBillingReconciliationEntryId(reportDate,
        measurementId, resourceId);

    BigDecimal thisUsageCost = calculateReconciliationEntryCost(totalCost, totalUsageAmount,
        usageReport.getQuantity());

    return new GoogleBillingReconciliationEntry(id,
        reportDate, accountId, startTime, endTime, measurementId, resourceId, thisUsageCost,
        costCurrency, usageReport.getLocation(), projectNumber, new HashSet<>(billingReports),
        usageReport, LocalDateTime.now());
  }

  /**
   * Retrieves the resourceId from the resourceUri.
   */
  public String getResourceIdFromResourceUri(String resourceUri) {

    if (StringUtils.isBlank(resourceUri)) {
      return StringUtils.EMPTY;
    }

    return StringUtils.substringAfterLast(resourceUri, RESOURCE_URI_SEPARATOR);
  }

  /**
   * Calculates the {@link GoogleBillingReconciliationEntry#costAmount}.
   *
   * @param totalCost the sum of the google billing reports usage cost
   * @param totalUsageAmount the sum of the google billing reports usage amount
   * @param thisUsageAmount the usage amount of a single usage report entry
   * @return the usage cost of a single usage report entry
   */
  public BigDecimal calculateReconciliationEntryCost(BigDecimal totalCost,
      BigDecimal totalUsageAmount, BigDecimal thisUsageAmount) {

    if (totalUsageAmount == null || totalUsageAmount.compareTo(BigDecimal.ZERO) == 0) {
      return BigDecimal.ZERO;
    }

    return thisUsageAmount.divide(totalUsageAmount, MATH_CONTEXT)
        .multiply(totalCost, MATH_CONTEXT);
  }

}
