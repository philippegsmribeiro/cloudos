package cloudos.costanalysis;

import cloudos.billings.BillingsConstants;
import cloudos.models.CallResult;
import cloudos.models.SparkJob;
import cloudos.models.costanalysis.GenericBillingReport;
import cloudos.models.costanalysis.ProviderDailyCostEntry;
import cloudos.utils.DateUtil;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.spark.api.java.JavaRDD;
import scala.Tuple2;

@Log4j2
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProviderDailyCostJob extends SparkJob {

  private GenericBillingReportRepository genericBillingReportRepository;

  private ProviderDailyCostEntryRepository providerDailyCostEntryRepository;

  /**
   * Retrieves the billing reports in a range of dates, groups it by provider and usage date, reduce
   * it by summing the usage cost, and returns the result.
   */
  public CallResult<List<ProviderDailyCostEntry>> updateCost(LocalDate startDate,
      LocalDate endDate) {
    log.info("going to updating provider daily cost entries for range {}-{}", startDate, endDate);

    CallResult<List<ProviderDailyCostEntry>> result = new CallResult<>();

    if (startDate == null) {
      return result.fail("start date is mandatory");
    }

    if (endDate == null) {
      return result.fail("end date is mandatory");
    }

    try {

      Date start = DateUtil.toDate(startDate);
      Date end = DateUtil.toDate(endDate);

      List<GenericBillingReport> reports =
          genericBillingReportRepository.findByUsageStartTimeBetween(start, end);

      if (reports.isEmpty()) {
        return result;
      }

      JavaRDD<GenericBillingReport> reportsRdd = context.parallelize(reports);
      List<ProviderDailyCostEntry> entries = reportsRdd.mapToPair(r -> {
        LocalDate date = DateUtil.toLocalDate(r.getUsageStartTime());
        return new Tuple2<>(new Tuple2<>(r.getProvider(), date), r.getUsageCost());
      }).reduceByKey((i, n) -> i.add(n, BillingsConstants.MATH_CONTEXT)).map(r -> {
        return new ProviderDailyCostEntry(null, r._1()._1(), r._1()._2(), r._2(), new Date());
      }).collect();

      providerDailyCostEntryRepository.save(entries);
      result.setResult(entries);

    } catch (Exception e) {
      String message = String.format(
          "an error ocurred while updating provider daily cost entries: %s", e.getMessage());
      log.error(message);
      result.fail(message);
    }

    return result;
  }

}
