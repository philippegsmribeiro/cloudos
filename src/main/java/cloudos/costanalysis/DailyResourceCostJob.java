package cloudos.costanalysis;

import cloudos.billings.BillingReport;
import cloudos.billings.BillingsConstants;
import cloudos.models.CallResult;
import cloudos.models.SparkJob;
import cloudos.models.costanalysis.BillingCost;
import cloudos.models.costanalysis.DailyResourceCost;
import cloudos.models.costanalysis.DailyResourceCostId;
import cloudos.models.costanalysis.GenericBillingReport;
import cloudos.utils.DateUtil;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import scala.Tuple2;
import scala.Tuple3;

/**
 * Job that collects and stores the provider resource cost in a daily basis.
 */
@Log4j2
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DailyResourceCostJob extends SparkJob {

  private GenericBillingReportRepository genericBillingReportRepository;

  private DailyResourceCostService service;

  /**
   * Executes the job.
   * 
   * @param start the start date range
   * @param end the end date range
   * @return the call result wrapper
   */
  public CallResult<List<DailyResourceCost>> execute(LocalDate start, LocalDate end) {

    CallResult<List<DailyResourceCost>> result = new CallResult<>();

    if (start == null) {
      return result.fail("start date is mandatory");
    }

    if (end == null) {
      return result.fail("end date is mandatory");
    }
    
    Instant startOfProcess = Instant.now();
    String trace = String.format("executing daily resource cost job from range %s to %s",
        start.toString(), end.toString());
    log.info(trace);

    List<GenericBillingReport> billingReports = genericBillingReportRepository
        .findByUsageStartTimeBetween(DateUtil.toDate(start), DateUtil.toDate(end));

    if (!billingReports.isEmpty()) {
      try {

        trace =
            String.format("extracing daily resource cost from billing reports from range %s to %s",
                start.toString(), end.toString());

        List<DailyResourceCost> entries = extractDailyResourceCost(billingReports);
        service.save(entries);
        result.setResult(entries);
      } catch (Exception e) {
        log.error("Error while {}: {}", trace, e.getMessage());
        result.fail(String.format("An error occured while %s", trace));
      }
    }
    
    Duration duration = Duration.between(startOfProcess, Instant.now());
    trace = String.format(
        "finished executing daily resource cost job from range %s to %s in %d seconds",
        start.toString(), end.toString(), duration.getSeconds());
    log.info(trace);

    return result;
  }

  /**
   * Extracts a list of {@link DailyResourceCost} from a list of {@link BillingReport}.
   * @param billingReports the billing reports to extract from
   * @return a list of {@link DailyResourceCost}
   */
  private List<DailyResourceCost> extractDailyResourceCost(
      List<GenericBillingReport> billingReports) {
    List<DailyResourceCost> entries = context.parallelize(billingReports).mapToPair(r -> {
      return new Tuple2<>(
          new Tuple3<>(
              DateUtil.toLocalDate(r.getUsageStartTime()), 
              r.getProvider(),
              r.getResourceId()), 
          r.getUsageCost());
    }).reduceByKey((i, n) -> i.add(n, BillingsConstants.MATH_CONTEXT)).map(i -> {
      DailyResourceCostId id = new DailyResourceCostId(i._1()._1(), i._1()._2(), i._1()._3());
      DailyResourceCost drc = new DailyResourceCost(id, new BillingCost(i._2()), new Date());
      return drc;
    }).collect();
    return entries;
  }

}
