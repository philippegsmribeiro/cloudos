package cloudos.costanalysis.costsummary;

import cloudos.Providers;
import cloudos.costanalysis.GenericBillingReportRepository;
import cloudos.models.costanalysis.GenericBillingReport;
import cloudos.utils.DateUtil;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CostSummaryService {

  @Autowired
  private GenericBillingReportRepository genericBillingReportRepository;
  @Autowired
  private CostSummaryRepository costSummaryRepository;

  /**
   * Update (create a newest cost summary) based on the billing reports. It should be called only on
   * scheduling (monitors) as it takes a while to process.
   * 
   * @return updated CostSummaty id
   */
  public CostSummary updateCostSummary(Providers provider) {
    log.info("========= Updating the dashboard cost entries for provider {} ===========", provider);

    // today
    LocalDateTime today = LocalDateTime.now();

    // get all year data
    LocalDateTime firstDayOfYear = today.withMonth(1)
                                        .withDayOfMonth(1)
                                        .withHour(0)
                                        .withMinute(0)
                                        .withSecond(0)
                                        .withNano(0);
    
    LocalDateTime lastDayOfYear = firstDayOfYear.withMonth(12)
                                                .withDayOfMonth(31)
                                                .withHour(23)
                                                .withMinute(59)
                                                .withSecond(59)
                                                .withNano(999999999);

    List<GenericBillingReport> allYearReports = null;
    if (provider == null) {
      allYearReports = genericBillingReportRepository.findByUsageStartTimeBetween(
          DateUtil.toDate(firstDayOfYear), DateUtil.toDate(lastDayOfYear));
    } else {
      allYearReports = genericBillingReportRepository.findByProviderAndUsageStartTimeBetween(
          provider, DateUtil.toDate(firstDayOfYear), DateUtil.toDate(lastDayOfYear));
    }

    allYearReports =
        allYearReports.stream().filter(t -> !t.isCredit()).collect(Collectors.toList());


    // this year by month
    List<Double> thisYear = executeThisYearCalculation(allYearReports);
    Double thisYearSum = thisYear.stream().collect(Collectors.summingDouble(Double::doubleValue));

    // this month by day
    List<Double> thisMonth = executeThisMonthCalculation(today, allYearReports);
    Double thisMonthSum = thisMonth.stream().collect(Collectors.summingDouble(Double::doubleValue));
    Double lastMonthSum = thisYear.get(today.getMonthValue() - 2);
    // Percentage
    Double thisMonthPercentage = getPercentage(lastMonthSum, thisMonthSum);

    // this week by day of week
    List<Double> thisWeek = executeThisWeekCalculation(today, allYearReports);
    Double thisWeekSum = thisWeek.stream().collect(Collectors.summingDouble(Double::doubleValue));

    // verify if needs to re-search to bring the last days
    if (today.minusDays(100).isBefore(firstDayOfYear)) {
      // re-search
      LocalDateTime todayMinus40days = today.minusDays(100)
                                            .withHour(0)
                                            .withMinute(0)
                                            .withSecond(0)
                                            .withNano(0);
      // re-search
      if (provider == null) {
        allYearReports = genericBillingReportRepository.findByUsageStartTimeBetween(
            DateUtil.toDate(todayMinus40days), DateUtil.toDate(lastDayOfYear));
      } else {
        allYearReports = genericBillingReportRepository.findByProviderAndUsageStartTimeBetween(
            provider, DateUtil.toDate(todayMinus40days), DateUtil.toDate(lastDayOfYear));
      }
    }

    // last 7 days
    List<Double> last7days = executeLast7daysCalculation(today, allYearReports);
    Double last7daysSum = last7days.stream().collect(Collectors.summingDouble(Double::doubleValue));
    // last 30 days
    List<Double> last30days = executeLast30daysCalculation(today, allYearReports);
    Double last30daysSum =
        last30days.stream().collect(Collectors.summingDouble(Double::doubleValue));
    // last 4 weeks
    List<Double> last4weeks = executeLast4WeeksCalculation(today, allYearReports);
    Double last4weeksSum =
        last4weeks.stream().collect(Collectors.summingDouble(Double::doubleValue));
    // last 3 weeks
    Double last3WeeksSum = last4weeksSum - last4weeks.get(0);
    // last 3 months
    List<Double> last3months = executeLast3MonthsCalculation(today, allYearReports);
    Double last3monthsSum =
        last3months.stream().collect(Collectors.summingDouble(Double::doubleValue));
    Double last2monthsSum = last3monthsSum - last3months.get(0);

    //
    Double pastWeekSum = last4weeks.get(3);
    Double thisWeekPercentage = getPercentage(pastWeekSum, thisWeekSum);

    // daily average
    Double dailyAverage = last7daysSum / 7;
    Double dailyAveragePercentage = getPercentage(pastWeekSum / 7, dailyAverage);

    // weekly average
    Double weeklyAverage = (last3WeeksSum + thisWeekSum) / 4;
    Double weeklyAveragePercentage = getPercentage(last4weeksSum / 4, weeklyAverage);

    // monthly average
    Double monthlyAverage = (last2monthsSum + thisMonthSum) / 3;
    Double monthlyAveragePercentage = getPercentage(last3monthsSum / 3, monthlyAverage);

    CostSummary costSummary = CostSummary.builder()
                                         .provider(provider)
                                         .thisWeek(thisWeek)
                                         .thisWeekSum(thisWeekSum)
                                         .thisWeekPercentage(thisWeekPercentage)
                                         .thisMonth(thisMonth)
                                         .thisMonthSum(thisMonthSum)
                                         .thisMonthPercentage(thisMonthPercentage)
                                         .thisYear(thisYear)
                                         .thisYearSum(thisYearSum)
                                         .last7days(last7days)
                                         .last7daysSum(last7daysSum)
                                         .last30days(last30days)
                                         .last30daysSum(last30daysSum)
                                         .last4weeks(last4weeks)
                                         .last4weeksSum(last4weeksSum)
                                         .dailyAverage(dailyAverage)
                                         .dailyAveragePercentage(dailyAveragePercentage)
                                         .weeklyAverage(weeklyAverage)
                                         .weeklyAveragePercentage(weeklyAveragePercentage)
                                         .monthlyAverage(monthlyAverage)
                                         .monthlyAveragePercentage(monthlyAveragePercentage)
                                         .timestamp(new Date())
                                         .build();
    CostSummary save = costSummaryRepository.save(costSummary);
    return save;
  }

  /**
   * Get the percentage change between 2 numbers.
   * 
   * @param originalNumber original
   * @param newNumber new
   * @return percentage, negative if decreased, positive if increased
   */
  private static Double getPercentage(Double originalNumber, Double newNumber) {
    if (originalNumber.equals(newNumber)) {
      return new Double(0);
    }
    if (originalNumber.equals(new Double(0)) && !newNumber.equals(new Double(0))) {
      return new Double(newNumber > 0 ? 1 : -1) * 100;
    }
    return ((newNumber - originalNumber) / originalNumber) * 100;
  }

  /**
   * Return key YYYY-MM for a day.
   * 
   * @param date reference
   * @return String key
   */
  private static String getMonthYearKey(LocalDate date) {
    return date.getYear() + "-" + (date.getMonthValue() < 10 ? "0" : "") + date.getMonthValue();
  }

  /**
   * Execute a calculation for the last 3 months data.
   * 
   * @param today reference date
   * @param allYearReports reports
   * @return List of double values, sorted by date (month) asc
   */
  private static List<Double> executeLast3MonthsCalculation(LocalDateTime today,
      List<GenericBillingReport> allYearReports) {
    // filter by month
    LocalDateTime firstDayOfMonth =
        today.with(ChronoField.DAY_OF_MONTH, 1).withHour(0).withMinute(0).withSecond(0).withNano(0);

    LocalDateTime firstDayOfMonth3 = firstDayOfMonth.minusMonths(3);

    int lengthOfMonth = today.minusMonths(1).toLocalDate().lengthOfMonth();
    LocalDateTime lastDayOfMonth =
        today.minusMonths(1).with(ChronoField.DAY_OF_MONTH, lengthOfMonth).withHour(23)
            .withMinute(59).withSecond(59).withNano(999999999);

    List<GenericBillingReport> monthsReports = allYearReports.stream()
        .filter(t -> !t.getUsageStartTime().before(DateUtil.toDate(firstDayOfMonth3))
            && t.getUsageStartTime().before(DateUtil.toDate(lastDayOfMonth)))
        .collect(Collectors.toList());

    // collect by month
    Map<String, Double> collectByMonth = monthsReports.stream().collect(
        Collectors.groupingBy(t -> getMonthYearKey(DateUtil.toLocalDate(t.getUsageStartTime())),
            Collectors.summingDouble(t -> t.getUsageCost().doubleValue())));
    // adding the missing months
    LocalDate referenceDate = lastDayOfMonth.toLocalDate();
    for (int i = 0; i < 3; i++) {
      String key = getMonthYearKey(referenceDate);
      if (!collectByMonth.containsKey(key)) {
        collectByMonth.put(key, new Double(0));
      }
      referenceDate = referenceDate.minusMonths(1);
    }
    List<String> keyList = new ArrayList<String>(collectByMonth.keySet());
    Collections.sort(keyList);
    List<Double> values = new ArrayList<>();
    for (String string : keyList) {
      values.add(collectByMonth.get(string));
    }
    return values;
  }

  /**
   * Execute a calculation for the last 4 week data.
   * 
   * @param today reference date
   * @param allYearReports reports
   * @return List of double values, sorted by date (week) asc
   */
  private static List<Double> executeLast4WeeksCalculation(LocalDateTime today,
      List<GenericBillingReport> allYearReports) {
    // filter by week
    LocalDateTime firstDayOfWeek = today.with(ChronoField.DAY_OF_WEEK, 1)
                                        .withHour(0)
                                        .withMinute(0)
                                        .withSecond(0)
                                        .withNano(0);
    
    LocalDateTime firstDayOfWeek4 = firstDayOfWeek.minusWeeks(4);
    
    LocalDateTime lastDayOfWeek = firstDayOfWeek.minusDays(1)
                                       .withHour(23)
                                       .withMinute(59)
                                       .withSecond(59)
                                       .withNano(999999999);

    List<GenericBillingReport> weekReports = allYearReports.stream()
        .filter(
            t -> !t.getUsageStartTime().before(DateUtil.toDate(firstDayOfWeek4))
                && t.getUsageStartTime().before(DateUtil.toDate(lastDayOfWeek)))
        .collect(Collectors.toList());

    LocalDate.now().getYear();

    // collect by day of week
    Map<String, Double> collectByWeek =
        weekReports.stream()
            .collect(Collectors.groupingBy(
                t -> DateUtil.toLocalDate(t.getUsageStartTime()).getYear() + "-"
                    + DateUtil.toLocalDate(t.getUsageStartTime())
                        .get(ChronoField.ALIGNED_WEEK_OF_YEAR),
                Collectors.summingDouble(t -> t.getUsageCost().doubleValue())));
    // adding the missing days
    LocalDate firstLast30daysDate = firstDayOfWeek4.toLocalDate();
    while (!firstLast30daysDate.isAfter(lastDayOfWeek.toLocalDate())) {
      String key = firstLast30daysDate.getYear() + "-"
          + firstLast30daysDate.get(ChronoField.ALIGNED_WEEK_OF_YEAR);
      if (!collectByWeek.containsKey(key)) {
        collectByWeek.put(key, new Double(0));
      }
      firstLast30daysDate = firstLast30daysDate.plusDays(1);
    }
    List<String> keyList = new ArrayList<String>(collectByWeek.keySet());
    Collections.sort(keyList);
    List<Double> values = new ArrayList<>();
    for (String string : keyList) {
      values.add(collectByWeek.get(string));
    }
    return values;
  }

  /**
   * Execute a calculation for the last 30 days data.
   * 
   * @param today reference date
   * @param allYearReports reports
   * @return List of double values, sorted by date (day) asc
   */
  private static List<Double> executeLast30daysCalculation(LocalDateTime today,
      List<GenericBillingReport> allYearReports) {
    LocalDateTime firstLast30days = today.minusDays(30)
                                        .withHour(0)
                                        .withMinute(0)
                                        .withSecond(0)
                                        .withNano(0);
    
    LocalDateTime lastLast30days = today.withHour(23)
                                       .withMinute(59)
                                       .withSecond(59)
                                       .withNano(999999999);

    List<GenericBillingReport> last30Reports = allYearReports.stream()
        .filter(
            t -> !t.getUsageStartTime().before(DateUtil.toDate(firstLast30days))
                && t.getUsageStartTime().before(DateUtil.toDate(lastLast30days)))
        .collect(Collectors.toList());

    // collect by last 30 days
    Map<String, Double> collectByLast30days = last30Reports.stream()
        .collect(Collectors.groupingBy(t -> DateUtil.toLocalDate(t.getUsageStartTime()).toString(),
            Collectors.summingDouble(t -> t.getUsageCost().doubleValue())));
    // adding the missing days
    LocalDate firstLast30daysDate = firstLast30days.toLocalDate();
    while (firstLast30daysDate.isBefore(lastLast30days.toLocalDate())) {
      String key = firstLast30daysDate.toString();
      if (!collectByLast30days.containsKey(key)) {
        collectByLast30days.put(key, new Double(0));
      }
      firstLast30daysDate = firstLast30daysDate.plusDays(1);
    }
    List<String> keyList = new ArrayList<String>(collectByLast30days.keySet());
    Collections.sort(keyList);
    List<Double> values = new ArrayList<>();
    for (String string : keyList) {
      values.add(collectByLast30days.get(string));
    }
    return values;
  }

  /**
   * Execute a calculation for the last 7 days data.
   * 
   * @param today reference date
   * @param allYearReports reports
   * @return List of double values, sorted by date (day) asc
   */
  private static List<Double> executeLast7daysCalculation(LocalDateTime today,
      List<GenericBillingReport> allYearReports) {
    LocalDateTime firstLast7days = today.minusDays(6)
                                        .withHour(0)
                                        .withMinute(0)
                                        .withSecond(0)
                                        .withNano(0);
    
    LocalDateTime lastLast7days = today.withHour(23)
                                       .withMinute(59)
                                       .withSecond(59)
                                       .withNano(999999999);

    List<GenericBillingReport> last7Reports = allYearReports.stream()
        .filter(
            t -> !t.getUsageStartTime().before(DateUtil.toDate(firstLast7days))
                && t.getUsageStartTime().before(DateUtil.toDate(lastLast7days)))
        .collect(Collectors.toList());

    // collect by last 7 days
    Map<String, Double> collectByLast7days = last7Reports.stream()
        .collect(Collectors.groupingBy(t -> DateUtil.toLocalDate(t.getUsageStartTime()).toString(),
            Collectors.summingDouble(t -> t.getUsageCost().doubleValue())));
    // adding the missing days
    LocalDate firstLast7daysDate = firstLast7days.toLocalDate();
    while (!firstLast7daysDate.isAfter(lastLast7days.toLocalDate())) {
      String key = firstLast7daysDate.toString();
      if (!collectByLast7days.containsKey(key)) {
        collectByLast7days.put(key, new Double(0));
      }
      firstLast7daysDate = firstLast7daysDate.plusDays(1);
    }
    List<String> keyList = new ArrayList<String>(collectByLast7days.keySet());
    Collections.sort(keyList);
    List<Double> values = new ArrayList<>();
    for (String string : keyList) {
      values.add(collectByLast7days.get(string));
    }
    return values;
  }

  /**
   * Execute a calculation for the this week data.
   * 
   * @param today reference date
   * @param allYearReports reports
   * @return List of double values, sorted by date (day of week) asc
   */
  private static List<Double> executeThisWeekCalculation(LocalDateTime today,
      List<GenericBillingReport> allYearReports) {
    // filter by week
    LocalDateTime firstDayOfWeek = today.with(ChronoField.DAY_OF_WEEK, 1)
                                        .withHour(0)
                                        .withMinute(0)
                                        .withSecond(0)
                                        .withNano(0);
    
    LocalDateTime lastDayOfWeek = today.with(ChronoField.DAY_OF_WEEK, 7)
                                       .withHour(23)
                                       .withMinute(59)
                                       .withSecond(59)
                                       .withNano(999999999);

    List<GenericBillingReport> weekReports = allYearReports.stream()
        .filter(
            t -> !t.getUsageStartTime().before(DateUtil.toDate(firstDayOfWeek))
                && t.getUsageStartTime().before(DateUtil.toDate(lastDayOfWeek)))
        .collect(Collectors.toList());

    // collect by day of week
    Map<Integer, Double> collectByDayOfWeek = weekReports.stream()
        .collect(Collectors.groupingBy(
            t -> DateUtil.toLocalDate(t.getUsageStartTime()).getDayOfWeek().ordinal(),
            Collectors.summingDouble(t -> t.getUsageCost().doubleValue())));
    // adding the missing days
    ((List<DayOfWeek>) Arrays.asList(DayOfWeek.values())).forEach(t -> {
      DayOfWeek day = (DayOfWeek) t;
      int key = day.ordinal();
      if (!collectByDayOfWeek.containsKey(key)) {
        collectByDayOfWeek.put(key, new Double(0));
      }
    });
    List<Integer> keyList = new ArrayList<Integer>(collectByDayOfWeek.keySet());
    Collections.sort(keyList);
    List<Double> values = new ArrayList<>();
    for (Integer key : keyList) {
      values.add(collectByDayOfWeek.get(key));
    }
    return values;
  }

  /**
   * Execute a calculation for the this month data.
   * 
   * @param today reference date
   * @param allYearReports reports
   * @return List of double values, sorted by date (day) asc
   */
  private static List<Double> executeThisMonthCalculation(LocalDateTime today,
      List<GenericBillingReport> allYearReports) {
    // filter by month
    LocalDateTime firstDayOfMonth = today.with(ChronoField.DAY_OF_MONTH, 1)
                                         .withHour(0)
                                         .withMinute(0)
                                         .withSecond(0)
                                         .withNano(0);
    
    int lengthOfMonth = today.toLocalDate().lengthOfMonth();
    LocalDateTime lastDayOfMonth = today.with(ChronoField.DAY_OF_MONTH, lengthOfMonth)
                                        .withHour(23)
                                        .withMinute(59)
                                        .withSecond(59)
                                        .withNano(999999999);

    List<GenericBillingReport> monthReports =
        allYearReports.stream()
            .filter(t -> !t.getUsageStartTime().before(DateUtil.toDate(firstDayOfMonth))
                && t.getUsageStartTime().before(DateUtil.toDate(lastDayOfMonth)))
            .collect(Collectors.toList());
    
    
    // collect by day
    Map<Integer, Double> collectByDayOfMonth = monthReports.stream()
        .collect(Collectors.groupingBy(
            t -> DateUtil.toLocalDate(t.getUsageStartTime()).getDayOfMonth(),
            Collectors.summingDouble(t -> t.getUsageCost().doubleValue())));
    // adding the missing days
    for (int i = 1; i <= lengthOfMonth; i++) {
      if (!collectByDayOfMonth.containsKey(i)) {
        collectByDayOfMonth.put(i, new Double(0));
      }
    }
    List<Integer> keyList = new ArrayList<Integer>(collectByDayOfMonth.keySet());
    Collections.sort(keyList);
    List<Double> values = new ArrayList<>();
    for (Integer key : keyList) {
      values.add(collectByDayOfMonth.get(key));
    }
    return values;
  }

  /**
   * Execute a calculation for the this year data.
   * 
   * @param allYearReports reports
   * @return List of double values, grouped by month, sorted by date (month) asc
   */
  private static List<Double> executeThisYearCalculation(
      List<GenericBillingReport> allYearReports) {
    Map<Integer, Double> collectByMonth = allYearReports.stream()
        .collect(Collectors.groupingBy(
            t -> DateUtil.toLocalDate(t.getUsageStartTime()).getMonth().ordinal(),
            Collectors.summingDouble(t -> t.getUsageCost().doubleValue())));
    // adding the missing days
    ((List<Month>) Arrays.asList(Month.values())).forEach(t -> {
      Month month = (Month) t;
      if (!collectByMonth.containsKey(month.ordinal())) {
        collectByMonth.put(month.ordinal(), new Double(0));
      }
    });
    List<Integer> keyList = new ArrayList<Integer>(collectByMonth.keySet());
    Collections.sort(keyList);
    List<Double> values = new ArrayList<>();
    for (Integer string : keyList) {
      values.add(collectByMonth.get(string));
    }
    return values;
  }

  /**
   * Retrieve the updated cost summary.
   * 
   * @param provider to filter
   * @return last updated cost summary
   */
  public CostSummary retrieveUpdatedCostSummary(Providers provider) {
    return this.costSummaryRepository.findTopByProviderOrderByTimestampDesc(provider);
  }
}
