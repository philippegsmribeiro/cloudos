package cloudos.costanalysis.costsummary;

import cloudos.Providers;
import org.springframework.data.mongodb.repository.MongoRepository;

interface CostSummaryRepository extends MongoRepository<CostSummary, String> {

  /* Used to return the very last one added */
  CostSummary findTopByProviderOrderByTimestampDesc(Providers provider);
}
