package cloudos.costanalysis.costsummary;

import cloudos.Providers;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "dashboard_cost_summary")
public class CostSummary implements Serializable {

  private static final long serialVersionUID = 4753825641718145317L;

  @Id
  private String id;
  /** Provider of the summary, null if all. */
  private Providers provider;

  /** actual cost for this week, listed by dayOfweek. */
  private List<Double> thisWeek;
  /** actual cost for this week sum. */
  private Double thisWeekSum;
  /** percentage of this week sum over last week sum. */
  private Double thisWeekPercentage;

  /** actual cost for this month, listed by dayOfMonth. */
  private List<Double> thisMonth;
  /** actual cost for this month sum. */
  private Double thisMonthSum;
  /** percentage of this month sum over last month sum. */
  private Double thisMonthPercentage;

  /** actual cost for this year, listed by month. */
  private List<Double> thisYear;
  /** actual cost for this year sum. */
  private Double thisYearSum;
  /** percentage of this year sum over last year sum. */
  private Double thisYearPercentage;

  /** actual cost for the last 7 days, listed by the 7th last day to today. */
  private List<Double> last7days;
  /** actual cost for the last 7 days sum. */
  private Double last7daysSum;

  /** actual cost for the last 30 days, listed by the 30th last day to today. */
  private List<Double> last30days;
  /** actual cost for the last 30 days sum. */
  private Double last30daysSum;

  /** actual cost for the last 4 weeks, listed by the 4th last week to this week. */
  private List<Double> last4weeks;
  /** actual cost for the last 4 weeks sum. */
  private Double last4weeksSum;

  /** average cost per day over the last 7 days. */
  private Double dailyAverage;
  private Double dailyAveragePercentage;

  /** average cost per week over the last 4 weeks. */
  private Double weeklyAverage;
  private Double weeklyAveragePercentage;

  /** average cost per month over the last 3 months. */
  private Double monthlyAverage;
  private Double monthlyAveragePercentage;

  @DateTimeFormat(pattern = "dd-MM-yyyy")
  private Date timestamp;

}
