package cloudos.costanalysis;

import cloudos.models.costanalysis.ProviderTrendEntry;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring data repository interface for {@link ProviderTrendEntry}.
 */
public interface ProviderTrendEntryRepository extends MongoRepository<ProviderTrendEntry, String> {

  ProviderTrendEntry findFirstByOrderByUpdatedDesc();
}
