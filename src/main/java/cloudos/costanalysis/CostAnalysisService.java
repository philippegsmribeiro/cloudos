
package cloudos.costanalysis;

import cloudos.Providers;
import cloudos.amazon.TermType;
import cloudos.billings.AwsBillingReport;
import cloudos.billings.AwsBillingReportRepository;
import cloudos.billings.BillingsConstants;
import cloudos.exceptions.InvalidRequestException;
import cloudos.exceptions.NotImplementedException;
import cloudos.instances.InstanceService;
import cloudos.models.CostProvidersReservationRequest;
import cloudos.models.Instance;
import cloudos.models.InstanceStatus;
import cloudos.models.QInstance;
import cloudos.models.costanalysis.BillingAccount;
import cloudos.models.costanalysis.BillingAccountCost;
import cloudos.models.costanalysis.BillingCost;
import cloudos.models.costanalysis.BillingCostProvider;
import cloudos.models.costanalysis.BillingCostProviderTotal;
import cloudos.models.costanalysis.BillingEstimatedCostMonth;
import cloudos.models.costanalysis.DailyResourceCost;
import cloudos.models.costanalysis.GenericBillingReport;
import cloudos.models.costanalysis.GenericBillingReportQueryFilter;
import cloudos.models.costanalysis.InstanceCost;
import cloudos.models.costanalysis.InstanceCost.InstanceCostBuilder;
import cloudos.models.costanalysis.InstanceTypeCost;
import cloudos.models.costanalysis.LocationCost;
import cloudos.models.costanalysis.OfferingCostByMonths;
import cloudos.models.costanalysis.OfferingReport;
import cloudos.models.costanalysis.OfferingType;
import cloudos.models.costanalysis.OfferingUtilizationReport;
import cloudos.models.costanalysis.ProviderDailyCostEntry;
import cloudos.models.costanalysis.ProviderRegionalCost;
import cloudos.models.costanalysis.ProviderServiceCost;
import cloudos.models.costanalysis.ProviderServiceCostComparisonEntry;
import cloudos.models.costanalysis.ProviderTrendEntry;
import cloudos.models.costanalysis.QGenericBillingReport;
import cloudos.models.costanalysis.ResourceCost;
import cloudos.models.costanalysis.RetrieveInstanceCostRequest;
import cloudos.models.costanalysis.RetrieveProviderServiceCostRequest;
import cloudos.models.costanalysis.ServiceCategoryCost;
import cloudos.models.costanalysis.TemporalCost;
import cloudos.models.services.CloudosService;
import cloudos.models.services.ServiceCategory;
import cloudos.models.services.ServiceRequest;
import cloudos.services.CloudosServiceService;
import cloudos.utils.DateUtil;
import cloudos.utils.JavaSparkContextBuilder;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import java.math.BigDecimal;
import java.math.MathContext;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.validation.constraints.NotNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import scala.Tuple2;
import scala.Tuple3;

/**
 * Service implementation for cost analysis features.
 */
@Service
@Log4j2
public class CostAnalysisService {

  private static final int TOP_FIVE = 5;
  private static final int TOP_TEN = 10;

  @Autowired
  private BillingAccountRepository billingAccountRepository;

  @Autowired
  private AwsBillingReportRepository awsBillingReportRepository;

  @Autowired
  private GenericBillingReportRepository genericBillingReportRepository;

  @Autowired
  private ProviderTrendEntryRepository providerTrendEntryRepository;

  @Autowired
  private InstanceService instanceService;

  @Autowired
  private AsyncTaskExecutor taskExecutor;

  @Autowired
  private CloudosServiceService cloudosServiceService;

  @Autowired
  private ProviderDailyCostEntryRepository providerDailyCostEntryRepository;

  @Autowired
  private DailyResourceCostService dailyResourceCostService;

  /**
   * Retrieves the {@link BillingAccount} collection by {@link Providers}.
   *
   * @param provider the cloud provider
   */
  public List<BillingAccount> findBillingAccountsByProvider(Providers provider) {
    return billingAccountRepository.findByProvider(provider);
  }

  /**
   * Retrieves the {@link BillingAccount} by its id.
   *
   * @param id the billing account id
   * @return the {@link BillingAccount} instance or a null result.
   */
  public BillingAccount findBillingAccountById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    return billingAccountRepository.findOne(id);
  }

  /**
   * List top 10 billing accounts by cost in descending order.
   */
  public List<BillingAccountCost> listTopBillingAccountsByCost(Date startDate, Date endDate,
      Integer limit) {

    if (limit == null || limit == 0) {
      limit = TOP_TEN;
    }

    if (startDate == null && endDate == null) {
      Date now = new Date();
      startDate = DateUtils.addMonths(now, -1);
      endDate = now;
    }

    // billing reports from all providers
    List<GenericBillingReport> billingReportsFromDb =
        genericBillingReportRepository.findByUsageStartTimeBetween(startDate, endDate);

    // query, group and sum the costs
    Map<BillingAccount, Double> dataResult =
        billingReportsFromDb.stream()
            .collect(Collectors.groupingBy(report -> new BillingAccount(
                report.getAccountId() != null ? report.getAccountId() : "0", report.getProvider()),
                Collectors.summingDouble(report -> report.getUsageCost().doubleValue())));

    // prepares and sort the result
    List<BillingAccountCost> result = new ArrayList<>();
    dataResult.entrySet().stream()
        .sorted(Comparator
            .comparingDouble((Map.Entry<BillingAccount, Double> entry) -> entry.getValue())
            .reversed())
        .limit(limit).forEach(entry -> {
          result.add(new BillingAccountCost(entry.getKey().getAccountId(),
              entry.getKey().getProvider(), entry.getValue()));
        });

    return result;
  }

  /**
   * List top 10 billing accounts by cost in descending order.
   */
  public List<BillingAccountCost> listTop10BillingAccounts(Date startDate, Date endDate) {
    return this.listTopBillingAccountsByCost(startDate, endDate, TOP_TEN);
  }

  /**
   * Retrieves the top 10 resource cost, filtered by a date range.
   * 
   * @param start the start date range (inclusive). Defaults to 1 month ago.
   * @param end the end date range (inclusive). Defaults to current date.
   */
  public List<ResourceCost> listTop10ResourcesByCost(LocalDate start, LocalDate end) {

    if (start == null || end == null) {
      end = LocalDate.now();
      start = end.minusMonths(1);
    }

    List<DailyResourceCost> resourceCosts = dailyResourceCostService.findByDateRange(start, end);

    if (resourceCosts.isEmpty()) {
      return Collections.emptyList();
    }

    JavaSparkContext context = JavaSparkContextBuilder.defaultContext();
    List<ResourceCost> collected = context.parallelize(resourceCosts)
        .mapToPair(
            i -> new Tuple2<>(new Tuple2<>(i.getId().getProvider(), i.getId().getResourceId()),
                i.getCost().getTotalCost()))
        .reduceByKey((a, n) -> a.add(n, BillingsConstants.MATH_CONTEXT))
        .map(r -> new ResourceCost(r._1()._1(), r._1()._2(), r._2())).collect();

    return collected.stream().sorted(Comparator.comparing(ResourceCost::getCost).reversed())
        .limit(TOP_TEN).collect(Collectors.toList());
  }

  /**
   * Lists top 5 service categories between a period.
   *
   * @param startDate the start date
   * @param endDate the end date
   * @return the top 5 service categories list.
   */
  public List<ServiceCategoryCost> listTopServiceCategories(Date startDate, Date endDate) {
    return this.listTopServiceCategories(startDate, endDate, TOP_FIVE);
  }

  /**
   * Lists top service categories.
   */
  public List<ServiceCategoryCost> listTopServiceCategories(Date startDate, Date endDate,
      Integer limit) {

    if (limit == null || limit == 0) {
      limit = TOP_FIVE;
    }

    if (startDate == null && endDate == null) {
      Date now = new Date();
      startDate = DateUtils.addMonths(now, -1);
      endDate = now;
    }

    List<GenericBillingReport> billingReportsFromDb =
        genericBillingReportRepository.findByUsageStartTimeBetween(startDate, endDate);
    List<CloudosService> cloudosServices = cloudosServiceService.findAllActive();
    // query, group and sum the costs
    Map<ServiceCategory, Double> dataResult = billingReportsFromDb.stream()
        .collect(Collectors.groupingBy(
            report -> findCategoryByProductCodeProvider(cloudosServices, report.getProductCode(),
                report.getProvider()),
            Collectors.summingDouble(report -> report.getUsageCost().doubleValue())));

    // prepares and sort the result
    List<ServiceCategoryCost> result = new ArrayList<>();
    dataResult.entrySet().stream()
        .sorted(Comparator
            .comparingDouble((Map.Entry<ServiceCategory, Double> entry) -> entry.getValue())
            .reversed())
        .limit(limit).forEach(entry -> {
          result.add(new ServiceCategoryCost(entry.getKey(), entry.getValue()));
        });

    return result;
  }

  /**
   * Method that searches for categories by product code and provider.
   * 
   * @param services the list of CloudosService
   * @param productCode the product code for category
   * @param provider the cloud provider
   * @return ServiceCategory
   */
  private ServiceCategory findCategoryByProductCodeProvider(List<CloudosService> services,
      String productCode, Providers provider) {
    CloudosService serviceCategoryFiltered = services.stream().filter(
        service -> productCode.equals(service.getCode()) && provider.equals(service.getProviders()))
        .findAny().orElse(null);
    if (serviceCategoryFiltered != null) {
      return serviceCategoryFiltered.getServiceCategory();
    } else {
      return ServiceCategory.OTHER;
    }
  }

  /**
   * Method that searches for service by product code and provider.
   * 
   * @param services the list of CloudosService
   * @param productCode the product code for category
   * @param provider the cloud provider
   * @retur String product code service
   */
  private String findServiceByProductCodeProvider(List<CloudosService> services, String productCode,
      Providers provider) {
    CloudosService serviceCategoryFiltered = services.stream().filter(
        service -> productCode.equals(service.getCode()) && provider.equals(service.getProviders()))
        .findAny().orElse(null);
    if (serviceCategoryFiltered != null) {
      return serviceCategoryFiltered.getCode();
    } else {
      return ServiceCategory.OTHER.name();
    }
  }



  /**
   * Lists the top 10 instance types by cost.
   */
  public List<InstanceTypeCost> listTop10InstanceTypesByCost() {
    return instanceService.findTopInstanceTypesOrderByCostDesc(TOP_TEN);
  }

  /**
   * Method that returns the estimated cost of the month by each provider.
   *
   * @return entity with the cost estimates for the current month.
   */
  public BillingEstimatedCostMonth getCostEstimatedMonthCurrentProvider() {
    BillingEstimatedCostMonth estimatedCostOfMonth = new BillingEstimatedCostMonth();
    estimatedCostOfMonth.setMonth(String.valueOf(DateUtil.getCalendarForNow().get(Calendar.MONTH)));

    addCostEachProvider(estimatedCostOfMonth);
    addCostTotalProviders(estimatedCostOfMonth);
    return estimatedCostOfMonth;
  }


  /**
   * Method that returns the cost by provider.
   * 
   * @param provider the cloud provider.
   * @param start the start date.
   * @param end the end date.
   * @return
   */
  public List<BillingCostProviderTotal> getCostByProvider(Providers provider, LocalDate start,
      LocalDate end) {

    if (start == null || end == null) {
      end = LocalDate.now();
      start = end.minusMonths(1);
    }
    Date startDate = DateUtil.toDate(start);
    Date endDate = DateUtil.toDate(end);
    List<BillingCostProviderTotal> result = new ArrayList<>();
    List<GenericBillingReport> billingReportsFromDb =
        (List<GenericBillingReport>) genericBillingReportRepository
            .findAll(filterBillingForProviderAndDataQ(provider, startDate, endDate));

    if (billingReportsFromDb != null) {
      // query, group and sum the costs
      Map<String, Double> dataResult = billingReportsFromDb.stream()
          .collect(Collectors.groupingBy(report -> report.getProvider().name(),
              Collectors.summingDouble(report -> report.getUsageCost().doubleValue())));

      dataResult
          .entrySet().stream().sorted(Comparator
              .comparingDouble((Map.Entry<String, Double> entry) -> entry.getValue()).reversed())
          .forEach(entry -> {
            result.add(
                new BillingCostProviderTotal(Providers.valueOf(entry.getKey()), entry.getValue()));
          });
    }

    return result;
  }

  /**
   * Method that calculates the cost of each provider.
   *
   * @param estimatedCostOfMonth Entity where the costs of each provider will be set.
   */
  private void addCostEachProvider(BillingEstimatedCostMonth estimatedCostOfMonth) {
    Pair<Date, Date> pairMonthAtual = DateUtil.getDateRange();
    Pair<Date, Date> pairPreviusMonth = DateUtil.getDateRangePreviousMonth();

    List<GenericBillingReport> billingReports =
        genericBillingReportRepository.findByUsageStartTimeBetween(pairMonthAtual.getFirst(),
            DateUtil.getDateNowEndToDay().getTime());

    List<GenericBillingReport> billingReportsPreviousMonth = genericBillingReportRepository
        .findByUsageStartTimeBetween(pairPreviusMonth.getFirst(), pairPreviusMonth.getSecond());

    estimatedCostOfMonth.addCostProvider(
        this.findCostByProvider(Providers.AMAZON_AWS, billingReports, billingReportsPreviousMonth));

    estimatedCostOfMonth.addCostProvider(this.findCostByProvider(Providers.GOOGLE_COMPUTE_ENGINE,
        billingReports, billingReportsPreviousMonth));
  }

  /**
   * Method that calculates the total costs of providers.
   *
   * @param estimatedCostOfMonth Entity where the total costs of each provider will be set.
   */
  private void addCostTotalProviders(BillingEstimatedCostMonth estimatedCostOfMonth) {

    BigDecimal estimatedCost = estimatedCostOfMonth.getProviders().stream()
        .map(BillingCostProvider::getEstimatedCost).reduce(BigDecimal.ZERO, BigDecimal::add);

    BigDecimal costPreviousMonth = estimatedCostOfMonth.getProviders().stream()
        .map(BillingCostProvider::getCostPreviousMonth).reduce(BigDecimal.ZERO, BigDecimal::add);

    estimatedCostOfMonth.setEstimatedCost(estimatedCost);
    estimatedCostOfMonth.setCostPreviousMonth(costPreviousMonth);
  }

  /**
   * Method that returns the cost estimated and cost of the previous month of a provider.
   *
   * @param provider the cloud provider.
   * @param billingReports list of billing Reports of the current month.
   * @param billingReportsPreviousMonth list of billing Reports of the previous month.
   * @return entity with the costs of a provider.
   */
  private BillingCostProvider findCostByProvider(Providers provider,
      List<GenericBillingReport> billingReports,
      List<GenericBillingReport> billingReportsPreviousMonth) {

    List<GenericBillingReport> billingReportsFiltered = billingReports.stream()
        .filter(report -> provider.equals(report.getProvider())).collect(Collectors.toList());

    List<GenericBillingReport> billingReportsPreviousMonthFiltered =
        billingReportsPreviousMonth.stream().filter(report -> provider.equals(report.getProvider()))
            .collect(Collectors.toList());

    return mountCostProvider(sumUsageCostGeneric(billingReportsFiltered),
        sumUsageCostGeneric(billingReportsPreviousMonthFiltered), provider);
  }

  /**
   * Method that performs sum of generic billing reports.
   *
   * @param genericBillingReports list of the billing reports
   * @return sum of billing reports
   */
  private BigDecimal sumUsageCostGeneric(List<GenericBillingReport> genericBillingReports) {
    return genericBillingReports.stream().map(GenericBillingReport::getUsageCost)
        .reduce(BigDecimal.ZERO, BigDecimal::add);
  }

  /**
   * Method that mounts the costs of a provider.
   *
   * @param partialCostMonth the provider partial cost of current month
   * @param costPreviousMonth the provider's partial cost of the previous month
   * @param provider the cloud provider
   * @return Entity with the Cost the Month Estimated
   */
  private BillingCostProvider mountCostProvider(BigDecimal partialCostMonth,
      BigDecimal costPreviousMonth, Providers provider) {
    int currentDayOfMonth = DateUtil.getDateNowEndToDay().get(Calendar.DAY_OF_MONTH);
    int endDayOfCurrentMonth = DateUtil.getLastDayCurrentMonth();
    BigDecimal averageCostPerDay =
        partialCostMonth.divide(new BigDecimal(currentDayOfMonth), MathContext.DECIMAL128);
    int daysRemainingMonth = (endDayOfCurrentMonth - currentDayOfMonth);
    BigDecimal costEstimated = new BigDecimal(0);
    for (int i = 0; i < daysRemainingMonth; i++) {
      costEstimated = costEstimated.add(averageCostPerDay);
    }
    BillingCostProvider costProvider = new BillingCostProvider();
    costProvider.setProvider(provider);
    costProvider.setEstimatedCost(partialCostMonth.add(costEstimated));
    costProvider.setCostPreviousMonth(costPreviousMonth);
    return costProvider;
  }


  /**
   * Method that returns the cost of the month by provider and reservation.
   *
   * @param costProvidersReservationRequest Entity that include the cloud provider and the
   *        reservation type of provider.
   * @return entity with the cost estimates for the current month.
   * @throws InvalidRequestException when the reservation is unknown or does not exist.
   * @throws NotImplementedException when the chosen provider calculation is not implemented.
   */
  public BillingCostReservation getCostByProviderAndReservation(
      CostProvidersReservationRequest costProvidersReservationRequest)
      throws InvalidRequestException, NotImplementedException {
    this.validateReservationType(costProvidersReservationRequest);

    BillingCostReservation costReservation = new BillingCostReservation();
    costReservation.setMonth(DateUtil.getMonth());
    costReservation.setProvider(costProvidersReservationRequest.getProvider());
    if (Providers.AMAZON_AWS.equals(costReservation.getProvider())) {
      costReservation.setReservationType(
          TermType.getTermTypeByTerm(costProvidersReservationRequest.getReservation()).toString());
      this.mountCostReservationAws(costReservation.getReservationType(), costReservation);
    } else {
      throw new NotImplementedException();
    }
    return costReservation;
  }

  /**
   * Service that return the cost by service is by provider, and how much each service cost.
   * 
   * @param request Request generic by search services.
   * @return the service list.
   */
  public List<ProviderServiceCost> getCostProviderAndService(
      RetrieveProviderServiceCostRequest request) {
    // prepares and sort the result
    List<ProviderServiceCost> result = new ArrayList<>();

    List<GenericBillingReport> billingReportsFromDb =
        (List<GenericBillingReport>) genericBillingReportRepository
            .findAll(filterBillingForProviderAndServiceQ(request));


    List<CloudosService> cloudosServices = cloudosServiceService.findAllActive();

    if (billingReportsFromDb != null) {
      // query, group and sum the costs
      Map<Pair<String, String>, Double> dataResult = billingReportsFromDb.stream()
          .collect(Collectors.groupingBy(
              report -> Pair.of(findServiceByProductCodeProvider(cloudosServices,
                  report.getProductCode(), report.getProvider()), report.getProvider().name()),
              Collectors.summingDouble(report -> report.getUsageCost().doubleValue())));

      dataResult.entrySet().stream()
          .sorted(Comparator
              .comparingDouble((Map.Entry<Pair<String, String>, Double> entry) -> entry.getValue())
              .reversed())
          .forEach(entry -> {
            result.add(new ProviderServiceCost(Providers.valueOf(entry.getKey().getSecond()),
                entry.getKey().getFirst(), entry.getValue()));
          });
    }
    return result;
  }

  /**
   * Method that mount a filter from entity.
   * 
   * @return Predicate Conditions the filter
   */
  private Predicate filterBillingForProviderAndDataQ(Providers provider, Date start, Date end) {
    BooleanBuilder builder = new BooleanBuilder();

    QGenericBillingReport genericBillingReport = new QGenericBillingReport("genericBillingReport");

    if (provider != null) {
      builder.and(genericBillingReport.provider.eq(provider));
    }
    if (start != null && end != null) {
      builder.and(genericBillingReport.usageStartTime.between(start, end));
    }
    return builder;
  }

  /**
   * Method that mount a filter from RetrieveProviderServiceCostRequest entity.
   * 
   * @param request Request request to filter generic billing report.
   * @return Predicate Conditions the filter
   */
  private Predicate filterBillingForProviderAndServiceQ(
      RetrieveProviderServiceCostRequest request) {
    BooleanBuilder builder = new BooleanBuilder();

    QGenericBillingReport genericBillingReport = new QGenericBillingReport("genericBillingReport");
    if (StringUtils.isNotBlank(request.getService())) {
      builder.and(genericBillingReport.productCode.contains(request.getService()));
    }
    if (request.getProvider() != null) {
      builder.and(genericBillingReport.provider.eq(request.getProvider()));
    }
    if (request.getStartDate() != null && request.getEndDate() != null) {
      builder.and(genericBillingReport.usageStartTime.between(request.getStartDate(),
          request.getEndDate()));
    }
    return builder;
  }

  /**
   * Method that mount a filter from RetrieveInstanceCostRequest entity.
   * 
   * @param request Request request to filter generic billing report.
   * @return Predicate Conditions the filter
   */
  private Predicate filterInstanceByProviderMachine(RetrieveInstanceCostRequest request) {
    BooleanBuilder builder = new BooleanBuilder();
    QInstance instance = new QInstance("instance");
    if (request.getProvider() != null) {
      builder.and(instance.provider.eq(request.getProvider()));
    }
    if (StringUtils.isNotBlank(request.getMachineType())) {
      builder.and(instance.machineType.eq(request.getMachineType()));
    }
    builder.or(instance.status.eq(InstanceStatus.RUNNING))
        .or(instance.status.eq(InstanceStatus.STOPPED));
    builder.and(instance.deleted.eq(false));
    return builder;
  }

  /**
   * Method that mount a filter from RetrieveInstanceCostRequest entity.
   * 
   * @param request Request request to filter generic billing report.
   * @return Predicate Conditions the filter
   */
  private Predicate filterBillingByProvider(RetrieveInstanceCostRequest request) {
    BooleanBuilder builder = new BooleanBuilder();
    QGenericBillingReport genericBillingReport = new QGenericBillingReport("genericBillingReport");
    if (request.getProvider() != null) {
      builder.and(genericBillingReport.provider.eq(request.getProvider()));
    }
    return builder;
  }

  /**
   * Method that validates the reservation type.
   *
   * @param costProvidersReservationRequest Entity that include the cloud provider and the
   *        reservation type of provider.
   */
  private void validateReservationType(
      CostProvidersReservationRequest costProvidersReservationRequest)
      throws InvalidRequestException {
    if (Providers.AMAZON_AWS.equals(costProvidersReservationRequest.getProvider())
        && TermType.getTermTypeByTerm(costProvidersReservationRequest.getReservation()) == null) {
      throw new InvalidRequestException("Reservation is invalid");
    }
  }

  /**
   * Method that mounts the cost by reservation type of the AWS provider.
   *
   * @param reservation the reservation type of AWS provider.
   * @param costReservation the cost reservation type.
   */
  private void mountCostReservationAws(String reservation, BillingCostReservation costReservation) {
    Pair<Date, Date> pairMonthAtual = DateUtil.getDateRange();
    Pair<Date, Date> pairPreviusMonth = DateUtil.getDateRangePreviousMonth();

    List<AwsBillingReport> awsBillingReports =
        awsBillingReportRepository.findBillingsReportUsageStartDateBetweenAndTerm(
            pairMonthAtual.getFirst(), pairMonthAtual.getSecond(), reservation);
    List<AwsBillingReport> awsBillingReportsPreviousMonth =
        awsBillingReportRepository.findBillingsReportUsageStartDateBetweenAndTerm(
            pairPreviusMonth.getFirst(), pairPreviusMonth.getSecond(), reservation);

    costReservation.setCostMonth(sumUsageCostAws(awsBillingReports));
    costReservation.setCostPreviousMonth(sumUsageCostAws(awsBillingReportsPreviousMonth));
  }

  /**
   * Method that performs sum of AWS reports.
   *
   * @param awsBillingReports list of the billing reports
   * @return sum of billing reports
   */
  private double sumUsageCostAws(List<AwsBillingReport> awsBillingReports) {
    return awsBillingReports.stream().mapToDouble(billingReport -> billingReport.getUsageCost())
        .sum();
  }

  /**
   * Get the service category cost between month to date, 30 days and 60 days.
   *
   * @param provider the cloud provider (optional)
   * @param serviceCategory the service category (mandatory)
   */
  public ProviderServiceCostComparisonEntry getServiceCategoryCost60DaysReport(Providers provider,
      @NotNull ServiceCategory serviceCategory) {

    ProviderServiceCostComparisonEntry result = new ProviderServiceCostComparisonEntry();
    result.setServiceCategory(serviceCategory);
    result.setProvider(provider);
    Date now = new Date();
    result.setTimestamp(now);

    Date startOfCurrentMonth = DateUtils.truncate(DateUtils.setDays(now, 1), Calendar.DAY_OF_MONTH);
    Date oneMonthAgo = DateUtils.truncate(DateUtils.addMonths(now, -1), Calendar.DAY_OF_MONTH);
    Date twoMonthsAgo = DateUtils.truncate(DateUtils.addMonths(now, -2), Calendar.DAY_OF_MONTH);

    Set<String> productCodes = getProductCodesFromServiceCategory(serviceCategory, provider);

    // 60 days
    TemporalCost cost60Days = getBillingReportsAsTemporalCost(productCodes, twoMonthsAgo, now);
    result.setTotal60DaysCost(cost60Days);

    // 30 days
    TemporalCost cost30Days = getBillingReportsAsTemporalCost(productCodes, oneMonthAgo, now);
    result.setTotal30DaysCost(cost30Days);

    // month to date
    TemporalCost costMonthToDate =
        getBillingReportsAsTemporalCost(productCodes, startOfCurrentMonth, now);
    result.setMonthToDateCost(costMonthToDate);

    return result;
  }

  /**
   * Get the offering cost between month to date, 30 days and 60 days.
   * 
   * @return the {@link OfferingCostByMonths}
   */
  public OfferingCostByMonths getOfferingCostByMonthComparison() {

    OfferingCostByMonths result = new OfferingCostByMonths();

    // 60 days
    result.setCost60DaysReport(
        getCostOfferingByPeriod(LocalDate.now().minusMonths(3), LocalDate.now().minusMonths(2)));
    result.setTotal60DaysCost(result.getCost60DaysReport().stream().map(p -> p.getCost())
        .reduce(BigDecimal.ZERO, (b1, b2) -> b1.add(b2)));
    // 30 days
    result.setCost30DaysReport(
        getCostOfferingByPeriod(LocalDate.now().minusMonths(2), LocalDate.now().minusMonths(1)));
    result.setTotal30DaysCost(result.getCost30DaysReport().stream().map(p -> p.getCost())
        .reduce(BigDecimal.ZERO, (b1, b2) -> b1.add(b2)));
    // month to date
    result.setCostMonthToDateReport(
        getCostOfferingByPeriod(LocalDate.now().minusMonths(1), LocalDate.now()));
    result.setTotalMonthToDate(result.getCostMonthToDateReport().stream().map(p -> p.getCost())
        .reduce(BigDecimal.ZERO, (b1, b2) -> b1.add(b2)));
    return result;
  }

  /**
   * Method that calculate offering cost by Period.
   * 
   * @param start the start date to filter
   * @param end the end date to filter
   * @return List of the {@link OfferingReport}
   */
  private List<OfferingReport> getCostOfferingByPeriod(LocalDate start, LocalDate end) {
    List<OfferingUtilizationReport> costDays = retrieveOfferingUtilization(start, end);
    JavaSparkContext javaSparkContext = JavaSparkContextBuilder.defaultContext();
    // Group the billing reports by provider
    JavaRDD<OfferingUtilizationReport> reportsRdd = javaSparkContext.parallelize(costDays);
    JavaPairRDD<OfferingType, Iterable<OfferingUtilizationReport>> groupBy =
        reportsRdd.groupBy(OfferingUtilizationReport::getType);
    Map<OfferingType, Iterable<OfferingUtilizationReport>> providerReportsMap =
        groupBy.collectAsMap();

    List<OfferingReport> offeringReports = new ArrayList<>();
    for (final Entry<OfferingType, Iterable<OfferingUtilizationReport>> entry : providerReportsMap
        .entrySet()) {
      OfferingReport report = OfferingReport.builder()
          .cost(((Collection<OfferingUtilizationReport>) entry.getValue()).stream()
              .map(p -> p.getCost().getCost()).reduce(BigDecimal.ZERO, (b1, b2) -> b1.add(b2)))
          .discount(((Collection<OfferingUtilizationReport>) entry.getValue()).stream()
              .map(p -> p.getCost().getDiscount()).reduce(BigDecimal.ZERO, (b1, b2) -> b1.add(b2)))
          .totalCost(((Collection<OfferingUtilizationReport>) entry.getValue()).stream()
              .map(p -> p.getCost().getTotalCost()).reduce(BigDecimal.ZERO, (b1, b2) -> b1.add(b2)))
          .type(entry.getKey()).build();
      offeringReports.add(report);
    }
    return offeringReports;
  }

  /**
   * Method that returns the list of {@link GenericBillingReport} by the given filter.
   *
   * @param filter the filter to apply to the query (mandatory)
   * @return a list of GenericBillingReport or null if a null filter is provided
   */
  public List<GenericBillingReport> getBillingReportsByFilter(
      GenericBillingReportQueryFilter filter) {
    if (filter == null) {
      return null;
    }

    return genericBillingReportRepository.findByFilter(filter);
  }

  /**
   * Retrieves the billing report by its id.
   *
   * @param id the billing report it
   * @return the billing report object
   */
  public GenericBillingReport getBillingReportById(String id) {
    return genericBillingReportRepository.findOne(id);
  }

  /**
   * Retrieves the {@link GenericBillingReport} between the startDate and endDate and converts it as
   * a {@link TemporalCost} object.
   *
   * @return a {@link TemporalCost} instance
   */
  private TemporalCost getBillingReportsAsTemporalCost(Set<String> productCodes, Date startDate,
      Date endDate) {

    TemporalCost result = new TemporalCost(startDate, endDate);

    List<GenericBillingReport> dbResult = genericBillingReportRepository
        .findByProductCodeInAndUsageStartTimeIsBetween(productCodes, startDate, endDate);

    if (CollectionUtils.isNotEmpty(dbResult)) {
      JavaRDD<GenericBillingReport> reportsRdd =
          JavaSparkContextBuilder.defaultContext().parallelize(dbResult);

      // _1() = cost
      // _2() = discount
      JavaRDD<Tuple2<BigDecimal, BigDecimal>> costDiscountRdd = reportsRdd.map(r -> {

        BigDecimal cost = BigDecimal.ZERO;
        BigDecimal discount = BigDecimal.ZERO;

        if (BigDecimal.ZERO.compareTo(r.getUsageCost()) > 0) {
          discount = r.getUsageCost();
        } else {
          cost = r.getUsageCost();
        }

        return new Tuple2<>(cost, discount);
      });

      Tuple2<BigDecimal, BigDecimal> costDiscountsReduced = costDiscountRdd
          .reduce((acum, n) -> new Tuple2<>(acum._1().add(n._1(), BillingsConstants.MATH_CONTEXT),
              acum._2().add(n._2(), BillingsConstants.MATH_CONTEXT))

      );

      result.setCost(costDiscountsReduced._1());
      result.setDiscount(costDiscountsReduced._2());
      result.setTotalCost(
          costDiscountsReduced._1().add(costDiscountsReduced._2(), BillingsConstants.MATH_CONTEXT));
    }

    return result;
  }

  /**
   * Returns the product codes for a specific {@link ServiceCategory}, it can be filtered by
   * {@link Providers}.
   *
   * @param serviceCategory the service category (mandatory)
   * @param provider the cloud provider (optional)
   * @return the product codes collection or empty.
   */
  private Set<String> getProductCodesFromServiceCategory(ServiceCategory serviceCategory,
      Providers provider) {
    Set<String> productCodes = new HashSet<>();
    List<CloudosService> cloudosServiceResult;

    cloudosServiceResult = cloudosServiceService.findByIdProvidersAndIdServiceCategory(
        ServiceRequest.builder().providers(provider).serviceCategory(serviceCategory).build());

    for (CloudosService cloudosService : cloudosServiceResult) {
      productCodes.add(cloudosService.getCode());
    }
    return productCodes;
  }

  /**
   * Retrieves the instance cost.
   *
   * @param provider the cloud provider
   * @param machineType the resource type
   * @param startDate the start date to filter (inclusive)
   * @param endDate the end date to filter (inclusive)
   * @return a list of instance cost
   */
  public List<InstanceCost> retrieveInstanceCost(@NotNull Providers provider,
      @NotNull String machineType, LocalDate startDate, LocalDate endDate) {

    if (log.isDebugEnabled()) {
      log.debug("going to retrieve instance cost by params: ",
          StringUtils.join(new Object[] {provider, machineType, startDate, endDate}));
    }

    Date start = DateUtil.toDate(startDate);
    Date end = DateUtil.toDate(endDate);

    List<Instance> instances = instanceService.findByProviderAndMachineTypeAndDateBetween(provider,
        machineType, start, end);
    if (instances.isEmpty()) {
      return Collections.emptyList();
    }

    List<GenericBillingReport> reports =
        genericBillingReportRepository.findByProviderAndUsageStartTimeBetween(provider, start, end);
    if (reports.isEmpty()) {
      return Collections.emptyList();
    }

    JavaSparkContext javaSparkContext = JavaSparkContextBuilder.defaultContext();

    JavaRDD<Instance> instancesRdd = javaSparkContext.parallelize(instances);

    JavaPairRDD<String, Iterable<Instance>> left = instancesRdd.groupBy(Instance::getProviderId);

    JavaRDD<GenericBillingReport> reportsRdd = javaSparkContext.parallelize(reports);
    JavaPairRDD<String, BigDecimal> right =
        reportsRdd.mapToPair(r -> new Tuple2<>(r.getResourceId(), r.getUsageCost()))
            .reduceByKey((x, y) -> x.add(y, BillingsConstants.MATH_CONTEXT));

    JavaPairRDD<String, Tuple2<Iterable<Instance>, Optional<BigDecimal>>> result =
        left.leftOuterJoin(right);

    final List<InstanceCost> instanceCostList = new ArrayList<>();
    result.collect().forEach(item -> {
      String resourceId = item._1;
      BigDecimal usageCost = item._2()._2().isPresent() ? item._2()._2().get() : BigDecimal.ZERO;
      BigDecimal discount =
          BigDecimal.ZERO.compareTo(usageCost) > 0 ? usageCost.abs(BillingsConstants.MATH_CONTEXT)
              : BigDecimal.ZERO;
      BigDecimal cost = discount.compareTo(BigDecimal.ZERO) > 0 ? BigDecimal.ZERO : usageCost;

      InstanceCost instanceCost = new InstanceCost();
      instanceCost.setProvider(provider);
      instanceCost.setInstance(resourceId);
      instanceCost.setCost(cost);
      instanceCost.setDiscount(discount);
      instanceCost.setTotalCost(usageCost);

      instanceCostList.add(instanceCost);
    });

    return instanceCostList;
  }

  /**
   * Retrieves the instance cost.
   *
   * @param request the end date to filter (inclusive)
   * @return a list of instance cost
   */
  public List<InstanceCost> retrieveInstanceCost(RetrieveInstanceCostRequest request) {

    List<Instance> instances = new ArrayList<>();
    if (request.getPage() == 0 && request.getPageSize() == 0) {
      instances = instanceService.findInstancesByFilter(filterInstanceByProviderMachine(request));
    } else {
      Pageable pageable = new PageRequest(request.getPage(), request.getPageSize(), Direction.DESC,
          Instance.CREATION_DATE, Instance.ID);
      Page<Instance> pageInstances = instanceService
          .findInstancesByFilter(filterInstanceByProviderMachine(request), pageable);
      if (pageInstances != null && pageInstances.getContent() != null) {
        instances.addAll(pageInstances.getContent());
      }
    }
    if (instances.isEmpty()) {
      return Collections.emptyList();
    }

    List<GenericBillingReport> billings =
        (List<GenericBillingReport>) genericBillingReportRepository
            .findAll(filterBillingByProvider(request));

    if (billings.isEmpty()) {
      return Collections.emptyList();
    }

    return processInstancesAndBillingsReport(instances, billings);
  }

  /**
   * Method that performs the merge between instances and billings reports.
   * 
   * @param instances List of instances
   * @param billings List of billings report
   * @return a list of instance cost
   */
  private List<InstanceCost> processInstancesAndBillingsReport(List<Instance> instances,
      List<GenericBillingReport> billings) {
    JavaSparkContext javaSparkContext = JavaSparkContextBuilder.defaultContext();

    JavaRDD<Instance> instancesRdd = javaSparkContext.parallelize(instances);
    JavaPairRDD<String, Iterable<Instance>> groupInstance =
        instancesRdd.groupBy(Instance::getProviderId);

    JavaRDD<GenericBillingReport> billingsRdd = javaSparkContext.parallelize(billings);
    JavaPairRDD<String, BigDecimal> sumBillings = billingsRdd
        .mapToPair(billing -> new Tuple2<>(billing.getResourceId(), billing.getUsageCost()))
        .reduceByKey((x, y) -> x.add(y, BillingsConstants.MATH_CONTEXT));

    JavaPairRDD<String, Tuple2<Iterable<Instance>, BigDecimal>> resultJoin =
        groupInstance.join(sumBillings);

    final List<InstanceCost> instanceCostList = new ArrayList<>();
    resultJoin.collect().forEach(item -> {
      BigDecimal totalCost = item._2()._2();
      BigDecimal discount =
          BigDecimal.ZERO.compareTo(totalCost) > 0 ? totalCost.abs(BillingsConstants.MATH_CONTEXT)
              : BigDecimal.ZERO;
      BigDecimal cost = discount.compareTo(BigDecimal.ZERO) > 0 ? BigDecimal.ZERO : totalCost;
      InstanceCostBuilder instanceCostBuilder = InstanceCost.builder().instance(item._1).cost(cost)
          .discount(discount).totalCost(totalCost);
      instanceCostBuilder.provider(item._2._1.iterator().next().getProvider())
          .region(item._2._1.iterator().next().getRegion());

      instanceCostList.add(instanceCostBuilder.build());
    });
    return instanceCostList;
  }



  /**
   * Retrieves provider resource costs filtered by region.
   * 
   * @param startDate the start date
   * @param endDate the end date
   * @return a map of provider/location/cost
   */
  public List<ProviderRegionalCost> retrieveProviderRegionalCosts(@NotNull LocalDate startDate,
      @NotNull LocalDate endDate) {

    List<GenericBillingReport> reports = (List<GenericBillingReport>) genericBillingReportRepository
        .findAll(filterGenericBillingByDate(DateUtil.toDate(startDate), DateUtil.toDate(endDate)));

    Map<Providers, List<GenericBillingReport>> reportMap =
        reports.stream().collect(Collectors.groupingBy(GenericBillingReport::getProvider));
    List<ProviderRegionalCost> result = new ArrayList<>();
    for (final Entry<Providers, List<GenericBillingReport>> entry : reportMap.entrySet()) {

      List<GenericBillingReport> billingReports =
          StreamSupport.stream(entry.getValue().spliterator(), false).collect(Collectors.toList());

      Map<String, BigDecimal> calculateRegionsReports = billingReports.stream()
          .collect(Collectors.groupingBy(GenericBillingReport::getLocation, Collectors
              .reducing(BigDecimal.ZERO, GenericBillingReport::getUsageCost, BigDecimal::add)));

      List<LocationCost> locationCosts = new ArrayList<LocationCost>();

      calculateRegionsReports.entrySet().stream().forEach(report -> {
        locationCosts.add(new LocationCost(report.getKey(), report.getValue()));
      });

      result.add(new ProviderRegionalCost(entry.getKey(), locationCosts));
    }
    return result;

  }

  /**
   * Method that filter from GenericBillingReport entity.
   * 
   * @param startDate the start date
   * @param endDate the end date
   * @return Predicate Conditions the filter
   */
  private Predicate filterGenericBillingByDate(Date startDate, Date endDate) {
    BooleanBuilder builder = new BooleanBuilder();
    QGenericBillingReport genericBillingReport = new QGenericBillingReport("genericBillingReport");
    if (startDate != null && endDate != null) {
      builder.and(genericBillingReport.usageStartTime.between(startDate, endDate));
    }
    return builder;
  }

  /**
   * Triggers the execution of the provider trend job.
   */
  public void triggerProviderTrendJob() {
    log.info("triggering the execution of the provider trend job");
    taskExecutor.execute(
        () -> new ProviderTrendJob(genericBillingReportRepository, providerTrendEntryRepository)
            .execute());
  }

  /**
   * Triggers the provider daily cost job.
   */
  public void triggerProviderDailyCostJob(LocalDate start, LocalDate end) {
    log.info("triggering the execution of the provider daily cost job");
    taskExecutor.execute(() -> new ProviderDailyCostJob(genericBillingReportRepository,
        providerDailyCostEntryRepository).updateCost(start, end));
  }

  /**
   * Triggers the resource daily cost job.
   */
  public void triggerResourceDailyCost(LocalDate start, LocalDate end) {
    log.info("triggering the execution of the resource daily cost job");
    taskExecutor.execute(
        () -> new DailyResourceCostJob(genericBillingReportRepository, dailyResourceCostService)
            .execute(start, end));
  }

  /**
   * Returns the latest provider trend available.
   */
  public ProviderTrendEntry getLatestProviderTrend() {
    return providerTrendEntryRepository.findFirstByOrderByUpdatedDesc();
  }

  /**
   * Retrieves the offering utilization between a range of dates. If the range is not provided the
   * default range is one month.
   * 
   * @param start the start date, inclusive.
   * @param end the end date, inclusive.
   */
  public List<OfferingUtilizationReport> retrieveOfferingUtilization(LocalDate start,
      LocalDate end) {

    if (start == null || end == null) {
      end = LocalDate.now();
      start = end.minusMonths(1);
    }

    Date startDate = DateUtil.toDate(start);
    Date endDate = DateUtil.toDate(end);

    List<GenericBillingReport> reports =
        genericBillingReportRepository.findByUsageStartTimeBetween(startDate, endDate);

    if (reports.isEmpty()) {
      return Collections.emptyList();
    }

    List<Instance> instances = instanceService.findByCreationDateBefore(endDate);

    if (instances.isEmpty()) {
      return Collections.emptyList();
    }

    JavaSparkContext javaSparkContext = JavaSparkContextBuilder.defaultContext();

    JavaRDD<GenericBillingReport> reportsRdd = javaSparkContext.parallelize(reports);

    JavaPairRDD<Tuple2<Providers, String>, GenericBillingReport> left = reportsRdd
        .mapToPair(r -> new Tuple2<>(new Tuple2<>(r.getProvider(), r.getResourceId()), r));

    JavaRDD<Instance> instancesRdd = javaSparkContext.parallelize(instances);

    /* get the number of instances per group */
    final Map<Tuple3<Providers, Boolean, Boolean>, Long> instancesGroupCount = instancesRdd
        .groupBy(item -> new Tuple3<>(item.getProvider(), item.isSpot(), item.isReserved()))
        .mapToPair(item -> {
          Long count = item._2().spliterator().getExactSizeIfKnown();
          return new Tuple2<>(item._1(), count);
        }).collectAsMap();

    JavaPairRDD<Tuple2<Providers, String>, Instance> right = instancesRdd
        .mapToPair(i -> new Tuple2<>(new Tuple2<>(i.getProvider(), i.getProviderId()), i));

    /*
     * Convert the combination of GenericBillingReport + Instance into a single
     * OfferingUtilizationRerport
     */
    JavaRDD<OfferingUtilizationReport> offUtilReports = left.join(right).map(item -> {
      Tuple2<GenericBillingReport, Instance> reportInstance = item._2();
      GenericBillingReport report = reportInstance._1();
      Instance instance = reportInstance._2();

      Providers provider = instance.getProvider();
      Boolean spot = instance.isSpot();
      Boolean reserved = instance.isReserved();
      BillingCost cost = new BillingCost(report.getUsageCost());

      long diff =
          DateUtil.getDifferenceInMillis(report.getUsageStartTime(), report.getUsageEndTime());
      BigDecimal hours = new BigDecimal(diff / 1000 / 60 / 60);

      return new OfferingUtilizationReport(provider, spot, reserved, cost, hours, 1L);
    });

    /*
     * group the reports (Provider/Spot/Reserved) in order to be able to reduce and count by groups
     */
    Set<Entry<Tuple2<Providers, OfferingType>, Iterable<OfferingUtilizationReport>>> entrySet =
        offUtilReports.groupBy(item -> new Tuple2<>(item.getProvider(), item.getType()))
            .collectAsMap().entrySet();

    List<OfferingUtilizationReport> result = entrySet.stream().map(entry -> {

      Tuple2<Providers, OfferingType> key = entry.getKey();
      Iterable<OfferingUtilizationReport> value = entry.getValue();

      List<OfferingUtilizationReport> target = new ArrayList<>();
      value.iterator().forEachRemaining(target::add);

      BigDecimal cost = BigDecimal.ZERO;
      BigDecimal hours = BigDecimal.ZERO;

      for (OfferingUtilizationReport r : target) {
        cost = cost.add(r.getCost().getTotalCost(), BillingsConstants.MATH_CONTEXT);
        hours = hours.add(r.getHours(), BillingsConstants.MATH_CONTEXT);
      }

      Boolean spot = key._2() == OfferingType.SPOT;
      Boolean reserved = key._2() == OfferingType.RESERVED;
      Tuple3<Providers, Boolean, Boolean> key2 = new Tuple3<>(key._1(), spot, reserved);

      Long instancesCount = ObjectUtils.defaultIfNull(instancesGroupCount.get(key2), 0L);
      return new OfferingUtilizationReport(key._1(), key._2(), new BillingCost(cost), hours,
          instancesCount);
    }).collect(Collectors.toList());

    return result;
  }

  /**
   * Retrieve a list of {@link ProviderDailyCostEntry} which its date is between a range of dates
   * and provide. If start or end dates are null, it will be used the default range of one month
   * ago, starting from current date. if the provider is null it will return unfiltered by the
   * provider
   */
  public List<ProviderDailyCostEntry> retrieveProviderDailyCostEntries(Providers provider,
      LocalDate start, LocalDate end) {

    if (start == null || end == null) {
      end = LocalDate.now();
      start = end.minusMonths(1);
    }
    List<ProviderDailyCostEntry> result = null;
    if (provider == null) {
      result = providerDailyCostEntryRepository.findByDateBetween(start, end);
    } else {
      result = providerDailyCostEntryRepository.findByProviderAndDateBetween(provider, start, end);
    }
    return result;
  }



}
