package cloudos.costanalysis;

import cloudos.Providers;
import cloudos.models.costanalysis.ProviderDailyCostEntry;
import java.time.LocalDate;
import java.util.List;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/**
 * Spring data repository for {@link ProviderDailyCostEntry} collection.
 */
public interface ProviderDailyCostEntryRepository
    extends MongoRepository<ProviderDailyCostEntry, String> {

  @Cacheable("providerDailyCostEntry_findByDateBetween")
  @Query(value = "{'date':{ $gte: ?0, $lte: ?1}}")
  List<ProviderDailyCostEntry> findByDateBetween(LocalDate start, LocalDate end);

  @Cacheable("providerDailyCostEntry_findByProviderAndDateBetween")
  @Query(value = "{'provider': ?0, 'date':{ $gte: ?1, $lte: ?2}}")
  List<ProviderDailyCostEntry> findByProviderAndDateBetween(Providers provider, LocalDate start,
      LocalDate end);
}
