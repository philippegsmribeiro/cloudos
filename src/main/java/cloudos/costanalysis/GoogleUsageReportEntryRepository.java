package cloudos.costanalysis;

import cloudos.models.costanalysis.GoogleUsageReportEntry;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/**
 * Spring data repository for {@link cloudos.models.costanalysis.GoogleUsageReportEntry}
 * collection.
 */
public interface GoogleUsageReportEntryRepository extends
    MongoRepository<GoogleUsageReportEntry, String> {

  List<GoogleUsageReportEntry> findByReportDateAndMeasurementIdAndNumericProjectId(
      LocalDate reportDate,
      String measurementId, Long numericProjectId);

  @Query(value = "{ 'reportDate':{ $gte: ?1, $lte: ?2}}")
  List<GoogleUsageReportEntry> findByReportDateBetween(LocalDate start, LocalDate end);

  List<GoogleUsageReportEntry> findByReportDate(LocalDate reportDate);
}
