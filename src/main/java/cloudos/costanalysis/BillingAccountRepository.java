package cloudos.costanalysis;

import cloudos.Providers;
import cloudos.models.costanalysis.BillingAccount;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BillingAccountRepository extends MongoRepository<BillingAccount, String> {

  List<BillingAccount> findByProvider(Providers provider);

  List<BillingAccount> findByProviderAndAccountId(Providers provider, String accountId);

}
