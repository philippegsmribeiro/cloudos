package cloudos.costanalysis;

import cloudos.billings.AwsBillingReport;
import cloudos.models.costanalysis.GenericBillingReport;
import cloudos.models.costanalysis.GenericBillingReportQueryFilter;
import cloudos.models.costanalysis.GoogleBillingReconciliationEntry;
import java.util.List;

/**
 * Custom spring data repository interface for {@link GenericBillingReport}.
 */
public interface GenericBillingReportRepositoryCustom {

  /**
   * Converts a {@link AwsBillingReport} instance into a {@link GenericBillingReport} and saves it.
   */
  GenericBillingReport save(AwsBillingReport awsBillingReport);

  /**
   * Converts a {@link cloudos.models.costanalysis.GoogleBillingReconciliationEntry} instance into a
   * {@link GenericBillingReport} and saves it.
   */
  GenericBillingReport save(GoogleBillingReconciliationEntry googleBillingReconciliationEntry);
  
  /**
   * Save in batch.
   * 
   * @param entries to be saved.
   * @return list of saved reports.
   */
  List<GenericBillingReport> save(List<GoogleBillingReconciliationEntry> entries);
  
  /**
   * Save in batch.
   * 
   * @param reports to be saved.
   * @return list of saved reports.
   */
  List<GenericBillingReport> saveAll(List<AwsBillingReport> reports);

  /**
   * Retrives a list of {@link GenericBillingReport} by a given
   * {@link GenericBillingReportQueryFilter}.
   *
   * @param filter the filter
   */
  List<GenericBillingReport> findByFilter(GenericBillingReportQueryFilter filter);
}
