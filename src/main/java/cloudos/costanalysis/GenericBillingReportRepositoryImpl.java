package cloudos.costanalysis;

import cloudos.Providers;
import cloudos.billings.AwsBillingReport;
import cloudos.models.costanalysis.GenericBillingReport;
import cloudos.models.costanalysis.GenericBillingReportQueryFilter;
import cloudos.models.costanalysis.GoogleBillingReconciliationEntry;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.StringMatcher;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

/**
 * Custom spring data repository implementation for {@link GenericBillingReport}.
 */
public class GenericBillingReportRepositoryImpl implements GenericBillingReportRepositoryCustom {

  private static final String CREDIT = "Credit";
  private static final String ID_SEPARATOR = "_";
  private static final String REGEX_ALPHANUMERIC_AND_UNDERSCORE = "[^_A-Za-z0-9]";
  private static final String AWS_RESOURCE_ID_SEPARATOR = "/";
  private static final String ANOTHER_LOCATION = "Another location";

  /**
   * Injection of spring data generic billing report repository in order to use common methods.
   */
  @Autowired
  private GenericBillingReportRepository repository;

  @Autowired
  private MongoTemplate mongoTemplate;

  @Override
  public GenericBillingReport save(AwsBillingReport awsReport) {

    // unique identifier
    final String id = getId(awsReport.getProvider(), awsReport.getLineItemId());

    BigDecimal usageCost = awsReport.getUsageCost() == null ? BigDecimal.ZERO
        : BigDecimal.valueOf(awsReport.getUsageCost());

    String resourceId = getAwsRealResourceId(awsReport.getResourceId());

    // separating the credit values
    boolean credit = CREDIT.equals(awsReport.getLineItemType());
    GenericBillingReport billingReport =
        GenericBillingReport.builder().id(id).lineItemId(awsReport.getLineItemId())
            .provider(awsReport.getProvider()).accountId(awsReport.getPayerAccountId())
            .productCode(awsReport.getProductCode()).resourceId(resourceId)
            .usageStartTime(awsReport.getUsageStartDate()).usageEndTime(awsReport.getUsageEndDate())
            .usageCost(usageCost).location(getAwsLocation(awsReport)).credit(credit).build();

    awsReport.setBillingReport(billingReport.getId());
    return repository.save(billingReport);
  }

  @Override
  public GenericBillingReport save(GoogleBillingReconciliationEntry googleEntry) {

    String id = getId(Providers.GOOGLE_COMPUTE_ENGINE, googleEntry.getId().getReportDate(),
        googleEntry.getId().getMeasurementId(), googleEntry.getId().getResourceId());
    String measurementId = googleEntry.getMeasurementId();
    Providers provider = Providers.GOOGLE_COMPUTE_ENGINE;
    String accountId = googleEntry.getAccountId();
    String productCode = googleEntry.getMeasurementId();
    String resourceId = googleEntry.getResourceId();
    Date usageStartTime = googleEntry.getStartTime();
    Date usageEndTime = googleEntry.getEndTime();
    BigDecimal usageCost = googleEntry.getCostAmount();
    String location = googleEntry.getLocation();

    GenericBillingReport billingReport = GenericBillingReport.builder().id(id)
        .lineItemId(measurementId).provider(provider).accountId(accountId).productCode(productCode)
        .resourceId(resourceId).usageStartTime(usageStartTime).usageEndTime(usageEndTime)
        .usageCost(usageCost).location(location).credit(false).build();

    return repository.save(billingReport);
  }

  @Override
  public List<GenericBillingReport> save(List<GoogleBillingReconciliationEntry> entries) {
    List<GenericBillingReport> list = new ArrayList<>();
    for (GoogleBillingReconciliationEntry googleEntry : entries) {
      String id = getId(Providers.GOOGLE_COMPUTE_ENGINE, googleEntry.getId().getReportDate(),
          googleEntry.getId().getMeasurementId(), googleEntry.getId().getResourceId());
      String measurementId = googleEntry.getMeasurementId();
      Providers provider = Providers.GOOGLE_COMPUTE_ENGINE;
      String accountId = googleEntry.getAccountId();
      String productCode = googleEntry.getMeasurementId();
      String resourceId = googleEntry.getResourceId();
      Date usageStartTime = googleEntry.getStartTime();
      Date usageEndTime = googleEntry.getEndTime();
      BigDecimal usageCost = googleEntry.getCostAmount();
      String location = googleEntry.getLocation();

      GenericBillingReport billingReport = GenericBillingReport.builder().id(id)
          .lineItemId(measurementId).provider(provider).accountId(accountId)
          .productCode(productCode).resourceId(resourceId).usageStartTime(usageStartTime)
          .usageEndTime(usageEndTime).usageCost(usageCost).location(location).credit(false).build();
      list.add(billingReport);
    }
    return repository.save(list);
  }

  @Override
  public List<GenericBillingReport> saveAll(List<AwsBillingReport> reports) {
    List<GenericBillingReport> list = new ArrayList<>();
    for (AwsBillingReport awsReport : reports) {
      // unique identifier - add date as it was ignoring some items (please don't remove it)
      final String id = getId(awsReport.getProvider(), awsReport.getLineItemId(),
          awsReport.getUsageStartDate(), awsReport.getUsageEndDate());

      BigDecimal usageCost = awsReport.getUsageCost() == null ? BigDecimal.ZERO
          : BigDecimal.valueOf(awsReport.getUsageCost());

      String resourceId = getAwsRealResourceId(awsReport.getResourceId());

      boolean credit = CREDIT.equals(awsReport.getLineItemType());

      GenericBillingReport billingReport = GenericBillingReport.builder().id(id)
          .lineItemId(awsReport.getLineItemId()).provider(awsReport.getProvider())
          .accountId(awsReport.getPayerAccountId()).productCode(awsReport.getProductCode())
          .resourceId(resourceId).usageStartTime(awsReport.getUsageStartDate())
          .usageEndTime(awsReport.getUsageEndDate()).usageCost(usageCost)
          .location(getAwsLocation(awsReport)).credit(credit).build();

      awsReport.setBillingReport(billingReport.getId());

      list.add(billingReport);
    }
    return repository.save(list);
  }


  /**
   * Retrieves the region cost AWS.
   * 
   * @param AwsBillingReport billing report.
   */
  private String getAwsLocation(AwsBillingReport awsBillingReport) {
    String result = ANOTHER_LOCATION;
    if (StringUtils.isNotBlank(awsBillingReport.getRegion())) {
      result = awsBillingReport.getRegion();
    } else if (StringUtils.isNotBlank(awsBillingReport.getRegion())) {
      result = awsBillingReport.getAvailabilityZone();
    }
    return result;
  }

  /**
   * Retrieves the real AWS resource id.
   *
   *
   * <pre>
   *  &#64;{code
   * "arn:aws:ec2:us-west-1:079337523389:instance/i-0d2c739e0c836db7e" becomes
   * "i-0d2c739e0c836db7e"
   * }
   * </pre>
   *
   * @param resourceId the raw AWS resource id.
   */
  private String getAwsRealResourceId(String resourceId) {
    if (StringUtils.contains(resourceId, AWS_RESOURCE_ID_SEPARATOR)) {
      return StringUtils.substringAfterLast(resourceId, AWS_RESOURCE_ID_SEPARATOR);
    } else {
      return resourceId;
    }
  }

  /**
   * Generates and unique ID by concatenating the params and removing non alphanumeric characters.
   */
  public static String getId(Providers provider, Object... params) {

    if (provider == null) {
      throw new IllegalArgumentException("provider must not be null");
    }

    if (params == null || params.length == 0) {
      throw new IllegalArgumentException("at least one param is necessary to create an ID");
    }

    String generatedId = StringUtils.join(params, ID_SEPARATOR);
    generatedId = StringUtils.join(new Object[] {provider.toString(), generatedId}, ID_SEPARATOR);
    generatedId = StringUtils.stripAccents(generatedId);
    generatedId =
        StringUtils.replaceAll(generatedId, REGEX_ALPHANUMERIC_AND_UNDERSCORE, StringUtils.EMPTY);
    return generatedId;
  }

  @Override
  public List<GenericBillingReport> findByFilter(GenericBillingReportQueryFilter filter) {

    if (filter == null) {
      return null;
    }

    Query query = new Query();

    // usage start timestamp filter
    if (filter.getStartDateBeforeOrEqualTo() != null
        || filter.getStartDateAfterOrEqualTo() != null) {

      Criteria usageStartTimeCriteria = Criteria.where(GenericBillingReport.FIELD_USAGE_START_TIME);
      if (filter.getStartDateAfterOrEqualTo() != null) {
        usageStartTimeCriteria.gte(filter.getStartDateAfterOrEqualTo());
      }

      if (filter.getStartDateBeforeOrEqualTo() != null) {
        usageStartTimeCriteria.lte(filter.getStartDateBeforeOrEqualTo());
      }
      query.addCriteria(usageStartTimeCriteria);
    }

    // cost filter
    if (filter.getUsageCostGreaterThanOrEqualTo() != null
        || filter.getUsageCostLowerThanOrEqualTo() != null) {

      Criteria costCriteria = Criteria.where(GenericBillingReport.FIELD_USAGE_COST);
      if (filter.getUsageCostGreaterThanOrEqualTo() != null) {
        costCriteria.gte(filter.getUsageCostGreaterThanOrEqualTo());
      }

      if (filter.getUsageCostLowerThanOrEqualTo() != null) {
        costCriteria.lte(filter.getUsageCostLowerThanOrEqualTo());
      }
      query.addCriteria(costCriteria);
    }

    // find by example criteria matcher
    ExampleMatcher exampleMatcher = ExampleMatcher.matching().withStringMatcher(StringMatcher.EXACT)
        .withIgnoreCase().withIgnoreNullValues();

    // creating the example
    GenericBillingReport exampleData = new GenericBillingReport();
    exampleData.setAccountId(filter.getAccountId());
    exampleData.setLineItemId(filter.getLineItemId());
    exampleData.setProductCode(filter.getProductCode());
    exampleData.setProvider(filter.getProvider());
    exampleData.setResourceId(filter.getResourceId());
    exampleData.setLocation(StringUtils.trimToNull(filter.getLocation()));
    Example<GenericBillingReport> example = Example.of(exampleData, exampleMatcher);

    // adding the example to the criteria
    query.addCriteria(Criteria.byExample(example));

    List<GenericBillingReport> result = mongoTemplate.find(query, GenericBillingReport.class);

    return result;
  }
}
