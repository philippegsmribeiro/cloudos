package cloudos.costanalysis;

import static cloudos.models.costanalysis.GoogleUsageReportCsvHeader.LOCATION;
import static cloudos.models.costanalysis.GoogleUsageReportCsvHeader.MEASUREMENT_ID;
import static cloudos.models.costanalysis.GoogleUsageReportCsvHeader.QUANTITY;
import static cloudos.models.costanalysis.GoogleUsageReportCsvHeader.REPORT_DATE;
import static cloudos.models.costanalysis.GoogleUsageReportCsvHeader.RESOURCE_ID;
import static cloudos.models.costanalysis.GoogleUsageReportCsvHeader.RESOURCE_URI;
import static cloudos.models.costanalysis.GoogleUsageReportCsvHeader.UNIT;

import cloudos.Application;
import cloudos.models.CallResult;
import cloudos.models.CallStatus;
import cloudos.models.costanalysis.GoogleUsageReportEntry;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import lombok.extern.log4j.Log4j2;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Mapper for Google Cloud Engine usage reports.
 */
@Component
@Log4j2
public class GoogleUsageReportMapper {

  @Autowired
  private GoogleUsageReportEntryRepository repository;

  /**
   * Parses a Google Cloud Engine CSV usage report to a collection of {@link
   * GoogleUsageReportEntry}.
   *
   * @throws IllegalArgumentException if no file is provided
   */
  public CallResult<Void> processReport(File file) {

    // guard clause
    if (file == null) {
      return new CallResult<>(null,
          CallStatus.FAILURE, Collections.singleton("no file provided to map"));
    }

    log.info("Going to process Google Cloud Engine usage report file {}", file);

    try {

      // getting rows as csv records
      Iterable<CSVRecord> csvRecords = getCsvRecords(file);

      // processing rows
      Instant startTime = Instant.now();
      int count = 0;
      Set<String> errors = new HashSet<>();
      CallStatus callStatus = CallStatus.SUCCESS;
      for (CSVRecord csvRow : csvRecords) {
        // converting csv row to object and saving it

        // in order to close properly the CSVRecord all lines should be read
        // so we surrround with try catch and store the exceptions
        try {
          GoogleUsageReportEntry googleUsageReportEntry = mapCsvRecordToObject(csvRow,
              file.getName(), getNumericProjectIdFromFileName(file.getName()));
          repository.save(googleUsageReportEntry);
          count++;
        } catch (Exception e) {
          callStatus = CallStatus.FAILURE;
          errors.add(String.format("%d: %s", csvRow.getRecordNumber(), e.getMessage()));
        }
      }

      Duration duration = Duration.between(startTime, Instant.now());
      log.info("Finished processing {} records of Google Cloud Engine usage report in {} seconds",
          count, duration.getSeconds());

      return new CallResult<>(null, callStatus, errors);

    } catch (IOException e) {
      log.error("Could not map Google Cloud Engine usage report file {}: {}", file, e.getMessage());
      return new CallResult<>(null, CallStatus.FAILURE, Collections.singleton(e.getMessage()));
    }
  }

  /**
   * Maps and converts a {@link CSVRecord} into a {@link GoogleUsageReportEntry} instance.
   *
   * @param csvRecord the csv record row to be parsed
   * @param fileName the usage report file name
   * @param numericProjectId the usage report numeric project id
   * @return a {@link CSVRecord} into a {@link GoogleUsageReportEntry} instance.
   */
  private GoogleUsageReportEntry mapCsvRecordToObject(CSVRecord csvRecord, String fileName,
      Long numericProjectId) {
    if (csvRecord == null) {
      throw new IllegalArgumentException("csvRecord is mandatory");
    }
    GoogleUsageReportEntry entry = new GoogleUsageReportEntry();
    // sets the ID to avoid duplicates
    entry.setId(fileName + csvRecord.getRecordNumber());
    entry.setReportDate(getLocalDateFromIsoDate(csvRecord.get(REPORT_DATE.getHeader())));
    entry.setLocation(csvRecord.get(LOCATION.getHeader()));
    entry.setMeasurementId(csvRecord.get(MEASUREMENT_ID.getHeader()));
    entry.setQuantity(BigDecimal.valueOf(Long.valueOf(csvRecord.get(QUANTITY.getHeader()))));
    entry.setUnit(csvRecord.get(UNIT.getHeader()));
    entry.setResourceUri(csvRecord.get(RESOURCE_URI.getHeader()));
    entry.setResourceId(csvRecord.get(RESOURCE_ID.getHeader()));
    entry.setNumericProjectId(numericProjectId);
    return entry;
  }

  /**
   * Converts a ISO formatted string into a {@link LocalDate}.
   *
   * @param value the string to be parsed
   * @return a {@link LocalDate} or null if a blank value is provided.
   */
  private LocalDate getLocalDateFromIsoDate(String value) {
    if (StringUtils.isNotBlank(value)) {
      DateTimeFormatter dtf = DateTimeFormatter.ISO_DATE.withLocale(Application.LOCALE_DEFAULT);
      return LocalDate.parse(value, dtf);
    }
    return null;
  }

  /**
   * Reads the CSV file to a {@link CSVRecord} collection.
   *
   * @param file the file to be parsed
   * @return a {@link CSVRecord} collection
   * @throws IOException if an IO error occurs
   * @throws IllegalArgumentException if no file is provided
   */
  public Iterable<CSVRecord> getCsvRecords(File file) throws IOException {

    if (file == null) {
      throw new IllegalArgumentException("no input file was provided");
    }

    Reader reader = new BufferedReader(new FileReader(file));
    CSVParser parse = CSVFormat.RFC4180
        .withAllowMissingColumnNames(true)
        .withFirstRecordAsHeader()
        .withIgnoreEmptyLines(true)
        .parse(reader);
    return parse;

  }

  /**
   * Returns the numeric project id from the usage report file name.
   *
   * <p>Daily usage reports have the following name format:</p>
   * <pre>
   * {@code
   * <bucket>/<report_prefix>_<numeric_project_id>_<YYYYMMDD>.csv
   * }
   * </pre>
   *
   * @param usageReportFileName the usage report file name
   * @return the numeric project id from the file name or null if file is not provided
   */
  public Long getNumericProjectIdFromFileName(String usageReportFileName) {

    if (StringUtils.isBlank(usageReportFileName)) {
      return null;
    }

    String[] split = StringUtils.split(usageReportFileName, "_");
    if (split.length < 2) {
      log.error("incorrect usage report file name pattern. returning null ");
      return null;
    }

    return Long.parseLong(split[split.length - 2]);
  }
}
