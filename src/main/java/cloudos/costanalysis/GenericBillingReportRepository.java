package cloudos.costanalysis;

import cloudos.Providers;
import cloudos.models.costanalysis.GenericBillingReport;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * Spring data repository for {@link cloudos.models.costanalysis.GenericBillingReport} collection.
 */
public interface GenericBillingReportRepository
    extends MongoRepository<GenericBillingReport, String>, GenericBillingReportRepositoryCustom,
    QueryDslPredicateExecutor<GenericBillingReport> {

  List<GenericBillingReport> findByResourceId(String resourceId);

  List<GenericBillingReport> findByResourceId(String resourceId, Pageable pageable);

  List<GenericBillingReport> findByResourceIdAfter(String resourceId, Date date);

  List<GenericBillingReport> findByProviderAndResourceIdAfter(Providers provider, String resourceId,
      Date date);

  @Query(value = "{'usageStartTime':{ $gte: ?0, $lte: ?1}}")
  @Cacheable("billingreports")
  List<GenericBillingReport> findByUsageStartTimeBetween(Date start, Date end);

  @Query(value = "{'usageStartTime':{ $gte: ?0, $lte: ?1}}")
  List<GenericBillingReport> findByUsageStartTimeBetween(Date start, Date end, Pageable pageable);

  @Query(value = "{'provider': ?0, 'usageStartTime':{ $gte: ?1, $lte: ?2}}")
  @Cacheable("billingreportsandproviders")
  List<GenericBillingReport> findByProviderAndUsageStartTimeBetween(Providers provider, Date start,
      Date end);

  List<GenericBillingReport> findByAccountId(String accountId);

  List<GenericBillingReport> findByAccountId(String accountId, Pageable pageable);

  @Query(value = "{ 'productCode': { $in: ?0 }, 'usageStartTime':{ $gte: ?1, $lte: ?2}}")
  List<GenericBillingReport> findByProductCodeInAndUsageStartTimeIsBetween(
      Collection<String> productCodes, Date start, Date end);

}
