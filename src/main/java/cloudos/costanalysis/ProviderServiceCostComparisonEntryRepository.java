package cloudos.costanalysis;

import cloudos.models.costanalysis.ProviderServiceCostComparisonEntry;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring data repository for {@link ProviderServiceCostComparisonEntry}.
 */
public interface ProviderServiceCostComparisonEntryRepository extends
    MongoRepository<ProviderServiceCostComparisonEntry, String> {

}
