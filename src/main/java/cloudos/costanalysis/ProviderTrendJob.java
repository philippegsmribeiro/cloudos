package cloudos.costanalysis;

import cloudos.Providers;
import cloudos.billings.BillingsConstants;
import cloudos.models.CallResult;
import cloudos.models.SparkJob;
import cloudos.models.costanalysis.BillingCost;
import cloudos.models.costanalysis.GenericBillingReport;
import cloudos.models.costanalysis.ProviderTrendEntry;
import cloudos.models.costanalysis.ProviderTrendEntryCost;
import cloudos.utils.DateUtil;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.spark.api.java.JavaRDD;
import scala.Tuple2;

/**
 * Provider trend spark job.
 */
@Log4j2
@AllArgsConstructor
public final class ProviderTrendJob extends SparkJob {

  private GenericBillingReportRepository genericBillingReportRepository;

  private ProviderTrendEntryRepository providerTrendRepository;

  /**
   * Default contructor.
   */
  @SuppressWarnings("unused")
  private ProviderTrendJob() {
    super();
  }

  /**
   * Executes the provider trend job using today's date.
   */
  public CallResult<ProviderTrendEntry> execute() {
    return this.execute(LocalDate.now());
  }

  /**
   * Executes the provider trend job using the given date.
   */
  public CallResult<ProviderTrendEntry> execute(LocalDate date) {

    CallResult<ProviderTrendEntry> result = new CallResult<>();
    if (date == null) {
      result.fail("parameter date is mandatory.");
      return result;
    }

    log.info("executing provider trend job using date {}", date);

    Instant start = Instant.now();

    try {
      result.setResult(getProviderTradeEntries(date));
    } catch (Exception e) {
      log.error("Exception caught while getting provider trade entries: {}", e.getMessage());
      result.fail("Error while getting provider trade entries");
    }
    Instant stop = Instant.now();
    Duration d = Duration.between(start, stop);
    log.info("finished executing provider trend job using date {} in {} seconds", date,
        d.getSeconds());
    return result;
  }

  /**
   * Queries billing reports from month to date and previous month by the given date and returns the
   * provider trend entry for the given date.
   */
  private ProviderTrendEntry getProviderTradeEntries(LocalDate date) {

    LocalDate startOfMonth = date.withDayOfMonth(1);
    LocalDate startOfLastMonth = startOfMonth.minusMonths(1);
    LocalDate endOfLastMonth = startOfMonth.minusDays(1);

    Map<Providers, BigDecimal> lastMonthCostMap =
        getProviderCostMap(startOfLastMonth, endOfLastMonth);

    Map<Providers, BigDecimal> monthToDateCostMap = getProviderCostMap(startOfMonth, date);

    ProviderTrendEntry result = getProviderTrendEntries(date, lastMonthCostMap, monthToDateCostMap);

    return result;
  }

  /**
   * Aggregates the last month's provider cost map and month to date provider cost map into a list
   * of provider trend entries.
   * 
   * @param date the date of provider trend entry
   * @param lastMonthCostMap last months provider cost map
   * @param monthToDateCostMap month to date provider cost map
   */
  private ProviderTrendEntry getProviderTrendEntries(LocalDate date,
      Map<Providers, BigDecimal> lastMonthCostMap, Map<Providers, BigDecimal> monthToDateCostMap) {

    Set<Providers> allProviders = new HashSet<>();

    if (!lastMonthCostMap.isEmpty()) {
      allProviders.addAll(lastMonthCostMap.keySet());
    }

    if (!monthToDateCostMap.isEmpty()) {
      allProviders.addAll(monthToDateCostMap.keySet());
    }

    List<ProviderTrendEntryCost> values = new ArrayList<>();
    for (Providers provider : allProviders) {

      BillingCost previousMonthCost = new BillingCost(
          ObjectUtils.defaultIfNull(lastMonthCostMap.get(provider), BigDecimal.ZERO));

      BillingCost monthToDateCost = new BillingCost(
          ObjectUtils.defaultIfNull(monthToDateCostMap.get(provider), BigDecimal.ZERO));

      ProviderTrendEntryCost cost =
          new ProviderTrendEntryCost(provider, previousMonthCost, monthToDateCost);

      values.add(cost);
    }

    ProviderTrendEntry result = new ProviderTrendEntry(null, date, values, new Date());
    providerTrendRepository.save(result);

    return result;
  }

  /**
   * Returns the provider cost map between a range of dates (inclusive).
   * 
   * @param start start date inclusive
   * @param end end date inclusive
   */
  private Map<Providers, BigDecimal> getProviderCostMap(LocalDate start, LocalDate end) {
    List<GenericBillingReport> reports = genericBillingReportRepository
        .findByUsageStartTimeBetween(DateUtil.toDate(start), DateUtil.toDate(end));

    JavaRDD<GenericBillingReport> reportsRdd = context.parallelize(reports);

    return reportsRdd.mapToPair(r -> new Tuple2<>(r.getProvider(), r.getUsageCost()))
        .reduceByKey((i, n) -> i.add(n, BillingsConstants.MATH_CONTEXT)).collectAsMap();
  }

}
