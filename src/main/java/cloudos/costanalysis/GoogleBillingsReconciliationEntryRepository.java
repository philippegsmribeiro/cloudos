package cloudos.costanalysis;

import cloudos.models.costanalysis.GoogleBillingReconciliationEntry;
import cloudos.models.costanalysis.GoogleBillingReconciliationEntryId;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Repository for {@link GoogleBillingReconciliationEntry}.
 */
public interface GoogleBillingsReconciliationEntryRepository extends
    MongoRepository<GoogleBillingReconciliationEntry, GoogleBillingReconciliationEntryId> {

  List<GoogleBillingReconciliationEntry> findByReportDateAndProjectNumber(LocalDate reportDate,
      Long projectNumber);

}
