package cloudos.costanalysis;

import cloudos.models.costanalysis.DailyResourceCost;
import cloudos.models.costanalysis.DailyResourceCostId;
import java.time.LocalDate;
import java.util.List;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/**
 * Spring data repository for {@link DailyResourceCost} collection.
 */
public interface DailyResourceCostRepository
    extends MongoRepository<DailyResourceCost, DailyResourceCostId> {

  @Query(value = "{'_id.date':{ $gte: ?0, $lte: ?1}}")
  @Cacheable("dailyresourcecostrepositorybydatebetween")
  List<DailyResourceCost> findByDateBetween(LocalDate start, LocalDate end);
}
