package cloudos.costanalysis;

import cloudos.Providers;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * TO object to Billing Cost Reservation.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BillingCostReservation implements Serializable {

  private static final long serialVersionUID = 1L;
  private Providers provider;
  private String reservationType;
  private String month;
  private Double costPreviousMonth;
  private Double costMonth;

}
