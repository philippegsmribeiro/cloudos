package cloudos.costanalysis;

import cloudos.models.costanalysis.DailyResourceCost;
import cloudos.models.costanalysis.DailyResourceCostId;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service implementation for resource cost analysis features.
 */
@Log4j2
@Service
@NoArgsConstructor
@AllArgsConstructor
public class DailyResourceCostService {

  @Autowired
  private DailyResourceCostRepository dailyResourceCostRepository;

  /**
   * Saves a {@link DailyResourceCost} instance.
   * 
   * @param dailyResourceCost the object to be saved
   */
  public DailyResourceCost save(DailyResourceCost dailyResourceCost) {
    if (dailyResourceCost == null) {
      log.error("dailyResourceCost is mandatory. Returning null.");
      return null;
    }
    return dailyResourceCostRepository.save(dailyResourceCost);
  }

  /**
   * Saves a {@link DailyResourceCost} collection.
   * 
   * @param dailyResourceCosts the objects to be saved
   */
  public List<DailyResourceCost> save(Iterable<DailyResourceCost> dailyResourceCosts) {
    if (dailyResourceCosts == null) {
      log.error("dailyResourceCosts is mandatory. Returning an empty collection.");
      return Collections.emptyList();
    }
    return dailyResourceCostRepository.save(dailyResourceCosts);
  }

  /**
   * Retrieves a list of {@link DailyResourceCost} filtered by a date range.
   * 
   * @param start the start date range (inclusive)
   * @param end the end date range (inclusive)
   * @return a list of {@link DailyResourceCost}
   */
  public List<DailyResourceCost> findByDateRange(LocalDate start, LocalDate end) {
    if (start == null || end == null) {
      log.error("start and end dates are mandatory. Returning an empty collection.");
      return Collections.emptyList();
    }
    return dailyResourceCostRepository.findByDateBetween(start, end);
  }

  /**
   * Retrieves a {@link DailyResourceCost} instance by its identifier.
   * 
   * @param id the identifier
   * @return the {@link DailyResourceCost} or {@code null} if no id was provided
   */
  public DailyResourceCost findById(DailyResourceCostId id) {
    if (id == null) {
      log.error("id is mandatory. Returning null.");
      return null;
    }
    return dailyResourceCostRepository.findOne(id);
  }

  /**
   * Deletes a {@link DailyResourceCost} instance.
   * 
   * @param item the instance to be deleted
   */
  public void delete(DailyResourceCost item) {
    dailyResourceCostRepository.delete(item);
  }

}
