package cloudos;

import cloudos.elasticsearch.model.ChartParameters;
import cloudos.elasticsearch.model.KibanaSearchParametersTransformer;
import cloudos.elasticsearch.services.PacketbeatService;
import cloudos.security.WebSecurityConfig;

import java.net.UnknownHostException;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/* Import the healthchecker package */

/**
 * Implement the HealthChecker methods from PacketbeatService.
 *
 * @author Rogerio Souza
 */
@Controller
@RestController
@RequestMapping(value = HealthCheckerPacketbeatController.HEALTHCHECKER)
@Log4j2
public class HealthCheckerPacketbeatController {

  public static final String HEALTHCHECKER =
      WebSecurityConfig.API_PATH + "/healthchecker/packetbeat";

  @Autowired
  private PacketbeatService packetbeatService;

  @Autowired
  private KibanaSearchParametersTransformer transformer;

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by BYTES_IN packetbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/bytes_in_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getBytesInAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return packetbeatService.getBytesInAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by BYTES_OUT packetbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/bytes_out_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getBytesOutAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return packetbeatService.getBytesOutAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by COUNT packetbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/count_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getCountAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return packetbeatService.getCountAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by HTTP_CONTENT_LENGTH
   * packetbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/http_content_length_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getHttpContentLengthAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return packetbeatService.getHttpContentLengthAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by RESPONSETIME
   * packetbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/responsetime_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getResponsetimeAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return packetbeatService.getResponsetimeAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by SHIPPER packetbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/shipper_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getShipperAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return packetbeatService.getShipperAvg(transformer.apply(chartParameters));
  }

}
