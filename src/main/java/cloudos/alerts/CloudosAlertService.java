package cloudos.alerts;

import cloudos.exceptions.NotImplementedException;
import cloudos.models.alerts.AlertRequestGeneric;
import cloudos.models.alerts.CloudosAlert;
import cloudos.models.alerts.QCloudosAlert;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

/**
 * Service implementation for Cloudos alerts.
 * 
 * @author Alex Calagua
 *
 */
@Service
public class CloudosAlertService {

  @Autowired
  private CloudosAlertRepository cloudosAlertRepository;

  /**
   * Method that create an alert.
   * 
   * @param cloudosAlert request to create an the Cloudos Alert.
   * @return CloudosAlert Class representing the creation of Cloudos alert.
   */
  public CloudosAlert createAlert(CloudosAlert cloudosAlert) {
    return cloudosAlertRepository.save(cloudosAlert);
  }

  /**
   * Method that returns a list of CloudosAlert alerts.
   * 
   * @param alertRequest Request generic by search alerts.
   * @return list the CloudosAlert
   */
  public List<CloudosAlert> getListAlertByFilter(AlertRequestGeneric alertRequest)
      throws AlertException, NotImplementedException {
    Predicate filters = getFiltersQ(alertRequest);
    Sort sort = new Sort(new Order(Sort.Direction.ASC, "metricName").ignoreCase());
    return (List<CloudosAlert>) cloudosAlertRepository.findAll(filters, sort);
  }

  /**
   * Method that mount a filter for CloudosAlert.
   * 
   * @param alertRequest Request generic by search alerts.
   * @return Predicate Conditions the filter
   */
  private Predicate getFiltersQ(AlertRequestGeneric alertRequest) {
    BooleanBuilder builder = new BooleanBuilder();
    QCloudosAlert cloudosAlert = new QCloudosAlert("cloudosAlert");
    if (StringUtils.isNotBlank(alertRequest.getRegion())) {
      builder.and(cloudosAlert.region.eq(alertRequest.getRegion()));
    }
    if (alertRequest.getProvider() != null) {
      builder.and(cloudosAlert.provider.eq(alertRequest.getProvider()));
    }
    if (StringUtils.isNotBlank(alertRequest.getAlarmName())) {
      builder.and(cloudosAlert.alarmName.startsWith(alertRequest.getAlarmName()));
    }
    return builder;
  }

  /**
   * Method that returns a list of CloudosAlert alerts.
   * 
   * @return list the CloudosAlert
   */
  public List<CloudosAlert> getListAlert() throws AlertException, NotImplementedException {
    return cloudosAlertRepository.findAll();
  }

  /**
   * Method that searches an Cloudos alert by name and by region.
   * 
   * @param alarmName name the alert.
   * @param region region the alert.
   * @return CloudosAlert class representing the alert search response.
   */
  public CloudosAlert getAlertByAlarmNameAndRegion(String alarmName, String region) {
    return cloudosAlertRepository.findByAlarmNameAndRegion(alarmName, region);
  }

  /**
   * Method that delete an Cloudos alert by Id.
   * 
   * @param id request to delete an Cloudos Alert.
   * 
   */
  public void deleteAlert(String id) {
    cloudosAlertRepository.delete(id);
  }


  /**
   * Method that update an Cloudos alert.
   * 
   * @param cloudosAlert request to update an the Cloudos Alert.
   * @return CloudosAlert Class representing the updated of Cloudos alert.
   */
  public CloudosAlert updateAlert(CloudosAlert cloudosAlert) {
    return cloudosAlertRepository.save(cloudosAlert);
  }

}

