package cloudos.alerts;

/**
 * Exception when a action request on an instance fails.
 * 
 */
public class AlertException extends Exception {

  private static final long serialVersionUID = -6283521504310814784L;

  public AlertException() {
    super();
  }

  public AlertException(String message) {
    super(message);
  }
}
