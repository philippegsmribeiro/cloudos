package cloudos.alerts.manager;

import cloudos.alerts.AlertException;
import cloudos.models.alerts.AlertRequest;
import cloudos.models.alerts.AlertRequestGeneric;
import cloudos.models.alerts.AlertResponse;
import cloudos.models.alerts.CategoryMetricResponse;
import cloudos.models.alerts.GoogleAlert;
import cloudos.models.alerts.MetricMissingDataAlertResponse;
import cloudos.models.alerts.MetricNotificationTopicResponse;
import cloudos.models.alerts.MetricPeriodAlertResponse;
import cloudos.models.alerts.MetricRequest;
import cloudos.models.alerts.MetricResponse;
import cloudos.models.alerts.MetricStatisticRequest;
import cloudos.models.alerts.MetricStatisticResponse;
import cloudos.models.alerts.MetricStatusAlarmResponse;
import java.util.List;
import org.springframework.stereotype.Service;


/**
 * Class to create, delete, and list google alerts.
 *
 * @author Alex Calagua.
 */
@Service
public class GoogleAlertManager extends AlertManager<GoogleAlert> {

  @Override
  public AlertResponse<GoogleAlert> createAlert(AlertRequest alert, boolean notify)
      throws AlertException {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public AlertResponse<GoogleAlert> deleteAlert(AlertRequestGeneric alertRequestGeneric)
      throws AlertException {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<GoogleAlert> getListAlert(AlertRequestGeneric alertRequestGeneric)
      throws AlertException {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public AlertResponse<GoogleAlert> updateAlert(AlertRequest alert) throws AlertException {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public AlertResponse<GoogleAlert> getAlertByName(AlertRequestGeneric alertRequestGeneric)
      throws AlertException {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<MetricResponse> getListMetrics(MetricRequest metricRequest) throws AlertException {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<String> getListNameSpaceOfMetrics(MetricRequest metricRequest) throws AlertException {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<CategoryMetricResponse> getListCategoriesOfMetrics(MetricRequest metricRequest)
      throws AlertException {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<MetricStatusAlarmResponse> getListMetricStatusAlarm() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<MetricNotificationTopicResponse> getListNotificationTopic(MetricRequest metricRequest)
      throws AlertException {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<MetricStatisticResponse> getListMetricStatistic(
      MetricStatisticRequest metricStatisticRequest) throws AlertException {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<MetricPeriodAlertResponse> getListMetricPeriodAlert() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<MetricMissingDataAlertResponse> getListMetricMissingDataAlert() {
    // TODO Auto-generated method stub
    return null;
  }

}
