package cloudos.alerts.manager;

import cloudos.Providers;
import cloudos.alerts.AlertException;
import cloudos.alerts.CloudosAlertService;
import cloudos.amazon.AWSCredentialService;
import cloudos.models.alerts.AlertRequest;
import cloudos.models.alerts.AlertRequestGeneric;
import cloudos.models.alerts.AlertResponse;
import cloudos.models.alerts.CategoryMetricResponse;
import cloudos.models.alerts.CloudosAlert;
import cloudos.models.alerts.CloudosDimension;
import cloudos.models.alerts.CloudosNotification;
import cloudos.models.alerts.CloudosReadStatus;
import cloudos.models.alerts.MetricMissingDataAlert;
import cloudos.models.alerts.MetricMissingDataAlertResponse;
import cloudos.models.alerts.MetricNotificationTopicResponse;
import cloudos.models.alerts.MetricPeriodAlert;
import cloudos.models.alerts.MetricPeriodAlertResponse;
import cloudos.models.alerts.MetricRequest;
import cloudos.models.alerts.MetricResponse;
import cloudos.models.alerts.MetricStatisticRequest;
import cloudos.models.alerts.MetricStatisticResponse;
import cloudos.models.alerts.MetricStatusAlarm;
import cloudos.models.alerts.MetricStatusAlarmResponse;
import cloudos.queue.QueueManagerService;
import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.model.ComparisonOperator;
import com.amazonaws.services.cloudwatch.model.Datapoint;
import com.amazonaws.services.cloudwatch.model.DeleteAlarmsRequest;
import com.amazonaws.services.cloudwatch.model.DeleteAlarmsResult;
import com.amazonaws.services.cloudwatch.model.DescribeAlarmsRequest;
import com.amazonaws.services.cloudwatch.model.DescribeAlarmsResult;
import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.DimensionFilter;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsRequest;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsResult;
import com.amazonaws.services.cloudwatch.model.ListMetricsRequest;
import com.amazonaws.services.cloudwatch.model.ListMetricsResult;
import com.amazonaws.services.cloudwatch.model.Metric;
import com.amazonaws.services.cloudwatch.model.MetricAlarm;
import com.amazonaws.services.cloudwatch.model.PutMetricAlarmRequest;
import com.amazonaws.services.cloudwatch.model.PutMetricAlarmResult;
import com.amazonaws.services.cloudwatch.model.StandardUnit;
import com.amazonaws.services.cloudwatch.model.Statistic;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.ListTopicsResult;
import com.amazonaws.services.sns.model.Topic;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Class to create, delete, and list amazon alerts using CloudWatch.
 *
 * @author Alex Calagua.
 */
@Service
public class AmazonAlertManager extends AlertManager<CloudosAlert> {

  @Autowired
  private AWSCredentialService awsCredentialService;

  @Autowired
  private CloudosAlertService cloudosAlertService;

  @Autowired
  private QueueManagerService queueManagerService;

  private static final String PREFIX_BY = "By ";
  private static final String PREFIX_ALL = "All ";
  private static final String COMMA = ",";
  private static final String WORD_UNION = " and ";
  private static final String COLON = ":";

  @Override
  public AlertResponse<CloudosAlert> createAlert(AlertRequest alertRequest, boolean notify)
      throws AlertException {
    this.validateEnumAmazonAlert(alertRequest);
    this.validateAlertName(alertRequest);
    this.validateAlertNotifications(alertRequest);
    this.validateAlertExistent(alertRequest, notify);

    AmazonCloudWatch cloudWatch =
        this.getConnectionAwsCloudWatchSingleton(alertRequest.getRegion());

    PutMetricAlarmRequest request = new PutMetricAlarmRequest()
        .withAlarmName(alertRequest.getAlarmName())
        .withComparisonOperator(alertRequest.getComparisonOperator())
        .withEvaluationPeriods(alertRequest.getEvaluationPeriods())
        .withMetricName(alertRequest.getMetricName()).withNamespace(alertRequest.getNamespace())
        .withPeriod(alertRequest.getPeriod()).withStatistic(alertRequest.getStatistic())
        .withThreshold(alertRequest.getThreshold())
        .withActionsEnabled(alertRequest.isActionsEnabled())
        .withTreatMissingData(alertRequest.getTreatMissingData())
        .withAlarmDescription(alertRequest.getAlarmDescription()).withUnit(alertRequest.getUnit());

    this.mountAlertDimensions(alertRequest, request);
    this.mountAlertNotifications(alertRequest, request);

    PutMetricAlarmResult response = cloudWatch.putMetricAlarm(request);
    AlertResponse<CloudosAlert> alertResponse = new AlertResponse<CloudosAlert>();
    alertResponse.setResponse(response.getSdkResponseMetadata().getRequestId());
    this.cloudosAlertService.createAlert(this.mountCloudosAlertFromRequest(alertRequest));
    alertResponse.setAlert(this.getAlertByName((AlertRequestGeneric) alertRequest).getAlert());
    if (notify) {
      queueManagerService.sendAlertCreatedMessage(alertResponse.getAlert());
    }
    return alertResponse;

  }

  @Override
  public AlertResponse<CloudosAlert> updateAlert(AlertRequest alertRequest) throws AlertException {
    this.validateAlert(alertRequest);
    AlertResponse<CloudosAlert> alertResponse = new AlertResponse<CloudosAlert>();
    alertResponse = this.createAlert(alertRequest, false);
    queueManagerService.sendAlertUpdatedMessage(alertResponse.getAlert());
    return alertResponse;
  }

  @Override
  public AlertResponse<CloudosAlert> deleteAlert(AlertRequestGeneric alertRequest)
      throws AlertException {
    AlertResponse<CloudosAlert> alertResponse = new AlertResponse<CloudosAlert>();
    AmazonCloudWatch cloudWatch =
        this.getConnectionAwsCloudWatchSingleton(alertRequest.getRegion());
    this.validateAlertName(alertRequest);
    DeleteAlarmsRequest request =
        new DeleteAlarmsRequest().withAlarmNames(alertRequest.getAlarmName());
    DeleteAlarmsResult response = cloudWatch.deleteAlarms(request);
    if (StringUtils.isNotBlank(response.getSdkResponseMetadata().getRequestId())) {
      alertResponse.setResponse(response.getSdkResponseMetadata().getRequestId());
      CloudosAlert cloudosAlertRepository = this.cloudosAlertService
          .getAlertByAlarmNameAndRegion(alertRequest.getAlarmName(), alertRequest.getRegion());
      if (cloudosAlertRepository != null) {
        this.cloudosAlertService.deleteAlert(cloudosAlertRepository.getId());
      }
      queueManagerService.sendAlertDeletedMessage(cloudosAlertRepository);

    }
    return alertResponse;
  }

  @Override
  public List<CloudosAlert> getListAlert(AlertRequestGeneric alertRequest) throws AlertException {
    AmazonCloudWatch cloudWatch =
        this.getConnectionAwsCloudWatchSingleton(alertRequest.getRegion());
    List<CloudosAlert> listResult = new ArrayList<>();
    boolean done = false;
    while (!done) {
      DescribeAlarmsRequest request = new DescribeAlarmsRequest();
      if (alertRequest.getAlarmName() != null && !alertRequest.getAlarmName().isEmpty()) {
        request.setAlarmNamePrefix(alertRequest.getAlarmName());
      }
      DescribeAlarmsResult response = cloudWatch.describeAlarms(request);
      for (MetricAlarm alarm : response.getMetricAlarms()) {
        CloudosAlert cloudosAlertAmazon =
            this.mountCloudosAlertFromMetricAlarm(alarm, alertRequest.getRegion());
        CloudosAlert cloudosAlertRepository = cloudosAlertService.getAlertByAlarmNameAndRegion(
            cloudosAlertAmazon.getAlarmName(), cloudosAlertAmazon.getRegion());
        listResult
            .add(processUpdateCloudosAlert(cloudosAlertAmazon, cloudosAlertRepository, false));
      }
      request.setNextToken(response.getNextToken());
      if (response.getNextToken() == null) {
        done = true;
      }
    }
    return listResult;
  }

  @Override
  public AlertResponse<CloudosAlert> getAlertByName(AlertRequestGeneric alertRequest)
      throws AlertException {
    AmazonCloudWatch cloudWatch =
        this.getConnectionAwsCloudWatchSingleton(alertRequest.getRegion());
    this.validateAlertName(alertRequest);
    List<CloudosAlert> listResult = new ArrayList<>();
    boolean done = false;
    String responseId = StringUtils.EMPTY;
    while (!done) {
      DescribeAlarmsRequest request = new DescribeAlarmsRequest();
      request.withAlarmNames(alertRequest.getAlarmName());
      DescribeAlarmsResult response = cloudWatch.describeAlarms();
      responseId = response.getSdkResponseMetadata().getRequestId();
      for (MetricAlarm alarm : response.getMetricAlarms()) {
        listResult.add(this.mountCloudosAlertFromMetricAlarm(alarm, alertRequest.getRegion()));
      }
      request.setNextToken(response.getNextToken());
      if (response.getNextToken() == null) {
        done = true;
      }
    }
    CloudosAlert cloudosAlertAmazon = listResult.stream()
        .filter(alert -> alertRequest.getAlarmName().equals(alert.getAlarmName())).findAny()
        .orElse(null);
    if (cloudosAlertAmazon == null) {
      throw new AlertException("Alert was not found");
    }

    CloudosAlert cloudosAlertRepository = cloudosAlertService
        .getAlertByAlarmNameAndRegion(alertRequest.getAlarmName(), alertRequest.getRegion());

    AlertResponse<CloudosAlert> alertResponse = new AlertResponse<CloudosAlert>();
    alertResponse
        .setAlert(processUpdateCloudosAlert(cloudosAlertAmazon, cloudosAlertRepository, true));
    alertResponse.setResponse(responseId);
    return alertResponse;
  }

  @Override
  public List<MetricResponse> getListMetrics(MetricRequest metricRequest) throws AlertException {
    AmazonCloudWatch cloudWatch =
        this.getConnectionAwsCloudWatchSingleton(metricRequest.getRegion());
    boolean done = false;
    List<MetricResponse> response = new ArrayList<>();
    while (!done) {
      ListMetricsRequest request =
          new ListMetricsRequest().withMetricName(metricRequest.getMetricNameParameter())
              .withNamespace(metricRequest.getMetricNameSpaceParameter())
              .withDimensions(this.getDimensionFilters(metricRequest));
      ListMetricsResult result = cloudWatch.listMetrics(request);
      for (Metric metric : result.getMetrics()) {
        int numberDimensionRequest = 0;
        if (StringUtils.isNotBlank(metricRequest.getDimensionFilters())) {
          numberDimensionRequest = metricRequest.getDimensionFilters().split(COMMA).length;
        }
        if (metric.getDimensions().size() == numberDimensionRequest) {
          MetricResponse metricResponse = MetricResponse.builder()
              .metricName(metric.getMetricName()).metricNamespace(metric.getNamespace()).build();
          if (metricRequest.getDimensionFilters() != null) {
            for (Dimension dimensionAws : metric.getDimensions()) {
              CloudosDimension dimension =
                  new CloudosDimension(dimensionAws.getName(), dimensionAws.getValue());
              metricResponse.addDimension(dimension);
            }
            response.add(metricResponse);
          } else {
            if (!metric.getDimensions().isEmpty()) {
              continue;
            }
            response.add(metricResponse);
          }
        }
      }
      request.setNextToken(result.getNextToken());
      if (result.getNextToken() == null) {
        done = true;
      }
    }
    return response;
  }

  @Override
  public List<String> getListNameSpaceOfMetrics(MetricRequest metricRequest) throws AlertException {
    AmazonCloudWatch cloudWatch =
        this.getConnectionAwsCloudWatchSingleton(metricRequest.getRegion());
    boolean done = false;
    List<String> response = new ArrayList<>();
    while (!done) {
      ListMetricsResult result = cloudWatch.listMetrics();
      result.getMetrics().stream().filter(distinctByKey(b -> b.getNamespace()))
          .forEach(b -> response.add(b.getNamespace()));
      if (result.getNextToken() == null) {
        done = true;
      }
    }
    return response.stream().sorted().collect(Collectors.toList());
  }

  @Override
  public List<CategoryMetricResponse> getListCategoriesOfMetrics(MetricRequest metricRequest)
      throws AlertException {
    AmazonCloudWatch cloudWatch =
        this.getConnectionAwsCloudWatchSingleton(metricRequest.getRegion());

    boolean done = false;
    List<CategoryMetricResponse> response = new ArrayList<>();
    final Map<String, String> categoryMetricMap = new HashMap<String, String>();
    while (!done) {
      ListMetricsRequest request =
          new ListMetricsRequest().withNamespace(metricRequest.getMetricNameSpaceParameter());
      ListMetricsResult result = cloudWatch.listMetrics(request);
      this.getDimensionsByMetrics(categoryMetricMap, result);
      request.setNextToken(result.getNextToken());
      if (result.getNextToken() == null) {
        done = true;
      }
    }
    categoryMetricMap.forEach((dimension, metricName) -> response
        .add(CategoryMetricResponse.builder().metricNamespace(metricRequest.getMetricNamespace())
            .metricCategory(this.mountNameCategory(dimension))
            .dimensions(Arrays.asList(dimension.split(COMMA))).build()));

    return response;
  }

  @Override
  public List<MetricStatusAlarmResponse> getListMetricStatusAlarm() {
    List<MetricStatusAlarmResponse> response = new ArrayList<MetricStatusAlarmResponse>();
    for (MetricStatusAlarm metricStatusAlarm : MetricStatusAlarm.values()) {
      response.add(MetricStatusAlarmResponse.builder().code(metricStatusAlarm.name())
          .name(metricStatusAlarm.getDescription()).build());
    }
    return response;
  }

  @Override
  public List<MetricNotificationTopicResponse> getListNotificationTopic(MetricRequest metricRequest)
      throws AlertException {
    AmazonSNS snsClient = this.getConnectionAwsSnsSingleton(metricRequest.getRegion());
    List<MetricNotificationTopicResponse> response =
        new ArrayList<MetricNotificationTopicResponse>();
    ListTopicsResult listTopicsResult = snsClient.listTopics();
    String nextToken = listTopicsResult.getNextToken();
    List<Topic> topics = (List<Topic>) listTopicsResult.getTopics();
    while (nextToken != null) {
      listTopicsResult = snsClient.listTopics(nextToken);
      nextToken = listTopicsResult.getNextToken();
      topics.addAll(listTopicsResult.getTopics());
    }
    response = topics.stream()
        .map(topic -> MetricNotificationTopicResponse.builder()
            .name(this.getNameTopic(topic.getTopicArn())).code(topic.getTopicArn()).build())
        .collect(Collectors.toList());
    return response;
  }

  @Override
  public List<MetricPeriodAlertResponse> getListMetricPeriodAlert() {
    List<MetricPeriodAlertResponse> response = new ArrayList<MetricPeriodAlertResponse>();
    for (MetricPeriodAlert metricPeriodAlert : MetricPeriodAlert.values()) {
      response.add(MetricPeriodAlertResponse.builder().order(metricPeriodAlert.getOrder())
          .period(metricPeriodAlert.getPeriod()).title(metricPeriodAlert.getTitle()).build());
    }
    response.sort((MetricPeriodAlertResponse m1, MetricPeriodAlertResponse m2) -> m1.getOrder()
        - m2.getOrder());
    return response;
  }

  @Override
  public List<MetricMissingDataAlertResponse> getListMetricMissingDataAlert() {
    List<MetricMissingDataAlertResponse> response = new ArrayList<MetricMissingDataAlertResponse>();
    for (MetricMissingDataAlert metricMissingDataAlert : MetricMissingDataAlert.values()) {
      response.add(MetricMissingDataAlertResponse.builder().code(metricMissingDataAlert.getCode())
          .title(metricMissingDataAlert.getTitle()).build());
    }
    return response;
  }

  @Override
  public List<MetricStatisticResponse> getListMetricStatistic(
      MetricStatisticRequest metricStatisticRequest) throws AlertException {
    AmazonCloudWatch cloudWatch =
        this.getConnectionAwsCloudWatchSingleton(metricStatisticRequest.getRegion());

    GetMetricStatisticsRequest request = new GetMetricStatisticsRequest();
    request.withMetricName(metricStatisticRequest.getMetricName())
        .withNamespace(metricStatisticRequest.getMetricNamespace())
        .withStatistics(Collections.singletonList(metricStatisticRequest.getStatistic()))
        .withStartTime(metricStatisticRequest.getStart())
        .withEndTime(metricStatisticRequest.getEnd())
        .withPeriod(metricStatisticRequest.getPeriod());

    List<MetricStatisticResponse> response = new ArrayList<>();
    GetMetricStatisticsResult result = cloudWatch.getMetricStatistics(request);
    List<Datapoint> datapoints = result.getDatapoints();
    for (Datapoint datapoint : datapoints) {
      response.add(MetricStatisticResponse.builder().timestamp(datapoint.getTimestamp())
          .sampleCount(datapoint.getSampleCount()).average(datapoint.getAverage())
          .sum(datapoint.getSum()).minimum(datapoint.getMinimum()).maximum(datapoint.getMaximum())
          .unit(datapoint.getUnit()).build());
    }
    return response;
  }

  /**
   * Method in charge of updating the alert that was returned from the provider with the mongo db
   * alert.
   * 
   * @param cloudosAlertAmazon Amazon Alert
   * @param cloudosAlertRepository Cloudos Alert
   * @param checkReadStatus It is sent true in case you want to evaluate if the alert is opened for
   *        the first time to change the status to read.
   * @return CloudosAlert updated
   */
  private CloudosAlert processUpdateCloudosAlert(CloudosAlert cloudosAlertAmazon,
      CloudosAlert cloudosAlertRepository, boolean checkReadStatus) {
    boolean isCloudosAlertNeedUpdated = false;

    if (!cloudosAlertAmazon.equals(cloudosAlertRepository)) {
      isCloudosAlertNeedUpdated = true;
    }
    if (checkReadStatus && CloudosReadStatus.NOT_READ.equals(cloudosAlertAmazon.getReadStatus())) {
      cloudosAlertAmazon.setReadStatus(CloudosReadStatus.READ);
      isCloudosAlertNeedUpdated = true;
    }
    if (isCloudosAlertNeedUpdated) {
      if (cloudosAlertRepository != null) {
        cloudosAlertAmazon.setId(cloudosAlertRepository.getId());
      }
      cloudosAlertRepository = cloudosAlertService.updateAlert(cloudosAlertAmazon);
    }
    return cloudosAlertRepository;
  }

  /**
   * Method that prepare list the dimension filter.
   * 
   * @param metricRequest request to return list the metrics.
   * @return List the {@link DimensionFilter}
   * @throws AlertException if something goes wrong
   */
  private List<DimensionFilter> getDimensionFilters(MetricRequest metricRequest)
      throws AlertException {
    List<DimensionFilter> dimensionsFilter = null;
    if (StringUtils.isNotBlank(metricRequest.getDimensionFilters())) {
      dimensionsFilter = new ArrayList<>();
      for (String dimension : metricRequest.getDimensionFilters().split(COMMA)) {
        DimensionFilter filter = new DimensionFilter();
        filter.setName(dimension);
        dimensionsFilter.add(filter);
      }
    }
    return dimensionsFilter;
  }

  /**
   * Method that assembles the name of the metric categories.
   * 
   * @param dimension Dimension names to create category name.
   * @param metricName name the Metric
   * @return Name of the categories.
   */
  private String mountNameCategory(String dimensions) {
    List<String> categoryNames = new ArrayList<>();
    String result = PREFIX_ALL;
    String[] dimensionsArray = dimensions.split(COMMA);
    if (dimensionsArray.length != 0) {
      if (!(dimensionsArray.length == 1 && dimensionsArray[0].isEmpty())) {
        for (String dimensionName : Arrays.asList(dimensionsArray)) {
          if (StringUtils.isNotBlank(dimensionName)) {
            String categoryNameBuilder = StringUtils.EMPTY;
            List<Integer> positionUppercase = new ArrayList<>();
            for (int i = 0; i < dimensionName.length(); i++) {
              if (Character.isUpperCase(dimensionName.charAt(i))) {
                positionUppercase.add(i);
              }
            }
            if (positionUppercase.size() == 1) {
              categoryNameBuilder = dimensionName;
            } else {
              categoryNameBuilder = dimensionName.substring(0, positionUppercase.get(1));
            }
            categoryNames.add(PREFIX_BY + categoryNameBuilder);
          }
        }
        result = categoryNames.stream().map(dimension -> dimension)
            .collect(Collectors.joining(WORD_UNION));
      }

    }
    return result;
  }

  /**
   * Method that assembles a map that has how to identify the dimensions with the value relating to
   * the name of the metric.
   * 
   * @param categoryMetricMap map that will return to the metrics with the dimensions.
   * @param result list of metrics returned from cloudwatch.
   */
  private void getDimensionsByMetrics(Map<String, String> categoryMetricMap,
      ListMetricsResult result) {
    for (final Metric metric : result.getMetrics()) {
      String dimensionKey = (String) metric.getDimensions().stream()
          .map(dimension -> dimension.getName()).collect(Collectors.joining(COMMA));
      categoryMetricMap.put(dimensionKey, metric.getMetricName());
    }
  }

  /**
   * This is used in a filter() operation, and it takes a key extractor function to derive a value
   * from the stream element. This derived value is in turn used do determine uniqueness.
   * 
   * @param keyExtractor class attribute that will be used to do distinct.
   * @return filter to use in the list
   */
  private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
    Map<Object, Boolean> seen = new ConcurrentHashMap<>();
    return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
  }

  /**
   * Method that validate enum values the Amazon Alert.
   * 
   * @param alertRequest represents the alert request.
   * @throws AlertException if something goes wrong
   */
  private void validateEnumAmazonAlert(AlertRequest alertRequest) throws AlertException {
    if (!EnumUtils.isValidEnum(Statistic.class, alertRequest.getStatistic())) {
      throw new AlertException("Statistic value not is valid");
    }

    if (!EnumUtils.isValidEnum(ComparisonOperator.class, alertRequest.getComparisonOperator())) {
      throw new AlertException("ComparisonOperator value not is valid");
    }

    if (!EnumUtils.isValidEnum(StandardUnit.class, alertRequest.getUnit())) {
      throw new AlertException("StandardUnit value not is valid");
    }
  }

  /**
   * Method that validate name of the Alert.
   * 
   * @param alertRequest represents the alert request.
   * @throws AlertException if something goes wrong
   */
  private void validateAlertName(AlertRequestGeneric alertRequest) throws AlertException {
    if (alertRequest.getAlarmName().isEmpty()) {
      throw new AlertException("Alert name is required");
    }
  }

  /**
   * Method that validates the notifications of the alert to be created.
   * 
   * @param alertRequest represents the alert request.
   * @throws AlertException if something goes wrong
   */
  private void validateAlertNotifications(AlertRequest alertRequest) throws AlertException {
    if (alertRequest.getNotifications() != null && !alertRequest.getNotifications().isEmpty()) {
      Set<String> alertNotifications = new HashSet<>();
      for (CloudosNotification cloudosNotification : alertRequest.getNotifications()) {
        alertNotifications.add(cloudosNotification.toString());
      }
      if (!alertNotifications.isEmpty()
          && alertRequest.getNotifications().size() != alertNotifications.size()) {
        throw new AlertException("You have duplicate notifications");
      }
    }
  }

  /**
   * Method that validates the necessary parameters and makes the connection with Aws CloudWatch.
   * 
   * @param region the region the client is located.
   * @return AmazonCloudWatch connection AWS.
   * @throws AlertException if something goes wrong
   */
  private AmazonCloudWatch getConnectionAwsCloudWatchSingleton(String region)
      throws AlertException {
    if (region == null || region.isEmpty()) {
      throw new AlertException("The region field is required to perform a connection com aws");
    }
    AmazonCloudWatch cloudWatch =
        ConnectionAwsCloudWatchSingleton.getInstance(region, awsCredentialService);
    return cloudWatch;
  }

  /**
   * Method that validates the necessary parameters and makes the connection with AmazonSNS.
   * 
   * @param region the region the client is located.
   * @return AmazonSNS connection SNS.
   * @throws AlertException if something goes wrong
   */
  private AmazonSNS getConnectionAwsSnsSingleton(final String region) throws AlertException {
    if (region == null || region.isEmpty()) {
      throw new AlertException("The region field is required to perform a connection com aws");
    }
    AmazonSNS sns = ConnectionAwsSnsSingleton.getInstance(region, this.awsCredentialService);
    return sns;
  }

  /**
   * Method that validate if alert name is filled or alert exists.
   * 
   * @param alertRequestGeneric Generic alert request.
   * @return AlertResponse class representing the alert search response.
   * @throws AlertException if something goes wrong.
   */
  private AlertResponse<CloudosAlert> validateAlert(AlertRequestGeneric alertRequest)
      throws AlertException {
    this.validateAlertName(alertRequest);
    return this.getAlertByName(alertRequest);
  }

  /**
   * Method that mount entity @{link CloudosAlert} from @{link MetricAlarm}.
   * 
   * @param alarm entity @{link MetricAlarm} the amazon aws.
   * @param region the region the client is located.
   * @return entity @{link CloudosAlert} with information the @{link MetricAlarm}.
   */
  private CloudosAlert mountCloudosAlertFromMetricAlarm(MetricAlarm alarm, String region) {
    CloudosAlert cloudosAlert = CloudosAlert.builder()
        .evaluationPeriods(alarm.getEvaluationPeriods()).metricName(alarm.getMetricName())
        .namespace(alarm.getNamespace()).period(alarm.getPeriod()).threshold(alarm.getThreshold())
        .actionsEnabled(alarm.getActionsEnabled()).statistic(alarm.getStatistic())
        .comparisonOperator(alarm.getComparisonOperator()).unit(alarm.getUnit())
        .alarmConfigurationUpdatedTimestamp(alarm.getAlarmConfigurationUpdatedTimestamp())
        .stateValue(alarm.getStateValue()).stateReason(alarm.getStateReason())
        .stateUpdatedTimestamp(alarm.getStateUpdatedTimestamp()).region(region)
        .treatMissingData(alarm.getTreatMissingData()).alarmName(alarm.getAlarmName())
        .alarmDescription(alarm.getAlarmDescription()).provider(Providers.AMAZON_AWS).build();

    List<CloudosDimension> dimensions = alarm.getDimensions().stream().map(dimensionResponse -> {
      return CloudosDimension.builder().name(dimensionResponse.getName())
          .value(dimensionResponse.getValue()).build();
    }).collect(Collectors.toList());
    cloudosAlert.setDimensions(dimensions);
    cloudosAlert.setNotifications(new ArrayList<CloudosNotification>());
    List<CloudosNotification> notificationsOk =
        alarm.getOKActions().stream().map(notificationResponse -> {
          return CloudosNotification.builder().codeStatus(MetricStatusAlarm.OK.name())
              .nameStatus(MetricStatusAlarm.OK.getDescription())
              .codeNotificationTopic(notificationResponse)
              .nameNotificationTopic(this.getNameTopic(notificationResponse)).build();
        }).collect(Collectors.toList());
    cloudosAlert.getNotifications().addAll(notificationsOk);
    List<CloudosNotification> notificationsAlarm =
        alarm.getAlarmActions().stream().map(notificationResponse -> {
          return CloudosNotification.builder().codeStatus(MetricStatusAlarm.ALARM.name())
              .nameStatus(MetricStatusAlarm.ALARM.getDescription())
              .codeNotificationTopic(notificationResponse)
              .nameNotificationTopic(this.getNameTopic(notificationResponse)).build();
        }).collect(Collectors.toList());
    cloudosAlert.getNotifications().addAll(notificationsAlarm);
    List<CloudosNotification> notificationInsufficientData =
        alarm.getInsufficientDataActions().stream().map(notificationResponse -> {
          return CloudosNotification.builder()
              .codeStatus(MetricStatusAlarm.INSUFFICIENT_DATA.name())
              .nameStatus(MetricStatusAlarm.INSUFFICIENT_DATA.getDescription())
              .codeNotificationTopic(notificationResponse)
              .nameNotificationTopic(this.getNameTopic(notificationResponse)).build();
        }).collect(Collectors.toList());
    cloudosAlert.getNotifications().addAll(notificationInsufficientData);

    return cloudosAlert;
  }

  /**
   * Method that mount entity @{link CloudosAlert} from @{link AlertRequest}.
   * 
   * @param alertRequest entity @{link AlertRequest}.
   * @return entity @{link CloudosAlert} with information the @{link AlertRequest}.
   */
  private CloudosAlert mountCloudosAlertFromRequest(AlertRequest alertRequest) {

    CloudosAlert cloudosAlert = CloudosAlert.builder()
        .evaluationPeriods(alertRequest.getEvaluationPeriods())
        .metricName(alertRequest.getMetricName()).namespace(alertRequest.getNamespace())
        .period(alertRequest.getPeriod()).threshold(alertRequest.getThreshold())
        .actionsEnabled(alertRequest.isActionsEnabled()).statistic(alertRequest.getStatistic())
        .comparisonOperator(alertRequest.getComparisonOperator()).unit(alertRequest.getUnit())
        .alarmConfigurationUpdatedTimestamp(alertRequest.getAlarmConfigurationUpdatedTimestamp())
        .stateValue(alertRequest.getStateValue()).stateReason(alertRequest.getStateReason())
        .stateUpdatedTimestamp(alertRequest.getStateUpdatedTimestamp())
        .readStatus(CloudosReadStatus.NOT_READ).region(alertRequest.getRegion())
        .alarmName(alertRequest.getAlarmName()).alarmDescription(alertRequest.getAlarmDescription())
        .provider(Providers.AMAZON_AWS).id(alertRequest.getId())
        .dimensions(alertRequest.getDimensions()).notifications(alertRequest.getNotifications())
        .build();

    return cloudosAlert;
  }

  /**
   * Method that mounts alert dimensions in amazon aws.
   * 
   * @param alertRequest represents the alert request.
   * @param request Represents the request to create the alarm in aws.
   */
  ;

  private void mountAlertDimensions(final AlertRequest alertRequest,
      final PutMetricAlarmRequest request) {
    List<Dimension> dimensions = (List<Dimension>) alertRequest
        .getDimensions().stream().map(dimensionRequest -> new Dimension()
            .withName(dimensionRequest.getName()).withValue(dimensionRequest.getValue()))
        .collect(Collectors.toList());
    request.setDimensions(dimensions);
  }

  /**
   * Method that mounts alert notifications for save in amazon aws.
   * 
   * @param alertRequest represents the alert request.
   * @param request Represents the request to create the alarm in aws.
   */
  private void mountAlertNotifications(final AlertRequest alertRequest,
      final PutMetricAlarmRequest request) {
    Map<String, List<String>> alertNotifications = new HashMap<String, List<String>>();
    if (alertRequest.getNotifications() != null && !alertRequest.getNotifications().isEmpty()) {
      for (CloudosNotification notifications : alertRequest.getNotifications()) {
        if (alertNotifications.containsKey(notifications.getCodeStatus())) {
          alertNotifications.get(notifications.getCodeStatus())
              .add(notifications.getCodeNotificationTopic());
        } else {
          List<String> notificationTopic = new ArrayList<String>();
          notificationTopic.add(notifications.getCodeNotificationTopic());
          alertNotifications.put(notifications.getCodeStatus(), notificationTopic);
        }
      }
      if (alertNotifications.get(MetricStatusAlarm.ALARM.name()) != null) {
        request.withAlarmActions(alertNotifications.get(MetricStatusAlarm.ALARM.name()));
      }
      if (alertNotifications.get(MetricStatusAlarm.OK.name()) != null) {
        request.withOKActions(alertNotifications.get(MetricStatusAlarm.OK.name()));
      }
      if (alertNotifications.get(MetricStatusAlarm.INSUFFICIENT_DATA.name()) != null) {
        request.withInsufficientDataActions(
            alertNotifications.get(MetricStatusAlarm.INSUFFICIENT_DATA.name()));
      }
    }
  }

  /**
   * Method that returns only the topic name from the ARN topic.
   * 
   * @param topicArn name of topic in cloudwatch.
   * @return String topic name.
   */
  private String getNameTopic(final String topicArn) {
    String result = StringUtils.EMPTY;
    if (StringUtils.isNotBlank(topicArn)) {
      final String[] topicArnArray = topicArn.split(COLON);
      result = topicArnArray[topicArnArray.length - 1];
    }
    return result;
  }

  /**
   * Validating method if alert is already existent.
   * 
   * @param alertRequest represents the alert request.
   * @param isCreated Boolean which identifies whether the method is used to create the alert
   * @throws AlertException if something goes wrong.
   */
  private void validateAlertExistent(AlertRequest alertRequest, boolean isCreated)
      throws AlertException {
    if (isCreated) {
      CloudosAlert cloudosAlert = cloudosAlertService
          .getAlertByAlarmNameAndRegion(alertRequest.getAlarmName(), alertRequest.getRegion());
      if (cloudosAlert != null) {
        throw new AlertException(
            String.format("Alert with name %s already exists", alertRequest.getAlarmName()));
      }
    }
  }
}
