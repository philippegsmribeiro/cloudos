package cloudos.alerts.manager;

import cloudos.alerts.AlertException;
import cloudos.exceptions.NotImplementedException;
import cloudos.models.alerts.AlertRequest;
import cloudos.models.alerts.AlertRequestGeneric;
import cloudos.models.alerts.AlertResponse;
import cloudos.models.alerts.CategoryMetricResponse;
import cloudos.models.alerts.GenericAlert;
import cloudos.models.alerts.MetricMissingDataAlertResponse;
import cloudos.models.alerts.MetricNotificationTopicResponse;
import cloudos.models.alerts.MetricPeriodAlertResponse;
import cloudos.models.alerts.MetricRequest;
import cloudos.models.alerts.MetricResponse;
import cloudos.models.alerts.MetricStatisticRequest;
import cloudos.models.alerts.MetricStatisticResponse;
import cloudos.models.alerts.MetricStatusAlarmResponse;
import java.util.List;

/**
 * Abstract class for all AlertManager.
 * 
 * @author Alex Calagua
 *
 */
public abstract class AlertManager<T extends GenericAlert> {

  /**
   * Generic method to create an alert for a particular provider.
   * 
   * @param alert Request Class that represents the request to create an the Alert.
   * @param notify parameter that enables the sending of the notification.
   * @return AlertResponse Class representing the response to the creation of an alert.
   * @throws AlertException if something goes wrong
   */
  public abstract AlertResponse<T> createAlert(AlertRequest alert, boolean notify)
      throws AlertException;

  /**
   * Generic method to delete an alert for a particular provider.
   * 
   * @param alertRequestGeneric Class that represents the request to delete an the Alert.
   * @return AlertResponse Class representing the response to the deletion of an alert.
   * @throws AlertException if something goes wrong.
   */
  public abstract AlertResponse<T> deleteAlert(AlertRequestGeneric alertRequestGeneric)
      throws AlertException;

  /**
   * Generic method that return list the alert from a particular provider.
   * 
   * @param alertRequestGeneric Class that represents the request to search the alerts.
   * @return list the alert.
   * @throws AlertException if something goes wrong
   */
  public abstract List<T> getListAlert(AlertRequestGeneric alertRequestGeneric)
      throws AlertException;

  /**
   * Generic method to update an alert for a particular provider.
   * 
   * @param alert Class that represents the request to update an the Alert.
   * @return AlertResponse Class representing the response to the updating of an alert.
   * @throws AlertException if something goes wrong
   */
  public abstract AlertResponse<T> updateAlert(AlertRequest alert) throws AlertException;

  /**
   * Generic method to search an alert by name.
   * 
   * @param alertRequestGeneric Class that represents the request to search the alerts by name.
   * @return AlertResponse Class that represents the search response performed
   * @throws AlertException if something goes wrong
   */
  public abstract AlertResponse<T> getAlertByName(AlertRequestGeneric alertRequestGeneric)
      throws AlertException;

  /**
   * Method that return a list of metrics and dimensions that are posted by AWS services.
   * 
   * @param metricRequest Class that represents the request to search the metric.
   * @return list the Metrics with dimensions.
   * @throws AlertException if something goes wrong
   */
  public abstract List<MetricResponse> getListMetrics(MetricRequest metricRequest)
      throws AlertException;

  /**
   * Method that returns a metric namespace list.
   * 
   * @param metricRequest request to returns metric namespace list.
   * @return list the metric namespace.
   * @throws AlertException if something goes wrong
   */
  public abstract List<String> getListNameSpaceOfMetrics(MetricRequest metricRequest)
      throws AlertException;

  /**
   * Method that returns a metric category list.
   * 
   * @param metricRequest request to returns metric category list.
   * @return list the metric categories.
   * @throws AlertException if something goes wrong
   */
  public abstract List<CategoryMetricResponse> getListCategoriesOfMetrics(
      MetricRequest metricRequest) throws AlertException;

  /**
   * Method that returns a metric status alarm list.
   * 
   * @return list the metric status alarm.
   */
  public abstract List<MetricStatusAlarmResponse> getListMetricStatusAlarm();

  /**
   * Method that returns a notification topic list.
   * 
   * @param metricRequest request to returns notification topic list.
   * @return list the notification topic list.
   * @throws AlertException if something goes wrong
   */
  public abstract List<MetricNotificationTopicResponse> getListNotificationTopic(
      MetricRequest metricRequest) throws AlertException;

  /**
   * Method that returns a metric statistic list.
   * 
   * @param metricStatisticRequest request to return the metric statistic list.
   * @return list the metric statistic.
   * @throws AlertException if something goes wrong
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  public abstract List<MetricStatisticResponse> getListMetricStatistic(
      MetricStatisticRequest metricStatisticRequest) throws AlertException;

  /**
   * Method that returns a metric period list.
   * 
   * @return list the metric period.
   * @throws AlertException if something goes wrong
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  public abstract List<MetricPeriodAlertResponse> getListMetricPeriodAlert();

  /**
   * Method that returns missing data list options.
   * 
   * @return list the missing data list options
   * @throws AlertException if something goes wrong.
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  public abstract List<MetricMissingDataAlertResponse> getListMetricMissingDataAlert();



}
