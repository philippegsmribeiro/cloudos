package cloudos.alerts.manager;

import cloudos.amazon.AWSCredentialService;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Connection Class. This Singleton Class is responsible for loading the connection with amazon aws
 * sns.
 *
 * @author Alex Calagua
 */
public class ConnectionAwsSnsSingleton {

  /**
   * Store connection AmazonSNS for each region.
   */
  private static ConcurrentMap<String, AmazonSNS> mapInstanceSnS =
      new ConcurrentHashMap<String, AmazonSNS>();

  /**
   * Method that connects with Amazon SNS.
   * 
   * @param region the region the client is located
   * @param awsCredentialService Service class keeping the AWSCredentials for the services.
   */
  private static AmazonSNS connectionAwsSingleton(String region,
      AWSCredentialService awsCredentialService) {
    return AmazonSNSClientBuilder.standard()
        .withCredentials(awsCredentialService.getAWSCredentials()).withRegion(region).build();
  }

  /**
   * Get a {@link AmazonSNS} Instance by region There should be only one {@link AmazonSNS} per
   * region.
   * 
   * @param region the region the client is located
   * @param awsCredentialService Service class keeping the AWSCredentials for the services.
   * @return ConnectionAwsSingleton the stored Instance of this class
   */
  public static AmazonSNS getInstance(String region, AWSCredentialService awsCredentialService) {
    if (!mapInstanceSnS.containsKey(region)) {
      mapInstanceSnS.put(region, connectionAwsSingleton(region, awsCredentialService));
    }
    return mapInstanceSnS.get(region);
  }

  /**
   * Default Constructor. Private constructor prevents instantiation from other classes
   */
  private ConnectionAwsSnsSingleton() {}

}
