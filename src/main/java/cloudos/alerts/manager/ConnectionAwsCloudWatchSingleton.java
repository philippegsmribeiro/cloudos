package cloudos.alerts.manager;

import cloudos.amazon.AWSCredentialService;
import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClientBuilder;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Connection Class. This Singleton Class is responsible for loading the connection with amazon aws.
 *
 * @author Alex Calagua
 */
public class ConnectionAwsCloudWatchSingleton {

  /**
   * Store connection AmazonCloudWatch for each region.
   */
  private static ConcurrentMap<String, AmazonCloudWatch> mapInstance =
      new ConcurrentHashMap<String, AmazonCloudWatch>();

  /**
   * Method that connects with CloudWatch.
   * 
   * @param region the region the client is located
   * @param awsCredentialService Service class keeping the AWSCredentials for the services.
   */
  private static AmazonCloudWatch connectionAwsSingleton(String region,
      AWSCredentialService awsCredentialService) {
    return AmazonCloudWatchClientBuilder.standard()
        .withCredentials(awsCredentialService.getAWSCredentials()).withRegion(region).build();
  }

  /**
   * Get a {@link AmazonCloudWatch} Instance by region There should be only one
   * {@link AmazonCloudWatch} per region.
   * 
   * @param region the region the client is located
   * @param awsCredentialService Service class keeping the AWSCredentials for the services.
   * @return ConnectionAwsSingleton the stored Instance of this class
   */
  public static AmazonCloudWatch getInstance(String region,
      AWSCredentialService awsCredentialService) {
    if (!mapInstance.containsKey(region)) {
      mapInstance.put(region, connectionAwsSingleton(region, awsCredentialService));
    }
    return mapInstance.get(region);
  }

  /**
   * Default Constructor. Private constructor prevents instantiation from other classes
   */
  private ConnectionAwsCloudWatchSingleton() {}

}
