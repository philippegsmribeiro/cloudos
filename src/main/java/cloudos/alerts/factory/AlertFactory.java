package cloudos.alerts.factory;

import cloudos.Providers;
import cloudos.alerts.AlertException;
import cloudos.alerts.manager.AlertManager;
import cloudos.exceptions.NotImplementedException;

/**
 * This is the abstract factory the Client code uses to build different Alert Manager. Concrete
 * factories subclass this to build/instantiate the actual AlertManager objects.
 *
 * @author Alex Calagua
 */
public interface AlertFactory {

  /**
   * This is the 'Factory Method' that all concrete factory subclasses must implement to return the
   * AlertManager that they build. Note that the method is protected and only visible to subclass
   * factories.
   *
   * @param provider type of provider that AlertManager will build
   * @return the built AlertManager.
   * @throws NotImplementedException Exception
   * @throws AlertException Exception
   */
  AlertManager<?> getAlertManager(Providers provider)
      throws NotImplementedException, AlertException;

}
