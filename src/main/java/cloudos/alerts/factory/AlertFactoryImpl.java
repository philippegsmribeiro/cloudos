package cloudos.alerts.factory;

import cloudos.Providers;
import cloudos.alerts.AlertException;
import cloudos.alerts.manager.AlertManager;
import cloudos.alerts.manager.AmazonAlertManager;
import cloudos.alerts.manager.GoogleAlertManager;
import cloudos.exceptions.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Concrete factory implementation for building Alert Manager, e.g. AmazonAlertManager,
 * GoogleAlertManager
 *
 * @author Alex Calagua
 */
@Service
public class AlertFactoryImpl implements AlertFactory {
  @Autowired
  AmazonAlertManager amazonAlertManager;
  @Autowired
  GoogleAlertManager googleAlertManager;

  @Override
  public AlertManager<?> getAlertManager(Providers provider)
      throws NotImplementedException, AlertException {
    this.validateProvider(provider);
    switch (provider) {
      case AMAZON_AWS:
        return amazonAlertManager;
      case GOOGLE_COMPUTE_ENGINE:
        return googleAlertManager;
      default:
        throw new NotImplementedException();
    }
  }

  /**
   * Method that validate the provider is not null.
   * 
   * @param provider type of provider.
   * @throws AlertException if something goes wrong
   */
  private void validateProvider(Providers provider) throws AlertException {
    if (provider == null) {
      throw new AlertException("Provider can not be null");
    }
  }

}
