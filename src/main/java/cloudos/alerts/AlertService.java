package cloudos.alerts;

import cloudos.alerts.factory.AlertFactory;
import cloudos.exceptions.NotImplementedException;
import cloudos.models.alerts.AlertRequest;
import cloudos.models.alerts.AlertRequestGeneric;
import cloudos.models.alerts.AlertResponse;
import cloudos.models.alerts.CategoryMetricResponse;
import cloudos.models.alerts.CloudosAlert;
import cloudos.models.alerts.GenericAlert;
import cloudos.models.alerts.MetricMissingDataAlertResponse;
import cloudos.models.alerts.MetricNotificationTopicResponse;
import cloudos.models.alerts.MetricPeriodAlertResponse;
import cloudos.models.alerts.MetricRequest;
import cloudos.models.alerts.MetricResponse;
import cloudos.models.alerts.MetricStatisticRequest;
import cloudos.models.alerts.MetricStatisticResponse;
import cloudos.models.alerts.MetricStatusAlarmResponse;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service implementation for alerts.
 * 
 * @author Alex Calagua
 *
 */
@Service
public class AlertService {
  private static final String ALERT_REQUEST = "Alert";
  private static final String METRIC_REQUEST = "Metric";
  private static final String MESSAGE_ERRO_REQUEST = "%s request can not be null";
  @Autowired
  private AlertFactory alertFactory;

  @Autowired
  private CloudosAlertService cloudosAlertService;


  /**
   * Method that create an alert.
   * 
   * @param alertRequest request to create an the Alert.
   * @return AlertResponse Class representing the response to the creation of an alert.
   * @throws AlertException if something goes wrong
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  public AlertResponse<?> createAlert(AlertRequest alertRequest)
      throws AlertException, NotImplementedException {
    this.validateRequest(alertRequest, ALERT_REQUEST);
    return alertFactory.getAlertManager(alertRequest.getProvider()).createAlert(alertRequest, true);
  }

  /**
   * Method that returns a list of alerts.
   * 
   * @param alertRequest Class that represents the request to search the alerts.
   * @return list the alerts
   * @throws AlertException if something goes wrong
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  public List<? extends GenericAlert> getListAlert(AlertRequestGeneric alertRequest)
      throws AlertException, NotImplementedException {
    this.validateRequest(alertRequest, ALERT_REQUEST);
    if (StringUtils.isNotBlank(alertRequest.getRegion()) || alertRequest.getProvider() != null) {
      return cloudosAlertService.getListAlertByFilter(alertRequest);
    } else {
      return cloudosAlertService.getListAlert();
    }
  }

  /**
   * Method that returns a list of alerts.
   * 
   * @param alertRequest Class that represents the request to search the alerts.
   * @throws AlertException if something goes wrong
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  public void synchronizedAlertByProvider(AlertRequestGeneric alertRequest)
      throws AlertException, NotImplementedException {
    this.validateRequest(alertRequest, ALERT_REQUEST);
    List<CloudosAlert> listCloudosAlert = cloudosAlertService.getListAlert();

    if (listCloudosAlert != null && !listCloudosAlert.isEmpty()) {
      for (CloudosAlert cloudosAlert : listCloudosAlert) {
        alertFactory.getAlertManager(alertRequest.getProvider())
            .getAlertByName(AlertRequest.builder().alarmName(cloudosAlert.getAlarmName())
                .region(cloudosAlert.getRegion()).provider(alertRequest.getProvider()).build());
      }
    }
  }

  /**
   * Method that searches for alert by name.
   * 
   * @param alertRequest request to search an alert by name..
   * @return AlertResponse class representing the alert search response.
   * @throws AlertException if something goes wrong
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  public AlertResponse<?> getAlertByName(AlertRequestGeneric alertRequest)
      throws AlertException, NotImplementedException {
    this.validateRequest(alertRequest, ALERT_REQUEST);
    return alertFactory.getAlertManager(alertRequest.getProvider()).getAlertByName(alertRequest);
  }

  /**
   * Method that delete an alert.
   * 
   * @param alertRequest request to delete an Alert.
   * @return AlertResponse Class representing the response to the deletion of an alert.
   * @throws AlertException if something goes wrong
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  public AlertResponse<?> deleteAlert(AlertRequestGeneric alertRequest)
      throws AlertException, NotImplementedException {
    this.validateRequest(alertRequest, ALERT_REQUEST);
    return alertFactory.getAlertManager(alertRequest.getProvider()).deleteAlert(alertRequest);
  }

  /**
   * Method that update an alert.
   * 
   * @param alertRequest request to update an Alert.
   * @return AlertResponse Class representing the response to the updating of an alert.
   * @throws AlertException if something goes wrong
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  public AlertResponse<?> updateAlert(AlertRequest alertRequest)
      throws AlertException, NotImplementedException {
    this.validateRequest(alertRequest, ALERT_REQUEST);
    return alertFactory.getAlertManager(alertRequest.getProvider()).updateAlert(alertRequest);
  }

  /**
   * Method that return a list of metrics and dimensions that are posted by AWS services.
   * 
   * @param metricRequest request to return list the metrics.
   * @return list the metrics.
   * @throws AlertException if something goes wrong
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  public List<MetricResponse> getListMetric(MetricRequest metricRequest)
      throws AlertException, NotImplementedException {
    this.validateRequest(metricRequest, METRIC_REQUEST);
    return alertFactory.getAlertManager(metricRequest.getProvider()).getListMetrics(metricRequest);
  }


  /**
   * Method that returns a list the namespace of the metrics.
   * 
   * @param metricRequest request to return the metric namespaces.
   * @return list the namespace of the metrics.
   * @throws AlertException if something goes wrong
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  public List<String> getListNameSpaceOfMetrics(MetricRequest metricRequest)
      throws AlertException, NotImplementedException {
    this.validateRequest(metricRequest, METRIC_REQUEST);
    return alertFactory.getAlertManager(metricRequest.getProvider())
        .getListNameSpaceOfMetrics(metricRequest);
  }

  /**
   * Method that returns a list the category of the metrics.
   * 
   * @param metricRequest request to return the metric categories.
   * @return list the category of the metrics.
   * @throws AlertException if something goes wrong
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  public List<CategoryMetricResponse> getListCategoriesOfMetrics(MetricRequest metricRequest)
      throws AlertException, NotImplementedException {
    return alertFactory.getAlertManager(metricRequest.getProvider())
        .getListCategoriesOfMetrics(metricRequest);
  }

  /**
   * Method that returns a metric status alarm list.
   * 
   * @param metricRequest request to return the metric status alarms
   * @return list the metric status alarm.
   * @throws AlertException if something goes wrong
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  public List<MetricStatusAlarmResponse> getListMetricStatusAlarm(MetricRequest metricRequest)
      throws AlertException, NotImplementedException {
    return alertFactory.getAlertManager(metricRequest.getProvider()).getListMetricStatusAlarm();
  }

  /**
   * Method that returns a notification topic list.
   * 
   * @param metricRequest request to returns notification topic list.
   * @return list the notification topic list.
   * @throws AlertException if something goes wrong
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  public List<MetricNotificationTopicResponse> getListNotificationTopic(MetricRequest metricRequest)
      throws AlertException, NotImplementedException {
    return alertFactory.getAlertManager(metricRequest.getProvider())
        .getListNotificationTopic(metricRequest);
  }


  /**
   * Method that validate the request.
   * 
   * @param request request object to be validated.
   * @param nameRequest name related to the request made.
   * @throws AlertException if something goes wrong
   */
  private void validateRequest(Object request, String nameRequest) throws AlertException {
    if (request == null) {
      throw new AlertException(String.format(MESSAGE_ERRO_REQUEST, nameRequest));
    }
  }

  /**
   * Method that returns a metric statistic list.
   * 
   * @param metricStatisticRequest request to return the metric statistic list.
   * @return list the metric statistic.
   * @throws AlertException if something goes wrong
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  public List<MetricStatisticResponse> getListMetricStatistic(
      MetricStatisticRequest metricStatisticRequest)
      throws AlertException, NotImplementedException {
    return alertFactory.getAlertManager(metricStatisticRequest.getProvider())
        .getListMetricStatistic(metricStatisticRequest);
  }

  /**
   * Method that returns a metric period list.
   * 
   * @param metricRequest request to return the metric period list.
   * @return list the metric period.
   * @throws AlertException if something goes wrong
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  public List<MetricPeriodAlertResponse> getListMetricPeriodAlert(MetricRequest metricRequest)
      throws AlertException, NotImplementedException {
    return alertFactory.getAlertManager(metricRequest.getProvider()).getListMetricPeriodAlert();
  }

  /**
   * Method that returns missing data list options.
   * 
   * @param metricRequest request to return missing data list options.
   * @return list the missing data list options
   * @throws AlertException if something goes wrong.
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  public List<MetricMissingDataAlertResponse> getListMetricMissingDataAlert(
      MetricRequest metricRequest) throws AlertException, NotImplementedException {
    return alertFactory.getAlertManager(metricRequest.getProvider())
        .getListMetricMissingDataAlert();
  }
}

