package cloudos.alerts;

import cloudos.Providers;
import cloudos.models.alerts.CloudosAlert;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * Store all the Cloudos Alert in a Mongo collection.
 * 
 * @author Alex Calagua
 */
interface CloudosAlertRepository
    extends MongoRepository<CloudosAlert, String>, QueryDslPredicateExecutor<CloudosAlert> {

  CloudosAlert findByAlarmNameAndRegion(String alarmName, String region);

  List<CloudosAlert> findByRegionOrProviderOrAlarmName(String region, Providers provider,
      String alarmName);

}
