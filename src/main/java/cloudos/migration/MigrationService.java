package cloudos.migration;

import cloudos.Providers;
import cloudos.exceptions.NotImplementedException;
import cloudos.instances.AmazonInstanceManagement;
import cloudos.instances.GoogleInstanceManagement;
import cloudos.instances.InstanceManagement;
import cloudos.instances.InstanceRequestException;
import cloudos.models.AbstractInstance;
import cloudos.queue.QueueManagerService;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class MigrationService {

  @Autowired
  private AmazonInstanceManagement amazonInstanceManagement;
  @Autowired
  private GoogleInstanceManagement googleInstanceManagement;
  @Autowired
  QueueManagerService queueManagerService;

  /**
   * Get management class.
   *
   * @param providers a cloud provider
   * @return a instance of the InstanceManagement
   * @throws NotImplementedException whenever the service is not implemented for the provider
   */
  private InstanceManagement<?> getManagement(Providers providers) throws NotImplementedException {
    switch (providers) {
      case AMAZON_AWS:
        return amazonInstanceManagement;
      case GOOGLE_COMPUTE_ENGINE:
        return googleInstanceManagement;
      default:
        break;
    }
    throw new NotImplementedException();
  }

  /**
   * Clone an instance.
   *
   * @param originalInstance to be cloned
   * @return a new instance cloned from the original
   * @throws NotImplementedException whenever the service is not implemented for the provider
   * @throws InstanceRequestException if something goes wrong
   */
  public <T extends AbstractInstance> T cloneInstance(AbstractInstance originalInstance)
      throws NotImplementedException, InstanceRequestException {
    return (T) getManagement(originalInstance.getInstance().getProvider())
        .cloneInstance(originalInstance);
  }
}
