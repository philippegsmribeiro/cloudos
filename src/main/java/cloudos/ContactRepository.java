package cloudos;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

/** @author Philippe Ribeiros */
public interface ContactRepository extends MongoRepository<Contact, String> {

  // Define the interfaces to be used to query the 'contact' collection
  public Contact findByName(String name);

  public List<Contact> findByEmail(String email);

  public Contact findByMessage(String message);
}
