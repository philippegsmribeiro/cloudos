package cloudos.queue;

import cloudos.Application;
import cloudos.queue.message.AbstractQueueMessage;
import cloudos.queue.message.QueueMessageType;
import cloudos.security.LogFilter;

import java.util.List;

import lombok.extern.log4j.Log4j2;

import org.apache.logging.log4j.ThreadContext;

/**
 * Thread class for the listeners.
 *
 * @author flavio
 */
@Log4j2
class QueueListenerTask implements Runnable {

  /** Min duration between the readings. */
  static final int MIN_DURATION = 5 * 1000;

  QueueMessageType queueMessageType;
  QueueListenerImpl queueListener;

  public QueueListenerTask(QueueListenerImpl queueListener, QueueMessageType queueMessageType) {
    super();
    this.queueListener = queueListener;
    this.queueMessageType = queueMessageType;
  }

  @Override
  public void run() {
    String threadName = queueMessageType.name();
    ThreadContext.put(LogFilter.LOG_PATH, threadName);
    log.info("Listening... {}", queueMessageType);
    while (true) {
      if (!Application.STARTED.get()) {
        try {
          log.debug("Waiting the application gets started to start the {} queue listener!",
              queueMessageType);
          Thread.sleep(MIN_DURATION);
        } catch (Exception e) {
          log.error(e);
        }
      } else {
        long start = System.currentTimeMillis();
        List<AbstractQueueMessage> messages;
        try {
          // read messages
          messages = queueListener.queueService.readMessages(queueMessageType);
          for (AbstractQueueMessage message : messages) {
            // notify consumers
            queueListener.consumerList.forEach(c -> {
              // just notify the consumer for the message type
              if (c.getType().equals(message.getClass())) {
                try {
                  c.acceptAbstractQueueMessage(message);
                } catch (Exception e) {
                  // adding a try catch to show the message consuming errors
                  log.error("Error consuming {} message {}", c.getType(), message, e);
                }
              }
            });
          }
          // TODO: delete just the consumed messages
          // delete messages afterwards
          queueListener.queueService.deleteMessages(messages);
        } catch (QueueServiceException e) {
          log.error("Error reading the messages", e);
        }
        //
        // if less the minDuration seconds, wait
        long duration = System.currentTimeMillis() - start;
        if (duration < MIN_DURATION) {
          try {
            long waitingTime = MIN_DURATION - duration;
            log.debug("Waiting for {} seconds", waitingTime / 1000);
            Thread.sleep(waitingTime);
            log.debug("Finished waiting");
          } catch (Exception e) {
            log.error("Error waiting the thread", e);
          }
        }
      }
    }
  }
}
