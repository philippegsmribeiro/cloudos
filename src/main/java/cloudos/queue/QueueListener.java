package cloudos.queue;

import cloudos.queue.message.AbstractQueueMessage.AbstractQueueMessageConsumer;

public interface QueueListener {

  /**
   * Add a consumer to the listener.
   *
   * @param consumer queue
   */
  void addConsumer(AbstractQueueMessageConsumer<?> consumer);
}
