package cloudos.queue;

import cloudos.config.ThreadingConfig;
import cloudos.queue.message.AbstractQueueMessage.AbstractQueueMessageConsumer;
import cloudos.queue.message.QueueMessageType;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.task.AsyncTaskExecutor;

/** Listener class for the queue messages. */
@Lazy
@Log4j2
public class QueueListenerImpl implements QueueListener {

  @Autowired
  QueueService queueService;

  @Autowired
  @Qualifier(ThreadingConfig.QUEUE)
  AsyncTaskExecutor taskExecutor;

  @PostConstruct
  private void startListener() {
    log.info("Starting queue listener!");
    for (QueueMessageType queueMessageType : QueueMessageType.values()) {
      taskExecutor.execute(new QueueListenerTask(this, queueMessageType));
    }
  }

  /** List of consumers. */
  List<AbstractQueueMessageConsumer<?>> consumerList = new ArrayList<>();

  @Override
  public void addConsumer(AbstractQueueMessageConsumer<?> consumer) {
    log.info("Adding consumer for {}.", consumer.getType());
    this.consumerList.add(consumer);
  }
}
