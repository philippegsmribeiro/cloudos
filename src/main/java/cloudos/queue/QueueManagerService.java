package cloudos.queue;

import cloudos.billings.BillingFile;
import cloudos.deploy.deployments.Deployment;
import cloudos.keys.cloudoskey.CloudosKey;
import cloudos.models.AbstractInstance;
import cloudos.models.CloudActionRequest;
import cloudos.models.CloudosCredential;
import cloudos.models.CloudosDatapoint;
import cloudos.models.Instance;
import cloudos.models.InstanceStatus;
import cloudos.models.alerts.GenericAlert;
import cloudos.models.autoscaler.AutoscaleRequest;
import cloudos.models.spotinstances.CloudSpotCreateRequest;
import cloudos.models.spotinstances.CloudSpotCreateResponse;
import cloudos.models.spotinstances.CloudSpotPricingRequest;
import cloudos.models.spotinstances.CloudSpotPricingResponse;
import cloudos.notifications.Notification;
import cloudos.queue.message.AlertCreatedMessage;
import cloudos.queue.message.AlertDeletedMessage;
import cloudos.queue.message.AlertUpdatedMessage;
import cloudos.queue.message.BillingFileDownloadedMessage;
import cloudos.queue.message.ClientInstallMessage;
import cloudos.queue.message.ClientReInstallMessage;
import cloudos.queue.message.CloudActionRequestMessage;
import cloudos.queue.message.CredentialChangeMessage;
import cloudos.queue.message.DatapointMessage;
import cloudos.queue.message.DeploymentMessage;
import cloudos.queue.message.DeploymentStopMessage;
import cloudos.queue.message.InstanceCreatedMessage;
import cloudos.queue.message.InstanceDeletedMessage;
import cloudos.queue.message.InstanceKeyNotFoundMessage;
import cloudos.queue.message.InstanceKeySavedMessage;
import cloudos.queue.message.InstanceUpdatedMessage;
import cloudos.queue.message.NotificationMessage;
import cloudos.queue.message.SpotInstanceTerminatedMessage;
import cloudos.queue.message.autoscale.AutoscaleMessage;
import cloudos.queue.message.insights.CpuInsightMessage;
import cloudos.queue.message.insights.FilesystemInsightMessage;
import cloudos.queue.message.insights.MemoryInsightMessage;
import cloudos.queue.message.insights.ProcessInsightMessage;
import cloudos.queue.message.spotbidder.SpotCreateRequestMessage;
import cloudos.queue.message.spotbidder.SpotCreateResponseMessage;
import cloudos.queue.message.spotbidder.SpotPricingPredictRequestMessage;
import cloudos.queue.message.spotbidder.SpotPricingPredictResponseMessage;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import javax.validation.constraints.NotNull;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Utility service class to centralize all the queue message sending.
 */
@Service
@Log4j2
public class QueueManagerService {

  @Autowired
  QueueService queueService;

  /**
   * Send a Autoscale message to the queue.
   *
   * @param autoscaleRequest the autoscale request
   */
  public void sendAutoscaleMessage(AutoscaleRequest autoscaleRequest) {
    try {
      AutoscaleMessage message = new AutoscaleMessage();
      message.setAutoscaleRequest(autoscaleRequest);
      queueService.sendMessage(message);
    } catch (QueueServiceException e) {
      log.error("Error sending the message", e);
    }
  }

  /**
   * Send a new Process Insight message.
   *
   * @param message the message about the process
   */
  public void sendProcessInsightMessage(@NotNull String message) {
    ProcessInsightMessage insightMessage = new ProcessInsightMessage();
    insightMessage.setMessage(message);
    try {
      this.queueService.sendMessage(insightMessage);
    } catch (QueueServiceException ex) {
      log.error("Error sending Process insight message", ex);
    }
  }

  /**
   * Send a new Filesystem Insight message.
   *
   * @param message the message about the process
   */
  public void sendFilesystemInsightMessage(@NotNull String message) {
    FilesystemInsightMessage insightMessage = new FilesystemInsightMessage();
    insightMessage.setMessage(message);
    try {
      this.queueService.sendMessage(insightMessage);
    } catch (QueueServiceException ex) {
      log.error("Error sending Filesystem insight message", ex);
    }
  }

  /**
   * Send a new Memory Insight message.
   *
   * @param message the message about the process
   */
  public void sendMemoryInsightMessage(@NotNull String message) {
    MemoryInsightMessage insightMessage = new MemoryInsightMessage();
    insightMessage.setMessage(message);
    try {
      this.queueService.sendMessage(insightMessage);
    } catch (QueueServiceException ex) {
      log.error("Error sending Memory insight message", ex);
    }
  }

  /**
   * Send a new CPU Insight Message.
   *
   * @param message the message about the process
   */
  public void sendCpuInsightMessage(@NotNull String message) {
    CpuInsightMessage insightMessage = new CpuInsightMessage();
    insightMessage.setMessage(message);
    try {
      this.queueService.sendMessage(insightMessage);
    } catch (QueueServiceException ex) {
      log.error("Error sending CPU insight message", ex);
    }
  }

  /**
   * Send a message about a new instance created/found.
   *
   * @param instance object
   */
  public void sendInstanceCreatedMessage(AbstractInstance instance) {
    try {
      InstanceCreatedMessage message = new InstanceCreatedMessage();
      message.setInstance(instance);
      queueService.sendMessage(message);
    } catch (QueueServiceException e) {
      log.error("Error sending the message", e);
    }
  }

  /**
   * Send a message about a instance been updated.
   *
   * @param oldInstanceStatus status
   * @param newInstance object
   */
  public void sendInstanceUpdatedMessage(InstanceStatus oldInstanceStatus,
      AbstractInstance newInstance) {
    try {
      InstanceUpdatedMessage message = new InstanceUpdatedMessage();
      message.setOldInstanceStatus(oldInstanceStatus);
      message.setNewInstance(newInstance);
      queueService.sendMessage(message);
    } catch (QueueServiceException e) {
      log.error("Error sending the message", e);
    }
  }

  /**
   * Send a message after a instance been delete / not found in the provider.
   *
   * @param instance object
   */
  public void sendInstanceDeletedMessage(Instance instance) {
    try {
      InstanceDeletedMessage message = new InstanceDeletedMessage();
      message.setInstance(instance);
      queueService.sendMessage(message);
    } catch (QueueServiceException e) {
      log.error("Error sending the message", e);
    }
  }

  /**
   * Send a message when a deploy is change.
   *
   * @param deployment object
   * @param text description
   */
  public void sendDeploymentMessage(Deployment deployment, String text) {
    try {
      DeploymentMessage message = new DeploymentMessage();
      message.setDeployment(deployment);
      message.setDescription(text);
      queueService.sendMessage(message);
    } catch (QueueServiceException e) {
      log.error("Error sending the message", e);
    }
  }

  /**
   * Send a message of a new notification.
   *
   * @param notification object
   */
  public void sendNotificationMessage(Notification notification) {
    try {
      NotificationMessage message = new NotificationMessage();
      message.setNotification(notification);
      queueService.sendMessage(message);
    } catch (QueueServiceException e) {
      log.error("Error sending the message", e);
    }
  }

  /**
   * Send a message of a cloud action request is made.
   *
   * @param request object
   * @param userName user
   */
  public void sendCloudActionRequestNotification(CloudActionRequest request, String userName) {
    try {
      CloudActionRequestMessage message = new CloudActionRequestMessage();
      message.setRequest(request);
      message.setUser(userName);
      queueService.sendMessage(message);
    } catch (QueueServiceException e) {
      log.error("Error sending the message", e);
    }
  }

  /**
   * Send a message of credential changes.
   *
   * @param cloudosCredential credential
   */
  public void sendCredentialChangeMessage(CloudosCredential cloudosCredential) {
    try {
      CredentialChangeMessage message = new CredentialChangeMessage();
      message.setCredential(cloudosCredential);
      queueService.sendMessage(message);
    } catch (QueueServiceException e) {
      log.error("Error sending the message", e);
    }
  }

  /**
   * Send a message with datapoints.
   *
   * @param datapoints list
   */
  public void sendDatapointMessage(List<CloudosDatapoint> datapoints) {
    try {
      Map<String, List<CloudosDatapoint>> collect =
          datapoints.stream().collect(Collectors.groupingBy(CloudosDatapoint::getCloudId));
      for (Entry<String, List<CloudosDatapoint>> entry : collect.entrySet()) {
        DatapointMessage message = new DatapointMessage();
        message.setDatapoints(entry.getValue());
        log.info("Sending datapoints message for instance {}", entry.getKey());
        queueService.sendMessage(message);
      }
    } catch (QueueServiceException e) {
      log.error("Error sending the message", e);
    }
  }

  /**
   * Send a message of client install.
   *
   * @param deployment object
   * @param string description
   */
  public void sendClientInstallMessage(Deployment deployment, String string) {
    try {
      ClientInstallMessage message = new ClientInstallMessage();
      message.setDeployment(deployment);
      message.setDescription(string);
      queueService.sendMessage(message);
    } catch (QueueServiceException e) {
      log.error("Error sending the message", e);
    }
  }

  /**
   * Send a message to stop a deployment.
   *
   * @param id of the deployment
   */
  public void sendDeploymentStopMessage(String id) {
    try {
      DeploymentStopMessage message = new DeploymentStopMessage();
      message.setDeploymentId(id);
      message.setDescription(id);
      queueService.sendMessage(message);
    } catch (QueueServiceException e) {
      log.error("Error sending the message", e);
    }
  }

  /**
   * Send a message to say that key used on the instance is not on cloudos.
   *
   * @param keyName - name of the key
   * @param cloudOsInstance - instance related
   */
  public void sendInstanceKeyNotFound(String keyName, Instance cloudOsInstance) {
    try {
      InstanceKeyNotFoundMessage message = new InstanceKeyNotFoundMessage();
      message.setKeyName(keyName);
      message.setCloudOsInstance(cloudOsInstance);
      queueService.sendMessage(message);
    } catch (QueueServiceException e) {
      log.error("Error sending the message", e);
    }
  }

  /**
   * Send a message to add new billing file to be processed.
   *
   * @param billingFile the billing file to be processed
   */
  public void sendBillingFileDownloadedMessage(BillingFile billingFile) {
    try {
      BillingFileDownloadedMessage message = new BillingFileDownloadedMessage();
      message.setBillingFile(billingFile);
      queueService.sendMessage(message);
    } catch (QueueServiceException e) {
      log.error("Error sending the message", e);
    }
  }

  /**
   * Send a message to say that a key /private key was saved.
   *
   * @param key - saved cloudoskey
   */
  public void sendInstanceKeySaved(CloudosKey key) {
    try {
      InstanceKeySavedMessage message = new InstanceKeySavedMessage();
      message.setKey(key);
      queueService.sendMessage(message);
    } catch (QueueServiceException e) {
      log.error("Error sending the message", e);
    }
  }

  /**
   * Send a message to re-install the client.
   *
   * @param instance to be re installed
   */
  public void sendReInstallClient(AbstractInstance instance) {
    try {
      ClientReInstallMessage message = new ClientReInstallMessage();
      message.setInstance(instance);
      queueService.sendMessage(message);
    } catch (QueueServiceException e) {
      log.error("Error sending the message", e);
    }
  }

  /**
   * Send a message when a spot instance is marked to be terminated.
   *
   * @param instance object
   */
  public void sendSpotInstanceTerminatedMessage(Instance instance) {
    try {
      SpotInstanceTerminatedMessage message = new SpotInstanceTerminatedMessage();
      message.setInstance(instance);
      queueService.sendMessage(message);
    } catch (QueueServiceException e) {
      log.error("Error sending the message", e);
    }
  }

  /**
   * Send a message requesting the pricing.
   *
   * @param request param
   * @param autoscaleRequestId for identify the requester
   */
  public void sendSpotPricingPredictRequestMessage(CloudSpotPricingRequest request,
      String autoscaleRequestId) {
    try {
      SpotPricingPredictRequestMessage message = new SpotPricingPredictRequestMessage();
      message.setSpotPricingRequest(request);
      message.setAutoscaleRequestId(autoscaleRequestId);
      queueService.sendMessage(message);
    } catch (QueueServiceException e) {
      log.error("Error sending the message", e);
    }
  }

  /**
   * Send a message of response the pricing.
   *
   * @param response result
   * @param autoscaleRequestId for identify the requester
   */
  public void sendSpotPricingPredictResponseMessage(CloudSpotPricingResponse response,
      String autoscaleRequestId) {
    try {
      SpotPricingPredictResponseMessage message = new SpotPricingPredictResponseMessage();
      message.setSpotPricingResponse(response);
      message.setAutoscaleRequestId(autoscaleRequestId);
      queueService.sendMessage(message);
    } catch (QueueServiceException e) {
      log.error("Error sending the message", e);
    }
  }

  /**
   * Send a message for creating spot instances.
   *
   * @param cloudSpotCreateRequest spot request
   * @param autoscaleRequestId for identify the request
   */
  public void sendSpotCreateRequestMessage(CloudSpotCreateRequest cloudSpotCreateRequest,
      String autoscaleRequestId) {
    try {
      SpotCreateRequestMessage spotCreateMessage = new SpotCreateRequestMessage();
      spotCreateMessage.setCloudSpotCreateRequest(cloudSpotCreateRequest);
      spotCreateMessage.setAutoscaleRequestId(autoscaleRequestId);
      queueService.sendMessage(spotCreateMessage);
    } catch (QueueServiceException e) {
      log.error("Error sending the message", e);
    }
  }

  /**
   * Send a message of response the creation.
   *
   * @param response spotCreateResponse
   * @param autoscaleRequestId for identify the request
   */
  public void sendSpotCreateResponseMessage(CloudSpotCreateResponse response,
      String autoscaleRequestId) {
    try {
      SpotCreateResponseMessage spotCreateMessage = new SpotCreateResponseMessage();
      spotCreateMessage.setCloudSpotCreateResponse(response);
      spotCreateMessage.setAutoscaleRequestId(autoscaleRequestId);
      queueService.sendMessage(spotCreateMessage);
    } catch (QueueServiceException e) {
      log.error("Error sending the message", e);
    }
  }

  /**
   * Send a message about a new alert created.
   *
   * @param alert object
   */
  public void sendAlertCreatedMessage(GenericAlert alert) {
    try {
      AlertCreatedMessage message = new AlertCreatedMessage();
      message.setAlert(alert);
      queueService.sendMessage(message);
    } catch (QueueServiceException e) {
      log.error("Error sending the message", e);
    }
  }

  /**
   * Send a message about a instance been updated.
   *
   * @param alert object
   */
  public void sendAlertUpdatedMessage(GenericAlert alert) {
    try {
      AlertUpdatedMessage message = new AlertUpdatedMessage();

      queueService.sendMessage(message);
    } catch (QueueServiceException e) {
      log.error("Error sending the message", e);
    }
  }

  /**
   * Send a message after a alert been delete.
   *
   * @param alert object
   */
  public void sendAlertDeletedMessage(GenericAlert alert) {
    try {
      AlertDeletedMessage message = new AlertDeletedMessage();
      message.setAlert(alert);
      queueService.sendMessage(message);
    } catch (QueueServiceException e) {
      log.error("Error sending the message", e);
    }
  }
}
