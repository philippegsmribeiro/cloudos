package cloudos.queue.message.beats;

import cloudos.queue.message.AbstractQueueMessage;
import cloudos.queue.message.QueueMessageType;
import lombok.Data;

/**
 * Filebeat message to hold only elasticsearch heartbeat hit information in a JSON format to be
 * consumed by FilebeatMessageConsumer.
 *
 * @author Rogério Souza
 */
@Data
public class FilebeatMessage extends AbstractQueueMessage {

  public interface FilebeatMessageConsumer extends AbstractQueueMessageConsumer<FilebeatMessage> {

    @Override
    default Class<FilebeatMessage> getType() {
      return FilebeatMessage.class;
    }
  }

  private String hitJson;

  public FilebeatMessage() {
    super(QueueMessageType.FILEBEAT);
  }

}
