package cloudos.queue.message.beats;

import cloudos.queue.message.AbstractQueueMessage;
import cloudos.queue.message.QueueMessageType;
import lombok.Data;

/**
 * Metricbeat message to hold only elasticsearch heartbeat hit information in a JSON format to be
 * consumed by MetricbeatMessageConsumer.
 *
 * @author Rogério Souza
 */
@Data
public class MetricbeatMessage extends AbstractQueueMessage {

  public interface MetricbeatMessageConsumer
      extends AbstractQueueMessageConsumer<MetricbeatMessage> {

    @Override
    default Class<MetricbeatMessage> getType() {
      return MetricbeatMessage.class;
    }
  }

  private String hitJson;

  public MetricbeatMessage() {
    super(QueueMessageType.METRICBEAT);
  }

}
