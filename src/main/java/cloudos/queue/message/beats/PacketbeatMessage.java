package cloudos.queue.message.beats;

import cloudos.queue.message.AbstractQueueMessage;
import cloudos.queue.message.QueueMessageType;
import lombok.Data;

/**
 * Packetbeat message to hold only elasticsearch heartbeat hit information in a JSON format to be
 * consumed by PacketbeatMessageConsumer.
 *
 * @author Rogério Souza
 */
@Data
public class PacketbeatMessage extends AbstractQueueMessage {

  public interface PacketbeatMessageConsumer
      extends AbstractQueueMessageConsumer<PacketbeatMessage> {

    @Override
    default Class<PacketbeatMessage> getType() {
      return PacketbeatMessage.class;
    }
  }

  private String hitJson;

  public PacketbeatMessage() {
    super(QueueMessageType.PACKETBEAT);
  }

}
