package cloudos.queue.message.beats;

import cloudos.queue.message.AbstractQueueMessage;
import cloudos.queue.message.QueueMessageType;
import lombok.Data;

/**
 * Heartbeat message to hold only elasticsearch heartbeat hit information in a JSON format to be
 * consumed by HeartbeatMessageConsumer.
 *
 * @author Rogério Souza
 */
@Data
public class HeartbeatMessage extends AbstractQueueMessage {

  public interface HeartbeatMessageConsumer extends AbstractQueueMessageConsumer<HeartbeatMessage> {

    @Override
    default Class<HeartbeatMessage> getType() {
      return HeartbeatMessage.class;
    }
  }

  private String hitJson;

  public HeartbeatMessage() {
    super(QueueMessageType.HEARTBEAT);
  }
}
