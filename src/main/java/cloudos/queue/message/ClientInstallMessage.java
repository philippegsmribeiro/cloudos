package cloudos.queue.message;

import cloudos.deploy.deployments.Deployment;
import lombok.Data;

/**
 * Message for client install status.
 *
 */
@Data
public class ClientInstallMessage extends AbstractQueueMessage {

  public interface ClientInstallMessageConsumer
      extends AbstractQueueMessageConsumer<ClientInstallMessage> {

    @Override
    default Class<ClientInstallMessage> getType() {
      return ClientInstallMessage.class;
    }
  }

  private Deployment deployment;

  public ClientInstallMessage() {
    super(QueueMessageType.CLIENT_INSTALL);
  }

}
