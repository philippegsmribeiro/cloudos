package cloudos.queue.message;

import cloudos.models.Instance;
import lombok.Data;

/**
 * Message to notify when a spot instance will be terminated (preempted).
 *
 */
@Data
public class SpotInstanceTerminatedMessage extends AbstractQueueMessage {

  public interface SpotInstanceTerminatedMessageConsumer
      extends AbstractQueueMessageConsumer<SpotInstanceTerminatedMessage> {

    @Override
    default Class<SpotInstanceTerminatedMessage> getType() {
      return SpotInstanceTerminatedMessage.class;
    }
  }

  private Instance instance;

  public SpotInstanceTerminatedMessage() {
    super(QueueMessageType.SPOT_INSTANCE_TERMINATED);
  }
}
