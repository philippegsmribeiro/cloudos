package cloudos.queue.message.insights;

import cloudos.queue.message.AbstractQueueMessage;
import cloudos.queue.message.QueueMessageType;
import lombok.Data;

@Data
public class MemoryInsightMessage extends AbstractQueueMessage {

  public interface MemoryInsightMessageConsumer
      extends AbstractQueueMessageConsumer<cloudos.queue.message.insights.MemoryInsightMessage> {

    @Override
    default Class<cloudos.queue.message.insights.MemoryInsightMessage> getType() {
      return cloudos.queue.message.insights.MemoryInsightMessage.class;
    }
  }

  private String message;

  public MemoryInsightMessage() {
    super(QueueMessageType.MEMORY_INSIGHT);
  }

}