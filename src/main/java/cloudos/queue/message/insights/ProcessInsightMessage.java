package cloudos.queue.message.insights;

import cloudos.queue.message.AbstractQueueMessage;
import cloudos.queue.message.QueueMessageType;
import lombok.Data;


@Data
public class ProcessInsightMessage extends AbstractQueueMessage {

  public interface ProcessInsightMessageConsumer
      extends AbstractQueueMessageConsumer<cloudos.queue.message.insights.ProcessInsightMessage> {

    @Override
    default Class<cloudos.queue.message.insights.ProcessInsightMessage> getType() {
      return cloudos.queue.message.insights.ProcessInsightMessage.class;
    }
  }

  private String message;

  public ProcessInsightMessage() {
    super(QueueMessageType.PROCESS_INSIGHT);
  }

}
