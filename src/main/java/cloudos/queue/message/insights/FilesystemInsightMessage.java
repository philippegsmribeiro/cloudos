package cloudos.queue.message.insights;

import cloudos.queue.message.AbstractQueueMessage;
import cloudos.queue.message.QueueMessageType;
import lombok.Data;

@Data
public class FilesystemInsightMessage extends AbstractQueueMessage {

  public interface FilesystemInsightMessageConsumer
      extends AbstractQueueMessageConsumer<cloudos.queue.message.insights.FilesystemInsightMessage> {

    @Override
    default Class<cloudos.queue.message.insights.FilesystemInsightMessage> getType() {
      return cloudos.queue.message.insights.FilesystemInsightMessage.class;
    }
  }

  private String message;

  public FilesystemInsightMessage() {
    super(QueueMessageType.FILESYSTEM_INSIGHT);
  }

}