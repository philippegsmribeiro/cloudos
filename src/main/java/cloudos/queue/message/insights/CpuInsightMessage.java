package cloudos.queue.message.insights;

import cloudos.queue.message.AbstractQueueMessage;
import cloudos.queue.message.QueueMessageType;
import lombok.Data;


@Data
public class CpuInsightMessage extends AbstractQueueMessage {

  public interface CpuInsightMessageConsumer
      extends AbstractQueueMessageConsumer<cloudos.queue.message.insights.CpuInsightMessage> {

    @Override
    default Class<cloudos.queue.message.insights.CpuInsightMessage> getType() {
      return cloudos.queue.message.insights.CpuInsightMessage.class;
    }
  }

  private String message;

  public CpuInsightMessage() {
    super(QueueMessageType.CPU_INSIGHT);
  }

}