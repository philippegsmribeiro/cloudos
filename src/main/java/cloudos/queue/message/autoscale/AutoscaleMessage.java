package cloudos.queue.message.autoscale;

import cloudos.models.autoscaler.AutoscaleRequest;
import cloudos.queue.message.AbstractQueueMessage;
import cloudos.queue.message.QueueMessageType;
import lombok.Data;

/**
 * AutoscaleMessage is used to send a Autoscale message to the optimizer.
 */
@Data
public class AutoscaleMessage extends AbstractQueueMessage {

  public interface AutoscaleMessageConsumer extends AbstractQueueMessageConsumer<AutoscaleMessage> {

    @Override
    default Class<AutoscaleMessage> getType() {
      return AutoscaleMessage.class;
    }
  }

  private AutoscaleRequest autoscaleRequest;

  public AutoscaleMessage() {
    super(QueueMessageType.AUTOSCALE);
  }

}
