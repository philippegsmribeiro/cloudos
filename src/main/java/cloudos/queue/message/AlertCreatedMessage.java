package cloudos.queue.message;

import cloudos.models.alerts.GenericAlert;
import lombok.Data;

/**
 * Message for when an alert is created.
 *
 */
@Data
public class AlertCreatedMessage extends AbstractQueueMessage {

  public interface AlertCreatedMessageConsumer
      extends AbstractQueueMessageConsumer<AlertCreatedMessage> {

    @Override
    default Class<AlertCreatedMessage> getType() {
      return AlertCreatedMessage.class;
    }
  }

  private GenericAlert alert;

  public AlertCreatedMessage() {
    super(QueueMessageType.ALERT_CREATED);
  }

}
