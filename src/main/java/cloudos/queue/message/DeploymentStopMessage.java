package cloudos.queue.message;

import lombok.Data;

/**
 * Message for stopping a deployment.
 *
 */
@Data
public class DeploymentStopMessage extends AbstractQueueMessage {

  public interface DeploymentStopMessageConsumer
      extends AbstractQueueMessageConsumer<DeploymentStopMessage> {

    @Override
    default Class<DeploymentStopMessage> getType() {
      return DeploymentStopMessage.class;
    }
  }

  private String deploymentId;

  public DeploymentStopMessage() {
    super(QueueMessageType.DEPLOYMENTS_STOP);
  }

}
