package cloudos.queue.message;

import cloudos.models.Instance;
import lombok.Data;

/**
 * Message for key not found on the mongo for an instance.
 *
 */
@Data
public class InstanceKeyNotFoundMessage extends AbstractQueueMessage {

  public interface InstanceKeyNotFoundConsumer
      extends AbstractQueueMessageConsumer<InstanceKeyNotFoundMessage> {

    @Override
    default Class<InstanceKeyNotFoundMessage> getType() {
      return InstanceKeyNotFoundMessage.class;
    }
  }

  public InstanceKeyNotFoundMessage() {
    super(QueueMessageType.INSTANCE_KEY_NOT_FOUND);
  }

  private String keyName;
  private Instance cloudOsInstance;

}
