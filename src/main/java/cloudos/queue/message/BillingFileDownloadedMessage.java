package cloudos.queue.message;

import cloudos.billings.BillingFile;
import lombok.Data;

/**
 * Downloaded billing files message consumer.
 */
@Data
public class BillingFileDownloadedMessage extends AbstractQueueMessage {

  public interface BillingFileDownloadedMessageConsumer
      extends AbstractQueueMessageConsumer<BillingFileDownloadedMessage> {

    @Override
    default Class<BillingFileDownloadedMessage> getType() {
      return BillingFileDownloadedMessage.class;
    }
  }

  private BillingFile billingFile;

  public BillingFileDownloadedMessage() {
    super(QueueMessageType.BILLING_FILE);
  }

}
