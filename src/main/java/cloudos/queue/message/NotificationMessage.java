package cloudos.queue.message;

import cloudos.notifications.Notification;
import lombok.Data;

/**
 * Message for notifications.
 *
 */
@Data
public class NotificationMessage extends AbstractQueueMessage {

  public interface NotificationMessageConsumer
      extends AbstractQueueMessageConsumer<NotificationMessage> {

    @Override
    default Class<NotificationMessage> getType() {
      return NotificationMessage.class;
    }
  }

  private Notification notification;

  public NotificationMessage() {
    super(QueueMessageType.NOTIFICATION);
  }

}
