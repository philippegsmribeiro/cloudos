package cloudos.queue.message;

import cloudos.models.CloudActionRequest;
import lombok.Data;

/**
 * Message for cloud action requests.
 *
 */
@Data
public class CloudActionRequestMessage extends AbstractQueueMessage {

  public interface CloudActionRequestMessageConsumer
      extends AbstractQueueMessageConsumer<CloudActionRequestMessage> {

    @Override
    default Class<CloudActionRequestMessage> getType() {
      return CloudActionRequestMessage.class;
    }
  }

  private CloudActionRequest request;

  public CloudActionRequestMessage() {
    super(QueueMessageType.CLOUD_ACTION_REQUEST);
  }

}
