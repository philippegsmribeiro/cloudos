package cloudos.queue.message;

import cloudos.models.alerts.GenericAlert;
import lombok.Data;

/**
 * Message for when an alert is updated.
 *
 */
@Data
public class AlertUpdatedMessage extends AbstractQueueMessage {

  public interface AlertUpdatedMessageConsumer
      extends AbstractQueueMessageConsumer<AlertUpdatedMessage> {

    @Override
    default Class<AlertUpdatedMessage> getType() {
      return AlertUpdatedMessage.class;
    }
  }

  private GenericAlert alert;

  public AlertUpdatedMessage() {
    super(QueueMessageType.ALERT_UPDATED);
  }

}
