package cloudos.queue.message;

import java.util.function.Consumer;

import com.amazonaws.services.sqs.model.Message;

import cloudos.config.CloudOSJson;
import cloudos.utils.ReflectionToJson;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 * Abstract queue message which contains the default methods for queue message manipulation.
 *
 */
@CloudOSJson
@Getter
@Setter
public abstract class AbstractQueueMessage {

  public interface AbstractQueueMessageConsumer<T extends AbstractQueueMessage>
      extends Consumer<T> {

    Class<T> getType();

    @SuppressWarnings("unchecked")
    default void acceptAbstractQueueMessage(AbstractQueueMessage t) {
      accept((T) t);
    }
  }

  /** Type of the Message. */
  @Setter(value = AccessLevel.PRIVATE)
  private QueueMessageType type;
  /** Description. */
  private String description;
  /** Message read from the queue. (populated when reading the message) */
  private Message sqsMessage;
  /** User. */
  private String user;

  public AbstractQueueMessage(QueueMessageType type) {
    this.setType(type);
  }

  @Override
  public String toString() {
    return ReflectionToJson.toString(this.getSqsMessage());
  }
}
