package cloudos.queue.message;

public enum QueueMessageType {

  /** for deployments updates. */
  DEPLOYMENTS,
  /** for stop a deployment. */
  DEPLOYMENTS_STOP,
  /** for notifications. */
  NOTIFICATION,
  /** for credential changes. */
  CREDENTIAL_CHANGE,
  /** for instance status updated. */
  INSTANCE_UPDATED,
  /** for instance created. */
  INSTANCE_CREATED,
  /** for instance deleted. */
  INSTANCE_DELETED,
  /** for actions on the cloud. */
  CLOUD_ACTION_REQUEST,
  /** es heartbeat. */
  HEARTBEAT,
  /** es filebeat. */
  FILEBEAT,
  /** es metricbeat. */
  METRICBEAT,
  /** es packetbeat. */
  PACKETBEAT,
  /** for datapoints read. */
  DATAPOINT,
  /** for client installation status. */
  CLIENT_INSTALL,
  /** for client re-installation. */
  CLIENT_REINSTALL,
  /** for key not found. */
  INSTANCE_KEY_NOT_FOUND,
  /** for key saved. */
  INSTANCE_KEY_SAVED,
  /** for billing files. */
  BILLING_FILE,
  /** for spot instances terminated or masked to terminate. */
  SPOT_INSTANCE_TERMINATED,
  /** for the autoscaler notification. */
  AUTOSCALE, 
  /** for the spot requests. */
  SPOT_CANCEL, 
  /** for the spot requests. */
  SPOT_CREATE_REQUEST,
  /** for the spot requests. */
  SPOT_CREATE_RESPONSE,
  /** for the spot requests. */
  SPOT_DESCRIBE, 
  /** for the spot requests. */
  SPOT_PRICING_PREDICT_REQUEST,
  /** for the spot requests. */
  SPOT_PRICING_PREDICT_RESPONSE,
  /** for the spot requests. */
  SPOT_TERMINATE,
  /** for alert is updated. */
  ALERT_UPDATED,
  /** for alert is created. */
  ALERT_CREATED,
  /** for alert is deleted. */
  ALERT_DELETED,
  /** for when there's an process insight information */
  PROCESS_INSIGHT,
  /** for when there's a filesystem insight information */
  FILESYSTEM_INSIGHT,
  /** for when there's a CPU insight information */
  CPU_INSIGHT,
  /** for when there's a memory insight information */
  MEMORY_INSIGHT
}

