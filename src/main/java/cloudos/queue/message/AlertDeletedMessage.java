package cloudos.queue.message;

import cloudos.models.alerts.GenericAlert;
import lombok.Data;

/**
 * Message for when an alert is deleted.
 *
 */
@Data
public class AlertDeletedMessage extends AbstractQueueMessage {

  public interface AlertDeletedMessageConsumer
      extends AbstractQueueMessageConsumer<AlertDeletedMessage> {

    @Override
    default Class<AlertDeletedMessage> getType() {
      return AlertDeletedMessage.class;
    }
  }

  private GenericAlert alert;

  public AlertDeletedMessage() {
    super(QueueMessageType.ALERT_DELETED);
  }

}
