package cloudos.queue.message;

import cloudos.models.AbstractInstance;
import lombok.Data;

/**
 * Message for when an instance is created.
 *
 */
@Data
public class InstanceCreatedMessage extends AbstractQueueMessage {

  public interface InstanceCreatedMessageConsumer
      extends AbstractQueueMessageConsumer<InstanceCreatedMessage> {

    @Override
    default Class<InstanceCreatedMessage> getType() {
      return InstanceCreatedMessage.class;
    }
  }

  private AbstractInstance instance;

  public InstanceCreatedMessage() {
    super(QueueMessageType.INSTANCE_CREATED);
  }

}
