package cloudos.queue.message;

import cloudos.models.AbstractInstance;
import cloudos.models.InstanceStatus;
import lombok.Data;

/**
 * Message for when an instance is updated.
 *
 */
@Data
public class InstanceUpdatedMessage extends AbstractQueueMessage {

  public interface InstanceUpdatedMessageConsumer
      extends AbstractQueueMessageConsumer<InstanceUpdatedMessage> {

    @Override
    default Class<InstanceUpdatedMessage> getType() {
      return InstanceUpdatedMessage.class;
    }
  }

  private InstanceStatus oldInstanceStatus;
  private AbstractInstance newInstance;

  public InstanceUpdatedMessage() {
    super(QueueMessageType.INSTANCE_UPDATED);
  }

}
