package cloudos.queue.message;

import java.util.List;

import cloudos.models.CloudosDatapoint;
import lombok.Data;

/**
 * Message for datapoints reading.
 *
 */
@Data
public class DatapointMessage extends AbstractQueueMessage {

  public interface DatapointMessageConsumer extends AbstractQueueMessageConsumer<DatapointMessage> {

    @Override
    default Class<DatapointMessage> getType() {
      return DatapointMessage.class;
    }
  }

  private List<CloudosDatapoint> datapoints;

  public DatapointMessage() {
    super(QueueMessageType.DATAPOINT);
  }
}
