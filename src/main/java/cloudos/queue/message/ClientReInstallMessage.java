package cloudos.queue.message;

import cloudos.models.AbstractInstance;
import lombok.Data;

/**
 * Message for client re-install status.
 *
 */
@Data
public class ClientReInstallMessage extends AbstractQueueMessage {

  public interface ClientReInstallMessageConsumer
      extends AbstractQueueMessageConsumer<ClientReInstallMessage> {

    @Override
    default Class<ClientReInstallMessage> getType() {
      return ClientReInstallMessage.class;
    }
  }

  private AbstractInstance instance;

  public ClientReInstallMessage() {
    super(QueueMessageType.CLIENT_REINSTALL);
  }

}
