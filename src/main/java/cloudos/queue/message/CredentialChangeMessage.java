package cloudos.queue.message;

import cloudos.models.CloudosCredential;
import lombok.Data;

/**
 * Message for credential changes.
 *
 */
@Data
public class CredentialChangeMessage extends AbstractQueueMessage {

  public interface CredentialChangeMessageConsumer
      extends AbstractQueueMessageConsumer<CredentialChangeMessage> {

    @Override
    default Class<CredentialChangeMessage> getType() {
      return CredentialChangeMessage.class;
    }
  }

  private CloudosCredential credential;

  public CredentialChangeMessage() {
    super(QueueMessageType.NOTIFICATION);
  }

}
