package cloudos.queue.message;

import cloudos.deploy.deployments.Deployment;
import lombok.Data;

/**
 * Message for deployment status.
 *
 */
@Data
public class DeploymentMessage extends AbstractQueueMessage {

  public interface DeploymentMessageConsumer
      extends AbstractQueueMessageConsumer<DeploymentMessage> {

    @Override
    default Class<DeploymentMessage> getType() {
      return DeploymentMessage.class;
    }
  }

  private Deployment deployment;

  public DeploymentMessage() {
    super(QueueMessageType.DEPLOYMENTS);
  }

}
