package cloudos.queue.message.spotbidder;

import cloudos.models.spotinstances.CloudSpotPricingRequest;
import cloudos.queue.message.AbstractQueueMessage;
import cloudos.queue.message.QueueMessageType;
import cloudos.utils.ReflectionToJson;
import lombok.Getter;
import lombok.Setter;

/**
 * Used to send a message with regarding the predicted spot pricing. We define this message in order
 * to create a complete separation between the optimizer and the spot bidder.
 */
@Getter
@Setter
public class SpotPricingPredictRequestMessage extends AbstractQueueMessage {

  /**
   * Define the consumer for the SpotPricingPredictMessage.
   */
  public interface SpotPricingPredictRequestMessageConsumer
      extends AbstractQueueMessageConsumer<SpotPricingPredictRequestMessage> {

    @Override
    default Class<SpotPricingPredictRequestMessage> getType() {
      return SpotPricingPredictRequestMessage.class;
    }
  }

  public SpotPricingPredictRequestMessage() {
    super(QueueMessageType.SPOT_PRICING_PREDICT_REQUEST);
  }

  private CloudSpotPricingRequest spotPricingRequest;
  private String autoscaleRequestId;

  @Override
  public String toString() {
    return ReflectionToJson.toString(this);
  }

}
