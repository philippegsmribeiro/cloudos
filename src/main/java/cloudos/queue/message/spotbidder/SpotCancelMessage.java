package cloudos.queue.message.spotbidder;

import cloudos.models.spotinstances.CloudSpotCancelResponse;
import cloudos.queue.message.AbstractQueueMessage;
import cloudos.queue.message.QueueMessageType;
import cloudos.utils.ReflectionToJson;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SpotCancelMessage extends AbstractQueueMessage {

  /* Define the consumer for the SpotCancelMessage */
  public interface SpotCancelMessageConsumer
      extends AbstractQueueMessageConsumer<SpotCancelMessage> {

    @Override
    default Class<SpotCancelMessage> getType() {
      return SpotCancelMessage.class;
    }
  }

  private CloudSpotCancelResponse cancelResponse;

  public SpotCancelMessage() {
    super(QueueMessageType.SPOT_CANCEL);
  }

  @Override
  public String toString() {
    return ReflectionToJson.toString(this);
  }

}
