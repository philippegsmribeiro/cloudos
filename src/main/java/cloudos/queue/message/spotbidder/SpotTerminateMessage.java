package cloudos.queue.message.spotbidder;

import cloudos.models.spotinstances.CloudSpotTerminateResponse;
import cloudos.queue.message.AbstractQueueMessage;
import cloudos.queue.message.QueueMessageType;
import cloudos.utils.ReflectionToJson;
import lombok.Getter;
import lombok.Setter;

/**
 * The SpotTerminateMessage is used to send a message that the requested spot instances were, in
 * fact, terminated.
 */
@Getter
@Setter
public class SpotTerminateMessage extends AbstractQueueMessage {

  /* Define the consumer for the SpotTerminateMessage. */
  public interface SpotTerminateMessageConsumer
      extends AbstractQueueMessageConsumer<SpotTerminateMessage> {

    @Override
    default Class<SpotTerminateMessage> getType() {
      return SpotTerminateMessage.class;
    }
  }

  public SpotTerminateMessage() {
    super(QueueMessageType.SPOT_TERMINATE);
  }

  private CloudSpotTerminateResponse terminateResponse;

  @Override
  public String toString() {
    return ReflectionToJson.toString(this);
  }

}
