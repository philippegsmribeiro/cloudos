package cloudos.queue.message.spotbidder;

import cloudos.models.spotinstances.CloudSpotPricingResponse;
import cloudos.queue.message.AbstractQueueMessage;
import cloudos.queue.message.QueueMessageType;
import cloudos.utils.ReflectionToJson;
import lombok.Getter;
import lombok.Setter;

/**
 * Used to send a message with regarding the predicted spot pricing. We define this message in order
 * to create a complete separation between the optimizer and the spot bidder.
 */
@Getter
@Setter
public class SpotPricingPredictResponseMessage extends AbstractQueueMessage {

  /**
   * Define the consumer for the SpotPricingPredictMessage.
   */
  public interface SpotPricingPredictResponseMessageConsumer
      extends AbstractQueueMessageConsumer<SpotPricingPredictResponseMessage> {

    @Override
    default Class<SpotPricingPredictResponseMessage> getType() {
      return SpotPricingPredictResponseMessage.class;
    }
  }

  public SpotPricingPredictResponseMessage() {
    super(QueueMessageType.SPOT_PRICING_PREDICT_RESPONSE);
  }

  private CloudSpotPricingResponse spotPricingResponse;
  private String autoscaleRequestId;

  @Override
  public String toString() {
    return ReflectionToJson.toString(this);
  }

}
