package cloudos.queue.message.spotbidder;

import cloudos.models.spotinstances.CloudSpotCreateResponse;
import cloudos.queue.message.AbstractQueueMessage;
import cloudos.queue.message.QueueMessageType;
import cloudos.utils.ReflectionToJson;

import lombok.Getter;
import lombok.Setter;

/**
 * Used to send a message with regarding the creation of spot instances.
 */
@Getter
@Setter
public class SpotCreateResponseMessage extends AbstractQueueMessage {

  /* Define the consumer for the SpotCreateMessage. */
  public interface SpotCreateResponseMessageConsumer
      extends AbstractQueueMessageConsumer<SpotCreateResponseMessage> {

    @Override
    default Class<SpotCreateResponseMessage> getType() {
      return SpotCreateResponseMessage.class;
    }
  }

  public SpotCreateResponseMessage() {
    super(QueueMessageType.SPOT_CREATE_RESPONSE);
  }

  private CloudSpotCreateResponse cloudSpotCreateResponse;
  private String autoscaleRequestId;

  @Override
  public String toString() {
    return ReflectionToJson.toString(this);
  }
}