package cloudos.queue.message.spotbidder;

import cloudos.models.spotinstances.CloudSpotDescribeResponse;
import cloudos.queue.message.AbstractQueueMessage;
import cloudos.queue.message.QueueMessageType;
import cloudos.utils.ReflectionToJson;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SpotDescribeMessage extends AbstractQueueMessage {


  public interface SpotDescribeMessageConsumer
      extends AbstractQueueMessageConsumer<SpotDescribeMessage> {

    @Override
    default Class<SpotDescribeMessage> getType() {
      return SpotDescribeMessage.class;
    }
  }

  public SpotDescribeMessage() {
    super(QueueMessageType.SPOT_DESCRIBE);
  }

  private CloudSpotDescribeResponse describeResponse;

  @Override
  public String toString() {
    return ReflectionToJson.toString(this);
  }

}
