package cloudos.queue.message.spotbidder;

import cloudos.models.spotinstances.CloudSpotCreateRequest;
import cloudos.queue.message.AbstractQueueMessage;
import cloudos.queue.message.QueueMessageType;
import cloudos.utils.ReflectionToJson;
import lombok.Getter;
import lombok.Setter;

/**
 * Used to send a message with regarding the creation of spot instances.
 */
@Getter
@Setter
public class SpotCreateRequestMessage extends AbstractQueueMessage {

  /* Define the consumer for the SpotCreateMessage. */
  public interface SpotCreateRequestMessageConsumer
      extends AbstractQueueMessageConsumer<SpotCreateRequestMessage> {

    @Override
    default Class<SpotCreateRequestMessage> getType() {
      return SpotCreateRequestMessage.class;
    }
  }

  public SpotCreateRequestMessage() {
    super(QueueMessageType.SPOT_CREATE_REQUEST);
  }

  private CloudSpotCreateRequest cloudSpotCreateRequest;
  private String autoscaleRequestId;

  @Override
  public String toString() {
    return ReflectionToJson.toString(this);
  }

}
