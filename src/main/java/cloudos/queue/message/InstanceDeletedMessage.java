package cloudos.queue.message;

import cloudos.models.Instance;
import lombok.Data;

/**
 * Message for when an instance is deleted.
 *
 */
@Data
public class InstanceDeletedMessage extends AbstractQueueMessage {

  public interface InstanceDeletedMessageConsumer
      extends AbstractQueueMessageConsumer<InstanceDeletedMessage> {

    @Override
    default Class<InstanceDeletedMessage> getType() {
      return InstanceDeletedMessage.class;
    }
  }

  private Instance instance;

  public InstanceDeletedMessage() {
    super(QueueMessageType.INSTANCE_DELETED);
  }

}
