package cloudos.queue.message;

import cloudos.keys.cloudoskey.CloudosKey;
import lombok.Data;

/**
 * Message for when a key is saved.
 *
 */
@Data
public class InstanceKeySavedMessage extends AbstractQueueMessage {

  public interface InstanceKeySavedConsumer
      extends AbstractQueueMessageConsumer<InstanceKeySavedMessage> {

    @Override
    default Class<InstanceKeySavedMessage> getType() {
      return InstanceKeySavedMessage.class;
    }
  }

  public InstanceKeySavedMessage() {
    super(QueueMessageType.INSTANCE_KEY_SAVED);
  }

  private CloudosKey key;
}
