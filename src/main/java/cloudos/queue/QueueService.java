package cloudos.queue;

import cloudos.amazon.SQS;
import cloudos.config.ThreadingConfig;
import cloudos.queue.message.AbstractQueueMessage;
import cloudos.queue.message.QueueMessageType;
import cloudos.utils.ProfileUtils;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.model.Message;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import lombok.extern.log4j.Log4j2;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.stereotype.Service;

@Service
@Lazy
@Log4j2
public class QueueService {

  @Value("${cloudos.tenant.name}")
  private String tenant;

  @Value("${cloudos.queue.region}")
  private String region;

  @Value("${cloudos.queue.access_key_id}")
  private String accessKey;

  @Value("${cloudos.queue.secret_key}")
  private String accessSecret;

  @Value("${cloudos.queue.name}")
  private String name;

  @Value("${cloudos.queue.dead_letter}")
  private String deadLetter;

  private SQS sqs;

  private Map<QueueMessageType, String> queueUrlMap;

  @Autowired
  ObjectMapper mapper;

  @Autowired
  ProfileUtils profileUtils;

  @Autowired
  @Qualifier(ThreadingConfig.QUEUE)
  AsyncTaskExecutor taskExecutor;

  /**
   * Execute the queue setup.
   *
   * @throws QueueServiceException if something goes wrong
   */
  @PostConstruct
  private void setup() throws QueueServiceException {
    try {
      log.info("Setting up Queue Service");
      // config access
      BasicAWSCredentials credentials = new BasicAWSCredentials(accessKey, accessSecret);
      AWSStaticCredentialsProvider provider = new AWSStaticCredentialsProvider(credentials);
      sqs = new SQS(region, provider);

      queueUrlMap = new HashMap<>();

      QueueMessageType[] types = QueueMessageType.values();
      for (QueueMessageType queueMessageType : types) {

        if (profileUtils.isTest()) {
          // executing on another thread for cutting time
          taskExecutor.execute(() -> {
            addQueue(queueMessageType);
          });
        } else {
          // add on the same thread on normal deploy
          addQueue(queueMessageType);
        }
      }
    } catch (Exception e) {
      throw new QueueServiceException("Error setting up the sqs queue", e);
    }
  }

  /**
   * Add the queue to the map.
   * @param queueMessageType message type
   */
  private void addQueue(QueueMessageType queueMessageType) {
    String queueName = getQueueName(queueMessageType);
    // config queues
    final List<String> queues = sqs.filterQueues(queueName);
    if (CollectionUtils.isNotEmpty(queues)) {
      // if exists - get the url
      log.info("Queue {} already exists. Using it.", queueName);
      queueUrlMap.put(queueMessageType, queues.get(0));
    } else {
      // if queue doesnt exist
      log.info("Queue {} doesn't exists. Creating it.", queueName);
      // create normal queue
      String queueUrl = sqs.createQueue(queueName);
      // create dead letter queue
      String deadLetterQueueUrl = sqs.createQueue(getDeadLetterQueueName(queueMessageType));
      // config the dead letter queue
      sqs.setDeadLetterQueueForQueue(queueUrl, deadLetterQueueUrl);
      // add to the map
      queueUrlMap.put(queueMessageType, queueUrl);
    }
  }

  /**
   * Read the messages.
   *
   * @return list of Messages
   * @throws QueueServiceException if something goes wrong
   */
  @SuppressWarnings("unchecked")
  public <T extends AbstractQueueMessage> List<T> readMessages(QueueMessageType queueMessageType)
      throws QueueServiceException {
    List<T> result = new ArrayList<>();
    if (isSetup() && queueUrlMap != null) {
      List<Message> readMessages = sqs.readMessages(queueUrlMap.get(queueMessageType), 2, 10);
      for (Message message : readMessages) {
        AbstractQueueMessage broadcastObject = null;
        // convert the string in objects
        try {
          broadcastObject = mapper.readValue(message.getBody(), AbstractQueueMessage.class);
          // set the message
          broadcastObject.setSqsMessage(message);
          log.debug("reading: {} - {}", broadcastObject.getClass(), message.getBody());
          // add to the result list
          result.add((T) broadcastObject);
        } catch (IOException e) {
          log.error("Message type wrong in the queue: {}", e.getMessage(), e);
        }
      }
    }
    return result;
  }

  /**
   * Return if the queues are all setup.
   *
   * @return true if setup, false otherwise.
   */
  private boolean isSetup() {
    return queueUrlMap.keySet().size() == QueueMessageType.values().length;
  }

  /**
   * Send a message.
   *
   * @param message to be sent
   * @return true if message was sent
   * @throws QueueServiceException if something goes wrong
   */
  public boolean sendMessage(AbstractQueueMessage message) throws QueueServiceException {
    try {
      String text = mapper.writeValueAsString(message);
      return sqs.sendMessage(queueUrlMap.get(message.getType()), text, 0);
    } catch (Exception e) {
      throw new QueueServiceException("Error sending a message", e);
    }
  }

  /**
   * Delete a list of read messages.
   *
   * @param messages to be marked as deleted
   * @return true if messages were deleted
   */
  public boolean deleteMessages(List<AbstractQueueMessage> messages) {
    Map<QueueMessageType, List<AbstractQueueMessage>> result =
        messages.stream().collect(Collectors.groupingBy(AbstractQueueMessage::getType));
    for (Entry<QueueMessageType, List<AbstractQueueMessage>> entry : result.entrySet()) {
      sqs.deleteMessageBatch(queueUrlMap.get(entry.getKey()),
          entry.getValue().stream().map(t -> t.getSqsMessage()).collect(Collectors.toList()));
    }
    return true;
  }

  /**
   * Delete a read message.
   *
   * @param message to be marked as deleted
   * @return true if message was deleted
   */
  public boolean deleteMessage(AbstractQueueMessage message) {
    return sqs.deleteMessage(queueUrlMap.get(message.getType()), message.getSqsMessage());
  }

  /**
   * Purge the queue.
   *
   * @param messageType of the queue
   * @return true if queue was purged
   */
  public boolean purgeQueue(QueueMessageType messageType) {
    return sqs.purgeQueue(queueUrlMap.get(messageType));
  }

  /**
   * The queue name.
   *
   * @param messageType of the queue
   * @return name
   */
  private String getQueueName(QueueMessageType messageType) {
    return String.format("%s-%s-%s", tenant, name, messageType);
  }

  /**
   * The dead letter queue name.
   *
   * @param messageType of the queue
   * @return name
   */
  private String getDeadLetterQueueName(QueueMessageType messageType) {
    return String.format("%s-%s-%s", tenant, deadLetter, messageType);
  }

  /**
   * Get the total of messages on the queue.
   * @param queueMessageType message type
   * @return number approximate of messages
   */
  public long countMessages(QueueMessageType queueMessageType) {
    if (isSetup() && queueUrlMap != null) {
      return sqs.countMessages(queueUrlMap.get(queueMessageType));
    }
    return 0;
  }
}
