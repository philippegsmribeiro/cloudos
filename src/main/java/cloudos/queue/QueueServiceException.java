package cloudos.queue;

/** Exception from the queue service. */
public class QueueServiceException extends Exception {

  private static final long serialVersionUID = 1L;

  public QueueServiceException() {}

  public QueueServiceException(String message) {
    super(message);
  }

  public QueueServiceException(String message, Throwable throwable) {
    super(message, throwable);
  }
}
