package cloudos.queue;

import cloudos.queue.message.AbstractQueueMessage;
import cloudos.queue.message.AbstractQueueMessage.AbstractQueueMessageConsumer;
import cloudos.queue.message.QueueMessageType;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * Queue listener for tests. It will not read the messages automatically (like the real one). The
 * test needs to call the read for force it.
 */
@Log4j2
public class QueueListenerForTest implements QueueListener {

  @Autowired
  QueueService queueService;

  /** List of consumers. */
  List<AbstractQueueMessageConsumer<?>> consumerList = new ArrayList<>();

  @Override
  public void addConsumer(AbstractQueueMessageConsumer<?> consumer) {
    log.info("TEST: Adding consumer for {}.", consumer.getType());
    this.consumerList.add(consumer);
  }

  /**
   * Force the message reading.
   *
   * @param type message type
   * @return number of read messages
   */
  public Integer read(QueueMessageType type) {
    List<AbstractQueueMessage> messages;
    try {
      // read messages
      messages = queueService.readMessages(type);
      for (AbstractQueueMessage message : messages) {
        // notify consumers
        consumerList.forEach(c -> {
          // just notify the consumer for the message type
          if (c.getType().equals(message.getClass())) {
            try {
              c.acceptAbstractQueueMessage(message);
            } catch (Exception e) {
              // adding a try catch to show the message consuming errors
              log.error("Error consuming {} message {}", c.getType(), message, e);
            }
          }
        });
      }
      // TODO: delete just the consumed messages
      // delete messages afterwards
      queueService.deleteMessages(messages);
      return messages.size();
    } catch (QueueServiceException e) {
      log.error("Error reading the messages", e);
      return null;
    }
  }

  /**
   * Purge the queue.
   *
   * @param type message type
   */
  public void purge(QueueMessageType type) {
    queueService.purgeQueue(type);
  }

  /**
   * Get the total of messages on the queue.
   * @param type message type
   * @return number approximate of messages
   */
  public long getTotalMessages(QueueMessageType type) {
    return queueService.countMessages(type);
  }
}

