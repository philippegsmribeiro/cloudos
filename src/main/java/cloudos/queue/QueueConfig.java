package cloudos.queue;

import cloudos.config.LoadOnTestCondition;
import cloudos.config.NotLoadOnTestCondition;

import lombok.extern.log4j.Log4j2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

/**
 * Configuration class for the queue service / listener.
 *
 * @author flavio
 */
@Configuration
@Log4j2
public class QueueConfig {

  /**
   * Real queue listener, reading messages from the queue.
   *
   * @return QueueListener
   */
  @Bean
  @Lazy
  @Conditional(NotLoadOnTestCondition.class)
  public QueueListener queueListener() {
    log.debug("###################Starting queue listener!");
    return new QueueListenerImpl();
  }

  /**
   * Fake queue listener, for the tests.
   *
   * @return QueueListener
   */
  @Bean
  @Conditional(LoadOnTestCondition.class)
  public QueueListener queueListenerTest() {
    return new QueueListenerForTest();
  }
}
