package cloudos;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cloudos.security.WebSecurityConfig;

/**
 * Created by philipperibeiro on 5/14/17.
 *
 * <p>Include simple controller that returns the current version
 */
@RestController
@RequestMapping(value = VersionController.VERSION)
public class VersionController {

  public static final String LATEST_VERSION = "0.7.6";
  public static final String VERSION = WebSecurityConfig.API_PATH + "/version";

  /**
   * Return the current CloudOS version.
   *
   * @return the latest version
   */
  @RequestMapping(value = "/", method = RequestMethod.GET)
  public String getVersion() {
    return LATEST_VERSION;
  }
}
