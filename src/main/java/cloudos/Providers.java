package cloudos;

/** Created by philipperibeiro on 12/16/16. */
public enum Providers {
  AMAZON_AWS("Amazon AWS"),
  MICROSOFT_AZURE("Azure"),
  GOOGLE_COMPUTE_ENGINE("Google Cloud"),
  ORACLE_CLOUD("Oracle"),
  IBM_SMART_CLOUD("Smartcloud"),
  ALIBABA_ALIYUN("Alibaba");
  
  private String shortName;
  
  Providers(String shortName) {
    this.shortName = shortName;
  }
  
  public String getShortName() {
    return shortName;
  }
  
  /**
   * Method that return the Provider by shortName.
   * @param name The name of provider.
   * @return Provider associated with the name.
   */
  public static final Providers getProviderByName(String name) {
    for (Providers provider : Providers.values()) {
      if (provider.getShortName().equals(name)) {
        return provider;
      }
    }
    return null;
  }
  
}
