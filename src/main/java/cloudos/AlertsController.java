package cloudos;

import cloudos.alerts.AlertException;
import cloudos.alerts.AlertService;
import cloudos.exceptions.NotImplementedException;
import cloudos.models.CloudosResponse;
import cloudos.models.alerts.AlertRequest;
import cloudos.models.alerts.AlertRequestGeneric;
import cloudos.models.alerts.AlertResponse;
import cloudos.models.alerts.CategoryMetricResponse;
import cloudos.models.alerts.GenericAlert;
import cloudos.models.alerts.MetricMissingDataAlertResponse;
import cloudos.models.alerts.MetricNotificationTopicResponse;
import cloudos.models.alerts.MetricPeriodAlertResponse;
import cloudos.models.alerts.MetricRequest;
import cloudos.models.alerts.MetricResponse;
import cloudos.models.alerts.MetricStatisticRequest;
import cloudos.models.alerts.MetricStatisticResponse;
import cloudos.models.alerts.MetricStatusAlarmResponse;
import cloudos.security.WebSecurityConfig;
import java.util.List;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = AlertsController.ALERTS)
@Log4j
public class AlertsController {

  public static final String ALERTS = WebSecurityConfig.API_PATH + "/alerts";

  @Autowired
  private AlertService alertService;

  /**
   * Retrieve list of the alerts.
   */
  @RequestMapping(method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> listAlerts(AlertRequestGeneric alertRequest) {
    try {
      if (alertRequest == null) {
        return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.PRECONDITION_FAILED)
            .message("Alert request is invalid").build(), HttpStatus.PRECONDITION_FAILED);
      }
      List<? extends GenericAlert> listAlerts = alertService.getListAlert(alertRequest);
      if (listAlerts != null) {
        return new ResponseEntity<>(listAlerts, HttpStatus.OK);
      }
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.INTERNAL_SERVER_ERROR)
          .message("Response invalid").build(), HttpStatus.INTERNAL_SERVER_ERROR);

    } catch (NotImplementedException e) {
      log.error("Error", e);
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.NOT_IMPLEMENTED)
          .message(e.getMessage()).build(), HttpStatus.NOT_IMPLEMENTED);
    } catch (AlertException e) {
      log.error("Error", e);
      return new ResponseEntity<>(
          CloudosResponse.builder().status(HttpStatus.BAD_REQUEST).message(e.getMessage()).build(),
          HttpStatus.BAD_REQUEST);
    }
  }

  /**
   * Create Alert.
   */
  @RequestMapping(method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> createAlert(@RequestBody AlertRequest alertRequest) {
    try {
      if (alertRequest == null) {
        return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.PRECONDITION_FAILED)
            .message("Alert request is invalid").build(), HttpStatus.PRECONDITION_FAILED);
      }
      AlertResponse<?> response = alertService.createAlert(alertRequest);
      if (response != null && !response.getResponse().isEmpty()) {
        return new ResponseEntity<>(response.getAlert(), HttpStatus.OK);
      }
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.INTERNAL_SERVER_ERROR)
          .message("Response invalid").build(), HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (AlertException e) {
      log.error("Error - Validation", e);
      return new ResponseEntity<>(
          CloudosResponse.builder().status(HttpStatus.BAD_REQUEST).message(e.getMessage()).build(),
          HttpStatus.BAD_REQUEST);
    } catch (NotImplementedException e) {
      log.error("Error - Not implemented", e);
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.NOT_IMPLEMENTED)
          .message(e.getMessage()).build(), HttpStatus.NOT_IMPLEMENTED);
    } catch (Exception e) {
      log.error("Error", e);
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.INTERNAL_SERVER_ERROR)
          .message(e.getMessage()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Update Alert.
   */
  @RequestMapping(method = RequestMethod.PUT)
  @ResponseBody
  public ResponseEntity<?> updateAlert(@RequestBody AlertRequest alertRequest) {
    try {
      if (alertRequest == null) {
        return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.PRECONDITION_FAILED)
            .message("Alert request is invalid").build(), HttpStatus.PRECONDITION_FAILED);
      }
      AlertResponse<?> response = alertService.updateAlert(alertRequest);
      if (response != null && !response.getResponse().isEmpty()) {
        return new ResponseEntity<>(response.getAlert(), HttpStatus.OK);
      }
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.INTERNAL_SERVER_ERROR)
          .message("Response invalid").build(), HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (NotImplementedException e) {
      log.error("Error", e);
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.NOT_IMPLEMENTED)
          .message(e.getMessage()).build(), HttpStatus.NOT_IMPLEMENTED);
    } catch (AlertException e) {
      log.error("Error", e);
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.INTERNAL_SERVER_ERROR)
          .message(e.getMessage()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Delete Alert.
   */
  @RequestMapping(method = RequestMethod.DELETE)
  @ResponseBody
  public ResponseEntity<?> deleteAlert(@RequestBody AlertRequestGeneric alertRequest) {
    try {
      if (alertRequest == null) {
        return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.PRECONDITION_FAILED)
            .message("Alert request is invalid").build(), HttpStatus.PRECONDITION_FAILED);
      }
      AlertResponse<?> response = alertService.deleteAlert(alertRequest);
      if (response != null && !response.getResponse().isEmpty()) {
        return new ResponseEntity<>(response, HttpStatus.OK);
      }
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.INTERNAL_SERVER_ERROR)
          .message("Response invalid").build(), HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (NotImplementedException e) {
      log.error("Error", e);
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.NOT_IMPLEMENTED)
          .message(e.getMessage()).build(), HttpStatus.NOT_IMPLEMENTED);
    } catch (AlertException e) {
      log.error("Error", e);
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.INTERNAL_SERVER_ERROR)
          .message(e.getMessage()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Get Alert by Name.
   */
  @RequestMapping(value = "/{name}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getAlertByName(@PathVariable(value = "name") String name,
      AlertRequestGeneric alertRequest) {
    try {
      if (alertRequest == null) {
        return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.PRECONDITION_FAILED)
            .message("Alert request is invalid").build(), HttpStatus.PRECONDITION_FAILED);
      }
      alertRequest.setAlarmName(name);
      AlertResponse<?> response = alertService.getAlertByName(alertRequest);

      if (response != null && !response.getResponse().isEmpty()) {
        return new ResponseEntity<>(response.getAlert(), HttpStatus.OK);
      } else {
        log.info("Alert with name " + name + " not found");
        return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.NOT_FOUND)
            .message("Alert with name " + name + " not found").build(), HttpStatus.NOT_FOUND);
      }
    } catch (NotImplementedException e) {
      log.error("Error", e);
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.NOT_IMPLEMENTED)
          .message(e.getMessage()).build(), HttpStatus.NOT_IMPLEMENTED);
    } catch (AlertException e) {
      log.error("Error", e);
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.INTERNAL_SERVER_ERROR)
          .message(e.getMessage()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Retrieve list of the metrics.
   */
  @RequestMapping(value = "/metrics", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> listMetrics(MetricRequest metricRequest) {
    try {
      if (metricRequest == null) {
        return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.PRECONDITION_FAILED)
            .message("Metric request is invalid").build(), HttpStatus.PRECONDITION_FAILED);
      }
      List<MetricResponse> listMetrics = alertService.getListMetric(metricRequest);
      if (listMetrics != null) {
        return new ResponseEntity<>(listMetrics, HttpStatus.OK);
      }
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.INTERNAL_SERVER_ERROR)
          .message("Response invalid").build(), HttpStatus.INTERNAL_SERVER_ERROR);

    } catch (NotImplementedException e) {
      log.error("Error", e);
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.NOT_IMPLEMENTED)
          .message(e.getMessage()).build(), HttpStatus.NOT_IMPLEMENTED);
    } catch (AlertException e) {
      log.error("Error", e);
      return new ResponseEntity<>(
          CloudosResponse.builder().status(HttpStatus.BAD_REQUEST).message(e.getMessage()).build(),
          HttpStatus.BAD_REQUEST);
    }
  }

  /**
   * Retrieve a list the namespace of the metrics.
   */
  @RequestMapping(value = "/metrics/namespaces", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> listNameSpaceOfMetrics(String region, Providers provider) {
    try {
      MetricRequest request = new MetricRequest();
      request.setRegion(region);
      request.setProvider(provider);
      List<String> listNameSpaceOfMetrics = alertService.getListNameSpaceOfMetrics(request);
      if (listNameSpaceOfMetrics != null) {
        return new ResponseEntity<>(listNameSpaceOfMetrics, HttpStatus.OK);
      }
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.INTERNAL_SERVER_ERROR)
          .message("Response invalid").build(), HttpStatus.INTERNAL_SERVER_ERROR);

    } catch (NotImplementedException e) {
      log.error("Error", e);
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.NOT_IMPLEMENTED)
          .message(e.getMessage()).build(), HttpStatus.NOT_IMPLEMENTED);
    } catch (AlertException e) {
      log.error("Error", e);
      return new ResponseEntity<>(
          CloudosResponse.builder().status(HttpStatus.BAD_REQUEST).message(e.getMessage()).build(),
          HttpStatus.BAD_REQUEST);
    }
  }

  /**
   * Retrieve a category list of metrics.
   */
  @RequestMapping(value = "/metrics/categories", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> listCategoriesOfMetrics(String nameSpace, Providers provider,
      String region) {
    try {
      MetricRequest request = new MetricRequest();
      request.setMetricNamespace(nameSpace);
      request.setProvider(provider);
      request.setRegion(region);
      List<CategoryMetricResponse> listCategoriesOfMetrics =
          alertService.getListCategoriesOfMetrics(request);
      if (listCategoriesOfMetrics != null) {
        return new ResponseEntity<>(listCategoriesOfMetrics, HttpStatus.OK);
      }
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.INTERNAL_SERVER_ERROR)
          .message("Response invalid").build(), HttpStatus.INTERNAL_SERVER_ERROR);

    } catch (NotImplementedException e) {
      log.error("Error", e);
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.NOT_IMPLEMENTED)
          .message(e.getMessage()).build(), HttpStatus.NOT_IMPLEMENTED);
    } catch (AlertException e) {
      log.error("Error", e);
      return new ResponseEntity<>(
          CloudosResponse.builder().status(HttpStatus.BAD_REQUEST).message(e.getMessage()).build(),
          HttpStatus.BAD_REQUEST);
    }
  }

  /**
   * Retrieve list of the alarm status.
   */
  @RequestMapping(value = "/metrics/alarmstatus", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getListMetricStatusAlarm(Providers provider) {
    try {
      List<MetricStatusAlarmResponse> listMetricStatusAlarm =
          alertService.getListMetricStatusAlarm(MetricRequest.builder().provider(provider).build());
      if (listMetricStatusAlarm != null) {
        return new ResponseEntity<>(listMetricStatusAlarm, HttpStatus.OK);
      }
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.INTERNAL_SERVER_ERROR)
          .message("Response invalid").build(), HttpStatus.INTERNAL_SERVER_ERROR);

    } catch (NotImplementedException e) {
      log.error("Error", e);
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.NOT_IMPLEMENTED)
          .message(e.getMessage()).build(), HttpStatus.NOT_IMPLEMENTED);
    } catch (AlertException e) {
      log.error("Error", e);
      return new ResponseEntity<>(
          CloudosResponse.builder().status(HttpStatus.BAD_REQUEST).message(e.getMessage()).build(),
          HttpStatus.BAD_REQUEST);
    }
  }

  /**
   * Retrieve list of the notification topics.
   */
  @RequestMapping(value = "/metrics/notificationtopics", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getListNotificationTopic(Providers provider, String region) {
    try {
      List<MetricNotificationTopicResponse> listNotificationTopics =
          alertService.getListNotificationTopic(
              MetricRequest.builder().provider(provider).region(region).build());
      if (listNotificationTopics != null) {
        return new ResponseEntity<>(listNotificationTopics, HttpStatus.OK);
      }
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.INTERNAL_SERVER_ERROR)
          .message("Response invalid").build(), HttpStatus.INTERNAL_SERVER_ERROR);

    } catch (NotImplementedException e) {
      log.error("Error", e);
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.NOT_IMPLEMENTED)
          .message(e.getMessage()).build(), HttpStatus.NOT_IMPLEMENTED);
    } catch (AlertException e) {
      log.error("Error", e);
      return new ResponseEntity<>(
          CloudosResponse.builder().status(HttpStatus.BAD_REQUEST).message(e.getMessage()).build(),
          HttpStatus.BAD_REQUEST);
    }
  }

  /**
   * Retrieve list of the metric statistic.
   */
  @RequestMapping(value = "/metrics/statistics", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getListMetricStatistic(MetricStatisticRequest metricStatisticRequest) {
    try {
      List<MetricStatisticResponse> listMetricStatistic =
          alertService.getListMetricStatistic(metricStatisticRequest);
      if (listMetricStatistic != null) {
        return new ResponseEntity<>(listMetricStatistic, HttpStatus.OK);
      }
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.INTERNAL_SERVER_ERROR)
          .message("Response invalid").build(), HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (NotImplementedException e) {
      log.error("Error", e);
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.NOT_IMPLEMENTED)
          .message(e.getMessage()).build(), HttpStatus.NOT_IMPLEMENTED);
    } catch (AlertException e) {
      log.error("Error", e);
      return new ResponseEntity<>(
          CloudosResponse.builder().status(HttpStatus.BAD_REQUEST).message(e.getMessage()).build(),
          HttpStatus.BAD_REQUEST);
    }
  }

  /**
   * Retrieve list of the alarm period.
   */
  @RequestMapping(value = "/metrics/periods", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getListMetricPeriodAlert(Providers provider) {
    try {
      List<MetricPeriodAlertResponse> listMetricPeriodAlert =
          alertService.getListMetricPeriodAlert(MetricRequest.builder().provider(provider).build());
      if (listMetricPeriodAlert != null) {
        return new ResponseEntity<>(listMetricPeriodAlert, HttpStatus.OK);
      }
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.INTERNAL_SERVER_ERROR)
          .message("Response invalid").build(), HttpStatus.INTERNAL_SERVER_ERROR);

    } catch (NotImplementedException e) {
      log.error("Error", e);
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.NOT_IMPLEMENTED)
          .message(e.getMessage()).build(), HttpStatus.NOT_IMPLEMENTED);
    } catch (AlertException e) {
      log.error("Error", e);
      return new ResponseEntity<>(
          CloudosResponse.builder().status(HttpStatus.BAD_REQUEST).message(e.getMessage()).build(),
          HttpStatus.BAD_REQUEST);
    }
  }

  /**
   * Retrieve list of the options missing data.
   */
  @RequestMapping(value = "/metrics/missingdatas", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getListMetricMissingDataAlert(Providers provider) {
    try {
      List<MetricMissingDataAlertResponse> listMetricMissingDataAlert = alertService
          .getListMetricMissingDataAlert(MetricRequest.builder().provider(provider).build());
      if (listMetricMissingDataAlert != null) {
        return new ResponseEntity<>(listMetricMissingDataAlert, HttpStatus.OK);
      }
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.INTERNAL_SERVER_ERROR)
          .message("Response invalid").build(), HttpStatus.INTERNAL_SERVER_ERROR);

    } catch (NotImplementedException e) {
      log.error("Error", e);
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.NOT_IMPLEMENTED)
          .message(e.getMessage()).build(), HttpStatus.NOT_IMPLEMENTED);
    } catch (AlertException e) {
      log.error("Error", e);
      return new ResponseEntity<>(
          CloudosResponse.builder().status(HttpStatus.BAD_REQUEST).message(e.getMessage()).build(),
          HttpStatus.BAD_REQUEST);
    }
  }

}
