package cloudos.encryptionKeys;

import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.kms.AWSKMS;
import com.amazonaws.services.kms.model.CreateAliasRequest;
import com.amazonaws.services.kms.model.CreateAliasResult;
import com.amazonaws.services.kms.model.CreateKeyResult;
import com.amazonaws.services.kms.model.DataKeySpec;
import com.amazonaws.services.kms.model.DecryptRequest;
import com.amazonaws.services.kms.model.DescribeKeyRequest;
import com.amazonaws.services.kms.model.DescribeKeyResult;
import com.amazonaws.services.kms.model.GenerateDataKeyWithoutPlaintextRequest;
import com.amazonaws.services.kms.model.NotFoundException;
import com.amazonaws.services.kms.model.ScheduleKeyDeletionRequest;
import com.amazonaws.services.kms.model.ScheduleKeyDeletionResult;

import cloudos.AmazonDataKey;
import cloudos.AmazonDataKeyRepository;

@Service
public class AmazonKMSService {

  private final Logger logger = LoggerFactory.getLogger(AmazonKMSService.class);

  public static final String CLOUDOS_USERS_KEY_ALIAS = "alias/CLOUDOS_USERS_KEY_ALIAS";
  public static final String CLOUDOS_CREDENTIALS_KEY_ALIAS = "alias/CLOUDOS_CREDENTIALS_KEY_ALIAS";
  public static final String KEY_STATE_ENABLED = "Enabled";
  public static final String KEY_STATE_PENDING_DELETION = "PendingDeletion";

  @Autowired
  private AWSKMS kmsClient;

  @Autowired
  private AmazonDataKeyRepository amazonDataKeyRepository;

  private byte[] userDataKey = null;
  private byte[] credentialDataKey = null;

  /**
   * Creates a customer master key (CMK).
   * 
   * @param aliasName - String that contains the display name. The name must start with the word
   *        "alias" followed by a forward slash (alias/). Aliases that begin with "alias/AWS" are
   *        reserved.
   * @return The Amazon Resource Name (ARN) of the customer master key (CMK).
   * @throws AmazonKMSServiceException - when an error occurs or the result is null or empty
   */
  public String createMasterKey(String aliasName) throws AmazonKMSServiceException {
    CreateKeyResult result = kmsClient.createKey();

    if (result == null
        || !isKeyStateEqualsTo(result.getKeyMetadata().getArn(), KEY_STATE_ENABLED)) {
      logger.error("The AWS master key {} has not been created.", aliasName);
      throw new AmazonKMSServiceException(
          String.format("The AWS master key %s has not been created.", aliasName));
    }

    String keyARN = result.getKeyMetadata().getArn();

    CreateAliasRequest request =
        new CreateAliasRequest().withTargetKeyId(keyARN).withAliasName(aliasName);
    CreateAliasResult createAliasResult = kmsClient.createAlias(request);
    if (createAliasResult == null) {
      logger.error("The AWS master key alias {} has not been created.", aliasName);
      throw new AmazonKMSServiceException(
          String.format("The AWS master key alias %s has not been created.", aliasName));
    }

    keyARN = getKeyARNByAliasName(aliasName);

    if (StringUtils.isEmpty(keyARN)) {
      logger.error("The AWS data key alias {} has not been found after creation.", aliasName);
      throw new AmazonKMSServiceException(
          String.format("The AWS data key alias %s has not been found for creation", aliasName));
    }

    return keyARN;
  }

  /**
   * Retrieves a data key (after creating when it doesn't exist) based on a customer master key
   * (CMK) exclusively to be used when encrypting/decrypting sensitive data.
   * 
   * @return The Amazon Resource Name (ARN) of the customer master key (CMK).
   * @throws AmazonKMSServiceException - when an error occurs or the result is null or empty
   */
  private byte[] getAmazonEncryptedDataKey(String alias, byte[] dataKey)
      throws AmazonKMSServiceException {

    // Try retrieving encrypted key from memory
    if (dataKey != null) {
      return dataKey;
    }

    // Try retrieving encrypted key from DB
    AmazonDataKey amazonDataKey = amazonDataKeyRepository.findByAlias(alias);
    if (amazonDataKey != null) {
      return amazonDataKey.getEncryptedKey();
    }

    // Try retrieving encrypted key from Amazon KMS
    String keyARN = getKeyARNByAliasName(alias);

    if (StringUtils.isEmpty(keyARN)) {
      logger.error("The {} data key has not been found, will create a new one.", alias);
      keyARN = createMasterKey(alias);
    }

    // Save the encrypted data key on DB and keep it in memory
    dataKey = generateEncryptedDataKey(keyARN);
    amazonDataKey = AmazonDataKey.builder().alias(alias).encryptedKey(dataKey).build();
    amazonDataKeyRepository.save(amazonDataKey);

    return dataKey;

  }

  /**
   * Retrieves a data key (after creating when it doesn't exist) based on a customer master key
   * (CMK) exclusively to be used when encrypting/decrypting user sensitive data.
   * 
   * @return The Amazon Resource Name (ARN) of the customer master key (CMK).
   * @throws AmazonKMSServiceException - when an error occurs or the result is null or empty
   */
  public byte[] getUserEncryptedDataKey() throws AmazonKMSServiceException {
    userDataKey = getAmazonEncryptedDataKey(CLOUDOS_USERS_KEY_ALIAS, userDataKey);
    return userDataKey;
  }

  /**
   * Retrieves a data key (after creating when it doesn't exist) based on a customer master key
   * (CMK) exclusively to be used when encrypting/decrypting credential sensitive data.
   * 
   * @return The Amazon Resource Name (ARN) of the customer master key (CMK).
   * @throws AmazonKMSServiceException - when an error occurs or the result is null or empty
   */
  public byte[] getCredentialEncryptedDataKey() throws AmazonKMSServiceException {
    credentialDataKey = getAmazonEncryptedDataKey(CLOUDOS_CREDENTIALS_KEY_ALIAS, credentialDataKey);
    return credentialDataKey;
  }

  /**
   * Schedules the deletion of a customer master key (CMK).
   * 
   * @param aliasName - String that contains the display name. The name must start with the word
   *        "alias" followed by a forward slash (alias/). Aliases that begin with "alias/AWS" are
   *        reserved.
   * @return ScheduleKeyDeletionResult - The detailed result object.
   * @throws AmazonKMSServiceException - when an error occurs or the result is null or empty
   */
  public ScheduleKeyDeletionResult scheduleKeyDeletion(String aliasName)
      throws AmazonKMSServiceException {

    if (StringUtils.isEmpty(aliasName)) {
      logger.error("The aliasName must not be null or empty");
      throw new AmazonKMSServiceException("The aliasName must not be null or empty");
    }

    if (aliasName.equals(CLOUDOS_CREDENTIALS_KEY_ALIAS)
        || aliasName.equals(CLOUDOS_USERS_KEY_ALIAS)) {
      logger.error("There was an attempt to delete the key {}. Deleting this key is not allowed.",
          aliasName);
      throw new AmazonKMSServiceException(String.format(
          "There was an attempt to delete the key %s. Deleting this key is not allowed.",
          aliasName));
    }

    String keyARN = getKeyARNByAliasName(aliasName);

    if (StringUtils.isEmpty(keyARN)) {
      logger.error("The AWS data key has not been found for deletion {}", aliasName);
      throw new AmazonKMSServiceException(
          String.format("The AWS data key has not been found for deletion %s", aliasName));
    }

    ScheduleKeyDeletionRequest request =
        new ScheduleKeyDeletionRequest().withKeyId(keyARN).withPendingWindowInDays(7);

    ScheduleKeyDeletionResult result = kmsClient.scheduleKeyDeletion(request);

    if (result == null || !isKeyStateEqualsTo(keyARN, KEY_STATE_PENDING_DELETION)) {
      logger.error("The AWS data key couldn't be scheduled for deletion {}", keyARN);
      throw new AmazonKMSServiceException(
          String.format("The AWS data key couldn't be scheduled for deletion %s", keyARN));
    }

    return result;

  }

  /**
   * Retrieves a key from Amazon by it's ARN and compares its state to a given state.
   * 
   * @param aliasName - String that contains the display name. The name must start with the word
   *        "alias" followed by a forward slash (alias/). Aliases that begin with "alias/AWS" are
   *        reserved.
   * @param keyState - The key state value.
   * @return true when equals, false otherwise
   * @throws AmazonKMSServiceException - when an error occurs or the result is null or empty
   */
  public boolean isKeyStateEqualsTo(String aliasName, String keyState)
      throws AmazonKMSServiceException {

    if (StringUtils.isEmpty(aliasName)) {
      logger.error("The aliasName must not be null or empty");
      throw new AmazonKMSServiceException("The aliasName must not be null or empty");
    }

    DescribeKeyResult result = describeKey(aliasName);

    if (result == null || result.getKeyMetadata() == null) {
      logger.error("The AWS data key has not been found {}", aliasName);
      throw new AmazonKMSServiceException(
          String.format("The AWS data key has not been found %s", aliasName));
    }

    return keyState.equals(result.getKeyMetadata().getKeyState());

  }

  /**
   * Search an existing key by aliasName and retrieves its ARN.
   * 
   * @param aliasName - String that contains the display name. The name must start with the word
   *        "alias" followed by a forward slash (alias/). Aliases that begin with "alias/AWS" are
   *        reserved.
   * @return The Amazon Resource Name (ARN) of the customer master key (CMK).
   * @throws AmazonKMSServiceException - when an error occurs or null when the request was rejected
   *         because the specified entity or resource could not be found.
   */
  private String getKeyARNByAliasName(String aliasName) throws AmazonKMSServiceException {

    try {

      DescribeKeyResult result = describeKey(aliasName);
      return result.getKeyMetadata().getArn();

    } catch (NotFoundException e) {

      // The request was rejected because the specified entity or resource could not be found.
      return null;

    }

  }

  /**
   * Provides detailed information about the specified customer master key.
   * 
   * @param aliasName - String that contains the display name. The name must start with the word
   *        "alias" followed by a forward slash (alias/). Aliases that begin with "alias/AWS" are
   *        reserved.
   * @return DescribeKeyResult - The detailed result object
   * @throws AmazonKMSServiceException - when an error occurs or the result is null or empty
   */
  public DescribeKeyResult describeKey(String aliasName) throws AmazonKMSServiceException {
    if (StringUtils.isEmpty(aliasName)) {
      throw new AmazonKMSServiceException("The aliasName must not be null or empty");
    }

    DescribeKeyRequest request = new DescribeKeyRequest().withKeyId(aliasName);
    DescribeKeyResult result = kmsClient.describeKey(request);

    if (result == null) {
      logger.error("The AWS data key has not been found {}", aliasName);
      throw new AmazonKMSServiceException(
          String.format("The AWS data key has not been found %s", aliasName));
    }

    return result;

  }

  /**
   * Returns a data encryption key encrypted under a customer master key (CMK). This operation is
   * identical to <a>GenerateDataKey</a> but returns only the encrypted copy of the data key.
   * 
   * @param keyARN - The Amazon Resource Name (ARN) of the customer master key (CMK).
   * @return
   */
  public byte[] generateEncryptedDataKey(String keyARN) {
    GenerateDataKeyWithoutPlaintextRequest generateDataKeyRequest =
        new GenerateDataKeyWithoutPlaintextRequest();
    generateDataKeyRequest.setKeyId(keyARN);
    generateDataKeyRequest.setKeySpec(DataKeySpec.AES_128);
    return kmsClient.generateDataKeyWithoutPlaintext(generateDataKeyRequest).getCiphertextBlob()
        .array();
  }

  /**
   * Encrypts a plaintext based on a encrypted customer master key (CMK).
   * 
   * @param encryptedKey - The encrypted CMK.
   * @param message - The plaintext to be encrypted.
   * @return The encrypted cipherText.
   * @throws AmazonKMSServiceException - when an error occurs or the result is null or empty
   */
  public String encryptMessage(final byte[] encryptedKey, final String message)
      throws AmazonKMSServiceException {

    try {
      Cipher cipher = Cipher.getInstance("AES");
      cipher.init(Cipher.ENCRYPT_MODE, decryptKey(encryptedKey));
      byte[] enc = cipher.doFinal(message.getBytes());
      return Base64.getEncoder().encodeToString(enc);
    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
        | IllegalBlockSizeException | BadPaddingException e) {
      logger.error("Error while encrypting message.");
      logger.error(e.getMessage());
      throw new AmazonKMSServiceException(e.getMessage());
    }

  }

  /**
   * Decrypts a customer master key (CMK).
   * 
   * @param encryptedKey - An encrypted Amazon CMK.
   * @return - The decrypted Amazon CMK.
   */
  private SecretKeySpec decryptKey(final byte[] encryptedKey) {
    ByteBuffer encryptedKeyBuffer = ByteBuffer.wrap(encryptedKey);
    DecryptRequest decryptRequest = new DecryptRequest().withCiphertextBlob(encryptedKeyBuffer);
    ByteBuffer plainTextKey = kmsClient.decrypt(decryptRequest).getPlaintext();
    return new SecretKeySpec(plainTextKey.array(), "AES");
  }

  /**
   * Decrypts ciphertext based on a encrypted customer master key (CMK).
   * 
   * @param encryptedKey - An encrypted Amazon CMK.
   * @param cipherText - Ciphertext is plaintext that has been previously encrypted.
   * @return - The decrypted cipherText.
   * @throws AmazonKMSServiceException - when an error occurs or the result is null or empty
   */
  public String decryptMessage(final byte[] encryptedKey, String cipherText)
      throws AmazonKMSServiceException {

    try {
      byte[] decodeBase64src = Base64.getDecoder().decode(cipherText);
      Cipher cipher = Cipher.getInstance("AES");
      cipher.init(Cipher.DECRYPT_MODE, decryptKey(encryptedKey));
      return new String(cipher.doFinal(decodeBase64src));
    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
        | IllegalBlockSizeException | BadPaddingException e) {
      logger.error("Error while decrypting cipherText.");
      logger.error(e.getMessage());
      throw new AmazonKMSServiceException(e.getMessage());
    }

  }

}
