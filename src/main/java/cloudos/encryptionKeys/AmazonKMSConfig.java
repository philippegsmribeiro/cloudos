package cloudos.encryptionKeys;

import java.net.UnknownHostException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.kms.AWSKMS;
import com.amazonaws.services.kms.AWSKMSClientBuilder;

import cloudos.amazon.AWSCredentialSingleton;
import lombok.extern.log4j.Log4j2;

@Configuration
@Log4j2
public class AmazonKMSConfig {

  @Bean(destroyMethod = "shutdown")
  @Lazy
  public AWSKMS kmsClient() throws UnknownHostException {
    return AWSKMSClientBuilder.standard().withCredentials(AWSCredentialSingleton.getProvider())
        .withRegion(Regions.US_WEST_1.getName()).build();
  }

}
