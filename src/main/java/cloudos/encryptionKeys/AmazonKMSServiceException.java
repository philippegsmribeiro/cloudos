package cloudos.encryptionKeys;

/**
 * To be used as the AmazonKMSService exception
 * 
 * @author Rogério Souza
 *
 */
public class AmazonKMSServiceException extends Exception {
  private static final long serialVersionUID = -2828557088217365549L;

  public AmazonKMSServiceException(String message) {
    super(message);
  }

  public AmazonKMSServiceException(String message, Throwable cause) {
    super(message, cause);
  }
}
