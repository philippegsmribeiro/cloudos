package cloudos.securitygroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.nd4j.linalg.io.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.AuthorizeSecurityGroupEgressRequest;
import com.amazonaws.services.ec2.model.AuthorizeSecurityGroupIngressRequest;
import com.amazonaws.services.ec2.model.CreateSecurityGroupRequest;
import com.amazonaws.services.ec2.model.CreateSecurityGroupResult;
import com.amazonaws.services.ec2.model.DeleteSecurityGroupRequest;
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsRequest;
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsResult;
import com.amazonaws.services.ec2.model.IpPermission;
import com.amazonaws.services.ec2.model.IpRange;
import com.amazonaws.services.ec2.model.SecurityGroup;

import cloudos.AmazonService;
import cloudos.Providers;
import cloudos.models.AwsSecurityGroup;
import cloudos.models.CloudosIpPermission;
import cloudos.models.CloudosProtocol;
import cloudos.models.CloudosSecurityGroup;
import cloudos.models.CloudosSecurityGroupRepository;
import lombok.extern.log4j.Log4j2;


@Service
@Log4j2
public class AmazonSecurityGroupManagement implements SecurityGroupManagement<AwsSecurityGroup> {

  private static String SOURCE_ANYWHERE_PATTERN = "0.0.0.0/0";
  private static String SSH_PROTOCOL = "SSH";
  private static String TCP_PROTOCOL = "tcp";
  private static String UDP_PROTOCOL = "udp";
  private static Integer UDP_PROTOCOL_PORT = 53;
  private static Integer SSH_PROTOCOL_PORT = 22;

  @Autowired
  AmazonService amazonService;

  @Autowired
  private CloudosSecurityGroupRepository cloudosSecurityGroupRepository;

  /**
   * Returns the aws client.
   *
   * @param region of client
   * @return AmazonEC2 client
   * @throws CloudSecurityGroupException if something goes wrong creating the client
   */
  private AmazonEC2 getAmazonClient(String region) throws CloudSecurityGroupException {
    AmazonEC2 awsClient = amazonService.getClient(region);
    if (awsClient == null) {
      throw new CloudSecurityGroupException(
          String.format("No Amazon Client obtained by the region informed. Region: %s", region));
    }
    return awsClient;
  }


  /**
   * Create a new AmazonSecurityGroupManagement in AWS, given the group name and the description.
   *
   * @param groupName the name of the security group.
   * @param description the description of the security group.
   * @return the newly created security group
   * @throws CloudSecurityGroupException whenever there's an error
   */
  public String createSecurityGroup(String region, String groupName, String description)
      throws CloudSecurityGroupException {
    try {
      assert this.getAmazonClient(region) != null;

      CreateSecurityGroupRequest csgr =
          new CreateSecurityGroupRequest().withGroupName(groupName).withDescription(description);

      CreateSecurityGroupResult result = this.getAmazonClient(region).createSecurityGroup(csgr);

      return result.getGroupId();
    } catch (AmazonServiceException e) {
      // Output the error to the logger
      log.error("Error when calling CreateSecurityGroupResult with groupname {}", groupName);
      log.error("Caught exception: {}", e.getMessage());
      log.error("Response Status Code: {}", e.getStatusCode());
      log.error("Error Code: {}", e.getErrorCode());
      log.error("Request ID: {}", e.getRequestId());
      throw new CloudSecurityGroupException(e.getMessage(), e);
    }
  }

  /**
   * List all the security groups within the region defined by the client.
   *
   * @return a list of Amazon AWS Security Groups
   * @throws CloudSecurityGroupException whenever there's an error
   */
  public List<com.amazonaws.services.ec2.model.SecurityGroup> findAllSecurityGroups(String region)
      throws CloudSecurityGroupException {

    DescribeSecurityGroupsRequest securityRequest = new DescribeSecurityGroupsRequest();
    DescribeSecurityGroupsResult securityDescription =
        this.getAmazonClient(region).describeSecurityGroups(securityRequest);
    return securityDescription.getSecurityGroups();
  }

  /**
   * Attempt to find a single security group, based on its name.
   *
   * @param region for the security
   * @param securityGroupName the name of the security group
   * @return a single security group object
   * @throws CloudSecurityGroupException whenever there's an error
   */
  public com.amazonaws.services.ec2.model.SecurityGroup describe(String region,
      String securityGroupName) throws CloudSecurityGroupException {
    DescribeSecurityGroupsRequest securityRequest =
        new DescribeSecurityGroupsRequest().withGroupNames(Arrays.asList(securityGroupName));
    DescribeSecurityGroupsResult securityDescription =
        this.getAmazonClient(region).describeSecurityGroups(securityRequest);

    if (securityDescription.getSecurityGroups().size() > 0) {
      return securityDescription.getSecurityGroups().get(0);
    }
    return null;
  }

  /**
   * Attempt to find a single security group, based on groupId.
   *
   * @param region for the security
   * @param groupId the id of the security group
   * @return a single security group object
   * @throws CloudSecurityGroupException whenever there's an error
   */
  public com.amazonaws.services.ec2.model.SecurityGroup describeByGroupId(String region,
      String groupId) throws CloudSecurityGroupException {
    DescribeSecurityGroupsRequest securityRequest =
        new DescribeSecurityGroupsRequest().withGroupIds(groupId);
    DescribeSecurityGroupsResult securityDescription =
        this.getAmazonClient(region).describeSecurityGroups(securityRequest);

    if (securityDescription.getSecurityGroups().size() > 0) {
      return securityDescription.getSecurityGroups().get(0);
    }
    return null;
  }

  /**
   * Create an ingress security group in AWS for out-bound connections. This group contains the IP
   * permission from start to end.
   *
   * @param protocol the type of the protocol being allowed (e.g 'HTTP')
   * @param fromPort the start port number
   * @param toPort the end port number
   * @param ranges the ip Range
   * @throws CloudSecurityGroupException whenever there's an error
   */
  @SuppressWarnings("Duplicates")
  public void configureGroupEngress(String region, String groupId, String protocol,
      Integer fromPort, Integer toPort, IpRange... ranges) throws CloudSecurityGroupException {

    try {
      IpPermission permission = new IpPermission();
      permission.withFromPort(fromPort).withToPort(toPort).withIpProtocol(protocol)
          .withIpv4Ranges(ranges);

      AuthorizeSecurityGroupEgressRequest authorize = new AuthorizeSecurityGroupEgressRequest();
      authorize.withGroupId(groupId).withIpPermissions(permission);

      this.getAmazonClient(region).authorizeSecurityGroupEgress(authorize);
    } catch (AmazonServiceException e) {
      // Output the error to the logger
      log.error("Error when calling AuthorizeSecurityGroupEgressRequest for protocol {}", protocol);
      log.error("Caught exception: {}", e.getMessage());
      log.error("Response Status Code: {}", e.getStatusCode());
      log.error("Error Code: {}", e.getErrorCode());
      log.error("Request ID: {}", e.getRequestId());
    }
  }

  /**
   * Create an ingress security group in AWS for in-bound connections. This group contains the IP
   * permission from start to end.
   *
   * @param protocol the type of the protocol being allowed (e.g 'HTTP')
   * @param fromPort the start port number
   * @param toPort the end port number
   * @param ranges the ip Range
   * @throws CloudSecurityGroupException whenever there's an error
   */
  public void configureGroupIngress(String region, String groupId, String protocol,
      Integer fromPort, Integer toPort, IpRange... ranges) throws CloudSecurityGroupException {

    try {
      IpPermission permission = new IpPermission();
      permission.withFromPort(fromPort).withToPort(toPort).withIpProtocol(protocol)
          .withIpv4Ranges(ranges);

      AuthorizeSecurityGroupIngressRequest authorize = new AuthorizeSecurityGroupIngressRequest();
      authorize.withGroupId(groupId).withIpPermissions(permission);

      this.getAmazonClient(region).authorizeSecurityGroupIngress(authorize);
    } catch (AmazonServiceException e) {
      // Output the error to the logger
      log.error("Error when calling AuthorizeSecurityGroupIngressRequest for protocol {}",
          protocol);
      log.error("Caught exception: {}", e.getMessage());
      log.error("Response Status Code: {}", e.getStatusCode());
      log.error("Error Code: {}", e.getErrorCode());
      log.error("Request ID: {}", e.getRequestId());
    }
  }

  /**
   * Create an egress security group in AWS for in-bound connections. This group contains the IP
   * permission from start to end.
   *
   * @param protocol the type of the protocol being allowed (e.g 'HTTP')
   * @param fromPort the start port number
   * @param toPort the end port number
   * @param ranges the ip Range
   * @throws CloudSecurityGroupException whenever there's an error
   */
  @SuppressWarnings("Duplicates")
  public void configureGroupEgress(String region, String groupId, String protocol, Integer fromPort,
      Integer toPort, IpRange... ranges) throws CloudSecurityGroupException {

    try {
      IpPermission permission = new IpPermission();
      permission.withFromPort(fromPort).withToPort(toPort).withIpProtocol(protocol)
          .withIpv4Ranges(ranges);

      AuthorizeSecurityGroupEgressRequest authorize = new AuthorizeSecurityGroupEgressRequest();
      authorize.withGroupId(groupId).withIpPermissions(permission);

      this.getAmazonClient(region).authorizeSecurityGroupEgress(authorize);
    } catch (AmazonServiceException e) {
      // Output the error to the logger
      log.error("Error when calling AuthorizeSecurityGroupEgressRequest for protocol {}", protocol);
      log.error("Caught exception: {}", e.getMessage());
      log.error("Response Status Code: {}", e.getStatusCode());
      log.error("Error Code: {}", e.getErrorCode());
      log.error("Request ID: {}", e.getRequestId());
    }
  }

  /**
   * Configure the security group given its name and a list of IpPermissions.
   *
   * @param groupName the name of the security group
   * @param permissions the list of ip ingress rules to be allowed
   * @throws CloudSecurityGroupException whenever there's an error
   */
  public void configureGroupIngress(String region, String groupName, List<IpPermission> permissions,
      IpRange... ranges) throws CloudSecurityGroupException {

    try {

      AuthorizeSecurityGroupIngressRequest authorize = new AuthorizeSecurityGroupIngressRequest();
      authorize.withGroupName(groupName).withIpPermissions(permissions);

      this.getAmazonClient(region).authorizeSecurityGroupIngress(authorize);
    } catch (AmazonServiceException e) {
      // Output the error to the logger
      log.error("Error when calling creating security group");
      log.error("Caught exception: {}", e.getMessage());
      log.error("Response Status Code: {}", e.getStatusCode());
      log.error("Error Code: {}", e.getErrorCode());
      log.error("Request ID: {}", e.getRequestId());
    }
  }

  /**
   * Delete a security group, given its groupId.
   *
   * @param groupId the Security group groupId
   * @throws CloudSecurityGroupException whenever there's an error
   */
  public void deleteSecurityGroup(String region, String groupId)
      throws CloudSecurityGroupException {
    DeleteSecurityGroupRequest request = new DeleteSecurityGroupRequest().withGroupId(groupId);

    this.getAmazonClient(region).deleteSecurityGroup(request);

    log.info("Successfully deleted security group with id {}", groupId);
  }

  /**
   * Create a new security group.
   *
   * @param awsSecurityGroup the security group model
   * @return a newly created security group or null
   * @throws CloudSecurityGroupException if failed to create the security group
   */
  @Override
  public AwsSecurityGroup create(AwsSecurityGroup awsSecurityGroup)
      throws CloudSecurityGroupException {

    if (StringUtils.isBlank(awsSecurityGroup.getName())) {
      throw new CloudSecurityGroupException("The group name need   to be informed");
    }

    if (StringUtils.isBlank(awsSecurityGroup.getDescription())) {
      throw new CloudSecurityGroupException("The description need to be informed");
    }

    String awsSecurityGroupId = this.createSecurityGroup(awsSecurityGroup.getRegion(),
        awsSecurityGroup.getName(), awsSecurityGroup.getDescription());

    if (awsSecurityGroupId == null) {
      throw new CloudSecurityGroupException("There is a error in the AWS security group.");
    }

    SecurityGroup describeByGroupId =
        this.describeByGroupId(awsSecurityGroup.getRegion(), awsSecurityGroupId);

    awsSecurityGroup.setGroupId(awsSecurityGroupId);
    awsSecurityGroup.setCreation(new Date());
    awsSecurityGroup.setVpcId(describeByGroupId.getVpcId());

    configureSecurityGroupRules(awsSecurityGroupId, awsSecurityGroup);

    return this.cloudosSecurityGroupRepository.save(awsSecurityGroup);
  }

  /**
   * Search all AWS security groups and import to MongoDB objects if it doesn't exist. Also
   * synchronizes all inbound and outbound rules from AWS keeping CloudosSecurityGroups up to date.
   * <p>
   * Delete non existing records on AWS and found in MongoDB.
   * </p>
   *
   * @throws CloudSecurityGroupException whenever there's an error
   */
  public void synchronizeSecurityGroups(String region) throws CloudSecurityGroupException {

    log.info("STARTING SecurityGroup synchronization for region {}", region);

    List<com.amazonaws.services.ec2.model.SecurityGroup> awsSecutiryGroups =
        findAllSecurityGroups(region);

    for (com.amazonaws.services.ec2.model.SecurityGroup awsSecurityGroup : awsSecutiryGroups) {

      CloudosSecurityGroup cloudosSecurityGroup = null;

      // get the db ones
      List<CloudosSecurityGroup> cloudosSecurityGroups =
          cloudosSecurityGroupRepository.findByNameAndProviderAndRegion(
              awsSecurityGroup.getGroupName(), Providers.AMAZON_AWS, region);

      if (!CollectionUtils.isEmpty(cloudosSecurityGroups)) {
        // if there is db ones - try to get the right one
        for (CloudosSecurityGroup dbSecurityGroup : cloudosSecurityGroups) {
          if (awsSecurityGroup.getVpcId().equals(((AwsSecurityGroup) dbSecurityGroup).getVpcId())) {
            cloudosSecurityGroup = dbSecurityGroup;
            break;
          }
        }
      }

      if (cloudosSecurityGroup == null) {
        // if not exist, add it
        cloudosSecurityGroup = buildSecurityGroupFromAws(region, awsSecurityGroup);
        cloudosSecurityGroup.setCreation(new Date());
      }

      synchronizeInboundRules(awsSecurityGroup, cloudosSecurityGroup);
      synchronizeOutboundRules(awsSecurityGroup, cloudosSecurityGroup);

      cloudosSecurityGroupRepository.save(cloudosSecurityGroup);

    }

    deleteNonExistingLocalSecurityGroups(awsSecutiryGroups);

    log.info("FINISHING SecurityGroup synchronization");

  }

  /**
   * Deletes a security group if there is any local mongoDB security group that has been delete on
   * AWS environment.
   *
   * @param awsSecurityGroups a list of security groups
   */
  private void deleteNonExistingLocalSecurityGroups(
      List<com.amazonaws.services.ec2.model.SecurityGroup> awsSecurityGroups) {
    List<CloudosSecurityGroup> cloudosSecurityGroups =
        cloudosSecurityGroupRepository.findByProvider(Providers.AMAZON_AWS);
    for (CloudosSecurityGroup localCloudosSecurityGroup : cloudosSecurityGroups) {
      boolean shouldDeleteLocalGroup = true;
      for (com.amazonaws.services.ec2.model.SecurityGroup awsSecurityGroup : awsSecurityGroups) {
        if (awsSecurityGroup.getGroupName().equals(localCloudosSecurityGroup.getName())) {
          shouldDeleteLocalGroup = false;
          break;
        }
      }

      if (shouldDeleteLocalGroup) {
        cloudosSecurityGroupRepository.delete(localCloudosSecurityGroup);
      }
    }
  }

  /**
   * Builds a CloudosSecurityGroup object from AWS
   * SecurityGroup(com.amazonaws.services.ec2.model.SecurityGroup).
   *
   * @param awsSecurityGroup securityGroup
   * @return CloudosSecurityGroup
   */
  private CloudosSecurityGroup buildSecurityGroupFromAws(String region,
      com.amazonaws.services.ec2.model.SecurityGroup awsSecurityGroup) {
    AwsSecurityGroup cloudosSecurityGroup = new AwsSecurityGroup();
    cloudosSecurityGroup.setProvider(Providers.AMAZON_AWS);
    cloudosSecurityGroup.setName(awsSecurityGroup.getGroupName());
    cloudosSecurityGroup.setDescription(awsSecurityGroup.getDescription());
    cloudosSecurityGroup.setGroupId(awsSecurityGroup.getGroupId());
    cloudosSecurityGroup.setVpcId(awsSecurityGroup.getVpcId());
    cloudosSecurityGroup.setRegion(region);
    return cloudosSecurityGroup;
  }

  /**
   * Iterate all AWS IP permissions and add them all to cloudosSecurityGroup inbounds list.
   *
   * @param awsSecurityGroup the AWS security group to be sync-ed
   * @param cloudosSecurityGroup the cloudos security group
   */
  private void synchronizeInboundRules(
      com.amazonaws.services.ec2.model.SecurityGroup awsSecurityGroup,
      CloudosSecurityGroup cloudosSecurityGroup) {

    cloudosSecurityGroup.setInbounds(new ArrayList<>());

    for (IpPermission awsIpPermission : awsSecurityGroup.getIpPermissions()) {
      if (!"-1".equals(awsIpPermission.getIpProtocol())) {
        CloudosIpPermission cloudosIpPermission = buildIpPermissionFromAws(awsIpPermission);
        cloudosSecurityGroup.getInbounds().add(cloudosIpPermission);
      }
    }

  }

  /**
   * Iterate all AWS IP permissions egress and add them all to cloudosSecurityGroup outbounds list.
   *
   * @param awsSecurityGroup the AWS security group to be sync-ed
   * @param cloudosSecurityGroup the cloudos security group
   */
  private void synchronizeOutboundRules(
      com.amazonaws.services.ec2.model.SecurityGroup awsSecurityGroup,
      CloudosSecurityGroup cloudosSecurityGroup) {

    cloudosSecurityGroup.setOutbounds(new ArrayList<>());

    for (IpPermission awsIpPermission : awsSecurityGroup.getIpPermissionsEgress()) {
      if (!"-1".equals(awsIpPermission.getIpProtocol())) {
        CloudosIpPermission cloudosIpPermission = buildIpPermissionFromAws(awsIpPermission);
        cloudosSecurityGroup.getOutbounds().add(cloudosIpPermission);
      }
    }
  }

  /**
   * Builds a CloudosIpPermission object from AWS IpPermission.
   *
   * @param awsIpPermission a IP permission range
   * @return a new Cloudos IP permission
   */
  private CloudosIpPermission buildIpPermissionFromAws(IpPermission awsIpPermission) {

    CloudosIpPermission cloudosIpPermission = new CloudosIpPermission();
    CloudosProtocol protocol = new CloudosProtocol();

    protocol.setName(awsIpPermission.getIpProtocol());
    cloudosIpPermission.setProtocol(protocol);

    cloudosIpPermission.setPortFrom(awsIpPermission.getFromPort());
    cloudosIpPermission.setPortTo(awsIpPermission.getToPort());

    if (!CollectionUtils.isEmpty(awsIpPermission.getIpv4Ranges())) {
      cloudosIpPermission.setSource(awsIpPermission.getIpv4Ranges().get(0).getCidrIp());
    } else if (!CollectionUtils.isEmpty(awsIpPermission.getIpv6Ranges())) {
      cloudosIpPermission.setSource(awsIpPermission.getIpv6Ranges().get(0).getCidrIpv6());
    } else {
      log.debug(
          "Error building IP PERMISSION from AWS - no source found on Ipv4Ranges nor Ipv6Ranges {}",
          awsIpPermission);
    }

    return cloudosIpPermission;
  }

  /**
   * Create an ingress security group in AWS for in-bound connections. This group contains the IP
   * permission from start to end.
   *
   * @param awsSecurityGroupId Id the type security groupd id created on AWS
   * @param cloudosSecurityGroup the security group model
   * @throws CloudSecurityGroupException whenever failed to configure the security group
   */
  private void configureSecurityGroupRules(String awsSecurityGroupId,
      CloudosSecurityGroup cloudosSecurityGroup) throws CloudSecurityGroupException {

    if (!existSshIngressRuleBySourcePattern(cloudosSecurityGroup, SOURCE_ANYWHERE_PATTERN)) {
      configureSshIngressRule(cloudosSecurityGroup, SOURCE_ANYWHERE_PATTERN);
    }

    for (CloudosIpPermission ipPermission : cloudosSecurityGroup.getInbounds()) {

      IpRange ipRange = new IpRange().withCidrIp(ipPermission.getSource());

      String protocolType = TCP_PROTOCOL;
      if (UDP_PROTOCOL_PORT.equals(ipPermission.getProtocol().getPort())) {
        protocolType = UDP_PROTOCOL;
      }

      this.configureGroupIngress(cloudosSecurityGroup.getRegion(), awsSecurityGroupId, protocolType,
          ipPermission.getPortFrom(), ipPermission.getPortTo(), ipRange);

    }

    for (CloudosIpPermission ipPermission : cloudosSecurityGroup.getOutbounds()) {

      IpRange ipRange = new IpRange().withCidrIp(ipPermission.getSource());

      String protocolType = TCP_PROTOCOL;
      if (UDP_PROTOCOL_PORT.equals(ipPermission.getProtocol().getPort())) {
        protocolType = UDP_PROTOCOL;
      }

      this.configureGroupEgress(cloudosSecurityGroup.getRegion(), awsSecurityGroupId, protocolType,
          ipPermission.getPortFrom(), ipPermission.getPortTo(), ipRange);

    }

  }

  /**
   * Add a new IpPermission as IngressRule with TCP port 22 (SSH).
   *
   * @param cloudosSecurityGroup the cloudos security group
   * @param source - Define the source pattern to configure such as 0.0.0.0/0, ::/0
   */
  private void configureSshIngressRule(CloudosSecurityGroup cloudosSecurityGroup, String source) {

    CloudosIpPermission cloudosIpPermission = new CloudosIpPermission();
    cloudosIpPermission.setSource(source);

    CloudosProtocol protocol = new CloudosProtocol();
    protocol.setName(SSH_PROTOCOL);
    protocol.setPort(SSH_PROTOCOL_PORT);
    cloudosIpPermission.setProtocol(protocol);

    cloudosIpPermission.setPortFrom(SSH_PROTOCOL_PORT);
    cloudosIpPermission.setPortTo(SSH_PROTOCOL_PORT);

    cloudosSecurityGroup.getInbounds().add(cloudosIpPermission);

  }

  /**
   * Return true if it finds any IpPermission as IngressRule with TCP port 22 (SSH) configured by
   * source.
   *
   * @param cloudosSecurityGroup the cloudos security group
   * @param source - Define the source pattern to be searched such as 0.0.0.0/0, ::/0
   */
  private boolean existSshIngressRuleBySourcePattern(CloudosSecurityGroup cloudosSecurityGroup,
      String source) {

    for (CloudosIpPermission cloudosIpPermission : cloudosSecurityGroup.getInbounds()) {
      if (cloudosIpPermission.getProtocol().getName().equals(SSH_PROTOCOL)
          && cloudosIpPermission.getSource().equals(source)
          && cloudosIpPermission.getPortFrom() <= SSH_PROTOCOL_PORT
          && cloudosIpPermission.getPortTo() >= SSH_PROTOCOL_PORT) {
        return true;
      }

    }

    return false;

  }

  /**
   * Delete a security group if it exists.
   *
   * @param awsSecurityGroup the security group to be deleted
   * @throws CloudSecurityGroupException if failed to execute the delete operation
   */
  @Override
  public void delete(AwsSecurityGroup awsSecurityGroup) throws CloudSecurityGroupException {

    if (!awsSecurityGroup.getProvider().equals(Providers.AMAZON_AWS)) {
      throw new CloudSecurityGroupException("The provider need to be Amazon");
    }

    if (StringUtils.isBlank(awsSecurityGroup.getGroupId())) {
      throw new CloudSecurityGroupException(
          "To delete amazon security group, need to inform the group Id field.");
    }

    this.deleteSecurityGroup(awsSecurityGroup.getRegion(), awsSecurityGroup.getGroupId());

    cloudosSecurityGroupRepository.delete(awsSecurityGroup);

  }

  @Override
  public void verifySshAccessForSecurityGroup(String region, String securityGroupName)
      throws CloudSecurityGroupException {
    SecurityGroup awsSecurityGroup = this.describe(region, securityGroupName);
    CloudosSecurityGroup cloudosSecurityGroup = buildSecurityGroupFromAws(region, awsSecurityGroup);
    synchronizeInboundRules(awsSecurityGroup, cloudosSecurityGroup);
    if (!existSshIngressRuleBySourcePattern(cloudosSecurityGroup, SOURCE_ANYWHERE_PATTERN)) {
      configureGroupIngress(region, awsSecurityGroup.getGroupId(), TCP_PROTOCOL, SSH_PROTOCOL_PORT,
          SSH_PROTOCOL_PORT, new IpRange().withCidrIp(SOURCE_ANYWHERE_PATTERN));
    }
  }
}
