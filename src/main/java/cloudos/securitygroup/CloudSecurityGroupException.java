package cloudos.securitygroup;

/** Created by gleimar on 21/05/2017. */
public class CloudSecurityGroupException extends Exception {
  private static final long serialVersionUID = -2828557088217365549L;

  public CloudSecurityGroupException(String message) {
    super(message);
  }

  public CloudSecurityGroupException(String message, Throwable cause) {
    super(message, cause);
  }
}
