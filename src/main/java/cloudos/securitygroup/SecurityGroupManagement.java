package cloudos.securitygroup;

import cloudos.models.CloudosSecurityGroup;

/** Created by gleimar on 23/04/2017. */
public interface SecurityGroupManagement<T extends CloudosSecurityGroup> {

  T create(T cloudosSecurityGroup) throws CloudSecurityGroupException;

  void delete(T cloudosSecurityGroup) throws CloudSecurityGroupException;

  void verifySshAccessForSecurityGroup(String region, String securityGroupName)
      throws CloudSecurityGroupException;
}
