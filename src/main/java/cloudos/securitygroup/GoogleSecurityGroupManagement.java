package cloudos.securitygroup;

import cloudos.models.GoogleSecurityGroup;

import org.springframework.stereotype.Service;

@Service
public class GoogleSecurityGroupManagement implements SecurityGroupManagement<GoogleSecurityGroup> {

  @Override
  public GoogleSecurityGroup create(GoogleSecurityGroup cloudosSecurityGroup) {
    return null;
  }

  @Override
  public void delete(GoogleSecurityGroup cloudosSecurityGroup) {

  }

  @Override
  public void verifySshAccessForSecurityGroup(String region, String securityGroupName) {
    // TODO Auto-generated method stub
  }
}
