package cloudos.resources;

import cloudos.data.InstanceData;
import cloudos.data.InstanceDataMetric;
import cloudos.models.AbstractInstance;
import cloudos.models.CloudosDatapoint;
import cloudos.models.Instance;
import java.util.List;
import javax.validation.constraints.NotNull;

public interface ResourcesManagement {

  /**
   * Find the datapoints for a resource.
   *
   * @param resource instance
   * @param instanceData data
   * @return List of CloudosDatapoint
   */
  List<CloudosDatapoint> findDatapoints(
      @NotNull Instance resource, @NotNull InstanceData instanceData);

  /**
   * Find a specific instance.
   *
   * @param instance specific
   * @return AbstractInstance
   */
  AbstractInstance findByInstance(Instance instance);

  /**
   * Find and save the datapoints for all instances.
   *
   * @param timeWindow window
   * @param period frame
   * @param dataMetrics metrics
   * @return List of CloudosDatapoint
   */
  List<CloudosDatapoint> findAllDatapoints(
      long timeWindow, int period, InstanceDataMetric... dataMetrics);
}
