package cloudos.resources;

import cloudos.Providers;
import cloudos.data.InstanceData;
import cloudos.data.InstanceDataMetric;
import cloudos.google.GoogleInstanceRepository;
import cloudos.google.integration.GoogleMetricsIntegration;
import cloudos.instances.InstanceService;
import cloudos.models.AbstractInstance;
import cloudos.models.CloudosDatapoint;
import cloudos.models.Instance;
import cloudos.provider.GoogleProviderManagement;

import com.clearspring.analytics.util.Lists;
import com.google.monitoring.v3.Aggregation;
import com.google.monitoring.v3.Aggregation.Aligner;
import com.google.monitoring.v3.Point;
import com.google.monitoring.v3.TimeSeries;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import lombok.extern.log4j.Log4j2;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by philipperibeiro on 3/25/17. The GoogleResourcesService provide all kind of
 * resources-related utils for CloudOS. One example is querying GCloud for metric datapoints.
 */
@Service
@Log4j2
public class GoogleResourcesService implements ResourcesManagement {

  /** Seconds to adjust the difference to bring data. */
  private static final int TIME_SECONDS_DIFF = 60;

  private static final String INSTANCE_NAME = "instance_name";
  private static final String ZONE = "zone";
  @Resource
  private GoogleMetricsIntegration googleMetricsIntegration;
  @Autowired
  GoogleInstanceRepository googleInstanceRepository;
  @Autowired
  GoogleProviderManagement googleProviderManagement;
  @Autowired
  InstanceService instanceService;

  @Override
  public AbstractInstance findByInstance(Instance instance) {
    return googleInstanceRepository.findByInstance(instance);
  }

  /**
   * Find all the datapoints associated with a particular resource, convert to a list of CloudOS
   * datapoints and returns the datapoints.
   *
   * @param resource The resource id.
   * @param instance The InstanceUtil request object
   * @return List of CloudosDatapoint if successful, null otherwise.
   */
  @Override
  public List<CloudosDatapoint> findDatapoints(@NotNull Instance resource,
      @NotNull InstanceData instance) {
    Long timeWindow = instance.getTimeWindowInSeconds();
    Integer timeFrame = instance.getTimeFrameInSeconds();
    String measure = (String) ResourcesUtils.convertMetricToMeasure(Providers.GOOGLE_COMPUTE_ENGINE,
        instance.getMetric());
    Iterable<TimeSeries> datapoints = googleMetricsIntegration.retrieveMetricTimeSeriesForInstance(
        resource.getProviderId(), measure, timeWindow, timeFrame, Aligner.ALIGN_MEAN);
    // fetch the corresponding measure
    if (log.isDebugEnabled()) {
      printResponse(datapoints);
    }
    // @TODO: Add the unique cloudosID and instanceID
    return ResourcesUtils.convertToCloudosDatapoint(resource.getId(), resource.getProviderId(),
        Lists.newArrayList(datapoints), Providers.GOOGLE_COMPUTE_ENGINE, instance.getMetric(),
        resource.getRegion());
  }

  /**
   * Print the response for logging reasons.
   *
   * @param iterable of timeSeries
   */
  private void printResponse(Iterable<TimeSeries> iterable) {
    Map<String, List<Point>> map = new HashMap<>();
    for (TimeSeries timeSeries : iterable) {
      String instance = timeSeries.getMetric().getLabelsOrDefault(INSTANCE_NAME, "");
      if (!map.containsKey(instance)) {
        map.put(instance, new ArrayList<>());
      }
      List<Point> pointsList = timeSeries.getPointsList();
      map.get(instance).addAll(pointsList);
    }
    Iterator<Entry<String, List<Point>>> iterator = map.entrySet().iterator();
    while (iterator.hasNext()) {
      Map.Entry<String, List<Point>> entry = iterator.next();
      List<Point> points = entry.getValue();
      Collections.sort(points, new Comparator<Point>() {
        @Override
        public int compare(Point o1, Point o2) {
          long i = o1.getInterval().getStartTime().getSeconds()
              - o2.getInterval().getStartTime().getSeconds();
          return i == 0 ? 0 : (i < 0 ? -1 : 1);
        }
      });
      for (Point point : points) {
        log.info("P: {} {} {} - Instance: {}",
            GoogleMetricsIntegration.formatInterval(point.getInterval()),
            point.getValue().getDescriptorForType().getName(), point.getValue().getDoubleValue(),
            entry.getKey());
      }
    }
  }

  @Override
  public List<CloudosDatapoint> findAllDatapoints(long timeWindow, int period,
      InstanceDataMetric... dataMetrics) {
    List<Instance> instances = instanceService
        .findAllActiveAndNotStoppedInstancesByProvider(Providers.GOOGLE_COMPUTE_ENGINE);
    List<String> instanceIds = instances.stream().filter(Objects::nonNull)
        .map(t -> t.getProviderId()).sorted().distinct().collect(Collectors.toList());
    List<CloudosDatapoint> datapoints = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(instanceIds)) {
      log.info("###################### instances: {}", Arrays.toString(instanceIds.toArray()));
      for (InstanceDataMetric googleMetric : dataMetrics) {
        // go to the provider
        String measure = (String) ResourcesUtils
            .convertMetricToMeasure(Providers.GOOGLE_COMPUTE_ENGINE, googleMetric);
        long timeWindowInSeconds = timeWindow / 1000;
        // add the difference to make it work
        timeWindowInSeconds += TIME_SECONDS_DIFF;
        Iterable<TimeSeries> points =
            googleMetricsIntegration.retrieveMetricTimeSeriesForInstance(instanceIds, measure,
                timeWindowInSeconds, period, Aggregation.Aligner.ALIGN_MEAN, null);
        printResponse(points);
        for (TimeSeries timeSeries : points) {
          String instanceId = timeSeries.getMetric().getLabelsMap().get(INSTANCE_NAME);
          String region = googleProviderManagement
              .getRegionFromZoneString(timeSeries.getResource().getLabelsMap().get(ZONE));
          Instance instance = instances.stream().filter(t -> t.getProviderId().equals(instanceId))
              .findFirst().get();
          List<CloudosDatapoint> convertToCloudosDatapoint =
              ResourcesUtils.convertToCloudosDatapoint(instance.getId(), instanceId,
                  Arrays.asList(timeSeries), Providers.GOOGLE_COMPUTE_ENGINE, googleMetric, region);
          if (CollectionUtils.isNotEmpty(convertToCloudosDatapoint)) {
            datapoints.addAll(convertToCloudosDatapoint);
          }
        }
      }
    }
    return datapoints;
  }
}
