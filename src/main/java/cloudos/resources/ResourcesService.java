package cloudos.resources;

import cloudos.Providers;
import cloudos.data.InstanceData;
import cloudos.exceptions.NotImplementedException;
import cloudos.instances.AmazonInstanceManagement;
import cloudos.instances.GoogleInstanceManagement;
import cloudos.instances.InstanceService;
import cloudos.models.AbstractInstance;
import cloudos.models.CloudosDatapoint;
import cloudos.models.CloudosDatapointsRepository;
import cloudos.models.Instance;
import cloudos.models.InstanceStatus;
import cloudos.models.QInstance;
import cloudos.models.ResourceFetchRequest;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;


/** Created by philipperibeiro on 3/25/17. */
@Service
@Log4j2
public class ResourcesService {

  @Autowired
  private AmazonResourcesService amazonResourcesService;

  @Autowired
  private GoogleResourcesService googleResourcesService;

  @Autowired
  private AmazonInstanceManagement amazonInstanceManagement;

  @Autowired
  private GoogleInstanceManagement googleInstanceManagement;

  @Autowired
  InstanceService instanceService;

  @Autowired
  AsyncTaskExecutor taskExecutor;
  
  @Autowired 
  private MongoTemplate mongoTemplate;

  /* We will save all the data points we fetch in the MongoDB */
  @Autowired
  private CloudosDatapointsRepository cloudosDatapointsRepository;

  /**
   * Get management class.
   *
   * @param providers provider
   * @return Management Class
   * @throws NotImplementedException if not implemented
   */
  private ResourcesManagement getManagement(Providers providers) throws NotImplementedException {
    switch (providers) {
      case AMAZON_AWS:
        return amazonResourcesService;
      case GOOGLE_COMPUTE_ENGINE:
        return googleResourcesService;
      default:
        break;
    }
    throw new NotImplementedException();
  }

  /**
   * Multiplex accross different cloud providers in order to find the actual datapoints. Currently
   * only AWS is being supported.
   *
   * @param resource The resource id.
   * @param instanceData The InstanceUtil request.
   * @return List of CloudosDatapoint A list of datapoints if successful, null otherwise.
   * @throws NotImplementedException if not implemented
   */
  public List<CloudosDatapoint> getDatapoints(@NotNull String resource,
      @NotNull InstanceData instanceData) throws NotImplementedException {
    Instance instance = instanceService.findById(resource);

    if (instance == null && instanceData != null && instanceData.getProvider() != null) {
      // TODO: remove it afterwards - failover for passing the provider id instead of the
      // cloudos instance id
      instance = instanceService.findByProviderAndProviderId(instanceData.getProvider(), resource);
    }

    // Multiplex across the cloud provider
    final List<CloudosDatapoint> datapoints = new ArrayList<>();
    if (instance != null && instanceData != null && instanceData.getProvider() != null) {
      final List<CloudosDatapoint> findDatapoints =
          this.getManagement(instanceData.getProvider()).findDatapoints(instance, instanceData);
      datapoints.addAll(findDatapoints);
      if (CollectionUtils.isNotEmpty(datapoints)) {
        taskExecutor.execute(new Runnable() {

          @Override
          public void run() {
            // save in another thread for performance
            log.info("saving datapoints: {}", findDatapoints.size());
            cloudosDatapointsRepository.save(findDatapoints);
            log.info("datapoints saved: {}", findDatapoints.size());
          }
        });
      }
    }
    return ResourcesUtils.sortDataPoints(datapoints);
  }

  /**
   * List the resources {@link Instance}.
   *
   * @param fetchRequest request {@link Instance}
   * @return page list of Instance and pagination data.
   */
  public Page<Instance> listResourcesByFilter(ResourceFetchRequest fetchRequest) {
    Pageable pageable = new PageRequest(fetchRequest.getPage(), fetchRequest.getPageSize(),
        Direction.DESC, Instance.CREATION_DATE, Instance.ID);
    return instanceService.findInstancesByFilter(getFiltersQ(fetchRequest), pageable);
  }
  

  /**
   * List the resources (instances).
   *
   * @param fetchRequest request
   * @return List of Instance
   */
  public List<Instance> listResources(ResourceFetchRequest fetchRequest) {
    Query query = new Query();
    if (StringUtils.isNotBlank(fetchRequest.getName())) {
      query.addCriteria(new Criteria(Instance.NAME).regex(fetchRequest.getName()));
    }
    if (fetchRequest.getProvider() != null) {
      query.addCriteria(new Criteria(Instance.PROVIDER).is(fetchRequest.getProvider()));
    }
    if (StringUtils.isNotBlank(fetchRequest.getRegion())) {
      query.addCriteria(new Criteria(Instance.REGION).is(fetchRequest.getRegion()));
    }
    if (StringUtils.isNotBlank(fetchRequest.getZone())) {
      query.addCriteria(new Criteria(Instance.ZONE).is(fetchRequest.getZone()));
    }
    if (CollectionUtils.isNotEmpty(fetchRequest.getInstanceStatus())) {
      query.addCriteria(new Criteria(Instance.STATUS).in(fetchRequest.getInstanceStatus()));
    }
    if (CollectionUtils.isNotEmpty(fetchRequest.getInstanceStatus())
        && fetchRequest.getInstanceStatus().contains(InstanceStatus.TERMINATED)) {
      // it can be deleted or not
      // query.addCriteria(new Criteria(Instance.DELETED).is(true));
    } else {
      // just not deleted
      query.addCriteria(new Criteria(Instance.DELETED).is(false));
    }
    Pageable pageable =
        new PageRequest(
            fetchRequest.getPage(),
            fetchRequest.getPageSize(),
            Direction.DESC,
            Instance.CREATION_DATE,
            Instance.ID);
    query.with(pageable);
    List<Instance> list = mongoTemplate.find(query, Instance.class);
    
    return list;
  }

  /**
   * Method that mount a filter for Instance.
   * 
   * @param fetchRequest Request by search resource.
   * @return Predicate Conditions the filter
   */
  private Predicate getFiltersQ(ResourceFetchRequest fetchRequest) {
    BooleanBuilder builder = new BooleanBuilder();
    QInstance instance = new QInstance("instance");

    if (StringUtils.isNotBlank(fetchRequest.getName())) {
      builder.and(instance.name.contains(fetchRequest.getName()));
    }
    if (fetchRequest.getProvider() != null) {
      builder.and(instance.provider.eq(fetchRequest.getProvider()));
    }
    if (StringUtils.isNotBlank(fetchRequest.getRegion())) {
      builder.and(instance.region.eq(fetchRequest.getRegion()));
    }
    if (StringUtils.isNotBlank(fetchRequest.getZone())) {
      builder.and(instance.zone.eq(fetchRequest.getZone()));
    }
    if (CollectionUtils.isNotEmpty(fetchRequest.getInstanceStatus())) {
      builder.and(instance.status.in(fetchRequest.getInstanceStatus()));
    }
    builder.and(instance.deleted.eq(false));
    return builder;
  }

  /**
   * Return a resource.
   *
   * @param resourceId id
   * @return Instance
   * @throws NotImplementedException if not implemented
   */
  public AbstractInstance retrieveResource(String resourceId) throws NotImplementedException {
    Instance instance = instanceService.findById(resourceId);
    if (instance != null) {
      return this.getManagement(instance.getProvider()).findByInstance(instance);
    }
    return null;
  }

  /** Sync all the cloudos Instances with the provider instances. */
  public void syncInstances() {
    amazonInstanceManagement.syncInstances();
    googleInstanceManagement.syncInstances();
  }
}
