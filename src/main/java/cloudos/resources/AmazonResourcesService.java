package cloudos.resources;

import cloudos.Providers;
import cloudos.amazon.AmazonInstanceRepository;
import cloudos.amazon.CloudWatchDimensions;
import cloudos.amazon.CloudWatchMetrics;
import cloudos.amazon.CloudWatchService;
import cloudos.data.InstanceData;
import cloudos.data.InstanceDataMetric;
import cloudos.instances.InstanceService;
import cloudos.models.AbstractInstance;
import cloudos.models.CloudosDatapoint;
import cloudos.models.Instance;

import com.amazonaws.services.cloudwatch.model.Datapoint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.extern.log4j.Log4j2;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by philipperibeiro on 3/25/17. The AmazonResourcesService provide all kind of
 * resources-related utils for CloudOS. One example is querying AWS for CloudWatch datapoints.
 */
@Service
@Log4j2
public class AmazonResourcesService implements ResourcesManagement {

  private static final String INSTANCE_ID = "InstanceId";
  private static final String AWS_AVERAGE = "Average";

  @Autowired
  InstanceService instanceService;
  @Autowired
  CloudWatchService cloudWatchService;
  @Autowired
  AmazonInstanceRepository amazonInstanceRepository;

  @Override
  public AbstractInstance findByInstance(Instance instance) {
    return amazonInstanceRepository.findByInstance(instance);
  }

  /**
   * Find all the datapoints associated with a particular resource, convert to a list of CloudOS
   * datapoints and returns the datapoints.
   *
   * @param resource The resource id.
   * @param instance The InstanceUtil request object
   * @return List of CloudosDatapoint if successful, null otherwise.
   */
  @Override
  public List<CloudosDatapoint> findDatapoints(@NotNull Instance resource,
      @NotNull InstanceData instance) {
    // fetch all the data points corresponding to the given measure
    String region = resource.getRegion();
    int timeFrame = instance.getTimeFrameInSeconds() * 1;
    long timeWindow = instance.getTimeWindowInSeconds() * 1000;
    CloudWatchMetrics measure = (CloudWatchMetrics) ResourcesUtils
        .convertMetricToMeasure(Providers.AMAZON_AWS, instance.getMetric());
    List<Datapoint> datapoints =
        cloudWatchService.getClient(region).getDatapoints(Arrays.asList(resource.getProviderId()),
            CloudWatchDimensions.getDimensionByValue(INSTANCE_ID), measure,
            Arrays.asList(instance.getType().getDescription()), timeFrame, timeWindow);
    log.info("Datapoints returned: \n{}", datapoints);
    // @TODO: Add the unique cloudosID and instanceID
    return ResourcesUtils.convertToCloudosDatapoint(resource.getId(), resource.getProviderId(),
        datapoints, Providers.AMAZON_AWS, instance.getMetric(), region);
  }

  @Override
  public List<CloudosDatapoint> findAllDatapoints(long timeWindow, int period,
      InstanceDataMetric... dataMetrics) {
    List<CloudosDatapoint> datapoints = new ArrayList<>();
    List<Instance> instances =
        instanceService.findAllActiveAndNotStoppedInstancesByProvider(Providers.AMAZON_AWS);
    if (CollectionUtils.isNotEmpty(instances)) {
      for (InstanceDataMetric awsMetric : dataMetrics) {
        CloudWatchMetrics measure = (CloudWatchMetrics) ResourcesUtils
            .convertMetricToMeasure(Providers.AMAZON_AWS, awsMetric);
        for (Instance instance2 : instances) {
          String region = instance2.getRegion();
          log.info("###################### region: {} instances: {}", region, instance2.getName());
          List<Datapoint> points = cloudWatchService.getClient(region).getDatapoints(
              Arrays.asList(instance2.getProviderId()), CloudWatchDimensions.INSTANCE_ID, measure,
              Arrays.asList(AWS_AVERAGE), period, timeWindow);
          if (CollectionUtils.isNotEmpty(points)) {
            datapoints.addAll(ResourcesUtils.convertToCloudosDatapoint(instance2.getId(),
                instance2.getProviderId(), points, Providers.AMAZON_AWS, awsMetric, region));
          }
        }
      }
    }
    return datapoints;
  }
}
