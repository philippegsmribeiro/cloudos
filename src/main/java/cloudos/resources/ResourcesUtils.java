package cloudos.resources;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.amazonaws.services.cloudwatch.model.Datapoint;
import com.google.monitoring.v3.Point;
import com.google.monitoring.v3.TimeSeries;

import cloudos.Providers;
import cloudos.amazon.CloudWatchMetrics;
import cloudos.data.InstanceDataMetric;
import cloudos.data.InstanceDataUnit;
import cloudos.google.integration.GoogleMetrics;
import cloudos.google.integration.GoogleMetricsIntegration;
import cloudos.models.AbstractCloudosDatapoint;
import cloudos.models.CloudosDatapoint;
import cloudos.models.CloudosDatapointAggregated;

/** Created by philipperibeiro on 3/25/17. */
public class ResourcesUtils {

  /** Make the constructor private since the class ResourcesUtils is static. */
  private ResourcesUtils() {}

  /**
   * Convert a cloud-specific datapoint into a Cloudos Datapoint.
   *
   * @param instanceId instance
   * @param cloudId cloud identifier
   * @param cloudDatapoints points
   * @param providers provider
   * @param metric metric
   * @return List of CloudosDatapoint
   */
  public static List<CloudosDatapoint> convertToCloudosDatapoint(String instanceId, String cloudId,
      List<?> cloudDatapoints, Providers providers, InstanceDataMetric metric, String region) {
    return convertToCloudosDatapoints(instanceId, cloudId, cloudDatapoints, providers, metric,
        region);
  }

  /**
   * Convert a cloud-specific datapoint into a Cloudos Datapoint Aggregated.
   *
   * @param instanceId id
   * @param cloudDatapoints points
   * @param providers provider
   * @param metric metric
   * @return List of CloudosDatapointAggregated
   */
  public static List<CloudosDatapointAggregated> convertToCloudosDatapointAggregated(
      String instanceId, List<?> cloudDatapoints, Providers providers, InstanceDataMetric metric,
      String region) {
    return convertToCloudosDatapoints(instanceId, null, cloudDatapoints, providers, metric, region);
  }

  /**
   * Convert a cloud-specific datapoint into a Cloudos Datapoint.
   *
   * @param instanceId id
   * @param cloudId cloud identifier
   * @param cloudDatapoints points
   * @param providers provider
   * @param metric metric
   * @return List of CloudosDatapoint
   */
  @SuppressWarnings("unchecked")
  private static <T extends AbstractCloudosDatapoint> List<T> convertToCloudosDatapoints(
      String instanceId, String cloudId, List<?> cloudDatapoints, Providers providers,
      InstanceDataMetric metric, String region) {
    List<T> datapoints = new LinkedList<>();
    InstanceDataUnit dataUnit = getDataUnit(metric);
    switch (providers) {
      case AMAZON_AWS:
        for (Object object : cloudDatapoints) {
          Datapoint datapoint = (Datapoint) object;
          T abstractCloudosDatapoint = null;
          if (StringUtils.isNotEmpty(cloudId)) {
            // normal datapoint
            abstractCloudosDatapoint = (T) new CloudosDatapoint(providers, region, instanceId,
                cloudId, metric, datapoint.getTimestamp(), datapoint.getAverage(), dataUnit);
          } else {
            // aggregated datapoint
            abstractCloudosDatapoint = (T) new CloudosDatapointAggregated(providers, instanceId,
                region, metric, datapoint.getTimestamp(), datapoint.getAverage(), dataUnit);
          }
          datapoints.add(abstractCloudosDatapoint);
        }
        break;
      case GOOGLE_COMPUTE_ENGINE:
        for (Object object : cloudDatapoints) {
          TimeSeries timeSeries = (TimeSeries) object;
          List<Point> pointsList = timeSeries.getPointsList();
          for (Point point : pointsList) {
            T abstractCloudosDatapoint = null;
            double value = point.getValue().getDoubleValue();
            if (dataUnit == InstanceDataUnit.PERCENTUAL) {
              value = value * 100;
            } else {
              // TODO: validate it
            }
            if (StringUtils.isNotEmpty(cloudId)) {
              // Object fromJson = gson.fromJson(gson.toJson(point), Object.class);
              // normal datapoint
              abstractCloudosDatapoint =
                  (T) new CloudosDatapoint(
                      providers, region, instanceId, cloudId, metric, GoogleMetricsIntegration
                          .getDateFromTimestamp(point.getInterval().getStartTime()),
                      value, dataUnit);
            } else {
              // aggregated datapoint
              abstractCloudosDatapoint =
                  (T) new CloudosDatapointAggregated(
                      providers, region, instanceId, metric, GoogleMetricsIntegration
                          .getDateFromTimestamp(point.getInterval().getStartTime()),
                      value, dataUnit);
            }
            datapoints.add(abstractCloudosDatapoint);
          }
        }
        break;
      default:
        break;
    }
    return datapoints;
  }

  /**
   * Sort Datapoints.
   *
   * @param list datapoints
   * @return list of datapoints sorted
   */
  public static <T extends AbstractCloudosDatapoint> List<T> sortDataPoints(List<T> list) {
    Collections.sort(list, new Comparator<T>() {
      @Override
      public int compare(T o1, T o2) {
        return o1.getTimestamp().compareTo(o2.getTimestamp());
      }
    });
    return list;
  }

  /**
   * Convert a metric to a cloud-specific metric.
   *
   * @param providers provider
   * @param instanceDataMetric metric
   * @return object
   */
  public static Object convertMetricToMeasure(Providers providers,
      InstanceDataMetric instanceDataMetric) {
    switch (providers) {
      case AMAZON_AWS:
        return convertToMeasureAws(instanceDataMetric);
      case GOOGLE_COMPUTE_ENGINE:
        return convertToMeasureGCloud(instanceDataMetric);
      default:
        break;
    }
    return null;
  }

  /**
   * GCloud.
   *
   * @param instanceDataMetric metric
   * @return String measure
   */
  private static String convertToMeasureGCloud(InstanceDataMetric instanceDataMetric) {
    switch (instanceDataMetric) {
      case CPU_UTILIZATION:
        return GoogleMetrics.COMPUTE_GOOGLEAPIS_COM_INSTANCE_CPU_UTILIZATION;
      case DISK_READ_BYTES:
        return GoogleMetrics.COMPUTE_GOOGLEAPIS_COM_INSTANCE_DISK_READ_BYTES_COUNT;
      case DISK_WRITE_BYTES:
        return GoogleMetrics.COMPUTE_GOOGLEAPIS_COM_INSTANCE_DISK_WRITE_BYTES_COUNT;
      case NETWORK_RECEIVED_BYTES:
        return GoogleMetrics.COMPUTE_GOOGLEAPIS_COM_INSTANCE_NETWORK_RECEIVED_BYTES_COUNT;
      case NETWORK_SENT_BYTES:
        return GoogleMetrics.COMPUTE_GOOGLEAPIS_COM_INSTANCE_NETWORK_SENT_BYTES_COUNT;
      default:
        break;
    }
    return null;
  }

  /**
   * AWS.
   *
   * @param instanceDataType datatype
   * @return CloudWatchMetrics
   */
  private static CloudWatchMetrics convertToMeasureAws(InstanceDataMetric instanceDataType) {
    switch (instanceDataType) {
      case CPU_UTILIZATION:
        return CloudWatchMetrics.CPU_UTILIZATION;
      case DISK_READ_BYTES:
        return CloudWatchMetrics.DISK_READ_BYTES;
      case DISK_WRITE_BYTES:
        return CloudWatchMetrics.DISK_WRITE_BYTES;
      case NETWORK_RECEIVED_BYTES:
        return CloudWatchMetrics.NETWORK_IN;
      case NETWORK_SENT_BYTES:
        return CloudWatchMetrics.NETWORK_OUT;
      default:
        break;
    }
    return null;
  }

  /**
   * Get the data unit.
   *
   * @param instanceDataMetric metric
   * @return InstanceDataUnit
   */
  public static InstanceDataUnit getDataUnit(InstanceDataMetric instanceDataMetric) {
    switch (instanceDataMetric) {
      case CPU_UTILIZATION:
        return InstanceDataUnit.PERCENTUAL;
      case DISK_READ_BYTES:
        return InstanceDataUnit.BYTES;
      case DISK_WRITE_BYTES:
        return InstanceDataUnit.BYTES;
      case NETWORK_RECEIVED_BYTES:
        return InstanceDataUnit.BYTES;
      case NETWORK_SENT_BYTES:
        return InstanceDataUnit.BYTES;
      default:
        break;
    }
    return null;
  }
}
