package cloudos;

import cloudos.data.LogRecord;
import cloudos.data.LogRecordRequest;
import cloudos.data.LogsService;
import cloudos.elasticsearch.model.KibanaSearchParametersTransformer;
import cloudos.elasticsearch.model.LogHit;
import cloudos.elasticsearch.model.LogSearchParameters;
import cloudos.elasticsearch.services.FilebeatService;
import cloudos.security.WebSecurityConfig;

import java.net.UnknownHostException;
import java.util.List;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/** Created by philipperibeiro on 3/31/17. */
@RestController
@RequestMapping(value = LogsController.LOGS)
@Log4j2
public class LogsController {

  public static final String LOGS = WebSecurityConfig.API_PATH + "/logs";

  @Autowired
  private LogsService logsService;

  @Autowired
  private FilebeatService filebeatService;

  @Autowired
  private KibanaSearchParametersTransformer transformer;

  public LogsController() {}

  public LogsController(LogsService logsService) {
    this.logsService = logsService;
  }

  @RequestMapping(value = "/list/{instance}/logs", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> saveLogRecord(@RequestBody LogRecordRequest logRecordRequest) {
    return new ResponseEntity<HttpStatus>(HttpStatus.NOT_FOUND);
  }

  @RequestMapping(value = "/list/{instance}/logs", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> fetchLogRecord(@PathVariable String instance) {
    // @TODO: Add pagination... otherwise response may be huge
    List<LogRecord> logRecords = this.logsService.fetchLogRecords(instance);
    return new ResponseEntity<>(logRecords, HttpStatus.OK);
  }

  /**
   * Retrieves the hits matching the query word against message field
   *
   * @param LogSearchParameters: represents the parameters to query and match the word/phrase.
   * @return
   */
  @RequestMapping(value = "/error_message_query", method = RequestMethod.POST)
  @ResponseBody
  public List<LogHit> getHitsByMessageQueryWord(
      @RequestBody LogSearchParameters logSearchParameters) throws UnknownHostException {
    return filebeatService.getHitsByMessageQueryWord(transformer.apply(logSearchParameters));
  }
}
