package cloudos.monitoring;

import static java.lang.annotation.ElementType.TYPE;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Run after application starting up.
 * All monitors annotated with this will start(run once) as soon as the application gets started.
 * Not depending of the first scheduled time to kick in.
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(TYPE)
public @interface RunAfterStartup {
}
