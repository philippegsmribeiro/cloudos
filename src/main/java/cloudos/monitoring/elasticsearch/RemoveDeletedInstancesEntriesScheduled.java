package cloudos.monitoring.elasticsearch;

import cloudos.Application;
import cloudos.config.NotLoadOnTestCondition;
import cloudos.instances.InstanceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Conditional;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * Remove all the ElasticSearch data and repository entries related to the instances deleted before
 * the given timewindow.
 * 
 * @author Rogério Souza
 *
 */
@Service
@Conditional(NotLoadOnTestCondition.class)
public class RemoveDeletedInstancesEntriesScheduled {

  @Value("${cloudos.data.service.deleted.instances.entries.timewindow.minutes}")
  private Integer deletedTimewindowInMinutes;

  @Autowired
  private InstanceService instanceService;

  /**
   * Remove all the ElasticSearch data and repository entries related to the instances deleted
   * before the given timewindow (deletedTimewindowInMinutes attribute).
   * 
   */
  @Scheduled(cron = "${cloudos.data.service.deleted.instances.entries.cron}")
  public void removeDeletedInstancesEntries() {
    if (Application.STARTED.get()) {
      instanceService.removeDeletedInstancesEntries(deletedTimewindowInMinutes);
    }
  }
}
