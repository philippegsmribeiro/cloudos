package cloudos.monitoring.elasticsearch;

import cloudos.Application;
import cloudos.config.NotLoadOnTestCondition;
import cloudos.elasticsearch.services.FilebeatService;
import cloudos.elasticsearch.services.HeartbeatService;
import cloudos.elasticsearch.services.MetricbeatService;
import cloudos.elasticsearch.services.PacketbeatService;
import cloudos.queue.QueueServiceException;

import java.net.UnknownHostException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Conditional;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * Scheduled service responsible to ship data from Elasticsearch to SQS tenants indexes queues every
 * 60 seconds based on cron and timewindow properties
 *
 * @author Rogerio Souza
 */

// TODO Retrive the correct tenant value for each method
@Service
@Conditional(NotLoadOnTestCondition.class)
public class BeatDataShipScheduled {

  @Value("${cloudos.data.service.heartbeat.scheduling.timewindow.seconds}")
  private Integer heartbeatTimewindowInSeconds;

  @Value("${cloudos.data.service.filebeat.scheduling.timewindow.seconds}")
  private Integer filebeatTimewindowInSeconds;

  @Value("${cloudos.data.service.packetbeat.scheduling.timewindow.seconds}")
  private Integer packetbeatTimewindowInSeconds;

  @Value("${cloudos.data.service.metricbeat.scheduling.timewindow.seconds}")
  private Integer metricbeatTimewindowInSeconds;

  @Autowired private MetricbeatService metricbeatService;

  @Autowired private HeartbeatService heartbeatService;

  @Autowired private PacketbeatService packetbeatService;

  @Autowired private FilebeatService filebeatService;

  /**
   * Runs every CRON time definition and ships all data from elasticsearch metrcibeat index to a
   * specific tenant and beat index queue - filtering by range considering the timewindow definition
   *
   * @throws UnknownHostException
   * @throws QueueServiceException
   */
  @Scheduled(cron = "${cloudos.data.service.metricbeat.scheduling.cron}")
  public void shipMetricbeatData() throws UnknownHostException, QueueServiceException {
    // TODO Retrive the correct tenant value
    if (Application.STARTED.get()) {
      metricbeatService.shipData(metricbeatTimewindowInSeconds, 10);
    }
  }

  /**
   * Runs every CRON time definition and ships all data from elasticsearch heartbeat index to a
   * specific tenant and beat index queue - filtering by range considering the timewindow definition
   *
   * @throws UnknownHostException
   * @throws QueueServiceException
   */
  @Scheduled(cron = "${cloudos.data.service.heartbeat.scheduling.cron}")
  public void shipHeartbeatData() throws UnknownHostException, QueueServiceException {
    // TODO Retrive the correct tenant value
    if (Application.STARTED.get()) {
      heartbeatService.shipData(heartbeatTimewindowInSeconds, 10);
    }
  }

  /**
   * Runs every CRON time definition and ships all data from elasticsearch packetbeat index to a
   * specific tenant and beat index queue - filtering by range considering the timewindow definition
   *
   * @throws UnknownHostException
   * @throws QueueServiceException
   */
  @Scheduled(cron = "${cloudos.data.service.packetbeat.scheduling.cron}")
  public void shipPacketbeatData() throws UnknownHostException, QueueServiceException {
    // TODO Retrive the correct tenant value
    if (Application.STARTED.get()) {
      packetbeatService.shipData(packetbeatTimewindowInSeconds, 10);
    }
  }

  /**
   * Runs every CRON time definition and ships all data from elasticsearch filebeat index to a
   * specific tenant and beat index queue - filtering by range considering the timewindow definition
   *
   * @throws UnknownHostException
   * @throws QueueServiceException
   */
  @Scheduled(cron = "${cloudos.data.service.filebeat.scheduling.cron}")
  public void shipFilebeatData() throws UnknownHostException, QueueServiceException {
    // TODO Retrive the correct tenant value
    if (Application.STARTED.get()) {
      filebeatService.shipData(filebeatTimewindowInSeconds, 10);
    }
  }
}
