package cloudos.monitoring.dashboard;

import cloudos.Application;
import cloudos.config.NotLoadOnTestCondition;
import cloudos.config.ThreadingConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Conditional;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@Conditional(NotLoadOnTestCondition.class)
class DashboardMonitor {

  @Autowired
  @Qualifier(ThreadingConfig.MONITOR)
  AsyncTaskExecutor taskExecutor;

  @Autowired
  DashboardCostUpdateMonitor costUpdateMonitor;

  /**
   * Update the cost related information in the dashboard.
   */
  @Scheduled(cron = "${cloudos.dashboard.cost.update}")
  public void runDashboardMonitor() {
    if (Application.STARTED.get()) {
      taskExecutor.execute(costUpdateMonitor);
    }
  }
  
  @Autowired
  DashboardResourcesMonitor resourcesMonitor;
  
  /**
   * Update the resources table from the current date to the beginning of the month.
   */
  @Scheduled(cron = "${cloudos.dashboard.resources.update}")
  public void runResourcesMonitor() {
    if (Application.STARTED.get()) {
      taskExecutor.execute(resourcesMonitor);
    }
  }
  
  @Autowired
  DashboardRegionsMonitor regionsMonitor;
  
  /**
   * Updates the regions entry to display which the regions the user currently has offerings.
   */
  @Scheduled(cron = "${cloudos.dashboard.regions.update}")
  public void runRegionsMonitor() {
    if (Application.STARTED.get()) {
      taskExecutor.execute(regionsMonitor);
    }
  }
}