package cloudos.monitoring.dashboard;

import cloudos.dashboard.DashboardService;
import cloudos.monitoring.AbstractMonitor;
import cloudos.monitoring.RunAfterStartup;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by philipperibeiro on 6/23/17. The DashboardMonitor is responsible for executing Spark
 * tasks, updating the dashboard information and storing it in the respective collection.
 *
 * <p>
 * This is done in order to speed up requests since no job is done when the request is made.
 * Instead, the job is executed in the background every 30 minutes.
 * </p>
 */
@Component
@RunAfterStartup
@Log4j2
class DashboardResourcesMonitor extends AbstractMonitor {

  @Autowired
  private DashboardService dashboardService;

  @Override
  public void runTask() {
    log.info("========= Updating the dashboard resources entries ===========");
    dashboardService.updateResources();
    log.info("========= Updating the dashboard resources entries done! =====");
  }
}