package cloudos.monitoring.dashboard;

import cloudos.dashboard.DashboardService;
import cloudos.monitoring.AbstractMonitor;
import cloudos.monitoring.RunAfterStartup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by philipperibeiro on 6/23/17. The DashboardMonitor is responsible for executing Spark
 * tasks, updating the dashboard information and storing it in the respective collection.
 *
 * <p>
 * This is done in order to speed up requests since no job is done when the request is made.
 * Instead, the job is executed in the background every 30 minutes.
 * </p>
 */
@Component
@RunAfterStartup
public class DashboardCostUpdateMonitor extends AbstractMonitor {

  @Autowired
  private DashboardService dashboardService;

  @Override
  public void runTask() {
    dashboardService.updateCostSummary();
  }
}
