package cloudos.monitoring.healthchecker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cloudos.Providers;
import cloudos.alerts.AlertService;
import cloudos.exceptions.NotImplementedException;
import cloudos.models.alerts.AlertRequest;
import cloudos.monitoring.AbstractProviderMonitor;
import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class HealthcheckerMonitorUpdateAlerts extends AbstractProviderMonitor {

  @Autowired
  private AlertService alertService;

  @Override
  public void run(Providers provider) {

    try {

      log.info("============= Processing {} Alert =================", provider);

      if (!getCredentialManagement(provider).hasCredential()) {
        log.info("Provider: {} has no credentials yet.", provider);
        return;
      }

      alertService.synchronizedAlertByProvider(AlertRequest.builder().provider(provider).build());
    } catch (NotImplementedException e) {
      log.info("Processing Alerts {} ", provider, e.getMessage());
    } catch (Exception e) {
      log.error("Error caught while Processing Alerts " + e.getMessage(), e);
    }
    log.info("==========================================================");
  }

}
