package cloudos.monitoring.healthchecker;

import cloudos.Application;
import cloudos.config.NotLoadOnTestCondition;
import cloudos.config.ThreadingConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Conditional;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * Created by philipperibeiro on 6/27/17.
 *
 * <p>
 * Perform monitoring tasks for the Healthchecker service. This monitor will run periodically every
 * 30 minutes, and it will run tasks and jobs that is going to update the Healthchecker and
 * Resources related information.
 */
@Service
@Conditional(NotLoadOnTestCondition.class)
public class HealthcheckerMonitor {

  @Autowired
  @Qualifier(ThreadingConfig.MONITOR)
  private AsyncTaskExecutor taskExecutor;

  @Autowired
  private HealthcheckerMonitorUpdateResourcesEstimate healthcheckerMonitorUpdateResourcesEstimate;

  /** Updates the resources estimated cost. */
  @Scheduled(cron = "${cloudos.healthchecker.resources.estimate.update}")
  public void updateResourcesEstCost() {
    if (Application.STARTED.get()) {
      taskExecutor.execute(healthcheckerMonitorUpdateResourcesEstimate);
    }
  }

  @Autowired
  HealthcheckerMonitorUpdateResourceCost healthcheckerMonitorUpdateResourceCost;

  /** Updates the resources cost. */
  @Scheduled(cron = "${cloudos.healthchecker.resource.cost.update}")
  public void updateResourceCost() {
    if (Application.STARTED.get()) {
      taskExecutor.execute(healthcheckerMonitorUpdateResourceCost);
    }
  }

  @Autowired
  HealthcheckerMonitorUpdateResourcesCost healthcheckerMonitorUpdateResourcesCost;

  /**
   * Run a job that will update the resources cost table. This Spark job will aggregate the cost of
   * running the cloud by provider and by day, for the past 10 days. The newly created resources
   * cost will then be stored in Mongo
   */
  @Scheduled(cron = "${cloudos.healthchecker.resources.cost.update}")
  public void updateResourcesCost() {
    if (Application.STARTED.get()) {
      taskExecutor.execute(healthcheckerMonitorUpdateResourcesCost);
    }
  }

  @Autowired
  HealthcheckerMonitorUpdateAlerts healthcheckerMonitorUpdateAlerts;

  /** Updates the alert. */
  @Scheduled(cron = "${cloudos.healthchecker.alert.update}")
  public void updateAlerts() {
    if (Application.STARTED.get()) {
      this.taskExecutor.execute(healthcheckerMonitorUpdateAlerts);
    }
  }
}
