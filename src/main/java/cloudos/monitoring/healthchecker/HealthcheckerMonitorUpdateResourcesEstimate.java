package cloudos.monitoring.healthchecker;

import cloudos.Providers;
import cloudos.costanalysis.GenericBillingReportRepository;
import cloudos.healthchecker.HealthcheckerJob;
import cloudos.instances.InstanceService;
import cloudos.models.Instance;
import cloudos.models.costanalysis.GenericBillingReport;
import cloudos.monitoring.AbstractProviderMonitor;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import lombok.extern.log4j.Log4j2;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Log4j2
class HealthcheckerMonitorUpdateResourcesEstimate extends AbstractProviderMonitor {

  private static final int MAX_RESOURCES_COST_WINDOW = 45;

  @Autowired
  private InstanceService instanceService;

  @Autowired
  private GenericBillingReportRepository genericBillingReportRepository;

  private HealthcheckerJob healthcheckerJob;

  public HealthcheckerMonitorUpdateResourcesEstimate() {
    this.healthcheckerJob = new HealthcheckerJob();
  }

  @Override
  public void run(Providers provider) {
    log.info("========== Updating the Resources Estimate Cost =========");
    try {
      // get 45 days worth of billings reports, starting today
      final Date startDate = new Date(
          System.currentTimeMillis() - TimeUnit.DAYS.toMillis(MAX_RESOURCES_COST_WINDOW));
      final Date endDate = new Date();

      // AWS
      List<GenericBillingReport> billingReports =
          genericBillingReportRepository.findByUsageStartTimeBetween(startDate, endDate);
      log.info("Found {} Billing Reports", CollectionUtils.size(billingReports));

      // get all the instances
      List<Instance> instances = this.instanceService.findByCreationDateAfter(new Date(
          System.currentTimeMillis() - TimeUnit.DAYS.toMillis(MAX_RESOURCES_COST_WINDOW)));

      List<Instance> updatesInstances =
          this.healthcheckerJob.updateResourcesEstCost(instances, billingReports);

      // add only if the update instances is non-empty
      if (CollectionUtils.isNotEmpty(updatesInstances)) {
        // update the instances cost
        log.info("Saving {} instances", updatesInstances.size());
        this.instanceService.save(updatesInstances);
      }
    } catch (Exception e) {
      log.error(e.getMessage(),e);
    }
    log.info("===================================================");
  }

}
