package cloudos.monitoring.healthchecker;

import cloudos.Providers;
import cloudos.costanalysis.GenericBillingReportRepository;
import cloudos.healthchecker.HealthcheckerJob;
import cloudos.healthchecker.ResourcesCost;
import cloudos.healthchecker.ResourcesCostEntry;
import cloudos.healthchecker.ResourcesCostRepository;
import cloudos.instances.InstanceService;
import cloudos.models.Instance;
import cloudos.models.costanalysis.GenericBillingReport;
import cloudos.monitoring.AbstractProviderMonitor;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Log4j2
class HealthcheckerMonitorUpdateResourcesCost extends AbstractProviderMonitor {

  private static final int RESOURCES_COST_WINDOW = 10;

  @Autowired
  private InstanceService instanceService;

  @Autowired
  private GenericBillingReportRepository genericBillingReportRepository;

  @Autowired
  private ResourcesCostRepository costRepository;

  private HealthcheckerJob healthcheckerJob;

  public HealthcheckerMonitorUpdateResourcesCost() {
    this.healthcheckerJob = new HealthcheckerJob();
  }

  @Override
  public void run(Providers provider) {
    log.info("============= Updating the Resources Cost ==========");
    try {
      // get 10 days worth of billings reports, starting today
      final Date startDate =
          new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(RESOURCES_COST_WINDOW));
      final Date endDate = new Date();

      List<GenericBillingReport> billingReports =
          genericBillingReportRepository.findByUsageStartTimeBetween(startDate, endDate);

      List<Instance> instances = this.instanceService.findByCreationDateAfter(
          new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(RESOURCES_COST_WINDOW)));

      // calculate the cost of running the cost in the past 10 days, by provider
      LinkedList<ResourcesCostEntry> entries = healthcheckerJob.getResourcesCostLast10Days(
          instances, billingReports, providersService.retrieveSupportedProviders());

      double totalCost = 0.0;

      if (entries != null) {
        // sum over the all the entries
        for (ResourcesCostEntry entry : entries) {
          // create and save the new cost repository
          totalCost += entry.getCost().stream().mapToDouble(x -> x.getCost()).sum();
        }
      }

      ResourcesCost resourcesCost = new ResourcesCost(entries, totalCost);
      log.info("Inserting Resource cost {} into the collection", resourcesCost);
      this.costRepository.save(resourcesCost);
    } catch (Exception e) {
      log.error(e.getMessage(),e);
    }
    log.info("===================================================");
  }

}
