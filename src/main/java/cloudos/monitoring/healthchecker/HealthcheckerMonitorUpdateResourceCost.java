package cloudos.monitoring.healthchecker;

import cloudos.Providers;
import cloudos.costanalysis.GenericBillingReportRepository;
import cloudos.healthchecker.HealthcheckerJob;
import cloudos.healthchecker.ResourceChart;
import cloudos.healthchecker.ResourceChartRepository;
import cloudos.instances.InstanceService;
import cloudos.models.Instance;
import cloudos.models.costanalysis.GenericBillingReport;
import cloudos.monitoring.AbstractProviderMonitor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import lombok.extern.log4j.Log4j2;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Log4j2
class HealthcheckerMonitorUpdateResourceCost extends AbstractProviderMonitor {

  private static final int MAX_RESOURCES_COST_WINDOW = 45;

  private static final int RESOURCE_MONTLY_WINDOW = 30;

  @Autowired
  private InstanceService instanceService;

  @Autowired
  private GenericBillingReportRepository genericBillingReportRepository;

  @Autowired
  private ResourceChartRepository resourceChartRepository;

  private HealthcheckerJob healthcheckerJob;

  public HealthcheckerMonitorUpdateResourceCost() {
    this.healthcheckerJob = new HealthcheckerJob();
  }

  @Override
  public void run(Providers provider) {
    log.info("============= Updating the Resource Cost ==========");
    try {
      // get all the instances
      List<Instance> instances = this.instanceService.findByCreationDateAfter(new Date(
          System.currentTimeMillis() - TimeUnit.DAYS.toMillis(MAX_RESOURCES_COST_WINDOW)));

      instances.forEach(instance -> {
        List<GenericBillingReport> billingReports = new ArrayList<>();

        List<GenericBillingReport> reports =
            genericBillingReportRepository.findByProviderAndResourceIdAfter(
                instance.getProvider(), instance.getProviderId(), new Date(
                    System.currentTimeMillis() - TimeUnit.DAYS.toMillis(RESOURCE_MONTLY_WINDOW)));
        CollectionUtils.addAll(billingReports, reports);

        // @TODO: Add other providers
        try {
          ResourceChart resourceChart = this.healthcheckerJob.updateResourceCost(billingReports,
              instance.getProviderId(), instance.getProvider());
          // check if the resource chart is null
          if (resourceChart != null) {
            resourceChartRepository.save(resourceChart);
          }
        } catch (Exception e) {
          // TODO: handle exception
          log.error("error", e);
        }
      });

    } catch (Exception e) {
      log.error(e.getMessage(),e);
    }
    log.info("===================================================");
  }

}
