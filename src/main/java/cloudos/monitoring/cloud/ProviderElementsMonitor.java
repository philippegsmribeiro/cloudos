package cloudos.monitoring.cloud;

import cloudos.Providers;
import cloudos.config.ThreadingConfig;
import cloudos.exceptions.NotImplementedException;
import cloudos.monitoring.AbstractProviderMonitor;
import cloudos.monitoring.RunAfterStartup;
import cloudos.provider.AmazonProviderManagement;
import cloudos.provider.GoogleProviderManagement;
import cloudos.provider.ProviderManagement;
import cloudos.security.LogFilter;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.stereotype.Component;

/**
 * Monitoring the resources (keys, security groups, machine types, images) on the providers Keeps
 * the cloudOS provider elements up-to-date.
 */
@Component
@RunAfterStartup
@Log4j2
class ProviderElementsMonitor extends AbstractProviderMonitor {

  @Autowired
  private AmazonProviderManagement amazonProviderManagement;

  @Autowired
  private GoogleProviderManagement googleProviderManagement;
  
  @Autowired
  @Qualifier(ThreadingConfig.MONITOR)
  private AsyncTaskExecutor taskExecutor;

  private ProviderManagement getManagement(Providers providers) throws NotImplementedException {
    switch (providers) {
      case AMAZON_AWS:
        return amazonProviderManagement;
      case GOOGLE_COMPUTE_ENGINE:
        return googleProviderManagement;
      default:
        break;
    }
    throw new NotImplementedException();
  }

  @Override
  public void run(Providers provider) {
    try {

      log.info("Updating provider elements for provider: {}", provider);

      if (!getCredentialManagement(provider).hasCredential()) {
        log.info("Provider: {} has no credentials yet.", provider);
        return;
      }

      ProviderManagement providerManagement = getManagement(provider);

      // running in parallel but waiting all to finish
      List<Future<?>> threads = new ArrayList<>();

      String logToken = ThreadContext.get(LogFilter.LOG_PATH);
      
      threads.add(taskExecutor.submit(() -> {
        ThreadContext.put(LogFilter.LOG_PATH, logToken);
        providerManagement.syncRegions();
        ThreadContext.remove(LogFilter.LOG_PATH);
      }));
      threads.add(taskExecutor.submit(() -> {
        ThreadContext.put(LogFilter.LOG_PATH, logToken);
        providerManagement.syncMachineTypes();
        ThreadContext.remove(LogFilter.LOG_PATH);
      }));
      threads.add(taskExecutor.submit(() -> {
        ThreadContext.put(LogFilter.LOG_PATH, logToken);
        providerManagement.syncImagesTypes();
        ThreadContext.remove(LogFilter.LOG_PATH);
      }));
      threads.add(taskExecutor.submit(() -> {
        ThreadContext.put(LogFilter.LOG_PATH, logToken);
        providerManagement.syncKeys();
        ThreadContext.remove(LogFilter.LOG_PATH);
      }));
      threads.add(taskExecutor.submit(() -> {
        ThreadContext.put(LogFilter.LOG_PATH, logToken);
        providerManagement.syncSecurityGroups();
        ThreadContext.remove(LogFilter.LOG_PATH);
      }));
      
      // wait for all threads to get done
      Instant time = Instant.now();
      while (threads.stream().filter(t -> !t.isDone()).count() > 0) {
        if (Duration.between(time, Instant.now()).toMillis() > 2000) {
          time = Instant.now();
          log.info(
              "Waiting threads to finish! Threads: {}(waiting) / {}(total)",
              threads.stream().filter(t -> !t.isDone()).count(),
              threads.size());
        }
      }

    } catch (NotImplementedException e) {
      log.info("Provider: {} not implemented yet.", provider);
    } catch (Exception e) {
      log.error("Error updating provider elements for provider: {}", provider, e);
    }
  }
}
