package cloudos.monitoring.cloud;

import cloudos.Providers;
import cloudos.exceptions.NotImplementedException;
import cloudos.monitoring.AbstractProviderMonitor;
import cloudos.pricing.PricingManagementService;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Monitoring the pricing on the providers.
 */
@Component
@Log4j2
class PricingMonitor extends AbstractProviderMonitor {

  @Autowired
  private PricingManagementService pricingManagementService;

  /*
   * (non-Javadoc)
   *
   * @see cloudos.monitoring.AbstractProviderMonitor#run(cloudos.Providers)
   */
  @Override
  public void run(Providers provider) {
    try {
      log.info("Retrieving pricing for provider: {}", provider);
      pricingManagementService.updatePricingForProvider(provider);
    } catch (NotImplementedException e) {
      log.info("Provider: {} not implemented yet.", provider);
    } catch (Exception e) {
      log.error("Error retrieving pricing for provider: {}", provider, e);
    }
  }
}
