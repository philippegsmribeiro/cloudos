package cloudos.monitoring.cloud;

import cloudos.Providers;
import cloudos.data.InstanceDataMetric;
import cloudos.exceptions.NotImplementedException;
import cloudos.models.CloudosDatapoint;
import cloudos.models.CloudosDatapointsRepository;
import cloudos.monitoring.AbstractProviderMonitor;
import cloudos.queue.QueueManagerService;
import cloudos.resources.AmazonResourcesService;
import cloudos.resources.GoogleResourcesService;
import cloudos.resources.ResourcesManagement;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/** Monitor to get the instance datapoints from the provider, storing them on the mongo. */
@Component
@Log4j2
class DatapointsMonitor extends AbstractProviderMonitor {

  @Autowired
  private AmazonResourcesService amazonInstanceManagement;
  
  @Autowired
  private GoogleResourcesService googleInstanceManagement;
  
  @Autowired
  private CloudosDatapointsRepository cloudosDatapointsRepository;
  
  @Autowired
  private QueueManagerService queueManagerService;
  

  public static final Long WINDOW = (long) (1000 * 120 * 1);
  public static final Integer PERIOD = 60;

  private ResourcesManagement getInstanceManagement(Providers providers)
      throws NotImplementedException {
    switch (providers) {
      case AMAZON_AWS:
        return amazonInstanceManagement;
      case GOOGLE_COMPUTE_ENGINE:
        return googleInstanceManagement;
      default:
        break;
    }
    throw new NotImplementedException();
  }
  
  /**
   * For each provider, go to the provider and get the datapoints for the available base metrics and
   * save them on the database.
   */
  @Override
  public void run(Providers provider) {
    try {
      
      log.info("Getting datapoints for provider: {}", provider);
      
      if (!getCredentialManagement(provider).hasCredential()) {
        log.info("Provider: {} has no credentials yet.", provider);
        return;
      }
      
      List<CloudosDatapoint> datapoints =
          getInstanceManagement(provider)
              .findAllDatapoints(
                  WINDOW,
                  PERIOD,
                  InstanceDataMetric.CPU_UTILIZATION,
                  InstanceDataMetric.DISK_READ_BYTES,
                  InstanceDataMetric.DISK_WRITE_BYTES,
                  InstanceDataMetric.NETWORK_RECEIVED_BYTES,
                  InstanceDataMetric.NETWORK_SENT_BYTES);
      if (CollectionUtils.isNotEmpty(datapoints)) {
        queueManagerService.sendDatapointMessage(datapoints);
        cloudosDatapointsRepository.save(datapoints);
        log.info("Found {} datapoints for provider: {}", datapoints.size(), provider);
      } else {
        log.info("Datapoints not found for provider: {}", provider);
      }
    } catch (NotImplementedException e) {
      log.info("Provider: {} not implemented yet.", provider);
    } catch (Exception e) {
      log.error("Error updating status for provider: {}", provider, e);
    }
  }
}
