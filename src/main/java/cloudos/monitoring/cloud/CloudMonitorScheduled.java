package cloudos.monitoring.cloud;

import cloudos.Application;
import cloudos.config.NotLoadOnTestCondition;
import cloudos.config.ThreadingConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Conditional;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * Schedule bean Entry point for all the monitoring services TODO: add the performance threads, etc.
 */
@Service
@Conditional(NotLoadOnTestCondition.class)
public class CloudMonitorScheduled {

  @Autowired
  @Qualifier(ThreadingConfig.MONITOR)
  AsyncTaskExecutor taskExecutor;

  @Autowired
  InstancesMonitor instancesMonitor;

  /**
   * Instances Monitor Monitoring the instances on the providers Keeps the cloudOS instances
   * up-to-date.
   */
  @Scheduled(cron = "${cloudos.scheduling.instancesMonitor}")
  public void runInstancesMonitor() {
    if (Application.STARTED.get()) {
      taskExecutor.execute(instancesMonitor);
    }
  }

  @Autowired
  ProviderElementsMonitor providerElementsMonitor;

  /**
   * ProviderElements Monitor Monitoring the resources (keys, security groups, machine types,
   * images) on the providers Keeps the cloudOS provider elements up-to-date.
   */
  @Scheduled(cron = "${cloudos.scheduling.providerElementsMonitor}")
  public void runProviderElementsMonitor() {
    if (Application.STARTED.get()) {
      taskExecutor.execute(providerElementsMonitor);
    }
  }

  @Autowired
  InstanceAnalyzerMonitor instanceAnalyzerMonitor;

  /**
   * Instance Analyzer Task responsible to verify and analyze if an instance becomes unresponsive.
   * TODO: enable this when machine learned finished
   */
  // @Scheduled(cron = "${cloudos.scheduling.instanceAnalyzerMonitor}")
  public void runInstanceAnalyzerMonitor() {
    if (Application.STARTED.get()) {
      taskExecutor.execute(instanceAnalyzerMonitor);
    }
  }

  @Autowired
  DatapointsMonitor datapointsMonitor;

  /**
   * Datapoints Monitor Monitor to get the instance datapoints from the provider, storing them on
   * the mongo.
   */
  @Scheduled(cron = "${cloudos.scheduling.datapointsMonitor}")
  public void runDatapointsMonitor() {
    if (Application.STARTED.get()) {
      taskExecutor.execute(datapointsMonitor);
    }
  }

  @Autowired
  PricingMonitor pricingMonitor;

  /**
   * Pricing Monitor Monitor to get the pricing from the provider, storing them on the mongo.
   */
  @Scheduled(cron = "${cloudos.scheduling.pricingMonitor}")
  public void runPricingMonitor() {
    if (Application.STARTED.get()) {
      taskExecutor.execute(pricingMonitor);
    }
  }
}
