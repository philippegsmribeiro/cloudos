package cloudos.monitoring.cloud;

import cloudos.Providers;
import cloudos.machinelearning.tasks.InstanceAnalyzerTask;
import cloudos.monitoring.AbstractProviderMonitor;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/** Monitor for the Instance Analyzer. */
@Component
@Log4j2
class InstanceAnalyzerMonitor extends AbstractProviderMonitor {

  @Autowired 
  private InstanceAnalyzerTask instanceAnalyzerTask;

  @Override
  public void run(Providers provider) {
    log.info("Calling the analyzer for {}.", provider);
    instanceAnalyzerTask.analyze(provider);
  }
}
