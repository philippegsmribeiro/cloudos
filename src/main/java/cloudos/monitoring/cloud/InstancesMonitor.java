package cloudos.monitoring.cloud;

import cloudos.Providers;
import cloudos.exceptions.NotImplementedException;
import cloudos.instances.AmazonInstanceManagement;
import cloudos.instances.GoogleInstanceManagement;
import cloudos.instances.InstanceManagement;
import cloudos.monitoring.AbstractProviderMonitor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/** Monitoring the instances on the providers Keeps the cloudOS instances up-to-date. */
@Component
@Log4j2
class InstancesMonitor extends AbstractProviderMonitor {

  @Autowired
  private AmazonInstanceManagement amazonInstanceManagement;

  @Autowired
  private GoogleInstanceManagement googleInstanceManagement;

  private InstanceManagement<?> getInstanceManagement(Providers providers)
      throws NotImplementedException {
    switch (providers) {
      case AMAZON_AWS:
        return amazonInstanceManagement;
      case GOOGLE_COMPUTE_ENGINE:
        return googleInstanceManagement;
      default:
        break;
    }
    throw new NotImplementedException();
  }

  @Override
  public void run(Providers provider) {
    try {
      log.info("Updating status for provider: {}", provider);

      if (!getCredentialManagement(provider).hasCredential()) {
        log.info("Provider: {} has no credentials yet.", provider);
        return;
      }

      getInstanceManagement(provider).syncInstances();
      log.info("Syncing end for provider: {}", provider);
      
    } catch (NotImplementedException e) {
      log.info("Provider: {} not implemented yet.", provider);
    } catch (Exception e) {
      log.error("Error updating status for provider: {}", provider, e);
    }
  }
}
