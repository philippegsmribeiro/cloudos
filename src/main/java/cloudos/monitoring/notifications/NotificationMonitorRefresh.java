package cloudos.monitoring.notifications;

import cloudos.monitoring.AbstractMonitor;
import cloudos.notifications.NotificationsService;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Monitor the notifications and runs periodically at every 30 minutes, Looks for notifications to
 * change to read status or deletion.
 *
 */
@Component
@Log4j2
class NotificationMonitorRefresh extends AbstractMonitor {

  @Autowired
  private NotificationsService notificationsService;

  /**
   * Update the notification information. This job will run in the background of the application,
   * keeping the data always up to date.
   */
  @Override
  public void runTask() {
    log.info("============= Processing {} Notication =================");
    try {
      notificationsService.processDeletionNotification();
    } catch (Exception e) {
      log.error("Error caught while Processing Notications " + e.getMessage());
    }
    log.info("==========================================================");
  }
}
