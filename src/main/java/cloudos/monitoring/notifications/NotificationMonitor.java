package cloudos.monitoring.notifications;

import cloudos.Application;
import cloudos.config.NotLoadOnTestCondition;
import cloudos.config.ThreadingConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Conditional;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * Monitor the notifications and runs periodically at every 30 minutes, Looks for notifications to
 * change to read status or deletion.
 *
 */
@Service
@Conditional(NotLoadOnTestCondition.class)
public class NotificationMonitor {

  @Autowired
  @Qualifier(ThreadingConfig.MONITOR)
  private AsyncTaskExecutor taskExecutor;

  @Autowired
  private NotificationMonitorRefresh notificationMonitorRefresh;

  /**
   * Update the notification information. This job will run in the background of the application,
   * keeping the data always up to date.
   */
  @Scheduled(cron = "${cloudos.notification.refresh}")
  public void processNotification() {
    if (Application.STARTED.get()) {
      this.taskExecutor.execute(notificationMonitorRefresh);
    }
  }
}
