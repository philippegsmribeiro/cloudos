package cloudos.monitoring.costanalysis;

import cloudos.costanalysis.DailyResourceCostJob;
import cloudos.costanalysis.DailyResourceCostService;
import cloudos.costanalysis.GenericBillingReportRepository;
import cloudos.monitoring.AbstractMonitor;
import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
class ProviderDailyResourceCostMonitor extends AbstractMonitor {

  private DailyResourceCostJob job;

  @Autowired
  public ProviderDailyResourceCostMonitor(
      GenericBillingReportRepository genericBillingReportRepository,
      DailyResourceCostService dailyResourceCostService) {
    this.job = new DailyResourceCostJob(genericBillingReportRepository, dailyResourceCostService);
  }

  @Override
  public void runTask() {
    LocalDate endDate = LocalDate.now();
    LocalDate startDate = endDate.minusDays(1);
    job.execute(startDate, endDate);
  }
}