package cloudos.monitoring.costanalysis;

import cloudos.costanalysis.GenericBillingReportRepository;
import cloudos.costanalysis.ProviderDailyCostEntryRepository;
import cloudos.costanalysis.ProviderDailyCostJob;
import cloudos.monitoring.AbstractMonitor;
import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
class ProviderDailyCostMonitor extends AbstractMonitor {

  private ProviderDailyCostJob job;

  @Autowired
  public ProviderDailyCostMonitor(GenericBillingReportRepository genericBillingReportRepository,
      ProviderDailyCostEntryRepository providerDailyCostEntryRepository) {
    this.job =
        new ProviderDailyCostJob(genericBillingReportRepository, providerDailyCostEntryRepository);
  }

  @Override
  public void runTask() {
    LocalDate endDate = LocalDate.now();
    LocalDate startDate = endDate.minusDays(1);
    job.updateCost(startDate, endDate);
  }
}