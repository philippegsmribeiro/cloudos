package cloudos.monitoring.costanalysis;

import cloudos.Application;
import cloudos.config.NotLoadOnTestCondition;
import cloudos.config.ThreadingConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Conditional;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * Cost analysis related monitor jobs, which runs periodically.
 */
@Service
@Conditional(NotLoadOnTestCondition.class)
public class CostAnalysisMonitor {
  
  @Autowired
  @Qualifier(ThreadingConfig.MONITOR)
  AsyncTaskExecutor taskExecutor;

  @Autowired
  ProviderDailyCostMonitor providerDailyCostMonitor;

  /**
   * Job that updates provider daily cost data.
   */
  @Scheduled(cron = "${cloudos.costanalysis.monitor.providerdailycost}")
  public void runProviderDailyCostMonitor() {
    if (Application.STARTED.get()) {
      taskExecutor.execute(providerDailyCostMonitor);
    }
  }
  
  @Autowired
  ProviderDailyResourceCostMonitor providerDailyResourceCostMonitor;
  
  /**
   * Job that updates the daily resource cost data.
   */
  @Scheduled(cron = "${cloudos.costanalysis.monitor.dailyresourcecost}")
  public void runProviderDailyResourceCostMonitor() {
    if (Application.STARTED.get()) {
      taskExecutor.execute(providerDailyResourceCostMonitor);
    }
  }
  
  @Autowired
  ProviderTrendMonitor providerTrendMonitor;
  
  /**
   * Job that updates provider trend data.
   */
  @Scheduled(cron = "${cloudos.costanalysis.monitor.providertrend}")
  public void runProviderTrendMonitor() {
    if (Application.STARTED.get()) {
      taskExecutor.execute(providerTrendMonitor);
    }
  }
}