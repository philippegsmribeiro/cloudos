package cloudos.monitoring.costanalysis;

import cloudos.costanalysis.GenericBillingReportRepository;
import cloudos.costanalysis.ProviderTrendEntryRepository;
import cloudos.costanalysis.ProviderTrendJob;
import cloudos.monitoring.AbstractMonitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
class ProviderTrendMonitor extends AbstractMonitor {

  private ProviderTrendJob job;

  @Autowired
  public ProviderTrendMonitor(GenericBillingReportRepository genericBillingReportRepository,
      ProviderTrendEntryRepository providerTrendEntryRepository) {
    this.job = new ProviderTrendJob(genericBillingReportRepository, providerTrendEntryRepository);
  }

  @Override
  public void runTask() {
    job.execute();
  }
}