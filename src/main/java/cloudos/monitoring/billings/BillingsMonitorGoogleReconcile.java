package cloudos.monitoring.billings;

import cloudos.costanalysis.GoogleBillingsReconciliationService;
import cloudos.monitoring.AbstractMonitor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Update the billings information by polling from the source buckets as specified. This job will
 * run in the background of the application, keeping the data always up to date.
 */
@Component
@Log4j2
class BillingsMonitorGoogleReconcile extends AbstractMonitor {

  @Autowired
  private GoogleBillingsReconciliationService googleBillingsReconciliationService;

  @Value("${cloudos.billings.limitindays:90}")
  private Integer limitInDays;

  @Override
  public void runTask() {
    try {
      log.info("Scheduled Job: Reconciling google billing reports for {} days", limitInDays);
      googleBillingsReconciliationService.reconcile();
      log.info("Finished Scheduled Job: Reconciling google billing reports");
    } catch (Exception e) {
      log.error(e.getLocalizedMessage(), e);
    }
  }
}
