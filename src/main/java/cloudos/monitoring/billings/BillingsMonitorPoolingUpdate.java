package cloudos.monitoring.billings;

import cloudos.Providers;
import cloudos.billings.BillingFileProcessor;
import cloudos.billings.BillingFileProcessorFactory;
import cloudos.monitoring.AbstractProviderMonitor;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Update the billings information by polling from the source buckets as specified. This job will
 * run in the background of the application, keeping the data always up to date.
 */
@Component
@Log4j2
class BillingsMonitorPoolingUpdate extends AbstractProviderMonitor {

  @Autowired
  private BillingFileProcessorFactory billingFileProcessorFactory;

  @Override
  public void run(Providers provider) {
    log.info("============= Polling {} billing reports =================",
        String.valueOf(provider));
    try {
      BillingFileProcessor billingFileProcessor =
          billingFileProcessorFactory.getBillingFileProcessor(provider);
      if (billingFileProcessor != null) {
        billingFileProcessor.poll();
      } else {
        log.warn("Billing file processor not implemented for provider {}",
            String.valueOf(provider));
      }
    } catch (Exception e) {
      log.error("Error caught while Processing billing reports " + e.getMessage());
    }
    log.info("==========================================================");
  }
}
