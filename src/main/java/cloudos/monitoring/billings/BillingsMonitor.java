package cloudos.monitoring.billings;

import cloudos.Application;
import cloudos.config.NotLoadOnTestCondition;
import cloudos.config.ThreadingConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Conditional;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * Created by philipperibeiro on 6/29/17. Monitor the billings accounts and runs periodically at
 * every 60 minutes, attempting to find and process billings files that haven't being processed yet.
 * It will update the respective billings collections with the information obtained.
 */
@Service
@Conditional(NotLoadOnTestCondition.class)
public class BillingsMonitor {

  @Autowired
  @Qualifier(ThreadingConfig.MONITOR)
  private AsyncTaskExecutor taskExecutor;

  @Autowired
  BillingsMonitorPoolingUpdate billingsMonitorPoolingUpdate;

  /**
   * Update the billings information by polling from the source buckets as specified. This job will
   * run in the background of the application, keeping the data always up to date.
   */
  @Scheduled(cron = "${cloudos.billings.polling.update}")
  public void updateBillings() {
    if (Application.STARTED.get()) {
      this.taskExecutor.execute(billingsMonitorPoolingUpdate);
    }
  }

  @Autowired
  private BillingsMonitorGoogleReconcile billingsMonitorGoogleReconcile;

  /**
   * Update the billings information by polling from the source buckets as specified. This job will
   * run in the background of the application, keeping the data always up to date.
   */
  @Scheduled(cron = "${cloudos.billings.google.reconcile}")
  public void updateGoogleReconciliationEntries() {
    if (Application.STARTED.get()) {
      this.taskExecutor.execute(billingsMonitorGoogleReconcile);
    }
  }
}
