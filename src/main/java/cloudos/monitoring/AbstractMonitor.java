package cloudos.monitoring;

import cloudos.config.ThreadingConfig;
import cloudos.security.LogFilter;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.Future;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.AsyncTaskExecutor;

/**
 * Abstract class for the monitor TODO: add the performance threads, etc.
 */
@Log4j2
public abstract class AbstractMonitor implements Runnable {

  private static final int ESTIMATED_TIME_30_SECONDS = 30;

  @Autowired
  @Qualifier(ThreadingConfig.MONITOR)
  AsyncTaskExecutor taskExecutor;

  Future<?> runningTask = null;
  
  /**
   * Returns the max estimated time (in seconds) to complete the task.
   * If task takes longer than that it will throw an error on the logs.
   * @return time in seconds
   */
  protected double getTaskMaxEstimatedTime() {
    return ESTIMATED_TIME_30_SECONDS;
  }

  /*
   * (non-Javadoc)
   *
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    // set log path
    String simpleName = this.getClass().getSimpleName();
    ThreadContext.put(LogFilter.LOG_PATH, simpleName);
    if (runningTask != null && !runningTask.isDone()) {
      log.info("# Already running - ignoring");
      return;
    }

    runningTask = taskExecutor.submit(() -> {
      Instant now = Instant.now();
      try {
        // set log path
        ThreadContext.put(LogFilter.LOG_PATH, simpleName);
        log.info("# Starting cloud-monitor");
        runTask();
      } catch (Exception e) {
        log.error("# Error cloud-monitor", e);
      } finally {
        double duration = new Double(Duration.between(now, Instant.now()).toMillis()) / 1000;
        if (duration > getTaskMaxEstimatedTime()) {
          log.error("# Ending cloud-monitor! MONITOR_TIME: {} s - Revalidate it.", duration);
        } else {
          log.info("# Ending cloud-monitor! MONITOR_TIME: {} s", duration);
        }
        ThreadContext.remove(LogFilter.LOG_PATH);
      }
    });
  }

  /**
   * Run the monitor task.
   * 
   * @throws Exception if something goes wrong
   */
  protected abstract void runTask() throws Exception;

  /**
   * Verify if monitoring is running.
   *
   * @return true if it is running
   */
  public boolean isRunning() {
    return runningTask != null && !runningTask.isDone();
  }
}
