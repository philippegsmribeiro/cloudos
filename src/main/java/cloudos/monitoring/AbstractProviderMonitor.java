package cloudos.monitoring;

import cloudos.Providers;
import cloudos.config.ThreadingConfig;
import cloudos.credentials.AmazonCredentialManagement;
import cloudos.credentials.AzureCredentialManagement;
import cloudos.credentials.CredentialManagement;
import cloudos.credentials.GoogleCredentialManagement;
import cloudos.exceptions.NotImplementedException;
import cloudos.provider.ProvidersService;
import cloudos.security.LogFilter;
import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.AsyncTaskExecutor;

/**
 * Abstract class for the monitor which splits the providers TODO: add the performance threads, etc.
 */
@Log4j2
public abstract class AbstractProviderMonitor implements Runnable {

  private static final int ESTIMATED_TIME_30_SECONDS = 30;

  @Autowired
  @Qualifier(ThreadingConfig.MONITOR)
  AsyncTaskExecutor taskExecutor;
  
  @Autowired
  private AmazonCredentialManagement amazonCredentialManagement;

  @Autowired
  private AzureCredentialManagement azureCredentialManagement;

  @Autowired
  private GoogleCredentialManagement googleCredentialManagement;
  
  @Autowired
  protected ProvidersService providersService;
  
  private Map<Providers, Future<?>> runningMap = new HashMap<>();

  protected CredentialManagement<?> getCredentialManagement(Providers providers)
      throws NotImplementedException {
    switch (providers) {
      case AMAZON_AWS:
        return amazonCredentialManagement;
      case GOOGLE_COMPUTE_ENGINE:
        return googleCredentialManagement;
      case MICROSOFT_AZURE:
        return azureCredentialManagement;
      default:
        break;
    }
    throw new NotImplementedException();
  }  
  
  /**
   * Returns the max estimated time (in seconds) to complete the task.
   * If task takes longer than that it will throw an error on the logs.
   * @param providers of the task
   * @return time in seconds
   */
  protected double getTaskMaxEstimatedTime(Providers providers) {
    return ESTIMATED_TIME_30_SECONDS;
  }
  
  /*
   * (non-Javadoc)
   *
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    // set log path
    String simpleName = this.getClass().getSimpleName();
    ThreadContext.put(LogFilter.LOG_PATH, simpleName);
    log.info("Monitor Started Running!");

    // for each provider
    for (Providers provider : providersService.retrieveSupportedProviders()) {

      Future<?> oldFuture = runningMap.get(provider);
      if (oldFuture != null && !oldFuture.isDone()) {
        log.info("Monitor Already running for {} - ignoring", provider);
        continue;
      }

      Future<?> future = taskExecutor.submit(new Runnable() {
        @Override
        public void run() {
          Instant now = Instant.now();
          try {
            ThreadContext.put(LogFilter.LOG_PATH, simpleName + provider);
            log.info("# Starting cloud-monitor for provider {}", provider);
            AbstractProviderMonitor.this.run(provider);
          } catch (Exception e) {
            log.error("# Error cloud-monitor for provider {}", provider, e);
          } finally {
            double duration = new Double(Duration.between(now, Instant.now()).toMillis()) / 1000;
            if (duration > getTaskMaxEstimatedTime(provider)) {
              log.error(
                  "# Ending cloud-monitor for provider {}! MONITOR_TIME: {} s - Revalidate it!",
                  provider, duration);
            } else {
              log.info("# Ending cloud-monitor for provider {}! MONITOR_TIME: {} s", provider,
                  duration);
            }
            ThreadContext.remove(LogFilter.LOG_PATH);
          }
        }
      });
      runningMap.put(provider, future);
    }

    log.info("Monitor Stoped Running!");
    ThreadContext.remove(LogFilter.LOG_PATH);
  }

  /**
   * Run the monitor on the provider.
   *
   * @param provider to run
   */
  protected abstract void run(Providers provider);

  /**
   * Verify if monitoring is running.
   *
   * @return true if it is running
   */
  public boolean isRunning() {
    Collection<Future<?>> values = runningMap.values();
    for (Future<?> future : values) {
      if (!future.isDone()) {
        return true;
      }
    }
    return false;
  }
}
