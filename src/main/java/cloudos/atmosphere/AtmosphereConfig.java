package cloudos.atmosphere;

import cloudos.config.NotLoadOnTestCondition;
import cloudos.security.WebSecurityConfig;
import org.atmosphere.cpr.AtmosphereFramework;
import org.atmosphere.cpr.AtmosphereServlet;
import org.atmosphere.cpr.BroadcasterFactory;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

@Configuration
@Conditional(NotLoadOnTestCondition.class)
public class AtmosphereConfig {

  /**
   * Bean registration for the atmosphere servlet.
   *
   * @return ServletRegistrationBean
   */
  @Bean
  public ServletRegistrationBean atmosphereServletRegistration() {
    // configure the atmosphere servlet to the path
    ServletRegistrationBean registration =
        new ServletRegistrationBean(atmosphereServlet(), WebSecurityConfig.WS_PATH + "/*");
    registration.setLoadOnStartup(1);
    registration.setOrder(Ordered.HIGHEST_PRECEDENCE);
    return registration;
  }

  @Bean
  public AtmosphereServlet atmosphereServlet() {
    return new AtmosphereServlet();
  }

  @Bean(destroyMethod = "destroy")
  public AtmosphereFramework atmosphereFramework() {
    return atmosphereServlet().framework();
  }

  @Bean
  public BroadcasterFactory broadcasterFactory() {
    return atmosphereFramework().getAtmosphereConfig().getBroadcasterFactory();
  }
}
