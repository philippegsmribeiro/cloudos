package cloudos.atmosphere;

import cloudos.security.WebSecurityConfig;

import java.io.IOException;

import lombok.extern.log4j.Log4j2;

import org.atmosphere.config.service.Disconnect;
import org.atmosphere.config.service.ManagedService;
import org.atmosphere.config.service.Ready;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.atmosphere.cpr.AtmosphereResponse;
import org.atmosphere.cpr.Broadcaster;
import org.atmosphere.handler.OnMessage;

@ManagedService(path = WebSecurityConfig.WS_PATH)
@Log4j2
public class AtmosphereManageService extends OnMessage<String> {

  /**
   * When the resource gets ready.
   *
   * @param r atmoshpere resource
   */
  @Ready
  public void onReady(final AtmosphereResource r) {
    String userName = getUserInfo(r);
    log.info("User {} connected.", userName);
    // create a new broadcaster for the user - it allows sending message just for the user
    Broadcaster lookup = r.getAtmosphereConfig().getBroadcasterFactory().lookup(userName, true);
    r.setBroadcaster(lookup);
    lookup.addAtmosphereResource(r);
  }

  /**
   * When the resource gets disconnected.
   *
   * @param event of disconnecting
   */
  @Disconnect
  public void onDisconnect(AtmosphereResourceEvent event) {
    if (event.isCancelled()) {
      log.info("Browser {} unexpectedly disconnected", event.getResource().uuid());
    } else if (event.isClosedByClient()) {
      log.info("Browser {} closed the connection", event.getResource().uuid());
    }
    event.getResource().removeFromAllBroadcasters();
  }

  @Override
  public void onMessage(AtmosphereResponse r, String message) throws IOException {
    log.info("{} just send {}", r.uuid(), message);
  }

  /**
   * Get the user info for the broadcast.
   *
   * @param r resource
   * @return String with user name
   */
  private String getUserInfo(AtmosphereResource r) {
    return r.getRequest().getRequestURI().replaceAll(WebSecurityConfig.WS_PATH + "/", "");
  }
}
