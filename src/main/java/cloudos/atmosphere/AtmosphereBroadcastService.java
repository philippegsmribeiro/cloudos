package cloudos.atmosphere;

import cloudos.broadcast.BroadcastObject;
import cloudos.broadcast.BroadcastType;
import cloudos.config.NotLoadOnTestCondition;
import cloudos.queue.QueueListener;
import cloudos.queue.message.DeploymentMessage.DeploymentMessageConsumer;
import cloudos.queue.message.InstanceCreatedMessage.InstanceCreatedMessageConsumer;
import cloudos.queue.message.InstanceDeletedMessage.InstanceDeletedMessageConsumer;
import cloudos.queue.message.InstanceUpdatedMessage.InstanceUpdatedMessageConsumer;
import cloudos.queue.message.NotificationMessage.NotificationMessageConsumer;
import cloudos.security.WebSecurityConfig;
import cloudos.user.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.annotation.PostConstruct;
import lombok.extern.log4j.Log4j2;
import org.atmosphere.cpr.Broadcaster;
import org.atmosphere.cpr.BroadcasterFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Service;

@Service
@Conditional(NotLoadOnTestCondition.class)
@Log4j2
public class AtmosphereBroadcastService {

  @Autowired BroadcasterFactory broadcasterFactory;
  @Autowired QueueListener queueListener;

  /** Post constructor method to set up the listeners. */
  @PostConstruct
  public void setUpListener() {
    // add listener to the queue
    queueListener.addConsumer(
        (DeploymentMessageConsumer) t -> broadcastForAll(
            new BroadcastObject(
                BroadcastType.DEPLOYMENTS, t.getDescription(), t.getDeployment())));

    queueListener.addConsumer(
        (NotificationMessageConsumer) t -> broadcastForAll(
            new BroadcastObject(
                BroadcastType.NOTIFICATION, t.getDescription(), t.getNotification())));

    queueListener.addConsumer(
        (InstanceCreatedMessageConsumer) t -> broadcastForAll(
            new BroadcastObject(
                BroadcastType.INSTANCE_CREATED, t.getDescription(), t.getInstance())));

    queueListener.addConsumer(
        (InstanceUpdatedMessageConsumer) t -> broadcastForAll(
            new BroadcastObject(
                BroadcastType.INSTANCE_UPDATED, t.getDescription(), t.getNewInstance())));

    queueListener.addConsumer(
        (InstanceDeletedMessageConsumer) t -> broadcastForAll(
            new BroadcastObject(
                BroadcastType.INSTANCE_DELETED, t.getDescription(), t.getInstance())));
  }

  @Autowired
  ObjectMapper mapper;

  /**
   * Broadcast an object to all users/browsers connected.
   *
   * @param broadcastObject object to broadcast
   */
  public void broadcastForAll(BroadcastObject broadcastObject) {
    this.broadcastObject(WebSecurityConfig.WS_PATH, broadcastObject);
  }

  /**
   * Broadcast an object to a specific user.
   *
   * @param user to broadcast
   * @param broadcastObject object
   */
  public void broadcastForUser(User user, BroadcastObject broadcastObject) {
    this.broadcastObject(user.getUsername(), broadcastObject);
  }

  /**
   * Internal method to do the broadcast.
   *
   * @param broadcasterLookkupId id
   * @param broadcastObject object
   */
  private void broadcastObject(String broadcasterLookkupId, BroadcastObject broadcastObject) {
    try {
      log.info(
          "Broadcasting to: {} type: {} message: {} data: {}",
          broadcasterLookkupId,
          broadcastObject.getType(),
          broadcastObject.getMessage(),
          mapper.writeValueAsString(broadcastObject.getObject()));
      Broadcaster broadcaster = broadcasterFactory.lookup(broadcasterLookkupId);
      if (broadcaster != null) {
        String writeValueAsString = mapper.writeValueAsString(broadcastObject);
        broadcaster.broadcast(writeValueAsString);
      }
    } catch (Exception e) {
      log.error("Broadcasting failed!", e);
    }
  }
}
