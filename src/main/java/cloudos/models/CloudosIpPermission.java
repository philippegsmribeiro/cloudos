package cloudos.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The InboundRule object will represent the inbound protocol and port rules for instances.
 *
 * @author rogerio.souza
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CloudosIpPermission {

  private CloudosProtocol protocol;
  private String source;
  private Integer portFrom;
  private Integer portTo;

}
