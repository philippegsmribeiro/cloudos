package cloudos.models;

import java.util.Map;

import cloudos.Providers;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CloudCreateRequest {

  private Providers provider;
  private String region;
  private String zone;
  private String name;
  private String machineType;
  private String image;
  private String imageProject;
  private int count;
  private int minCount;
  private String securityGroup;
  private String keyName;
  private Map<String,String> tag;
  private String cloudGroup;
  private boolean spot = false;

  /**
   * Create the request for AWS.
   *
   * @param region the cloud region
   * @param machineType the machine type
   * @param image the instance's image type
   * @param count the number of instances
   * @param minCount the minimum count
   * @return a new CloudCreateRequest object
   */
  public static CloudCreateRequest createAwsRequest(String region, String machineType, String image,
      String securityGroup, int count, int minCount, String keyName, String zone, String name) {
    return new CloudCreateRequest(Providers.AMAZON_AWS, region, zone, name, machineType, image,
        null, count, minCount, securityGroup, keyName, null, null, false);
  }

  /**
   * Create the request for GCloud.
   *
   * @param name the name of the instance
   * @param region the cloud region
   * @param machineType the machine type
   * @param image the instance's image type
   * @param imageProject the name of the image project
   * @param count the number of instances
   * @param keyName the name of the key to be used
   * @param zone the cloud region availability zone
   * @return a new CloudCreateRequest object
   */
  public static CloudCreateRequest createGCloudRequest(String region, String name,
      String machineType, String image, String imageProject, int count, String keyName,
      String zone) {
    return new CloudCreateRequest(Providers.GOOGLE_COMPUTE_ENGINE, region, zone, name, machineType,
        image, imageProject, count, count, null, keyName, null, null, false);
  }
}
