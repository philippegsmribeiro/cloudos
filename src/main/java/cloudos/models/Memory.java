package cloudos.models;

import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by philipperibeiro on 4/7/17.
 *
 * <p>The class Memory defines the memory usage of each instance, for a given time window.
 */
@Document(collection = "healthchecker_memory")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Memory {

  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  public String id;

  public String instance;
  public Map<String, Object> virtualMemory;
  public Map<String, Object> swapMemory;

}
