package cloudos.models.services;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.EnumPath;
import com.querydsl.core.types.dsl.StringPath;
import javax.annotation.Generated;

/**
 * QCloudosService is a Querydsl query type for CloudosService.
 * 
 * @author Alex Calagua
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCloudosService extends EntityPathBase<CloudosService> {

  private static final long serialVersionUID = -687478915L;

  public static final QCloudosService cloudosService = new QCloudosService("cloudosService");

  public final DateTimePath<java.util.Date> created =
      createDateTime("created", java.util.Date.class);

  public final StringPath id = createString("id");

  public final StringPath name = createString("name");

  public final StringPath code = createString("code");

  public final EnumPath<cloudos.Providers> providers =
      createEnum("providers", cloudos.Providers.class);

  public final EnumPath<ServiceCategory> serviceCategory =
      createEnum("serviceCategory", ServiceCategory.class);


  public final EnumPath<ServiceStatus> serviceStatus =
      createEnum("serviceStatus", ServiceStatus.class);

  public QCloudosService(String variable) {
    super(CloudosService.class, forVariable(variable));
  }

  public QCloudosService(Path<? extends CloudosService> path) {
    super(path.getType(), path.getMetadata());
  }

  public QCloudosService(PathMetadata metadata) {
    super(CloudosService.class, metadata);
  }

}

