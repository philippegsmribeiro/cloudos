package cloudos.models.services;

/**
 * Exception when a action request on an instance fails.
 * 
 * @author Alex Calagua
 * 
 */
public class ServiceException extends Exception {

  private static final long serialVersionUID = -6283521504310814784L;

  public ServiceException() {
    super();
  }

  public ServiceException(String message) {
    super(message);
  }
}
