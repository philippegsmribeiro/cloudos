package cloudos.models.services;

import com.querydsl.core.annotations.QueryEntity;
import cloudos.Providers;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * The CloudosService class is responsible for serving as the model for the service in mongoDB.
 *
 * @author Alex Calagua
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@QueryEntity
@Builder
@Document(collection = "cloudos_provider_service")
public class CloudosService implements Serializable {
  private static final long serialVersionUID = 1L;
  @Id
  private String id;
  String code;
  Providers providers;
  ServiceCategory serviceCategory;
  String name;
  ServiceStatus serviceStatus;
  Date created;
}
