package cloudos.models.services;

/**
 * Enum used to control the status of a service.
 * 
 * @author Alex Calagua
 *
 */
public enum ServiceStatus {
  ACTIVE,
  INACTIVE
}
