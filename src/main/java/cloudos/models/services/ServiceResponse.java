package cloudos.models.services;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Class representing response in registering, updating and deleting an service.
 *
 * @author Alex Calagua
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ServiceResponse implements Serializable {

  private static final long serialVersionUID = 1L;
  private String response;
  private CloudosService service;

}
