package cloudos.models.services;

/**
 * Enum used to control the services of a category from a provider.
 * 
 * @author Alex Calagua
 *
 */
public enum ServiceCategory {
  
  ANALYTICS("Analytics"),
  
  COMPUTING("Computing"),
  
  MESSAGING("Messaging"),

  MANAGEMENT_TOOLS("Management Tools"),

  NETWORKING("Networking"),
  
  SECURITY_IDENTITY_COMPLIANCE("Security Identity Compliance"),

  STORAGE("Storage"),
  
  SECURITY("Security"),

  MANAGEMENT_SERVICES("Management Services"),
  
  OTHER("Other");
  
  private String description;

  ServiceCategory(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }

}
