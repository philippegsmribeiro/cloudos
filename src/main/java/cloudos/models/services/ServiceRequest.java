package cloudos.models.services;

import cloudos.Providers;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Class representing the request to create and update an service.
 *
 * @author Alex Calagua
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ServiceRequest implements Serializable {

  private static final long serialVersionUID = 1L;
  String id;
  Providers providers;
  String code;
  String name;
  ServiceCategory serviceCategory;
  ServiceStatus serviceStatus;
  Date created;

}
