package cloudos.models.cloudgroup;

import cloudos.Providers;
import cloudos.utils.ReflectionToJson;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * The cloud group filter request is responsible for sending parameters that can be used to
 * filter all instances that match (or not) the particular parameters.
 */
@Builder
@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class CloudGroupFilterRequest {

  private Providers provider;
  private String region;
  private String instanceType;
  private String availabilityZone;
  private String key;
  private String securityGroup;

}
