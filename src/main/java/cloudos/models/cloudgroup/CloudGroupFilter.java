package cloudos.models.cloudgroup;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class CloudGroupFilter {

  private List<String> regions = new ArrayList<>();
  private List<String> zones = new ArrayList<>();
  private List<String> machineTypes = new ArrayList<>();
  private List<String> keys = new ArrayList<>();
  private List<String> securityGroup = new ArrayList<>();
  
}
