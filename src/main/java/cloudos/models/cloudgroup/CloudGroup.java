package cloudos.models.cloudgroup;

import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;

import com.amazonaws.services.inspector.model.ResourceGroupTag;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import cloudos.Providers;
import cloudos.policies.AutoscalerPolicy;
import cloudos.policies.SpotBidderPolicy;
import cloudos.utils.ReflectionToJson;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CloudGroup {

  @Id
  private String id;

  // a cloud group can span across different cloud providers
  @NotNull
  private Providers provider;

  private String name;

  private String arn;

  private Date createdAt;

  // I will use ResourceGroupTag from amazonaws here since it's just a Key:Pair object
  private List<ResourceGroupTag> tags;

  /**
   * the min number of instances in the cloud group.
   */
  @Builder.Default
  private Integer min = 0;

  /**
   * the max number of instances in the cloud group.
   */
  @Builder.Default
  private Integer max = 0;

  private CloudGroupFilter filter;

  @Builder.Default
  private AtomicBoolean isBlocked = new AtomicBoolean(false);

  /* Add the Spot Bidder Policy to the Cloud Group */
  private SpotBidderPolicy spotBidderPolicy;

  /* Add the Autoscaler Policy to the Cloud Group */
  private AutoscalerPolicy autoscalerPolicy;

  @Override
  public String toString() {
    return ReflectionToJson.toString(this);
  }
}
