package cloudos.models.cloudgroup;

import cloudos.Providers;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

interface CloudGroupRepository extends MongoRepository<CloudGroup, String> {

  List<CloudGroup> findByProvider(Providers provider);

  CloudGroup findByName(String name);
}
