package cloudos.models.cloudgroup;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cloudos.Providers;
import cloudos.instances.InstanceService;
import cloudos.models.Instance;
import cloudos.policies.SpotBidderPolicy;

/**
 * The CloudGroup service is responsible for manipulating the cloud groups across the CloudTown
 * platform.
 *
 * @author Philippe Ribeiro
 */
@Service
public class CloudGroupService {
  
  // define the repository that contains the cloud groups.
  @Autowired
  private CloudGroupRepository cloudGroupRepository;
  
  @Autowired
  private InstanceService instanceService;

  /**
   * Create a new CloudGroup, if it does not exist.
   *
   * @param cloudGroup the cloud group object
   * @return the newly created CloudGroup with its id
   */
  public CloudGroup createCloudGroup(@NotNull CloudGroup cloudGroup) {
    if (cloudGroup.getSpotBidderPolicy() != null) {
      // validate the spot bidder percentage
      SpotBidderPolicy spotBidderPolicy = cloudGroup.getSpotBidderPolicy();
      if (spotBidderPolicy.getSpotInstancesPercentage() != null) {
        // check if percentage is between 0.0 <= x <= 1.0
        if (spotBidderPolicy.getSpotInstancesPercentage() < 0 ||
            spotBidderPolicy.getSpotInstancesPercentage() > 1.0) {
          spotBidderPolicy.setSpotInstancesPercentage(0.0);
        }
      }
    }
    // if the cloud group already exists (by its name)
    if (this.cloudGroupRepository.findByName(cloudGroup.getName()) == null) {
      return this.cloudGroupRepository.save(cloudGroup);
    }
    return null;
  }

  /**
   * Update an existing CloudGroup, if it exists.
   *
   * @param cloudGroup the cloud group object
   * @return the updated CloudGroup
   */
  public CloudGroup updateCloudGroup(@NotNull CloudGroup cloudGroup) {
    // if the cloud grouo does not exist (by its name)
    if (this.cloudGroupRepository.findByName(cloudGroup.getName()) != null) {
      return this.cloudGroupRepository.save(cloudGroup);
    }
    return null;
  }

  /**
   * Check if the cloud group is blocked. If blocked, no new instance can be added.
   *
   * @param id the cloud group id
   * @return whether the group is blocked or not
   */
  public boolean isBlocked(@NotNull String id) {
    if (this.cloudGroupRepository.exists(id)) {
      AtomicBoolean isBlocked = this.cloudGroupRepository.findOne(id).getIsBlocked();
      if (isBlocked == null) {
        return false;
      }
      return isBlocked.get();
    }
    return false;
  }

  /**
   * Block or unblock the cloud group from adding any new instances.
   *
   * @param id the cloud group id
   * @param status the block status (true for blocking, false for unblocking)
   * @return the newly updated cloud group
   */
  public synchronized CloudGroup setBlock(@NotNull String id, boolean status) {
    CloudGroup cloudGroup = this.cloudGroupRepository.findOne(id);
    if (cloudGroup != null) {
      AtomicBoolean blocked = cloudGroup.getIsBlocked();
      if (blocked == null) {
        blocked = new AtomicBoolean();
      }
      blocked.set(status);
      cloudGroup.setIsBlocked(blocked);
      
      return cloudGroupRepository.save(cloudGroup);
    }
    return null;
  }
  
  /**
   * Get a list of the cloud groups based on the provider. If the provider is null,
   * return a list of all cloud groups.
   *
   * @param provider the provider the cloud group belongs to
   * @return a list of cloud groups
   */
  public List<CloudGroup> getCloudGroups(Providers provider) {
    // if the provider is null, return all cloud groups.
    if (provider == null) {
      return this.cloudGroupRepository.findAll();
    }
    return this.cloudGroupRepository.findByProvider(provider);
  }
  
  /**
   * Describe a single CloudGroup, if it exists.
   *
   * @param id the id of the CloudGroup
   * @return a CloudGroup if found, otherwise null
   */
  public CloudGroup describeCloudGroup(@NotNull String id) {
    if (this.cloudGroupRepository.exists(id)) {
      return this.cloudGroupRepository.findOne(id);
    }
    return null;
  }
  
  /**
   * Delete a cloud group based on its id, if it exists.
   *
   * @param id the cloud group id
   * @return true if successful, false otherwise
   */
  public boolean deleteCloudGroup(@NotNull String id) {
    // attempt to find the cloud group
    if (this.cloudGroupRepository.exists(id)) {
      this.cloudGroupRepository.delete(id);
      return true;
    }
    // return false if not found
    return false;
  }

  /**
   * Add instance(s) to a cloudGroup.
   *
   * @param cloudGroupId the cloud group id
   * @param instances a list of instances id, by the provider id
   * @return cloudGroup updated
   */
  public CloudGroup addInstanceToCloudGroup(String cloudGroupId, List<String> instances) {
    CloudGroup cloudGroup = this.describeCloudGroup(cloudGroupId);
    if (cloudGroup != null) {
      if (CollectionUtils.isNotEmpty(instances)) {
        List<Instance> instanceList = instanceService.findAllById(instances);
        for (Instance savedInstance : instanceList) {
          // update just the cloudGroupId
          savedInstance.setCloudGroupId(cloudGroupId);
        }
        instanceService.save(instanceList);
        // save the cloud group
      }
      return cloudGroupRepository.save(cloudGroup);
    }
    return null;
  }

  /**
   * Retrieve a cloud group by name.
   * 
   * @param name of cloud group.
   * @return CloudGroup if found.
   */
  public CloudGroup describeCloudGroupByName(String name) {
    return this.cloudGroupRepository.findByName(name);
  }

  /**
   * Remove instance(s) from a cloudGroup.
   * 
   * @param cloudGroupId the cloud group id
   * @param instances a list of instances id, by the provider id
   * @return a CloudGroup without the instances
   */
  public CloudGroup removeInstanceFromCloudGroup(String cloudGroupId, List<String> instances) {
    CloudGroup cloudGroup = this.describeCloudGroup(cloudGroupId);
    if (cloudGroup != null && CollectionUtils.isNotEmpty(instances)) {
      List<Instance> instanceList = instanceService.findAllByCloudGroupId(cloudGroupId);
      // use a set for performance ... lookup if faster
      Set<String> instanceSet = new HashSet<>(instances);
      for (Instance savedInstance : instanceList) {
        if (instanceSet.contains(savedInstance.getProviderId())) {
          // update just the cloudGroupId
          savedInstance.setCloudGroupId(null);
        }
      }
      instanceService.save(instanceList);
      // save the cloud group
      return cloudGroupRepository.save(cloudGroup);
    }
    return null;
  }

  /**
   * Add a cloudGroup Filter to a cloudGroup.
   * 
   * @param cloudGroupId the cloud group id
   * @param cloudGroupFilter the cloud group filter
   * @return the CloudGroup updated with the filters
   */
  public CloudGroup addCloudGroupFilterToCloudGroup(String cloudGroupId,
      CloudGroupFilter cloudGroupFilter) {
    CloudGroup cloudGroup = this.describeCloudGroup(cloudGroupId);
    if (cloudGroup != null && cloudGroupFilter != null) {
      cloudGroup.setFilter(cloudGroupFilter);
      return cloudGroupRepository.save(cloudGroup);
    }
    return null;
  }
  
}
