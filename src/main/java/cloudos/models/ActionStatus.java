package cloudos.models;

public enum ActionStatus {
  NOT_EXECUTED,
  EXECUTED,
  EXECUTED_ASYNCH,
  ERROR
}
