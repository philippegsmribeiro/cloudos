package cloudos.models;

import cloudos.Providers;

/** Created by philipperibeiro on 12/18/16. */
public class AzureSecurityGroup extends CloudosSecurityGroup {

  public AzureSecurityGroup() {
    this.setProvider(Providers.MICROSOFT_AZURE);
  }
}
