package cloudos.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import cloudos.Providers;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Represents the abstraction of target groups to be specialized by provider such as:
 * AmazonTargetGroup, AzureTargetGroup, GoogleTargetGroup
 * 
 * @author Rogério Souza
 *
 */

@Document(collection = "cloudos_target_group")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY,
    property = "providerType", defaultImpl = AmazonTargetGroup.class)
@JsonSubTypes({@JsonSubTypes.Type(value = AmazonTargetGroup.class, name = "AmazonTargetGroup"),
    @JsonSubTypes.Type(value = AzureTargetGroup.class, name = "AzureTargetGroup"),
    @JsonSubTypes.Type(value = GoogleTargetGroup.class, name = "GoogleTargetGroup")})
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CloudosTargetGroup {

  @Id
  protected String id;
  protected Providers provider;
  protected String region;
  protected String name;

}
