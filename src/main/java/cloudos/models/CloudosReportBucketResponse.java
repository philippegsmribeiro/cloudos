package cloudos.models;

import org.springframework.http.HttpStatus;

import cloudos.billings.CloudosReportBucket;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by philipperibeiro on 6/22/17.
 *
 * <p>The CloudosReportBucketResponse is used to return a fully comprehensive response regarding the
 * status of the CloudosReportBucket created or fetched. This will allow for a more throughout
 * response.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CloudosReportBucketResponse {

  private HttpStatus status;
  private CloudosReportBucket reportBucket;
  private String message;

}
