package cloudos.models;

import cloudos.models.elasticsearch.System;
import cloudos.utils.ReflectionToJson;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by philipperibeiro on 7/8/17.
 *
 * <p>Write a representation of the Metricbeat index. This way, whenever we receive a metric beat
 * index hit, we can unmarshal the json to the Metricbeat, and utilize it as an object
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Metricbeat implements Serializable {

  @SerializedName("@timestamp")
  public Date timestamp;

  @JsonProperty(value = "beat")
  public Beat beat;

  @JsonProperty(value = "metricset")
  public MetricSet metricset;

  @JsonProperty(value = "system")
  public System system;

  @JsonProperty(value = "type")
  public String type;

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  // make Beat an inner class
  public class Beat {

    @JsonProperty(value = "hostname")
    public String hostname;

    @JsonProperty(value = "name")
    public String name;

    @JsonProperty(value = "version")
    public String version;

    @Override
    public String toString() {
      return ReflectionToJson.toString(this);
    }
  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  // define the class MetricSet
  public class MetricSet {

    @JsonProperty(value = "module")
    public String module;

    @JsonProperty(value = "name")
    public String name;

    @JsonProperty(value = "rtt")
    public String rtt;

    @Override
    public String toString() {
      return ReflectionToJson.toString(this);
    }
  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  public class Total {
    @JsonProperty(value = "bytes")
    public Double bytes;

    @JsonProperty(value = "ios")
    public Double ios;
  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  public class Usage {
    @JsonProperty(value = "bytes")
    public Long bytes;

    @JsonProperty(value = "max")
    public Max max;
  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  public class Limit {
    @JsonProperty(value = "bytes")
    public Double bytes;
  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  public class Quota {
    @JsonProperty(value = "us")
    public Integer us;
  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  public class Max {
    @JsonProperty(value = "bytes")
    public Long bytes;
  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  public class Ns {
    @JsonProperty(value = "ns")
    public Integer ns;
  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  public class Percpu {
    @SerializedName(value = "1")
    public Double value;
  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  public class Entry {

    @JsonProperty(value = "pct")
    public Double pct;

    @JsonProperty(value = "ticks")
    public Long ticks;
  }
}
