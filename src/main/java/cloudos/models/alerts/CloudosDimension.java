package cloudos.models.alerts;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * Class representing the dimension of an alert.
 *
 * @author Alex Calagua
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CloudosDimension implements Serializable {
  private static final long serialVersionUID = 1L;
  private String name;
  private String value;
}
