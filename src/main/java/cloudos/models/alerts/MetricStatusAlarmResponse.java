package cloudos.models.alerts;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class representing the Metric Status Alarm Response.
 *
 * @author Alex Calagua
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MetricStatusAlarmResponse implements Serializable {
  private static final long serialVersionUID = 1L;
  private String code;
  private String name;
}
