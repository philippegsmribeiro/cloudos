package cloudos.models.alerts;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class representing the response metric statistic.
 *
 * @author Alex Calagua
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MetricStatisticResponse implements Serializable {
  private static final long serialVersionUID = 1L;
  /**
   * <p>
   * The time stamp used for the data point.
   * </p>
   */
  private java.util.Date timestamp;
  /**
   * <p>
   * The number of metric values that contributed to the aggregate value of this data point.
   * </p>
   */
  private Double sampleCount;
  /**
   * <p>
   * The average of the metric values that correspond to the data point.
   * </p>
   */
  private Double average;
  /**
   * <p>
   * The sum of the metric values for the data point.
   * </p>
   */
  private Double sum;
  /**
   * <p>
   * The minimum metric value for the data point.
   * </p>
   */
  private Double minimum;
  /**
   * <p>
   * The maximum metric value for the data point.
   * </p>
   */
  private Double maximum;
  /**
   * <p>
   * The standard unit for the data point.
   * </p>
   */
  private String unit;
}
