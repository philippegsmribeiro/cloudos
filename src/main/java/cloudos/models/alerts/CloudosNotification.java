package cloudos.models.alerts;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * Class representing the notification of an alert.
 *
 * @author Alex Calagua
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CloudosNotification implements Serializable {
  private static final long serialVersionUID = 1L;
  private String nameStatus;
  private String codeStatus;
  private String nameNotificationTopic;
  private String codeNotificationTopic;
}
