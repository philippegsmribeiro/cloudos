package cloudos.models.alerts;

/**
 * Enum used to control the period metric for alert.
 * 
 * @author Alex Calagua
 *
 */
public enum MetricPeriodAlert {
  TEN_SECONDS(0,10,"10 Seconds"), 
  THIRTY_SECONDS(1,30,"30 Seconds"), 
  ONE_MINUTE(2,60,"1 Minute"),
  FIVE_MINUTES(3,300,"5 Minutes"),
  FIFTEEN_MINUTES(4,900,"15 Minutes"),
  ONE_HOUR(5,3600,"1 Hour"),
  SIX_HOURS(6,21600,"6 Hours"),
  ONE_DAY(7,86400,"1 Day");

  private final int order;
  private final int period;
  private final String title;

  public int getOrder() {
    return order;
  }

  public int getPeriod() {
    return period;
  }

  public String getTitle() {
    return title;
  }

  MetricPeriodAlert(int order, int period, String title) {
    this.order = order;
    this.period = period;
    this.title = title;
  }
  
  

}
