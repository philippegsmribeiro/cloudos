package cloudos.models.alerts;

import cloudos.Providers;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class representing the request statistic metric.
 *
 * @author Alex Calagua
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MetricStatisticRequest implements Serializable {
  private static final long serialVersionUID = 1L;
  private String region;
  private Providers provider;
  private String metricName;
  private String metricNamespace;
  private String dimensionFilters;
  private String statistic;
  private Date start;
  private Date end;
  private Integer period;

}
