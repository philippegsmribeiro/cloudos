package cloudos.models.alerts;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.querydsl.core.annotations.QueryEntity;

import cloudos.Providers;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * The CloudosAlert class is responsible for serving as the model for the alert in mongoDB.
 *
 * @author Alex Calagua
 */

@Getter
@Setter
@NoArgsConstructor
@QueryEntity
@Document(collection = "cloudos_alert")
public class CloudosAlert extends GenericAlert {

  private int evaluationPeriods;
  private String metricName;
  private String namespace;
  private int period;
  private double threshold;
  private boolean actionsEnabled;
  private String statistic;
  private String comparisonOperator;
  private String unit;
  private String alarmResourceName;
  private Date alarmConfigurationUpdatedTimestamp;
  private Date stateUpdatedTimestamp;
  private String stateValue;
  private String stateReason;
  private CloudosReadStatus readStatus;
  private List<CloudosNotification> notifications;
  private String treatMissingData;
  private List<CloudosDimension> dimensions;


  /**
   * Constructor for use @Builder and inheritance.
   */
  @Builder
  public CloudosAlert(String id, String region, String alarmName, String alarmDescription,
                      Providers provider, int evaluationPeriods, String metricName, String namespace, int period,
                      double threshold, boolean actionsEnabled, String statistic, String comparisonOperator,
                      String unit, String alarmResourceName, Date alarmConfigurationUpdatedTimestamp,
                      Date stateUpdatedTimestamp, String stateValue, String stateReason,
                      CloudosReadStatus readStatus, String treatMissingData, List<CloudosDimension> dimensions,
                      List<CloudosNotification> notifications) {

    super(id, region, alarmName, alarmDescription, provider);

    this.evaluationPeriods = evaluationPeriods;
    this.metricName = metricName;
    this.namespace = namespace;
    this.period = period;
    this.threshold = threshold;
    this.actionsEnabled = actionsEnabled;
    this.statistic = statistic;
    this.comparisonOperator = comparisonOperator;
    this.unit = unit;
    this.alarmResourceName = alarmResourceName;
    this.alarmConfigurationUpdatedTimestamp = alarmConfigurationUpdatedTimestamp;
    this.stateUpdatedTimestamp = stateUpdatedTimestamp;
    this.stateValue = stateValue;
    this.stateReason = stateReason;
    this.readStatus = readStatus;
    this.treatMissingData = treatMissingData;
    this.dimensions = dimensions;
    this.notifications = notifications;
  }


  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + (actionsEnabled ? 1231 : 1237);
    result = prime * result + ((alarmConfigurationUpdatedTimestamp == null) ? 0
        : alarmConfigurationUpdatedTimestamp.hashCode());
    result = prime * result + ((alarmResourceName == null) ? 0 : alarmResourceName.hashCode());
    result = prime * result + ((comparisonOperator == null) ? 0 : comparisonOperator.hashCode());
    result = prime * result + evaluationPeriods;
    result = prime * result + ((metricName == null) ? 0 : metricName.hashCode());
    result = prime * result + ((namespace == null) ? 0 : namespace.hashCode());
    result = prime * result + period;
    result = prime * result + ((readStatus == null) ? 0 : readStatus.hashCode());
    result = prime * result + ((stateReason == null) ? 0 : stateReason.hashCode());
    result =
        prime * result + ((stateUpdatedTimestamp == null) ? 0 : stateUpdatedTimestamp.hashCode());
    result = prime * result + ((stateValue == null) ? 0 : stateValue.hashCode());
    result = prime * result + ((statistic == null) ? 0 : statistic.hashCode());
    long temp;
    temp = Double.doubleToLongBits(threshold);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    result = prime * result + ((unit == null) ? 0 : unit.hashCode());
    result = prime * result + ((treatMissingData == null) ? 0 : treatMissingData.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    CloudosAlert other = (CloudosAlert) obj;

    if (!super.equals(obj)) {
      return false;
    }
    if (actionsEnabled != other.actionsEnabled) {
      return false;
    }
    if (alarmResourceName != null ? !alarmResourceName.equals(other.alarmResourceName)
        : other.alarmResourceName != null) {
      return false;
    }
    if (comparisonOperator != null ? !comparisonOperator.equals(other.comparisonOperator)
        : other.comparisonOperator != null) {
      return false;
    }
    if (evaluationPeriods != other.evaluationPeriods) {
      return false;
    }
    if (metricName != null ? !metricName.equals(other.metricName) : other.metricName != null) {
      return false;
    }
    if (namespace != null ? !namespace.equals(other.namespace) : other.namespace != null) {
      return false;
    }
    if (period != other.period) {
      return false;
    }
    if (stateReason != null ? !stateReason.equals(other.stateReason) : other.stateReason != null) {
      return false;
    }
    if (stateValue != null ? !stateValue.equals(other.stateValue) : other.stateValue != null) {
      return false;
    }
    if (statistic != null ? !statistic.equals(other.statistic) : other.statistic != null) {
      return false;
    }
    if (treatMissingData != null ? !treatMissingData.equals(other.treatMissingData)
        : other.treatMissingData != null) {
      return false;
    }
    if (Double.doubleToLongBits(threshold) != Double.doubleToLongBits(other.threshold)) {
      return false;
    }
    if (unit != null ? !unit.equals(other.unit) : other.unit != null) {
      return false;
    }
    return true;
  }



}