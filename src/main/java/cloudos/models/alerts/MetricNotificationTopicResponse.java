package cloudos.models.alerts;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class representing the response metric notification topic.
 *
 * @author Alex Calagua
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MetricNotificationTopicResponse implements Serializable {
  private static final long serialVersionUID = 1L;
  private String name;
  private String code;
}
