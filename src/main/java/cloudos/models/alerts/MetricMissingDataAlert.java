package cloudos.models.alerts;

/**
 * Enum used to control the missing data alert.
 * 
 * @author Alex Calagua
 *
 */
public enum MetricMissingDataAlert {

  NOT_BREACHING("notBreaching", "good (not breaching threshold)"), 
  BREACHING("breaching", "bad (breaching threshold)"), 
  IGNORE("ignore", "ignore (maintain the alarm state)"), 
  MISSING("missing", "missing");

  private final String code;
  private final String title;

  public String getCode() {
    return code;
  }

  public String getTitle() {
    return title;
  }

  MetricMissingDataAlert(String code, String title) {
    this.code = code;
    this.title = title;
  }



}
