package cloudos.models.alerts;

import cloudos.Providers;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.querydsl.core.annotations.QueryEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;

/**
 * Parent class the alert.
 * 
 * @author Alex Calagua
 */
@NoArgsConstructor
@AllArgsConstructor
@QueryEntity
@Data
public class GenericAlert {

  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  private String id;
  private String region;
  private String alarmName;
  private String alarmDescription;
  private Providers provider;

}
