package cloudos.models.alerts;

/**
 * Enum used to control the read status of the alert.
 * 
 * @author Alex Calagua
 *
 */
public enum CloudosReadStatus {
  NOT_READ,
  READ
}
