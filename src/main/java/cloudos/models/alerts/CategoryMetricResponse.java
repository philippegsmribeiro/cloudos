package cloudos.models.alerts;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class representing the category metric response.
 *
 * @author Alex Calagua
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CategoryMetricResponse implements Serializable {
  private static final long serialVersionUID = 1L;
  private String metricNamespace;
  private String metricCategory;
  private List<String> dimensions;

  /**
   * Method that initialize the list dimensions. If it is already initialized it will add an element
   * in the list
   * 
   * @param dimension request to search the metrics
   * @return List of the dimension
   */
  public List<String> addDimension(String dimension) {
    if (dimensions == null) {
      dimensions = new ArrayList<>();
    }
    dimensions.add(dimension);
    return dimensions;
  }
}
