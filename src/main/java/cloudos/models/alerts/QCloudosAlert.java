package cloudos.models.alerts;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.BooleanPath;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.EnumPath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import javax.annotation.Generated;

/**
 * QCloudosAlert is a Querydsl query type for CloudosAlert.
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCloudosAlert extends EntityPathBase<CloudosAlert> {

  private static final long serialVersionUID = -812075317L;

  public static final QCloudosAlert cloudosAlert = new QCloudosAlert("cloudosAlert");

  public final QGenericAlert parent = new QGenericAlert(this);

  public final BooleanPath actionsEnabled = createBoolean("actionsEnabled");

  public final DateTimePath<java.util.Date> alarmConfigurationUpdatedTimestamp =
      createDateTime("alarmConfigurationUpdatedTimestamp", java.util.Date.class);

  // inherited
  public final StringPath alarmDescription = parent.alarmDescription;

  // inherited
  public final StringPath alarmName = parent.alarmName;

  public final StringPath alarmResourceName = createString("alarmResourceName");

  public final StringPath comparisonOperator = createString("comparisonOperator");


  public final NumberPath<Integer> evaluationPeriods =
      createNumber("evaluationPeriods", Integer.class);

  // inherited
  public final StringPath id = parent.id;

  public final StringPath metricName = createString("metricName");

  public final StringPath namespace = createString("namespace");

  public final NumberPath<Integer> period = createNumber("period", Integer.class);
  
  // inherited
  public final EnumPath<cloudos.Providers> provider = parent.provider;

  public final StringPath region = parent.region;

  public final StringPath stateReason = createString("stateReason");

  public final DateTimePath<java.util.Date> stateUpdatedTimestamp =
      createDateTime("stateUpdatedTimestamp", java.util.Date.class);

  public final StringPath stateValue = createString("stateValue");

  public final StringPath statistic = createString("statistic");

  public final NumberPath<Double> threshold = createNumber("threshold", Double.class);

  public final StringPath unit = createString("unit");

  public QCloudosAlert(String variable) {
    super(CloudosAlert.class, forVariable(variable));
  }

  public QCloudosAlert(Path<? extends CloudosAlert> path) {
    super(path.getType(), path.getMetadata());
  }

  public QCloudosAlert(PathMetadata metadata) {
    super(CloudosAlert.class, metadata);
  }

}

