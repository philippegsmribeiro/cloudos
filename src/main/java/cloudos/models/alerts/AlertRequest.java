package cloudos.models.alerts;

import cloudos.Providers;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class representing the request to create and update an alert.
 *
 * @author Alex Calagua
 */
@Getter
@Setter
@NoArgsConstructor
public class AlertRequest extends AlertRequestGeneric implements Serializable {

  private static final long serialVersionUID = 1L;
  private String id;
  private String alarmDescription;
  private List<CloudosDimension> dimensions;
  private int evaluationPeriods;
  private String metricName;
  private String namespace;
  private int period;
  private double threshold;
  private boolean actionsEnabled;
  private String statistic;
  private String comparisonOperator;
  private String unit;
  private String alarmResourceName;
  private Date alarmConfigurationUpdatedTimestamp;
  private Date stateUpdatedTimestamp;
  private String stateValue;
  private String stateReason;
  private String treatMissingData;
  private List<CloudosNotification> notifications;


  /**
   * Constructor for use @Builder and inheritance.
   */
  @Builder
  public AlertRequest(String region, Providers provider, String alarmName, String id,
      String alarmDescription, List<CloudosDimension> dimensions, int evaluationPeriods,
      String metricName, String namespace, int period, double threshold, boolean actionsEnabled,
      String statistic, String comparisonOperator, String unit, String alarmResourceName,
      Date alarmConfigurationUpdatedTimestamp, Date stateUpdatedTimestamp, String stateValue,
      String stateReason,String treatMissingData, List<CloudosNotification> notifications) {
    super(region, provider, alarmName);
    this.id = id;
    this.alarmDescription = alarmDescription;
    this.dimensions = dimensions;
    this.evaluationPeriods = evaluationPeriods;
    this.metricName = metricName;
    this.namespace = namespace;
    this.period = period;
    this.threshold = threshold;
    this.actionsEnabled = actionsEnabled;
    this.statistic = statistic;
    this.comparisonOperator = comparisonOperator;
    this.unit = unit;
    this.alarmResourceName = alarmResourceName;
    this.alarmConfigurationUpdatedTimestamp = alarmConfigurationUpdatedTimestamp;
    this.stateUpdatedTimestamp = stateUpdatedTimestamp;
    this.stateValue = stateValue;
    this.stateReason = stateReason;
    this.treatMissingData = treatMissingData;
    this.notifications = notifications;
  }
}
