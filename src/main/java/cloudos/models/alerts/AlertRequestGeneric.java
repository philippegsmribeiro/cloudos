package cloudos.models.alerts;

import cloudos.Providers;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class representing the query request and deletion of an alert.
 *
 * @author Alex Calagua
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AlertRequestGeneric implements Serializable {
  private static final long serialVersionUID = 1L;
  private String region;
  private Providers provider;
  private String alarmName;

}
