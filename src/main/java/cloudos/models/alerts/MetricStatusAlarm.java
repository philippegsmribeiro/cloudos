package cloudos.models.alerts;

/**
 * Enum used to metric status for alert.
 * 
 * @author Alex Calagua
 *
 */
public enum MetricStatusAlarm {
  ALARM("State is ALARM"), 
  OK("State is OK"), 
  INSUFFICIENT_DATA("State is INSUFFICIENT");

  private String description;

  public String getDescription() {
    return description;
  }

  MetricStatusAlarm(String description) {
    this.description = description;
  }

}
