package cloudos.models.alerts;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class representing the response metric.
 *
 * @author Alex Calagua
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MetricResponse implements Serializable {
  private static final long serialVersionUID = 1L;
  private String metricName;
  private String metricNamespace;
  private List<CloudosDimension> dimensions;

  /**
   * Method that initialize the list dimensions. If it is already initialized it will add an element
   * in the list
   * 
   * @param dimension request to search the metrics 
   * @return List of the dimension
   */
  public List<CloudosDimension> addDimension(CloudosDimension dimension) {
    if (dimensions == null) {
      dimensions = new ArrayList<>();
    }
    dimensions.add(dimension);
    return dimensions;
  }
}
