package cloudos.models.alerts;

import cloudos.Providers;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class representing the request metric.
 *
 * @author Alex Calagua
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MetricRequest implements Serializable {
  private static final long serialVersionUID = 1L;
  private String region;
  private Providers provider;
  private String metricName;
  private String metricNamespace;
  private String dimensionFilters;
  private String statistic;

  /**
   * Method that returns the name of the metric.
   * 
   * @return returns the metric name. In case the value is empty it will return null.
   */
  public String getMetricNameParameter() {
    if (this.metricName != null && !this.metricName.isEmpty()) {
      return this.metricName;
    }
    return null;
  }

  /**
   * Method that returns the name space of the metric. 
   * @return returns the metric name space. In case the value is empty it will return null.
   */
  public String getMetricNameSpaceParameter() {
    if (this.metricNamespace != null && !this.metricNamespace.isEmpty()) {
      return this.metricNamespace;
    }
    return null;
  }


}
