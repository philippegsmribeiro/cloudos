package cloudos.models.alerts;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.BeanPath;
import com.querydsl.core.types.dsl.EnumPath;
import com.querydsl.core.types.dsl.StringPath;
import javax.annotation.Generated;

/**
 * QGenericAlert is a Querydsl query type for GenericAlert.
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QGenericAlert extends BeanPath<GenericAlert> {

  private static final long serialVersionUID = -114585971L;

  public static final QGenericAlert genericAlert = new QGenericAlert("genericAlert");

  public final StringPath alarmDescription = createString("alarmDescription");

  public final StringPath alarmName = createString("alarmName");

  public final StringPath id = createString("id");

  public final EnumPath<cloudos.Providers> provider =
      createEnum("provider", cloudos.Providers.class);

  public final StringPath region = createString("region");

  public QGenericAlert(String variable) {
    super(GenericAlert.class, forVariable(variable));
  }

  public QGenericAlert(Path<? extends GenericAlert> path) {
    super(path.getType(), path.getMetadata());
  }

  public QGenericAlert(PathMetadata metadata) {
    super(GenericAlert.class, metadata);
  }

}

