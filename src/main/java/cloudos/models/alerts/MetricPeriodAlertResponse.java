package cloudos.models.alerts;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class representing the Metric Status Alert Response.
 *
 * @author Alex Calagua
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MetricPeriodAlertResponse implements Serializable {
  private static final long serialVersionUID = 1L;
  private int order;
  private int period;
  private String title;
}
