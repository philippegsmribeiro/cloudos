package cloudos.models.alerts;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class representing response in registering, updating and deleting an alert.
 *
 * @author Alex Calagua
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AlertResponse<T extends GenericAlert> implements Serializable {

  private static final long serialVersionUID = 1L;
  private String response;
  private T alert;

}
