package cloudos.models;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import cloudos.Providers;

/**
 * Represents the CloudosTargetGroup model repository class.
 * 
 * @author Rogério Souza
 *
 */
public interface CloudosTargetGroupRepository extends MongoRepository<CloudosTargetGroup, String> {

  List<CloudosSecurityGroup> findById(String id);

  List<CloudosSecurityGroup> findByProvider(Providers providers);

}
