package cloudos.models;

import cloudos.Providers;

import lombok.Getter;
import lombok.Setter;

/** Created by philipperibeiro on 12/18/16. */
@Getter
@Setter
public class AwsSecurityGroup extends CloudosSecurityGroup {

  private String groupId;
  private String vpcId;

  public AwsSecurityGroup() {
    this.setProvider(Providers.AMAZON_AWS);
  }
}
