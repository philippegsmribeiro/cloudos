package cloudos.models;

import cloudos.Providers;
import cloudos.data.InstanceDataMetric;
import cloudos.data.InstanceDataUnit;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.springframework.data.mongodb.core.mapping.Document;

/** Created by philipperibeiro on 3/24/17. */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Document(collection = "cloudos_datapoints")
public class CloudosDatapoint extends AbstractCloudosDatapoint implements Serializable {

  private static final long serialVersionUID = 1L;
  public String cloudId;

  public CloudosDatapoint(Providers providers, String region, String instanceId, String cloudId,
      InstanceDataMetric metric, Date dateFromTimestamp, double value, InstanceDataUnit dataUnit) {
    super(null, providers, region, instanceId, metric, dateFromTimestamp, value, dataUnit);
    this.setCloudId(cloudId);
  }

}
