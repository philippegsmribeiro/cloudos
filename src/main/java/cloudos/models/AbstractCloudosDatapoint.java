package cloudos.models;

import java.io.Serializable;
import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import cloudos.Providers;
import cloudos.data.InstanceDataMetric;
import cloudos.data.InstanceDataUnit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractCloudosDatapoint implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  public String id = new ObjectId().toHexString();

  public Providers provider;
  public String region;
  public String instanceId;
  public InstanceDataMetric metric;
  private Date timestamp;
  private Double value;
  private InstanceDataUnit unit;

}
