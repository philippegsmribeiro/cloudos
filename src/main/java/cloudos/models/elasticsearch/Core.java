package cloudos.models.elasticsearch;

import org.codehaus.jackson.annotate.JsonProperty;

import cloudos.models.Metricbeat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/** Define the System Core Metricset. */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Core extends SystemMetricset {

  @JsonProperty(value = "id")
  public Integer id;

  @JsonProperty(value = "idle")
  public Metricbeat.Entry idle;

  @JsonProperty(value = "iowait")
  public Metricbeat.Entry iowait;

  @JsonProperty(value = "irq")
  public Metricbeat.Entry irq;

  @JsonProperty(value = "nice")
  public Metricbeat.Entry nice;

  @JsonProperty(value = "softirq")
  public Metricbeat.Entry softirq;

  @JsonProperty(value = "steal")
  public Metricbeat.Entry steal;

  @JsonProperty(value = "system")
  public Metricbeat.Entry system;

  @JsonProperty(value = "user")
  public Metricbeat.Entry user;
}
