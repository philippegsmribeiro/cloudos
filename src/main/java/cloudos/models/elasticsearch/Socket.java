package cloudos.models.elasticsearch;

import org.codehaus.jackson.annotate.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/** Define the System socket Metricset. */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Socket extends SystemMetricset {

  @JsonProperty(value = "direction")
  public String direction;

  @JsonProperty(value = "family")
  public String family;

  @JsonProperty(value = "local")
  public Socket.Local local;

  @JsonProperty(value = "process")
  public Socket.Process process;

  @JsonProperty(value = "user")
  public Socket.User user;

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  public class Local {

    @JsonProperty(value = "ip")
    public String ip;

    @JsonProperty(value = "port")
    public Integer port;
  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  public class Process {

    @JsonProperty(value = "cmdline")
    public String cmdline;

    @JsonProperty(value = "command")
    public String command;

    @JsonProperty(value = "exe")
    public String exe;

    @JsonProperty(value = "pid")
    public String pid;
  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  public class User {

    @JsonProperty(value = "id")
    public Integer id;

    @JsonProperty(value = "name")
    public String name;
  }
}
