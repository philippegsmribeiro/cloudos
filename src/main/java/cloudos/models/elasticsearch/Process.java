package cloudos.models.elasticsearch;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonProperty;

import cloudos.models.Metricbeat;
import cloudos.utils.ReflectionToJson;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/** Define the System Process Metricset. */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Process extends SystemMetricset {

  @JsonProperty(value = "cgroup")
  public Process.CGroup cGroup;

  @JsonProperty(value = "cmdline")
  public String cmdline;

  @JsonProperty(value = "cpu")
  public Process.CPU cpu;

  @JsonProperty(value = "fd")
  public Process.FileDescriptor fd;

  @JsonProperty(value = "memory")
  public Process.Memory memory;

  @JsonProperty(value = "name")
  public String name;

  @JsonProperty(value = "pgid")
  public Integer pgid;

  @JsonProperty(value = "pid")
  public Integer pid;

  @JsonProperty(value = "ppid")
  public Integer ppid;

  @JsonProperty(value = "state")
  public String state;

  @JsonProperty(value = "username")
  public String username;

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  public class Memory {

    @JsonProperty(value = "rss")
    public Process.Memory.RSS rss;

    @JsonProperty(value = "share")
    public Integer share;

    @JsonProperty(value = "size")
    public Integer size;

    @Setter
    @Getter
    @NoArgsConstructor
    @AllArgsConstructor
    public class RSS {

      @JsonProperty(value = "bytes")
      public Double bytes;

      @JsonProperty(value = "pct")
      public Double pct;
    }
  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  public class FileDescriptor {

    @JsonProperty(value = "total")
    public Process.FileDescriptor.Limit limit;

    @JsonProperty(value = "open")
    public Integer open;

    public class Limit {
      @JsonProperty(value = "hard")
      public Integer hard;

      @JsonProperty(value = "soft")
      public Integer soft;
    }
  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  public class CPU {

    @JsonProperty(value = "start_time")
    public Date start_time;

    @JsonProperty(value = "total")
    public Process.CPU.Total total;

    public class Total {

      @JsonProperty(value = "pct")
      public Double pct;
    }
  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  public class CGroup {

    @JsonProperty(value = "blkio")
    public Process.CGroup.Blkio blkio;

    @JsonProperty(value = "cpu")
    public Process.CGroup.Cpu cpu;

    @JsonProperty(value = "cpuacct")
    public Process.CGroup.Cpuacct cpuacct;

    @JsonProperty(value = "id")
    public String id;

    @JsonProperty(value = "memory")
    public Process.CGroup.Memory memory;

    @JsonProperty(value = "path")
    public String path;

    @Override
    public String toString() {
      return ReflectionToJson.toString(this);
    }

    @Setter
    @Getter
    @NoArgsConstructor
    @AllArgsConstructor
    public class Blkio {

      @JsonProperty(value = "id")
      public String id;

      @JsonProperty(value = "path")
      public String path;

      @JsonProperty(value = "total")
      public Metricbeat.Total total;

      @Override
      public String toString() {
        return ReflectionToJson.toString(this);
      }
    }

    @Setter
    @Getter
    @NoArgsConstructor
    @AllArgsConstructor
    public class Cpu {

      @JsonProperty(value = "cfs")
      public Process.CGroup.Cpu.Cfs cfs;

      @JsonProperty(value = "id")
      public String id;

      @JsonProperty(value = "path")
      public String path;

      @JsonProperty(value = "rt")
      public Process.CGroup.Cpu.Rt rt;

      @JsonProperty(value = "stats")
      public Process.CGroup.Cpu.Stats stats;

      @Setter
      @Getter
      @NoArgsConstructor
      @AllArgsConstructor
      public class Cfs {

        @JsonProperty(value = "period")
        public Metricbeat.Quota period;

        @JsonProperty(value = "quota")
        public Metricbeat.Quota quota;

        @JsonProperty(value = "shares")
        public Integer shares;
      }

      @Setter
      @Getter
      @NoArgsConstructor
      @AllArgsConstructor
      public class Rt {

        @JsonProperty(value = "period")
        public Metricbeat.Quota period;

        @JsonProperty(value = "runtime")
        public Metricbeat.Quota runtime;
      }

      @Setter
      @Getter
      @NoArgsConstructor
      @AllArgsConstructor
      public class Stats {

        @JsonProperty(value = "periods")
        public Integer periods;

        @JsonProperty(value = "throttled")
        public Process.CGroup.Cpu.Stats.Throttled throttled;

        public class Throttled {
          @JsonProperty(value = "ns")
          public Integer ns;

          @JsonProperty(value = "periods")
          public Integer periods;
        }
      }

      @Override
      public String toString() {
        return ReflectionToJson.toString(this);
      }
    }

    @Setter
    @Getter
    @NoArgsConstructor
    @AllArgsConstructor
    public class Cpuacct {

      @JsonProperty(value = "id")
      public String id;

      @JsonProperty(value = "path")
      public String path;

      @JsonProperty(value = "percpu")
      public Metricbeat.Percpu percpu;

      @JsonProperty(value = "stats")
      public Process.CGroup.Cpuacct.Stats stats;

      @JsonProperty(value = "total")
      public Metricbeat.Ns total;

      @Setter
      @Getter
      @NoArgsConstructor
      @AllArgsConstructor
      public class Stats {

        @JsonProperty(value = "system")
        public Metricbeat.Ns system;

        @JsonProperty(value = "user")
        public Metricbeat.Ns user;
      }

      @Override
      public String toString() {
        return ReflectionToJson.toString(this);
      }
    }

    @Setter
    @Getter
    @NoArgsConstructor
    @AllArgsConstructor
    public class Memory {

      @JsonProperty(value = "id")
      public String id;

      @JsonProperty(value = "kmem")
      public Process.CGroup.Memory.Kmem kmem;

      @JsonProperty(value = "kmem_tcp")
      public Process.CGroup.Memory.KmemTcp kmem_tcp;

      @JsonProperty(value = "mem")
      public Process.CGroup.Memory.Mem mem;

      @JsonProperty(value = "memsw")
      public Process.CGroup.Memory.Memsw memsw;

      @JsonProperty(value = "path")
      public String path;

      @JsonProperty(value = "stats")
      public Process.CGroup.Memory.Stats stats;

      @Setter
      @Getter
      @NoArgsConstructor
      @AllArgsConstructor
      public class Kmem {

        @JsonProperty(value = "failures")
        public Integer failures;

        @JsonProperty(value = "limit")
        public Metricbeat.Limit limit;

        @JsonProperty(value = "usage")
        public Metricbeat.Usage usage;
      }

      @Setter
      @Getter
      @NoArgsConstructor
      @AllArgsConstructor
      public class KmemTcp {

        @JsonProperty(value = "failures")
        public Integer failures;

        @JsonProperty(value = "limit")
        public Metricbeat.Limit limit;

        @JsonProperty(value = "usage")
        public Metricbeat.Usage usage;

        @Override
        public String toString() {
          return ReflectionToJson.toString(this);
        }
      }

      @Setter
      @Getter
      @NoArgsConstructor
      @AllArgsConstructor
      public class Mem {

        @JsonProperty(value = "failures")
        public Integer failures;

        @JsonProperty(value = "limit")
        public Metricbeat.Limit limit;

        @JsonProperty(value = "usage")
        public Metricbeat.Usage usage;
      }

      @Setter
      @Getter
      @NoArgsConstructor
      @AllArgsConstructor
      public class Memsw {

        @JsonProperty(value = "failures")
        public Integer failures;

        @JsonProperty(value = "limit")
        public Metricbeat.Limit limit;

        @JsonProperty(value = "usage")
        public Metricbeat.Usage usage;
      }

      @Setter
      @Getter
      @NoArgsConstructor
      @AllArgsConstructor
      public class Stats {

        @JsonProperty(value = "active_anon")
        public Metricbeat.Limit active_anon;

        @JsonProperty(value = "active_file")
        public Metricbeat.Limit active_file;

        @JsonProperty(value = "cache")
        public Metricbeat.Limit cache;

        @JsonProperty(value = "hierarchical_memory_limit")
        public Metricbeat.Limit hierarchical_memory_limit;

        @JsonProperty(value = "hierarchical_memsw_limit")
        public Metricbeat.Limit hierarchical_memsw_limit;

        @JsonProperty(value = "inactive_anon")
        public Metricbeat.Limit inactive_anon;

        @JsonProperty(value = "inactive_file")
        public Metricbeat.Limit inactive_file;

        @JsonProperty(value = "major_page_faults")
        public Integer major_page_faults;

        @JsonProperty(value = "mapped_file")
        public Metricbeat.Limit mapped_file;

        @JsonProperty(value = "page_faults")
        public Integer page_faults;

        @JsonProperty(value = "pages_in")
        public Integer pages_in;

        @JsonProperty(value = "pages_out")
        public Integer pages_out;

        @JsonProperty(value = "rss")
        public Metricbeat.Limit rss;

        @JsonProperty(value = "rss_huge")
        public Metricbeat.Limit rss_huge;

        @JsonProperty(value = "swap")
        public Metricbeat.Limit swap;

        @JsonProperty(value = "unevictable")
        public Metricbeat.Limit unevictable;

        @Override
        public String toString() {
          return ReflectionToJson.toString(this);
        }
      }
    }
  }
}
