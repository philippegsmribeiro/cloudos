package cloudos.models.elasticsearch;

import org.codehaus.jackson.annotate.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/** Define the Diskio Metricset. */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Diskio extends SystemMetricset {

  @JsonProperty(value = "io")
  public Diskio.IO io;

  @JsonProperty(value = "name")
  public String name;

  @JsonProperty(value = "read")
  public Diskio.Entry read;

  @JsonProperty(value = "write")
  public Diskio.Entry write;

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  public class IO {

    @JsonProperty(value = "time")
    public Long time;
  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  public class Entry {

    @JsonProperty(value = "bytes")
    public Long bytes;

    @JsonProperty(value = "count")
    public Integer count;

    @JsonProperty(value = "time")
    public Long time;
  }
}
