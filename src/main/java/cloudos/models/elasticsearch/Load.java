package cloudos.models.elasticsearch;

import org.codehaus.jackson.annotate.JsonProperty;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/** Define the Load Metricset. */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Load extends SystemMetricset {

  @SerializedName(value = "1")
  public Double one;

  @SerializedName(value = "15")
  public Double fifteen;

  @SerializedName(value = "5")
  public Double five;

  @JsonProperty(value = "norm")
  public Load.Norm norm;

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  public class Norm {

    @SerializedName(value = "1")
    public Double one;

    @SerializedName(value = "15")
    public Double fifteen;

    @SerializedName(value = "5")
    public Double five;
  }
}
