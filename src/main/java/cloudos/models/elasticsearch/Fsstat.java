package cloudos.models.elasticsearch;

import org.codehaus.jackson.annotate.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/** Define the Fsstat Metricset. */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Fsstat extends SystemMetricset {

  @JsonProperty(value = "count")
  public Integer count;

  @JsonProperty(value = "total_files")
  public Long total_files;

  @JsonProperty(value = "total_size")
  public Fsstat.TotalSize total_size;

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  public class TotalSize {

    @JsonProperty(value = "free")
    public Long free;

    @JsonProperty(value = "total")
    public Long total;

    @JsonProperty(value = "used")
    public Long used;
  }
}
