package cloudos.models.elasticsearch;

import org.codehaus.jackson.annotate.JsonProperty;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import cloudos.utils.ReflectionToJson;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/** Created by philipperibeiro on 7/14/17. */
// define the class System]
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class System {

  @JsonProperty(value = "core")
  public Core core;

  @JsonProperty(value = "socket")
  public Socket socket;

  @JsonProperty(value = "cpu")
  public CPU cpu;

  @JsonProperty(value = "diskio")
  public Diskio diskio;

  @JsonProperty(value = "filesystem")
  public Filesystem filesystem;

  @JsonProperty(value = "fsstat")
  public Fsstat fsstat;

  @JsonProperty(value = "load")
  public Load load;

  @JsonProperty(value = "memory")
  public Memory memory;

  @JsonProperty(value = "network")
  public Network network;

  @JsonProperty(value = "process")
  public Process process;

  @Override
  public String toString() {
    return ReflectionToJson.toString(this);
  }
}
