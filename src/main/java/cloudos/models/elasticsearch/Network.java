package cloudos.models.elasticsearch;

import org.codehaus.jackson.annotate.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/** Define the System Network Metricset. */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Network extends SystemMetricset {

  @JsonProperty(value = "in")
  public Network.Entry in;

  @JsonProperty(value = "name")
  public String name;

  @JsonProperty(value = "out")
  public Network.Entry out;

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  public class Entry {

    @JsonProperty(value = "bytes")
    public Long bytes;

    @JsonProperty(value = "dropped")
    public Long dropped;

    @JsonProperty(value = "errors")
    public Long errors;

    @JsonProperty(value = "packets")
    public Long packets;
  }
}
