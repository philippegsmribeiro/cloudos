package cloudos.models.elasticsearch;

import org.codehaus.jackson.annotate.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/** Define the Filesystem Metricset */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Filesystem extends SystemMetricset {

  @JsonProperty(value = "available")
  public Long available;

  @JsonProperty(value = "device_name")
  public String device_name;

  @JsonProperty(value = "files")
  public Long files;

  @JsonProperty(value = "free_files")
  public Long free_files;

  @JsonProperty(value = "mount_point")
  public String mount_point;

  @JsonProperty(value = "total")
  public Long total;

  @JsonProperty(value = "used")
  public Filesystem.Used used;

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  public class Used {

    @JsonProperty(value = "bytes")
    public Long bytes;

    @JsonProperty(value = "pct")
    public Double pct;
  }
}
