package cloudos.models.elasticsearch;

import org.codehaus.jackson.annotate.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/** Define the Memory Metricset. */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Memory extends SystemMetricset {

  @JsonProperty(value = "actual")
  public Actual actual;

  @JsonProperty(value = "free")
  public Long free;

  @JsonProperty(value = "swap")
  public Memory.Swap swap;

  @JsonProperty(value = "total")
  public Long total;

  @JsonProperty(value = "used")
  public Memory.Used used;

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  public class Actual {

    @JsonProperty(value = "free")
    public Long free;

    @JsonProperty(value = "used")
    public Memory.Used used;
  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  public class Swap {

    @JsonProperty(value = "free")
    public Long free;

    @JsonProperty(value = "total")
    public Long total;

    @JsonProperty(value = "used")
    public Memory.Used used;
  }

  @Setter
  @Getter
  @NoArgsConstructor
  @AllArgsConstructor
  public class Used {

    @JsonProperty(value = "bytes")
    public Long bytes;

    @JsonProperty(value = "pct")
    public Double pct;
  }
}
