package cloudos.models.elasticsearch;

import cloudos.utils.ReflectionToJson;

import com.fasterxml.jackson.annotation.JsonSubTypes;

import org.codehaus.jackson.annotate.JsonTypeInfo;

/** Define the SystemMetricset type. */
@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.WRAPPER_OBJECT,
    property = "type"
)
@JsonSubTypes({
    @JsonSubTypes.Type(value = Core.class, name = "core"),
    @JsonSubTypes.Type(value = CPU.class, name = "cpu"),
    @JsonSubTypes.Type(value = Diskio.class, name = "diskio"),
    @JsonSubTypes.Type(value = Filesystem.class, name = "filesystem"),
    @JsonSubTypes.Type(value = Fsstat.class, name = "fsstat"),
    @JsonSubTypes.Type(value = Load.class, name = "load"),
    @JsonSubTypes.Type(value = Memory.class, name = "memory"),
    @JsonSubTypes.Type(value = Network.class, name = "network"),
    @JsonSubTypes.Type(value = Process.class, name = "process"),
    @JsonSubTypes.Type(value = Socket.class, name = "socket")
})
public abstract class SystemMetricset {

  @Override
  public String toString() {
    return ReflectionToJson.toString(this);
  }
}
