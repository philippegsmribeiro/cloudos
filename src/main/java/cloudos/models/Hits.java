package cloudos.models;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Created by philipperibeiro on 7/8/17. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Hits<T> {

  private List<Hit<T>> hits;
  private Long total;

  @JsonProperty("max_score")
  private Double maxScore;

}
