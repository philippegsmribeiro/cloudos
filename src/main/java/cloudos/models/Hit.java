package cloudos.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Created by philipperibeiro on 7/8/17. */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Hit<T> {

  @SerializedName(value = "_index")
  private String index;

  @SerializedName(value = "_type")
  private String type;

  @SerializedName(value = "_id")
  private String id;

  @SerializedName(value = "_score")
  private Double score;

  @SerializedName(value = "_source")
  private T source;

}
