package cloudos.models.spotinstances;

import cloudos.Providers;
import cloudos.models.CloudCreateRequest;
import cloudos.utils.ReflectionToJson;

import com.amazonaws.services.ec2.model.SpotInstanceType;
import com.amazonaws.services.ec2.model.Tag;

import java.util.Date;
import java.util.List;
import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * The CloudSpotCreateRequest class is responsible for serving as the model for the spot request,
 * carrying the information that will be needed in other to fulfill the request.
 *
 * <p>
 * The model is used by all the cloud providers.
 * </p>
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class CloudSpotCreateRequest extends CloudCreateRequest {


  private Double spotPrice;
  // The duration must be a multiple of 60, in minutes
  private Integer duration;
  private String placementName;
  private String availabilityZoneGroup;
  private Date validFrom;
  private Date validUntil;
  private SpotInstanceType spotInstanceType;
  private String launchGroup;
  private List<Tag> tags;


  /**
   * Default constructor.
   *
   * @param provider cloud provider
   * @param region instance region
   * @param name instance name
   * @param machineType instance type
   * @param image instance image
   * @param imageProject instance imageProject
   * @param count number of instances
   * @param minCount number minimum of instances
   * @param securityGroup instance security group
   * @param zone instance zone
   * @param keyName instance key
   * @param tag instance tag
   * @param cloudGroup instance cloudGroup
   * @param spotPrice price of the instance
   * @param duration duration of the instances
   * @param placementName placement name
   * @param availabilityZoneGroup availability zone group
   * @param validFrom spot start date
   * @param validUntil spot stop date
   * @param spotInstanceType spot instance type
   * @param launchGroup instance launchgroup
   */
  public CloudSpotCreateRequest(Providers provider, String region, String name, String machineType,
      String image, String imageProject, int count, int minCount, String securityGroup, String zone,
      String keyName, Map<String,String> tag, String cloudGroup, Double spotPrice, Integer duration,
      String placementName, String availabilityZoneGroup, Date validFrom, Date validUntil,
      SpotInstanceType spotInstanceType, String launchGroup) {
    super(provider, region, zone, name, machineType, image, imageProject, count, minCount,
        securityGroup, keyName, tag, cloudGroup, true);
    this.setSpotPrice(spotPrice);
    this.setDuration(duration);
    this.setPlacementName(placementName);
    this.setAvailabilityZoneGroup(availabilityZoneGroup);
    this.setValidFrom(validFrom);
    this.setValidUntil(validUntil);
    this.setSpotInstanceType(spotInstanceType);
    this.setLaunchGroup(launchGroup);
  }

  /**
   * Constructor which creates a spot create request based on the normal create request.
   *
   * @param createRequest normal create request
   * @param spotPrice price of the instance
   * @param duration duration of the instances
   * @param placementName placement name
   * @param availabilityZoneGroup availability zone group
   * @param validFrom spot start date
   * @param validUntil spot stop date
   * @param spotInstanceType spot instance type
   * @param launchGroup instance launchgroup
   */
  public CloudSpotCreateRequest(CloudCreateRequest createRequest, Double spotPrice,
      Integer duration, String placementName, String availabilityZoneGroup, Date validFrom,
      Date validUntil, SpotInstanceType spotInstanceType, String launchGroup) {
    this(createRequest.getProvider(), createRequest.getRegion(), createRequest.getName(),
        createRequest.getMachineType(), createRequest.getImage(), createRequest.getImageProject(),
        createRequest.getCount(), createRequest.getMinCount(), createRequest.getSecurityGroup(),
        createRequest.getZone(), createRequest.getKeyName(), createRequest.getTag(),
        createRequest.getCloudGroup(), spotPrice, duration, placementName, availabilityZoneGroup,
        validFrom, validUntil, spotInstanceType, launchGroup);

  }

  /**
   * Create the request for GCloud.
   *
   * @param region instance region
   * @param name instance name
   * @param machineType instance type
   * @param image instance image
   * @param imageProject instance imageProject
   * @param count number of instances
   * @param keyName instance key
   * @param zone instance zone
   * @param cloudGroup instance cloudGroup
   * @return CloudoSpotCreateRequest
   */
  public static CloudSpotCreateRequest createGCloudRequest(String region, String name,
      String machineType, String image, String imageProject, int count, String keyName, String zone,
      String cloudGroup) {
    return new CloudSpotCreateRequest(Providers.GOOGLE_COMPUTE_ENGINE, region, name, machineType,
        image, imageProject, count, count, null, zone, keyName, null, cloudGroup, null, null, null,
        null, null, null, null, null);
  }

  /**
   * Create the request for Aws.
   *
   * @param region instance region
   * @param name instance name
   * @param machineType instance type
   * @param image instance image
   * @param count number of instances
   * @param securityGroup instance securityGroup
   * @param keyName instances key
   * @param zone instance zone
   * @param cloudGroup instance cloud group
   * @param spotPrice price of the instance
   * @param duration duration of the instances
   * @param placementName placement name
   * @param availabilityZoneGroup availability zone group
   * @param validFrom spot start date
   * @param validUntil spot stop date
   * @param spotInstanceType spot instance type
   * @param launchGroup instance launchgroup
   * @return CloudoSpotCreateRequest
   */
  public static CloudSpotCreateRequest createAwsCloudRequest(String region, String name,
      String machineType, String image, int count, String securityGroup, String keyName,
      String zone, String cloudGroup, Double spotPrice, Integer duration, String placementName,
      String availabilityZoneGroup, Date validFrom, Date validUntil,
      SpotInstanceType spotInstanceType, String launchGroup) {
    return new CloudSpotCreateRequest(Providers.AMAZON_AWS, region, name, machineType, image, null,
        count, count, securityGroup, zone, keyName, null, cloudGroup, spotPrice, duration,
        placementName, availabilityZoneGroup, validFrom, validUntil, spotInstanceType, launchGroup);
  }

  @Override
  public String toString() {
    return ReflectionToJson.toString(this);
  }

}
