package cloudos.models.spotinstances;

import cloudos.Providers;
import com.amazonaws.services.ec2.model.CancelSpotFleetRequestsErrorItem;
import com.amazonaws.services.ec2.model.CancelSpotFleetRequestsSuccessItem;
import java.util.List;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

/**
 * Cancel response for spot fleet.
 */
@Data
@Builder
public class CloudSpotFleetCancelResponse {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private String id;
  private Providers provider;
  private String region;
  private List<CancelSpotFleetRequestsSuccessItem> successfulFleetRequests;
  private List<CancelSpotFleetRequestsErrorItem> unsuccessfulFleetRequests;

}
