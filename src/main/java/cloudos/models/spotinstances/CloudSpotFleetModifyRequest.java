package cloudos.models.spotinstances;

import cloudos.Providers;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

/**
 * Modify request for spot fleet.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CloudSpotFleetModifyRequest {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private String id;
  private Providers provider;
  private String region;
  private String excessCapacityTerminationPolicy;
  private String spotFleetRequestId;
  private Integer targetCapacity;

}
