package cloudos.models.spotinstances;

import cloudos.Providers;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

/**
 * Define the response for the spot cancel request.
 */
@Builder
@Data
public class CloudSpotCancelResponse {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private String id;
  private Providers provider;
  private String region;
  private Boolean status;

}
