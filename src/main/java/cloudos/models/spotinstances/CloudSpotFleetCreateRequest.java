package cloudos.models.spotinstances;

import cloudos.Providers;
import com.amazonaws.services.ec2.model.SpotFleetLaunchSpecification;
import com.amazonaws.services.ec2.model.Tag;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

/**
 * Create request for spot fleet.
 */
@Data
@Builder
public class CloudSpotFleetCreateRequest {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private String id;
  private Providers provider;
  private String region;
  private String allocationStrategy;
  private String clientToken;
  private String excessCapacityTerminationPolicy;
  private Double fulfilledCapacity;
  private String iamFleetRole;
  private String instanceInterruptionBehavior;
  private Collection<SpotFleetLaunchSpecification> launchSpecifications;
  private Boolean replaceUnhealthyInstances;
  private String spotPrice;
  private Integer targetCapacity;
  private Boolean terminateInstancesWithExpiration;
  private String type;
  private Date validFrom;
  private Date validUntil;
  private List<Tag> tags;
  private String cloudGroupId;

}
