package cloudos.models.spotinstances;

import cloudos.Providers;
import cloudos.machinelearning.spotbidder.SpotBidderRequestStatus;
import cloudos.models.AbstractInstance;
import com.amazonaws.services.ec2.model.SpotInstanceType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import lombok.Builder;
import lombok.Data;

/**
 * Obtain the response from the create request for spot instances.
 */
@Builder
@Data
public class CloudSpotCreateResponse {

  private Providers provider;
  private String region;
  private Integer target;
  private Integer created;
  private Integer minCount;
  private Integer duration;
  private Double spotPrice;
  private String availabilityZone;
  private SpotInstanceType spotInstanceType;
  @JsonIgnore // not sending it in the queue
  private List<AbstractInstance> instances;
  private SpotBidderRequestStatus status;

}
