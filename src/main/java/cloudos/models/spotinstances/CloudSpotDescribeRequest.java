package cloudos.models.spotinstances;

import cloudos.Providers;
import cloudos.utils.ReflectionToJson;
import java.util.List;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Builder;
import lombok.Data;

/**
 * Define the spot describe request.
 */
@Builder
@Data
public class CloudSpotDescribeRequest {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private String id;
  private Providers provider;
  private String region;
  private List<String> instanceIds;

}
