package cloudos.models.spotinstances;

import cloudos.Providers;
import java.util.List;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CloudSpotCancelRequest {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private String id;
  private Providers provider;
  private String region;
  private List<String> spotInstanceRequestIds;
  private Boolean terminateInstances;

}
