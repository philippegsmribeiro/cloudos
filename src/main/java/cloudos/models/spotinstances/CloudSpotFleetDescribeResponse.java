package cloudos.models.spotinstances;

import cloudos.Providers;
import com.amazonaws.services.ec2.model.ActiveInstance;
import java.util.List;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

/**
 * Describe response for spot fleet.
 */
@Data
@Builder
public class CloudSpotFleetDescribeResponse {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private String id;
  private Providers provider;
  private String region;
  private List<ActiveInstance> activeInstances;
  private String nextToken;
  private String spotFleetRequestId;

}
