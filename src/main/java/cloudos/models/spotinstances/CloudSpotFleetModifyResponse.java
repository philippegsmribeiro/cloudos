package cloudos.models.spotinstances;

import cloudos.Providers;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

/**
 * Modify response for spot fleet.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CloudSpotFleetModifyResponse {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private String id;
  private Providers provider;
  private String region;
  private Boolean returnValue;

}
