package cloudos.models.spotinstances;

/**
 * Describe all the spot instance products offered.
 */
public enum SpotProductDescription {

  AWS_LINUX_UNIX("Linux/UNIX "),
  AWS_SUSE_LINUX("SUSE Linux"),
  AWS_WINDOWS("Windows"),
  AWS_LINUX_UNIX_VPC("Linux/UNIX (Amazon VPC)"),
  AWS_SUSE_LINUX_VPC("SUSE Linux (Amazon VPC)"),
  AAWS_WINDOWS_VPC("Windows (Amazon VPC)");

  private String name;

  SpotProductDescription(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return this.name;
  }
}
