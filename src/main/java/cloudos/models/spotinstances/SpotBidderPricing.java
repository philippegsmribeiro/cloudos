package cloudos.models.spotinstances;

import cloudos.Providers;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@Document(collection = "spot_bidder_pricing")
public class SpotBidderPricing {

  private Providers provider;
  private String region;
  private String availabilityZone;
  private String product;
  private String instanceType;
  private Double maxPrice;
  private Double minPrice;

}
