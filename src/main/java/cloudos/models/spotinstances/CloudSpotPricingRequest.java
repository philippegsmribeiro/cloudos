package cloudos.models.spotinstances;

import cloudos.Providers;
import java.util.Date;
import java.util.List;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Define the request for the spot pricing.
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CloudSpotPricingRequest {

  /*
   * for AWS product description use one of
   * (Linux/UNIX | SUSE Linux | Windows | Linux/UNIX (Amazon VPC) | SUSE Linux (Amazon VPC) |
   * Windows (Amazon VPC)).
   */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private String id;
  private Providers provider;
  private String region;
  private String availabilityZone;
  private Date startTime;
  private Date endTime;
  private List<String> instanceTypes;
  private List<String> productDescription;

}

