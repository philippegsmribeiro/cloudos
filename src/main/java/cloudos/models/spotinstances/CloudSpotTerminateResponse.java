package cloudos.models.spotinstances;

import cloudos.Providers;
import cloudos.models.CloudActionResponse;
import java.util.List;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

/**
 * Define the response for the spot terminate request.
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CloudSpotTerminateResponse {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private String id;
  private Providers provider;
  private String region;
  private List<? extends CloudActionResponse<?>> actionResponses;

}
