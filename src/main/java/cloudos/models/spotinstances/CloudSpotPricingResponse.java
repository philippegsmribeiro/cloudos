package cloudos.models.spotinstances;

import cloudos.Providers;
import java.util.Date;
import java.util.List;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

/**
 * Define the response for the spot pricing request.
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CloudSpotPricingResponse {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private String id;
  private Providers provider;
  private String region;
  private String availabilityZone;
  private List<String> instanceTypes;
  private List<String> productDescription;
  private Date timestamp;
  private Double price;

}
