package cloudos.models.spotinstances;

import cloudos.Providers;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

/**
 * Describe request for spot fleet.
 */
@Data
@Builder
public class CloudSpotFleetDescribeRequest {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private String id;
  private Providers provider;
  private String region;
  private String spotFleetRequestId;
  private Integer maxResults;
  private String nextToken;

}
