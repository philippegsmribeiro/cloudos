package cloudos.models.spotinstances;

import cloudos.Providers;
import java.util.List;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

/**
 * Cancel request for spot fleet.
 */
@Data
@Builder
public class CloudSpotFleetCancelRequest {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private String id;
  private Providers provider;
  private String region;
  private List<String> spotFleetRequestIds;
  private Boolean terminateInstances;

}
