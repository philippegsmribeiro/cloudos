package cloudos.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by philipperibeiro on 12/16/16. AwsKeyPair represents the model that will store all the
 * key pair .pem files in Mongo. This way we have total control of all the keyPair files created,
 * and if they are valid or not.
 */
@Document(collection = "aws_key_pairs")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AwsKeyPair {

  @Id
  public String id;
  private String region;
  private boolean valid;
  private String keyname;
  private String key;

  /**
   * AwsKeyPair creates a new AwsKeyPair object.
   *
   * @param region the region the keyPair file is being created
   * @param valid check if the key is valid
   * @param key the key content (TODO: encrypt the key content)
   */
  public AwsKeyPair(String region, String keyname, boolean valid, String key) {
    this.region = region;
    this.valid = valid;
    this.key = key;
    this.keyname = keyname;
  }

}
