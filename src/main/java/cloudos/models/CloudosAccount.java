package cloudos.models;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import cloudos.Providers;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Represents the account model.
 * 
 * @author Rogério Souza
 *
 */

@Document(collection = "cloudos_account")
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CloudosAccount {

  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  private String id;
  
  @NotEmpty 
  private String accountId;
  
  @NotEmpty 
  private String accountName;

  @NotNull
  private Providers provider;

  @DBRef(lazy = false)
  private List<CloudosCredential> credentials = new ArrayList<>();

}
