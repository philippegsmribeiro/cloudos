package cloudos.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import cloudos.Providers;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Represents the abstraction of load balancers to be specialized by provider such as:
 * AmazonLoadBalancer, AzureLoadBalancer, GoogleLoadBalancer
 * 
 * @author Rogério Souza
 *
 */

@Document(collection = "cloudos_load_balancer")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY,
    property = "providerType", defaultImpl = AmazonLoadBalancer.class)
@JsonSubTypes({@JsonSubTypes.Type(value = AmazonLoadBalancer.class, name = "AmazonLoadBalancer")})
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CloudosLoadBalancer {

  @Id
  protected String id;
  protected Providers provider;
  protected String region;
  protected String name;

}
