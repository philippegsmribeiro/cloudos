package cloudos.models.autoscaler;

public enum AutoscaleStatus {
  REQUESTING_SPOT_PRICING,
  AUTOSCALING_WITH_ON_DEMAND,
  REQUESTING_CREATE_SPOT,
  AUTOSCALING_WITH_SPOT
}
