package cloudos.models.autoscaler;

import cloudos.machinelearning.autoscaler.AutoscaleLabels;
import cloudos.models.CloudActionRequest;
import cloudos.models.CloudCreateRequest;
import cloudos.utils.ReflectionToJson;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "autoscale_requests")
@ToString
public class AutoscaleRequest implements Serializable {

  private static final long serialVersionUID = 1L;
  
  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  private String id;
  private CloudActionRequest actionRequest;
  private CloudCreateRequest cloudCreateRequest;
  private AutoscaleLabels label;
  private AutoscaleStatus status;
  private String cloudGroupId;
  private String instanceInfo;
  private boolean readonly;

}
