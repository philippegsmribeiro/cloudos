package cloudos.models.autoscaler;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Autoscale Request Repository.
 *
 */
public interface AutoscaleRequestRepository extends MongoRepository<AutoscaleRequest, String> {

}
