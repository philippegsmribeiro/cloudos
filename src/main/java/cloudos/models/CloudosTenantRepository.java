package cloudos.models;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Represents the CloudosTenant model repository class.
 * 
 * @author Rogério Souza
 *
 */
public interface CloudosTenantRepository extends MongoRepository<CloudosTenant, String> {

  public CloudosTenant findByDomainName(String domainName);

  public CloudosTenant findByUsersId(String id);

}
