package cloudos.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CloudActionResponse<T extends AbstractInstance> {

  private String name;
  private T instance;
  private ActionStatus status;
  private String errorMessage;
  private ActionType type;

  public CloudActionResponse(String name, T instance, ActionStatus status, ActionType type) {
    this.name = name;
    this.instance = instance;
    this.status = status;
    this.type = type;
  }

}
