package cloudos.models;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.BooleanPath;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.EnumPath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import javax.annotation.Generated;

/**
 * QInstance is a Querydsl query type for Instance.
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QInstance extends EntityPathBase<Instance> {

  private static final long serialVersionUID = -674769244L;

  public static final QInstance instance = new QInstance("instance");

  public final BooleanPath clientInstalled = createBoolean("clientInstalled");

  public final StringPath clientInstalledCheckDate = createString("clientInstalledCheckDate");

  public final EnumPath<ClientStatus> clientStatus = createEnum("clientStatus", ClientStatus.class);

  public final StringPath cloudGroupId = createString("cloudGrcoupId");

  public final NumberPath<Double> cost = createNumber("cost", Double.class);

  public final DateTimePath<java.util.Date> creationDate =
      createDateTime("creationDate", java.util.Date.class);

  public final BooleanPath deleted = createBoolean("deleted");

  public final DateTimePath<java.util.Date> deletedDate =
      createDateTime("deletedDate", java.util.Date.class);

  public final StringPath id = createString("id");

  public final StringPath imageProject = createString("imageProject");

  public final StringPath imageType = createString("imageType");

  public final DateTimePath<java.util.Date> includedDate =
      createDateTime("includedDate", java.util.Date.class);

  public final StringPath ipAddress = createString("ipAddress");

  public final StringPath key = createString("key");

  public final StringPath machineType = createString("machineType");

  public final StringPath name = createString("name");

  public final EnumPath<cloudos.Providers> provider =
      createEnum("provider", cloudos.Providers.class);

  public final StringPath providerId = createString("providerId");

  public final StringPath region = createString("region");

  public final BooleanPath registered = createBoolean("registered");

  public final BooleanPath reserved = createBoolean("reserved");

  public final StringPath resourceGroup = createString("resourceGroup");

  public final StringPath securityGroup = createString("securityGroup");

  public final BooleanPath spot = createBoolean("spot");

  public final EnumPath<InstanceStatus> status = createEnum("status", InstanceStatus.class);

  public final StringPath tag = createString("tag");

  public final StringPath zone = createString("zone");

  public QInstance(String variable) {
    super(Instance.class, forVariable(variable));
  }

  public QInstance(Path<? extends Instance> path) {
    super(path.getType(), path.getMetadata());
  }

  public QInstance(PathMetadata metadata) {
    super(Instance.class, metadata);
  }

}

