package cloudos.models;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

/** Created by philipperibeiro on 12/16/16. */
public interface AwsKeyPairRepository extends MongoRepository<AwsKeyPair, String> {

  List<AwsKeyPair> findByRegion(String region);

  List<AwsKeyPair> findByValid(Boolean valid);

  AwsKeyPair findByRegionAndKeyname(String region, String keyname);

  AwsKeyPair findByKeyname(String keyname);
}
