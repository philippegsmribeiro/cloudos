package cloudos.models;

import cloudos.config.CloudOSJson;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

@CloudOSJson
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractInstance {

  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  private String id;

  @DBRef
  private Instance instance;
}
