package cloudos.models;

/** Represents a call result status, whether it is a SUCCESS or ERROR. */
public enum CallStatus {

  /** Success result. */
  SUCCESS(0),

  /** Failure result. */
  FAILURE(1);

  private final int signal;

  CallStatus(int signal) {
    this.signal = signal;
  }

  /**
   * Returns the {@link CallStatus} type based in the signal code.
   *
   * @param signal the signal code
   * @return the {@link CallStatus} or null if an invalid signal is provided
   */
  public static CallStatus fromSignal(int signal) {
    for (CallStatus status : CallStatus.values()) {
      if (status.signal == signal) {
        return status;
      }
    }
    return null;
  }
}
