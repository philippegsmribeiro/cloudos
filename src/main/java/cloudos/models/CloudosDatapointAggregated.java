package cloudos.models;

import cloudos.Providers;
import cloudos.data.InstanceDataMetric;
import cloudos.data.InstanceDataUnit;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "cloudos_datapoints_aggregated")
public class CloudosDatapointAggregated extends AbstractCloudosDatapoint {

  private static final long serialVersionUID = 1L;

  public CloudosDatapointAggregated(Providers provider, String instanceId, String region,
      InstanceDataMetric metric, Date timestamp, Double value, InstanceDataUnit unit) {
    super(null, provider, region, instanceId, metric, timestamp, value, unit);
  }


}
