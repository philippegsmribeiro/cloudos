package cloudos.models;

import java.io.Serializable;
import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import cloudos.Providers;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Declare the general Instance class. An instance can be a AmazonInstance, AzureInstance or
 * GoogleInstance.
 */
@Getter
@Setter
@AllArgsConstructor
@Builder
@Document(collection = "cloudos_instances")
@CompoundIndexes({@CompoundIndex(unique = true, dropDups = true, name = "provider_providerId",
    def = "{'provider' : 1, 'providerId': -1}")})
public class Instance implements Serializable {

  private static final long serialVersionUID = 1L;

  public static final String PROVIDER = "provider";
  public static final String CREATION_DATE = "creationDate";
  public static final String ID = "id";
  public static final String NAME = "name";
  public static final String REGION = "region";
  public static final String ZONE = "zone";
  public static final String STATUS = "status";
  public static final String DELETED = "deleted";
  public static final String DELETED_DATE = "deletedDate";
  public static final String MACHINE_TYPE = "machineType";

  // tags
  public static final String DEPLOYMENT_ID_TAG = "depIDTag";
  public static final String CLOUDGROUP_ID_TAG = "cgIDTag";

  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  private String id;
  private Providers provider;
  private String providerId;
  private String region;
  private String zone;
  private String machineType;
  private String imageType;
  private String imageProject;
  private InstanceStatus status;
  private String ipAddress;
  private Boolean clientInstalled;
  private String clientInstalledCheckDate;
  private Boolean registered;
  private Date includedDate;
  private Date creationDate;
  private Date deletedDate;
  private String name;
  private String resourceGroup;
  private Boolean deleted;
  private Double cost;
  private ClientStatus clientStatus;
  private String deploymentId;
  // set them to false by default
  @Builder.Default
  private boolean spot = false;
  @Builder.Default
  private boolean reserved = false;
  private String cloudGroupId;

  private String keyName;

  private String securityGroup;

  public Instance() {
    this.id = new ObjectId().toHexString();
  }


  /**
   * Return a description name, provider.
   *
   * @return the description
   */
  public String toDescriptionString() {
    return String.format("%s[%s]", this.getProviderId(),
        this.getProvider() != null ? this.getProvider().getShortName() : "");
  }
}
