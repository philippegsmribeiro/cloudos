package cloudos.models;

/**
 * Cover all types of amazon load balancers
 * 
 * @author Rogério Souza
 *
 */
public enum AmazonLoadBalancerType {

  Application("application"), 
  Network("network"), 
  Classic("classic");

  private String value;

  AmazonLoadBalancerType(String value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return this.value;
  }

  /**
   * Use this in place of valueOf.
   *
   * @param value real value
   * @return AmazonLoadBalancerType corresponding to the value
   *
   * @throws IllegalArgumentException If the specified value does not map to one of the known values
   *         in this enum.
   */
  public static AmazonLoadBalancerType fromValue(String value) {
    if (value == null || "".equals(value)) {
      throw new IllegalArgumentException("Value cannot be null or empty!");
    }

    for (AmazonLoadBalancerType enumEntry : AmazonLoadBalancerType.values()) {
      if (enumEntry.toString().equals(value)) {
        return enumEntry;
      }
    }

    throw new IllegalArgumentException("Cannot create enum from " + value + " value!");
  }
}
