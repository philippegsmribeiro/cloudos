package cloudos.models;

public enum ClientStatus {
  NOT_INSTALLED,
  INSTALLING,
  INSTALLED,
  RUNNING,
  ERROR
}
