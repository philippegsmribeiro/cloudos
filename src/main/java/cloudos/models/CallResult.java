package cloudos.models;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Back-end call result wrapper.
 *
 * @param <T> the result type
 */
public class CallResult<T> implements Serializable {

  private static final long serialVersionUID = 1L;

  private T result;
  private CallStatus status;
  private Set<String> messages;

  public CallResult() {
    this.status = CallStatus.SUCCESS;
  }

  public CallResult(CallStatus status) {
    this.status = status;
  }

  public CallResult(T result, CallStatus status, Set<String> messages) {
    this.result = result;
    this.status = status;
  }

  /**
   * Sets the status as FAILURE and adds the error message to message collections.
   *
   * @param message the message to be appended to the collection
   */
  public CallResult<T> fail(String message) {
    setStatus(CallStatus.FAILURE);
    if (message != null) {
      getMessages().add(message);
    }
    return this;
  }

  /**
   * Sets the status as FAILURE and adds the error message to message collections.
   *
   * @param messages the messages to be appended to the collection
   */
  public CallResult<T> fail(Set<String> messages) {
    setStatus(CallStatus.FAILURE);
    if (messages != null) {
      getMessages().addAll(messages);
    }
    return this;
  }

  public T getResult() {
    return result;
  }

  public void setResult(T result) {
    this.result = result;
  }

  public CallStatus getStatus() {
    return status;
  }

  public void setStatus(CallStatus status) {
    this.status = status;
  }

  public Set<String> getMessages() {
    return messages == null ? messages = new HashSet<>() : this.messages;
  }

  public void setMessages(Set<String> messages) {
    this.messages = messages;
  }
}
