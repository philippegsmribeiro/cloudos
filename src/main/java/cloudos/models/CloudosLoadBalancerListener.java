package cloudos.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Represents the abstraction of load balancer listeners to be specialized by provider such as:
 * AmazonLoadBalancerListener
 * 
 * @author Rogério Souza
 *
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY,
    property = "providerType", defaultImpl = AmazonLoadBalancerListener.class)
@JsonSubTypes({@JsonSubTypes.Type(value = AmazonLoadBalancerListener.class,
    name = "AmazonLoadBalancerListener")})
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CloudosLoadBalancerListener {

  /**
   * <p>
   * The protocol for connections from clients to the load balancer. For Application Load Balancers,
   * the supported protocols are HTTP and HTTPS. For Network Load Balancers, the supported protocol
   * is TCP.
   * </p>
   */
  private String protocol;
  /**
   * <p>
   * The port on which the load balancer is listening.
   * </p>
   */
  private Integer port;
  /**
   * <p>
   * [HTTPS listeners] The security policy that defines which ciphers and protocols are supported.
   * The default is the current predefined security policy.
   * </p>
   */
  private String sslPolicy;

}
