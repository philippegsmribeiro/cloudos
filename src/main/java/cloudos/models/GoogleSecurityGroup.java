package cloudos.models;

import cloudos.Providers;

/** Created by philipperibeiro on 12/18/16. */
public class GoogleSecurityGroup extends CloudosSecurityGroup {

  public GoogleSecurityGroup() {
    this.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
  }
}
