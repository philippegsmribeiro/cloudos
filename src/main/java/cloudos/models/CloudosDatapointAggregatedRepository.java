package cloudos.models;

import cloudos.Providers;
import cloudos.data.InstanceDataMetric;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface CloudosDatapointAggregatedRepository
    extends MongoRepository<CloudosDatapointAggregated, String> {

  List<CloudosDatapointAggregated> findByInstanceIdAndProviderAndMetricAndTimestampBetween(
      String instanceId,
      Providers provider,
      InstanceDataMetric metric,
      Date startDate,
      Date endDate);

  List<CloudosDatapointAggregated> findByInstanceIdInAndProviderAndMetricAndTimestampBetween(
      List<String> instanceId,
      Providers provider,
      InstanceDataMetric metric,
      Date startDate,
      Date endDate);
}
