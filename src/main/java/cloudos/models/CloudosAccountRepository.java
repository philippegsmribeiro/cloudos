package cloudos.models;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Represents the CloudosAccount model repository class.
 * 
 * @author Rogério Souza
 *
 */

public interface CloudosAccountRepository extends MongoRepository<CloudosAccount, String> {

}
