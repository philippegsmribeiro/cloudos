package cloudos.models;

import org.codehaus.jackson.annotate.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Created by philipperibeiro on 7/8/17. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Shards {

  @JsonProperty(value = "total")
  private Integer total;

  @JsonProperty(value = "successful")
  private Integer successful;

  @JsonProperty(value = "failed")
  private Integer failed;
}
