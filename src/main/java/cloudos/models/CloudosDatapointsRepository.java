package cloudos.models;

import cloudos.Providers;
import cloudos.data.InstanceDataMetric;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

/** Created by philipperibeiro on 3/21/17. */
public interface CloudosDatapointsRepository extends MongoRepository<CloudosDatapoint, String> {

  List<CloudosDatapoint> findByCloudId(String cloudId);

  List<CloudosDatapoint> findByProvider(Providers provider);

  List<CloudosDatapoint> findByProvider(Providers provider, Pageable pageable);

  List<CloudosDatapoint> findByInstanceId(String instanceId);

  List<CloudosDatapoint> findByMetric(InstanceDataMetric metric);

  List<CloudosDatapoint> findByProviderAndMetric(Providers provider, InstanceDataMetric metric);

  List<CloudosDatapoint> findAllByOrderByTimestamp();

  List<CloudosDatapoint> findTop100ByOrderByTimestamp();
}
