package cloudos.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import cloudos.Providers;
import lombok.Data;

/** Created by gleimar on 20/05/2017. */
@Document(collection = "cloudos_security_group")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type",
    defaultImpl = AwsSecurityGroup.class)
@JsonSubTypes({@JsonSubTypes.Type(value = AwsSecurityGroup.class, name = "AwsSecurityGroup"),
    @JsonSubTypes.Type(value = AzureSecurityGroup.class, name = "AzureSecurityGroup"),
    @JsonSubTypes.Type(value = GoogleSecurityGroup.class, name = "GoogleSecurityGroup")})
@Data
public class CloudosSecurityGroup {

  @Id
  private String id;
  private Providers provider;
  private String name;
  private String description;
  private String region;
  private Date creation;

  private List<CloudosIpPermission> inbounds = new ArrayList<>();
  private List<CloudosIpPermission> outbounds = new ArrayList<>();

  public CloudosSecurityGroup() {
    this.creation = new Date();
  }

}
