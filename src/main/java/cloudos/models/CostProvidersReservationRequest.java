package cloudos.models;

import cloudos.Providers;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The request model is used to request the costs of a provider for a reservation type.
 * 
 * @author Alex Calagua
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CostProvidersReservationRequest {

  /**
   * The provider the cloud provider.
   */
  private Providers provider;

  /**
   * The reservation the reservation type of provider aws and google.
   */
  private String reservation;

}
