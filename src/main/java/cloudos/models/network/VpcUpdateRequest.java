package cloudos.models.network;

import cloudos.Providers;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class VpcUpdateRequest {

  private Providers provider;
  private String region;
  private String vpcId;
  private String name;
  private String ipv4Cidr;
  private Boolean isIpv6Cidr;

}
