package cloudos.models.network;

import java.util.Date;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import cloudos.Providers;
import cloudos.utils.ReflectionToJson;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document(collection = "cloudos_subnet")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type",
    defaultImpl = AmazonSubnet.class)
@JsonSubTypes({@JsonSubTypes.Type(value = AmazonSubnet.class, name = "AmazonSubnet"),
    @JsonSubTypes.Type(value = GoogleSubnet.class, name = "GoogleSubnet")})
@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class CloudosSubnet {

  protected String name;
  protected Providers provider;
  protected String vpc;
  protected String subnetId;
  protected String cidr;
  protected String availabilityZone;
  protected Boolean isIpv6;

  protected Boolean isPrivateSubnet;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  protected Date creation;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  protected Date deleted;

  /**
   * Constructor with params passed as arguments.
   *
   * @param name the name of the subnet
   * @param provider the name of the provider
   * @param vpc the vpc the subnet belongs to
   * @param cidr the list of CIDR blocks associated with this VPC
   * @param availabilityZone the subnet's availability zone
   */
  public CloudosSubnet(String name, Providers provider, String vpc, String cidr,
      String availabilityZone) {
    this.setAvailabilityZone(availabilityZone);
    this.setCidr(cidr);
    this.setName(name);
    this.setProvider(provider);
    this.setVpc(vpc);
    this.setIsIpv6(false);
    this.setIsPrivateSubnet(false);
  }

}
