package cloudos.models.network;

import java.util.Date;
import java.util.Set;

import cloudos.Providers;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class GoogleVpc extends CloudosVpc {

  public final Providers providers = Providers.GOOGLE_COMPUTE_ENGINE;

  private String url;
  
  /**
   * Constructor for use @Builder and inheritance.
   */
  @Builder
  public GoogleVpc(Providers provider, String vpcId, String region, String name,
      Set<String> subnets, Date creation, Date deleted, String url) {
    super(provider, vpcId, region, name);
    this.creation = creation;
    this.deleted = deleted;
    this.url = url;
    this.subnets = subnets;
  }


}
