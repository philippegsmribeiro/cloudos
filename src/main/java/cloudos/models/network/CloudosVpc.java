package cloudos.models.network;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import cloudos.Providers;
import cloudos.utils.ReflectionToJson;
import lombok.Getter;
import lombok.Setter;

@Document(collection = "cloudos_vpc")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "type",
    defaultImpl = AwsVpc.class
)
@JsonSubTypes({
    @JsonSubTypes.Type(value = AwsVpc.class, name = "AwsVpc"),
    @JsonSubTypes.Type(value = GoogleVpc.class, name = "GoogleVpc")
})
@Data
public abstract class CloudosVpc {

  protected Providers provider;
  protected String vpcId;
  protected String region;

  protected String name;
  protected Set<String> subnets;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  protected Date creation;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  protected Date deleted;

  public CloudosVpc() {
    this.creation = new Date();
    this.subnets = new HashSet<>();
  }

  /**
   * Constructor for the CloudosVpc class.
   *
   * @param provider the name of the provider
   * @param vpcId the VPC id
   * @param region the region the VPC belongs to
   */
  public CloudosVpc(Providers provider, String vpcId, String region) {
    this.setProvider(provider);
    this.setVpcId(vpcId);
    this.setRegion(region);
    this.subnets = new HashSet<>();
  }

  /**
   * Constructor for the CloudosVpc class.
   *
   * @param provider the name of the provider
   * @param vpcId the VPC id
   * @param region the region the VPC belongs to
   * @param name the name of the vpc
   */
  public CloudosVpc(Providers provider, String vpcId, String region, String name) {
    this.setProvider(provider);
    this.setVpcId(vpcId);
    this.setRegion(region);
    this.setName(name);
    this.subnets = new HashSet<>();
  }
}
