package cloudos.models.network;

import java.util.List;

import com.amazonaws.services.ec2.model.Tag;

import cloudos.Providers;
import cloudos.utils.ReflectionToJson;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Implement a request model for CloudosVpc. The request model is used to create a new CloudosVpc,
 * either a AwsVpc or a GoogleVpc.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CloudosVpcRequest {

  private Providers provider;
  private String region;
  private String cidrBlock;
  private List<Tag> tags;
  private Boolean ipv6CidrBlock;
  private String instanceTenancy;
  private String name;
}
