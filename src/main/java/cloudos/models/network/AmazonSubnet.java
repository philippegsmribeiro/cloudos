package cloudos.models.network;

import java.util.List;

import com.amazonaws.services.ec2.model.Subnet;
import com.amazonaws.services.ec2.model.SubnetIpv6CidrBlockAssociation;
import com.amazonaws.services.ec2.model.Tag;

import cloudos.Providers;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@Data
@NoArgsConstructor
public class AmazonSubnet extends CloudosSubnet {

  private List<Tag> tags;
  private List<SubnetIpv6CidrBlockAssociation> ipv6CidrBlockAssociationSet;

  /**
   * Constructor uses a Amazon Subnet object to initialize its fields.
   *
   * @param subnet a Amazon Subnet object
   */
  public AmazonSubnet(Subnet subnet) {
    this.setSubnetId(subnet.getSubnetId());
    this.setVpc(subnet.getVpcId());
    this.setProvider(Providers.AMAZON_AWS);
    this.setCidr(subnet.getCidrBlock());
    this.setAvailabilityZone(subnet.getAvailabilityZone());
    this.setTags(subnet.getTags());
    this.setIpv6CidrBlockAssociationSet(subnet.getIpv6CidrBlockAssociationSet());
    this.setIsIpv6(subnet.isAssignIpv6AddressOnCreation());
    for (Tag tag : subnet.getTags()) {
      if (tag.getKey().equals("Name")) {
        this.setName(tag.getValue());
        break;
      }
    }
  }
}
