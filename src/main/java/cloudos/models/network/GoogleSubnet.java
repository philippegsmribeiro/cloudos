package cloudos.models.network;

import java.util.Date;
import cloudos.Providers;
import cloudos.models.network.CloudosSubnet;
import lombok.Builder;

public class GoogleSubnet extends CloudosSubnet {

  /**
   * Constructor for use @Builder and inheritance.
   */
  @Builder
  public GoogleSubnet(String availabilityZone, String cidr, Boolean isIpv6, String name,
      Boolean isPrivateSubnet, Date creation, Date deleted, String vpc, String subnetId) {
    super(name, Providers.GOOGLE_COMPUTE_ENGINE, vpc, cidr, availabilityZone);
    this.isIpv6 = isIpv6;
    this.isPrivateSubnet = isPrivateSubnet;
    this.creation = creation;
    this.deleted = deleted;
    this.subnetId = subnetId;
  }

}
