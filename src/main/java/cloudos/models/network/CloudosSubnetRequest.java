package cloudos.models.network;

import cloudos.Providers;
import cloudos.utils.ReflectionToJson;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CloudosSubnetRequest {

  private String name;
  private Providers provider;
  private String vpc;
  private String cidr;
  private String availabilityZone;
  private Boolean isIpv6;
  private Boolean isPrivateSubnet;

}
