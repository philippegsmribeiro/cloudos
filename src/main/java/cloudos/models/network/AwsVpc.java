package cloudos.models.network;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.amazonaws.services.ec2.model.Tag;
import com.amazonaws.services.ec2.model.VpcIpv6CidrBlockAssociation;

import cloudos.Providers;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AwsVpc extends CloudosVpc {

  public final Providers providers = Providers.AMAZON_AWS;

  private String availabilityZone;
  private Boolean isIpv6;
  private Boolean isPrivateSubnet;
  private String instanceTenancy;
  private Boolean isVpcDeleted;
  private String cidrBlock;
  private List<Tag> tags;
  private String state;
  private Boolean isDefault;
  private String dhcpOptionsId;
  private List<VpcIpv6CidrBlockAssociation> ipv6CidrBlockAssociationSet;

  @Builder
  public AwsVpc(Providers provider, String vpcId, String region, String name,
      Set<String> subnets, Date creation, Date deleted, String state, String availabilityZone,
      Boolean isDefault,  Boolean isIpv6, Boolean isPrivateSubnet, String instanceTenancy,
      Boolean isVpcDeleted, List<Tag> tags, String dhcpOptionsId, String cidrBlock,
      List<VpcIpv6CidrBlockAssociation> ipv6CidrBlockAssociationSet) {
    super(provider, vpcId, region, name);
    this.subnets = subnets;
    this.creation = creation;
    this.region = region;
    this.state = state;
    this.deleted = deleted;
    this.availabilityZone = availabilityZone;
    this.isDefault = isDefault;
    this.isIpv6 = isIpv6;
    this.isPrivateSubnet = isPrivateSubnet;
    this.instanceTenancy = instanceTenancy;
    this.isVpcDeleted = isVpcDeleted;
    this.tags = tags;
    this.dhcpOptionsId = dhcpOptionsId;
    this.cidrBlock = cidrBlock;
    this.ipv6CidrBlockAssociationSet = ipv6CidrBlockAssociationSet;

  }

}
