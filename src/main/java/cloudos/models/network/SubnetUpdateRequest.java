package cloudos.models.network;

import cloudos.Providers;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SubnetUpdateRequest {

  private Providers provider;
  private String vpcId;
  private String subnetId;
  private String region;
  private String availabilityZone;
  private String name;
  private Boolean isIpv6;
  private Boolean mapPublicIpOnLaunch;

}
