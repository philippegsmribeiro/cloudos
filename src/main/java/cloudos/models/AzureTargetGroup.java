package cloudos.models;

import cloudos.Providers;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AzureTargetGroup extends CloudosTargetGroup {

  public AzureTargetGroup() {
    this.setProvider(Providers.MICROSOFT_AZURE);
  }
}
