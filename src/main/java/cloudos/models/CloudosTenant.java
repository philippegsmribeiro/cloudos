package cloudos.models;

import cloudos.user.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Represents the tenant model.
 * 
 * @author Rogério Souza
 *
 */

@Data
@Document(collection = "cloudos_tenant")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CloudosTenant {

  @Id
  private String id;

  @NotEmpty
  private String accountName;

  @NotEmpty
  private String domainName;

  @NotEmpty
  private String region;

  @NotEmpty
  private String companySize;

  @NotEmpty
  private String companyName;

  @DBRef(lazy = false)
  private List<CloudosAccount> accounts = new ArrayList<>();

  @DBRef(lazy = false)
  private List<User> users = new ArrayList<>();

}
