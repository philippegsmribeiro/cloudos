package cloudos.models;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import cloudos.Providers;

/** Created by gleimar on 20/05/2017. */
public interface CloudosCredentialRepository extends MongoRepository<CloudosCredential, String> {

  List<CloudosCredential> findByProvider(Providers providers);

  CloudosCredential findById(String id);
}
