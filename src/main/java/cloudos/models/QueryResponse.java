package cloudos.models;

import org.codehaus.jackson.annotate.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Created by philipperibeiro on 7/8/17. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class QueryResponse<T> {

  private Long took;

  @JsonProperty(value = "timed_out")
  private Boolean timeOut;

  @JsonProperty(value = "_shards")
  private Shards shards;

  private Hits<T> hits;
}
