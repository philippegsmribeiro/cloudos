package cloudos.models;

import cloudos.utils.JavaSparkContextBuilder;

import com.mongodb.spark.MongoSpark;
import com.mongodb.spark.config.ReadConfig;
import com.mongodb.spark.config.WriteConfig;
import com.mongodb.spark.rdd.api.java.JavaMongoRDD;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import lombok.extern.log4j.Log4j2;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.bson.Document;

/**
 * Created by philipperibeiro on 6/23/17.
 *
 * <p>Implement SparkJob class, which is responsible for connecting to a Spark Master and executing
 * tasks. The goal is to maintain a single SparkContext running, so any job that may need Spark can
 * inherit from the
 */
@Log4j2
public abstract class SparkJob {

  protected static JavaSparkContext context;

  /** Default constructor for the SparkJob class. */
  public SparkJob() {
    context = JavaSparkContextBuilder.defaultContext();
  }

  /**
   * Assign the JavaSparkContext to the context that connects to a Mongo Collection.
   *
   * @param inputUri the name of the input collection
   * @param outputUri the name of the output collection
   */
  public SparkJob(String inputUri, String outputUri) {
    context = JavaSparkContextBuilder.mongo(inputUri, outputUri);
  }

  /**
   * Assign the JavaSparkContext to the context.
   *
   * @param sc a JavaSparkContext context
   */
  public SparkJob(JavaSparkContext sc) {
    context = sc;
  }

  /**
   * Return all the entries currently in the Mongo collection specified in the constructor.
   *
   * @return a Java RDD of the documents in the collection
   */
  public JavaRDD<Document> read() {
    return MongoSpark.load(context);
  }

  /**
   * Return all the entries currently in the Mongo collection specified in the constructor, but
   * allow the user the override default configuration.
   *
   * @param readOverrides a Map containing the values to be configured to.
   * @return a Java RDD of the documents in the collection
   */
  public JavaRDD<Document> read(Map<String, String> readOverrides) {
    ReadConfig readConfig = ReadConfig.create(context).withOptions(readOverrides);
    return MongoSpark.load(context, readConfig);
  }

  /**
   * Given a query, apply that to a collection in order to allow a user to filter data in MongoDB
   * and then pass only the matching documents to Spark.
   *
   * @param query a mongo String query
   * @return a Java RDD of the documents in the collection
   */
  public JavaRDD<Document> aggregate(String query) {
    JavaMongoRDD<Document> rdd = MongoSpark.load(context);
    return rdd.withPipeline(Collections.singletonList(Document.parse(query)));
  }

  /**
   * Write a list of elements of type T, and allows the user to override default write
   * configurations.
   *
   * @param elements a list of elements to be written in to the collection
   * @param writeOverrides whether to override the existing entries
   * @return if successful in writing or not
   */
  public <T> boolean write(List<T> elements, Map<String, String> writeOverrides) {
    try {
      // Override the default options
      WriteConfig writeConfig = WriteConfig.create(context).withOptions(writeOverrides);
      JavaRDD<Document> documents = context.parallelize(elements).map((new Function<T, Document>() {
        private static final long serialVersionUID = 1L;

        // not sure about this ... probably will have to fix
        @Override
        public Document call(final T element) throws Exception {
          return Document.parse("{test: " + element + "}");
        }
      }));
      MongoSpark.save(documents, writeConfig);
      return true;
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return false;
    }
  }

  /**
   * Write a list of elements of type T into the default mongo collection.
   *
   * @param elements a list of elements of type T to be inserted into Mongo
   * @return true if successfull, false otherwise
   */
  public <T> boolean write(List<T> elements) {
    try {
      JavaRDD<Document> documents = context.parallelize(elements).map((new Function<T, Document>() {
        private static final long serialVersionUID = 1L;

        // not sure about this ... probably will have to fix
        @Override
        public Document call(final T element) throws Exception {
          return Document.parse("{test: " + element + "}");
        }
      }));
      MongoSpark.save(documents);
      return true;
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return false;
    }
  }

  /**
   * Write a Document in the mongoDB.
   *
   * @param documents a list of RDD documents to be written to the Mongo collection
   * @return true if successfull, false otherwise
   */
  public boolean write(JavaRDD<Document> documents) {
    try {
      MongoSpark.save(documents);

      return true;
    } catch (Throwable t) {
      log.error("error save document in mongo", t);
    }
    return false;
  }

  /** Stop a current running JavaSparkContext. */
  public void stop() {
    if (context != null) {
      context.stop();
    }
  }

  /** Close a current running JavaSparkContext. */
  public void close() {
    if (context != null) {
      context.close();
    }
  }
}
