package cloudos.models;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.elasticloadbalancing.model.HealthCheck;

import cloudos.Providers;
import cloudos.utils.ReflectionToJson;
import lombok.Builder;
import lombok.Data;

@Data
public class AmazonLoadBalancer extends CloudosLoadBalancer {

  /**
   * <p>
   * The Amazon Resource Name (ARN) of the load balancer.
   * </p>
   */
  private String loadBalancerArn;

  /**
   * <p>
   * The IDs of the subnets to attach to the load balancer. You can specify only one subnet per
   * Availability Zone. You must specify either subnets or subnet mappings.
   * </p>
   * <p>
   * [Application Load Balancers] You must specify subnets from at least two Availability Zones.
   * </p>
   */
  private java.util.List<String> subnets = new ArrayList<>();

  /**
   * <p>
   * [Application Load Balancers] The IDs of the security groups to assign to the load balancer.
   * </p>
   */
  private java.util.List<String> securityGroups = new ArrayList<>();;

  /**
   * <p>
   * The nodes of an Internet-facing load balancer have public IP addresses. The DNS name of an
   * Internet-facing load balancer is publicly resolvable to the public IP addresses of the nodes.
   * Therefore, Internet-facing load balancers can route requests from clients over the Internet.
   * </p>
   * <p>
   * The nodes of an internal load balancer have only private IP addresses. The DNS name of an
   * internal load balancer is publicly resolvable to the private IP addresses of the nodes.
   * Therefore, internal load balancers can only route requests from clients with access to the VPC
   * for the load balancer.
   * </p>
   * <p>
   * The default is an Internet-facing load balancer.
   * </p>
   */
  private String scheme;

  /**
   * <p>
   * The type of load balancer to create. .
   * </p>
   */
  private AmazonLoadBalancerType type;

  /**
   * <p>
   * [Application Load Balancers] The type of IP addresses used by the subnets for your load
   * balancer. The possible values are <code>ipv4</code> (for IPv4 addresses) and
   * <code>dualstack</code> (for IPv4 and IPv6 addresses). Internal load balancers must use
   * <code>ipv4</code>.
   * </p>
   */
  private String ipAddressType;

  /**
   * <p>
   * The listeners.
   * </p>
   */
  private List<AmazonLoadBalancerListener> listeners = new ArrayList<>();

  /**
   * <p>
   * One or more Availability Zones from the same region as the load balancer.
   * </p>
   * <p>
   * You must specify at least one Availability Zone.
   * </p>
   * <p>
   * You can add more Availability Zones after you create the load balancer using
   * <a>EnableAvailabilityZonesForLoadBalancer</a>.
   * </p>
   */
  private List<String> availabilityZones = new ArrayList<>();

  /**
   * <p>
   * The configuration information.
   * </p>
   */
  private HealthCheck healthCheck;

  @Builder
  public AmazonLoadBalancer(String id, Providers provider, String region, String name,
      String loadBalancerArn, List<String> subnets, List<String> securityGroups, String scheme,
      AmazonLoadBalancerType type, String ipAddressType, List<AmazonLoadBalancerListener> listeners,
      List<String> availabilityZones, String vpcId, HealthCheck healthCheck) {
    super(id, provider, region, name);
    this.loadBalancerArn = loadBalancerArn;
    this.subnets = subnets;
    this.securityGroups = securityGroups;
    this.scheme = scheme;
    this.type = type;
    this.ipAddressType = ipAddressType;
    this.listeners = listeners;
    this.availabilityZones = availabilityZones;
    this.healthCheck = healthCheck;
  }


  public AmazonLoadBalancer() {
    super();
    this.setProvider(Providers.AMAZON_AWS);
  }


  @Override
  public String toString() {
    return ReflectionToJson.toString(this);
  }



}
