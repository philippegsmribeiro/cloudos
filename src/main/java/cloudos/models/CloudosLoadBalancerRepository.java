package cloudos.models;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import cloudos.Providers;

/**
 * Represents the CloudosLoadBalancer model repository class.
 * 
 * @author Rogério Souza
 *
 */
public interface CloudosLoadBalancerRepository
    extends MongoRepository<CloudosLoadBalancer, String> {

  List<CloudosSecurityGroup> findById(String id);

  List<CloudosSecurityGroup> findByProvider(Providers providers);

}
