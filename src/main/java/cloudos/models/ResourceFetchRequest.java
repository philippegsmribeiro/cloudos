package cloudos.models;

import java.util.List;

import cloudos.Providers;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ResourceFetchRequest implements Cloneable {

  private Providers provider;
  private String name;
  private String resourceGroup;
  private String resourceType;
  private String region;
  private String zone;
  private List<InstanceStatus> instanceStatus;
  private int page;
  private int pageSize;

  public ResourceFetchRequest() {
    page = 0;
    pageSize = 10;
  }

  @Override
  public ResourceFetchRequest clone() {
    try {
      return (ResourceFetchRequest) super.clone();
    } catch (CloneNotSupportedException e) {
      e.printStackTrace();
    }
    return null;
  }
}
