package cloudos.models;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import cloudos.Providers;
import cloudos.google.GoogleCredential;
import lombok.Data;

/** Created by gleimar on 20/05/2017. */
@Data
@Document(collection = "cloudos_credential")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "provider")
@JsonSubTypes({@JsonSubTypes.Type(value = AwsCredential.class, name = "AMAZON_AWS"),
    @JsonSubTypes.Type(value = AzureCredential.class, name = "MICROSOFT_AZURE"),
    @JsonSubTypes.Type(value = GoogleCredential.class, name = "GOOGLE_COMPUTE_ENGINE")})
public class CloudosCredential {

  @Id
  private String id;
  private Providers provider;
  private boolean decrypted = false;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  private Date creation;

  public CloudosCredential() {
    this.creation = new Date();
  }

}
