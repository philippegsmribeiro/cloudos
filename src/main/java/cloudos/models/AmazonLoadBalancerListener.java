package cloudos.models;

import java.util.List;

import com.amazonaws.services.elasticloadbalancingv2.model.Action;
import com.amazonaws.services.elasticloadbalancingv2.model.Certificate;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Represents the amazon load balancer listener.
 * 
 * @author Rogério Souza
 *
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AmazonLoadBalancerListener extends CloudosLoadBalancerListener {

  /**
   * <p>
   * The Amazon Resource Name (ARN) of the listener.
   * </p>
   */
  private String listenerArn;

  /**
   * <p>
   * The port on which the load balancer is listening. On EC2-VPC, you can specify any port from the
   * range 1-65535. On EC2-Classic, you can specify any port from the following list: 25, 80, 443,
   * 465, 587, 1024-65535.
   * </p>
   */
  private Integer loadBalancerPort;
  /**
   * <p>
   * The protocol to use for routing traffic to instances: HTTP, HTTPS, TCP, or SSL.
   * </p>
   * <p>
   * If the front-end protocol is HTTP, HTTPS, TCP, or SSL, <code>InstanceProtocol</code> must be at
   * the same protocol.
   * </p>
   * <p>
   * If there is another listener with the same <code>InstancePort</code> whose
   * <code>InstanceProtocol</code> is secure, (HTTPS or SSL), the listener's
   * <code>InstanceProtocol</code> must also be secure.
   * </p>
   * <p>
   * If there is another listener with the same <code>InstancePort</code> whose
   * <code>InstanceProtocol</code> is HTTP or TCP, the listener's <code>InstanceProtocol</code> must
   * be HTTP or TCP.
   * </p>
   */
  private String instanceProtocol;

  /**
   * <p>
   * The port on which the instance is listening.
   * </p>
   */
  private Integer instancePort;

  /**
   * <p>
   * The Amazon Resource Name (ARN) of the server certificate.
   * </p>
   */
  private String sSLCertificateId;

  /**
   * <p>
   * [HTTPS listeners] The SSL server certificate. You must provide exactly one certificate.
   * </p>
   */
  private java.util.List<Certificate> certificates;
  /**
   * <p>
   * The default action for the listener. For Application Load Balancers, the protocol of the
   * specified target group must be HTTP or HTTPS. For Network Load Balancers, the protocol of the
   * specified target group must be TCP.
   * </p>
   */
  private java.util.List<Action> defaultActions;

  @Builder
  public AmazonLoadBalancerListener(String protocol, Integer port, String sslPolicy,
      String listenerArn, Integer loadBalancerPort, String instanceProtocol, Integer instancePort,
      String sSLCertificateId, List<Certificate> certificates, List<Action> defaultActions) {
    super(protocol, port, sslPolicy);
    this.listenerArn = listenerArn;
    this.loadBalancerPort = loadBalancerPort;
    this.instanceProtocol = instanceProtocol;
    this.instancePort = instancePort;
    this.sSLCertificateId = sSLCertificateId;
    this.certificates = certificates;
    this.defaultActions = defaultActions;
  }
}
