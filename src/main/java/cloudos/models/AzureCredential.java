package cloudos.models;

import cloudos.Providers;

import lombok.Data;

/**
 * Created by Gleimar on 21/21/17.
 * Store Azure credential in mongodb
 */
@Data
public class AzureCredential extends CloudosCredential {

  private String client;
  private String tenant;
  private String key;

  /**
   * Default constructor.
   */
  public AzureCredential() {
    this.setProvider(Providers.MICROSOFT_AZURE);
  }

  /**
   * Constructor receiving keys.
   * @param client identifier
   * @param tenant identifier
   * @param key password
   */
  public AzureCredential(String client, String tenant, String key) {
    this();
    this.client = client;
    this.tenant = tenant;
    this.key = key;
    setDecrypted(true);
  }
}
