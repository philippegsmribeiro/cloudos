package cloudos.models.costanalysis;

import java.io.Serializable;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class OfferingReport implements Serializable {

  private static final long serialVersionUID = 1L;

  private OfferingType type;

  private BigDecimal cost;

  private BigDecimal discount;

  private BigDecimal totalCost;
  
}
