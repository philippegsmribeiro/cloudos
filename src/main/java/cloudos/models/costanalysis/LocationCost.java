package cloudos.models.costanalysis;

import java.io.Serializable;
import java.math.BigDecimal;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Represents a billing location cost.
 */
@Getter
@Setter
@ToString
public class LocationCost extends BillingCost implements Serializable {

  private static final long serialVersionUID = 1L;

  private String location;

  /**
   * Constructor that initializes location, cost, discount and totalCost.
   * 
   * @param location the location
   * @param cost the cost
   */
  public LocationCost(String location, BigDecimal cost) {
    super(cost);
    this.location = location;
  }

}
