package cloudos.models.costanalysis;

import cloudos.Providers;
import java.io.Serializable;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


/**
 * Represents a billing account.
 */
@Data
@NoArgsConstructor
@Document(collection = "cloudos_billing_account")
public class BillingAccount implements Serializable {

  @Id
  private String id;

  private String accountId;

  private Providers provider;

  /**
   * Constructor with all fields.
   *
   * @param accountId the billing account id
   * @param provider the billing provider
   */
  public BillingAccount(String accountId, Providers provider) {
    super();
    this.accountId = accountId;
    this.provider = provider;
    this.id = (provider + accountId);
  }
}
