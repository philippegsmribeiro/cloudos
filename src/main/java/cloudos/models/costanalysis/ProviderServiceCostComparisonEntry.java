package cloudos.models.costanalysis;

import cloudos.Providers;
import cloudos.models.services.CloudosService;
import cloudos.models.services.ServiceCategory;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

/**
 * DTO for provider service costs between month to day, 30 and 60 days.
 */
@Data
@Document(collection = "costanalysis_provider_service_cost_comparison")
public class ProviderServiceCostComparisonEntry implements Serializable {

  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  private String id;

  @JsonProperty("serviceCategory")
  private ServiceCategory serviceCategory;

  @JsonProperty("provider")
  private Providers provider;

  @JsonProperty("monthToDateCost")
  private TemporalCost monthToDateCost;

  @JsonProperty("total30DaysCost")
  private TemporalCost total30DaysCost;

  @JsonProperty("total60DaysCost")
  private TemporalCost total60DaysCost;

  @DateTimeFormat(iso = ISO.DATE_TIME)
  private Date timestamp;

}
