package cloudos.models.costanalysis;

import cloudos.Providers;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Filter DTO for generic billing report.
 */
@Getter
@Setter
@NoArgsConstructor
public class GenericBillingReportQueryFilter implements Serializable {

  private String lineItemId;

  private Providers provider;

  private String accountId;

  private String productCode;

  private String resourceId;

  private Date startDateAfterOrEqualTo;

  private Date startDateBeforeOrEqualTo;

  private BigDecimal usageCostGreaterThanOrEqualTo;

  private BigDecimal usageCostLowerThanOrEqualTo;

  private String location;
}
