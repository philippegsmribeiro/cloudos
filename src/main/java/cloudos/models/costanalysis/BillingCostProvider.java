package cloudos.models.costanalysis;

import cloudos.Providers;

import java.io.Serializable;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * TO object to Billing Cost the Month Estimated.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BillingCostProvider implements Serializable {

  private static final long serialVersionUID = 1L;
  private Providers provider;
  private BigDecimal estimatedCost;
  private BigDecimal costPreviousMonth;

}
