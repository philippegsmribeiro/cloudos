package cloudos.models.costanalysis;

import cloudos.models.services.ServiceCategory;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ServiceCategoryCost {

  private ServiceCategory category;

  private Double cost;

}
