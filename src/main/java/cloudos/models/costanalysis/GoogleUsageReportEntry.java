package cloudos.models.costanalysis;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.ser.std.ToStringSerializer;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

/**
 * Represents a Google Compute Engine usage report entry.
 */
@Getter
@Setter
@NoArgsConstructor
@Document(collection = "cloudos_google_usage_report")
public class GoogleUsageReportEntry implements Serializable {

  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  private String id;

  @DateTimeFormat(iso = ISO.DATE)
  private LocalDate reportDate;

  @JsonProperty
  private String measurementId;

  @JsonProperty
  private BigDecimal quantity;

  @JsonProperty
  private String unit;

  @JsonProperty
  private String resourceUri;

  @JsonProperty
  private String resourceId;

  @JsonProperty
  private String location;

  @JsonProperty
  private Long numericProjectId;

}
