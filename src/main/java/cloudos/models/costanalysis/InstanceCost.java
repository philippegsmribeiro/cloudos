package cloudos.models.costanalysis;

import cloudos.Providers;
import com.opencsv.bean.CsvBindByName;
import java.io.Serializable;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class InstanceCost implements Serializable {

  private static final long serialVersionUID = 1L;
  @CsvBindByName
  private Providers provider;
  @CsvBindByName
  private String instance;
  @CsvBindByName
  private String region;
  @CsvBindByName
  private BigDecimal cost;
  @CsvBindByName
  private BigDecimal totalCost;
  @CsvBindByName
  private BigDecimal discount;

}
