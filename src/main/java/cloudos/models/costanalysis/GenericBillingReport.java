package cloudos.models.costanalysis;

import cloudos.Providers;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.querydsl.core.annotations.QueryEntity;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Generic billing report collection representation.
 */
@Getter
@Setter
@EqualsAndHashCode
@ToString
@QueryEntity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Document(collection = "cloudos_billing_reports")
public class GenericBillingReport implements Serializable {

  private static final long serialVersionUID = 1L;
  // DATABASE COLLECTION FIELDS
  public static final String FIELD_USAGE_START_TIME = "usageStartTime";
  public static final String FIELD_USAGE_COST = "usageCost";

  @Id
  private String id;

  private String lineItemId;

  private Providers provider;

  private String accountId;

  private String productCode;

  private String resourceId;

  @Indexed(background = true, direction = IndexDirection.DESCENDING)
  private Date usageStartTime;

  private Date usageEndTime;

  private BigDecimal usageCost;

  private String location;
  
  private boolean credit;
}
