package cloudos.models.costanalysis;

/**
 * Represents a provider offering type.
 */
public enum OfferingType {

  /**
   * Reserved.
   */
  RESERVED,

  /**
   * Spot.
   */
  SPOT,

  /**
   * On demand.
   */
  ON_DEMAND

}
