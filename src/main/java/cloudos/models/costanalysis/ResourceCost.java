package cloudos.models.costanalysis;

import cloudos.Providers;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder(value = {"provider", "resourceId"})
public class ResourceCost extends BillingCost implements Serializable {

    private static final long serialVersionUID = 1L;

    private Providers provider;

    private String resourceId;

    /**
     * Constructor that initializes all local fields and parent fields.
     */
    public ResourceCost(Providers provider, String resourceId, BigDecimal cost) {
        super(cost);
        this.provider = provider;
        this.resourceId = resourceId;
    }
}
