package cloudos.models.costanalysis;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * TO object to Billing Cost the Month Estimated.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BillingEstimatedCostMonth implements Serializable {

  private static final long serialVersionUID = 1L;
  private String month;
  private BigDecimal estimatedCost;
  private BigDecimal costPreviousMonth;
  private List<BillingCostProvider> providers;

  /**
   * Method that adds an element to the list.
   *
   * @param costProvider the billing cost provider
   */
  public void addCostProvider(BillingCostProvider costProvider) {
    if (providers == null) {
      providers = new ArrayList<>();
    }
    providers.add(costProvider);
  }


}
