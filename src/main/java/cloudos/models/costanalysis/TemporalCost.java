package cloudos.models.costanalysis;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

/**
 * DTO to store cost and the start and end dates.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class TemporalCost implements Serializable {

  @DateTimeFormat(iso = ISO.DATE)
  private Date startDate;

  @DateTimeFormat(iso = ISO.DATE)
  private Date endDate;

  /**
   * The usage cost, without the discount.
   */
  private BigDecimal cost = BigDecimal.ZERO;

  /**
   * The discount.
   */
  private BigDecimal discount = BigDecimal.ZERO;

  /**
   * The cost plus the discount.
   */
  private BigDecimal totalCost = BigDecimal.ZERO;

  public TemporalCost(Date startDate, Date endDate) {
    this.startDate = startDate;
    this.endDate = endDate;
  }
}
