package cloudos.models.costanalysis;

import cloudos.Providers;
import java.io.Serializable;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class OfferingUtilizationReport implements Serializable {

  private static final long serialVersionUID = 1L;

  private Providers provider;

  private OfferingType type;

  private BillingCost cost;

  private BigDecimal hours;

  private Long numberOfInstances;

  /**
   * Constructor.
   * 
   * @param provider the cloud provider
   * @param spot whether the offering type is spot
   * @param reserved whether the offering type is reserved
   * @param cost the cost
   * @param hours the hours
   * @param numberOfInstances the number of instances
   */
  public OfferingUtilizationReport(Providers provider, boolean spot, boolean reserved,
      BillingCost cost, BigDecimal hours, Long numberOfInstances) {
    this.provider = provider;
    this.cost = cost;
    this.hours = hours;
    this.numberOfInstances = numberOfInstances;
    if (spot) {
      this.type = OfferingType.SPOT;
    } else if (reserved) {
      this.type = OfferingType.RESERVED;
    } else {
      this.type = OfferingType.ON_DEMAND;
    }
  }

}
