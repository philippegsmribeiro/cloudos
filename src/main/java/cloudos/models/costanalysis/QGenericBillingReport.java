package cloudos.models.costanalysis;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.EnumPath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import javax.annotation.Generated;

/**
 * QGenericBillingReport is a Querydsl query type for GenericBillingReport.
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QGenericBillingReport extends EntityPathBase<GenericBillingReport> {

  private static final long serialVersionUID = 1601024814L;

  public static final QGenericBillingReport genericBillingReport =
      new QGenericBillingReport("genericBillingReport");

  public final StringPath accountId = createString("accountId");

  public final StringPath id = createString("id");

  public final StringPath lineItemId = createString("lineItemId");

  public final StringPath location = createString("location");

  public final StringPath productCode = createString("productCode");

  public final EnumPath<cloudos.Providers> provider =
      createEnum("provider", cloudos.Providers.class);

  public final StringPath resourceId = createString("resourceId");

  public final NumberPath<java.math.BigDecimal> usageCost =
      createNumber("usageCost", java.math.BigDecimal.class);

  public final DateTimePath<java.util.Date> usageEndTime =
      createDateTime("usageEndTime", java.util.Date.class);

  public final DateTimePath<java.util.Date> usageStartTime =
      createDateTime("usageStartTime", java.util.Date.class);

  public QGenericBillingReport(String variable) {
    super(GenericBillingReport.class, forVariable(variable));
  }

  public QGenericBillingReport(Path<? extends GenericBillingReport> path) {
    super(path.getType(), path.getMetadata());
  }

  public QGenericBillingReport(PathMetadata metadata) {
    super(GenericBillingReport.class, metadata);
  }

}