package cloudos.models.costanalysis;

import cloudos.utils.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder(value = {"start", "end", "timestamp"})
public class Top10ResourcesResponse implements Serializable {

  private static final long serialVersionUID = 1L;

  @DateTimeFormat(iso = ISO.DATE)
  @JsonFormat(pattern = DateUtil.ISO_DATE, shape = Shape.STRING)
  private LocalDate start;

  @DateTimeFormat(iso = ISO.DATE)
  @JsonFormat(pattern = DateUtil.ISO_DATE, shape = Shape.STRING)
  private LocalDate end;

  @Builder.Default
  private List<ResourceCost> resources = new ArrayList<>();

}
