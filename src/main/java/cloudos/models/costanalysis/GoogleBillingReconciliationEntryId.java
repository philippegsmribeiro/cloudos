package cloudos.models.costanalysis;

import java.io.Serializable;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * ID object for entity {@link GoogleBillingReconciliationEntry}.
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class GoogleBillingReconciliationEntryId implements Serializable {

  private LocalDate reportDate;

  private String measurementId;

  private String resourceId;

}
