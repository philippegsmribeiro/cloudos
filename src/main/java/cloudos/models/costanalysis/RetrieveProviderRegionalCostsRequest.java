package cloudos.models.costanalysis;

import java.io.Serializable;
import java.time.LocalDate;
import lombok.Getter;
import lombok.Setter;

/**
 * Data transfer object for retrieve provider regional cost operation.
 */
@Getter
@Setter
public class RetrieveProviderRegionalCostsRequest implements Serializable {

  private static final long serialVersionUID = 1L;

  private LocalDate startDate;

  private LocalDate endDate;

}
