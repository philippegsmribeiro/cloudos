package cloudos.models.costanalysis;

import cloudos.Providers;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Represents a provider regional cost.
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class ProviderRegionalCost implements Serializable {

  private static final long serialVersionUID = 1L;

  private Providers provider;

  private List<LocationCost> locationCosts = new ArrayList<>();

}
