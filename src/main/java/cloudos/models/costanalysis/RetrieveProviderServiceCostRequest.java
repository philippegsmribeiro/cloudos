package cloudos.models.costanalysis;

import cloudos.Providers;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Date;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Builder
public class RetrieveProviderServiceCostRequest implements Serializable {

  private static final long serialVersionUID = 1L;

  @JsonProperty
  private Providers provider;

  @JsonProperty
  private String service;

  @NotNull
  private Date startDate;

  @NotNull
  private Date endDate;

}
