package cloudos.models.costanalysis;

import cloudos.billings.GoogleBillingReport;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"billingReports", "usageReport"})
@EqualsAndHashCode(exclude = {"billingReports", "usageReport"})
@Document(collection = "cloudos_google_billing_reconciliation")
public class GoogleBillingReconciliationEntry implements Serializable {

  @Id
  private GoogleBillingReconciliationEntryId id;

  @JsonProperty
  private LocalDate reportDate;

  @JsonProperty
  private String accountId;

  @JsonProperty
  private Date startTime;

  @JsonProperty
  private Date endTime;

  @JsonProperty
  private String measurementId;

  @JsonProperty
  private String resourceId;

  @JsonProperty
  private BigDecimal costAmount;

  @JsonProperty
  private String costCurrency;

  @JsonProperty
  private String location;

  @JsonProperty
  private Long projectNumber;

  @DBRef
  private Set<GoogleBillingReport> billingReports;

  @DBRef
  private GoogleUsageReportEntry usageReport;

  @JsonProperty
  private LocalDateTime updatedTimeStamp;

}
