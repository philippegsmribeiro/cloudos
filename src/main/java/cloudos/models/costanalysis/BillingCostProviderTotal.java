package cloudos.models.costanalysis;

import cloudos.Providers;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * TO object to Billing Cost.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BillingCostProviderTotal implements Serializable {

  private static final long serialVersionUID = 1L;
  private Providers provider;
  private Double cost;
}
