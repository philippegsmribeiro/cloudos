package cloudos.models.costanalysis;

import cloudos.Providers;
import cloudos.utils.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import java.io.Serializable;
import java.time.LocalDate;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

/**
 * Identifier for {@link DailyResourceCost}.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
@EqualsAndHashCode
public class DailyResourceCostId implements Serializable {

  private static final long serialVersionUID = 1L;

  @DateTimeFormat(iso = ISO.DATE)
  @JsonFormat(shape = Shape.STRING, pattern = DateUtil.ISO_DATE)
  private LocalDate date;

  private Providers provider;

  private String resourceId;

}
