package cloudos.models.costanalysis;

import cloudos.billings.BillingsConstants;
import java.io.Serializable;
import java.math.BigDecimal;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Represents a billing cost, which have cost and discount.
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class BillingCost implements Serializable {

  private static final long serialVersionUID = 1L;

  protected BigDecimal cost = BigDecimal.ZERO;

  protected BigDecimal discount = BigDecimal.ZERO;

  protected BigDecimal totalCost = BigDecimal.ZERO;

  /**
   * Constructor that initializes all the costs. If cost is negative then updates the discount,
   * otherwise updates the cost.
   */
  public BillingCost(BigDecimal cost) {
    if (BigDecimal.ZERO.compareTo(cost) > 0) {
      this.discount = cost.abs();
    } else {
      this.cost = cost;
    }
    this.totalCost = this.cost.subtract(this.discount, BillingsConstants.MATH_CONTEXT);
  }
}
