package cloudos.models.costanalysis;

/**
 * Header mapping for Google Cloud Engine usage report CSV file.
 */
public enum GoogleUsageReportCsvHeader {

  /**
   * Report Date.
   */
  REPORT_DATE("Report Date"),
  /**
   * Measurement Id.
   */
  MEASUREMENT_ID("MeasurementId"),
  /**
   * Quantity.
   */
  QUANTITY("Quantity"),
  /**
   * Unit.
   */
  UNIT("Unit"),
  /**
   * Resource URI.
   */
  RESOURCE_URI("Resource URI"),
  /**
   * Resource Id.
   */
  RESOURCE_ID("ResourceId"),
  /**
   * Location.
   */
  LOCATION("Location");

  private final String header;

  GoogleUsageReportCsvHeader(String header) {
    this.header = header;
  }

  public String getHeader() {
    return header;
  }

}
