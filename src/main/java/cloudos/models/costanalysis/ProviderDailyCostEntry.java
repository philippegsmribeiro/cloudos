package cloudos.models.costanalysis;

import cloudos.Providers;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Getter
@Setter
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@CompoundIndex(unique = true, name = "providerDailyCostEntry_provider_date",
    def = "{'provider': 1, 'date': -1}", dropDups = true)
@Document(collection = "cloudos_provider_daily_cost")
public class ProviderDailyCostEntry extends BillingCost implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  private String id;

  private Providers provider;

  @JsonFormat(pattern = "yyyy-MM-dd")
  @DateTimeFormat(iso = ISO.DATE)
  private LocalDate date;

  @DateTimeFormat(iso = ISO.DATE_TIME)
  private Date created;

  /**
   * Constructor that initializes all fields.
   */
  public ProviderDailyCostEntry(String id, Providers provider, LocalDate date, BigDecimal cost,
      Date created) {
    super(cost);
    this.id = id;
    this.provider = provider;
    this.date = date;
    this.created = created;
  }

}
