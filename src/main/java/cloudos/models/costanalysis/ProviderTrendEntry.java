package cloudos.models.costanalysis;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

/**
 * Represents a provider trend entry.
 */
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "cloudos_provider_trend_entry")
public class ProviderTrendEntry implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  private String id;

  /**
   * The date which this entry refers to.
   */
  @DateTimeFormat(iso = ISO.DATE)
  private LocalDate date;

  /**
   * Provider cost entries.
   */
  private List<ProviderTrendEntryCost> costs;

  /**
   * The date when this entry was updated.
   */
  @DateTimeFormat(iso = ISO.DATE_TIME)
  private Date updated;

}
