package cloudos.models.costanalysis;

import cloudos.Providers;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * TO object to carry billing account and cost data.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BillingAccountCost implements Serializable {

  private String account;

  private Providers provider;

  private Double cost;

}
