package cloudos.models.costanalysis;

import java.math.BigDecimal;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class OfferingCostByMonths {
  private List<OfferingReport> cost30DaysReport;
  private BigDecimal total30DaysCost;

  private List<OfferingReport> cost60DaysReport;
  private BigDecimal total60DaysCost;

  private List<OfferingReport> costMonthToDateReport;
  private BigDecimal totalMonthToDate;
}
