package cloudos.models.costanalysis;

import cloudos.Providers;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Represents the provider trend cost.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class ProviderTrendEntryCost implements Serializable {

  private static final long serialVersionUID = 1L;

  private Providers provider;

  private BillingCost previousMonth;

  private BillingCost monthToDate;

}
