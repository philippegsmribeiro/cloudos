package cloudos.models.costanalysis;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;


/**
 * Data transfer object for {@link cloudos.models.Instance} queries.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InstanceTypeCost implements Serializable {

  @Id
  private String type;

  private BigDecimal cost = BigDecimal.ZERO;

}
