package cloudos.models.costanalysis;

import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = "updated")
@Document(collection = "cloudos_daily_resource_cost")
public class DailyResourceCost implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  private DailyResourceCostId id;

  private BillingCost cost;

  @CreatedDate
  private Date updated;

}
