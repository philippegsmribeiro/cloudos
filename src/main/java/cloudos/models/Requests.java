package cloudos.models;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import lombok.extern.log4j.Log4j2;

import org.apache.http.entity.ContentType;

/** Created by philipperibeiro on 2/18/17. */
@Log4j2
public class Requests {

  /** Constructor. */
  public Requests() {
    // add object serialization
    Unirest.setObjectMapper(
        new ObjectMapper() {
          private Gson gson = new GsonBuilder().disableHtmlEscaping().create();

          @Override
          public <T> T readValue(String value, Class<T> valueType) {
            return gson.fromJson(value, valueType);
          }

          @Override
          public String writeValue(Object value) {
            return gson.toJson(value);
          }
        });
  }

  public HttpResponse<JsonNode> get(String url) throws UnirestException {
    HttpResponse<JsonNode> response = Unirest.get(url).asJson();
    return response;
  }

  public HttpResponse<JsonNode> get(String url, Map<String, String> headers)
      throws UnirestException {
    HttpResponse<JsonNode> response = Unirest.get(url).headers(headers).asJson();
    return response;
  }

  public HttpResponse<JsonNode> get(Map<String, Object> parameters, String url)
      throws UnirestException {
    HttpResponse<JsonNode> response =
        Unirest.get(url)
            .header("accept", String.valueOf(ContentType.APPLICATION_JSON))
            .header("Content-Type", String.valueOf(ContentType.APPLICATION_JSON))
            .queryString(parameters)
            .asJson();
    return response;
  }

  public HttpResponse<JsonNode> get(
      String url, Map<String, String> headers, Map<String, Object> query) throws UnirestException {
    HttpResponse<JsonNode> response = Unirest.get(url).headers(headers).queryString(query).asJson();
    return response;
  }

  public HttpResponse<JsonNode> get(
      String url, Map<String, String> headers, String username, String password)
      throws UnirestException {
    HttpResponse<JsonNode> response =
        Unirest.get(url).headers(headers).basicAuth(username, password).asJson();
    return response;
  }

  public HttpResponse<JsonNode> post(String url, Map<String, Object> params)
      throws UnirestException {
    HttpResponse<JsonNode> jsonResponse =
        Unirest.post(url)
            .header("accept", String.valueOf(ContentType.APPLICATION_JSON))
            .fields(params)
            .asJson();

    return jsonResponse;
  }

  public HttpResponse<JsonNode> post(String url, String json) throws UnirestException {
    HttpResponse<JsonNode> jsonResponse =
        Unirest.post(url)
            .header("accept", String.valueOf(ContentType.APPLICATION_JSON))
            .header("Content-Type", String.valueOf(ContentType.APPLICATION_JSON))
            .body(json)
            .asJson();

    return jsonResponse;
  }

  public HttpResponse<JsonNode> post(
      String url, Map<String, Object> query, Map<String, Object> params) throws UnirestException {
    HttpResponse<JsonNode> jsonResponse =
        Unirest.post(url)
            .header("accept", String.valueOf(ContentType.APPLICATION_JSON))
            .queryString(query)
            .fields(params)
            .asJson();

    return jsonResponse;
  }

  public HttpResponse<JsonNode> upload(String url, String filepath) throws UnirestException {
    HttpResponse<JsonNode> jsonResponse =
        Unirest.post(url)
            .header("accept", String.valueOf(ContentType.APPLICATION_JSON))
            .field("file", new File(filepath))
            .asJson();

    return jsonResponse;
  }

  public HttpResponse<JsonNode> upload(String url, String filepath, Map<String, Object> params)
      throws UnirestException {
    HttpResponse<JsonNode> jsonResponse =
        Unirest.post(url)
            .header("accept", String.valueOf(ContentType.APPLICATION_JSON))
            .fields(params)
            .field("file", new File(filepath))
            .asJson();

    return jsonResponse;
  }

  @Override
  public void finalize() {
    try {
      Unirest.shutdown();
    } catch (IOException e) {
      log.error(e.getMessage());
    }
  }
}
