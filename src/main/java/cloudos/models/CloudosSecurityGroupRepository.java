package cloudos.models;

import cloudos.Providers;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

/** Created by gleimar on 20/05/2017. */
public interface CloudosSecurityGroupRepository
    extends MongoRepository<CloudosSecurityGroup, String> {

  List<CloudosSecurityGroup> findByProvider(Providers providers);

  List<CloudosSecurityGroup> findByNameAndProvider(String name, Providers provider);

  List<CloudosSecurityGroup> findByNameAndProviderAndRegion(String name, Providers provider,
      String region);
}
