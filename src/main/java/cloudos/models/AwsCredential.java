package cloudos.models;

import cloudos.Providers;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by philipperibeiro on 12/18/16.
 * Store the AWS credentials in Mongo.
 */
@Data
public class AwsCredential extends CloudosCredential {

    private String accessKeyId;

    @JsonIgnore
    private String secretAccessKey;

    @JsonIgnore
    public String getSecretAccessKey() {
        return this.secretAccessKey;
    }

    @JsonProperty
    public void setSecretAccessKey(String secretAccessKey) {
        this.secretAccessKey = secretAccessKey;
    }

    /**
     * Default constructor.
     */
    public AwsCredential() {
        this.setProvider(Providers.AMAZON_AWS);
    }

    /**
     * Create a new AwsCredential object, which will store the AWS credentials for the API.
     *
     * @param accessKeyId     the access key id for the AWS provider
     * @param secretAccessKey the secret access key for the AWS provider
     */
    public AwsCredential(String accessKeyId, String secretAccessKey) {
        this();
        this.accessKeyId = accessKeyId;
        this.secretAccessKey = secretAccessKey;
        setDecrypted(true);
    }

}
