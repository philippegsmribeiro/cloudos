package cloudos.models;

import cloudos.Providers;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CloudCreateKeyRequest {

  private Providers provider;
  private String region;
  private String keyName;

}
