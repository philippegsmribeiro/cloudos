package cloudos.models;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;

/** Created by philipperibeiro on 4/7/17. */
//@RepositoryRestResource(
//    collectionResourceRel = "healthchecker_memory",
//    path = "/healthchecker/{instance}/memory"
//)
public interface MemoryRepository extends MongoRepository<Memory, String> {

  List<Memory> findByInstance(@Param("instance") String instance);
}
