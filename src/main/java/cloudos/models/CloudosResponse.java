package cloudos.models;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by philipperibeiro on 6/23/17.
 *
 * <p>Define the template for the Response pattern for CloudOS
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CloudosResponse {

  protected HttpStatus status;

  protected String message;

}
