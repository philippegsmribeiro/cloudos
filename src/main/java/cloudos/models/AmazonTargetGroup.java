package cloudos.models;

import cloudos.Providers;
import lombok.Builder;
import lombok.Data;

@Data
public class AmazonTargetGroup extends CloudosTargetGroup {

  /**
   * <p>
   * The Amazon Resource Name (ARN) of the target group.
   * </p>
   */
  private String targetGroupArn;

  /**
   * <p>
   * The protocol to use for routing traffic to the targets. For Application Load Balancers, the
   * supported protocols are HTTP and HTTPS. For Network Load Balancers, the supported protocol is
   * TCP.
   * </p>
   */
  private String protocol;
  /**
   * <p>
   * The port on which the targets receive traffic. This port is used unless you specify a port
   * override when registering the target.
   * </p>
   */
  private Integer port;
  /**
   * <p>
   * The identifier of the virtual private cloud (VPC).
   * </p>
   */
  private String vpcId;
  /**
   * <p>
   * The protocol the load balancer uses when performing health checks on targets. The TCP protocol
   * is supported only if the protocol of the target group is TCP. For Application Load Balancers,
   * the default is HTTP. For Network Load Balancers, the default is TCP.
   * </p>
   */
  private String healthCheckProtocol;
  /**
   * <p>
   * The port the load balancer uses when performing health checks on targets. The default is
   * <code>traffic-port</code>, which is the port on which each target receives traffic from the
   * load balancer.
   * </p>
   */
  private String healthCheckPort;
  /**
   * <p>
   * [HTTP/HTTPS health checks] The ping path that is the destination on the targets for health
   * checks. The default is /.
   * </p>
   */
  private String healthCheckPath;
  /**
   * <p>
   * The approximate amount of time, in seconds, between health checks of an individual target. For
   * Application Load Balancers, the range is 5 to 300 seconds. For Network Load Balancers, the
   * supported values are 10 or 30 seconds. The default is 30 seconds.
   * </p>
   */
  private Integer healthCheckIntervalSeconds;
  /**
   * <p>
   * The amount of time, in seconds, during which no response from a target means a failed health
   * check. For Application Load Balancers, the range is 2 to 60 seconds and the default is 5
   * seconds. For Network Load Balancers, this is 10 seconds for TCP and HTTPS health checks and 6
   * seconds for HTTP health checks.
   * </p>
   */
  private Integer healthCheckTimeoutSeconds;
  /**
   * <p>
   * The number of consecutive health checks successes required before considering an unhealthy
   * target healthy. For Application Load Balancers, the default is 5. For Network Load Balancers,
   * the default is 3.
   * </p>
   */
  private Integer healthyThresholdCount;
  /**
   * <p>
   * The number of consecutive health check failures required before considering a target unhealthy.
   * For Application Load Balancers, the default is 2. For Network Load Balancers, this value must
   * be the same as the healthy threshold count.
   * </p>
   */
  private Integer unhealthyThresholdCount;

  /**
   * <p>
   * The type of target that you must specify when registering targets with this target group. The
   * possible values are <code>instance</code> (targets are specified by instance ID) or
   * <code>ip</code> (targets are specified by IP address). The default is <code>instance</code>.
   * Note that you can't specify targets for a target group using both instance IDs and IP
   * addresses.
   * </p>
   * <p>
   * If the target type is <code>ip</code>, specify IP addresses from the subnets of the virtual
   * private cloud (VPC) for the target group, the RFC 1918 range (10.0.0.0/8, 172.16.0.0/12, and
   * 192.168.0.0/16), and the RFC 6598 range (100.64.0.0/10). You can't specify publicly routable IP
   * addresses.
   * </p>
   */
  private String targetType;

  public AmazonTargetGroup() {
    super();
    this.setProvider(Providers.AMAZON_AWS);
  }

  @Builder
  public AmazonTargetGroup(String id, Providers provider, String region, String name,
      String targetGroupArn, String protocol, Integer port, String vpcId,
      String healthCheckProtocol, String healthCheckPort, String healthCheckPath,
      Integer healthCheckIntervalSeconds, Integer healthCheckTimeoutSeconds,
      Integer healthyThresholdCount, Integer unhealthyThresholdCount, String targetType) {
    super(id, provider, region, name);
    this.targetGroupArn = targetGroupArn;
    this.protocol = protocol;
    this.port = port;
    this.vpcId = vpcId;
    this.healthCheckProtocol = healthCheckProtocol;
    this.healthCheckPort = healthCheckPort;
    this.healthCheckPath = healthCheckPath;
    this.healthCheckIntervalSeconds = healthCheckIntervalSeconds;
    this.healthCheckTimeoutSeconds = healthCheckTimeoutSeconds;
    this.healthyThresholdCount = healthyThresholdCount;
    this.unhealthyThresholdCount = unhealthyThresholdCount;
    this.targetType = targetType;
  }

}
