package cloudos.models;

import cloudos.Providers;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class GoogleTargetGroup extends CloudosTargetGroup {

  public GoogleTargetGroup() {
    this.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
  }
}
