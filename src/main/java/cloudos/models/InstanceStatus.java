package cloudos.models;

public enum InstanceStatus {
  RUNNING,
  PROVISIONING,
  STOPPING,
  STOPPED,
  TERMINATED
}
