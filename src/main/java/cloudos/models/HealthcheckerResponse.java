package cloudos.models;

import cloudos.healthchecker.ResourceChart;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Created by philipperibeiro on 7/3/17. Response object for the Healthchecker controller. */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HealthcheckerResponse extends CloudosResponse {

  private ResourceChart resourceChart;

}
