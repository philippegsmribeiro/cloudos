package cloudos.models;

import java.util.List;

import cloudos.Providers;
import cloudos.notifications.NotificationType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Created by philipperibeiro on 12/22/16. */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CloudActionRequest implements Cloneable {

  /* Provider: AWS, GCLOUD, etc. */
  private Providers provider;
  /* Instance names/id */
  private List<String> names;
  /* Region/zone */
  private String region;
  /* Region/zone */
  private String zone;
  /* Security Group */
  private String securityGroup;
  /* Synchronous request - wait until complete */
  private boolean synchronous = true;
  /* Fire notification */
  private boolean fireNotification = true;
  /* Fire notification type */
  private NotificationType notificationType = NotificationType.HEALTHCHECKER;

  /* Type of the request */
  private ActionType type;

  /**
   * Create a new CloudActionRequest with the params of the object.
   *
   * @param provider the name of the provider
   * @param names a list of the names of the instances to be created
   * @param region the regions where the instances will be created
   * @param zone the zone of the region
   * @param securityGroup the name of the security group
   * @param synchronous whether the request is async or sync
   * @param type the type of the action to be performed
   */
  public CloudActionRequest(Providers provider, List<String> names, String region, String zone,
      String securityGroup, boolean synchronous, ActionType type) {
    super();
    this.provider = provider;
    this.names = names;
    this.region = region;
    this.zone = zone;
    this.securityGroup = securityGroup;
    this.synchronous = synchronous;
    this.type = type;
  }

  /**
   * Create the request for AWS.
   *
   * @param names a list of instance names
   * @param zone the availability zone of the instance
   * @param securityGroup the security group the instance belongs to
   * @return a new CloudActionRequest
   */
  public static CloudActionRequest createAwsRequest(List<String> names, String region, String zone,
      String securityGroup, ActionType type) {
    return new CloudActionRequest(Providers.AMAZON_AWS, names, region, zone, securityGroup, true,
        type);
  }

  /**
   * Create the request for GCloud.
   *
   * @param names a list of instance names
   * @param zone the availability zone of the instance
   * @return a new CloudActionRequest
   */
  public static CloudActionRequest createGCloudRequest(List<String> names, String region,
      String zone, ActionType type) {
    return new CloudActionRequest(Providers.GOOGLE_COMPUTE_ENGINE, names, region, zone, null, true,
        type);
  }

  @Override
  public CloudActionRequest clone() {
    try {
      return (CloudActionRequest) super.clone();
    } catch (CloneNotSupportedException e) {
      e.printStackTrace();
    }
    return null;
  }

}
