package cloudos.models;

public enum ActionType {
  STOP,
  START,
  RESTART,
  TERMINATE,
  FETCH
}
