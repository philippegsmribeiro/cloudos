package cloudos.google;

import cloudos.models.Instance;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface GoogleInstanceRepository extends MongoRepository<GoogleInstance, String> {

  GoogleInstance findByInstance(Instance instance);
  // GoogleInstance findByName(String name);
  // List<GoogleInstance> findByMemoryInMb(Integer memoryInMb);
  // List<GoogleInstance> findByCores(Integer cores);

}
