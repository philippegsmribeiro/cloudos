package cloudos.google;

import cloudos.Providers;
import cloudos.keys.cloudoskey.CloudosKey;
import cloudos.keys.cloudoskey.CloudosKeyService;
import cloudos.keys.cloudoskey.GoogleKey;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.annotation.PostConstruct;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class GoogleKeyGenerator {

  public static final String keyName = "gcloudsshkey";
  private static final String sshKeyName = "gcloudsshkey";

  @Value("${google.sshDir}")
  private String dir;

  @Value("${google.sshkey}")
  private String key;

  @Value("${google.sshPubKey}")
  private String pubkey;

  private Path sshKey;
  private Path sshPubKey;

  @Autowired
  CloudosKeyService cloudosKeyService;

  public GoogleKeyGenerator() {}

  /**
   * Create default keys.
   */
  @PostConstruct
  public void deleteAndCreateNewKey() {
    try {
      log.info("generation gcloud keys");
      // .ssh dir
      Path dirPath = Paths.get(dir);
      Files.createDirectories(dirPath);
      // delete the keys if they exists
      sshPubKey = Paths.get(dir + File.separator + sshKeyName + ".pub");
      Files.deleteIfExists(sshPubKey);
      sshKey = Paths.get(dir + File.separator + sshKeyName);
      Files.deleteIfExists(sshKey);
      // recreate
      File sshFile = sshPubKey.toFile();
      File sshFile2 = sshKey.toFile();
      if (!sshFile.exists()) {
        Files.write(sshFile.toPath(), pubkey.getBytes());
        Files.write(sshFile2.toPath(), key.getBytes());
      }

      // create the default key
      CloudosKey googleKey = cloudosKeyService.findActiveKeysByKeyNameAndProvider(keyName,
          Providers.GOOGLE_COMPUTE_ENGINE);
      if (googleKey == null) {
        googleKey = new GoogleKey();
        googleKey.setKeyName(keyName);
        googleKey.setRegion("");
        googleKey.setPrivateKeyContent(key);
        googleKey.setPublicKeyContent(pubkey);
        cloudosKeyService.save(googleKey);
      }

    } catch (Exception e) {
      log.error("Error generating keys!", e);
    }
  }

  /**
   * Return the ssh key path.
   * 
   * @return filesystem path
   */
  public Path getSshKey() {
    return sshKey;
  }

  /**
   * Return the ssh pub key path.
   * 
   * @return filesystem path
   */
  public Path getSshPubKey() {
    return sshPubKey;
  }
}
