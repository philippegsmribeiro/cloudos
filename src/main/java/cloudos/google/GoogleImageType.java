package cloudos.google;

import cloudos.Providers;
import cloudos.provider.ProviderImageType;
import java.util.Date;

public class GoogleImageType extends ProviderImageType {

  public GoogleImageType() {
    setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
  }
  
  public GoogleImageType(String id, Providers provider, String project, String key,
      String description, String group, String region, Object object, Date includedDate,
      Boolean enabled) {
    super(id, provider, project, key, description, group, region, object, includedDate, enabled);
    setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
  }

}
