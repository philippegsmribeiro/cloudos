package cloudos.google;

import cloudos.Providers;
import cloudos.provider.ProviderRegion;
import java.util.Date;

public class GoogleRegion extends ProviderRegion {
  
  public GoogleRegion() {
    setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
  }

  public GoogleRegion(String id, Providers provider, String project, String key, String description,
      String group, String region, Object object, Date includedDate, Boolean enabled) {
    super(id, provider, project, key, description, group, region, object, includedDate, enabled);
    setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
  }

}
