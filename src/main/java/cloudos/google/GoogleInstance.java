package cloudos.google;

import cloudos.config.CloudOSJson;
import cloudos.models.AbstractInstance;
import cloudos.models.Instance;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "google_instance")
@CloudOSJson
@Getter
@Setter
public class GoogleInstance extends AbstractInstance {

  private String project;
  private String description;
  private String machineType;
  private String machineTypeDescription;
  private String key;
  private Object object;

  /** Default constructor. */
  public GoogleInstance() {
    super();
  }
  
  /**
   * Constructor for use @Builder and inheritance.
   */
  @Builder
  public GoogleInstance(String project, String description, String machineType,
      String machineTypeDescription, String key, Object object, String id, Instance instance) {
    super(id, instance);
    this.project = project;
    this.description = description;
    this.machineType = machineType;
    this.machineTypeDescription = machineTypeDescription;
    this.key = key;
    this.object = object;
  }
}
