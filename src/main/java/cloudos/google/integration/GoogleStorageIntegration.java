package cloudos.google.integration;

import cloudos.google.GoogleCredential;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.BucketInfo;
import com.google.cloud.storage.BucketInfo.Builder;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.Storage.BlobTargetOption;
import com.google.cloud.storage.StorageClass;
import com.google.cloud.storage.StorageException;
import com.google.cloud.storage.StorageOptions;

import java.util.Arrays;

import lombok.extern.log4j.Log4j2;

import org.springframework.stereotype.Service;

@Service
@Log4j2
public class GoogleStorageIntegration extends AbstractGoogleIntegration {

  public static final String user = "cloudos";

  private Storage storageService;

  @Override
  protected void resetCredential(GoogleCredential cloudosCredential) {
    super.resetCredential(cloudosCredential);
    storageService = null;
  }

  @Override
  protected synchronized void setUp() {
    try {
      if (storageService == null) {
        // storage
        storageService = createStorage(getGoogleCredential());
      }
    } catch (Exception e) {
      // TODO: handle exception
      throw new RuntimeException("Error setting up the service", e);
    }
  }

  /**
   * Create the storage service.
   *
   * @param googleCredential credential
   * @return Storage service
   * @throws Exception if something goes wrong
   */
  private Storage createStorage(GoogleCredential googleCredential) throws Exception {
    GoogleCredentials credential =
        GoogleCredentials.fromStream(getJsonInputStream(googleCredential));
    if (credential.createScopedRequired()) {
      credential =
          credential.createScoped(Arrays.asList(HTTPS_WWW_GOOGLEAPIS_COM_AUTH_CLOUD_PLATFORM));
    }
    // storage
    return StorageOptions.newBuilder()
        .setCredentials(credential)
        .setProjectId(googleCredential.getProject())
        .build()
        .getService();
  }

  /**
   * Return the service.
   *
   * @return Storage service
   */
  private Storage getStorageService() {
    if (storageService == null) {
      setUp();
    }
    return storageService;
  }

  /**
   * List all buckets.
   *
   * @return Iterable of Bucket
   */
  public Iterable<Bucket> listBuckets() {
    return getStorageService().list().getValues();
  }

  /**
   * Create a new bucket.
   *
   * @param bucketName name
   * @param location location
   * @param storageClass class
   * @return Bucket
   */
  public Bucket createBucket(
      String bucketName, GoogleStorageLocation location, StorageClass storageClass)
      throws StorageException {
    try {
      Builder newBuilder = BucketInfo.newBuilder(bucketName);
      newBuilder.setStorageClass(storageClass);
      newBuilder.setLocation(location.getValue());
      return getStorageService().create(newBuilder.build());
    } catch (StorageException e) {
      log.error("Error", e);
      throw e;
    }
  }

  /**
   * Retrieve bucket.
   *
   * @param bucketName name
   * @return Bucket
   */
  public Bucket retrieveBucket(String bucketName) {
    return getStorageService().get(bucketName);
  }

  /**
   * Update bucket.
   *
   * @param bucketInfo info
   * @return Bucket
   */
  public Bucket updateBucket(BucketInfo bucketInfo) {
    try {
      return getStorageService().update(bucketInfo);
    } catch (StorageException e) {
      log.error("Error", e);
      throw e;
    }
  }

  /**
   * Delete a bucket.
   *
   * @param bucketName name
   * @return true if it was deleted
   */
  public boolean deleteBucket(String bucketName) {
    return getStorageService().delete(bucketName);
  }

  /**
   * List all blobs.
   *
   * @return Iterable of Blob
   */
  public Iterable<Blob> listBlobs(String bucketName) {
    return getStorageService().list(bucketName).getValues();
  }

  /**
   * Create a blob on the bucket.
   *
   * @param bucket name
   * @param name of blob
   * @param content of blob
   * @return Blob
   */
  public Blob createBlob(String bucket, String name, byte[] content) {
    return getStorageService()
        .create(
            BlobInfo.newBuilder(BlobId.of(bucket, name)).build(),
            content,
            BlobTargetOption.doesNotExist());
  }

  /**
   * Retrieve a blob.
   *
   * @param bucket name
   * @param name of blob
   * @return Blob
   */
  public Blob retrieveBlob(String bucket, String name) {
    return getStorageService().get(BlobId.of(bucket, name));
  }

  /**
   * Update a blob.
   *
   * @param blobInfo info
   * @return Blob
   */
  public Blob updateBlob(BlobInfo blobInfo) {
    return getStorageService().update(blobInfo);
  }

  /**
   * Delete a blob.
   *
   * @param bucket name
   * @param name of blob
   * @return true if deleted
   */
  public boolean deleteBlob(String bucket, String name) {
    return getStorageService().delete(BlobId.of(bucket, name));
  }

  /**
   * Read a blob content.
   *
   * @param blobId id
   * @return byte[]
   */
  public byte[] readBlob(BlobId blobId) {
    return getStorageService().readAllBytes(blobId);
  }

  @Override
  public boolean validateCredential(GoogleCredential cloudosCredential) {
    try {
      createStorage(cloudosCredential).list().getValues();
      return true;
    } catch (Exception e) {
      log.error("Validating credential: {}", cloudosCredential.toString(), e);
      return false;
    }
  }
}
