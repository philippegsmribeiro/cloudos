package cloudos.google.integration;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.googleapis.batch.BatchRequest;
import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.ParentReference;
import com.google.api.services.drive.model.Permission;
import com.google.api.services.drive.model.PermissionList;
import com.google.api.services.oauth2.Oauth2;
import com.google.api.services.oauth2.model.Userinfoplus;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class GoogleDrive {

  /* Application name */
  private static final String APPLICATION_NAME = "cloudos";

  /* Directory to store user credentials for this application */
  private static final java.io.File DATA_STORE_DIR =
      new java.io.File(System.getProperty("user.home"), ".credentials/drive-java-quickstart");

  /* redirect URI */
  private static final String REDIRECT_URI = "urn:ietf:wg:oauth:2.0:oob";

  /* name of the client secret file */
  private static final String CLIENTSECRET_LOCATION = "/client_secret.json";

  private static GoogleAuthorizationCodeFlow flow = null;

  /* Global instance of the {@link FileDataStoreFactory}. */
  private static FileDataStoreFactory DATA_STORE_FACTORY;

  /* Global instance of the JSON Factory. */
  private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

  /* Global instance of the HTTP transport. */
  private static HttpTransport HTTP_TRANSPORT;

  /* Define the singleton for the drive connection */
  private static Drive service;

  /* Global instances of the score required by this file. */
  private static final List<String> SCOPES =
      Arrays.asList(
          DriveScopes.DRIVE,
          DriveScopes.DRIVE_APPDATA,
          DriveScopes.DRIVE_APPS_READONLY,
          DriveScopes.DRIVE_FILE,
          DriveScopes.DRIVE_METADATA,
          DriveScopes.DRIVE_METADATA_READONLY,
          DriveScopes.DRIVE_PHOTOS_READONLY,
          DriveScopes.DRIVE_READONLY,
          DriveScopes.DRIVE_SCRIPTS);

  /** Exception thrown when an error occurred while retrieving credentials. */
  public static class GetCredentialsException extends Exception {

    private static final long serialVersionUID = 1L;
    protected String authorizationUrl;

    /**
     * Construct a GetCredentialsException.
     *
     * @param authorizationUrl The authorization URL to redirect the user to.
     */
    public GetCredentialsException(String authorizationUrl) {
      this.authorizationUrl = authorizationUrl;
    }

    /** Set the authorization URL. */
    public void setAuthorizationUrl(String authorizationUrl) {
      this.authorizationUrl = authorizationUrl;
    }

    /** @return the authorizationUrl */
    public String getAuthorizationUrl() {
      return authorizationUrl;
    }
  }

  /** Exception thrown when a code exchange has failed. */
  public static class CodeExchangeException extends GetCredentialsException {
    private static final long serialVersionUID = 1L;

    /**
     * Construct a CodeExchangeException.
     *
     * @param authorizationUrl The authorization URL to redirect the user to.
     */
    public CodeExchangeException(String authorizationUrl) {
      super(authorizationUrl);
    }
  }

  /** Exception thrown when no refresh token has been found. */
  public static class NoRefreshTokenException extends GetCredentialsException {
    private static final long serialVersionUID = 1L;

    /**
     * Construct a NoRefreshTokenException.
     *
     * @param authorizationUrl The authorization URL to redirect the user to.
     */
    public NoRefreshTokenException(String authorizationUrl) {
      super(authorizationUrl);
    }
  }

  /** Exception thrown when no user ID could be retrieved. */
  private static class NoUserIdException extends Exception {

    private static final long serialVersionUID = 1L;
  }

  /** Default constructor. */
  public GoogleDrive() {
    try {
      HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
      DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
      /* Just to make sure service is never null */
      service = getDriveService();
    } catch (Throwable t) {
      log.error(t.getMessage());
    }
  }

  /**
   * Creates an authorized Credential object.
   *
   * @return an authorized Credential object.
   * @throws Exception if something goes wrong
   */
  public static Credential authorize() throws Exception {
    /* load client secrets. */
    InputStream in = GoogleDrive.class.getResourceAsStream("/client_secret.json");
    GoogleClientSecrets clientSecrets =
        GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

    /* Build flow and trigger user authorization request. */
    GoogleAuthorizationCodeFlow flow =
        new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
            .setDataStoreFactory(DATA_STORE_FACTORY)
            .setAccessType("offline")
            .build();
    Credential credential =
        new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
    log.info("Credentials saved to " + DATA_STORE_DIR.getAbsolutePath());
    return credential;
  }

  /**
   * Retrieved stored credentials for the provided user ID.
   *
   * @param userId User's ID.
   * @return Stored Credential if found, {@code null} otherwise.
   */
  public static Credential getStoredCredentials(String userId) {
    /*
     * TODO: Implement this method to work with your database. Instantiate a new Credential
     * instance with stored accessToken and refreshToken.
     */
    throw new UnsupportedOperationException();
  }

  /**
   * Store OAuth 2.0 credentials in the application's database.
   *
   * @param userId User's ID.
   * @param credentials The OAuth 2.0 credentials to store.
   */
  public static void storeCredentials(String userId, Credential credentials) {
    /*
     * TODO: Implement this method to work with your database. Store the
     * credentials.getAccessToken() and credentials.getRefreshToken() string values in your
     * database.
     */
    throw new UnsupportedOperationException();
  }

  /**
   * Build and return an authorized Drive Client Service.
   *
   * @return an authorized Drive client service
   * @throws Exception if something goes wrong
   */
  public static Drive getDriveService() throws Exception {
    if (service == null) {
      Credential credential = authorize();
      service =
          new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
              .setApplicationName(APPLICATION_NAME)
              .build();
    }
    return service;
  }

  /**
   * Build an authorization flow and store it as a static class attribute.
   *
   * @return GoogleAuthorizationCodeFlow instance.
   * @throws IOException Unable to load client_secret.json.
   */
  public static GoogleAuthorizationCodeFlow getFlow() throws IOException {

    if (flow == null) {
      InputStream in = GoogleDrive.class.getResourceAsStream(CLIENTSECRET_LOCATION);
      GoogleClientSecrets clientSecret =
          GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));
      flow =
          new GoogleAuthorizationCodeFlow.Builder(
                  HTTP_TRANSPORT, JSON_FACTORY, clientSecret, SCOPES)
              .setAccessType("offline")
              .setApprovalPrompt("force")
              .build();
    }
    return flow;
  }

  /**
   * Exchange an authorization code for OAuth 2.0 credentials.
   *
   * @param authorizationCode Authorization code to exchange for OAuth 2.0 credentials.
   * @return OAuth 2.0 credentials.
   * @throws CodeExchangeException An error occurred.
   */
  public static Credential exchangeCode(String authorizationCode) throws CodeExchangeException {

    try {
      GoogleAuthorizationCodeFlow flow = getFlow();
      GoogleTokenResponse response =
          flow.newTokenRequest(authorizationCode).setRedirectUri(REDIRECT_URI).execute();
      return flow.createAndStoreCredential(response, null);
    } catch (IOException e) {
      log.error("An error occurred: " + e);
      throw new CodeExchangeException(null);
    }
  }

  /**
   * Send a request to the UserInfo API to retrieve the user's information.
   *
   * @param credentials OAuth 2.0 credentials to authorize the request.
   * @return User's information.
   * @throws NoUserIdException An error occurred.
   */
  public static Userinfoplus getUserInfo(Credential credentials) throws NoUserIdException {

    Oauth2 userInfoService =
        new Oauth2.Builder(HTTP_TRANSPORT, JSON_FACTORY, credentials)
            .setApplicationName(APPLICATION_NAME)
            .build();
    Userinfoplus userInfo = null;
    try {
      userInfo = userInfoService.userinfo().get().execute();
    } catch (IOException e) {
      log.error("An error occurred: " + e);
    }
    if (userInfo != null && userInfo.getId() != null) {
      return userInfo;
    }
    throw new NoUserIdException();
  }

  /**
   * Retrieve the authorization URL.
   *
   * @param emailAddress User's e-mail address.
   * @param state State for the authorization URL.
   * @return Authorization URL to redirect the user to.
   * @throws IOException Unable to load client_secret.json.
   */
  public static String getAuthorizationUrl(String emailAddress, String state) throws IOException {

    GoogleAuthorizationCodeRequestUrl urlBuilder =
        getFlow().newAuthorizationUrl().setRedirectUri(REDIRECT_URI).setState(state);
    urlBuilder.set("user_id", emailAddress);
    return urlBuilder.build();
  }

  /**
   * Retrieve credentials using the provided authorization code.
   *
   * <p>This function exchanges the authorization code for an access token and queries the UserInfo
   * API to retrieve the user's e-mail address. If a refresh token has been retrieved along with an
   * access token, it is stored in the application database using the user's e-mail address as key.
   * If no refresh token has been retrieved, the function checks in the application database for one
   * and returns it if found or throws a NoRefreshTokenException with the authorization URL to
   * redirect the user to.
   *
   * @param authorizationCode Authorization code to use to retrieve an access token.
   * @param state State to set to the authorization URL in case of error.
   * @return OAuth 2.0 credentials instance containing an access and refresh token.
   * @throws NoRefreshTokenException No refresh token could be retrieved from the available sources.
   * @throws IOException Unable to load client_secret.json.
   */
  public static Credential getCredentials(String authorizationCode, String state)
      throws CodeExchangeException, NoRefreshTokenException, IOException {

    String email = "";
    try {
      Credential credentials = exchangeCode(authorizationCode);
      Userinfoplus userInfo = getUserInfo(credentials);
      String userId = userInfo.getId();
      email = userInfo.getEmail();

      if (credentials.getRefreshToken() != null) {
        storeCredentials(userId, credentials);
        return credentials;
      }
      credentials = getStoredCredentials(userId);
      if (credentials != null && credentials.getRefreshToken() != null) {
        return credentials;
      }
    } catch (CodeExchangeException e) {
      log.error(e.getMessage());
      /*
       * Drive apps should try to retrieve the user and credentials for the current session.
       * If none is available, redirect the user to the authorization URL.
       */
      e.setAuthorizationUrl(getAuthorizationUrl(email, state));
      throw e;
    } catch (NoUserIdException e) {
      log.error(e.getMessage());
    }
    /* No refresh token has been retrieved. */
    String authorizationUrl = getAuthorizationUrl(email, state);
    throw new NoRefreshTokenException(authorizationUrl);
  }

  /**
   * Download a file's content.
   *
   * @param file Drive File instance.
   * @return
   */
  public InputStream downloadFile(File file) {

    assert service != null;

    if (file.getDownloadUrl() != null && file.getDownloadUrl().length() > 0) {
      try {
        /* use alt=media query parameter to request content */
        return service.files().get(file.getId()).executeMediaAsInputStream();
      } catch (IOException e) {
        /* an error occured */
        log.error(e.getMessage());
        return null;
      }
    }
    /* File doesn't have any content stored on Drive */
    log.warn("File doesn't have any content stored on Drive");
    return null;
  }

  /**
   * Run a batch of Requests.
   *
   * @throws IOException
   */
  public static void runBatchRequest(String fileID, List<Permission> permissions)
      throws IOException {

    assert service != null;

    /* Create the callback */
    JsonBatchCallback<Permission> callback =
        new JsonBatchCallback<Permission>() {

          @Override
          public void onSuccess(Permission permission, HttpHeaders responseHeaders) {
            log.info("Success!");
          }

          @Override
          public void onFailure(GoogleJsonError e, HttpHeaders responseHeaders) {
            log.warn("Error message: " + e.getMessage());
          }
        };
    BatchRequest batch = service.batch();
    for (Permission permission : permissions) {
      service.permissions().insert(fileID, permission).queue(batch, callback);
    }

    batch.execute();
  }

  /**
   * Insert a new file.
   *
   * @param title Title of the file to insert, including the extension.
   * @param description Description of the file to insert.
   * @param parentId Optional parent folder's ID.
   * @param mimeType MIME type of the file to insert.
   * @param filename Filename of the file to insert.
   * @return Inserted file metadata if successful, {@code null} otherwise.
   */
  public File insertFile(
      String title, String description, String parentId, String mimeType, String filename) {

    assert service != null;

    /* File's metadata. */
    File body = new File();
    body.setTitle(title);
    body.setDescription(description);
    body.setMimeType(mimeType);

    /* Set the parent folder */
    if (parentId != null && parentId.length() > 0) {
      body.setParents(Arrays.asList(new ParentReference().setId(parentId)));
    }
    /* File's content. */
    java.io.File fileContent = new java.io.File(filename);
    FileContent mediaContent = new FileContent(mimeType, fileContent);
    try {
      File file = service.files().insert(body, mediaContent).execute();
      log.info("File ID: " + file.getId());
      return file;
    } catch (IOException e) {
      log.error(e.getMessage());
      return null;
    }
  }

  /**
   * Get a file from Google Drive.
   *
   * @param fileId ID of the file to print metadata for.
   */
  public File getFile(String fileId) {

    assert service != null;

    try {
      File file = service.files().get(fileId).execute();
      return file;
    } catch (IOException e) {
      log.error(e.getMessage());
      return null;
    }
  }

  /**
   * Rename a file.
   *
   * @param fileId ID of the file to rename.
   * @param newTitle New title for the file.
   * @return Updated file metadata if successful, {@code null} otherwise.
   */
  public File renameFile(String fileId, String newTitle) {

    assert service != null;

    try {
      File file = new File();
      file.setTitle(newTitle);

      /* Rename the file. */
      Files.Patch patchRequest = service.files().patch(fileId, file);
      patchRequest.setFields("title");

      File updatedFile = patchRequest.execute();
      return updatedFile;
    } catch (IOException e) {
      log.error("An error occurred: " + e);
      return null;
    }
  }

  /**
   * Update an existing file's metadata and content.
   *
   * @param fileId ID of the file to update.
   * @param newTitle New title for the file.
   * @param newDescription New description for the file.
   * @param newMimeType New MIME type for the file.
   * @param newFilename Filename of the new content to upload.
   * @param newRevision Whether or not to create a new revision for this file.
   * @return Updated file metadata if successful, {@code null} otherwise.
   */
  public File updateFile(
      String fileId,
      String newTitle,
      String newDescription,
      String newMimeType,
      String newFilename,
      boolean newRevision) {

    assert service != null;

    try {
      /* First retrieve the file from the API. */
      File file = service.files().get(fileId).execute();

      /* File's new metadata. */
      file.setTitle(newTitle);
      file.setDescription(newDescription);
      file.setMimeType(newMimeType);

      /* File's new content. */
      java.io.File fileContent = new java.io.File(newFilename);
      FileContent mediaContent = new FileContent(newMimeType, fileContent);

      /* Send the request to the API. */
      File updatedFile = service.files().update(fileId, file, mediaContent).execute();
      return updatedFile;
    } catch (IOException e) {
      log.error("An error occurred: " + e);
      return null;
    }
  }

  /**
   * Copy an existing file.
   *
   * @param originFileId ID of the origin file to copy.
   * @param copyTitle Title of the copy.
   * @return The copied file if successful, {@code null} otherwise.
   */
  public File copyFile(String originFileId, String copyTitle) {

    assert service != null;

    File copiedFile = new File();
    copiedFile.setTitle(copyTitle);
    try {
      return service.files().copy(originFileId, copiedFile).execute();
    } catch (IOException e) {
      log.error("An error occurred: " + e);
    }
    return null;
  }

  /**
   * Permanently delete a file, skipping the trash.
   *
   * @param fileId ID of the file to delete.
   */
  public boolean deleteFile(String fileId) {

    assert service != null;

    try {
      service.files().delete(fileId).execute();
      return true;
    } catch (IOException e) {
      log.error("An error occurred: " + e);
      return false;
    }
  }

  /**
   * Retrieve a list of File resources.
   *
   * @return List of File resources.
   */
  public List<File> retrieveFiles() throws IOException {

    assert service != null;

    List<File> result = new ArrayList<>();
    Files.List request = service.files().list();

    do {
      try {
        FileList files = request.execute();
        result.addAll(files.getItems());
        request.setPageToken(files.getNextPageToken());
      } catch (IOException e) {
        log.error("An error occurred: " + e);
        request.setPageToken(null);
      }
    } while (request.getPageToken() != null && request.getPageToken().length() > 0);

    return result;
  }

  /**
   * Update a file's modified date.
   *
   * @param fileId ID of the file to update the modified date for.
   * @return The updated file if successful, {@code null} otherwise.
   */
  public File updateModifiedDate(String fileId) {

    assert service != null;

    try {
      return service.files().touch(fileId).execute();
    } catch (IOException e) {
      log.error("An error occurred: " + e);
    }

    return null;
  }

  /**
   * Move a file to the trash.
   *
   * @param fileId ID of the file to trash.
   * @return The updated file if successful, {@code null} otherwise.
   */
  public File trashFile(String fileId) {

    assert service != null;

    try {
      return service.files().trash(fileId).execute();
    } catch (IOException e) {
      log.error("An error occurred: " + e);
    }
    return null;
  }

  /**
   * Restore a file from the trash.
   *
   * @param fileId ID of the file to restore.
   * @return The updated file if successful, {@code null} otherwise.
   */
  public File restoreFile(String fileId) {

    assert service != null;

    try {
      return service.files().untrash(fileId).execute();
    } catch (IOException e) {
      log.error("An error occurred: " + e);
    }
    return null;
  }

  /**
   * Remove a permission.
   *
   * @param fileId ID of the file to remove the permission for.
   * @param permissionId ID of the permission to remove.
   */
  public void removePermission(String fileId, String permissionId) {

    assert service != null;
    try {
      service.permissions().delete(fileId, permissionId).execute();
    } catch (IOException e) {
      log.error("An error occurred: " + e);
    }
  }

  /**
   * Print information about the specified permission.
   *
   * @param fileId ID of the file to print permission for.
   * @param permissionId ID of the permission to print.
   */
  public Permission getPermission(String fileId, String permissionId) {

    assert service != null;
    try {
      return service.permissions().get(fileId, permissionId).execute();
    } catch (IOException e) {
      log.error("An error occurred: " + e);
      return null;
    }
  }

  /**
   * Insert a new permission.
   *
   * @param fileId ID of the file to insert permission for.
   * @param value User or group e-mail address, domain name or {@code null} "default" type.
   * @param type The value "user", "group", "domain" or "default".
   * @param role The value "owner", "writer" or "reader".
   * @return The inserted permission if successful, {@code null} otherwise.
   */
  public Permission insertPermission(String fileId, String value, String type, String role) {
    assert service != null;
    Permission newPermission = new Permission();
    newPermission.setValue(value);
    newPermission.setType(type);
    newPermission.setRole(role);

    try {
      return service.permissions().insert(fileId, newPermission).execute();
    } catch (IOException e) {
      log.error("An error occurred: " + e);
    }
    return null;
  }

  /**
   * Retrieve a list of permissions.
   *
   * @param fileId ID of the file to retrieve permissions for.
   * @return List of permissions.
   */
  public List<Permission> retrievePermissions(String fileId) {
    assert service != null;
    try {
      PermissionList permissions = service.permissions().list(fileId).execute();
      return permissions.getItems();
    } catch (IOException e) {
      log.error("An error occurred: " + e);
    }
    return null;
  }

  /**
   * Patch a permission's role.
   *
   * @param fileId ID of the file to update permission for.
   * @param permissionId ID of the permission to patch.
   * @param newRole The value "owner", "writer" or "reader".
   * @return The patched permission if successful, {@code null} otherwise.
   */
  public Permission patchPermission(String fileId, String permissionId, String newRole) {
    assert service != null;
    Permission patchedPermission = new Permission();
    patchedPermission.setRole(newRole);
    try {
      return service.permissions().patch(fileId, permissionId, patchedPermission).execute();
    } catch (IOException e) {
      log.error("An error occurred: " + e);
    }
    return null;
  }

  /**
   * Update a permission's role.
   *
   * @param fileId ID of the file to update permission for.
   * @param permissionId ID of the permission to update.
   * @param newRole The value "owner", "writer" or "reader".
   * @return The updated permission if successful, {@code null} otherwise.
   */
  public Permission updatePermission(String fileId, String permissionId, String newRole) {

    assert service != null;
    try {
      /* First retrieve the permission from the API. */
      Permission permission = service.permissions().get(fileId, permissionId).execute();
      permission.setRole(newRole);
      return service.permissions().update(fileId, permissionId, permission).execute();
    } catch (IOException e) {
      log.error("An error occurred: " + e);
    }
    return null;
  }

  /**
   * Insert a file into a folder.
   *
   * @param folderId ID of the folder to insert the file into
   * @param fileId ID of the file to insert.
   * @return The inserted parent if successful, {@code null} otherwise.
   */
  public ParentReference insertFileIntoFolder(String folderId, String fileId) {

    assert service != null;
    ParentReference parent = new ParentReference();
    parent.setId(folderId);
    try {
      return service.parents().insert(fileId, parent).execute();
    } catch (IOException e) {
      log.error("An error occurred: " + e);
    }
    return null;
  }

  /**
   * Check if a file is in a specific folder
   *
   * @param folderId ID of the folder.
   * @param fileId ID of the file.
   * @return Whether or not the file is in the folder.
   */
  public boolean isFileInFolder(String folderId, String fileId) throws IOException {

    assert service != null;
    try {
      service.parents().get(fileId, folderId).execute();
    } catch (HttpResponseException e) {
      if (e.getStatusCode() == 404) {
        return false;
      }
      log.error("An error occurred: " + e);
      throw e;
    } catch (IOException e) {
      log.error("An error occurred: " + e);
      throw e;
    }
    return true;
  }

  /**
   * Remove a file from a folder.
   *
   * @param folderId ID of the folder to remove the file from.
   * @param fileId ID of the file to remove from the folder.
   */
  public boolean remoteFileFromFolder(String folderId, String fileId) {

    assert service != null;
    boolean status = false;
    try {
      service.parents().delete(fileId, folderId).execute();
      status = true;
    } catch (IOException e) {
      log.error("An error occurred: " + e);
    }

    return status;
  }
}
