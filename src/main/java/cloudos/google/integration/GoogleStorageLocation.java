package cloudos.google.integration;

/** Represents a Storage Location. */
public interface GoogleStorageLocation {

  /**
   * Returns the value to use on the integration.
   *
   * @return location value
   */
  String getValue();

  /**
   * Multi-regional locations https://cloud.google.com/storage/docs/storage-classes#multi-regional.
   */
  enum MULTI_REGIONAL implements GoogleStorageLocation {
    // us — United States
    US,
    // eu — European Union
    EU,
    // asia — Asia Pacific
    ASIA;

    @Override
    public String getValue() {
      return this.name().toLowerCase();
    }
  }

  /** Regional locations https://cloud.google.com/storage/docs/storage-classes#regional. */
  enum REGIONAL implements GoogleStorageLocation {
    // asia-east1 — Taiwan
    ASIA_EAST1,
    // asia-northeast1 — Tokyo
    ASIA_NORTHEAST1,
    // asia-southeast1 — Singapore
    ASIA_SOUTHEAST1,
    // europe-west1 — Belgium
    ASIA_WEST1,
    // us-central1 — Iowa
    US_CENTRAL1,
    // us-east1 — South Carolina
    US_EAST1,
    // us-west1 — Oregon
    US_WEST1;

    @Override
    public String getValue() {
      return this.name().toLowerCase().replace('_', '-');
    }
  }
}
