package cloudos.google.integration;

import cloudos.Providers;
import cloudos.pricing.PricingInfo;
import cloudos.pricing.PricingInfoRepository;
import cloudos.pricing.PricingType;
import cloudos.pricing.PricingValue;
import cloudos.pricing.PricingValueUnit;
import cloudos.pricing.PricingVersion;
import cloudos.pricing.PricingVersionRepository;
import cloudos.utils.FileUtils;

import com.google.gson.Gson;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Represent a list of prices.
 * 
 */
@Service
public class GooglePriceList {

  private static final String PREEMPTIBLE = "-PREEMPTIBLE";
  public static final String PRICING_JSON =
      "http://cloudpricingcalculator.appspot.com/static/data/pricelist.json";
  private static final String SUSTAINED_USE_TIERS = "sustained_use_tiers";
  private static final String SUSTAINED_USE_BASE = "sustained_use_base";
  private static final String GCP_PRICE_LIST = "gcp_price_list";
  private static final String UPDATED = "updated";
  private static final String VERSION = "version";
  private final SimpleDateFormat formatter = new SimpleDateFormat("dd-MMMM-yyyy");// 24-August-2017

  @Autowired
  PricingVersionRepository pricingRepository;

  @Autowired
  PricingInfoRepository infoRepository;

  private Gson gson = new Gson();

  /**
   * Get Json with the info.
   * 
   * @return Json object
   * @throws Exception if something goes wrong
   */
  @SuppressWarnings("unchecked")
  private Map<String, Object> getJson() throws Exception {
    InputStreamReader json = FileUtils.readUrlContentToReader(PRICING_JSON);
    return gson.fromJson(json, Map.class);
  }

  /**
   * Get file actual version.
   * 
   * @return version value
   * @throws Exception if someting goes wrong
   */
  public String getVersion() throws Exception {
    Map<String, Object> jsonObject = this.getJson();
    return (String) jsonObject.get(VERSION);
  }

  /**
   * Load the price list from the google.
   * 
   * @param type
   *
   * @throws Exception if something goes wrong
   */
  @SuppressWarnings("unchecked")
  public void loadPricing(GoogleService type) throws Exception {
    Map<String, Object> jsonObject = this.getJson();

    String version = (String) jsonObject.get(VERSION);
    String updated = (String) jsonObject.get(UPDATED);

    Map<String, Object> priceListJson = (Map<String, Object>) jsonObject.get(GCP_PRICE_LIST);

    Double sustainedUseBase = (Double) priceListJson.get(SUSTAINED_USE_BASE);
    Map<String, Object> sustainedUseTiers =
        (Map<String, Object>) priceListJson.get(SUSTAINED_USE_TIERS);

    Date loadingDate = new Date();
    Date referenceDate = formatter.parse(updated);

    PricingVersion pricingVersion = new PricingVersion();
    pricingVersion.setUrl(PRICING_JSON);
    pricingVersion.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
    pricingVersion.setType(PricingType.INSTANCE);
    pricingVersion.setContent(gson.toJson(jsonObject));
    pricingVersion.setVersion(version);
    pricingVersion.setLoadingDate(loadingDate);
    pricingVersion.setReferenceDate(referenceDate);
    pricingVersion = pricingRepository.save(pricingVersion);

    List<PricingInfo> infos = new ArrayList<>();
    for (Entry<String, Object> entry : priceListJson.entrySet()) {
      String key = entry.getKey();
      if (!key.contains(type.name)) {
        // ignore services not matching
        continue;
      }
      PricingInfo info = new PricingInfo();
      info.setName(key);
      String machineType = "";
      if (key.contains(GoogleService.CP_COMPUTEENGINE_VMIMAGE.name)) {
        machineType = key.replace(GoogleService.CP_COMPUTEENGINE_VMIMAGE.name + "-", "");
      }
      info.setMachineType(machineType);
      info.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
      Map<String, Object> value = (Map<String, Object>) entry.getValue();
      // add the sustained info
      value.put(SUSTAINED_USE_BASE, gson.toJson(sustainedUseBase));
      value.put(SUSTAINED_USE_TIERS, gson.toJson(sustainedUseTiers));
      info.setContent(gson.toJson(value));
      info.setPricingVersion(pricingVersion.getId());
      info.setReferenceDate(referenceDate);
      info.setLoadingDate(loadingDate);
      infos.add(info);
    }
    infoRepository.save(infos);
  }

  /**
   * Return the price of the service.
   *
   * @param service name
   * @param name key
   * @param region of service
   * @param spotInstance or not
   * @return pricing value
   */
  @SuppressWarnings("unchecked")
  public PricingValue getPrice(GoogleService service, String name, String region,
      Boolean spotInstance) {
    List<PricingVersion> versions = pricingRepository
        .findByProviderAndType(Providers.GOOGLE_COMPUTE_ENGINE, PricingType.INSTANCE);
    if (CollectionUtils.isNotEmpty(versions)) {
      // get the latest
      PricingVersion pricingVersion =
          versions.stream().sorted((a, b) -> a.getVersion().compareTo(b.getVersion()))
              .collect(Collectors.toList()).get(0);

      // get the pricing of using the name
      String keyName = String.format("%s-%s", service.name, name);
      if (spotInstance != null && spotInstance) {
        keyName += PREEMPTIBLE;
      }
      PricingInfo pricingInfo =
          infoRepository.findByPricingVersionAndName(pricingVersion.getId(), keyName);
      if (pricingInfo != null) {
        Map<String, Object> json = gson.fromJson(pricingInfo.getContent(), Map.class);
        Double price = (Double) json.get(region);
        if (price == null) {
          // if price not found for region, try to get the generic region
          price = (Double) json.get(getRegionForPrice(region));
        }
        // TODO: review the default values: USD and HRS
        return new PricingValue("USD", price, PricingValueUnit.HRS);
      }
    }
    return null;
  }

  /**
   * Map for the region - region - prices.
   *
   * @param region common name
   * @return generic name
   */
  private static String getRegionForPrice(String region) {
    String priceRegion = "";
    if (region.startsWith("us")) {
      priceRegion = "us";
    } else if (region.startsWith("europe")) {
      priceRegion = "europe";
    } else if (region.startsWith("asia-east")) {
      priceRegion = "asia-east";
    } else if (region.startsWith("asia-northeast")) {
      priceRegion = "asia-northeast";
    } else if (region.startsWith("asia")) {
      priceRegion = "asia";
    }
    return priceRegion;
  }

  public enum GoogleService {
    CP_COMPUTEENGINE_VMIMAGE("CP-COMPUTEENGINE-VMIMAGE"), CP_COMPUTEENGINE("CP-COMPUTEENGINE"),
    // CP-COMPUTEENGINE-LOCAL-SSD, CP-COMPUTEENGINE-OS
    CP_COMPUTEENGINE_STORAGE("CP-COMPUTEENGINE-STORAGE"),
    // "CP-COMPUTEENGINE-STORAGE-PD-CAPACITY": {
    // "CP-COMPUTEENGINE-STORAGE-PD-SSD": {
    // "CP-COMPUTEENGINE-VMIMAGE-F1-MICRO": {
    // "CP-COMPUTEENGINE-VMIMAGE-G1-SMALL": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-STANDARD-1": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-STANDARD-2": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-STANDARD-4": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-STANDARD-8": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-STANDARD-16": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-STANDARD-32": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-HIGHMEM-2": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-HIGHMEM-4": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-HIGHMEM-8": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-HIGHMEM-16": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-HIGHMEM-32": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-HIGHCPU-2": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-HIGHCPU-4": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-HIGHCPU-8": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-HIGHCPU-16": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-HIGHCPU-32": {
    // "CP-COMPUTEENGINE-VMIMAGE-F1-MICRO-PREEMPTIBLE": {
    // "CP-COMPUTEENGINE-VMIMAGE-G1-SMALL-PREEMPTIBLE": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-STANDARD-1-PREEMPTIBLE": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-STANDARD-2-PREEMPTIBLE": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-STANDARD-4-PREEMPTIBLE": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-STANDARD-8-PREEMPTIBLE": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-STANDARD-16-PREEMPTIBLE": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-STANDARD-32-PREEMPTIBLE": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-HIGHMEM-2-PREEMPTIBLE": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-HIGHMEM-4-PREEMPTIBLE": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-HIGHMEM-8-PREEMPTIBLE": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-HIGHMEM-16-PREEMPTIBLE": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-HIGHMEM-32-PREEMPTIBLE": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-HIGHCPU-2-PREEMPTIBLE": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-HIGHCPU-4-PREEMPTIBLE": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-HIGHCPU-8-PREEMPTIBLE": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-HIGHCPU-16-PREEMPTIBLE": {
    // "CP-COMPUTEENGINE-VMIMAGE-N1-HIGHCPU-32-PREEMPTIBLE": {
    // "CP-COMPUTEENGINE-LOCAL-SSD": {
    // "CP-COMPUTEENGINE-OS": {
    // "CP-COMPUTEENGINE-STORAGE-PD-CAPACITY": {
    // "CP-COMPUTEENGINE-STORAGE-PD-SSD": {
    // "CP-COMPUTEENGINE-PD-IO-REQUEST": {
    // "CP-COMPUTEENGINE-STORAGE-PD-SNAPSHOT": {
    // "CP-BIGSTORE-CLASS-A-REQUEST": {
    // "CP-BIGSTORE-CLASS-B-REQUEST": {
    // "CP-CLOUDSQL-PERUSE-D0": {
    // "CP-CLOUDSQL-PERUSE-D1": {
    // "CP-CLOUDSQL-PERUSE-D2": {
    // "CP-CLOUDSQL-PERUSE-D4": {
    // "CP-CLOUDSQL-PERUSE-D8": {
    // "CP-CLOUDSQL-PERUSE-D16": {
    // "CP-CLOUDSQL-PERUSE-D32": {
    // "CP-CLOUDSQL-PACKAGE-D0": {
    // "CP-CLOUDSQL-PACKAGE-D1": {
    // "CP-CLOUDSQL-PACKAGE-D2": {
    // "CP-CLOUDSQL-PACKAGE-D4": {
    // "CP-CLOUDSQL-PACKAGE-D8": {
    // "CP-CLOUDSQL-PACKAGE-D16": {
    // "CP-CLOUDSQL-PACKAGE-D32": {
    // "CP-CLOUDSQL-STORAGE": {
    // "CP-CLOUDSQL-TRAFFIC": {
    // "CP-CLOUDSQL-IO": {
    // "CP-BIGSTORE-STORAGE": {
    // "CP-BIGSTORE-STORAGE-DRA": {
    // "CP-NEARLINE-STORAGE": {
    // "CP-NEARLINE-RESTORE-SIZE": {
    // "FORWARDING_RULE_CHARGE_BASE": {
    // "FORWARDING_RULE_CHARGE_EXTRA": {
    // "NETWORK_LOAD_BALANCED_INGRESS": {
    // "CP-COMPUTEENGINE-INTERNET-EGRESS-NA-NA": {
    // "CP-COMPUTEENGINE-INTERNET-EGRESS-APAC-APAC": {
    // "CP-COMPUTEENGINE-INTERNET-EGRESS-AU-AU": {
    // "CP-COMPUTEENGINE-INTERNET-EGRESS-CN-CN": {
    // "CP-COMPUTEENGINE-INTERCONNECT-US-US": {
    // "CP-COMPUTEENGINE-INTERCONNECT-EU-EU": {
    // "CP-COMPUTEENGINE-INTERCONNECT-APAC-APAC": {
    // "CP-COMPUTEENGINE-INTERNET-EGRESS-ZONE": {
    // "CP-COMPUTEENGINE-INTERNET-EGRESS-REGION": {
    // "CP-APP-ENGINE-INSTANCES": {
    // "CP-APP-ENGINE-OUTGOING-TRAFFIC": {
    // "CP-APP-ENGINE-CLOUD-STORAGE": {
    // "CP-APP-ENGINE-MEMCACHE": {
    // "CP-APP-ENGINE-SEARCH": {
    // "CP-APP-ENGINE-INDEXING-DOCUMENTS": {
    // "CP-APP-ENGINE-DOCUMENT-STORAGE": {
    // "CP-APP-ENGINE-LOGS-API": {
    // "CP-APP-ENGINE-TASK-QUEUE": {
    // "CP-APP-ENGINE-LOGS-STORAGE": {
    // "CP-APP-ENGINE-SSL-VIRTUAL-IP": {
    // "CP-CLOUD-DATASTORE-INSTANCES": {
    // "CP-CLOUD-DATASTORE-WRITE-OP": {
    // "CP-CLOUD-DATASTORE-READ-OP": {
    // "CP-CLOUD-DATASTORE-ENTITY-READ": {
    // "CP-CLOUD-DATASTORE-ENTITY-WRITE": {
    // "CP-CLOUD-DATASTORE-ENTITY-DELETE": {
    // "CP-BIGQUERY-GENERAL": {
    // "CP-CLOUD-DNS-ZONES": {
    // "CP-CLOUD-DNS-QUERIES": {
    // "CP-TRANSLATE-API-TRANSLATION": {
    // "CP-TRANSLATE-API-DETECTION": {
    // "CP-PREDICTION-PREDICTION": {
    // "CP-PREDICTION-BULK-TRAINING": {
    // "CP-PREDICTION-STREAMING-TRAINING": {
    // "CP-GENOMICS-STORAGE": {
    // "CP-GENOMICS-QUERIES": {
    // "CP-DATAFLOW-BATCH-VCPU": {
    // "CP-DATAFLOW-STREAMING-VCPU": {
    // "CP-DATAFLOW-BATCH-MEMORY": {
    // "CP-DATAFLOW-STREAMING-MEMORY": {
    // "CP-DATAFLOW-BATCH-STORAGE-PD": {
    // "CP-DATAFLOW-STREAMING-STORAGE-PD": {
    // "CP-DATAFLOW-BATCH-STORAGE-PD-SSD": {
    // "CP-DATAFLOW-STREAMING-STORAGE-PD-SSD": {
    // "CP-BIGTABLE-NODES": {
    // "CP-BIGTABLE-SSD": {
    // "CP-BIGTABLE-HDD": {
    // "CP-PUB-SUB-OPERATIONS": {
    // "CP-COMPUTEENGINE-STATIC-IP-CHARGE": {
    // "CP-COMPUTEENGINE-VPN": {
    // "CP-DATAPROC": {
    // "CP-CONTAINER-ENGINE-BASIC": {
    // "CP-CONTAINER-ENGINE-STANDARD": {
    // "CP-SUPPORT-BRONZE": {
    // "CP-SUPPORT-SILVER": {
    // "CP-SUPPORT-GOLD": {
    // "CP-COMPUTEENGINE-CUSTOM-VM-CORE": {
    // "CP-COMPUTEENGINE-CUSTOM-VM-RAM": {
    // "CP-COMPUTEENGINE-CUSTOM-VM-CORE-PREEMPTIBLE": {
    // "CP-COMPUTEENGINE-CUSTOM-VM-RAM-PREEMPTIBLE": {
    // "CP-DB-F1-MICRO": {
    // "CP-DB-G1-SMALL": {
    // "CP-DB-N1-STANDARD-1": {
    // "CP-DB-N1-STANDARD-2": {
    // "CP-DB-N1-STANDARD-4": {
    // "CP-DB-N1-STANDARD-8": {
    // "CP-DB-N1-STANDARD-16": {
    // "CP-DB-N1-HIGHMEM-2": {
    // "CP-DB-N1-HIGHMEM-4": {
    // "CP-DB-N1-HIGHMEM-8": {
    // "CP-DB-N1-HIGHMEM-16": {
    // "CP-CLOUDSQL-STORAGE-SSD": {
    // "CP-CLOUDSQL-BACKUP": {
    // "CP-CLOUDSQL-IO_REPEATED": {
    // "CP-VISION-LABEL-DETECTION": {
    // "CP-VISION-OCR": {
    // "CP-VISION-EXPLICIT-CONTENT-DETECTION": {
    // "CP-VISION-FACIAL-DETECTION": {
    // "CP-VISION-LANDMARK-DETECTION": {
    // "CP-VISION-LOGO-DETECTION": {
    // "CP-VISION-IMAGE-PROPERTIES": {
    // "CP-STACKDRIVER-MONITORED-RESOURCES": {
    // "CP-STACKDRIVER-LOGS-VOLUME": {
    // "CP-STACKDRIVER-METRICS-DESCRIPTION": {
    // "CP-STACKDRIVER-TIME-SERIES": {
    // "CP-CLOUDCDN-CACHE-EGRESS-APAC": {
    // "CP-CLOUDCDN-CACHE-EGRESS-AU": {
    // "CP-CLOUDCDN-CACHE-EGRESS-CN": {
    // "CP-CLOUDCDN-CACHE-EGRESS-EU": {
    // "CP-CLOUDCDN-CACHE-EGRESS-NA": {
    // "CP-CLOUDCDN-CACHE-EGRESS-OTHER": {
    // "CP-CLOUDCDN-CACHE-FILL-INTRA-APAC": {
    // "CP-CLOUDCDN-CACHE-FILL-INTRA-EU": {
    // "CP-CLOUDCDN-CACHE-FILL-INTRA-NA": {
    // "CP-CLOUDCDN-CACHE-FILL-INTER-AU": {
    // "CP-CLOUDCDN-CACHE-FILL-INTER-OTHER": {
    // "CP-CLOUDCDN-CACHE-LOOKUP-REQUESTS": {
    // "CP-CLOUDCDN-CACHE-INVALIDATION": {
    // "CP-SPEECH-API-RECOGNITION": {
    // "CP-NL-API-ENTITY-RECOGNITION": {
    // "CP-NL-API-SENTIMENT-ANALYSIS": {
    // "CP-NL-API-SYNTAX-ANALYSIS": {
    // "BIG_QUERY_FLAT_RATE_ANALYSIS": {
    // "BIG_QUERY_FLAT_RATE_ANALYSIS_12_MONTHS": {
    // "CP-ML-TRAINING": {
    // "CP-ML-PREDICTION-ONLINE": {
    // "CP-ML-PREDICTION-PROCESSING": {
    // "CP-BIGSTORE-STORAGE-COLDLINE": {
    // "CP-BIGSTORE-DATA-RETRIEVAL-COLDLINE": {
    // "CP-BIGSTORE-STORAGE-MULTI_REGIONAL": {
    // "CP-BIGSTORE-STORAGE-REGIONAL": {
    // "CP-BIGSTORE-STORAGE-NEARLINE-US": {
    // "CP-GAE-FLEX-INSTANCE-CORE-HOURS": {
    // "CP-GAE-FLEX-INSTANCE-RAM": {
    // "CP-GAE-FLEX-STORAGE-PD-CAPACITY": {
    ;

    String name;

    GoogleService(String name) {
      this.name = name;
    }
  }
}
