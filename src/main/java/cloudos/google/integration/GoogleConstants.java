package cloudos.google.integration;

public interface GoogleConstants {

  /** Set the Network configuration values of the sample VM instance to be created. */
  public static final String NETWORK_INTERFACE_CONFIG = "ONE_TO_ONE_NAT";

  public static final String NETWORK_ACCESS_CONFIG = "External NAT";

  public enum ImagesOs {
    CENTOS("centos-cloud"),
    GOOGLE("google-containers"),
    COREOS("coreos-cloud"),
    DEBIAN("debian-cloud"),
    RED_HAT("rhel-cloud"),
    OPENSUSE("opensuse-cloud"),
    SUSE("suse-cloud"),
    UBUNTU("ubuntu-os-cloud"),
    WINDOWS("windows-cloud");

    String path;

    private ImagesOs(String path) {
      this.path = path;
    }
  }
}
