package cloudos.google.integration;

import cloudos.Providers;
import cloudos.credentials.GoogleCredentialManagement;
import cloudos.google.GoogleCredential;
import cloudos.models.CloudosCredential;
import cloudos.models.CloudosCredentialRepository;
import java.io.ByteArrayInputStream;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/** Parent class of the integration classes. */
public abstract class AbstractGoogleIntegration {
  
  private final Logger logger = LoggerFactory.getLogger(AbstractGoogleIntegration.class);

  protected static final String HTTPS_WWW_GOOGLEAPIS_COM_AUTH_CLOUD_PLATFORM =
      "https://www.googleapis.com/auth/cloud-platform";

  @Autowired
  private CloudosCredentialRepository cloudosCredentialRepository;

  @Autowired
  private GoogleCredentialManagement googleCredentialManagement;

  private GoogleCredential googleCredential;

  /**
   * Return the credential.
   *
   * @return GoogleCredential
   */
  protected GoogleCredential getGoogleCredential() {
    
    if (googleCredential == null) {
      List<CloudosCredential> findByProvider =
          cloudosCredentialRepository.findByProvider(Providers.GOOGLE_COMPUTE_ENGINE);
      if (CollectionUtils.isNotEmpty(findByProvider)) {
        googleCredential = (GoogleCredential) findByProvider.get(0);
      }
    }
    
    if (googleCredential != null && !googleCredential.isDecrypted()) {
      try {
        googleCredential = googleCredentialManagement.decryptSensitiveData(googleCredential);
      } catch (Exception e) {
        logger.error("Error while decrypting GoogleCredential:");
        logger.error(e.getMessage());
      }
    }
    
    return googleCredential;
  }

  /**
   * Return the project.
   *
   * @return Project name
   */
  protected String getGoogleProject() {
    if (getGoogleCredential() != null) {
      return getGoogleCredential().getProject();
    }
    return null;
  }

  /**
   * Return an input stream for the json file.
   *
   * @return InputStream with the json
   */
  protected ByteArrayInputStream getJsonInputStream(GoogleCredential googleCredential) {
    if (googleCredential != null) {
      return new ByteArrayInputStream(googleCredential.getJsonCredential().getBytes());
    }
    return null;
  }

  /**
   * Reset the credential.
   *
   * @param cloudosCredential credential
   */
  protected void resetCredential(GoogleCredential cloudosCredential) {
    googleCredential = cloudosCredential;
  }

  protected abstract void setUp();

  /**
   * Update for the new credential Force the reconnect.
   *
   * @param cloudosCredential to update
   */
  public void updateCredential(GoogleCredential cloudosCredential) {
    this.resetCredential(cloudosCredential);
    setUp();
  }

  public abstract boolean validateCredential(GoogleCredential cloudosCredential);
}
