package cloudos.google.integration;

import cloudos.google.GoogleCredential;
import cloudos.utils.DateUtil;

import com.google.api.MetricDescriptor;
import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.monitoring.v3.MetricServiceClient;
import com.google.cloud.monitoring.v3.MetricServiceSettings;
import com.google.cloud.monitoring.v3.PagedResponseWrappers.ListMetricDescriptorsPagedResponse;
import com.google.cloud.monitoring.v3.PagedResponseWrappers.ListTimeSeriesPagedResponse;
import com.google.monitoring.v3.Aggregation;
import com.google.monitoring.v3.Aggregation.Aligner;
import com.google.monitoring.v3.Aggregation.Reducer;
import com.google.monitoring.v3.ListTimeSeriesRequest;
import com.google.monitoring.v3.ListTimeSeriesRequest.Builder;
import com.google.monitoring.v3.ListTimeSeriesRequest.TimeSeriesView;
import com.google.monitoring.v3.ProjectName;
import com.google.monitoring.v3.TimeInterval;
import com.google.monitoring.v3.TimeSeries;
import com.google.protobuf.Timestamp;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import lombok.extern.log4j.Log4j2;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.threeten.bp.Duration;

@Service
@Log4j2
public class GoogleMetricsIntegration extends AbstractGoogleIntegration {

  private static final Duration TIMEOUT = Duration.ofSeconds(30);

  public static final String user = "cloudos";

  private MetricServiceClient metricServiceClient;

  @Override
  protected void resetCredential(GoogleCredential cloudosCredential) {
    super.resetCredential(cloudosCredential);
    metricServiceClient = null;
  }

  @Override
  protected synchronized void setUp() {
    try {
      if (metricServiceClient == null) {
        metricServiceClient = createMetric(getGoogleCredential());
      }
    } catch (Exception e) {
      // TODO: handle exception
      throw new RuntimeException("Error setting up the service", e);
    }
  }

  /**
   * Create a client.
   *
   * @param googleCredential credential
   * @return MetricServiceClient
   * @throws Exception if something goes wrong
   */
  private MetricServiceClient createMetric(GoogleCredential googleCredential) throws Exception {
    GoogleCredentials credential =
        GoogleCredentials.fromStream(getJsonInputStream(googleCredential));

    if (credential.createScopedRequired()) {
      credential = credential.createScoped(
          Arrays.asList(GoogleScope.CLOUD_PLATFORM.getValue(), GoogleScope.MONITORING.getValue(),
              GoogleScope.MONITORING_READ.getValue(), GoogleScope.MONITORING_WRITE.getValue()));
    }

    MetricServiceSettings.Builder metricServiceSettingsBuilder =
        MetricServiceSettings.defaultBuilder();

    metricServiceSettingsBuilder.getMonitoredResourceDescriptorSettings().getRetrySettingsBuilder()
        .setTotalTimeout(TIMEOUT);

    MetricServiceSettings metricServiceSettings = metricServiceSettingsBuilder
        .setCredentialsProvider(FixedCredentialsProvider.create(credential)).build();
    return MetricServiceClient.create(metricServiceSettings);

  }

  /**
   * Return the service.
   *
   * @return MetricServiceClient
   */
  private MetricServiceClient getMetricService() {
    if (metricServiceClient == null) {
      setUp();
    }
    return metricServiceClient;
  }

  /**
   * Retrieve a time series for an instance.
   *
   * @param instanceId id
   * @param metric metric
   * @param timeWindowInSeconds window
   * @param timeFrameInSeconds frame
   * @param aligner aligner
   * @return Iterable of TimeSeries
   */
  public Iterable<TimeSeries> retrieveMetricTimeSeriesForInstance(String instanceId, String metric,
      long timeWindowInSeconds, int timeFrameInSeconds, Aligner aligner) {
    return this.retrieveMetricTimeSeries(Arrays.asList(instanceId), metric, timeWindowInSeconds,
        timeFrameInSeconds, aligner, null);
  }

  /**
   * Retrieve a time series for a list of instances.
   *
   * @param instanceIds ids
   * @param metric metric
   * @param timeWindowInSeconds window
   * @param timeFrameInSeconds frame
   * @param aligner aligner
   * @return Iterable of TimeSeries
   */
  public Iterable<TimeSeries> retrieveMetricTimeSeriesForInstance(List<String> instanceIds,
      String metric, long timeWindowInSeconds, int timeFrameInSeconds, Aligner aligner,
      Reducer reducer) {
    return this.retrieveMetricTimeSeries(instanceIds, metric, timeWindowInSeconds,
        timeFrameInSeconds, aligner, reducer);
  }

  /**
   * Retrieve a time series for all instances.
   *
   * @param metric metric
   * @param timeWindowInSeconds window
   * @param timeFrameInSeconds frame
   * @param aligner aligner
   * @param reducer reducer
   * @return Iterable of TimeSeries
   */
  public Iterable<TimeSeries> getTimeSeriesForAll(String metric, long timeWindowInSeconds,
      int timeFrameInSeconds, Aligner aligner, Reducer reducer) {
    return this.retrieveMetricTimeSeries(null, metric, timeWindowInSeconds, timeFrameInSeconds,
        aligner, reducer);
  }

  /**
   * Retrieve the time series.
   *
   * @param instanceIds ids
   * @param metric metric
   * @param timeWindowInSeconds window
   * @param timeFrameInSeconds frame
   * @param aligner aligner
   * @param reducer reducer
   * @return Iterable of TimeSeries
   */
  private Iterable<TimeSeries> retrieveMetricTimeSeries(List<String> instanceIds, String metric,
      long timeWindowInSeconds, int timeFrameInSeconds, Aligner aligner, Reducer reducer) {
    // build the request
    Builder requestBuilder = ListTimeSeriesRequest.newBuilder();
    //
    requestBuilder.setNameWithProjectName(ProjectName.create(super.getGoogleProject()));
    //
    // interval
    // interval
    TimeInterval.Builder intervalBuilder = TimeInterval.newBuilder();
    // (Date.now() / 1000) - (60 * 20)
    long referenceTime = System.currentTimeMillis() / 1000;
    intervalBuilder.setStartTime(
        Timestamp.newBuilder().setSeconds(referenceTime - (timeWindowInSeconds)).build());
    // Date.now() / 1000
    intervalBuilder.setEndTime(Timestamp.newBuilder().setSeconds(referenceTime).build());
    TimeInterval timeInterval = intervalBuilder.build();
    requestBuilder.setInterval(timeInterval);
    // metrics
    StringBuffer filter = new StringBuffer();
    filter.append("metric.type=\"" + metric + "\"");
    if (CollectionUtils.isNotEmpty(instanceIds)) {
      filter.append(" AND ( ");
      boolean first = true;
      for (String instanceId : instanceIds) {
        if (!first) {
          filter.append(" OR ");
        }
        filter.append("metric.label.instance_name=\"" + instanceId + "\"");
        first = false;
      }
      filter.append(")");
    }
    requestBuilder.setFilter(filter.toString());
    //
    requestBuilder.setView(TimeSeriesView.FULL);
    requestBuilder.setPageSize(50);
    //

    // aggregation
    Aggregation.Builder aggregationBuilder = Aggregation.newBuilder();
    aggregationBuilder.setAlignmentPeriod(
        com.google.protobuf.Duration.newBuilder().setSeconds(timeFrameInSeconds).build());
    aggregationBuilder.setPerSeriesAligner(aligner);
    if (reducer != null) {
      aggregationBuilder.setCrossSeriesReducer(reducer);
    }
    requestBuilder.setAggregation(aggregationBuilder.build());

    log.info("Retrieve info: {} for: {} Interval: {} Frame: {} Aligner: {} Reducer: {}", metric,
        (CollectionUtils.isNotEmpty(instanceIds) ? Arrays.toString(instanceIds.toArray()) : ""),
        formatInterval(timeInterval), timeFrameInSeconds, aligner.name(),
        reducer != null ? reducer : "");

    ListTimeSeriesRequest request = requestBuilder.build();
    ListTimeSeriesPagedResponse listTimeSeries = getMetricService().listTimeSeries(request);
    return listTimeSeries.iterateAll();
  }

  public static String formatInterval(TimeInterval timeInterval) {
    return formatTimestamp(timeInterval.getStartTime()) + " to "
        + formatTimestamp(timeInterval.getEndTime());
  }

  public static String formatTimestamp(Timestamp timestamp) {
    return DateUtil.printDate(getDateFromTimestamp(timestamp));
  }

  public static Date getDateFromTimestamp(Timestamp timestamp) {
    return new Date(
        Instant.ofEpochSecond(timestamp.getSeconds(), timestamp.getNanos()).toEpochMilli());
  }

  /**
   * Return all available metrics.
   *
   * @return Iterable of MetricDescriptor
   */
  public Iterable<MetricDescriptor> listAllMetrics() {
    ListMetricDescriptorsPagedResponse listMetricDescriptors =
        getMetricService().listMetricDescriptors(ProjectName.create(super.getGoogleProject()));
    Iterable<MetricDescriptor> metricdescriptions = listMetricDescriptors.iterateAll();
    return metricdescriptions;
  }

  /** Generate (print on console) a list the metrics for the GoogleMetrics class. */
  public void generatelistAllMetrics() {
    ListMetricDescriptorsPagedResponse listMetricDescriptors =
        getMetricService().listMetricDescriptors(ProjectName.create(super.getGoogleProject()));
    Iterable<MetricDescriptor> metricdescriptions = listMetricDescriptors.iterateAll();
    List<String> result = new ArrayList<>();
    for (MetricDescriptor metricDescriptor : metricdescriptions) {
      StringBuffer buffer = new StringBuffer();
      // logger.info(metricDescriptor.getAllFields());
      String metricTypeName =
          metricDescriptor.getType().replace('/', '.').replace('.', '_').toUpperCase();
      buffer.append(metricTypeName);
      buffer.append("@");
      buffer.append("// " + metricDescriptor.getDescription().replaceAll("\n", " "));
      buffer.append("@");
      buffer.append("public static final String " + metricTypeName + " = \""
          + metricDescriptor.getType() + "\";");
      result.add(buffer.toString());
    }
    Collections.sort(result);
    log.info("\n\n\n\n####################################");
    for (String string : result) {
      String[] split = string.split("@");
      log.info(split[1]);
      log.info(split[2]);
    }
  }

  /**
   * Validate the credential.
   *
   * @param cloudosCredential to be validated
   * @return true if it is valid
   */
  @Override
  public boolean validateCredential(GoogleCredential cloudosCredential) {
    try {
      createMetric(cloudosCredential)
          .listMetricDescriptors(ProjectName.create(cloudosCredential.getProject()));
      return true;
    } catch (Exception e) {
      log.error("Validating credential: {}", cloudosCredential.toString(), e);
      return false;
    }
  }
}
