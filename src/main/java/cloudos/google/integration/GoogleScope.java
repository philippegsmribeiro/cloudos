package cloudos.google.integration;

public enum GoogleScope {

  /**
   * https://www.googleapis.com/auth/cloud-platform View and manage your data across Google Cloud
   * Platform services
   */
  CLOUD_PLATFORM("https://www.googleapis.com/auth/cloud-platform"),
  /**
   * https://www.googleapis.com/auth/monitoring View and write monitoring data for all of your
   * Google and third-party Cloud and API projects
   */
  MONITORING("https://www.googleapis.com/auth/monitoring"),
  /**
   * https://www.googleapis.com/auth/monitoring.read View monitoring data for all of your Google
   * Cloud and third-party projects
   */
  MONITORING_READ("https://www.googleapis.com/auth/monitoring.read"),
  /**
   * https://www.googleapis.com/auth/monitoring.write Publish metric data to your Google Cloud
   * projects
   */
  MONITORING_WRITE("https://www.googleapis.com/auth/monitoring.write");

  private String value;

  GoogleScope(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}
