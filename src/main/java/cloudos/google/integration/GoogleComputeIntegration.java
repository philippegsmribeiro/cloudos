package cloudos.google.integration;

import cloudos.google.GoogleCredential;
import cloudos.google.integration.GoogleConstants.ImagesOs;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.gax.paging.Page;
import com.google.api.services.compute.Compute.Networks;
import com.google.api.services.compute.Compute.Subnetworks;
import com.google.api.services.compute.ComputeRequest;
import com.google.api.services.compute.model.Address;
import com.google.api.services.compute.model.AddressList;
import com.google.api.services.compute.model.BackendService;
import com.google.api.services.compute.model.BackendServiceList;
import com.google.api.services.compute.model.Firewall;
import com.google.api.services.compute.model.FirewallList;
import com.google.api.services.compute.model.ForwardingRule;
import com.google.api.services.compute.model.ForwardingRuleList;
import com.google.api.services.compute.model.HttpHealthCheck;
import com.google.api.services.compute.model.HttpHealthCheckList;
import com.google.api.services.compute.model.HttpsHealthCheck;
import com.google.api.services.compute.model.HttpsHealthCheckList;
import com.google.api.services.compute.model.InstanceGroup;
import com.google.api.services.compute.model.InstanceGroupList;
import com.google.api.services.compute.model.InstanceGroupsAddInstancesRequest;
import com.google.api.services.compute.model.InstanceGroupsRemoveInstancesRequest;
import com.google.api.services.compute.model.InstanceReference;
import com.google.api.services.compute.model.Network;
import com.google.api.services.compute.model.Subnetwork;
import com.google.api.services.compute.model.TargetPool;
import com.google.api.services.compute.model.TargetPoolList;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.WaitForOption;
import com.google.cloud.WaitForOption.CheckingPeriod;
import com.google.cloud.WaitForOption.Timeout;
import com.google.cloud.compute.AttachedDisk;
import com.google.cloud.compute.AttachedDisk.AttachedDiskConfiguration;
import com.google.cloud.compute.AttachedDisk.PersistentDiskConfiguration;
import com.google.cloud.compute.Compute;
import com.google.cloud.compute.Compute.OperationField;
import com.google.cloud.compute.Compute.OperationFilter;
import com.google.cloud.compute.Compute.OperationListOption;
import com.google.cloud.compute.ComputeOptions;
import com.google.cloud.compute.DiskId;
import com.google.cloud.compute.DiskInfo;
import com.google.cloud.compute.DiskTypeId;
import com.google.cloud.compute.Image;
import com.google.cloud.compute.ImageId;
import com.google.cloud.compute.Instance;
import com.google.cloud.compute.InstanceId;
import com.google.cloud.compute.InstanceInfo;
import com.google.cloud.compute.MachineType;
import com.google.cloud.compute.MachineTypeId;
import com.google.cloud.compute.Metadata;
import com.google.cloud.compute.Metadata.Builder;
import com.google.cloud.compute.NetworkId;
import com.google.cloud.compute.NetworkInterface;
import com.google.cloud.compute.NetworkInterface.AccessConfig;
import com.google.cloud.compute.Operation;
import com.google.cloud.compute.Operation.OperationError;
import com.google.cloud.compute.Operation.Status;
import com.google.cloud.compute.Region;
import com.google.cloud.compute.SchedulingOptions;
import com.google.cloud.compute.ServiceAccount;
import com.google.cloud.compute.Snapshot;
import com.google.cloud.compute.SnapshotDiskConfiguration;
import com.google.cloud.compute.SnapshotId;
import com.google.cloud.compute.SnapshotInfo;
import com.google.cloud.compute.Tags;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class GoogleComputeIntegration extends AbstractGoogleIntegration {

  private static final String ATTACHED_DISK_DEV0 = "dev0";
  private static final long DEFAULT_TIMEOUT = 60 * 1000L;
  private static final int LONG_TIMEOUT = 120 * 1000;
  private static final String COMPUTE_INSTANCES_PREEMPTED = "compute.instances.preempted";
  public static final String KEY_NAME = "keyName";
  public static final String SSH_KEYS = "ssh-keys";

  public static final String user = "cloudos";

  private Compute computeService;
  private com.google.api.services.compute.Compute oldComputeService;

  @Override
  protected void resetCredential(GoogleCredential cloudosCredential) {
    super.resetCredential(cloudosCredential);
    computeService = null;
  }

  @Override
  protected synchronized void setUp() {
    try {
      if (computeService == null) {
        GoogleCredential googleCredential = getGoogleCredential();
        if (googleCredential != null) {
          computeService = createCompute(googleCredential);

          // old compute service for requests not implemented yet on the new library
          // service
          oldComputeService = createOldCompute(googleCredential);
        }
      }
    } catch (Exception e) {
      throw new RuntimeException("Error setting up the service", e);
    }
  }

  /**
   * SetUp the compute service.
   *
   * @param googleCredential credential
   * @return Compute
   * @throws IOException if something goes wrong reading the credentials
   */
  private Compute createCompute(GoogleCredential googleCredential) throws IOException {
    GoogleCredentials credential =
        GoogleCredentials.fromStream(getJsonInputStream(googleCredential));
    if (credential.createScopedRequired()) {
      credential = credential.createScoped(Arrays.asList(GoogleScope.CLOUD_PLATFORM.getValue()));
    }
    return ComputeOptions.newBuilder().setCredentials(credential)
        .setProjectId(googleCredential.getProject()).build().getService();
  }

  /**
   * SetUp the old compute service.
   *
   * @param googleCredential credential
   * @return old Compute
   * @throws Exception if something goes wrong
   */
  private com.google.api.services.compute.Compute createOldCompute(
      GoogleCredential googleCredential) throws Exception {
    com.google.api.client.googleapis.auth.oauth2.GoogleCredential fromStream =
        com.google.api.client.googleapis.auth.oauth2.GoogleCredential
            .fromStream(getJsonInputStream(googleCredential));
    fromStream =
        fromStream.createScoped(Arrays.asList(HTTPS_WWW_GOOGLEAPIS_COM_AUTH_CLOUD_PLATFORM));
    return new com.google.api.services.compute.Compute.Builder(
        GoogleNetHttpTransport.newTrustedTransport(),
        com.google.api.client.json.jackson2.JacksonFactory.getDefaultInstance(), fromStream)
            .build();
  }

  /**
   * Return the service.
   *
   * @return Compute
   */
  private Compute getComputeService() {
    if (computeService == null) {
      setUp();
    }
    return computeService;
  }

  /**
   * Return the old service.
   *
   * @return Compute
   */
  private com.google.api.services.compute.Compute getOldComputeService() {
    if (oldComputeService == null) {
      setUp();
    }
    return oldComputeService;
  }

  /**
   * Get all regions available.
   *
   * @return list of regions
   */
  public Iterable<Region> listRegions() {
    Set<Region> list = new HashSet<>();
    Page<Region> listReturned = getComputeService().listRegions();
    do {
      listReturned.getValues().forEach(t -> list.add(t));
      listReturned = listReturned.getNextPage();
    } while (listReturned != null);
    return list;
  }

  /**
   * Get all instances for a project on that zone.
   *
   * @return list of instances
   */
  public Iterable<Instance> listInstances() {
    Set<Instance> list = new HashSet<>();
    Page<Instance> listReturned = getComputeService().listInstances();
    do {
      listReturned.getValues().forEach(t -> list.add(t));
      listReturned = listReturned.getNextPage();
    } while (listReturned != null);
    return list;
  }

  /**
   * Retrieve the instance.
   *
   * @param zone zone
   * @param instanceName name
   * @return Instance
   */
  public Instance getInstance(String zone, String instanceName) {
    if (StringUtils.isNotBlank(instanceName)) {
      return getComputeService().getInstance(InstanceId.of(zone, instanceName));
    }
    return null;
  }

  /**
   * Get all (not deprecated) images for a project.
   *
   * @return list of images
   */
  public Iterable<Image> listImages() {
    Set<Image> list = new HashSet<>();
    Page<Image> listReturned = getComputeService().listImages();
    do {
      listReturned.getValues().forEach(t -> list.add(t));
      listReturned = listReturned.getNextPage();
    } while (listReturned != null);
    return list;
  }

  /**
   * Get all public images.
   *
   * @return list of images
   */
  public Iterable<Image> listPublicImages() {
    ImagesOs[] values = ImagesOs.values();
    Set<Image> items = new HashSet<>();
    for (ImagesOs images : values) {
      Page<Image> listReturned = getComputeService().listImages(images.path);
      do {
        listReturned.getValues().forEach(t -> items.add(t));
        listReturned = listReturned.getNextPage();
      } while (listReturned != null);
    }
    return items;
  }

  /**
   * Create an instance.
   *
   * @param zone name
   * @param name of instance
   * @param machineType machine
   * @param imageType image
   * @param spot or not
   * @param cloudGroup The cloudGroupId that the instance will belong
   * @return Operation
   */
  public Operation createInstance(String zone, String name, String machineType, ImageId imageType,
      Path key, String keyName, Map<String, String> tags, boolean spot, String cloudGroup) {
    // instance
    List<AttachedDisk> disks = Arrays.asList(createDefaultDisk(zone, name, imageType));
    List<NetworkInterface> networks = Arrays.asList(createDefaultNetwork());
    List<ServiceAccount> serviceAccounts = Collections.singletonList(createDefaultServiceAccount());
    Tags googleTags = Tags.newBuilder("http-server", "https-server").build();
    Metadata metadata = createMetadata(key, keyName, tags, cloudGroup);
    return createInstance(zone, name, machineType, spot, disks, networks, serviceAccounts,
        googleTags, metadata);
  }


  /**
   * Create an instance.
   *
   * @param zone name
   * @param name instance name
   * @param machineType instance type
   * @param spot or not
   * @param disks attached disks
   * @param networks attached networks
   * @param serviceAccounts attached serviceAccounts
   * @param googleTags attached googleTags
   * @param metadata instance metadata
   * @return Operation
   */
  public Operation createInstance(String zone, String name, String machineType, boolean spot,
      List<AttachedDisk> disks, List<NetworkInterface> networks,
      List<ServiceAccount> serviceAccounts, Tags googleTags, Metadata metadata) {
    com.google.cloud.compute.InstanceInfo.Builder builder =
        Instance.newBuilder(InstanceId.of(zone, name), MachineTypeId.of(zone, machineType))
            .setDescription(name + " - CloudOS")
            // network
            .setNetworkInterfaces(networks)
            // disk
            .setAttachedDisks(disks)
            // service account
            .setServiceAccounts(serviceAccounts)
            // enable http / https
            .setTags(googleTags)
            // metadata
            .setMetadata(metadata);
    if (spot) {
      builder.setSchedulingOptions(SchedulingOptions.preemptible());
    }
    InstanceInfo instance = builder.build();
    Operation create = getComputeService().create(instance);
    return create;
  }


  /**
   * Retrieves the list of networks available to the specified project.
   *
   * @return Networks.List - If successful, this method returns a request to list Network
   * @throws IOException - if something goes wrong
   */
  public Networks.List listVpc() throws IOException {
    return getOldComputeService().networks().list(getGoogleProject());
  }

  /**
   * Returns the specified network by name.
   *
   * @param network - Name of the network to return
   * @return Networks.Get - If successful, this method returns a request to get Network
   * @throws IOException - if something goes wrong
   */
  public Network getVpc(String network) throws IOException {
    return processGet(getOldComputeService().networks().get(getGoogleProject(), network));
  }

  /**
   * Create a new VPC network using the data included in the request.
   *
   * @param network - Network request
   * @return - Networks.Insert - If successful, this method returns a request to create Network
   * @throws IOException - if something goes wrong
   */
  public Network insertVpc(Network network) throws Exception {
    this.processOperation(getOldComputeService().networks().insert(getGoogleProject(), network));
    return getVpc(network.getName());
  }

  /**
   * Delete the specified network by name.
   *
   * @param network - Name of the network to delete.
   * @throws IOException - if something goes wrong
   */
  public void deleteVpc(String network) throws Exception {
    this.processOperation(getOldComputeService().networks().delete(getGoogleProject(), network));
  }

  /**
   * Retrieves a list of subnetworks available to the specified region.
   *
   * @param region - Name of the region scoping this request.
   * @return Subnetworks.List - If successful, this method returns a request to list Subnetwork
   * @throws IOException - if something goes wrong
   */
  public Subnetworks.List listSubnetwork(String region) throws IOException {
    return getOldComputeService().subnetworks().list(getGoogleProject(), region);
  }

  /**
   * Returns the specified subnetwork by region and name.
   *
   * @param region - Name of the region scoping this request
   * @param subnetwork - Name of the Subnetwork resource to return
   * @return Subnetwork - If successful, this method returns a request to get the Subnetwork
   * @throws IOException - if something goes wrong
   */
  public Subnetwork getSubnetwork(String region, String subnetwork) throws IOException {
    return processGet(
        getOldComputeService().subnetworks().get(getGoogleProject(), region, subnetwork));
  }

  /**
   * Creates a subnetwork in the specified region using the data included in the request.
   *
   * @param region - Name of the region scoping this request
   * @param subnetwork - Subnetwork request
   * @return Subnetworks - If successful, this method returns a request to create the Subnetwork
   * @throws IOException - if something goes wrong
   */
  public Subnetwork insertSubnetwork(String region, Subnetwork subnetwork) throws IOException {
    processOperation(
        getOldComputeService().subnetworks().insert(getGoogleProject(), region, subnetwork));
    return this.getSubnetwork(region, subnetwork.getName());

  }

  /**
   * Delete the specified Subnetwork by region and name.
   *
   * @param region - Name of the region scoping this request
   * @param subnetwork - Name of the Subnetwork resource to delete
   * @throws IOException - if something goes wrong
   */
  public void deleteSubnetwork(String region, String subnetwork) throws IOException {
    this.processOperation(
        getOldComputeService().subnetworks().delete(getGoogleProject(), region, subnetwork));
  }


  /**
   * Create default network.
   *
   * @return Network interface
   */
  private NetworkInterface createDefaultNetwork() {
    // Add Network Interface to be used by VM Instance.
    AccessConfig config = AccessConfig.newBuilder().setName(GoogleConstants.NETWORK_ACCESS_CONFIG)
        .setType(com.google.cloud.compute.NetworkInterface.AccessConfig.Type.ONE_TO_ONE_NAT)
        .build();
    return NetworkInterface.newBuilder(NetworkId.of("default")).setAccessConfigurations(config)
        .build();
  }

  /**
   * Create default disk.
   *
   * @param zone name
   * @param name of instance
   * @param sourceImage image
   * @return AttachedDisk
   */
  private AttachedDisk createDefaultDisk(String zone, String name, ImageId sourceImage) {
    return AttachedDisk
        .newBuilder(AttachedDisk.CreateDiskConfiguration.newBuilder(sourceImage).setAutoDelete(true)
            .setDiskType(DiskTypeId.of(zone, "pd-standard")).setDiskName(name).build())
        .build();
  }

  /**
   * Create default service account.
   *
   * @return ServiceAccount
   */
  private ServiceAccount createDefaultServiceAccount() {
    List<String> scopes = new ArrayList<>();
    scopes.add("https://www.googleapis.com/auth/devstorage.full_control");
    scopes.add("https://www.googleapis.com/auth/compute");
    ServiceAccount account = ServiceAccount.of("default", scopes);
    return account;
  }

  /**
   * Create metadata with the ssh key.
   *
   * @param sshFile info
   * @param keyName key
   * @param tags deployment
   * @param cloudGroup id
   * @return Metadata
   */
  private Metadata createMetadata(Path sshFile, String keyName, Map<String, String> tags,
      String cloudGroup) {
    try {
      String string = new String(Files.readAllBytes(sshFile));
      String value = String.format("%s:%s\n", user, string);
      Builder newBuilder = Metadata.newBuilder();
      newBuilder.add(GoogleComputeIntegration.SSH_KEYS, value);
      newBuilder.add(GoogleComputeIntegration.KEY_NAME, keyName);
      if (MapUtils.isNotEmpty(tags)) {
        tags.forEach((mapKey, mapValue) -> {
          newBuilder.add(mapKey, mapValue);
        });
      }
      if (StringUtils.isNotBlank(cloudGroup)) {
        newBuilder.add(cloudos.models.Instance.CLOUDGROUP_ID_TAG, cloudGroup);
      }
      return newBuilder.build();
    } catch (Exception e) {
      // TODO: handle exception
      e.printStackTrace();
    }
    return null;
  }

  /**
   * Delete an instance.
   *
   * @param zone name
   * @param instanceName name
   * @return Operation
   */
  public Operation deleteInstance(String zone, String instanceName) {
    Operation deleteInstance =
        getComputeService().deleteInstance(InstanceId.of(zone, instanceName));
    return deleteInstance;
  }

  /**
   * Start an instance.
   *
   * @param zone name
   * @param instanceName name
   * @return Operation
   */
  public Operation startInstance(String zone, String instanceName) {
    return getComputeService().start(InstanceId.of(zone, instanceName));
  }

  /**
   * Stop an instance.
   *
   * @param zone name
   * @param instanceName name
   * @return Operation
   */
  public Operation stopInstance(String zone, String instanceName) {
    return getComputeService().stop(InstanceId.of(zone, instanceName));
  }

  /**
   * Reboot an instance.
   *
   * @param zone name
   * @param instanceName name
   * @return Operation
   */
  public Operation rebootInstance(String zone, String instanceName) {
    return getComputeService().reset(InstanceId.of(zone, instanceName));
  }

  /**
   * Waits for the operation is done.
   *
   * @param operation to be wait
   * @param timeout in milliseconds
   * @return list of Errors if there is errors
   * @throws InterruptedException if failed to wait
   * @throws TimeoutException if failed to wait
   */
  public List<OperationError> blockUntilComplete(Operation operation, long timeout)
      throws InterruptedException, TimeoutException {
    CheckingPeriod checkEvery = WaitForOption.checkEvery(1, TimeUnit.SECONDS);
    Timeout timeout2 = WaitForOption.timeout(timeout, TimeUnit.MILLISECONDS);
    operation.waitFor(checkEvery, timeout2);
    return operation.getErrors();
  }

  /**
   * Waits for the operation is done.
   *
   * @param operation object
   * @param timeout limit
   * @return List of OperationError
   * @throws Exception if something goes wrong
   */
  public com.google.api.services.compute.model.Operation.Error blockUntilComplete(
      com.google.api.services.compute.model.Operation operation, long timeout)
      throws InterruptedException {
    long start = System.currentTimeMillis();
    final long pollInterval = 5 * 1000;
    String status = operation.getStatus();
    // OperationId opId = operation.getOperationId();
    while (operation != null && !status.equals(Status.DONE.name())) {
      Thread.sleep(pollInterval);
      long elapsed = System.currentTimeMillis() - start;
      if (elapsed >= timeout) {
        throw new InterruptedException("Timed out waiting for operation to complete");
      }
      log.debug("waiting...");
      try {
        operation = getOldComputeService().globalOperations()
            .get(getGoogleProject(), operation.getName()).execute();
      } catch (IOException e) {
        operation = null;
      }
      if (operation != null) {
        status = operation.getStatus();
      }
    }
    return operation == null ? null : operation.getError();
  }

  /**
   * Auxiliary method to process the return of the Get request. Returns null when the object is not
   * found.
   *
   * @param get method request
   * @return T Object of the get request
   * @throws IOException if something goes wrong
   */
  public <T> T processGet(ComputeRequest<T> get) throws IOException {
    try {
      long startTime = System.currentTimeMillis();
      T execute = get.execute();
      log.info("Get {} executed in {} s.", get.getClass().getName(),
          (System.currentTimeMillis() - startTime) / 1000);
      return execute;
    } catch (GoogleJsonResponseException e) {
      if (e.getStatusCode() != 404) {
        throw e;
      }
      return null;
    }
  }

  /**
   * Process an operation, handling the response.
   *
   * @param request to be executed
   * @throws IOException if something goes wrong
   */
  public void processOperation(
      ComputeRequest<com.google.api.services.compute.model.Operation> request) throws IOException {
    long startTime = System.currentTimeMillis();
    com.google.api.services.compute.model.Operation processGet = request.execute();
    if (processGet != null) {
      try {
        blockUntilComplete(processGet, DEFAULT_TIMEOUT);
        log.info("Request {} executed in {} s.", request.getClass().getName(),
            (System.currentTimeMillis() - startTime) / 1000);
      } catch (InterruptedException e) {
        throw new IOException("Request timeouted!");
      }
    }
  }


  /**
   * Get all machine types for a project.
   *
   * @return Iterable of MachineTypes
   */
  public Iterable<MachineType> listMachineType() {
    Set<MachineType> list = new HashSet<>();
    Page<MachineType> listReturned = getComputeService().listMachineTypes();
    do {
      listReturned.getValues().forEach(t -> list.add(t));
      listReturned = listReturned.getNextPage();
    } while (listReturned != null);
    return list;
  }

  /**
   * Get machine type for a project on that zone.
   *
   * @param zone name
   * @param machineType type
   * @return MachineType
   */
  public MachineType retrieveMachineType(String zone, String machineType) {
    return getComputeService().getMachineType(MachineTypeId.of(zone, machineType));
  }


  /**
   * Validate the credential.
   *
   * @param cloudosCredential to be validate
   * @return true if valid
   */
  @Override
  public boolean validateCredential(GoogleCredential cloudosCredential) {
    try {
      createCompute(cloudosCredential).listRegions();
      createOldCompute(cloudosCredential);
      return true;
    } catch (Exception e) {
      log.error("Validating credential: {}", cloudosCredential.toString(), e);
      return false;
    }
  }

  /**
   * Check if instance was preempted.
   *
   * @param instance to check
   * @return true if was, false otherwise
   */
  public boolean checkIfWasPreempted(Instance instance) {

    OperationFilter filterInstance =
        OperationFilter.equals(OperationField.TARGET_LINK, instance.getInstanceId().getSelfLink());

    Page<Operation> listGlobalOperations = computeService.listZoneOperations(
        instance.getInstanceId().getZone(), OperationListOption.filter(filterInstance));

    Iterator<Operation> iterator = listGlobalOperations.getValues().iterator();
    if (iterator != null && iterator.hasNext()) {
      while (iterator.hasNext()) {
        Operation operation = iterator.next();
        // if returned and it is done
        if (operation.getOperationType().equals(COMPUTE_INSTANCES_PREEMPTED)
            && operation.isDone()) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Return the list of instance group.
   *
   * @return InstanceGroupList
   * @throws IOException if something goes wrong
   */
  public InstanceGroupList listInstanceGroup(String zone) throws IOException {
    return getOldComputeService().instanceGroups().list(super.getGoogleProject(), zone).execute();
  }

  /**
   * Describe an instance group.
   *
   * @param zone where the group is located
   * @param name group name
   * @return InstanceGroup
   * @throws IOException if something goes wrong
   */
  public InstanceGroup getInstanceGroup(String zone, String name) throws IOException {
    return this.processGet(
        this.getOldComputeService().instanceGroups().get(getGoogleProject(), zone, name));
  }

  /**
   * Create an instance group.
   *
   * @param zone where the group is located
   * @param name group name
   * @param description group description
   * @return InstanceGroup
   * @throws Exception if something goes wrong
   */
  public InstanceGroup createInstanceGroup(String zone, String name, String description)
      throws Exception {
    InstanceGroup content = new InstanceGroup();
    content.setDescription(description);
    content.setName(name);
    this.processOperation(
        this.getOldComputeService().instanceGroups().insert(getGoogleProject(), zone, content));
    return getInstanceGroup(zone, name);
  }

  /**
   * Delete an instance group.
   *
   * @param zone where the group is located
   * @param name group name
   * @throws Exception if something goes wrong
   */
  public void deleteInstanceGroup(String zone, String name) throws Exception {
    this.processOperation(
        this.getOldComputeService().instanceGroups().delete(getGoogleProject(), zone, name));
  }

  /**
   * Add Instances to an instance group.
   *
   * @param zone where the group is located
   * @param name group name
   * @param instanceIds instances id to be added to the group
   * @return InstanceGroup
   * @throws Exception if something goes wrong
   */
  public InstanceGroup addInstanceToInstanceGroup(String zone, String name,
      List<InstanceId> instanceIds) throws Exception {
    InstanceGroupsAddInstancesRequest request = new InstanceGroupsAddInstancesRequest();
    request.setInstances(
        instanceIds.stream().map(t -> new InstanceReference().setInstance(t.getSelfLink()))
            .collect(Collectors.toList()));
    this.processOperation(this.getOldComputeService().instanceGroups()
        .addInstances(getGoogleProject(), zone, name, request));
    return this.getInstanceGroup(zone, name);
  }

  /**
   * Remove instances from an instance group.
   *
   * @param zone where the group is located
   * @param name group name
   * @param instanceIds instances id to be removed to the group
   * @return InstanceGroup
   * @throws Exception if something goes wrong
   */
  public InstanceGroup removeInstanceToInstanceGroup(String zone, String name,
      List<InstanceId> instanceIds) throws Exception {
    InstanceGroupsRemoveInstancesRequest request = new InstanceGroupsRemoveInstancesRequest();
    request.setInstances(
        instanceIds.stream().map(t -> new InstanceReference().setInstance(t.getSelfLink()))
            .collect(Collectors.toList()));
    this.processOperation(this.getOldComputeService().instanceGroups()
        .removeInstances(getGoogleProject(), zone, name, request));
    return this.getInstanceGroup(zone, name);
  }

  /**
   * List all firewall rules.
   *
   * @return FirewallList
   * @throws IOException if something goes wrong
   */
  public FirewallList listFirewall() throws IOException {
    return this.getOldComputeService().firewalls().list(getGoogleProject()).execute();
  }

  /**
   * Describe a firewall rule.
   *
   * @param firewall name
   * @return Firewall
   * @throws IOException if something goes wrong
   */
  public Firewall getFirewall(String firewall) throws IOException {
    return this
        .processGet(this.getOldComputeService().firewalls().get(getGoogleProject(), firewall));
  }

  /**
   * Insert a firewall rule.
   *
   * @param firewall details
   * @return created Firewall
   * @throws Exception if something goes wrong
   */
  public Firewall createFirewall(Firewall firewall) throws Exception {
    this.processOperation(
        this.getOldComputeService().firewalls().insert(getGoogleProject(), firewall));
    return getFirewall(firewall.getName());
  }

  /**
   * Delete a firewall rule.
   *
   * @param firewall name
   * @throws Exception if something goes wrong
   */
  public void deleteFirewall(String firewall) throws Exception {
    this.processOperation(
        this.getOldComputeService().firewalls().delete(getGoogleProject(), firewall));
  }

  /**
   * List all backend services.
   *
   * @return BackendServiceList
   * @throws IOException if something goes wrong
   */
  public BackendServiceList listBackendService() throws IOException {
    return this.getOldComputeService().backendServices().list(getGoogleProject()).execute();
  }

  /**
   * Describe a backend service.
   *
   * @param backendService name
   * @return BackendService
   * @throws IOException if something goes wrong
   */
  public BackendService getBackendService(String backendService) throws IOException {
    return this.processGet(
        this.getOldComputeService().backendServices().get(getGoogleProject(), backendService));
  }

  /**
   * Create a backend service.
   *
   * @param backendService details.
   * @return created BackendService
   * @throws Exception if something goes wrong
   */
  public BackendService createBackendService(BackendService backendService) throws Exception {
    this.processOperation(
        this.getOldComputeService().backendServices().insert(getGoogleProject(), backendService));
    return this.getBackendService(backendService.getName());
  }

  /**
   * Delete a backend service.
   *
   * @param backendService name
   * @throws Exception if something goes wrong
   */
  public void deleteBackendService(String backendService) throws Exception {
    this.processOperation(
        this.getOldComputeService().backendServices().delete(getGoogleProject(), backendService));
  }

  /**
   * List all http health checks.
   *
   * @return HttpHealthCheckList
   * @throws IOException if something goes wrong
   */
  public HttpHealthCheckList listHttpHealthCheck() throws IOException {
    return this.getOldComputeService().httpHealthChecks().list(getGoogleProject()).execute();
  }

  /**
   * Describe a http health check.
   *
   * @param healthcheck name
   * @return HttpHealthCheck
   * @throws IOException if something goes wrong
   */
  public HttpHealthCheck getHttpHealthcheck(String healthcheck) throws IOException {
    return this.processGet(
        this.getOldComputeService().httpHealthChecks().get(getGoogleProject(), healthcheck));
  }

  /**
   * Create a http health check.
   *
   * @param httpHealthCheck details
   * @return HttpHealthCheck
   * @throws Exception if something goes wrong
   */
  public HttpHealthCheck createHttpHealthCheck(HttpHealthCheck httpHealthCheck) throws Exception {
    this.processOperation(
        this.getOldComputeService().httpHealthChecks().insert(getGoogleProject(), httpHealthCheck));
    return this.getHttpHealthcheck(httpHealthCheck.getName());
  }

  /**
   * Delete a http health check.
   *
   * @param httpHealthCheck name
   * @throws Exception if something goes wrong
   */
  public void deleteHttpHealthCheck(String httpHealthCheck) throws Exception {
    this.processOperation(
        this.getOldComputeService().httpHealthChecks().delete(getGoogleProject(), httpHealthCheck));
  }

  /**
   * List all https health checks.
   *
   * @return HttpsHealthCheckList
   * @throws IOException if something goes wrong
   */
  public HttpsHealthCheckList listHttpsHealthCheck() throws IOException {
    return this.getOldComputeService().httpsHealthChecks().list(getGoogleProject()).execute();
  }

  /**
   * Describe a https health check.
   *
   * @param healthcheck name
   * @return HttpsHealthCheck
   * @throws IOException if something goes wrong
   */
  public HttpsHealthCheck getHttpsHealthcheck(String healthcheck) throws IOException {
    return this.processGet(
        this.getOldComputeService().httpsHealthChecks().get(getGoogleProject(), healthcheck));
  }

  /**
   * Create a https health check.
   *
   * @param httpsHealthCheck details
   * @return HttpsHealthCheck
   * @throws Exception if something goes wrong
   */
  public HttpsHealthCheck createHttpsHealthCheck(HttpsHealthCheck httpsHealthCheck)
      throws Exception {
    this.processOperation(this.getOldComputeService().httpsHealthChecks().insert(getGoogleProject(),
        httpsHealthCheck));
    return this.getHttpsHealthcheck(httpsHealthCheck.getName());
  }

  /**
   * Delete a https health check.
   *
   * @param httpsHealthCheck name
   * @throws Exception if something goes wrong
   */
  public void deleteHttpsHealthCheck(String httpsHealthCheck) throws Exception {
    this.processOperation(this.getOldComputeService().httpsHealthChecks().delete(getGoogleProject(),
        httpsHealthCheck));
  }

  /**
   * List all ip address.
   *
   * @param region of the address
   * @return AddressList
   * @throws IOException if something goes wrong
   */
  public AddressList listAddress(String region) throws IOException {
    return this.getOldComputeService().addresses().list(getGoogleProject(), region).execute();
  }

  /**
   * Describe a ip address.
   *
   * @param region of the address
   * @param address name
   * @return Address
   * @throws IOException if something goes wrong
   */
  public Address getAddress(String region, String address) throws IOException {
    return this.processGet(
        this.getOldComputeService().addresses().get(getGoogleProject(), region, address));
  }

  /**
   * Create an address.
   *
   * @param address details
   * @return Address
   * @throws Exception if something goes wrong
   */
  public Address createAddress(Address address) throws Exception {
    this.processOperation(this.getOldComputeService().addresses().insert(getGoogleProject(),
        address.getRegion(), address));
    return this.getAddress(address.getRegion(), address.getName());
  }

  /**
   * Delete an address.
   *
   * @param region of the address
   * @param address name
   * @throws Exception if something goes wrong
   */
  public void deleteAddress(String region, String address) throws Exception {
    this.processOperation(
        this.getOldComputeService().addresses().delete(getGoogleProject(), region, address));
  }

  /**
   * List all ip targetPool.
   *
   * @param region of the targetPool
   * @return TargetPoolList
   * @throws IOException if something goes wrong
   */
  public TargetPoolList listTargetPool(String region) throws IOException {
    return this.getOldComputeService().targetPools().list(getGoogleProject(), region).execute();
  }

  /**
   * Describe a ip targetPool.
   *
   * @param region of the targetPool
   * @param targetPool name
   * @return TargetPool
   * @throws IOException if something goes wrong
   */
  public TargetPool getTargetPool(String region, String targetPool) throws IOException {
    return this.processGet(
        this.getOldComputeService().targetPools().get(getGoogleProject(), region, targetPool));
  }

  /**
   * Create an targetPool.
   *
   * @param targetPool details
   * @return TargetPool
   * @throws Exception if something goes wrong
   */
  public TargetPool createTargetPool(TargetPool targetPool) throws Exception {
    this.processOperation(this.getOldComputeService().targetPools().insert(getGoogleProject(),
        targetPool.getRegion(), targetPool));
    return this.getTargetPool(targetPool.getRegion(), targetPool.getName());
  }

  /**
   * Delete an targetPool.
   *
   * @param region of the targetPool
   * @param targetPool name
   * @throws Exception if something goes wrong
   */
  public void deleteTargetPool(String region, String targetPool) throws Exception {
    this.processOperation(
        this.getOldComputeService().targetPools().delete(getGoogleProject(), region, targetPool));
  }

  /**
   * List all ip forwardingRule.
   *
   * @param region of the forwardingRule
   * @return ForwardingRuleList
   * @throws IOException if something goes wrong
   */
  public ForwardingRuleList listForwardingRule(String region) throws IOException {
    return this.getOldComputeService().forwardingRules().list(getGoogleProject(), region).execute();
  }

  /**
   * Describe a ip forwardingRule.
   *
   * @param region of the forwardingRule
   * @param forwardingRule name
   * @return ForwardingRule
   * @throws IOException if something goes wrong
   */
  public ForwardingRule getForwardingRule(String region, String forwardingRule) throws IOException {
    return this.processGet(this.getOldComputeService().forwardingRules().get(getGoogleProject(),
        region, forwardingRule));
  }

  /**
   * Create an forwardingRule.
   *
   * @param forwardingRule details
   * @return ForwardingRule
   * @throws Exception if something goes wrong
   */
  public ForwardingRule createForwardingRule(ForwardingRule forwardingRule) throws Exception {
    this.processOperation(this.getOldComputeService().forwardingRules().insert(getGoogleProject(),
        forwardingRule.getRegion(), forwardingRule));
    return this.getForwardingRule(forwardingRule.getRegion(), forwardingRule.getName());
  }

  /**
   * Delete an forwardingRule.
   *
   * @param region of the forwardingRule
   * @param forwardingRule name
   * @throws Exception if something goes wrong
   */
  public void deleteForwardingRule(String region, String forwardingRule) throws Exception {
    this.processOperation(this.getOldComputeService().forwardingRules().delete(getGoogleProject(),
        region, forwardingRule));
  }

  /**
   * Create a disk snapshot.
   *
   * @param snapshotName name of the snapshot (if null it will use the same disk name + SS)
   * @param attachedDisk original disk
   * @param timeout in milliseconds
   * @return SnapshotId
   * @throws InterruptedException if operation fails
   * @throws TimeoutException if operation fails
   */
  public Snapshot createDiskSnapshot(String snapshotName, AttachedDisk attachedDisk, long timeout)
      throws InterruptedException, TimeoutException {
    // create snapshot of that disk
    DiskId sourceDisk =
        ((com.google.cloud.compute.AttachedDisk.PersistentDiskConfiguration) attachedDisk
            .getConfiguration()).getSourceDisk();

    if (StringUtils.isBlank(snapshotName)) {
      // if name not informed
      snapshotName = sourceDisk.getDisk() + "-ss";
    }
    SnapshotId snapshotId = SnapshotId.of(snapshotName);
    SnapshotInfo snapshotInfo = SnapshotInfo
        .newBuilder(snapshotId, DiskId.of(sourceDisk.getZone(), sourceDisk.getDisk())).build();
    Operation createSnapshot = getComputeService().create(snapshotInfo);
    if (CollectionUtils.isNotEmpty(blockUntilComplete(createSnapshot, timeout))) {
      throw new RuntimeException("Error creating snapshot");
    }
    Snapshot snapshot2 = null;
    do {
      snapshot2 = getComputeService().getSnapshot(snapshotId.getSnapshot());
      log.info("waiting snapshot {} actual status: {}", snapshot2.getSnapshotId().getSnapshot(),
          snapshot2.getStatus());
    } while (snapshot2 != null
        && snapshot2.getStatus() != com.google.cloud.compute.SnapshotInfo.Status.READY
        && snapshot2.getStatus() != com.google.cloud.compute.SnapshotInfo.Status.FAILED);

    return snapshot2;
  }

  /**
   * Clone an instance.
   *
   * @param instanceClonedName - name of the cloned instance
   * @param instance - original instance
   * @param spot - set as spot or not
   * @param machineType - machineType of the cloned instance (if null it will be the same of the
   *        original instance)
   * @param timeout - timeout for the operations in milliseconds
   * @return Instance - new cloned instance
   * @throws InterruptedException if operation fails
   * @throws TimeoutException if operation fails
   */
  public Instance cloneInstance(String instanceClonedName, Instance instance, boolean spot,
      String machineType, long timeout) throws InterruptedException, TimeoutException {

    List<AttachedDisk> attachedDisks = instance.getAttachedDisks();
    AttachedDisk attachedDisk = null;
    // get the persistent disk
    for (AttachedDisk disk : attachedDisks) {
      log.info("DISK: {}", disk);
      if (disk.getConfiguration().getType() == AttachedDiskConfiguration.Type.PERSISTENT) {
        attachedDisk = disk;
      }
    }

    // create the snapshot
    log.info("creating snapshot");
    Snapshot snapshot = this.createDiskSnapshot(null, attachedDisk, timeout);
    log.info("waiting");
    Thread.sleep(LONG_TIMEOUT);

    // create disk using snapshot
    DiskId diskId =
        DiskId.of(instance.getInstanceId().getZone(), snapshot.getSnapshotId().getSnapshot());
    SnapshotDiskConfiguration diskConfiguration =
        SnapshotDiskConfiguration.of(snapshot.getSnapshotId());
    DiskInfo disk = DiskInfo.of(diskId, diskConfiguration);
    log.info("creating disk");
    Operation createDisk = getComputeService().create(disk);
    if (CollectionUtils.isNotEmpty(blockUntilComplete(createDisk, timeout))) {
      throw new RuntimeException("Error creating new disk");
    }

    log.info("waiting");
    Thread.sleep(LONG_TIMEOUT);

    // create the attachedDisk to set to the new instance
    PersistentDiskConfiguration attachConfiguration =
        PersistentDiskConfiguration.newBuilder(diskId).setBoot(true).setAutoDelete(true).build();
    AttachedDisk newAttachedDisk = AttachedDisk.of(ATTACHED_DISK_DEV0, attachConfiguration);

    // copy the tags over
    Tags googleTags = instance.getTags();
    List<String> tags = googleTags.getValues();
    for (String tag : tags) {
      log.info("TAG: {}", tag);
    }
    // copy the metadata over
    Metadata metadata = instance.getMetadata();
    Map<String, String> values = metadata.getValues();
    values.forEach((k, v) -> {
      log.info("METADATA: {} {}", k, v);
    });

    // copy the service accounts
    List<ServiceAccount> serviceAccounts = instance.getServiceAccounts();

    // create a new network
    // TODO: revisit it to copy the network config
    // List<NetworkInterface> networkInterfaces = instance.getNetworkInterfaces();
    List<NetworkInterface> networkInterfaces = Arrays.asList(this.createDefaultNetwork());

    // get the machine type
    if (StringUtils.isBlank(machineType)) {
      // if name not informed
      machineType = instance.getMachineType().getType();
    }

    // create the new instance
    log.info("creating instance");
    Operation createInstance = createInstance(instance.getInstanceId().getZone(),
        instanceClonedName, machineType, spot, Arrays.asList(newAttachedDisk), networkInterfaces,
        serviceAccounts, googleTags, metadata);
    if (CollectionUtils.isNotEmpty(blockUntilComplete(createInstance, timeout))) {
      throw new RuntimeException("Error cloning");
    }

    log.info("waiting");
    Thread.sleep(LONG_TIMEOUT);

    // delete snapshot
    getComputeService().deleteSnapshot(snapshot.getSnapshotId());
    // do it async - so, no need to blockUntilComplete

    return getInstance(instance.getInstanceId().getZone(), instanceClonedName);
  }


}
