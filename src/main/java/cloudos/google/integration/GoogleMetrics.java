package cloudos.google.integration;

public class GoogleMetrics {

  // API request count
  public static final String AGENT_GOOGLEAPIS_COM_AGENT_API_REQUEST_COUNT =
      "agent.googleapis.com/agent/api_request_count";
  // Agent memory in use
  public static final String AGENT_GOOGLEAPIS_COM_AGENT_MEMORY_USAGE =
      "agent.googleapis.com/agent/memory_usage";
  // Estimate of stream space used
  public static final String AGENT_GOOGLEAPIS_COM_AGENT_STREAMSPACE_SIZE =
      "agent.googleapis.com/agent/streamspace_size";
  // Whether the agent is in the throttling state
  public static final String AGENT_GOOGLEAPIS_COM_AGENT_STREAMSPACE_SIZE_THROTTLING =
      "agent.googleapis.com/agent/streamspace_size_throttling";
  // Agent uptime in seconds
  public static final String AGENT_GOOGLEAPIS_COM_AGENT_UPTIME =
      "agent.googleapis.com/agent/uptime";
  // The number of active connections currently attached to the HTTP server.
  public static final String AGENT_GOOGLEAPIS_COM_APACHE_CONNECTIONS =
      "agent.googleapis.com/apache/connections";
  // The number of idle workers currently attached to the HTTP server.
  public static final String AGENT_GOOGLEAPIS_COM_APACHE_IDLE_WORKERS =
      "agent.googleapis.com/apache/idle_workers";
  // Total requests serviced by the HTTP server.
  public static final String AGENT_GOOGLEAPIS_COM_APACHE_REQUEST_COUNT =
      "agent.googleapis.com/apache/request_count";
  // Apache HTTP server scoreboard values.
  public static final String AGENT_GOOGLEAPIS_COM_APACHE_SCOREBOARD =
      "agent.googleapis.com/apache/scoreboard";
  // Total HTTP server traffic.
  public static final String AGENT_GOOGLEAPIS_COM_APACHE_TRAFFIC =
      "agent.googleapis.com/apache/traffic";
  // Current data size of all commit log segments.
  public static final String AGENT_GOOGLEAPIS_COM_CASSANDRA_COMMITLOG_TOTAL_SIZE =
      "agent.googleapis.com/cassandra/commitlog_total_size";
  // Number of completed tasks in queue.
  public static final String AGENT_GOOGLEAPIS_COM_CASSANDRA_COMPLETED_TASKS =
      "agent.googleapis.com/cassandra/completed_tasks";
  // Number of tasks in queue with the given task status.
  public static final String AGENT_GOOGLEAPIS_COM_CASSANDRA_CURRENT_TASKS =
      "agent.googleapis.com/cassandra/current_tasks";
  // The number of exceptions thrown by the storage operations.
  public static final String AGENT_GOOGLEAPIS_COM_CASSANDRA_STORAGE_SERVICE_EXCEPTION_COUNT =
      "agent.googleapis.com/cassandra/storage_service_exception_count";
  // Total disk space used for this node.
  public static final String AGENT_GOOGLEAPIS_COM_CASSANDRA_STORAGE_SERVICE_LOAD =
      "agent.googleapis.com/cassandra/storage_service_load";
  // Average request time.
  public static final String AGENT_GOOGLEAPIS_COM_COUCHDB_AVERAGE_REQUEST_TIME =
      "agent.googleapis.com/couchdb/average_request_time";
  // Bulk requests count.
  public static final String AGENT_GOOGLEAPIS_COM_COUCHDB_HTTPD_BULK_REQUEST_COUNT =
      "agent.googleapis.com/couchdb/httpd/bulk_request_count";
  // Requests count.
  public static final String AGENT_GOOGLEAPIS_COM_COUCHDB_HTTPD_REQUEST_COUNT =
      "agent.googleapis.com/couchdb/httpd/request_count";
  // HTTP request method count.
  public static final String AGENT_GOOGLEAPIS_COM_COUCHDB_HTTPD_REQUEST_METHOD_COUNT =
      "agent.googleapis.com/couchdb/httpd/request_method_count";
  // HTTP response status code count.
  public static final String AGENT_GOOGLEAPIS_COM_COUCHDB_HTTPD_RESPONSE_CODE_COUNT =
      "agent.googleapis.com/couchdb/httpd/response_code_count";
  // Temporary view reads count.
  public static final String AGENT_GOOGLEAPIS_COM_COUCHDB_HTTPD_TEMPORARY_VIEW_READ_COUNT =
      "agent.googleapis.com/couchdb/httpd/temporary_view_read_count";
  // View reads count.
  public static final String AGENT_GOOGLEAPIS_COM_COUCHDB_HTTPD_VIEW_READ_COUNT =
      "agent.googleapis.com/couchdb/httpd/view_read_count";
  // Number of open databases.
  public static final String AGENT_GOOGLEAPIS_COM_COUCHDB_OPEN_DATABASES =
      "agent.googleapis.com/couchdb/open_databases";
  // Number of open files.
  public static final String AGENT_GOOGLEAPIS_COM_COUCHDB_OPEN_FILES =
      "agent.googleapis.com/couchdb/open_files";
  // Database read count.
  public static final String AGENT_GOOGLEAPIS_COM_COUCHDB_READ_COUNT =
      "agent.googleapis.com/couchdb/read_count";
  // Database write count.
  public static final String AGENT_GOOGLEAPIS_COM_COUCHDB_WRITE_COUNT =
      "agent.googleapis.com/couchdb/write_count";
  // CPU Load (15 minute intervals)
  public static final String AGENT_GOOGLEAPIS_COM_CPU_LOAD_15M =
      "agent.googleapis.com/cpu/load_15m";
  // CPU Load (1 minute intervals)
  public static final String AGENT_GOOGLEAPIS_COM_CPU_LOAD_1M = "agent.googleapis.com/cpu/load_1m";
  // CPU Load (5 minute intervals)
  public static final String AGENT_GOOGLEAPIS_COM_CPU_LOAD_5M = "agent.googleapis.com/cpu/load_5m";
  // CPU Usage
  public static final String AGENT_GOOGLEAPIS_COM_CPU_USAGE_TIME =
      "agent.googleapis.com/cpu/usage_time";
  // CPU Usage (percent)
  public static final String AGENT_GOOGLEAPIS_COM_CPU_UTILIZATION =
      "agent.googleapis.com/cpu/utilization";
  // Disk bytes used
  public static final String AGENT_GOOGLEAPIS_COM_DISK_BYTES_USED =
      "agent.googleapis.com/disk/bytes_used";
  // Average time an I/O-operation took to complete
  public static final String AGENT_GOOGLEAPIS_COM_DISK_IO_TIME =
      "agent.googleapis.com/disk/io_time";
  // Merged Operations Count
  public static final String AGENT_GOOGLEAPIS_COM_DISK_MERGED_OPERATIONS =
      "agent.googleapis.com/disk/merged_operations";
  // Disk Operations Count
  public static final String AGENT_GOOGLEAPIS_COM_DISK_OPERATION_COUNT =
      "agent.googleapis.com/disk/operation_count";
  // Time Spent In Disk Operations
  public static final String AGENT_GOOGLEAPIS_COM_DISK_OPERATION_TIME =
      "agent.googleapis.com/disk/operation_time";
  // Pending Operations Count
  public static final String AGENT_GOOGLEAPIS_COM_DISK_PENDING_OPERATIONS =
      "agent.googleapis.com/disk/pending_operations";
  // Disk percent used
  public static final String AGENT_GOOGLEAPIS_COM_DISK_PERCENT_USED =
      "agent.googleapis.com/disk/percent_used";
  // Disk Bytes Read
  public static final String AGENT_GOOGLEAPIS_COM_DISK_READ_BYTES_COUNT =
      "agent.googleapis.com/disk/read_bytes_count";
  // Weighted I/O time
  public static final String AGENT_GOOGLEAPIS_COM_DISK_WEIGHTED_IO_TIME =
      "agent.googleapis.com/disk/weighted_io_time";
  // Disk Bytes Transferred
  public static final String AGENT_GOOGLEAPIS_COM_DISK_WRITE_BYTES_COUNT =
      "agent.googleapis.com/disk/write_bytes_count";
  // Size in bytes of the caches.
  public static final String AGENT_GOOGLEAPIS_COM_ELASTICSEARCH_CACHE_MEMORY_USAGE =
      "agent.googleapis.com/elasticsearch/cache_memory_usage";
  // Evictions from field cache
  public static final String AGENT_GOOGLEAPIS_COM_ELASTICSEARCH_FIELD_EVICTION_COUNT =
      "agent.googleapis.com/elasticsearch/field_eviction_count";
  // Evictions from filter cache
  public static final String AGENT_GOOGLEAPIS_COM_ELASTICSEARCH_FILTER_CACHE_EVICTION_COUNT =
      "agent.googleapis.com/elasticsearch/filter_cache_eviction_count";
  // Garbage collection count
  public static final String AGENT_GOOGLEAPIS_COM_ELASTICSEARCH_GC_COLLECTION_COUNT =
      "agent.googleapis.com/elasticsearch/gc_collection_count";
  // Size in bytes of memory.
  public static final String AGENT_GOOGLEAPIS_COM_ELASTICSEARCH_MEMORY_USAGE =
      "agent.googleapis.com/elasticsearch/memory_usage";
  // Number of bytes transmitted and received on the network
  public static final String AGENT_GOOGLEAPIS_COM_ELASTICSEARCH_NETWORK =
      "agent.googleapis.com/elasticsearch/network";
  // Number of documents in the indexes on this node
  public static final String AGENT_GOOGLEAPIS_COM_ELASTICSEARCH_NUM_CURRENT_DOCUMENTS =
      "agent.googleapis.com/elasticsearch/num_current_documents";
  // Number of data nodes in the cluster
  public static final String AGENT_GOOGLEAPIS_COM_ELASTICSEARCH_NUM_DATA_NODES =
      "agent.googleapis.com/elasticsearch/num_data_nodes";
  // Number of open HTTP connections to this node
  public static final String AGENT_GOOGLEAPIS_COM_ELASTICSEARCH_NUM_HTTP_CONNECTIONS =
      "agent.googleapis.com/elasticsearch/num_http_connections";
  // Number of nodes in the cluster
  public static final String AGENT_GOOGLEAPIS_COM_ELASTICSEARCH_NUM_NODES =
      "agent.googleapis.com/elasticsearch/num_nodes";
  // Number of open file descriptors held by the server process
  public static final String AGENT_GOOGLEAPIS_COM_ELASTICSEARCH_NUM_OPEN_FILES =
      "agent.googleapis.com/elasticsearch/num_open_files";
  // Number of open network connections to the server
  public static final String AGENT_GOOGLEAPIS_COM_ELASTICSEARCH_NUM_SERVER_CONNECTIONS =
      "agent.googleapis.com/elasticsearch/num_server_connections";
  // Number of shards.
  public static final String AGENT_GOOGLEAPIS_COM_ELASTICSEARCH_NUM_SHARDS =
      "agent.googleapis.com/elasticsearch/num_shards";
  // Number of operations completed
  public static final String AGENT_GOOGLEAPIS_COM_ELASTICSEARCH_OPERATION_COUNT =
      "agent.googleapis.com/elasticsearch/operation_count";
  // Time per second in ms spent on operations
  public static final String AGENT_GOOGLEAPIS_COM_ELASTICSEARCH_OPERATION_TIME =
      "agent.googleapis.com/elasticsearch/operation_time";
  // Maximum number of open threads that have been open concurrently in the server JVM process
  public static final String AGENT_GOOGLEAPIS_COM_ELASTICSEARCH_PEAK_THREADS =
      "agent.googleapis.com/elasticsearch/peak_threads";
  // Size in bytes of the document storage on this node
  public static final String AGENT_GOOGLEAPIS_COM_ELASTICSEARCH_STORAGE_SIZE =
      "agent.googleapis.com/elasticsearch/storage_size";
  // Number of open threads in the server JVM process
  public static final String AGENT_GOOGLEAPIS_COM_ELASTICSEARCH_THREADS =
      "agent.googleapis.com/elasticsearch/threads";
  // The number of open connections.
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_IPC_CONNECTIONS =
      "agent.googleapis.com/hbase/ipc/connections";
  // The current size of the IPC queue.
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_IPC_QUEUE_LENGTH =
      "agent.googleapis.com/hbase/ipc/queue_length";
  // The number of bytes transmitted and received via IPC.
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_IPC_TRAFFIC_COUNT =
      "agent.googleapis.com/hbase/ipc/traffic_count";
  // The average master load.
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_MASTER_AVERAGE_LOAD =
      "agent.googleapis.com/hbase/master/average_load";
  // The number of dead region servers.
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_MASTER_DEAD_REGION_SERVERS =
      "agent.googleapis.com/hbase/master/dead_region_servers";
  // The number of live region servers.
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_MASTER_LIVE_REGION_SERVERS =
      "agent.googleapis.com/hbase/master/live_region_servers";
  // The number of blocks of StoreFiles (HFiles) requested from the cache.
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_REGIONSERVER_BLOCK_CACHE_ACCESS_COUNT =
      "agent.googleapis.com/hbase/regionserver/block_cache/access_count";
  // The number of blocks that had to be evicted from the block cache due to heap size
  // constraints.
  public static final String
      AGENT_GOOGLEAPIS_COM_HBASE_REGIONSERVER_BLOCK_CACHE_EVICTED_BLOCKS_COUNT =
          "agent.googleapis.com/hbase/regionserver/block_cache/evicted_blocks_count";
  // The running block cache hit ratio (0 to 100).
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_REGIONSERVER_BLOCK_CACHE_HIT_RATIO_PERCENT =
      "agent.googleapis.com/hbase/regionserver/block_cache/hit_ratio_percent";
  // Memory usage by the block cache.
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_REGIONSERVER_BLOCK_CACHE_MEMORY =
      "agent.googleapis.com/hbase/regionserver/block_cache/memory";
  // The number of blocks of StoreFiles (HFiles) in the cache.
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_REGIONSERVER_BLOCK_CACHE_NUM_ITEMS =
      "agent.googleapis.com/hbase/regionserver/block_cache/num_items";
  // The length of the call queue.
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_REGIONSERVER_CALL_QUEUE_LENGTH =
      "agent.googleapis.com/hbase/regionserver/call_queue/length";
  // The length of the compaction queue (the number of stores that have been targeted for
  // compaction).
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_REGIONSERVER_COMPACTION_QUEUE_LENGTH =
      "agent.googleapis.com/hbase/regionserver/compaction_queue/length";
  // The number of enqueued regions in the MemStore awaiting flush.
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_REGIONSERVER_FLUSH_QUEUE_LENGTH =
      "agent.googleapis.com/hbase/regionserver/flush_queue/length";
  // Heap space used by the regionserver.
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_REGIONSERVER_MEMORY_HEAP_USAGE =
      "agent.googleapis.com/hbase/regionserver/memory/heap_usage";
  // The number of store files on the regionserver.
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_REGIONSERVER_MEMSTORE_FILES =
      "agent.googleapis.com/hbase/regionserver/memstore/files";
  // The size of the store file index on the regionserver.
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_REGIONSERVER_MEMSTORE_INDEX_SIZE =
      "agent.googleapis.com/hbase/regionserver/memstore/index_size";
  // The number of stores open on the regionserver.
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_REGIONSERVER_MEMSTORE_OPEN_STORES =
      "agent.googleapis.com/hbase/regionserver/memstore/open_stores";
  // The sum of all the memstore sizes in the regionserver.
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_REGIONSERVER_MEMSTORE_SIZE =
      "agent.googleapis.com/hbase/regionserver/memstore/size";
  // The number of active regions in the regionserver.
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_REGIONSERVER_ONLINE_REGIONS =
      "agent.googleapis.com/hbase/regionserver/online_regions";
  // The point-in-time number of RegionServer RPC calls.
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_REGIONSERVER_REQUESTS_TOTAL_RATE =
      "agent.googleapis.com/hbase/regionserver/requests/total_rate";
  // The number of requests.
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_REGIONSERVER_REQUEST_COUNT =
      "agent.googleapis.com/hbase/regionserver/request_count";
  // The number of slow log operations.
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_REGIONSERVER_SLOW_OPERATION_COUNT =
      "agent.googleapis.com/hbase/regionserver/slow_operation_count";
  // Thrift latency for batch operations.
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_THRIFT_BATCH_LATENCY_AVERAGE =
      "agent.googleapis.com/hbase/thrift/batch_latency/average";
  // Thrift latency for all calls.
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_THRIFT_CALL_LATENCY_AVERAGE =
      "agent.googleapis.com/hbase/thrift/call_latency/average";
  // Thrift call queue length.
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_THRIFT_CALL_QUEUE_LENGTH =
      "agent.googleapis.com/hbase/thrift/call_queue/length";
  // Thrift latency for slow calls.
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_THRIFT_SLOW_CALL_LATENCY_AVERAGE =
      "agent.googleapis.com/hbase/thrift/slow_call_latency/average";
  // Thrift time spent in queue.
  public static final String AGENT_GOOGLEAPIS_COM_HBASE_THRIFT_TIME_IN_QUEUE_AVERAGE =
      "agent.googleapis.com/hbase/thrift/time_in_queue/average";
  // Network error counts.
  public static final String AGENT_GOOGLEAPIS_COM_INTERFACE_ERRORS =
      "agent.googleapis.com/interface/errors";
  // Packets sent over network.
  public static final String AGENT_GOOGLEAPIS_COM_INTERFACE_PACKETS =
      "agent.googleapis.com/interface/packets";
  // Traffic of bytes sent over network.
  public static final String AGENT_GOOGLEAPIS_COM_INTERFACE_TRAFFIC =
      "agent.googleapis.com/interface/traffic";
  // The total number of collections that have occurred.
  public static final String AGENT_GOOGLEAPIS_COM_JVM_GC_COUNT =
      "agent.googleapis.com/jvm/gc/count";
  // The accumulated collection elapsed time in milliseconds.
  public static final String AGENT_GOOGLEAPIS_COM_JVM_GC_TIME = "agent.googleapis.com/jvm/gc/time";
  // The memory usage.
  public static final String AGENT_GOOGLEAPIS_COM_JVM_MEMORY_USAGE =
      "agent.googleapis.com/jvm/memory/usage";
  // The CPU time used by the process on which the Java virtual machine is running.
  public static final String AGENT_GOOGLEAPIS_COM_JVM_OS_CPU_TIME =
      "agent.googleapis.com/jvm/os/cpu_time";
  // The number of open file descriptors.
  public static final String AGENT_GOOGLEAPIS_COM_JVM_OS_OPEN_FILES =
      "agent.googleapis.com/jvm/os/open_files";
  // The current number of live daemon threads.
  public static final String AGENT_GOOGLEAPIS_COM_JVM_THREAD_NUM_DAEMON =
      "agent.googleapis.com/jvm/thread/num_daemon";
  // The current number of live threads.
  public static final String AGENT_GOOGLEAPIS_COM_JVM_THREAD_NUM_LIVE =
      "agent.googleapis.com/jvm/thread/num_live";
  // The peak number of live threads.
  public static final String AGENT_GOOGLEAPIS_COM_JVM_THREAD_PEAK =
      "agent.googleapis.com/jvm/thread/peak";
  // The uptime of the Java virtual machine.
  public static final String AGENT_GOOGLEAPIS_COM_JVM_UPTIME = "agent.googleapis.com/jvm/uptime";
  // The number of failed requests.
  public static final String AGENT_GOOGLEAPIS_COM_KAFKA_BROKER_TOPICS_FAILED_REQUEST_COUNT =
      "agent.googleapis.com/kafka/broker/topics/failed_request_count";
  // The number of incoming messages in all topics.
  public static final String AGENT_GOOGLEAPIS_COM_KAFKA_BROKER_TOPICS_INCOMING_MESSAGE_COUNT =
      "agent.googleapis.com/kafka/broker/topics/incoming_message_count";
  // The number of bytes sent and received in all topics.
  public static final String AGENT_GOOGLEAPIS_COM_KAFKA_BROKER_TOPICS_TRAFFIC =
      "agent.googleapis.com/kafka/broker/topics/traffic";
  // The number of active controllers in the cluster.
  public static final String AGENT_GOOGLEAPIS_COM_KAFKA_CONTROLLER_KAFKA_ACTIVE =
      "agent.googleapis.com/kafka/controller/kafka/active";
  // The number of partitions that don’t have an active leader and are hence not writable or
  // readable.
  public static final String AGENT_GOOGLEAPIS_COM_KAFKA_CONTROLLER_KAFKA_OFFLINE_PARTITIONS =
      "agent.googleapis.com/kafka/controller/kafka/offline_partitions";
  // The total number of leader elections.
  public static final String AGENT_GOOGLEAPIS_COM_KAFKA_CONTROLLER_LEADER_ELECTIONS_ELECTION_COUNT =
      "agent.googleapis.com/kafka/controller/leader_elections/election_count";
  // The total number of leader elections where the leader is out-of-sync.
  public static final String AGENT_GOOGLEAPIS_COM_KAFKA_CONTROLLER_LEADER_ELECTIONS_UNCLEAN_COUNT =
      "agent.googleapis.com/kafka/controller/leader_elections/unclean_count";
  // The number of log flushes.
  public static final String AGENT_GOOGLEAPIS_COM_KAFKA_LOG_FLUSH_COUNT =
      "agent.googleapis.com/kafka/log/flush_count";
  // The number of requests.
  public static final String AGENT_GOOGLEAPIS_COM_KAFKA_NETWORK_REQUEST_COUNT =
      "agent.googleapis.com/kafka/network/request_count";
  // The current number of delayed requests in purgatory.
  public static final String AGENT_GOOGLEAPIS_COM_KAFKA_PURGATORY_NUM_DELAYED_REQUESTS =
      "agent.googleapis.com/kafka/purgatory/num_delayed_requests";
  // The current number of requests in purgatory.
  public static final String AGENT_GOOGLEAPIS_COM_KAFKA_PURGATORY_SIZE =
      "agent.googleapis.com/kafka/purgatory/size";
  // The maximum lag in messages between the follower and leader replicas.
  public static final String AGENT_GOOGLEAPIS_COM_KAFKA_REPLICA_FETCHER_MAX_LAG =
      "agent.googleapis.com/kafka/replica_fetcher/max_lag";
  // The minimum rate at which the follower replicas send fetch requests to the leaders.
  public static final String AGENT_GOOGLEAPIS_COM_KAFKA_REPLICA_FETCHER_MIN_FETCH_RATE =
      "agent.googleapis.com/kafka/replica_fetcher/min_fetch_rate";
  // The number of replicas catching up (ISR = in-sync replica).
  public static final String AGENT_GOOGLEAPIS_COM_KAFKA_REPLICA_MANAGER_ISR_EXPAND_COUNT =
      "agent.googleapis.com/kafka/replica_manager/isr/expand_count";
  // The number of replicas lagging behind (ISR = in-sync replica).
  public static final String AGENT_GOOGLEAPIS_COM_KAFKA_REPLICA_MANAGER_ISR_SHRINK_COUNT =
      "agent.googleapis.com/kafka/replica_manager/isr/shrink_count";
  // The current number of leaders on this broker.
  public static final String AGENT_GOOGLEAPIS_COM_KAFKA_REPLICA_MANAGER_LEADERS =
      "agent.googleapis.com/kafka/replica_manager/leaders";
  // The current number of partitions on this broker.
  public static final String AGENT_GOOGLEAPIS_COM_KAFKA_REPLICA_MANAGER_PARTITIONS =
      "agent.googleapis.com/kafka/replica_manager/partitions";
  // The current number of under-replicated partitions.
  public static final String
      AGENT_GOOGLEAPIS_COM_KAFKA_REPLICA_MANAGER_UNDER_REPLICATED_PARTITIONS =
          "agent.googleapis.com/kafka/replica_manager/under_replicated_partitions";
  // Commands executed.
  public static final String AGENT_GOOGLEAPIS_COM_MEMCACHED_COMMAND_COUNT =
      "agent.googleapis.com/memcached/command_count";
  // Number of current connections to the cache.
  public static final String AGENT_GOOGLEAPIS_COM_MEMCACHED_CURRENT_CONNECTIONS =
      "agent.googleapis.com/memcached/current_connections";
  // Number of items currently stored in the cache.
  public static final String AGENT_GOOGLEAPIS_COM_MEMCACHED_CURRENT_ITEMS =
      "agent.googleapis.com/memcached/current_items";
  // Cache item evictions.
  public static final String AGENT_GOOGLEAPIS_COM_MEMCACHED_EVICTION_COUNT =
      "agent.googleapis.com/memcached/eviction_count";
  // Memory usage.
  public static final String AGENT_GOOGLEAPIS_COM_MEMCACHED_MEMORY =
      "agent.googleapis.com/memcached/memory";
  // Bytes transferred over the network.
  public static final String AGENT_GOOGLEAPIS_COM_MEMCACHED_NETWORK =
      "agent.googleapis.com/memcached/network";
  // Memcached operation hit/miss counts.
  public static final String AGENT_GOOGLEAPIS_COM_MEMCACHED_OPERATION_COUNT =
      "agent.googleapis.com/memcached/operation_count";
  // Hit ratio for memcached operations.
  public static final String AGENT_GOOGLEAPIS_COM_MEMCACHED_OPERATION_HITRATIO =
      "agent.googleapis.com/memcached/operation_hitratio";
  // Accumulated user and system time.
  public static final String AGENT_GOOGLEAPIS_COM_MEMCACHED_RUSAGE =
      "agent.googleapis.com/memcached/rusage";
  // Number of threads used by the memcached instance.
  public static final String AGENT_GOOGLEAPIS_COM_MEMCACHED_THREADS =
      "agent.googleapis.com/memcached/threads";
  // Memory Usage (Bytes)
  public static final String AGENT_GOOGLEAPIS_COM_MEMORY_BYTES_USED =
      "agent.googleapis.com/memory/bytes_used";
  // Memory Usage (Percent)
  public static final String AGENT_GOOGLEAPIS_COM_MEMORY_PERCENT_USED =
      "agent.googleapis.com/memory/percent_used";
  // The number of cache hits.
  public static final String AGENT_GOOGLEAPIS_COM_MONGODB_CACHE_HITS =
      "agent.googleapis.com/mongodb/cache/hits";
  // The number of cache misses.
  public static final String AGENT_GOOGLEAPIS_COM_MONGODB_CACHE_MISSES =
      "agent.googleapis.com/mongodb/cache/misses";
  // The number of collections.
  public static final String AGENT_GOOGLEAPIS_COM_MONGODB_COLLECTIONS =
      "agent.googleapis.com/mongodb/collections";
  // The number of active database connections.
  public static final String AGENT_GOOGLEAPIS_COM_MONGODB_CONNECTIONS =
      "agent.googleapis.com/mongodb/connections";
  // The data size, in bytes.
  public static final String AGENT_GOOGLEAPIS_COM_MONGODB_DATA_SIZE =
      "agent.googleapis.com/mongodb/data_size";
  // The number of extents.
  public static final String AGENT_GOOGLEAPIS_COM_MONGODB_EXTENTS =
      "agent.googleapis.com/mongodb/extents";
  // The time the global lock has been held.
  public static final String AGENT_GOOGLEAPIS_COM_MONGODB_GLOBAL_LOCK_HOLD_TIME =
      "agent.googleapis.com/mongodb/global_lock_hold_time";
  // The number of indexes.
  public static final String AGENT_GOOGLEAPIS_COM_MONGODB_INDEXES =
      "agent.googleapis.com/mongodb/indexes";
  // The index size, in bytes.
  public static final String AGENT_GOOGLEAPIS_COM_MONGODB_INDEX_SIZE =
      "agent.googleapis.com/mongodb/index_size";
  // Mebibytes of memory used.
  public static final String AGENT_GOOGLEAPIS_COM_MONGODB_MEMORY_USAGE =
      "agent.googleapis.com/mongodb/memory_usage";
  // The number of objects.
  public static final String AGENT_GOOGLEAPIS_COM_MONGODB_OBJECTS =
      "agent.googleapis.com/mongodb/objects";
  // The number of operations executed.
  public static final String AGENT_GOOGLEAPIS_COM_MONGODB_OPERATION_COUNT =
      "agent.googleapis.com/mongodb/operation_count";
  // The storage size, in bytes.
  public static final String AGENT_GOOGLEAPIS_COM_MONGODB_STORAGE_SIZE =
      "agent.googleapis.com/mongodb/storage_size";
  // Buffer pool page count.
  public static final String AGENT_GOOGLEAPIS_COM_MYSQL_BUFFER_POOL_NUM_PAGES =
      "agent.googleapis.com/mysql/buffer_pool/num_pages";
  // Buffer pool operation count.
  public static final String AGENT_GOOGLEAPIS_COM_MYSQL_BUFFER_POOL_OPERATION_COUNT =
      "agent.googleapis.com/mysql/buffer_pool/operation_count";
  // Buffer pool size.
  public static final String AGENT_GOOGLEAPIS_COM_MYSQL_BUFFER_POOL_SIZE =
      "agent.googleapis.com/mysql/buffer_pool_size";
  // MySQL command count.
  public static final String AGENT_GOOGLEAPIS_COM_MYSQL_COMMAND_COUNT =
      "agent.googleapis.com/mysql/command_count";
  // MySQL handler count.
  public static final String AGENT_GOOGLEAPIS_COM_MYSQL_HANDLER_COUNT =
      "agent.googleapis.com/mysql/handler_count";
  // InnodDB doublewrite buffer count.
  public static final String AGENT_GOOGLEAPIS_COM_MYSQL_INNODB_DOUBLEWRITE_COUNT =
      "agent.googleapis.com/mysql/innodb/doublewrite_count";
  // InnoDB log operation count.
  public static final String AGENT_GOOGLEAPIS_COM_MYSQL_INNODB_LOG_OPERATION_COUNT =
      "agent.googleapis.com/mysql/innodb/log_operation_count";
  // InnoDB operation count.
  public static final String AGENT_GOOGLEAPIS_COM_MYSQL_INNODB_OPERATION_COUNT =
      "agent.googleapis.com/mysql/innodb/operation_count";
  // InnoDB page operation count.
  public static final String AGENT_GOOGLEAPIS_COM_MYSQL_INNODB_PAGE_OPERATION_COUNT =
      "agent.googleapis.com/mysql/innodb/page_operation_count";
  // InnoDB row lock count.
  public static final String AGENT_GOOGLEAPIS_COM_MYSQL_INNODB_ROW_LOCK_COUNT =
      "agent.googleapis.com/mysql/innodb/row_lock_count";
  // InnoDB row operation count.
  public static final String AGENT_GOOGLEAPIS_COM_MYSQL_INNODB_ROW_OPERATION_COUNT =
      "agent.googleapis.com/mysql/innodb/row_operation_count";
  // MySQL lock count.
  public static final String AGENT_GOOGLEAPIS_COM_MYSQL_LOCK_COUNT =
      "agent.googleapis.com/mysql/lock_count";
  // QCache operation count.
  public static final String AGENT_GOOGLEAPIS_COM_MYSQL_QCACHE_OPERATION_COUNT =
      "agent.googleapis.com/mysql/qcache/operation_count";
  // QCache query count.
  public static final String AGENT_GOOGLEAPIS_COM_MYSQL_QCACHE_QUERY_COUNT =
      "agent.googleapis.com/mysql/qcache/query_count";
  // Seconds behind master.
  public static final String AGENT_GOOGLEAPIS_COM_MYSQL_SLAVE_REPLICATION_LAG =
      "agent.googleapis.com/mysql/slave_replication_lag";
  // MySQL sort count.
  public static final String AGENT_GOOGLEAPIS_COM_MYSQL_SORT_COUNT =
      "agent.googleapis.com/mysql/sort_count";
  // Thread count.
  public static final String AGENT_GOOGLEAPIS_COM_MYSQL_THREAD_COUNT =
      "agent.googleapis.com/mysql/thread_count";
  // Count of TCP connections.
  public static final String AGENT_GOOGLEAPIS_COM_NETWORK_TCP_CONNECTIONS =
      "agent.googleapis.com/network/tcp_connections";
  // Total connections accepted.
  public static final String AGENT_GOOGLEAPIS_COM_NGINX_CONNECTIONS_ACCEPTED_COUNT =
      "agent.googleapis.com/nginx/connections/accepted_count";
  // The number of connections currently attached to Nginx.
  public static final String AGENT_GOOGLEAPIS_COM_NGINX_CONNECTIONS_CURRENT =
      "agent.googleapis.com/nginx/connections/current";
  // Total connections handled.
  public static final String AGENT_GOOGLEAPIS_COM_NGINX_CONNECTIONS_HANDLED_COUNT =
      "agent.googleapis.com/nginx/connections/handled_count";
  // The number of requests Nginx has serviced.
  public static final String AGENT_GOOGLEAPIS_COM_NGINX_REQUEST_COUNT =
      "agent.googleapis.com/nginx/request_count";
  // Number of blocks read.
  public static final String AGENT_GOOGLEAPIS_COM_POSTGRESQL_BLOCKS_READ_COUNT =
      "agent.googleapis.com/postgresql/blocks_read_count";
  // Number of commits
  public static final String AGENT_GOOGLEAPIS_COM_POSTGRESQL_COMMIT_COUNT =
      "agent.googleapis.com/postgresql/commit_count";
  // Database disk usage
  public static final String AGENT_GOOGLEAPIS_COM_POSTGRESQL_DB_SIZE =
      "agent.googleapis.com/postgresql/db_size";
  // Number of backends.
  public static final String AGENT_GOOGLEAPIS_COM_POSTGRESQL_NUM_BACKENDS =
      "agent.googleapis.com/postgresql/num_backends";
  // Number of tuples (rows) in the database.
  public static final String AGENT_GOOGLEAPIS_COM_POSTGRESQL_NUM_TUPLES =
      "agent.googleapis.com/postgresql/num_tuples";
  // Number of db row operations.
  public static final String AGENT_GOOGLEAPIS_COM_POSTGRESQL_OPERATION_COUNT =
      "agent.googleapis.com/postgresql/operation_count";
  // Number of rollbacks
  public static final String AGENT_GOOGLEAPIS_COM_POSTGRESQL_ROLLBACK_COUNT =
      "agent.googleapis.com/postgresql/rollback_count";
  // Count of processes in the given state
  public static final String AGENT_GOOGLEAPIS_COM_PROCESSES_COUNT_BY_STATE =
      "agent.googleapis.com/processes/count_by_state";
  // CPU time of the given process
  public static final String AGENT_GOOGLEAPIS_COM_PROCESSES_CPU_TIME =
      "agent.googleapis.com/processes/cpu_time";
  // Total number of processes forked.
  public static final String AGENT_GOOGLEAPIS_COM_PROCESSES_FORK_COUNT =
      "agent.googleapis.com/processes/fork_count";
  // VM Usage of the given process.
  public static final String AGENT_GOOGLEAPIS_COM_PROCESSES_VM_USAGE =
      "agent.googleapis.com/processes/vm_usage";
  // The number of consumers reading from the specified queue
  public static final String AGENT_GOOGLEAPIS_COM_RABBITMQ_CONSUMERS =
      "agent.googleapis.com/rabbitmq/consumers";
  // The rate (per second) at which messages are being delivered
  public static final String AGENT_GOOGLEAPIS_COM_RABBITMQ_DELIVERY_RATE =
      "agent.googleapis.com/rabbitmq/delivery_rate";
  // The number of messages in a queue
  public static final String AGENT_GOOGLEAPIS_COM_RABBITMQ_NUM_MESSAGES =
      "agent.googleapis.com/rabbitmq/num_messages";
  // The rate (per second) at which messages are being published
  public static final String AGENT_GOOGLEAPIS_COM_RABBITMQ_PUBLISH_RATE =
      "agent.googleapis.com/rabbitmq/publish_rate";
  // Change count since last save.
  public static final String AGENT_GOOGLEAPIS_COM_REDIS_CHANGES_SINCE_LAST_SAVE =
      "agent.googleapis.com/redis/changes_since_last_save";
  // Number of blocked clients.
  public static final String AGENT_GOOGLEAPIS_COM_REDIS_CLIENTS_BLOCKED =
      "agent.googleapis.com/redis/clients/blocked";
  // Number of client connections.
  public static final String AGENT_GOOGLEAPIS_COM_REDIS_CLIENTS_CONNECTED =
      "agent.googleapis.com/redis/clients/connected";
  // The total number of commands processed.
  public static final String AGENT_GOOGLEAPIS_COM_REDIS_COMMANDS_PROCESSED =
      "agent.googleapis.com/redis/commands_processed";
  // Number of slave connections.
  public static final String AGENT_GOOGLEAPIS_COM_REDIS_CONNECTIONS_SLAVES =
      "agent.googleapis.com/redis/connections/slaves";
  // The total number of connections accepted.
  public static final String AGENT_GOOGLEAPIS_COM_REDIS_CONNECTIONS_TOTAL =
      "agent.googleapis.com/redis/connections/total";
  // The total number of key expirations.
  public static final String AGENT_GOOGLEAPIS_COM_REDIS_EXPIRED_KEYS =
      "agent.googleapis.com/redis/expired_keys";
  // Memory usage in bytes.
  public static final String AGENT_GOOGLEAPIS_COM_REDIS_MEMORY_USAGE =
      "agent.googleapis.com/redis/memory/usage";
  // Lua memory usage in bytes.
  public static final String AGENT_GOOGLEAPIS_COM_REDIS_MEMORY_USAGE_LUA =
      "agent.googleapis.com/redis/memory/usage_lua";
  // Number of global pub/sub channels with subscribed clients.
  public static final String AGENT_GOOGLEAPIS_COM_REDIS_PUBSUB_CHANNELS =
      "agent.googleapis.com/redis/pubsub/channels";
  // Number of global pub/sub patterns with subscribed clients.
  public static final String AGENT_GOOGLEAPIS_COM_REDIS_PUBSUB_PATTERNS =
      "agent.googleapis.com/redis/pubsub/patterns";
  // Uptime in seconds.
  public static final String AGENT_GOOGLEAPIS_COM_REDIS_UPTIME =
      "agent.googleapis.com/redis/uptime";
  // Time between reception of client read request and subsequent response to client (95th
  // percentile).
  public static final String AGENT_GOOGLEAPIS_COM_RIAK_LATENCY_95TH_PERCENTILE =
      "agent.googleapis.com/riak/latency/95th_percentile";
  // Time between reception of client read request and subsequent response to client (mean).
  public static final String AGENT_GOOGLEAPIS_COM_RIAK_LATENCY_AVERAGE =
      "agent.googleapis.com/riak/latency/average";
  // Time between reception of client read request and subsequent response to client (maximum).
  public static final String AGENT_GOOGLEAPIS_COM_RIAK_LATENCY_MAXIMUM =
      "agent.googleapis.com/riak/latency/maximum";
  // Memory usage.
  public static final String AGENT_GOOGLEAPIS_COM_RIAK_MEMORY_USAGE =
      "agent.googleapis.com/riak/memory_usage";
  // Number of siblings encountered during all GET operations by this node within the last minute
  // (95th percentile).
  public static final String AGENT_GOOGLEAPIS_COM_RIAK_NUM_SIBLINGS_95TH_PERCENTILE =
      "agent.googleapis.com/riak/num_siblings/95th_percentile";
  // Number of siblings encountered during all GET operations by this node within the last minute
  // (mean).
  public static final String AGENT_GOOGLEAPIS_COM_RIAK_NUM_SIBLINGS_AVERAGE =
      "agent.googleapis.com/riak/num_siblings/average";
  // Number of siblings encountered during all GET operations by this node within the last minute
  // (maximum).
  public static final String AGENT_GOOGLEAPIS_COM_RIAK_NUM_SIBLINGS_MAXIMUM =
      "agent.googleapis.com/riak/num_siblings/maximum";
  // Size of objects encountered by this node within the last minute (95th percentile).
  public static final String AGENT_GOOGLEAPIS_COM_RIAK_OBJECT_SIZE_95TH_PERCENTILE =
      "agent.googleapis.com/riak/object_size/95th_percentile";
  // Size of objects encountered by this node within the last minute (mean).
  public static final String AGENT_GOOGLEAPIS_COM_RIAK_OBJECT_SIZE_AVERAGE =
      "agent.googleapis.com/riak/object_size/average";
  // Size of objects encountered by this node within the last minute (maximum).
  public static final String AGENT_GOOGLEAPIS_COM_RIAK_OBJECT_SIZE_MAXIMUM =
      "agent.googleapis.com/riak/object_size/maximum";
  // Coordinated operation count.
  public static final String AGENT_GOOGLEAPIS_COM_RIAK_OPERATION_COUNT =
      "agent.googleapis.com/riak/operation_count";
  // Swap bytes used, free, cached, and reserved.
  public static final String AGENT_GOOGLEAPIS_COM_SWAP_BYTES_USED =
      "agent.googleapis.com/swap/bytes_used";
  // Swap I/O operations
  public static final String AGENT_GOOGLEAPIS_COM_SWAP_IO = "agent.googleapis.com/swap/io";
  // Swap percent used, free, cached, and reserved.
  public static final String AGENT_GOOGLEAPIS_COM_SWAP_PERCENT_USED =
      "agent.googleapis.com/swap/percent_used";
  // The number of active sessions.
  public static final String AGENT_GOOGLEAPIS_COM_TOMCAT_MANAGER_SESSIONS =
      "agent.googleapis.com/tomcat/manager/sessions";
  // The number of errors encountered.
  public static final String AGENT_GOOGLEAPIS_COM_TOMCAT_REQUEST_PROCESSOR_ERROR_COUNT =
      "agent.googleapis.com/tomcat/request_processor/error_count";
  // The total processing time.
  public static final String AGENT_GOOGLEAPIS_COM_TOMCAT_REQUEST_PROCESSOR_PROCESSING_TIME =
      "agent.googleapis.com/tomcat/request_processor/processing_time";
  // The total number of requests.
  public static final String AGENT_GOOGLEAPIS_COM_TOMCAT_REQUEST_PROCESSOR_REQUEST_COUNT =
      "agent.googleapis.com/tomcat/request_processor/request_count";
  // The number of bytes transmitted and received.
  public static final String AGENT_GOOGLEAPIS_COM_TOMCAT_REQUEST_PROCESSOR_TRAFFIC_COUNT =
      "agent.googleapis.com/tomcat/request_processor/traffic_count";
  // The current number of busy threads.
  public static final String AGENT_GOOGLEAPIS_COM_TOMCAT_THREADS_BUSY =
      "agent.googleapis.com/tomcat/threads/busy";
  // The current number of threads.
  public static final String AGENT_GOOGLEAPIS_COM_TOMCAT_THREADS_CURRENT =
      "agent.googleapis.com/tomcat/threads/current";
  // Backend connection success count.
  public static final String AGENT_GOOGLEAPIS_COM_VARNISH_BACKEND_CONNECTION_COUNT =
      "agent.googleapis.com/varnish/backend_connection_count";
  // Cache operation count.
  public static final String AGENT_GOOGLEAPIS_COM_VARNISH_CACHE_OPERATION_COUNT =
      "agent.googleapis.com/varnish/cache_operation_count";
  // Client connection count.
  public static final String AGENT_GOOGLEAPIS_COM_VARNISH_CLIENT_CONNECTION_COUNT =
      "agent.googleapis.com/varnish/client_connection_count";
  // Number of connections currently alive.
  public static final String AGENT_GOOGLEAPIS_COM_ZOOKEEPER_CONNECTIONS_COUNT =
      "agent.googleapis.com/zookeeper/connections_count";
  // Bytes of data on this ZooKeeper server.
  public static final String AGENT_GOOGLEAPIS_COM_ZOOKEEPER_DATA_SIZE =
      "agent.googleapis.com/zookeeper/data_size";
  // Number of followers.
  public static final String AGENT_GOOGLEAPIS_COM_ZOOKEEPER_FOLLOWERS_COUNT =
      "agent.googleapis.com/zookeeper/followers/count";
  // Number of synchronized followers.
  public static final String AGENT_GOOGLEAPIS_COM_ZOOKEEPER_FOLLOWERS_SYNCED_COUNT =
      "agent.googleapis.com/zookeeper/followers/synced_count";
  // Packets received.
  public static final String AGENT_GOOGLEAPIS_COM_ZOOKEEPER_NETWORK_RECEIVED_PACKETS_COUNT =
      "agent.googleapis.com/zookeeper/network/received_packets_count";
  // Packets sent.
  public static final String AGENT_GOOGLEAPIS_COM_ZOOKEEPER_NETWORK_SENT_PACKETS_COUNT =
      "agent.googleapis.com/zookeeper/network/sent_packets_count";
  // ZooKeeper node count.
  public static final String AGENT_GOOGLEAPIS_COM_ZOOKEEPER_NODES_COUNT =
      "agent.googleapis.com/zookeeper/nodes/count";
  // Number of ephemeral nodes.
  public static final String AGENT_GOOGLEAPIS_COM_ZOOKEEPER_NODES_EPHEMERAL_COUNT =
      "agent.googleapis.com/zookeeper/nodes/ephemeral_count";
  // Number of watches on ZooKeeper nodes.
  public static final String AGENT_GOOGLEAPIS_COM_ZOOKEEPER_NODES_WATCHES_COUNT =
      "agent.googleapis.com/zookeeper/nodes/watches_count";
  // Average request latency.
  public static final String AGENT_GOOGLEAPIS_COM_ZOOKEEPER_REQUESTS_LATENCY_AVERAGE =
      "agent.googleapis.com/zookeeper/requests/latency/average";
  // Maximum request latency.
  public static final String AGENT_GOOGLEAPIS_COM_ZOOKEEPER_REQUESTS_LATENCY_MAXIMUM =
      "agent.googleapis.com/zookeeper/requests/latency/maximum";
  // Minimum request latency.
  public static final String AGENT_GOOGLEAPIS_COM_ZOOKEEPER_REQUESTS_LATENCY_MINIMUM =
      "agent.googleapis.com/zookeeper/requests/latency/minimum";
  // Number of outstanding requests.
  public static final String AGENT_GOOGLEAPIS_COM_ZOOKEEPER_REQUESTS_OUTSTANDING_COUNT =
      "agent.googleapis.com/zookeeper/requests/outstanding_count";
  // Number of pending synchronization operations.
  public static final String AGENT_GOOGLEAPIS_COM_ZOOKEEPER_SYNC_OPERATIONS_PENDING_COUNT =
      "agent.googleapis.com/zookeeper/sync_operations/pending_count";
  // Total number of CPU cores allocated to an App Engine flexible environment version.
  public static final String APPENGINE_GOOGLEAPIS_COM_FLEX_CPU_RESERVED_CORES =
      "appengine.googleapis.com/flex/cpu/reserved_cores";
  // The percentage of allocated CPU in use across an App Engine flexible environment version.
  // Note that some machine types allow bursting above 100%.
  public static final String APPENGINE_GOOGLEAPIS_COM_FLEX_CPU_UTILIZATION =
      "appengine.googleapis.com/flex/cpu/utilization";
  // Delta count of bytes read from disk across an App Engine flexible environment version.
  public static final String APPENGINE_GOOGLEAPIS_COM_FLEX_DISK_READ_BYTES_COUNT =
      "appengine.googleapis.com/flex/disk/read_bytes_count";
  // Delta count of bytes written from disk across an App Engine flexible environment version.
  public static final String APPENGINE_GOOGLEAPIS_COM_FLEX_DISK_WRITE_BYTES_COUNT =
      "appengine.googleapis.com/flex/disk/write_bytes_count";
  // Delta count of incoming network bytes across all VMs in an App Engine flexible environment
  // version
  public static final String APPENGINE_GOOGLEAPIS_COM_FLEX_NETWORK_RECEIVED_BYTES_COUNT =
      "appengine.googleapis.com/flex/network/received_bytes_count";
  // Delta count of outgoing network bytes across all VMs in an App Engine flexible environment
  // version
  public static final String APPENGINE_GOOGLEAPIS_COM_FLEX_NETWORK_SENT_BYTES_COUNT =
      "appengine.googleapis.com/flex/network/sent_bytes_count";
  // Delta count of interceptions performed to prevent DoS attacks.
  public static final String APPENGINE_GOOGLEAPIS_COM_HTTP_SERVER_DOS_INTERCEPT_COUNT =
      "appengine.googleapis.com/http/server/dos_intercept_count";
  // Delta count of requests that failed due to the app being over quota.
  public static final String APPENGINE_GOOGLEAPIS_COM_HTTP_SERVER_QUOTA_DENIAL_COUNT =
      "appengine.googleapis.com/http/server/quota_denial_count";
  // Delta HTTP response count.
  public static final String APPENGINE_GOOGLEAPIS_COM_HTTP_SERVER_RESPONSE_COUNT =
      "appengine.googleapis.com/http/server/response_count";
  // HTTP response latency.
  public static final String APPENGINE_GOOGLEAPIS_COM_HTTP_SERVER_RESPONSE_LATENCIES =
      "appengine.googleapis.com/http/server/response_latencies";
  // Delta counts on the HTTP serve style.
  public static final String APPENGINE_GOOGLEAPIS_COM_HTTP_SERVER_RESPONSE_STYLE_COUNT =
      "appengine.googleapis.com/http/server/response_style_count";
  // Memcache utilization in one hundredth of Memcache Compute Unit grouped by command.
  public static final String APPENGINE_GOOGLEAPIS_COM_MEMCACHE_CENTI_MCU_COUNT =
      "appengine.googleapis.com/memcache/centi_mcu_count";
  // Count of memcache key operations, grouped by command and status.
  public static final String APPENGINE_GOOGLEAPIS_COM_MEMCACHE_OPERATION_COUNT =
      "appengine.googleapis.com/memcache/operation_count";
  // Number of bytes received by app from the memcache API, grouped by status and memcache
  // command.
  public static final String APPENGINE_GOOGLEAPIS_COM_MEMCACHE_RECEIVED_BYTES_COUNT =
      "appengine.googleapis.com/memcache/received_bytes_count";
  // Number of bytes sent by app through the memcache API, grouped by memcache command.
  public static final String APPENGINE_GOOGLEAPIS_COM_MEMCACHE_SENT_BYTES_COUNT =
      "appengine.googleapis.com/memcache/sent_bytes_count";
  // CPU usage in megacycles.
  public static final String APPENGINE_GOOGLEAPIS_COM_SYSTEM_CPU_USAGE =
      "appengine.googleapis.com/system/cpu/usage";
  // Number of instances that exist.
  public static final String APPENGINE_GOOGLEAPIS_COM_SYSTEM_INSTANCE_COUNT =
      "appengine.googleapis.com/system/instance_count";
  // Total memory used by running instances.
  public static final String APPENGINE_GOOGLEAPIS_COM_SYSTEM_MEMORY_USAGE =
      "appengine.googleapis.com/system/memory/usage";
  // Delta count of incoming network bandwidth.
  public static final String APPENGINE_GOOGLEAPIS_COM_SYSTEM_NETWORK_RECEIVED_BYTES_COUNT =
      "appengine.googleapis.com/system/network/received_bytes_count";
  // Delta count of outgoing network bandwidth.
  public static final String APPENGINE_GOOGLEAPIS_COM_SYSTEM_NETWORK_SENT_BYTES_COUNT =
      "appengine.googleapis.com/system/network/sent_bytes_count";
  // The percent of requests that generate a 4xx error
  public static final String AWS_GOOGLEAPIS_COM_CLOUDFRONT_4XXERRORRATE_AVERAGE =
      "aws.googleapis.com/CloudFront/4xxErrorRate/Average";
  // The percent of requests that generate a 4xx error
  public static final String AWS_GOOGLEAPIS_COM_CLOUDFRONT_4XXERRORRATE_MAXIMUM =
      "aws.googleapis.com/CloudFront/4xxErrorRate/Maximum";
  // The percent of requests that generate a 4xx error
  public static final String AWS_GOOGLEAPIS_COM_CLOUDFRONT_4XXERRORRATE_MINIMUM =
      "aws.googleapis.com/CloudFront/4xxErrorRate/Minimum";
  // The percent of requests that generate a 4xx error
  public static final String AWS_GOOGLEAPIS_COM_CLOUDFRONT_4XXERRORRATE_SAMPLECOUNT =
      "aws.googleapis.com/CloudFront/4xxErrorRate/SampleCount";
  // The percent of requests that generate a 4xx error
  public static final String AWS_GOOGLEAPIS_COM_CLOUDFRONT_4XXERRORRATE_SUM =
      "aws.googleapis.com/CloudFront/4xxErrorRate/Sum";
  // The percent of requests that generate a 5xx error
  public static final String AWS_GOOGLEAPIS_COM_CLOUDFRONT_5XXERRORRATE_AVERAGE =
      "aws.googleapis.com/CloudFront/5xxErrorRate/Average";
  // The percent of requests that generate a 5xx error
  public static final String AWS_GOOGLEAPIS_COM_CLOUDFRONT_5XXERRORRATE_MAXIMUM =
      "aws.googleapis.com/CloudFront/5xxErrorRate/Maximum";
  // The percent of requests that generate a 5xx error
  public static final String AWS_GOOGLEAPIS_COM_CLOUDFRONT_5XXERRORRATE_MINIMUM =
      "aws.googleapis.com/CloudFront/5xxErrorRate/Minimum";
  // The percent of requests that generate a 5xx error
  public static final String AWS_GOOGLEAPIS_COM_CLOUDFRONT_5XXERRORRATE_SAMPLECOUNT =
      "aws.googleapis.com/CloudFront/5xxErrorRate/SampleCount";
  // The percent of requests that generate a 5xx error
  public static final String AWS_GOOGLEAPIS_COM_CLOUDFRONT_5XXERRORRATE_SUM =
      "aws.googleapis.com/CloudFront/5xxErrorRate/Sum";
  // Bytes download for GET, HEAD and OPTIONS requests
  public static final String AWS_GOOGLEAPIS_COM_CLOUDFRONT_BYTESDOWNLOADED_SAMPLECOUNT =
      "aws.googleapis.com/CloudFront/BytesDownloaded/SampleCount";
  // Bytes download for GET, HEAD and OPTIONS requests
  public static final String AWS_GOOGLEAPIS_COM_CLOUDFRONT_BYTESDOWNLOADED_SUM =
      "aws.googleapis.com/CloudFront/BytesDownloaded/Sum";
  // Bytes uploaded with POST and PUT requests
  public static final String AWS_GOOGLEAPIS_COM_CLOUDFRONT_BYTESUPLOADED_SAMPLECOUNT =
      "aws.googleapis.com/CloudFront/BytesUploaded/SampleCount";
  // Bytes uploaded with POST and PUT requests
  public static final String AWS_GOOGLEAPIS_COM_CLOUDFRONT_BYTESUPLOADED_SUM =
      "aws.googleapis.com/CloudFront/BytesUploaded/Sum";
  // The total number of requests for HTTP/HTTPS and all HTTP methods
  public static final String AWS_GOOGLEAPIS_COM_CLOUDFRONT_REQUESTS_SAMPLECOUNT =
      "aws.googleapis.com/CloudFront/Requests/SampleCount";
  // The total number of requests for HTTP/HTTPS and all HTTP methods
  public static final String AWS_GOOGLEAPIS_COM_CLOUDFRONT_REQUESTS_SUM =
      "aws.googleapis.com/CloudFront/Requests/Sum";
  // The percent of requests that generate either a 4xx or 5xx error
  public static final String AWS_GOOGLEAPIS_COM_CLOUDFRONT_TOTALERRORRATE_AVERAGE =
      "aws.googleapis.com/CloudFront/TotalErrorRate/Average";
  // The percent of requests that generate either a 4xx or 5xx error
  public static final String AWS_GOOGLEAPIS_COM_CLOUDFRONT_TOTALERRORRATE_MAXIMUM =
      "aws.googleapis.com/CloudFront/TotalErrorRate/Maximum";
  // The percent of requests that generate either a 4xx or 5xx error
  public static final String AWS_GOOGLEAPIS_COM_CLOUDFRONT_TOTALERRORRATE_MINIMUM =
      "aws.googleapis.com/CloudFront/TotalErrorRate/Minimum";
  // The percent of requests that generate either a 4xx or 5xx error
  public static final String AWS_GOOGLEAPIS_COM_CLOUDFRONT_TOTALERRORRATE_SAMPLECOUNT =
      "aws.googleapis.com/CloudFront/TotalErrorRate/SampleCount";
  // The percent of requests that generate either a 4xx or 5xx error
  public static final String AWS_GOOGLEAPIS_COM_CLOUDFRONT_TOTALERRORRATE_SUM =
      "aws.googleapis.com/CloudFront/TotalErrorRate/Sum";
  // Count of unsuccesful conditional writes
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_CONDITIONALCHECKFAILEDREQUESTS_AVERAGE =
      "aws.googleapis.com/DynamoDB/ConditionalCheckFailedRequests/Average";
  // Count of unsuccesful conditional writes
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_CONDITIONALCHECKFAILEDREQUESTS_MAXIMUM =
      "aws.googleapis.com/DynamoDB/ConditionalCheckFailedRequests/Maximum";
  // Count of unsuccesful conditional writes
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_CONDITIONALCHECKFAILEDREQUESTS_MINIMUM =
      "aws.googleapis.com/DynamoDB/ConditionalCheckFailedRequests/Minimum";
  // Count of unsuccesful conditional writes
  public static final String
      AWS_GOOGLEAPIS_COM_DYNAMODB_CONDITIONALCHECKFAILEDREQUESTS_SAMPLECOUNT =
          "aws.googleapis.com/DynamoDB/ConditionalCheckFailedRequests/SampleCount";
  // Count of unsuccesful conditional writes
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_CONDITIONALCHECKFAILEDREQUESTS_SUM =
      "aws.googleapis.com/DynamoDB/ConditionalCheckFailedRequests/Sum";
  // Consumed read capacity
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_CONSUMEDREADCAPACITYUNITS_AVERAGE =
      "aws.googleapis.com/DynamoDB/ConsumedReadCapacityUnits/Average";
  // Consumed read capacity
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_CONSUMEDREADCAPACITYUNITS_MAXIMUM =
      "aws.googleapis.com/DynamoDB/ConsumedReadCapacityUnits/Maximum";
  // Consumed read capacity
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_CONSUMEDREADCAPACITYUNITS_MINIMUM =
      "aws.googleapis.com/DynamoDB/ConsumedReadCapacityUnits/Minimum";
  // Consumed read capacity
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_CONSUMEDREADCAPACITYUNITS_SAMPLECOUNT =
      "aws.googleapis.com/DynamoDB/ConsumedReadCapacityUnits/SampleCount";
  // Consumed read capacity
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_CONSUMEDREADCAPACITYUNITS_SUM =
      "aws.googleapis.com/DynamoDB/ConsumedReadCapacityUnits/Sum";
  // Consumed write capacity
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_CONSUMEDWRITECAPACITYUNITS_AVERAGE =
      "aws.googleapis.com/DynamoDB/ConsumedWriteCapacityUnits/Average";
  // Consumed write capacity
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_CONSUMEDWRITECAPACITYUNITS_MAXIMUM =
      "aws.googleapis.com/DynamoDB/ConsumedWriteCapacityUnits/Maximum";
  // Consumed write capacity
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_CONSUMEDWRITECAPACITYUNITS_MINIMUM =
      "aws.googleapis.com/DynamoDB/ConsumedWriteCapacityUnits/Minimum";
  // Consumed write capacity
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_CONSUMEDWRITECAPACITYUNITS_SAMPLECOUNT =
      "aws.googleapis.com/DynamoDB/ConsumedWriteCapacityUnits/SampleCount";
  // Consumed write capacity
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_CONSUMEDWRITECAPACITYUNITS_SUM =
      "aws.googleapis.com/DynamoDB/ConsumedWriteCapacityUnits/Sum";
  // Consumed global secondary index write capacity
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_ONLINEINDEXCONSUMEDWRITECAPACITY_AVERAGE =
      "aws.googleapis.com/DynamoDB/OnlineIndexConsumedWriteCapacity/Average";
  // Consumed global secondary index write capacity
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_ONLINEINDEXCONSUMEDWRITECAPACITY_MAXIMUM =
      "aws.googleapis.com/DynamoDB/OnlineIndexConsumedWriteCapacity/Maximum";
  // Consumed global secondary index write capacity
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_ONLINEINDEXCONSUMEDWRITECAPACITY_MINIMUM =
      "aws.googleapis.com/DynamoDB/OnlineIndexConsumedWriteCapacity/Minimum";
  // Consumed global secondary index write capacity
  public static final String
      AWS_GOOGLEAPIS_COM_DYNAMODB_ONLINEINDEXCONSUMEDWRITECAPACITY_SAMPLECOUNT =
          "aws.googleapis.com/DynamoDB/OnlineIndexConsumedWriteCapacity/SampleCount";
  // Consumed global secondary index write capacity
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_ONLINEINDEXCONSUMEDWRITECAPACITY_SUM =
      "aws.googleapis.com/DynamoDB/OnlineIndexConsumedWriteCapacity/Sum";
  // Global Secondary Index progress
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_ONLINEINDEXPERCENTAGEPROGRESS_AVERAGE =
      "aws.googleapis.com/DynamoDB/OnlineIndexPercentageProgress/Average";
  // Global Secondary Index progress
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_ONLINEINDEXPERCENTAGEPROGRESS_MAXIMUM =
      "aws.googleapis.com/DynamoDB/OnlineIndexPercentageProgress/Maximum";
  // Global Secondary Index progress
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_ONLINEINDEXPERCENTAGEPROGRESS_MINIMUM =
      "aws.googleapis.com/DynamoDB/OnlineIndexPercentageProgress/Minimum";
  // Global Secondary Index progress
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_ONLINEINDEXPERCENTAGEPROGRESS_SAMPLECOUNT =
      "aws.googleapis.com/DynamoDB/OnlineIndexPercentageProgress/SampleCount";
  // Global Secondary Index progress
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_ONLINEINDEXPERCENTAGEPROGRESS_SUM =
      "aws.googleapis.com/DynamoDB/OnlineIndexPercentageProgress/Sum";
  // Global Secondary Index writes throttled
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_ONLINEINDEXTHROTTLEEVENTS_AVERAGE =
      "aws.googleapis.com/DynamoDB/OnlineIndexThrottleEvents/Average";
  // Global Secondary Index writes throttled
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_ONLINEINDEXTHROTTLEEVENTS_MAXIMUM =
      "aws.googleapis.com/DynamoDB/OnlineIndexThrottleEvents/Maximum";
  // Global Secondary Index writes throttled
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_ONLINEINDEXTHROTTLEEVENTS_MINIMUM =
      "aws.googleapis.com/DynamoDB/OnlineIndexThrottleEvents/Minimum";
  // Global Secondary Index writes throttled
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_ONLINEINDEXTHROTTLEEVENTS_SAMPLECOUNT =
      "aws.googleapis.com/DynamoDB/OnlineIndexThrottleEvents/SampleCount";
  // Global Secondary Index writes throttled
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_ONLINEINDEXTHROTTLEEVENTS_SUM =
      "aws.googleapis.com/DynamoDB/OnlineIndexThrottleEvents/Sum";
  // Provisioned read capacity
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_PROVISIONEDREADCAPACITYUNITS_AVERAGE =
      "aws.googleapis.com/DynamoDB/ProvisionedReadCapacityUnits/Average";
  // Provisioned read capacity
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_PROVISIONEDREADCAPACITYUNITS_MAXIMUM =
      "aws.googleapis.com/DynamoDB/ProvisionedReadCapacityUnits/Maximum";
  // Provisioned read capacity
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_PROVISIONEDREADCAPACITYUNITS_MINIMUM =
      "aws.googleapis.com/DynamoDB/ProvisionedReadCapacityUnits/Minimum";
  // Provisioned read capacity
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_PROVISIONEDREADCAPACITYUNITS_SAMPLECOUNT =
      "aws.googleapis.com/DynamoDB/ProvisionedReadCapacityUnits/SampleCount";
  // Provisioned read capacity
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_PROVISIONEDREADCAPACITYUNITS_SUM =
      "aws.googleapis.com/DynamoDB/ProvisionedReadCapacityUnits/Sum";
  // Provisioned write capacity
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_PROVISIONEDWRITECAPACITYUNITS_AVERAGE =
      "aws.googleapis.com/DynamoDB/ProvisionedWriteCapacityUnits/Average";
  // Provisioned write capacity
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_PROVISIONEDWRITECAPACITYUNITS_MAXIMUM =
      "aws.googleapis.com/DynamoDB/ProvisionedWriteCapacityUnits/Maximum";
  // Provisioned write capacity
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_PROVISIONEDWRITECAPACITYUNITS_MINIMUM =
      "aws.googleapis.com/DynamoDB/ProvisionedWriteCapacityUnits/Minimum";
  // Provisioned write capacity
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_PROVISIONEDWRITECAPACITYUNITS_SAMPLECOUNT =
      "aws.googleapis.com/DynamoDB/ProvisionedWriteCapacityUnits/SampleCount";
  // Provisioned write capacity
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_PROVISIONEDWRITECAPACITYUNITS_SUM =
      "aws.googleapis.com/DynamoDB/ProvisionedWriteCapacityUnits/Sum";
  // Count of reads throttled
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_READTHROTTLEEVENTS_SAMPLECOUNT =
      "aws.googleapis.com/DynamoDB/ReadThrottleEvents/SampleCount";
  // Count of reads throttled
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_READTHROTTLEEVENTS_SUM =
      "aws.googleapis.com/DynamoDB/ReadThrottleEvents/Sum";
  // Records retrieved by Scan/Query call
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_RETURNEDITEMCOUNT_AVERAGE =
      "aws.googleapis.com/DynamoDB/ReturnedItemCount/Average";
  // Records retrieved by Scan/Query call
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_RETURNEDITEMCOUNT_MAXIMUM =
      "aws.googleapis.com/DynamoDB/ReturnedItemCount/Maximum";
  // Records retrieved by Scan/Query call
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_RETURNEDITEMCOUNT_MINIMUM =
      "aws.googleapis.com/DynamoDB/ReturnedItemCount/Minimum";
  // Records retrieved by Scan/Query call
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_RETURNEDITEMCOUNT_SAMPLECOUNT =
      "aws.googleapis.com/DynamoDB/ReturnedItemCount/SampleCount";
  // Records retrieved by Scan/Query call
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_RETURNEDITEMCOUNT_SUM =
      "aws.googleapis.com/DynamoDB/ReturnedItemCount/Sum";
  // The number and latency of successful requests in the last time period.
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_SUCCESSFULREQUESTLATENCY_AVERAGE =
      "aws.googleapis.com/DynamoDB/SuccessfulRequestLatency/Average";
  // The number and latency of successful requests in the last time period.
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_SUCCESSFULREQUESTLATENCY_MAXIMUM =
      "aws.googleapis.com/DynamoDB/SuccessfulRequestLatency/Maximum";
  // The number and latency of successful requests in the last time period.
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_SUCCESSFULREQUESTLATENCY_MINIMUM =
      "aws.googleapis.com/DynamoDB/SuccessfulRequestLatency/Minimum";
  // The number and latency of successful requests in the last time period.
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_SUCCESSFULREQUESTLATENCY_SAMPLECOUNT =
      "aws.googleapis.com/DynamoDB/SuccessfulRequestLatency/SampleCount";
  // The number and latency of successful requests in the last time period.
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_SUCCESSFULREQUESTLATENCY_SUM =
      "aws.googleapis.com/DynamoDB/SuccessfulRequestLatency/Sum";
  // 500 errors from requests
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_SYSTEMERRORS_SAMPLECOUNT =
      "aws.googleapis.com/DynamoDB/SystemErrors/SampleCount";
  // 500 errors from requests
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_SYSTEMERRORS_SUM =
      "aws.googleapis.com/DynamoDB/SystemErrors/Sum";
  // Count of requests throttled
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_THROTTLEDREQUESTS_SAMPLECOUNT =
      "aws.googleapis.com/DynamoDB/ThrottledRequests/SampleCount";
  // Count of requests throttled
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_THROTTLEDREQUESTS_SUM =
      "aws.googleapis.com/DynamoDB/ThrottledRequests/Sum";
  // 400 errors from requests
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_USERERRORS_SAMPLECOUNT =
      "aws.googleapis.com/DynamoDB/UserErrors/SampleCount";
  // 400 errors from requests
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_USERERRORS_SUM =
      "aws.googleapis.com/DynamoDB/UserErrors/Sum";
  // Count of writes throttled
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_WRITETHROTTLEEVENTS_SAMPLECOUNT =
      "aws.googleapis.com/DynamoDB/WriteThrottleEvents/SampleCount";
  // Count of writes throttled
  public static final String AWS_GOOGLEAPIS_COM_DYNAMODB_WRITETHROTTLEEVENTS_SUM =
      "aws.googleapis.com/DynamoDB/WriteThrottleEvents/Sum";
  // Count of operations (read and write) executed
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMECONSUMEDREADWRITEOPS_SAMPLECOUNT =
      "aws.googleapis.com/EBS/VolumeConsumedReadWriteOps/SampleCount";
  // Count of operations (read and write) executed
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMECONSUMEDREADWRITEOPS_SUM =
      "aws.googleapis.com/EBS/VolumeConsumedReadWriteOps/Sum";
  // Time spent without any read or write operations (in seconds)
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMEIDLETIME_AVERAGE =
      "aws.googleapis.com/EBS/VolumeIdleTime/Average";
  // Time spent without any read or write operations (in seconds)
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMEIDLETIME_MAXIMUM =
      "aws.googleapis.com/EBS/VolumeIdleTime/Maximum";
  // Time spent without any read or write operations (in seconds)
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMEIDLETIME_MINIMUM =
      "aws.googleapis.com/EBS/VolumeIdleTime/Minimum";
  // Time spent without any read or write operations (in seconds)
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMEIDLETIME_SAMPLECOUNT =
      "aws.googleapis.com/EBS/VolumeIdleTime/SampleCount";
  // Time spent without any read or write operations (in seconds)
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMEIDLETIME_SUM =
      "aws.googleapis.com/EBS/VolumeIdleTime/Sum";
  // Total number of pending read and write operations
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMEQUEUELENGTH_SAMPLECOUNT =
      "aws.googleapis.com/EBS/VolumeQueueLength/SampleCount";
  // Total number of pending read and write operations
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMEQUEUELENGTH_SUM =
      "aws.googleapis.com/EBS/VolumeQueueLength/Sum";
  // Bytes read from the volume
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMEREADBYTES_AVERAGE =
      "aws.googleapis.com/EBS/VolumeReadBytes/Average";
  // Bytes read from the volume
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMEREADBYTES_MAXIMUM =
      "aws.googleapis.com/EBS/VolumeReadBytes/Maximum";
  // Bytes read from the volume
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMEREADBYTES_MINIMUM =
      "aws.googleapis.com/EBS/VolumeReadBytes/Minimum";
  // Bytes read from the volume
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMEREADBYTES_SAMPLECOUNT =
      "aws.googleapis.com/EBS/VolumeReadBytes/SampleCount";
  // Bytes read from the volume
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMEREADBYTES_SUM =
      "aws.googleapis.com/EBS/VolumeReadBytes/Sum";
  // Total number of read operations
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMEREADOPS_SAMPLECOUNT =
      "aws.googleapis.com/EBS/VolumeReadOps/SampleCount";
  // Total number of read operations
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMEREADOPS_SUM =
      "aws.googleapis.com/EBS/VolumeReadOps/Sum";
  // Total size of all the volumes.
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMESIZE_SAMPLECOUNT =
      "aws.googleapis.com/EBS/VolumeSize/SampleCount";
  // Total size of all the volumes.
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMESIZE_SUM =
      "aws.googleapis.com/EBS/VolumeSize/Sum";
  // The ratio of IOPS delivered to IOPS made available for a volume
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMETHROUGHPUTPERCENTAGE_AVERAGE =
      "aws.googleapis.com/EBS/VolumeThroughputPercentage/Average";
  // The ratio of IOPS delivered to IOPS made available for a volume
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMETHROUGHPUTPERCENTAGE_MAXIMUM =
      "aws.googleapis.com/EBS/VolumeThroughputPercentage/Maximum";
  // The ratio of IOPS delivered to IOPS made available for a volume
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMETHROUGHPUTPERCENTAGE_MINIMUM =
      "aws.googleapis.com/EBS/VolumeThroughputPercentage/Minimum";
  // The ratio of IOPS delivered to IOPS made available for a volume
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMETHROUGHPUTPERCENTAGE_SAMPLECOUNT =
      "aws.googleapis.com/EBS/VolumeThroughputPercentage/SampleCount";
  // The ratio of IOPS delivered to IOPS made available for a volume
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMETHROUGHPUTPERCENTAGE_SUM =
      "aws.googleapis.com/EBS/VolumeThroughputPercentage/Sum";
  // Time utilized for read operations (in seconds)
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMETOTALREADTIME_AVERAGE =
      "aws.googleapis.com/EBS/VolumeTotalReadTime/Average";
  // Time utilized for read operations (in seconds)
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMETOTALREADTIME_MAXIMUM =
      "aws.googleapis.com/EBS/VolumeTotalReadTime/Maximum";
  // Time utilized for read operations (in seconds)
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMETOTALREADTIME_MINIMUM =
      "aws.googleapis.com/EBS/VolumeTotalReadTime/Minimum";
  // Time utilized for read operations (in seconds)
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMETOTALREADTIME_SAMPLECOUNT =
      "aws.googleapis.com/EBS/VolumeTotalReadTime/SampleCount";
  // Time utilized for read operations (in seconds)
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMETOTALREADTIME_SUM =
      "aws.googleapis.com/EBS/VolumeTotalReadTime/Sum";
  // Time utilized for write operations (in seconds)
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMETOTALWRITETIME_AVERAGE =
      "aws.googleapis.com/EBS/VolumeTotalWriteTime/Average";
  // Time utilized for write operations (in seconds)
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMETOTALWRITETIME_MAXIMUM =
      "aws.googleapis.com/EBS/VolumeTotalWriteTime/Maximum";
  // Time utilized for write operations (in seconds)
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMETOTALWRITETIME_MINIMUM =
      "aws.googleapis.com/EBS/VolumeTotalWriteTime/Minimum";
  // Time utilized for write operations (in seconds)
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMETOTALWRITETIME_SAMPLECOUNT =
      "aws.googleapis.com/EBS/VolumeTotalWriteTime/SampleCount";
  // Time utilized for write operations (in seconds)
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMETOTALWRITETIME_SUM =
      "aws.googleapis.com/EBS/VolumeTotalWriteTime/Sum";
  // Bytes written to the volume
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMEWRITEBYTES_AVERAGE =
      "aws.googleapis.com/EBS/VolumeWriteBytes/Average";
  // Bytes written to the volume
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMEWRITEBYTES_MAXIMUM =
      "aws.googleapis.com/EBS/VolumeWriteBytes/Maximum";
  // Bytes written to the volume
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMEWRITEBYTES_MINIMUM =
      "aws.googleapis.com/EBS/VolumeWriteBytes/Minimum";
  // Bytes written to the volume
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMEWRITEBYTES_SAMPLECOUNT =
      "aws.googleapis.com/EBS/VolumeWriteBytes/SampleCount";
  // Bytes written to the volume
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMEWRITEBYTES_SUM =
      "aws.googleapis.com/EBS/VolumeWriteBytes/Sum";
  // Total number of write operations
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMEWRITEOPS_SAMPLECOUNT =
      "aws.googleapis.com/EBS/VolumeWriteOps/SampleCount";
  // Total number of write operations
  public static final String AWS_GOOGLEAPIS_COM_EBS_VOLUMEWRITEOPS_SUM =
      "aws.googleapis.com/EBS/VolumeWriteOps/Sum";
  // For T2 instances, the amount of time available for a virtual CPU to exceed its standard
  // performance measure
  public static final String AWS_GOOGLEAPIS_COM_EC2_CPUCREDITBALANCE_AVERAGE =
      "aws.googleapis.com/EC2/CPUCreditBalance/Average";
  // For T2 instances, the amount of time available for a virtual CPU to exceed its standard
  // performance measure
  public static final String AWS_GOOGLEAPIS_COM_EC2_CPUCREDITBALANCE_MAXIMUM =
      "aws.googleapis.com/EC2/CPUCreditBalance/Maximum";
  // For T2 instances, the amount of time available for a virtual CPU to exceed its standard
  // performance measure
  public static final String AWS_GOOGLEAPIS_COM_EC2_CPUCREDITBALANCE_MINIMUM =
      "aws.googleapis.com/EC2/CPUCreditBalance/Minimum";
  // For T2 instances, the amount of time available for a virtual CPU to exceed its standard
  // performance measure
  public static final String AWS_GOOGLEAPIS_COM_EC2_CPUCREDITBALANCE_SAMPLECOUNT =
      "aws.googleapis.com/EC2/CPUCreditBalance/SampleCount";
  // For T2 instances, the amount of time available for a virtual CPU to exceed its standard
  // performance measure
  public static final String AWS_GOOGLEAPIS_COM_EC2_CPUCREDITBALANCE_SUM =
      "aws.googleapis.com/EC2/CPUCreditBalance/Sum";
  // For T2 instances, the amount of time that virtual CPUs required from the physical CPUs for
  // processing
  public static final String AWS_GOOGLEAPIS_COM_EC2_CPUCREDITUSAGE_AVERAGE =
      "aws.googleapis.com/EC2/CPUCreditUsage/Average";
  // For T2 instances, the amount of time that virtual CPUs required from the physical CPUs for
  // processing
  public static final String AWS_GOOGLEAPIS_COM_EC2_CPUCREDITUSAGE_MAXIMUM =
      "aws.googleapis.com/EC2/CPUCreditUsage/Maximum";
  // For T2 instances, the amount of time that virtual CPUs required from the physical CPUs for
  // processing
  public static final String AWS_GOOGLEAPIS_COM_EC2_CPUCREDITUSAGE_MINIMUM =
      "aws.googleapis.com/EC2/CPUCreditUsage/Minimum";
  // For T2 instances, the amount of time that virtual CPUs required from the physical CPUs for
  // processing
  public static final String AWS_GOOGLEAPIS_COM_EC2_CPUCREDITUSAGE_SAMPLECOUNT =
      "aws.googleapis.com/EC2/CPUCreditUsage/SampleCount";
  // For T2 instances, the amount of time that virtual CPUs required from the physical CPUs for
  // processing
  public static final String AWS_GOOGLEAPIS_COM_EC2_CPUCREDITUSAGE_SUM =
      "aws.googleapis.com/EC2/CPUCreditUsage/Sum";
  // The percentage of provisioned processing resources being used by the instance.
  public static final String AWS_GOOGLEAPIS_COM_EC2_CPUUTILIZATION_AVERAGE =
      "aws.googleapis.com/EC2/CPUUtilization/Average";
  // The percentage of provisioned processing resources being used by the instance.
  public static final String AWS_GOOGLEAPIS_COM_EC2_CPUUTILIZATION_MAXIMUM =
      "aws.googleapis.com/EC2/CPUUtilization/Maximum";
  // The percentage of provisioned processing resources being used by the instance.
  public static final String AWS_GOOGLEAPIS_COM_EC2_CPUUTILIZATION_MINIMUM =
      "aws.googleapis.com/EC2/CPUUtilization/Minimum";
  // The percentage of provisioned processing resources being used by the instance.
  public static final String AWS_GOOGLEAPIS_COM_EC2_CPUUTILIZATION_SAMPLECOUNT =
      "aws.googleapis.com/EC2/CPUUtilization/SampleCount";
  // The percentage of provisioned processing resources being used by the instance.
  public static final String AWS_GOOGLEAPIS_COM_EC2_CPUUTILIZATION_SUM =
      "aws.googleapis.com/EC2/CPUUtilization/Sum";
  // Number of bytes read from ephemeral disks attached to the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_DISKREADBYTES_AVERAGE =
      "aws.googleapis.com/EC2/DiskReadBytes/Average";
  // Number of bytes read from ephemeral disks attached to the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_DISKREADBYTES_MAXIMUM =
      "aws.googleapis.com/EC2/DiskReadBytes/Maximum";
  // Number of bytes read from ephemeral disks attached to the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_DISKREADBYTES_MINIMUM =
      "aws.googleapis.com/EC2/DiskReadBytes/Minimum";
  // Number of bytes read from ephemeral disks attached to the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_DISKREADBYTES_SAMPLECOUNT =
      "aws.googleapis.com/EC2/DiskReadBytes/SampleCount";
  // Number of bytes read from ephemeral disks attached to the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_DISKREADBYTES_SUM =
      "aws.googleapis.com/EC2/DiskReadBytes/Sum";
  // Number of ephemeral read operations completed
  public static final String AWS_GOOGLEAPIS_COM_EC2_DISKREADOPS_SAMPLECOUNT =
      "aws.googleapis.com/EC2/DiskReadOps/SampleCount";
  // Number of ephemeral read operations completed
  public static final String AWS_GOOGLEAPIS_COM_EC2_DISKREADOPS_SUM =
      "aws.googleapis.com/EC2/DiskReadOps/Sum";
  // Number of bytes written to ephemeral disks attached to the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_DISKWRITEBYTES_AVERAGE =
      "aws.googleapis.com/EC2/DiskWriteBytes/Average";
  // Number of bytes written to ephemeral disks attached to the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_DISKWRITEBYTES_MAXIMUM =
      "aws.googleapis.com/EC2/DiskWriteBytes/Maximum";
  // Number of bytes written to ephemeral disks attached to the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_DISKWRITEBYTES_MINIMUM =
      "aws.googleapis.com/EC2/DiskWriteBytes/Minimum";
  // Number of bytes written to ephemeral disks attached to the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_DISKWRITEBYTES_SAMPLECOUNT =
      "aws.googleapis.com/EC2/DiskWriteBytes/SampleCount";
  // Number of bytes written to ephemeral disks attached to the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_DISKWRITEBYTES_SUM =
      "aws.googleapis.com/EC2/DiskWriteBytes/Sum";
  // Number of ephemeral write operations completed
  public static final String AWS_GOOGLEAPIS_COM_EC2_DISKWRITEOPS_SAMPLECOUNT =
      "aws.googleapis.com/EC2/DiskWriteOps/SampleCount";
  // Number of ephemeral write operations completed
  public static final String AWS_GOOGLEAPIS_COM_EC2_DISKWRITEOPS_SUM =
      "aws.googleapis.com/EC2/DiskWriteOps/Sum";
  // Bytes of incoming network traffic to the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_NETWORKIN_AVERAGE =
      "aws.googleapis.com/EC2/NetworkIn/Average";
  // Bytes of incoming network traffic to the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_NETWORKIN_MAXIMUM =
      "aws.googleapis.com/EC2/NetworkIn/Maximum";
  // Bytes of incoming network traffic to the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_NETWORKIN_MINIMUM =
      "aws.googleapis.com/EC2/NetworkIn/Minimum";
  // Bytes of incoming network traffic to the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_NETWORKIN_SAMPLECOUNT =
      "aws.googleapis.com/EC2/NetworkIn/SampleCount";
  // Bytes of incoming network traffic to the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_NETWORKIN_SUM =
      "aws.googleapis.com/EC2/NetworkIn/Sum";
  // Bytes of outgoing network traffic from the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_NETWORKOUT_AVERAGE =
      "aws.googleapis.com/EC2/NetworkOut/Average";
  // Bytes of outgoing network traffic from the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_NETWORKOUT_MAXIMUM =
      "aws.googleapis.com/EC2/NetworkOut/Maximum";
  // Bytes of outgoing network traffic from the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_NETWORKOUT_MINIMUM =
      "aws.googleapis.com/EC2/NetworkOut/Minimum";
  // Bytes of outgoing network traffic from the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_NETWORKOUT_SAMPLECOUNT =
      "aws.googleapis.com/EC2/NetworkOut/SampleCount";
  // Bytes of outgoing network traffic from the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_NETWORKOUT_SUM =
      "aws.googleapis.com/EC2/NetworkOut/Sum";
  // Incoming network packets to the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_NETWORKPACKETSIN_AVERAGE =
      "aws.googleapis.com/EC2/NetworkPacketsIn/Average";
  // Incoming network packets to the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_NETWORKPACKETSIN_MAXIMUM =
      "aws.googleapis.com/EC2/NetworkPacketsIn/Maximum";
  // Incoming network packets to the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_NETWORKPACKETSIN_MINIMUM =
      "aws.googleapis.com/EC2/NetworkPacketsIn/Minimum";
  // Incoming network packets to the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_NETWORKPACKETSIN_SAMPLECOUNT =
      "aws.googleapis.com/EC2/NetworkPacketsIn/SampleCount";
  // Incoming network packets to the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_NETWORKPACKETSIN_SUM =
      "aws.googleapis.com/EC2/NetworkPacketsIn/Sum";
  // Outgoing network packets from the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_NETWORKPACKETSOUT_AVERAGE =
      "aws.googleapis.com/EC2/NetworkPacketsOut/Average";
  // Outgoing network packets from the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_NETWORKPACKETSOUT_MAXIMUM =
      "aws.googleapis.com/EC2/NetworkPacketsOut/Maximum";
  // Outgoing network packets from the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_NETWORKPACKETSOUT_MINIMUM =
      "aws.googleapis.com/EC2/NetworkPacketsOut/Minimum";
  // Outgoing network packets from the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_NETWORKPACKETSOUT_SAMPLECOUNT =
      "aws.googleapis.com/EC2/NetworkPacketsOut/SampleCount";
  // Outgoing network packets from the instance
  public static final String AWS_GOOGLEAPIS_COM_EC2_NETWORKPACKETSOUT_SUM =
      "aws.googleapis.com/EC2/NetworkPacketsOut/Sum";
  // Boolean on the instance status check: 0 if it passes, 1 if it fails
  public static final String AWS_GOOGLEAPIS_COM_EC2_STATUSCHECKFAILED_INSTANCE_SAMPLECOUNT =
      "aws.googleapis.com/EC2/StatusCheckFailed_Instance/SampleCount";
  // Boolean on the instance status check: 0 if it passes, 1 if it fails
  public static final String AWS_GOOGLEAPIS_COM_EC2_STATUSCHECKFAILED_INSTANCE_SUM =
      "aws.googleapis.com/EC2/StatusCheckFailed_Instance/Sum";
  // Boolean on the AWS instance and system status checks: 0 if both pass, 1 if either fails
  public static final String AWS_GOOGLEAPIS_COM_EC2_STATUSCHECKFAILED_SAMPLECOUNT =
      "aws.googleapis.com/EC2/StatusCheckFailed/SampleCount";
  // Boolean on the AWS instance and system status checks: 0 if both pass, 1 if either fails
  public static final String AWS_GOOGLEAPIS_COM_EC2_STATUSCHECKFAILED_SUM =
      "aws.googleapis.com/EC2/StatusCheckFailed/Sum";
  // Boolean on the system status check: 0 if it passes, 1 if it fails
  public static final String AWS_GOOGLEAPIS_COM_EC2_STATUSCHECKFAILED_SYSTEM_SAMPLECOUNT =
      "aws.googleapis.com/EC2/StatusCheckFailed_System/SampleCount";
  // Boolean on the system status check: 0 if it passes, 1 if it fails
  public static final String AWS_GOOGLEAPIS_COM_EC2_STATUSCHECKFAILED_SYSTEM_SUM =
      "aws.googleapis.com/EC2/StatusCheckFailed_System/Sum";
  // Average percent of bytes used for cache items
  public static final String
      AWS_GOOGLEAPIS_COM_ELASTICACHE_AVERAGEBYTESUSEDFORCACHEITEMSPERCENTAGE_AVERAGE =
          "aws.googleapis.com/ElastiCache/AverageBytesUsedForCacheItemsPercentage/Average";
  // Average percent of bytes used for cache items
  public static final String
      AWS_GOOGLEAPIS_COM_ELASTICACHE_AVERAGEBYTESUSEDFORCACHEITEMSPERCENTAGE_MAXIMUM =
          "aws.googleapis.com/ElastiCache/AverageBytesUsedForCacheItemsPercentage/Maximum";
  // Average percent of bytes used for cache items
  public static final String
      AWS_GOOGLEAPIS_COM_ELASTICACHE_AVERAGEBYTESUSEDFORCACHEITEMSPERCENTAGE_MINIMUM =
          "aws.googleapis.com/ElastiCache/AverageBytesUsedForCacheItemsPercentage/Minimum";
  // Average percent of bytes used for cache items
  public static final String
      AWS_GOOGLEAPIS_COM_ELASTICACHE_AVERAGEBYTESUSEDFORCACHEITEMSPERCENTAGE_SAMPLECOUNT =
          "aws.googleapis.com/ElastiCache/AverageBytesUsedForCacheItemsPercentage/SampleCount";
  // Average percent of bytes used for cache items
  public static final String
      AWS_GOOGLEAPIS_COM_ELASTICACHE_AVERAGEBYTESUSEDFORCACHEITEMSPERCENTAGE_SUM =
          "aws.googleapis.com/ElastiCache/AverageBytesUsedForCacheItemsPercentage/Sum";
  // Average percent of bytes used for cache
  public static final String
      AWS_GOOGLEAPIS_COM_ELASTICACHE_AVERAGEBYTESUSEDFORCACHEPERCENTAGE_AVERAGE =
          "aws.googleapis.com/ElastiCache/AverageBytesUsedForCachePercentage/Average";
  // Average percent of bytes used for cache
  public static final String
      AWS_GOOGLEAPIS_COM_ELASTICACHE_AVERAGEBYTESUSEDFORCACHEPERCENTAGE_MAXIMUM =
          "aws.googleapis.com/ElastiCache/AverageBytesUsedForCachePercentage/Maximum";
  // Average percent of bytes used for cache
  public static final String
      AWS_GOOGLEAPIS_COM_ELASTICACHE_AVERAGEBYTESUSEDFORCACHEPERCENTAGE_MINIMUM =
          "aws.googleapis.com/ElastiCache/AverageBytesUsedForCachePercentage/Minimum";
  // Average percent of bytes used for cache
  public static final String
      AWS_GOOGLEAPIS_COM_ELASTICACHE_AVERAGEBYTESUSEDFORCACHEPERCENTAGE_SAMPLECOUNT =
          "aws.googleapis.com/ElastiCache/AverageBytesUsedForCachePercentage/SampleCount";
  // Average percent of bytes used for cache
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_AVERAGEBYTESUSEDFORCACHEPERCENTAGE_SUM =
      "aws.googleapis.com/ElastiCache/AverageBytesUsedForCachePercentage/Sum";
  // Average percent of freeable memory
  public static final String
      AWS_GOOGLEAPIS_COM_ELASTICACHE_AVERAGEFREEABLEMEMORYPERCENTAGE_AVERAGE =
          "aws.googleapis.com/ElastiCache/AverageFreeableMemoryPercentage/Average";
  // Average percent of freeable memory
  public static final String
      AWS_GOOGLEAPIS_COM_ELASTICACHE_AVERAGEFREEABLEMEMORYPERCENTAGE_MAXIMUM =
          "aws.googleapis.com/ElastiCache/AverageFreeableMemoryPercentage/Maximum";
  // Average percent of freeable memory
  public static final String
      AWS_GOOGLEAPIS_COM_ELASTICACHE_AVERAGEFREEABLEMEMORYPERCENTAGE_MINIMUM =
          "aws.googleapis.com/ElastiCache/AverageFreeableMemoryPercentage/Minimum";
  // Average percent of freeable memory
  public static final String
      AWS_GOOGLEAPIS_COM_ELASTICACHE_AVERAGEFREEABLEMEMORYPERCENTAGE_SAMPLECOUNT =
          "aws.googleapis.com/ElastiCache/AverageFreeableMemoryPercentage/SampleCount";
  // Average percent of freeable memory
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_AVERAGEFREEABLEMEMORYPERCENTAGE_SUM =
      "aws.googleapis.com/ElastiCache/AverageFreeableMemoryPercentage/Sum";
  // Average percent of unused memory
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_AVERAGEUNUSEDMEMORYPERCENTAGE_AVERAGE =
      "aws.googleapis.com/ElastiCache/AverageUnusedMemoryPercentage/Average";
  // Average percent of unused memory
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_AVERAGEUNUSEDMEMORYPERCENTAGE_MAXIMUM =
      "aws.googleapis.com/ElastiCache/AverageUnusedMemoryPercentage/Maximum";
  // Average percent of unused memory
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_AVERAGEUNUSEDMEMORYPERCENTAGE_MINIMUM =
      "aws.googleapis.com/ElastiCache/AverageUnusedMemoryPercentage/Minimum";
  // Average percent of unused memory
  public static final String
      AWS_GOOGLEAPIS_COM_ELASTICACHE_AVERAGEUNUSEDMEMORYPERCENTAGE_SAMPLECOUNT =
          "aws.googleapis.com/ElastiCache/AverageUnusedMemoryPercentage/SampleCount";
  // Average percent of unused memory
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_AVERAGEUNUSEDMEMORYPERCENTAGE_SUM =
      "aws.googleapis.com/ElastiCache/AverageUnusedMemoryPercentage/Sum";
  // Data read by Memcached
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_BYTESREADINTOMEMCACHED_AVERAGE =
      "aws.googleapis.com/ElastiCache/BytesReadIntoMemcached/Average";
  // Data read by Memcached
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_BYTESREADINTOMEMCACHED_MAXIMUM =
      "aws.googleapis.com/ElastiCache/BytesReadIntoMemcached/Maximum";
  // Data read by Memcached
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_BYTESREADINTOMEMCACHED_MINIMUM =
      "aws.googleapis.com/ElastiCache/BytesReadIntoMemcached/Minimum";
  // Data read by Memcached
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_BYTESREADINTOMEMCACHED_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/BytesReadIntoMemcached/SampleCount";
  // Data read by Memcached
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_BYTESREADINTOMEMCACHED_SUM =
      "aws.googleapis.com/ElastiCache/BytesReadIntoMemcached/Sum";
  // Storage space in bytes required by cache resources
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_BYTESUSEDFORCACHEITEMS_AVERAGE =
      "aws.googleapis.com/ElastiCache/BytesUsedForCacheItems/Average";
  // Storage space in bytes required by cache resources
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_BYTESUSEDFORCACHEITEMS_MAXIMUM =
      "aws.googleapis.com/ElastiCache/BytesUsedForCacheItems/Maximum";
  // Storage space in bytes required by cache resources
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_BYTESUSEDFORCACHEITEMS_MINIMUM =
      "aws.googleapis.com/ElastiCache/BytesUsedForCacheItems/Minimum";
  // Storage space in bytes required by cache resources
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_BYTESUSEDFORCACHEITEMS_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/BytesUsedForCacheItems/SampleCount";
  // Storage space in bytes required by cache resources
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_BYTESUSEDFORCACHEITEMS_SUM =
      "aws.googleapis.com/ElastiCache/BytesUsedForCacheItems/Sum";
  // Space required for Redis cache
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_BYTESUSEDFORCACHE_AVERAGE =
      "aws.googleapis.com/ElastiCache/BytesUsedForCache/Average";
  // Space required for Redis cache
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_BYTESUSEDFORCACHE_MAXIMUM =
      "aws.googleapis.com/ElastiCache/BytesUsedForCache/Maximum";
  // Space required for Redis cache
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_BYTESUSEDFORCACHE_MINIMUM =
      "aws.googleapis.com/ElastiCache/BytesUsedForCache/Minimum";
  // Space required for Redis cache
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_BYTESUSEDFORCACHE_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/BytesUsedForCache/SampleCount";
  // Space required for Redis cache
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_BYTESUSEDFORCACHE_SUM =
      "aws.googleapis.com/ElastiCache/BytesUsedForCache/Sum";
  // Space taken in bytes by hash tables
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_BYTESUSEDFORHASH_AVERAGE =
      "aws.googleapis.com/ElastiCache/BytesUsedForHash/Average";
  // Space taken in bytes by hash tables
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_BYTESUSEDFORHASH_MAXIMUM =
      "aws.googleapis.com/ElastiCache/BytesUsedForHash/Maximum";
  // Space taken in bytes by hash tables
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_BYTESUSEDFORHASH_MINIMUM =
      "aws.googleapis.com/ElastiCache/BytesUsedForHash/Minimum";
  // Space taken in bytes by hash tables
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_BYTESUSEDFORHASH_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/BytesUsedForHash/SampleCount";
  // Space taken in bytes by hash tables
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_BYTESUSEDFORHASH_SUM =
      "aws.googleapis.com/ElastiCache/BytesUsedForHash/Sum";
  // Data written by Memcached
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_BYTESWRITTENOUTFROMMEMCACHED_AVERAGE =
      "aws.googleapis.com/ElastiCache/BytesWrittenOutFromMemcached/Average";
  // Data written by Memcached
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_BYTESWRITTENOUTFROMMEMCACHED_MAXIMUM =
      "aws.googleapis.com/ElastiCache/BytesWrittenOutFromMemcached/Maximum";
  // Data written by Memcached
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_BYTESWRITTENOUTFROMMEMCACHED_MINIMUM =
      "aws.googleapis.com/ElastiCache/BytesWrittenOutFromMemcached/Minimum";
  // Data written by Memcached
  public static final String
      AWS_GOOGLEAPIS_COM_ELASTICACHE_BYTESWRITTENOUTFROMMEMCACHED_SAMPLECOUNT =
          "aws.googleapis.com/ElastiCache/BytesWrittenOutFromMemcached/SampleCount";
  // Data written by Memcached
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_BYTESWRITTENOUTFROMMEMCACHED_SUM =
      "aws.googleapis.com/ElastiCache/BytesWrittenOutFromMemcached/Sum";
  // Cache hits
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CACHEHITS_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/CacheHits/SampleCount";
  // Cache hits
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CACHEHITS_SUM =
      "aws.googleapis.com/ElastiCache/CacheHits/Sum";
  // Cache misses
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CACHEMISSES_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/CacheMisses/SampleCount";
  // Cache misses
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CACHEMISSES_SUM =
      "aws.googleapis.com/ElastiCache/CacheMisses/Sum";
  // The number of check-and-set requests received where the check failed.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CASBADVAL_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/CasBadval/SampleCount";
  // The number of check-and-set requests received where the check failed.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CASBADVAL_SUM =
      "aws.googleapis.com/ElastiCache/CasBadval/Sum";
  // The number of check-and-set requests received where the check was successful.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CASHITS_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/CasHits/SampleCount";
  // The number of check-and-set requests received where the check was successful.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CASHITS_SUM =
      "aws.googleapis.com/ElastiCache/CasHits/Sum";
  // The number of check-and-set requests received where the key was missing.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CASMISSES_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/CasMisses/SampleCount";
  // The number of check-and-set requests received where the key was missing.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CASMISSES_SUM =
      "aws.googleapis.com/ElastiCache/CasMisses/Sum";
  // Count of 'config get' calls
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CMDCONFIGGET_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/CmdConfigGet/SampleCount";
  // Count of 'config get' calls
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CMDCONFIGGET_SUM =
      "aws.googleapis.com/ElastiCache/CmdConfigGet/Sum";
  // Count of 'config set' calls
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CMDCONFIGSET_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/CmdConfigSet/SampleCount";
  // Count of 'config set' calls
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CMDCONFIGSET_SUM =
      "aws.googleapis.com/ElastiCache/CmdConfigSet/Sum";
  // Count of flush calls
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CMDFLUSH_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/CmdFlush/SampleCount";
  // Count of flush calls
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CMDFLUSH_SUM =
      "aws.googleapis.com/ElastiCache/CmdFlush/Sum";
  // Count of get calls
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CMDGET_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/CmdGet/SampleCount";
  // Count of get calls
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CMDGET_SUM =
      "aws.googleapis.com/ElastiCache/CmdGet/Sum";
  // Count of set calls
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CMDSET_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/CmdSet/SampleCount";
  // Count of set calls
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CMDSET_SUM =
      "aws.googleapis.com/ElastiCache/CmdSet/Sum";
  // Count of 'touch' calls
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CMDTOUCH_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/CmdTouch/SampleCount";
  // Count of 'touch' calls
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CMDTOUCH_SUM =
      "aws.googleapis.com/ElastiCache/CmdTouch/Sum";
  // CPU used by the cache node
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CPUUTILIZATION_AVERAGE =
      "aws.googleapis.com/ElastiCache/CPUUtilization/Average";
  // CPU used by the cache node
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CPUUTILIZATION_MAXIMUM =
      "aws.googleapis.com/ElastiCache/CPUUtilization/Maximum";
  // CPU used by the cache node
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CPUUTILIZATION_MINIMUM =
      "aws.googleapis.com/ElastiCache/CPUUtilization/Minimum";
  // CPU used by the cache node
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CPUUTILIZATION_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/CPUUtilization/SampleCount";
  // CPU used by the cache node
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CPUUTILIZATION_SUM =
      "aws.googleapis.com/ElastiCache/CPUUtilization/Sum";
  // Count of current configs
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CURRCONFIG_AVERAGE =
      "aws.googleapis.com/ElastiCache/CurrConfig/Average";
  // Count of current configs
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CURRCONFIG_MAXIMUM =
      "aws.googleapis.com/ElastiCache/CurrConfig/Maximum";
  // Count of current configs
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CURRCONFIG_MINIMUM =
      "aws.googleapis.com/ElastiCache/CurrConfig/Minimum";
  // Count of current configs
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CURRCONFIG_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/CurrConfig/SampleCount";
  // Count of current configs
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CURRCONFIG_SUM =
      "aws.googleapis.com/ElastiCache/CurrConfig/Sum";
  // Active connections
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CURRCONNECTIONS_AVERAGE =
      "aws.googleapis.com/ElastiCache/CurrConnections/Average";
  // Active connections
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CURRCONNECTIONS_MAXIMUM =
      "aws.googleapis.com/ElastiCache/CurrConnections/Maximum";
  // Active connections
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CURRCONNECTIONS_MINIMUM =
      "aws.googleapis.com/ElastiCache/CurrConnections/Minimum";
  // Active connections
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CURRCONNECTIONS_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/CurrConnections/SampleCount";
  // Active connections
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CURRCONNECTIONS_SUM =
      "aws.googleapis.com/ElastiCache/CurrConnections/Sum";
  // Items cached
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CURRITEMS_AVERAGE =
      "aws.googleapis.com/ElastiCache/CurrItems/Average";
  // Items cached
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CURRITEMS_MAXIMUM =
      "aws.googleapis.com/ElastiCache/CurrItems/Maximum";
  // Items cached
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CURRITEMS_MINIMUM =
      "aws.googleapis.com/ElastiCache/CurrItems/Minimum";
  // Items cached
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CURRITEMS_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/CurrItems/SampleCount";
  // Items cached
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_CURRITEMS_SUM =
      "aws.googleapis.com/ElastiCache/CurrItems/Sum";
  // Successful decrement requests
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_DECRHITS_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/DecrHits/SampleCount";
  // Successful decrement requests
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_DECRHITS_SUM =
      "aws.googleapis.com/ElastiCache/DecrHits/Sum";
  // Unsuccessful decrement requests
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_DECRMISSES_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/DecrMisses/SampleCount";
  // Unsuccessful decrement requests
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_DECRMISSES_SUM =
      "aws.googleapis.com/ElastiCache/DecrMisses/Sum";
  // Successful delete requests
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_DELETEHITS_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/DeleteHits/SampleCount";
  // Successful delete requests
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_DELETEHITS_SUM =
      "aws.googleapis.com/ElastiCache/DeleteHits/Sum";
  // Unsuccessful delete requests
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_DELETEMISSES_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/DeleteMisses/SampleCount";
  // Unsuccessful delete requests
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_DELETEMISSES_SUM =
      "aws.googleapis.com/ElastiCache/DeleteMisses/Sum";
  // Untouched LRU cache evictions
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_EVICTEDUNFETCHED_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/EvictedUnfetched/SampleCount";
  // Untouched LRU cache evictions
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_EVICTEDUNFETCHED_SUM =
      "aws.googleapis.com/ElastiCache/EvictedUnfetched/Sum";
  // Cache evictions
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_EVICTIONS_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/Evictions/SampleCount";
  // Cache evictions
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_EVICTIONS_SUM =
      "aws.googleapis.com/ElastiCache/Evictions/Sum";
  // Untouched LRU cache expirations
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_EXPIREDUNFETCHED_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/ExpiredUnfetched/SampleCount";
  // Untouched LRU cache expirations
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_EXPIREDUNFETCHED_SUM =
      "aws.googleapis.com/ElastiCache/ExpiredUnfetched/Sum";
  // Cache node's unused memory
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_FREEABLEMEMORY_AVERAGE =
      "aws.googleapis.com/ElastiCache/FreeableMemory/Average";
  // Cache node's unused memory
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_FREEABLEMEMORY_MAXIMUM =
      "aws.googleapis.com/ElastiCache/FreeableMemory/Maximum";
  // Cache node's unused memory
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_FREEABLEMEMORY_MINIMUM =
      "aws.googleapis.com/ElastiCache/FreeableMemory/Minimum";
  // Cache node's unused memory
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_FREEABLEMEMORY_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/FreeableMemory/SampleCount";
  // Cache node's unused memory
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_FREEABLEMEMORY_SUM =
      "aws.googleapis.com/ElastiCache/FreeableMemory/Sum";
  // Successful get requests
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_GETHITS_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/GetHits/SampleCount";
  // Successful get requests
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_GETHITS_SUM =
      "aws.googleapis.com/ElastiCache/GetHits/Sum";
  // Unsuccesful get requests
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_GETMISSES_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/GetMisses/SampleCount";
  // Unsuccesful get requests
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_GETMISSES_SUM =
      "aws.googleapis.com/ElastiCache/GetMisses/Sum";
  // Total get type commands issued
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_GETTYPECMDS_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/GetTypeCmds/SampleCount";
  // Total get type commands issued
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_GETTYPECMDS_SUM =
      "aws.googleapis.com/ElastiCache/GetTypeCmds/Sum";
  // Total number of hash commands issued.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_HASHBASEDCMDS_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/HashBasedCmds/SampleCount";
  // Total number of hash commands issued.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_HASHBASEDCMDS_SUM =
      "aws.googleapis.com/ElastiCache/HashBasedCmds/Sum";
  // Successful increment requests
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_INCRHITS_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/IncrHits/SampleCount";
  // Successful increment requests
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_INCRHITS_SUM =
      "aws.googleapis.com/ElastiCache/IncrHits/Sum";
  // Unsuccesful increment requests
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_INCRMISSES_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/IncrMisses/SampleCount";
  // Unsuccesful increment requests
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_INCRMISSES_SUM =
      "aws.googleapis.com/ElastiCache/IncrMisses/Sum";
  // Total number of key commands issued.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_KEYBASEDCMDS_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/KeyBasedCmds/SampleCount";
  // Total number of key commands issued.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_KEYBASEDCMDS_SUM =
      "aws.googleapis.com/ElastiCache/KeyBasedCmds/Sum";
  // Total number of list commands issued.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_LISTBASEDCMDS_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/ListBasedCmds/SampleCount";
  // Total number of list commands issued.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_LISTBASEDCMDS_SUM =
      "aws.googleapis.com/ElastiCache/ListBasedCmds/Sum";
  // Network data read by the cache node
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_NETWORKBYTESIN_AVERAGE =
      "aws.googleapis.com/ElastiCache/NetworkBytesIn/Average";
  // Network data read by the cache node
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_NETWORKBYTESIN_MAXIMUM =
      "aws.googleapis.com/ElastiCache/NetworkBytesIn/Maximum";
  // Network data read by the cache node
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_NETWORKBYTESIN_MINIMUM =
      "aws.googleapis.com/ElastiCache/NetworkBytesIn/Minimum";
  // Network data read by the cache node
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_NETWORKBYTESIN_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/NetworkBytesIn/SampleCount";
  // Network data read by the cache node
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_NETWORKBYTESIN_SUM =
      "aws.googleapis.com/ElastiCache/NetworkBytesIn/Sum";
  // Network data written by the cache node
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_NETWORKBYTESOUT_AVERAGE =
      "aws.googleapis.com/ElastiCache/NetworkBytesOut/Average";
  // Network data written by the cache node
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_NETWORKBYTESOUT_MAXIMUM =
      "aws.googleapis.com/ElastiCache/NetworkBytesOut/Maximum";
  // Network data written by the cache node
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_NETWORKBYTESOUT_MINIMUM =
      "aws.googleapis.com/ElastiCache/NetworkBytesOut/Minimum";
  // Network data written by the cache node
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_NETWORKBYTESOUT_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/NetworkBytesOut/SampleCount";
  // Network data written by the cache node
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_NETWORKBYTESOUT_SUM =
      "aws.googleapis.com/ElastiCache/NetworkBytesOut/Sum";
  // Additional connections received
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_NEWCONNECTIONS_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/NewConnections/SampleCount";
  // Additional connections received
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_NEWCONNECTIONS_SUM =
      "aws.googleapis.com/ElastiCache/NewConnections/Sum";
  // Additional items saved
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_NEWITEMS_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/NewItems/SampleCount";
  // Additional items saved
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_NEWITEMS_SUM =
      "aws.googleapis.com/ElastiCache/NewItems/Sum";
  // Expired items removed from the cache
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_RECLAIMED_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/Reclaimed/SampleCount";
  // Expired items removed from the cache
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_RECLAIMED_SUM =
      "aws.googleapis.com/ElastiCache/Reclaimed/Sum";
  // Read-replica only. The number of seconds behind the primary cluster.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_REPLICATIONLAG_AVERAGE =
      "aws.googleapis.com/ElastiCache/ReplicationLag/Average";
  // Read-replica only. The number of seconds behind the primary cluster.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_REPLICATIONLAG_MAXIMUM =
      "aws.googleapis.com/ElastiCache/ReplicationLag/Maximum";
  // Read-replica only. The number of seconds behind the primary cluster.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_REPLICATIONLAG_MINIMUM =
      "aws.googleapis.com/ElastiCache/ReplicationLag/Minimum";
  // Read-replica only. The number of seconds behind the primary cluster.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_REPLICATIONLAG_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/ReplicationLag/SampleCount";
  // Read-replica only. The number of seconds behind the primary cluster.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_REPLICATIONLAG_SUM =
      "aws.googleapis.com/ElastiCache/ReplicationLag/Sum";
  // Total number of set commands issued.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_SETBASEDCMDS_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/SetBasedCmds/SampleCount";
  // Total number of set commands issued.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_SETBASEDCMDS_SUM =
      "aws.googleapis.com/ElastiCache/SetBasedCmds/Sum";
  // Total set type commands issued
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_SETTYPECMDS_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/SetTypeCmds/SampleCount";
  // Total set type commands issued
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_SETTYPECMDS_SUM =
      "aws.googleapis.com/ElastiCache/SetTypeCmds/Sum";
  // Transferred slabs
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_SLABSMOVED_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/SlabsMoved/SampleCount";
  // Transferred slabs
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_SLABSMOVED_SUM =
      "aws.googleapis.com/ElastiCache/SlabsMoved/Sum";
  // Total number of sorted set commands issued.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_SORTEDSETBASEDCMDS_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/SortedSetBasedCmds/SampleCount";
  // Total number of sorted set commands issued.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_SORTEDSETBASEDCMDS_SUM =
      "aws.googleapis.com/ElastiCache/SortedSetBasedCmds/Sum";
  // Total number of string commands issued.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_STRINGBASEDCMDS_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/StringBasedCmds/SampleCount";
  // Total number of string commands issued.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_STRINGBASEDCMDS_SUM =
      "aws.googleapis.com/ElastiCache/StringBasedCmds/Sum";
  // The bytes of swap in use by the cache node.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_SWAPUSAGE_AVERAGE =
      "aws.googleapis.com/ElastiCache/SwapUsage/Average";
  // The bytes of swap in use by the cache node.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_SWAPUSAGE_MAXIMUM =
      "aws.googleapis.com/ElastiCache/SwapUsage/Maximum";
  // The bytes of swap in use by the cache node.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_SWAPUSAGE_MINIMUM =
      "aws.googleapis.com/ElastiCache/SwapUsage/Minimum";
  // The bytes of swap in use by the cache node.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_SWAPUSAGE_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/SwapUsage/SampleCount";
  // The bytes of swap in use by the cache node.
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_SWAPUSAGE_SUM =
      "aws.googleapis.com/ElastiCache/SwapUsage/Sum";
  // Successful touches
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_TOUCHHITS_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/TouchHits/SampleCount";
  // Successful touches
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_TOUCHHITS_SUM =
      "aws.googleapis.com/ElastiCache/TouchHits/Sum";
  // Failed touches
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_TOUCHMISSES_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/TouchMisses/SampleCount";
  // Failed touches
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_TOUCHMISSES_SUM =
      "aws.googleapis.com/ElastiCache/TouchMisses/Sum";
  // Memory available for storage operations
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_UNUSEDMEMORY_AVERAGE =
      "aws.googleapis.com/ElastiCache/UnusedMemory/Average";
  // Memory available for storage operations
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_UNUSEDMEMORY_MAXIMUM =
      "aws.googleapis.com/ElastiCache/UnusedMemory/Maximum";
  // Memory available for storage operations
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_UNUSEDMEMORY_MINIMUM =
      "aws.googleapis.com/ElastiCache/UnusedMemory/Minimum";
  // Memory available for storage operations
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_UNUSEDMEMORY_SAMPLECOUNT =
      "aws.googleapis.com/ElastiCache/UnusedMemory/SampleCount";
  // Memory available for storage operations
  public static final String AWS_GOOGLEAPIS_COM_ELASTICACHE_UNUSEDMEMORY_SUM =
      "aws.googleapis.com/ElastiCache/UnusedMemory/Sum";
  // The count of failed connections between the ELB and its back-end instances.
  public static final String AWS_GOOGLEAPIS_COM_ELB_BACKENDCONNECTIONERRORS_SAMPLECOUNT =
      "aws.googleapis.com/ELB/BackendConnectionErrors/SampleCount";
  // The count of failed connections between the ELB and its back-end instances.
  public static final String AWS_GOOGLEAPIS_COM_ELB_BACKENDCONNECTIONERRORS_SUM =
      "aws.googleapis.com/ELB/BackendConnectionErrors/Sum";
  // The count of the number of healthy instances in the balancer.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HEALTHYHOSTCOUNT_AVERAGE =
      "aws.googleapis.com/ELB/HealthyHostCount/Average";
  // The count of the number of healthy instances in the balancer.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HEALTHYHOSTCOUNT_MAXIMUM =
      "aws.googleapis.com/ELB/HealthyHostCount/Maximum";
  // The count of the number of healthy instances in the balancer.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HEALTHYHOSTCOUNT_MINIMUM =
      "aws.googleapis.com/ELB/HealthyHostCount/Minimum";
  // The count of the number of healthy instances in the balancer.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HEALTHYHOSTCOUNT_SAMPLECOUNT =
      "aws.googleapis.com/ELB/HealthyHostCount/SampleCount";
  // The count of the number of healthy instances in the balancer.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HEALTHYHOSTCOUNT_SUM =
      "aws.googleapis.com/ELB/HealthyHostCount/Sum";
  // Count of the hosts in service.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HOSTSINSERVICE_AVERAGE =
      "aws.googleapis.com/ELB/HostsInService/Average";
  // Count of the hosts in service.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HOSTSINSERVICE_MAXIMUM =
      "aws.googleapis.com/ELB/HostsInService/Maximum";
  // Count of the hosts in service.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HOSTSINSERVICE_MINIMUM =
      "aws.googleapis.com/ELB/HostsInService/Minimum";
  // Count of the hosts in service.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HOSTSINSERVICE_SAMPLECOUNT =
      "aws.googleapis.com/ELB/HostsInService/SampleCount";
  // Count of the hosts in service.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HOSTSINSERVICE_SUM =
      "aws.googleapis.com/ELB/HostsInService/Sum";
  // Count of the hosts out of service.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HOSTSOUTOFSERVICE_AVERAGE =
      "aws.googleapis.com/ELB/HostsOutOfService/Average";
  // Count of the hosts out of service.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HOSTSOUTOFSERVICE_MAXIMUM =
      "aws.googleapis.com/ELB/HostsOutOfService/Maximum";
  // Count of the hosts out of service.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HOSTSOUTOFSERVICE_MINIMUM =
      "aws.googleapis.com/ELB/HostsOutOfService/Minimum";
  // Count of the hosts out of service.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HOSTSOUTOFSERVICE_SAMPLECOUNT =
      "aws.googleapis.com/ELB/HostsOutOfService/SampleCount";
  // Count of the hosts out of service.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HOSTSOUTOFSERVICE_SUM =
      "aws.googleapis.com/ELB/HostsOutOfService/Sum";
  // The count of 2XX (HTTP) back-end instance error codes.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HTTPCODE_BACKEND_2XX_SAMPLECOUNT =
      "aws.googleapis.com/ELB/HTTPCode_Backend_2XX/SampleCount";
  // The count of 2XX (HTTP) back-end instance error codes.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HTTPCODE_BACKEND_2XX_SUM =
      "aws.googleapis.com/ELB/HTTPCode_Backend_2XX/Sum";
  // The count of 3XX (HTTP) back-end instance error codes.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HTTPCODE_BACKEND_3XX_SAMPLECOUNT =
      "aws.googleapis.com/ELB/HTTPCode_Backend_3XX/SampleCount";
  // The count of 3XX (HTTP) back-end instance error codes.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HTTPCODE_BACKEND_3XX_SUM =
      "aws.googleapis.com/ELB/HTTPCode_Backend_3XX/Sum";
  // The count of 4XX (HTTP) back-end instance error codes.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HTTPCODE_BACKEND_4XX_SAMPLECOUNT =
      "aws.googleapis.com/ELB/HTTPCode_Backend_4XX/SampleCount";
  // The count of 4XX (HTTP) back-end instance error codes.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HTTPCODE_BACKEND_4XX_SUM =
      "aws.googleapis.com/ELB/HTTPCode_Backend_4XX/Sum";
  // The count of 5XX (HTTP) back-end instance error codes.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HTTPCODE_BACKEND_5XX_SAMPLECOUNT =
      "aws.googleapis.com/ELB/HTTPCode_Backend_5XX/SampleCount";
  // The count of 5XX (HTTP) back-end instance error codes.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HTTPCODE_BACKEND_5XX_SUM =
      "aws.googleapis.com/ELB/HTTPCode_Backend_5XX/Sum";
  // The count of 4XX (HTTP) ELB error codes.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HTTPCODE_ELB_4XX_SAMPLECOUNT =
      "aws.googleapis.com/ELB/HTTPCode_ELB_4XX/SampleCount";
  // The count of 4XX (HTTP) ELB error codes.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HTTPCODE_ELB_4XX_SUM =
      "aws.googleapis.com/ELB/HTTPCode_ELB_4XX/Sum";
  // The count of 5XX (HTTP) ELB error codes.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HTTPCODE_ELB_5XX_SAMPLECOUNT =
      "aws.googleapis.com/ELB/HTTPCode_ELB_5XX/SampleCount";
  // The count of 5XX (HTTP) ELB error codes.
  public static final String AWS_GOOGLEAPIS_COM_ELB_HTTPCODE_ELB_5XX_SUM =
      "aws.googleapis.com/ELB/HTTPCode_ELB_5XX/Sum";
  // Time between when the request leaves the load balancer and a response is returned.
  public static final String AWS_GOOGLEAPIS_COM_ELB_LATENCY_AVERAGE =
      "aws.googleapis.com/ELB/Latency/Average";
  // Time between when the request leaves the load balancer and a response is returned.
  public static final String AWS_GOOGLEAPIS_COM_ELB_LATENCY_MAXIMUM =
      "aws.googleapis.com/ELB/Latency/Maximum";
  // Time between when the request leaves the load balancer and a response is returned.
  public static final String AWS_GOOGLEAPIS_COM_ELB_LATENCY_MINIMUM =
      "aws.googleapis.com/ELB/Latency/Minimum";
  // Time between when the request leaves the load balancer and a response is returned.
  public static final String AWS_GOOGLEAPIS_COM_ELB_LATENCY_SAMPLECOUNT =
      "aws.googleapis.com/ELB/Latency/SampleCount";
  // Time between when the request leaves the load balancer and a response is returned.
  public static final String AWS_GOOGLEAPIS_COM_ELB_LATENCY_SUM =
      "aws.googleapis.com/ELB/Latency/Sum";
  // Count of requests successfully transmitted to back-end instances.
  public static final String AWS_GOOGLEAPIS_COM_ELB_REQUESTCOUNT_SAMPLECOUNT =
      "aws.googleapis.com/ELB/RequestCount/SampleCount";
  // Count of requests successfully transmitted to back-end instances.
  public static final String AWS_GOOGLEAPIS_COM_ELB_REQUESTCOUNT_SUM =
      "aws.googleapis.com/ELB/RequestCount/Sum";
  // The count of requests refused once the queue reaches its limit.
  public static final String AWS_GOOGLEAPIS_COM_ELB_SPILLOVERCOUNT_SAMPLECOUNT =
      "aws.googleapis.com/ELB/SpilloverCount/SampleCount";
  // The count of requests refused once the queue reaches its limit.
  public static final String AWS_GOOGLEAPIS_COM_ELB_SPILLOVERCOUNT_SUM =
      "aws.googleapis.com/ELB/SpilloverCount/Sum";
  // The count of queued requests waiting to be sent to back-end instances.
  public static final String AWS_GOOGLEAPIS_COM_ELB_SURGEQUEUELENGTH_MAXIMUM =
      "aws.googleapis.com/ELB/SurgeQueueLength/Maximum";
  // The count of the number of unhealthy instances in the balancer.
  public static final String AWS_GOOGLEAPIS_COM_ELB_UNHEALTHYHOSTCOUNT_AVERAGE =
      "aws.googleapis.com/ELB/UnHealthyHostCount/Average";
  // The count of the number of unhealthy instances in the balancer.
  public static final String AWS_GOOGLEAPIS_COM_ELB_UNHEALTHYHOSTCOUNT_MAXIMUM =
      "aws.googleapis.com/ELB/UnHealthyHostCount/Maximum";
  // The count of the number of unhealthy instances in the balancer.
  public static final String AWS_GOOGLEAPIS_COM_ELB_UNHEALTHYHOSTCOUNT_MINIMUM =
      "aws.googleapis.com/ELB/UnHealthyHostCount/Minimum";
  // The count of the number of unhealthy instances in the balancer.
  public static final String AWS_GOOGLEAPIS_COM_ELB_UNHEALTHYHOSTCOUNT_SAMPLECOUNT =
      "aws.googleapis.com/ELB/UnHealthyHostCount/SampleCount";
  // The count of the number of unhealthy instances in the balancer.
  public static final String AWS_GOOGLEAPIS_COM_ELB_UNHEALTHYHOSTCOUNT_SUM =
      "aws.googleapis.com/ELB/UnHealthyHostCount/Sum";
  // Number of bytes read from the stream using GetRecord.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_GETRECORDSBYTES_SAMPLECOUNT =
      "aws.googleapis.com/Kinesis/GetRecordsBytes/SampleCount";
  // Number of bytes read from the stream using GetRecord.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_GETRECORDSBYTES_SUM =
      "aws.googleapis.com/Kinesis/GetRecordsBytes/Sum";
  // For a stream, time passed between last GetRecords call and current time.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_GETRECORDSITERATORAGEMILLISECONDS_AVERAGE =
      "aws.googleapis.com/Kinesis/GetRecordsIteratorAgeMilliseconds/Average";
  // For a stream, time passed between last GetRecords call and current time.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_GETRECORDSITERATORAGEMILLISECONDS_MAXIMUM =
      "aws.googleapis.com/Kinesis/GetRecordsIteratorAgeMilliseconds/Maximum";
  // For a stream, time passed between last GetRecords call and current time.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_GETRECORDSITERATORAGEMILLISECONDS_MINIMUM =
      "aws.googleapis.com/Kinesis/GetRecordsIteratorAgeMilliseconds/Minimum";
  // For a stream, time passed between last GetRecords call and current time.
  public static final String
      AWS_GOOGLEAPIS_COM_KINESIS_GETRECORDSITERATORAGEMILLISECONDS_SAMPLECOUNT =
          "aws.googleapis.com/Kinesis/GetRecordsIteratorAgeMilliseconds/SampleCount";
  // For a stream, time passed between last GetRecords call and current time.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_GETRECORDSITERATORAGEMILLISECONDS_SUM =
      "aws.googleapis.com/Kinesis/GetRecordsIteratorAgeMilliseconds/Sum";
  // Total time required by GetRecords operations.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_GETRECORDSLATENCY_AVERAGE =
      "aws.googleapis.com/Kinesis/GetRecordsLatency/Average";
  // Total time required by GetRecords operations.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_GETRECORDSLATENCY_MAXIMUM =
      "aws.googleapis.com/Kinesis/GetRecordsLatency/Maximum";
  // Total time required by GetRecords operations.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_GETRECORDSLATENCY_MINIMUM =
      "aws.googleapis.com/Kinesis/GetRecordsLatency/Minimum";
  // Total time required by GetRecords operations.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_GETRECORDSLATENCY_SAMPLECOUNT =
      "aws.googleapis.com/Kinesis/GetRecordsLatency/SampleCount";
  // Total time required by GetRecords operations.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_GETRECORDSLATENCY_SUM =
      "aws.googleapis.com/Kinesis/GetRecordsLatency/Sum";
  // Completed GetRecords operations.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_GETRECORDSSUCCESS_SAMPLECOUNT =
      "aws.googleapis.com/Kinesis/GetRecordsSuccess/SampleCount";
  // Completed GetRecords operations.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_GETRECORDSSUCCESS_SUM =
      "aws.googleapis.com/Kinesis/GetRecordsSuccess/Sum";
  // Total number of bytes sent to the stream with PutRecord and PutRecords.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_INCOMINGBYTES_SAMPLECOUNT =
      "aws.googleapis.com/Kinesis/IncomingBytes/SampleCount";
  // Total number of bytes sent to the stream with PutRecord and PutRecords.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_INCOMINGBYTES_SUM =
      "aws.googleapis.com/Kinesis/IncomingBytes/Sum";
  // Number of records put across PutRecord and PutRecords.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_INCOMINGRECORDS_SAMPLECOUNT =
      "aws.googleapis.com/Kinesis/IncomingRecords/SampleCount";
  // Number of records put across PutRecord and PutRecords.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_INCOMINGRECORDS_SUM =
      "aws.googleapis.com/Kinesis/IncomingRecords/Sum";
  // Bytes sent to the stream with PutRecord.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_PUTRECORDBYTES_SAMPLECOUNT =
      "aws.googleapis.com/Kinesis/PutRecordBytes/SampleCount";
  // Bytes sent to the stream with PutRecord.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_PUTRECORDBYTES_SUM =
      "aws.googleapis.com/Kinesis/PutRecordBytes/Sum";
  // Total time required by PutRecord operations.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_PUTRECORDLATENCY_AVERAGE =
      "aws.googleapis.com/Kinesis/PutRecordLatency/Average";
  // Total time required by PutRecord operations.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_PUTRECORDLATENCY_MAXIMUM =
      "aws.googleapis.com/Kinesis/PutRecordLatency/Maximum";
  // Total time required by PutRecord operations.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_PUTRECORDLATENCY_MINIMUM =
      "aws.googleapis.com/Kinesis/PutRecordLatency/Minimum";
  // Total time required by PutRecord operations.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_PUTRECORDLATENCY_SAMPLECOUNT =
      "aws.googleapis.com/Kinesis/PutRecordLatency/SampleCount";
  // Total time required by PutRecord operations.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_PUTRECORDLATENCY_SUM =
      "aws.googleapis.com/Kinesis/PutRecordLatency/Sum";
  // Bytes sent to the stream with PutRecords.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_PUTRECORDSBYTES_SAMPLECOUNT =
      "aws.googleapis.com/Kinesis/PutRecordsBytes/SampleCount";
  // Bytes sent to the stream with PutRecords.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_PUTRECORDSBYTES_SUM =
      "aws.googleapis.com/Kinesis/PutRecordsBytes/Sum";
  // Total time required by PutRecords operations.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_PUTRECORDSLATENCY_AVERAGE =
      "aws.googleapis.com/Kinesis/PutRecordsLatency/Average";
  // Total time required by PutRecords operations.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_PUTRECORDSLATENCY_MAXIMUM =
      "aws.googleapis.com/Kinesis/PutRecordsLatency/Maximum";
  // Total time required by PutRecords operations.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_PUTRECORDSLATENCY_MINIMUM =
      "aws.googleapis.com/Kinesis/PutRecordsLatency/Minimum";
  // Total time required by PutRecords operations.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_PUTRECORDSLATENCY_SAMPLECOUNT =
      "aws.googleapis.com/Kinesis/PutRecordsLatency/SampleCount";
  // Total time required by PutRecords operations.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_PUTRECORDSLATENCY_SUM =
      "aws.googleapis.com/Kinesis/PutRecordsLatency/Sum";
  // Count of records included in a PutRecords operations.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_PUTRECORDSRECORDS_SAMPLECOUNT =
      "aws.googleapis.com/Kinesis/PutRecordsRecords/SampleCount";
  // Count of records included in a PutRecords operations.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_PUTRECORDSRECORDS_SUM =
      "aws.googleapis.com/Kinesis/PutRecordsRecords/Sum";
  // Completed PutRecords operations.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_PUTRECORDSSUCCESS_SAMPLECOUNT =
      "aws.googleapis.com/Kinesis/PutRecordsSuccess/SampleCount";
  // Completed PutRecords operations.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_PUTRECORDSSUCCESS_SUM =
      "aws.googleapis.com/Kinesis/PutRecordsSuccess/Sum";
  // Completed PutRecord operations.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_PUTRECORDSUCCESS_SAMPLECOUNT =
      "aws.googleapis.com/Kinesis/PutRecordSuccess/SampleCount";
  // Completed PutRecord operations.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_PUTRECORDSUCCESS_SUM =
      "aws.googleapis.com/Kinesis/PutRecordSuccess/Sum";
  // Count of the shards.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_SHARDCOUNT_AVERAGE =
      "aws.googleapis.com/Kinesis/ShardCount/Average";
  // Count of the shards.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_SHARDCOUNT_MAXIMUM =
      "aws.googleapis.com/Kinesis/ShardCount/Maximum";
  // Count of the shards.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_SHARDCOUNT_MINIMUM =
      "aws.googleapis.com/Kinesis/ShardCount/Minimum";
  // Count of the shards.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_SHARDCOUNT_SAMPLECOUNT =
      "aws.googleapis.com/Kinesis/ShardCount/SampleCount";
  // Count of the shards.
  public static final String AWS_GOOGLEAPIS_COM_KINESIS_SHARDCOUNT_SUM =
      "aws.googleapis.com/Kinesis/ShardCount/Sum";
  // Elapsed time taken by an invocation of the function.
  public static final String AWS_GOOGLEAPIS_COM_LAMBDA_DURATION_AVERAGE =
      "aws.googleapis.com/Lambda/Duration/Average";
  // Elapsed time taken by an invocation of the function.
  public static final String AWS_GOOGLEAPIS_COM_LAMBDA_DURATION_MAXIMUM =
      "aws.googleapis.com/Lambda/Duration/Maximum";
  // Elapsed time taken by an invocation of the function.
  public static final String AWS_GOOGLEAPIS_COM_LAMBDA_DURATION_MINIMUM =
      "aws.googleapis.com/Lambda/Duration/Minimum";
  // Elapsed time taken by an invocation of the function.
  public static final String AWS_GOOGLEAPIS_COM_LAMBDA_DURATION_SAMPLECOUNT =
      "aws.googleapis.com/Lambda/Duration/SampleCount";
  // Elapsed time taken by an invocation of the function.
  public static final String AWS_GOOGLEAPIS_COM_LAMBDA_DURATION_SUM =
      "aws.googleapis.com/Lambda/Duration/Sum";
  // Count of failed operations.
  public static final String AWS_GOOGLEAPIS_COM_LAMBDA_ERRORS_SAMPLECOUNT =
      "aws.googleapis.com/Lambda/Errors/SampleCount";
  // Count of failed operations.
  public static final String AWS_GOOGLEAPIS_COM_LAMBDA_ERRORS_SUM =
      "aws.googleapis.com/Lambda/Errors/Sum";
  // Number of times the function was called in the period.
  public static final String AWS_GOOGLEAPIS_COM_LAMBDA_INVOCATIONS_SAMPLECOUNT =
      "aws.googleapis.com/Lambda/Invocations/SampleCount";
  // Number of times the function was called in the period.
  public static final String AWS_GOOGLEAPIS_COM_LAMBDA_INVOCATIONS_SUM =
      "aws.googleapis.com/Lambda/Invocations/Sum";
  // Count of functions that exceeded the invocation limit and were subsequently throttled.
  public static final String AWS_GOOGLEAPIS_COM_LAMBDA_THROTTLES_SAMPLECOUNT =
      "aws.googleapis.com/Lambda/Throttles/SampleCount";
  // Count of functions that exceeded the invocation limit and were subsequently throttled.
  public static final String AWS_GOOGLEAPIS_COM_LAMBDA_THROTTLES_SUM =
      "aws.googleapis.com/Lambda/Throttles/Sum";
  // The percentage of disk used.
  public static final String AWS_GOOGLEAPIS_COM_RDS_AVERAGEDISKUSAGE_AVERAGE =
      "aws.googleapis.com/RDS/AverageDiskUsage/Average";
  // The percentage of disk used.
  public static final String AWS_GOOGLEAPIS_COM_RDS_AVERAGEDISKUSAGE_MAXIMUM =
      "aws.googleapis.com/RDS/AverageDiskUsage/Maximum";
  // The percentage of disk used.
  public static final String AWS_GOOGLEAPIS_COM_RDS_AVERAGEDISKUSAGE_MINIMUM =
      "aws.googleapis.com/RDS/AverageDiskUsage/Minimum";
  // The percentage of disk used.
  public static final String AWS_GOOGLEAPIS_COM_RDS_AVERAGEDISKUSAGE_SAMPLECOUNT =
      "aws.googleapis.com/RDS/AverageDiskUsage/SampleCount";
  // The percentage of disk used.
  public static final String AWS_GOOGLEAPIS_COM_RDS_AVERAGEDISKUSAGE_SUM =
      "aws.googleapis.com/RDS/AverageDiskUsage/Sum";
  // The percentage of memory used.
  public static final String AWS_GOOGLEAPIS_COM_RDS_AVERAGEMEMORYUSAGE_AVERAGE =
      "aws.googleapis.com/RDS/AverageMemoryUsage/Average";
  // The percentage of memory used.
  public static final String AWS_GOOGLEAPIS_COM_RDS_AVERAGEMEMORYUSAGE_MAXIMUM =
      "aws.googleapis.com/RDS/AverageMemoryUsage/Maximum";
  // The percentage of memory used.
  public static final String AWS_GOOGLEAPIS_COM_RDS_AVERAGEMEMORYUSAGE_MINIMUM =
      "aws.googleapis.com/RDS/AverageMemoryUsage/Minimum";
  // The percentage of memory used.
  public static final String AWS_GOOGLEAPIS_COM_RDS_AVERAGEMEMORYUSAGE_SAMPLECOUNT =
      "aws.googleapis.com/RDS/AverageMemoryUsage/SampleCount";
  // The percentage of memory used.
  public static final String AWS_GOOGLEAPIS_COM_RDS_AVERAGEMEMORYUSAGE_SUM =
      "aws.googleapis.com/RDS/AverageMemoryUsage/Sum";
  // Bytes allocated to binary logs on the master.
  public static final String AWS_GOOGLEAPIS_COM_RDS_BINLOGDISKUSAGE_AVERAGE =
      "aws.googleapis.com/RDS/BinLogDiskUsage/Average";
  // Bytes allocated to binary logs on the master.
  public static final String AWS_GOOGLEAPIS_COM_RDS_BINLOGDISKUSAGE_MAXIMUM =
      "aws.googleapis.com/RDS/BinLogDiskUsage/Maximum";
  // Bytes allocated to binary logs on the master.
  public static final String AWS_GOOGLEAPIS_COM_RDS_BINLOGDISKUSAGE_MINIMUM =
      "aws.googleapis.com/RDS/BinLogDiskUsage/Minimum";
  // Bytes allocated to binary logs on the master.
  public static final String AWS_GOOGLEAPIS_COM_RDS_BINLOGDISKUSAGE_SAMPLECOUNT =
      "aws.googleapis.com/RDS/BinLogDiskUsage/SampleCount";
  // Bytes allocated to binary logs on the master.
  public static final String AWS_GOOGLEAPIS_COM_RDS_BINLOGDISKUSAGE_SUM =
      "aws.googleapis.com/RDS/BinLogDiskUsage/Sum";
  // Accrued CPU allottment.
  public static final String AWS_GOOGLEAPIS_COM_RDS_CPUCREDITBALANCE_AVERAGE =
      "aws.googleapis.com/RDS/CPUCreditBalance/Average";
  // Accrued CPU allottment.
  public static final String AWS_GOOGLEAPIS_COM_RDS_CPUCREDITBALANCE_MAXIMUM =
      "aws.googleapis.com/RDS/CPUCreditBalance/Maximum";
  // Accrued CPU allottment.
  public static final String AWS_GOOGLEAPIS_COM_RDS_CPUCREDITBALANCE_MINIMUM =
      "aws.googleapis.com/RDS/CPUCreditBalance/Minimum";
  // Accrued CPU allottment.
  public static final String AWS_GOOGLEAPIS_COM_RDS_CPUCREDITBALANCE_SAMPLECOUNT =
      "aws.googleapis.com/RDS/CPUCreditBalance/SampleCount";
  // Accrued CPU allottment.
  public static final String AWS_GOOGLEAPIS_COM_RDS_CPUCREDITBALANCE_SUM =
      "aws.googleapis.com/RDS/CPUCreditBalance/Sum";
  // Consumed CPU allottment.
  public static final String AWS_GOOGLEAPIS_COM_RDS_CPUCREDITUSAGE_AVERAGE =
      "aws.googleapis.com/RDS/CPUCreditUsage/Average";
  // Consumed CPU allottment.
  public static final String AWS_GOOGLEAPIS_COM_RDS_CPUCREDITUSAGE_MAXIMUM =
      "aws.googleapis.com/RDS/CPUCreditUsage/Maximum";
  // Consumed CPU allottment.
  public static final String AWS_GOOGLEAPIS_COM_RDS_CPUCREDITUSAGE_MINIMUM =
      "aws.googleapis.com/RDS/CPUCreditUsage/Minimum";
  // Consumed CPU allottment.
  public static final String AWS_GOOGLEAPIS_COM_RDS_CPUCREDITUSAGE_SAMPLECOUNT =
      "aws.googleapis.com/RDS/CPUCreditUsage/SampleCount";
  // Consumed CPU allottment.
  public static final String AWS_GOOGLEAPIS_COM_RDS_CPUCREDITUSAGE_SUM =
      "aws.googleapis.com/RDS/CPUCreditUsage/Sum";
  // CPU required for RDS processes
  public static final String AWS_GOOGLEAPIS_COM_RDS_CPUUTILIZATION_AVERAGE =
      "aws.googleapis.com/RDS/CPUUtilization/Average";
  // CPU required for RDS processes
  public static final String AWS_GOOGLEAPIS_COM_RDS_CPUUTILIZATION_MAXIMUM =
      "aws.googleapis.com/RDS/CPUUtilization/Maximum";
  // CPU required for RDS processes
  public static final String AWS_GOOGLEAPIS_COM_RDS_CPUUTILIZATION_MINIMUM =
      "aws.googleapis.com/RDS/CPUUtilization/Minimum";
  // CPU required for RDS processes
  public static final String AWS_GOOGLEAPIS_COM_RDS_CPUUTILIZATION_SAMPLECOUNT =
      "aws.googleapis.com/RDS/CPUUtilization/SampleCount";
  // CPU required for RDS processes
  public static final String AWS_GOOGLEAPIS_COM_RDS_CPUUTILIZATION_SUM =
      "aws.googleapis.com/RDS/CPUUtilization/Sum";
  // Count of connections to the database
  public static final String AWS_GOOGLEAPIS_COM_RDS_DATABASECONNECTIONS_AVERAGE =
      "aws.googleapis.com/RDS/DatabaseConnections/Average";
  // Count of connections to the database
  public static final String AWS_GOOGLEAPIS_COM_RDS_DATABASECONNECTIONS_MAXIMUM =
      "aws.googleapis.com/RDS/DatabaseConnections/Maximum";
  // Count of connections to the database
  public static final String AWS_GOOGLEAPIS_COM_RDS_DATABASECONNECTIONS_MINIMUM =
      "aws.googleapis.com/RDS/DatabaseConnections/Minimum";
  // Count of connections to the database
  public static final String AWS_GOOGLEAPIS_COM_RDS_DATABASECONNECTIONS_SAMPLECOUNT =
      "aws.googleapis.com/RDS/DatabaseConnections/SampleCount";
  // Count of connections to the database
  public static final String AWS_GOOGLEAPIS_COM_RDS_DATABASECONNECTIONS_SUM =
      "aws.googleapis.com/RDS/DatabaseConnections/Sum";
  // IO requests queued for access to the disk.
  public static final String AWS_GOOGLEAPIS_COM_RDS_DISKQUEUEDEPTH_AVERAGE =
      "aws.googleapis.com/RDS/DiskQueueDepth/Average";
  // IO requests queued for access to the disk.
  public static final String AWS_GOOGLEAPIS_COM_RDS_DISKQUEUEDEPTH_MAXIMUM =
      "aws.googleapis.com/RDS/DiskQueueDepth/Maximum";
  // IO requests queued for access to the disk.
  public static final String AWS_GOOGLEAPIS_COM_RDS_DISKQUEUEDEPTH_MINIMUM =
      "aws.googleapis.com/RDS/DiskQueueDepth/Minimum";
  // IO requests queued for access to the disk.
  public static final String AWS_GOOGLEAPIS_COM_RDS_DISKQUEUEDEPTH_SAMPLECOUNT =
      "aws.googleapis.com/RDS/DiskQueueDepth/SampleCount";
  // IO requests queued for access to the disk.
  public static final String AWS_GOOGLEAPIS_COM_RDS_DISKQUEUEDEPTH_SUM =
      "aws.googleapis.com/RDS/DiskQueueDepth/Sum";
  // Available RAM.
  public static final String AWS_GOOGLEAPIS_COM_RDS_FREEABLEMEMORY_AVERAGE =
      "aws.googleapis.com/RDS/FreeableMemory/Average";
  // Available RAM.
  public static final String AWS_GOOGLEAPIS_COM_RDS_FREEABLEMEMORY_MAXIMUM =
      "aws.googleapis.com/RDS/FreeableMemory/Maximum";
  // Available RAM.
  public static final String AWS_GOOGLEAPIS_COM_RDS_FREEABLEMEMORY_MINIMUM =
      "aws.googleapis.com/RDS/FreeableMemory/Minimum";
  // Available RAM.
  public static final String AWS_GOOGLEAPIS_COM_RDS_FREEABLEMEMORY_SAMPLECOUNT =
      "aws.googleapis.com/RDS/FreeableMemory/SampleCount";
  // Available RAM.
  public static final String AWS_GOOGLEAPIS_COM_RDS_FREEABLEMEMORY_SUM =
      "aws.googleapis.com/RDS/FreeableMemory/Sum";
  // Available storage.
  public static final String AWS_GOOGLEAPIS_COM_RDS_FREESTORAGESPACE_AVERAGE =
      "aws.googleapis.com/RDS/FreeStorageSpace/Average";
  // Available storage.
  public static final String AWS_GOOGLEAPIS_COM_RDS_FREESTORAGESPACE_MAXIMUM =
      "aws.googleapis.com/RDS/FreeStorageSpace/Maximum";
  // Available storage.
  public static final String AWS_GOOGLEAPIS_COM_RDS_FREESTORAGESPACE_MINIMUM =
      "aws.googleapis.com/RDS/FreeStorageSpace/Minimum";
  // Available storage.
  public static final String AWS_GOOGLEAPIS_COM_RDS_FREESTORAGESPACE_SAMPLECOUNT =
      "aws.googleapis.com/RDS/FreeStorageSpace/SampleCount";
  // Available storage.
  public static final String AWS_GOOGLEAPIS_COM_RDS_FREESTORAGESPACE_SUM =
      "aws.googleapis.com/RDS/FreeStorageSpace/Sum";
  // Incoming network traffic, including replication traffic.
  public static final String AWS_GOOGLEAPIS_COM_RDS_NETWORKRECEIVETHROUGHPUT_AVERAGE =
      "aws.googleapis.com/RDS/NetworkReceiveThroughput/Average";
  // Incoming network traffic, including replication traffic.
  public static final String AWS_GOOGLEAPIS_COM_RDS_NETWORKRECEIVETHROUGHPUT_MAXIMUM =
      "aws.googleapis.com/RDS/NetworkReceiveThroughput/Maximum";
  // Incoming network traffic, including replication traffic.
  public static final String AWS_GOOGLEAPIS_COM_RDS_NETWORKRECEIVETHROUGHPUT_MINIMUM =
      "aws.googleapis.com/RDS/NetworkReceiveThroughput/Minimum";
  // Incoming network traffic, including replication traffic.
  public static final String AWS_GOOGLEAPIS_COM_RDS_NETWORKRECEIVETHROUGHPUT_SAMPLECOUNT =
      "aws.googleapis.com/RDS/NetworkReceiveThroughput/SampleCount";
  // Incoming network traffic, including replication traffic.
  public static final String AWS_GOOGLEAPIS_COM_RDS_NETWORKRECEIVETHROUGHPUT_SUM =
      "aws.googleapis.com/RDS/NetworkReceiveThroughput/Sum";
  // Outgoing network traffic, including replication traffic.
  public static final String AWS_GOOGLEAPIS_COM_RDS_NETWORKTRANSMITTHROUGHPUT_AVERAGE =
      "aws.googleapis.com/RDS/NetworkTransmitThroughput/Average";
  // Outgoing network traffic, including replication traffic.
  public static final String AWS_GOOGLEAPIS_COM_RDS_NETWORKTRANSMITTHROUGHPUT_MAXIMUM =
      "aws.googleapis.com/RDS/NetworkTransmitThroughput/Maximum";
  // Outgoing network traffic, including replication traffic.
  public static final String AWS_GOOGLEAPIS_COM_RDS_NETWORKTRANSMITTHROUGHPUT_MINIMUM =
      "aws.googleapis.com/RDS/NetworkTransmitThroughput/Minimum";
  // Outgoing network traffic, including replication traffic.
  public static final String AWS_GOOGLEAPIS_COM_RDS_NETWORKTRANSMITTHROUGHPUT_SAMPLECOUNT =
      "aws.googleapis.com/RDS/NetworkTransmitThroughput/SampleCount";
  // Outgoing network traffic, including replication traffic.
  public static final String AWS_GOOGLEAPIS_COM_RDS_NETWORKTRANSMITTHROUGHPUT_SUM =
      "aws.googleapis.com/RDS/NetworkTransmitThroughput/Sum";
  // Average number of read IOPS.
  public static final String AWS_GOOGLEAPIS_COM_RDS_READIOPS_AVERAGE =
      "aws.googleapis.com/RDS/ReadIOPS/Average";
  // Average number of read IOPS.
  public static final String AWS_GOOGLEAPIS_COM_RDS_READIOPS_MAXIMUM =
      "aws.googleapis.com/RDS/ReadIOPS/Maximum";
  // Average number of read IOPS.
  public static final String AWS_GOOGLEAPIS_COM_RDS_READIOPS_MINIMUM =
      "aws.googleapis.com/RDS/ReadIOPS/Minimum";
  // Average number of read IOPS.
  public static final String AWS_GOOGLEAPIS_COM_RDS_READIOPS_SAMPLECOUNT =
      "aws.googleapis.com/RDS/ReadIOPS/SampleCount";
  // Average number of read IOPS.
  public static final String AWS_GOOGLEAPIS_COM_RDS_READIOPS_SUM =
      "aws.googleapis.com/RDS/ReadIOPS/Sum";
  // Average time taken for read IOPS.
  public static final String AWS_GOOGLEAPIS_COM_RDS_READLATENCY_AVERAGE =
      "aws.googleapis.com/RDS/ReadLatency/Average";
  // Average time taken for read IOPS.
  public static final String AWS_GOOGLEAPIS_COM_RDS_READLATENCY_MAXIMUM =
      "aws.googleapis.com/RDS/ReadLatency/Maximum";
  // Average time taken for read IOPS.
  public static final String AWS_GOOGLEAPIS_COM_RDS_READLATENCY_MINIMUM =
      "aws.googleapis.com/RDS/ReadLatency/Minimum";
  // Average time taken for read IOPS.
  public static final String AWS_GOOGLEAPIS_COM_RDS_READLATENCY_SAMPLECOUNT =
      "aws.googleapis.com/RDS/ReadLatency/SampleCount";
  // Average time taken for read IOPS.
  public static final String AWS_GOOGLEAPIS_COM_RDS_READLATENCY_SUM =
      "aws.googleapis.com/RDS/ReadLatency/Sum";
  // Bytes/second read from disk.
  public static final String AWS_GOOGLEAPIS_COM_RDS_READTHROUGHPUT_AVERAGE =
      "aws.googleapis.com/RDS/ReadThroughput/Average";
  // Bytes/second read from disk.
  public static final String AWS_GOOGLEAPIS_COM_RDS_READTHROUGHPUT_MAXIMUM =
      "aws.googleapis.com/RDS/ReadThroughput/Maximum";
  // Bytes/second read from disk.
  public static final String AWS_GOOGLEAPIS_COM_RDS_READTHROUGHPUT_MINIMUM =
      "aws.googleapis.com/RDS/ReadThroughput/Minimum";
  // Bytes/second read from disk.
  public static final String AWS_GOOGLEAPIS_COM_RDS_READTHROUGHPUT_SAMPLECOUNT =
      "aws.googleapis.com/RDS/ReadThroughput/SampleCount";
  // Bytes/second read from disk.
  public static final String AWS_GOOGLEAPIS_COM_RDS_READTHROUGHPUT_SUM =
      "aws.googleapis.com/RDS/ReadThroughput/Sum";
  // Replication lag (seconds behind master).
  public static final String AWS_GOOGLEAPIS_COM_RDS_REPLICALAG_AVERAGE =
      "aws.googleapis.com/RDS/ReplicaLag/Average";
  // Replication lag (seconds behind master).
  public static final String AWS_GOOGLEAPIS_COM_RDS_REPLICALAG_MAXIMUM =
      "aws.googleapis.com/RDS/ReplicaLag/Maximum";
  // Replication lag (seconds behind master).
  public static final String AWS_GOOGLEAPIS_COM_RDS_REPLICALAG_MINIMUM =
      "aws.googleapis.com/RDS/ReplicaLag/Minimum";
  // Replication lag (seconds behind master).
  public static final String AWS_GOOGLEAPIS_COM_RDS_REPLICALAG_SAMPLECOUNT =
      "aws.googleapis.com/RDS/ReplicaLag/SampleCount";
  // Replication lag (seconds behind master).
  public static final String AWS_GOOGLEAPIS_COM_RDS_REPLICALAG_SUM =
      "aws.googleapis.com/RDS/ReplicaLag/Sum";
  // Swap space used.
  public static final String AWS_GOOGLEAPIS_COM_RDS_SWAPUSAGE_AVERAGE =
      "aws.googleapis.com/RDS/SwapUsage/Average";
  // Swap space used.
  public static final String AWS_GOOGLEAPIS_COM_RDS_SWAPUSAGE_MAXIMUM =
      "aws.googleapis.com/RDS/SwapUsage/Maximum";
  // Swap space used.
  public static final String AWS_GOOGLEAPIS_COM_RDS_SWAPUSAGE_MINIMUM =
      "aws.googleapis.com/RDS/SwapUsage/Minimum";
  // Swap space used.
  public static final String AWS_GOOGLEAPIS_COM_RDS_SWAPUSAGE_SAMPLECOUNT =
      "aws.googleapis.com/RDS/SwapUsage/SampleCount";
  // Swap space used.
  public static final String AWS_GOOGLEAPIS_COM_RDS_SWAPUSAGE_SUM =
      "aws.googleapis.com/RDS/SwapUsage/Sum";
  // Average number of write IOPS.
  public static final String AWS_GOOGLEAPIS_COM_RDS_WRITEIOPS_AVERAGE =
      "aws.googleapis.com/RDS/WriteIOPS/Average";
  // Average number of write IOPS.
  public static final String AWS_GOOGLEAPIS_COM_RDS_WRITEIOPS_MAXIMUM =
      "aws.googleapis.com/RDS/WriteIOPS/Maximum";
  // Average number of write IOPS.
  public static final String AWS_GOOGLEAPIS_COM_RDS_WRITEIOPS_MINIMUM =
      "aws.googleapis.com/RDS/WriteIOPS/Minimum";
  // Average number of write IOPS.
  public static final String AWS_GOOGLEAPIS_COM_RDS_WRITEIOPS_SAMPLECOUNT =
      "aws.googleapis.com/RDS/WriteIOPS/SampleCount";
  // Average number of write IOPS.
  public static final String AWS_GOOGLEAPIS_COM_RDS_WRITEIOPS_SUM =
      "aws.googleapis.com/RDS/WriteIOPS/Sum";
  // Average time taken for write IOPS.
  public static final String AWS_GOOGLEAPIS_COM_RDS_WRITELATENCY_AVERAGE =
      "aws.googleapis.com/RDS/WriteLatency/Average";
  // Average time taken for write IOPS.
  public static final String AWS_GOOGLEAPIS_COM_RDS_WRITELATENCY_MAXIMUM =
      "aws.googleapis.com/RDS/WriteLatency/Maximum";
  // Average time taken for write IOPS.
  public static final String AWS_GOOGLEAPIS_COM_RDS_WRITELATENCY_MINIMUM =
      "aws.googleapis.com/RDS/WriteLatency/Minimum";
  // Average time taken for write IOPS.
  public static final String AWS_GOOGLEAPIS_COM_RDS_WRITELATENCY_SAMPLECOUNT =
      "aws.googleapis.com/RDS/WriteLatency/SampleCount";
  // Average time taken for write IOPS.
  public static final String AWS_GOOGLEAPIS_COM_RDS_WRITELATENCY_SUM =
      "aws.googleapis.com/RDS/WriteLatency/Sum";
  // Bytes/second written to disk.
  public static final String AWS_GOOGLEAPIS_COM_RDS_WRITETHROUGHPUT_AVERAGE =
      "aws.googleapis.com/RDS/WriteThroughput/Average";
  // Bytes/second written to disk.
  public static final String AWS_GOOGLEAPIS_COM_RDS_WRITETHROUGHPUT_MAXIMUM =
      "aws.googleapis.com/RDS/WriteThroughput/Maximum";
  // Bytes/second written to disk.
  public static final String AWS_GOOGLEAPIS_COM_RDS_WRITETHROUGHPUT_MINIMUM =
      "aws.googleapis.com/RDS/WriteThroughput/Minimum";
  // Bytes/second written to disk.
  public static final String AWS_GOOGLEAPIS_COM_RDS_WRITETHROUGHPUT_SAMPLECOUNT =
      "aws.googleapis.com/RDS/WriteThroughput/SampleCount";
  // Bytes/second written to disk.
  public static final String AWS_GOOGLEAPIS_COM_RDS_WRITETHROUGHPUT_SUM =
      "aws.googleapis.com/RDS/WriteThroughput/Sum";
  // CPU utilized by the cluster.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_CPUUTILIZATION_AVERAGE =
      "aws.googleapis.com/Redshift/CPUUtilization/Average";
  // CPU utilized by the cluster.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_CPUUTILIZATION_MAXIMUM =
      "aws.googleapis.com/Redshift/CPUUtilization/Maximum";
  // CPU utilized by the cluster.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_CPUUTILIZATION_MINIMUM =
      "aws.googleapis.com/Redshift/CPUUtilization/Minimum";
  // CPU utilized by the cluster.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_CPUUTILIZATION_SAMPLECOUNT =
      "aws.googleapis.com/Redshift/CPUUtilization/SampleCount";
  // CPU utilized by the cluster.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_CPUUTILIZATION_SUM =
      "aws.googleapis.com/Redshift/CPUUtilization/Sum";
  // Count of total database connections.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_DATABASECONNECTIONS_AVERAGE =
      "aws.googleapis.com/Redshift/DatabaseConnections/Average";
  // Count of total database connections.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_DATABASECONNECTIONS_MAXIMUM =
      "aws.googleapis.com/Redshift/DatabaseConnections/Maximum";
  // Count of total database connections.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_DATABASECONNECTIONS_MINIMUM =
      "aws.googleapis.com/Redshift/DatabaseConnections/Minimum";
  // Count of total database connections.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_DATABASECONNECTIONS_SAMPLECOUNT =
      "aws.googleapis.com/Redshift/DatabaseConnections/SampleCount";
  // Count of total database connections.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_DATABASECONNECTIONS_SUM =
      "aws.googleapis.com/Redshift/DatabaseConnections/Sum";
  // Boolean that represents the status of the cluster and its database.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_HEALTHSTATUS_SAMPLECOUNT =
      "aws.googleapis.com/Redshift/HealthStatus/SampleCount";
  // Boolean that represents the status of the cluster and its database.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_HEALTHSTATUS_SUM =
      "aws.googleapis.com/Redshift/HealthStatus/Sum";
  // Boolean indicating the maintenance mode status of the cluster.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_MAINTENANCEMODE_SAMPLECOUNT =
      "aws.googleapis.com/Redshift/MaintenanceMode/SampleCount";
  // Boolean indicating the maintenance mode status of the cluster.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_MAINTENANCEMODE_SUM =
      "aws.googleapis.com/Redshift/MaintenanceMode/Sum";
  // Data receive rate.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_NETWORKRECEIVETHROUGHPUT_AVERAGE =
      "aws.googleapis.com/Redshift/NetworkReceiveThroughput/Average";
  // Data receive rate.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_NETWORKRECEIVETHROUGHPUT_MAXIMUM =
      "aws.googleapis.com/Redshift/NetworkReceiveThroughput/Maximum";
  // Data receive rate.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_NETWORKRECEIVETHROUGHPUT_MINIMUM =
      "aws.googleapis.com/Redshift/NetworkReceiveThroughput/Minimum";
  // Data receive rate.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_NETWORKRECEIVETHROUGHPUT_SAMPLECOUNT =
      "aws.googleapis.com/Redshift/NetworkReceiveThroughput/SampleCount";
  // Data receive rate.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_NETWORKRECEIVETHROUGHPUT_SUM =
      "aws.googleapis.com/Redshift/NetworkReceiveThroughput/Sum";
  // Data write rate.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_NETWORKTRANSMITTHROUGHPUT_AVERAGE =
      "aws.googleapis.com/Redshift/NetworkTransmitThroughput/Average";
  // Data write rate.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_NETWORKTRANSMITTHROUGHPUT_MAXIMUM =
      "aws.googleapis.com/Redshift/NetworkTransmitThroughput/Maximum";
  // Data write rate.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_NETWORKTRANSMITTHROUGHPUT_MINIMUM =
      "aws.googleapis.com/Redshift/NetworkTransmitThroughput/Minimum";
  // Data write rate.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_NETWORKTRANSMITTHROUGHPUT_SAMPLECOUNT =
      "aws.googleapis.com/Redshift/NetworkTransmitThroughput/SampleCount";
  // Data write rate.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_NETWORKTRANSMITTHROUGHPUT_SUM =
      "aws.googleapis.com/Redshift/NetworkTransmitThroughput/Sum";
  // Utilized disk space.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_PERCENTAGEDISKSPACEUSED_AVERAGE =
      "aws.googleapis.com/Redshift/PercentageDiskSpaceUsed/Average";
  // Utilized disk space.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_PERCENTAGEDISKSPACEUSED_MAXIMUM =
      "aws.googleapis.com/Redshift/PercentageDiskSpaceUsed/Maximum";
  // Utilized disk space.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_PERCENTAGEDISKSPACEUSED_MINIMUM =
      "aws.googleapis.com/Redshift/PercentageDiskSpaceUsed/Minimum";
  // Utilized disk space.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_PERCENTAGEDISKSPACEUSED_SAMPLECOUNT =
      "aws.googleapis.com/Redshift/PercentageDiskSpaceUsed/SampleCount";
  // Utilized disk space.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_PERCENTAGEDISKSPACEUSED_SUM =
      "aws.googleapis.com/Redshift/PercentageDiskSpaceUsed/Sum";
  // Reads per second.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_READIOPS_AVERAGE =
      "aws.googleapis.com/Redshift/ReadIOPS/Average";
  // Reads per second.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_READIOPS_MAXIMUM =
      "aws.googleapis.com/Redshift/ReadIOPS/Maximum";
  // Reads per second.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_READIOPS_MINIMUM =
      "aws.googleapis.com/Redshift/ReadIOPS/Minimum";
  // Reads per second.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_READIOPS_SAMPLECOUNT =
      "aws.googleapis.com/Redshift/ReadIOPS/SampleCount";
  // Reads per second.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_READIOPS_SUM =
      "aws.googleapis.com/Redshift/ReadIOPS/Sum";
  // The average disk I/O read operation latency.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_READLATENCY_AVERAGE =
      "aws.googleapis.com/Redshift/ReadLatency/Average";
  // The average disk I/O read operation latency.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_READLATENCY_MAXIMUM =
      "aws.googleapis.com/Redshift/ReadLatency/Maximum";
  // The average disk I/O read operation latency.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_READLATENCY_MINIMUM =
      "aws.googleapis.com/Redshift/ReadLatency/Minimum";
  // The average disk I/O read operation latency.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_READLATENCY_SAMPLECOUNT =
      "aws.googleapis.com/Redshift/ReadLatency/SampleCount";
  // The average disk I/O read operation latency.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_READLATENCY_SUM =
      "aws.googleapis.com/Redshift/ReadLatency/Sum";
  // The average disk I/O read throughput.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_READTHROUGHPUT_AVERAGE =
      "aws.googleapis.com/Redshift/ReadThroughput/Average";
  // The average disk I/O read throughput.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_READTHROUGHPUT_MAXIMUM =
      "aws.googleapis.com/Redshift/ReadThroughput/Maximum";
  // The average disk I/O read throughput.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_READTHROUGHPUT_MINIMUM =
      "aws.googleapis.com/Redshift/ReadThroughput/Minimum";
  // The average disk I/O read throughput.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_READTHROUGHPUT_SAMPLECOUNT =
      "aws.googleapis.com/Redshift/ReadThroughput/SampleCount";
  // The average disk I/O read throughput.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_READTHROUGHPUT_SUM =
      "aws.googleapis.com/Redshift/ReadThroughput/Sum";
  // Writes per second.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_WRITEIOPS_AVERAGE =
      "aws.googleapis.com/Redshift/WriteIOPS/Average";
  // Writes per second.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_WRITEIOPS_MAXIMUM =
      "aws.googleapis.com/Redshift/WriteIOPS/Maximum";
  // Writes per second.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_WRITEIOPS_MINIMUM =
      "aws.googleapis.com/Redshift/WriteIOPS/Minimum";
  // Writes per second.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_WRITEIOPS_SAMPLECOUNT =
      "aws.googleapis.com/Redshift/WriteIOPS/SampleCount";
  // Writes per second.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_WRITEIOPS_SUM =
      "aws.googleapis.com/Redshift/WriteIOPS/Sum";
  // The average disk I/O write operation latency.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_WRITELATENCY_AVERAGE =
      "aws.googleapis.com/Redshift/WriteLatency/Average";
  // The average disk I/O write operation latency.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_WRITELATENCY_MAXIMUM =
      "aws.googleapis.com/Redshift/WriteLatency/Maximum";
  // The average disk I/O write operation latency.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_WRITELATENCY_MINIMUM =
      "aws.googleapis.com/Redshift/WriteLatency/Minimum";
  // The average disk I/O write operation latency.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_WRITELATENCY_SAMPLECOUNT =
      "aws.googleapis.com/Redshift/WriteLatency/SampleCount";
  // The average disk I/O write operation latency.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_WRITELATENCY_SUM =
      "aws.googleapis.com/Redshift/WriteLatency/Sum";
  // The average disk I/O write throughput.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_WRITETHROUGHPUT_AVERAGE =
      "aws.googleapis.com/Redshift/WriteThroughput/Average";
  // The average disk I/O write throughput.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_WRITETHROUGHPUT_MAXIMUM =
      "aws.googleapis.com/Redshift/WriteThroughput/Maximum";
  // The average disk I/O write throughput.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_WRITETHROUGHPUT_MINIMUM =
      "aws.googleapis.com/Redshift/WriteThroughput/Minimum";
  // The average disk I/O write throughput.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_WRITETHROUGHPUT_SAMPLECOUNT =
      "aws.googleapis.com/Redshift/WriteThroughput/SampleCount";
  // The average disk I/O write throughput.
  public static final String AWS_GOOGLEAPIS_COM_REDSHIFT_WRITETHROUGHPUT_SUM =
      "aws.googleapis.com/Redshift/WriteThroughput/Sum";
  // Number of bytes currently in bucket.
  public static final String AWS_GOOGLEAPIS_COM_S3_BUCKETSIZEBYTES_SAMPLECOUNT =
      "aws.googleapis.com/S3/BucketSizeBytes/SampleCount";
  // Number of bytes currently in bucket.
  public static final String AWS_GOOGLEAPIS_COM_S3_BUCKETSIZEBYTES_SUM =
      "aws.googleapis.com/S3/BucketSizeBytes/Sum";
  // Objects in bucket.
  public static final String AWS_GOOGLEAPIS_COM_S3_NUMBEROFOBJECTS_SAMPLECOUNT =
      "aws.googleapis.com/S3/NumberOfObjects/SampleCount";
  // Objects in bucket.
  public static final String AWS_GOOGLEAPIS_COM_S3_NUMBEROFOBJECTS_SUM =
      "aws.googleapis.com/S3/NumberOfObjects/Sum";
  // The number of bounced emails in the past 15 minutes.
  public static final String AWS_GOOGLEAPIS_COM_SES_BOUNCES = "aws.googleapis.com/SES/Bounces";
  // The number of email complaints in the past 15 minutes.
  public static final String AWS_GOOGLEAPIS_COM_SES_COMPLAINTS =
      "aws.googleapis.com/SES/Complaints";
  // The number of emails sent in the past 15 minutes.
  public static final String AWS_GOOGLEAPIS_COM_SES_DELIVERYATTEMPTS =
      "aws.googleapis.com/SES/DeliveryAttempts";
  // The maximum number of emails that you can send over the course of 24 hours.
  public static final String AWS_GOOGLEAPIS_COM_SES_MAX24HOURSEND =
      "aws.googleapis.com/SES/Max24HourSend";
  // The maximum number of emails that you can send per second.
  public static final String AWS_GOOGLEAPIS_COM_SES_MAXSENDRATE =
      "aws.googleapis.com/SES/MaxSendRate";
  // The number of rejected emails in the past 15 minutes.
  public static final String AWS_GOOGLEAPIS_COM_SES_REJECTS = "aws.googleapis.com/SES/Rejects";
  // Sent emails in the last 24 hours.
  public static final String AWS_GOOGLEAPIS_COM_SES_SENTLAST24HOURS =
      "aws.googleapis.com/SES/SentLast24Hours";
  // Messages published to the SNS topic.
  public static final String AWS_GOOGLEAPIS_COM_SNS_NUMBEROFMESSAGESPUBLISHED_SAMPLECOUNT =
      "aws.googleapis.com/SNS/NumberOfMessagesPublished/SampleCount";
  // Messages published to the SNS topic.
  public static final String AWS_GOOGLEAPIS_COM_SNS_NUMBEROFMESSAGESPUBLISHED_SUM =
      "aws.googleapis.com/SNS/NumberOfMessagesPublished/Sum";
  // Notifications successfully sent to the SNS topic.
  public static final String AWS_GOOGLEAPIS_COM_SNS_NUMBEROFNOTIFICATIONSDELIVERED_SAMPLECOUNT =
      "aws.googleapis.com/SNS/NumberOfNotificationsDelivered/SampleCount";
  // Notifications successfully sent to the SNS topic.
  public static final String AWS_GOOGLEAPIS_COM_SNS_NUMBEROFNOTIFICATIONSDELIVERED_SUM =
      "aws.googleapis.com/SNS/NumberOfNotificationsDelivered/Sum";
  // Notifications unable to be sent to the SNS topic.
  public static final String AWS_GOOGLEAPIS_COM_SNS_NUMBEROFNOTIFICATIONSFAILED_SAMPLECOUNT =
      "aws.googleapis.com/SNS/NumberOfNotificationsFailed/SampleCount";
  // Notifications unable to be sent to the SNS topic.
  public static final String AWS_GOOGLEAPIS_COM_SNS_NUMBEROFNOTIFICATIONSFAILED_SUM =
      "aws.googleapis.com/SNS/NumberOfNotificationsFailed/Sum";
  // Average message size published to the SNS topic.
  public static final String AWS_GOOGLEAPIS_COM_SNS_PUBLISHSIZE_AVERAGE =
      "aws.googleapis.com/SNS/PublishSize/Average";
  // Average message size published to the SNS topic.
  public static final String AWS_GOOGLEAPIS_COM_SNS_PUBLISHSIZE_MAXIMUM =
      "aws.googleapis.com/SNS/PublishSize/Maximum";
  // Average message size published to the SNS topic.
  public static final String AWS_GOOGLEAPIS_COM_SNS_PUBLISHSIZE_MINIMUM =
      "aws.googleapis.com/SNS/PublishSize/Minimum";
  // Average message size published to the SNS topic.
  public static final String AWS_GOOGLEAPIS_COM_SNS_PUBLISHSIZE_SAMPLECOUNT =
      "aws.googleapis.com/SNS/PublishSize/SampleCount";
  // Average message size published to the SNS topic.
  public static final String AWS_GOOGLEAPIS_COM_SNS_PUBLISHSIZE_SUM =
      "aws.googleapis.com/SNS/PublishSize/Sum";
  // Delayed and unavailable messages.
  public static final String AWS_GOOGLEAPIS_COM_SQS_APPROXIMATENUMBEROFMESSAGESDELAYED_AVERAGE =
      "aws.googleapis.com/SQS/ApproximateNumberOfMessagesDelayed/Average";
  // Delayed and unavailable messages.
  public static final String AWS_GOOGLEAPIS_COM_SQS_APPROXIMATENUMBEROFMESSAGESDELAYED_MAXIMUM =
      "aws.googleapis.com/SQS/ApproximateNumberOfMessagesDelayed/Maximum";
  // Delayed and unavailable messages.
  public static final String AWS_GOOGLEAPIS_COM_SQS_APPROXIMATENUMBEROFMESSAGESDELAYED_MINIMUM =
      "aws.googleapis.com/SQS/ApproximateNumberOfMessagesDelayed/Minimum";
  // Delayed and unavailable messages.
  public static final String AWS_GOOGLEAPIS_COM_SQS_APPROXIMATENUMBEROFMESSAGESDELAYED_SAMPLECOUNT =
      "aws.googleapis.com/SQS/ApproximateNumberOfMessagesDelayed/SampleCount";
  // Delayed and unavailable messages.
  public static final String AWS_GOOGLEAPIS_COM_SQS_APPROXIMATENUMBEROFMESSAGESDELAYED_SUM =
      "aws.googleapis.com/SQS/ApproximateNumberOfMessagesDelayed/Sum";
  // Messages currently in transit.
  public static final String AWS_GOOGLEAPIS_COM_SQS_APPROXIMATENUMBEROFMESSAGESNOTVISIBLE_AVERAGE =
      "aws.googleapis.com/SQS/ApproximateNumberOfMessagesNotVisible/Average";
  // Messages currently in transit.
  public static final String AWS_GOOGLEAPIS_COM_SQS_APPROXIMATENUMBEROFMESSAGESNOTVISIBLE_MAXIMUM =
      "aws.googleapis.com/SQS/ApproximateNumberOfMessagesNotVisible/Maximum";
  // Messages currently in transit.
  public static final String AWS_GOOGLEAPIS_COM_SQS_APPROXIMATENUMBEROFMESSAGESNOTVISIBLE_MINIMUM =
      "aws.googleapis.com/SQS/ApproximateNumberOfMessagesNotVisible/Minimum";
  // Messages currently in transit.
  public static final String
      AWS_GOOGLEAPIS_COM_SQS_APPROXIMATENUMBEROFMESSAGESNOTVISIBLE_SAMPLECOUNT =
          "aws.googleapis.com/SQS/ApproximateNumberOfMessagesNotVisible/SampleCount";
  // Messages currently in transit.
  public static final String AWS_GOOGLEAPIS_COM_SQS_APPROXIMATENUMBEROFMESSAGESNOTVISIBLE_SUM =
      "aws.googleapis.com/SQS/ApproximateNumberOfMessagesNotVisible/Sum";
  // Available messages.
  public static final String AWS_GOOGLEAPIS_COM_SQS_APPROXIMATENUMBEROFMESSAGESVISIBLE_AVERAGE =
      "aws.googleapis.com/SQS/ApproximateNumberOfMessagesVisible/Average";
  // Available messages.
  public static final String AWS_GOOGLEAPIS_COM_SQS_APPROXIMATENUMBEROFMESSAGESVISIBLE_MAXIMUM =
      "aws.googleapis.com/SQS/ApproximateNumberOfMessagesVisible/Maximum";
  // Available messages.
  public static final String AWS_GOOGLEAPIS_COM_SQS_APPROXIMATENUMBEROFMESSAGESVISIBLE_MINIMUM =
      "aws.googleapis.com/SQS/ApproximateNumberOfMessagesVisible/Minimum";
  // Available messages.
  public static final String AWS_GOOGLEAPIS_COM_SQS_APPROXIMATENUMBEROFMESSAGESVISIBLE_SAMPLECOUNT =
      "aws.googleapis.com/SQS/ApproximateNumberOfMessagesVisible/SampleCount";
  // Available messages.
  public static final String AWS_GOOGLEAPIS_COM_SQS_APPROXIMATENUMBEROFMESSAGESVISIBLE_SUM =
      "aws.googleapis.com/SQS/ApproximateNumberOfMessagesVisible/Sum";
  // Number of blank ReceiveMessage API calls.
  public static final String AWS_GOOGLEAPIS_COM_SQS_NUMBEROFEMPTYRECEIVES_SAMPLECOUNT =
      "aws.googleapis.com/SQS/NumberOfEmptyReceives/SampleCount";
  // Number of blank ReceiveMessage API calls.
  public static final String AWS_GOOGLEAPIS_COM_SQS_NUMBEROFEMPTYRECEIVES_SUM =
      "aws.googleapis.com/SQS/NumberOfEmptyReceives/Sum";
  // Deleted messages.
  public static final String AWS_GOOGLEAPIS_COM_SQS_NUMBEROFMESSAGESDELETED_SAMPLECOUNT =
      "aws.googleapis.com/SQS/NumberOfMessagesDeleted/SampleCount";
  // Deleted messages.
  public static final String AWS_GOOGLEAPIS_COM_SQS_NUMBEROFMESSAGESDELETED_SUM =
      "aws.googleapis.com/SQS/NumberOfMessagesDeleted/Sum";
  // Messages received from ReceiveMessage API calls.
  public static final String AWS_GOOGLEAPIS_COM_SQS_NUMBEROFMESSAGESRECEIVED_SAMPLECOUNT =
      "aws.googleapis.com/SQS/NumberOfMessagesReceived/SampleCount";
  // Messages received from ReceiveMessage API calls.
  public static final String AWS_GOOGLEAPIS_COM_SQS_NUMBEROFMESSAGESRECEIVED_SUM =
      "aws.googleapis.com/SQS/NumberOfMessagesReceived/Sum";
  // Messages sent to the SQS queue.
  public static final String AWS_GOOGLEAPIS_COM_SQS_NUMBEROFMESSAGESSENT_SAMPLECOUNT =
      "aws.googleapis.com/SQS/NumberOfMessagesSent/SampleCount";
  // Messages sent to the SQS queue.
  public static final String AWS_GOOGLEAPIS_COM_SQS_NUMBEROFMESSAGESSENT_SUM =
      "aws.googleapis.com/SQS/NumberOfMessagesSent/Sum";
  // Average message size of sent messages.
  public static final String AWS_GOOGLEAPIS_COM_SQS_SENTMESSAGESIZE_AVERAGE =
      "aws.googleapis.com/SQS/SentMessageSize/Average";
  // Average message size of sent messages.
  public static final String AWS_GOOGLEAPIS_COM_SQS_SENTMESSAGESIZE_MAXIMUM =
      "aws.googleapis.com/SQS/SentMessageSize/Maximum";
  // Average message size of sent messages.
  public static final String AWS_GOOGLEAPIS_COM_SQS_SENTMESSAGESIZE_MINIMUM =
      "aws.googleapis.com/SQS/SentMessageSize/Minimum";
  // Average message size of sent messages.
  public static final String AWS_GOOGLEAPIS_COM_SQS_SENTMESSAGESIZE_SAMPLECOUNT =
      "aws.googleapis.com/SQS/SentMessageSize/SampleCount";
  // Average message size of sent messages.
  public static final String AWS_GOOGLEAPIS_COM_SQS_SENTMESSAGESIZE_SUM =
      "aws.googleapis.com/SQS/SentMessageSize/Sum";
  // In flight queries.
  public static final String BIGQUERY_GOOGLEAPIS_COM_QUERY_COUNT =
      "bigquery.googleapis.com/query/count";
  // Distribution of queries execution times.
  public static final String BIGQUERY_GOOGLEAPIS_COM_QUERY_EXECUTION_TIMES =
      "bigquery.googleapis.com/query/execution_times";
  // Scanned bytes per minute.
  public static final String BIGQUERY_GOOGLEAPIS_COM_QUERY_SCANNED_BYTES =
      "bigquery.googleapis.com/query/scanned_bytes";
  // Scanned bytes billed per minute.
  public static final String BIGQUERY_GOOGLEAPIS_COM_QUERY_SCANNED_BYTES_BILLED =
      "bigquery.googleapis.com/query/scanned_bytes_billed";
  // Number of BigQuery slots currently allocated for the project.
  public static final String BIGQUERY_GOOGLEAPIS_COM_SLOTS_ALLOCATED_FOR_PROJECT =
      "bigquery.googleapis.com/slots/allocated_for_project";
  // Number of BigQuery slots currently allocated for the reservation.
  public static final String BIGQUERY_GOOGLEAPIS_COM_SLOTS_ALLOCATED_FOR_RESERVATION =
      "bigquery.googleapis.com/slots/allocated_for_reservation";
  // Total number of BigQuery slots available for the project.
  public static final String BIGQUERY_GOOGLEAPIS_COM_SLOTS_TOTAL_AVAILABLE =
      "bigquery.googleapis.com/slots/total_available";
  // Number of bytes stored.
  public static final String BIGQUERY_GOOGLEAPIS_COM_STORAGE_STORED_BYTES =
      "bigquery.googleapis.com/storage/stored_bytes";
  // Number of tables.
  public static final String BIGQUERY_GOOGLEAPIS_COM_STORAGE_TABLE_COUNT =
      "bigquery.googleapis.com/storage/table_count";
  // Uploaded bytes per minute.
  public static final String BIGQUERY_GOOGLEAPIS_COM_STORAGE_UPLOADED_BYTES =
      "bigquery.googleapis.com/storage/uploaded_bytes";
  // Uploaded bytes billed per minute.
  public static final String BIGQUERY_GOOGLEAPIS_COM_STORAGE_UPLOADED_BYTES_BILLED =
      "bigquery.googleapis.com/storage/uploaded_bytes_billed";
  // Uploaded rows per minute.
  public static final String BIGQUERY_GOOGLEAPIS_COM_STORAGE_UPLOADED_ROW_COUNT =
      "bigquery.googleapis.com/storage/uploaded_row_count";
  // Count of function executions broken down by status.
  public static final String CLOUDFUNCTIONS_GOOGLEAPIS_COM_FUNCTION_EXECUTION_COUNT =
      "cloudfunctions.googleapis.com/function/execution_count";
  // Distribution of functions execution times in nanoseconds.
  public static final String CLOUDFUNCTIONS_GOOGLEAPIS_COM_FUNCTION_EXECUTION_TIMES =
      "cloudfunctions.googleapis.com/function/execution_times";
  // Number of cores reserved for the database.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_CPU_RESERVED_CORES =
      "cloudsql.googleapis.com/database/cpu/reserved_cores";
  // Cumulative CPU usage time in seconds.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_CPU_USAGE_TIME =
      "cloudsql.googleapis.com/database/cpu/usage_time";
  // The fraction of the reserved CPU that is currently in use.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_CPU_UTILIZATION =
      "cloudsql.googleapis.com/database/cpu/utilization";
  // Data utilization in bytes.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_DISK_BYTES_USED =
      "cloudsql.googleapis.com/database/disk/bytes_used";
  // Maximum data disk size in bytes.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_DISK_QUOTA =
      "cloudsql.googleapis.com/database/disk/quota";
  // Delta count of data disk read IO operations.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_DISK_READ_OPS_COUNT =
      "cloudsql.googleapis.com/database/disk/read_ops_count";
  // The fraction of the disk quota that is currently in use.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_DISK_UTILIZATION =
      "cloudsql.googleapis.com/database/disk/utilization";
  // Delta count of disk write IO operations.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_DISK_WRITE_OPS_COUNT =
      "cloudsql.googleapis.com/database/disk/write_ops_count";
  // Maximum RAM size in bytes.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_MEMORY_QUOTA =
      "cloudsql.googleapis.com/database/memory/quota";
  // RAM usage in bytes.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_MEMORY_USAGE =
      "cloudsql.googleapis.com/database/memory/usage";
  // The fraction of the memory quota that is currently in use.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_MEMORY_UTILIZATION =
      "cloudsql.googleapis.com/database/memory/utilization";
  // Number of unflushed pages in the InnoDB buffer pool.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_MYSQL_INNODB_BUFFER_POOL_PAGES_DIRTY =
      "cloudsql.googleapis.com/database/mysql/innodb_buffer_pool_pages_dirty";
  // Number of unused pages in the InnoDB buffer pool.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_MYSQL_INNODB_BUFFER_POOL_PAGES_FREE =
      "cloudsql.googleapis.com/database/mysql/innodb_buffer_pool_pages_free";
  // Total number of pages in the InnoDB buffer pool.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_MYSQL_INNODB_BUFFER_POOL_PAGES_TOTAL =
      "cloudsql.googleapis.com/database/mysql/innodb_buffer_pool_pages_total";
  // Delta count of InnoDB fsync() calls.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_MYSQL_INNODB_DATA_FSYNCS =
      "cloudsql.googleapis.com/database/mysql/innodb_data_fsyncs";
  // Delta count of InnoDB fsync() calls to the log file.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_MYSQL_INNODB_OS_LOG_FSYNCS =
      "cloudsql.googleapis.com/database/mysql/innodb_os_log_fsyncs";
  // Delta count of InnoDB pages read.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_MYSQL_INNODB_PAGES_READ =
      "cloudsql.googleapis.com/database/mysql/innodb_pages_read";
  // Delta count of InnoDB pages written.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_MYSQL_INNODB_PAGES_WRITTEN =
      "cloudsql.googleapis.com/database/mysql/innodb_pages_written";
  // Delta count of statements executed by the server.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_MYSQL_QUERIES =
      "cloudsql.googleapis.com/database/mysql/queries";
  // Delta count of statements executed by the server sent by the client.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_MYSQL_QUESTIONS =
      "cloudsql.googleapis.com/database/mysql/questions";
  // This is > 0 if the failover operation is available on the master instance.master.
  public static final String
      CLOUDSQL_GOOGLEAPIS_COM_DATABASE_MYSQL_REPLICATION_AVAILABLE_FOR_FAILOVER =
          "cloudsql.googleapis.com/database/mysql/replication/available_for_failover";
  // Number of seconds the read replica is behind its master (approximation).
  public static final String
      CLOUDSQL_GOOGLEAPIS_COM_DATABASE_MYSQL_REPLICATION_SECONDS_BEHIND_MASTER =
          "cloudsql.googleapis.com/database/mysql/replication/seconds_behind_master";
  // Indicates whether the I/O thread for reading the master's binary log is running. Possible
  // values are Yes, No and Connecting.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_MYSQL_REPLICATION_SLAVE_IO_RUNNING =
      "cloudsql.googleapis.com/database/mysql/replication/slave_io_running";
  // Indicates whether the SQL thread for executing events in the relay log is running.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_MYSQL_REPLICATION_SLAVE_SQL_RUNNING =
      "cloudsql.googleapis.com/database/mysql/replication/slave_sql_running";
  // Number of connections to the Cloud SQL instance.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_NETWORK_CONNECTIONS =
      "cloudsql.googleapis.com/database/network/connections";
  // Delta count of bytes sent through the network.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_NETWORK_SENT_BYTES_COUNT =
      "cloudsql.googleapis.com/database/network/sent_bytes_count";
  // The current serving state of the Cloud SQL instance. This can be one of the following.
  // RUNNABLE: The instance is running, or is ready to run when accessed. SUSPENDED: The
  // instance is not available, for example due to problems with billing. PENDING_CREATE: The
  // instance is being created. MAINTENANCE: The instance is down for maintenance.
  // UNKNOWN_STATE: The state of the instance is unknown.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_STATE =
      "cloudsql.googleapis.com/database/state";
  // Indicates if the server is up or not. On-demand instances are spun down if no connections are
  // made for a sufficient amount of time.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_UP =
      "cloudsql.googleapis.com/database/up";
  // Delta count of the time in seconds the instance has been running.
  public static final String CLOUDSQL_GOOGLEAPIS_COM_DATABASE_UPTIME =
      "cloudsql.googleapis.com/database/uptime";
  // Count of Cloud Tasks API calls.
  public static final String CLOUDTASKS_GOOGLEAPIS_COM_API_REQUEST_COUNT =
      "cloudtasks.googleapis.com/api/request_count";
  // Count of task attempts broken down by response code.
  public static final String CLOUDTASKS_GOOGLEAPIS_COM_QUEUE_TASK_ATTEMPT_COUNT =
      "cloudtasks.googleapis.com/queue/task_attempt_count";
  // Delay between each scheduled attempt time and actual attempt time.
  public static final String CLOUDTASKS_GOOGLEAPIS_COM_QUEUE_TASK_ATTEMPT_DELAYS =
      "cloudtasks.googleapis.com/queue/task_attempt_delays";
  // Delta count of incoming bytes dropped by the firewall.
  public static final String COMPUTE_GOOGLEAPIS_COM_FIREWALL_DROPPED_BYTES_COUNT =
      "compute.googleapis.com/firewall/dropped_bytes_count";
  // Delta count of incoming packets dropped by the firewall.
  public static final String COMPUTE_GOOGLEAPIS_COM_FIREWALL_DROPPED_PACKETS_COUNT =
      "compute.googleapis.com/firewall/dropped_packets_count";
  // Number of cores reserved on the host of the instance.
  public static final String COMPUTE_GOOGLEAPIS_COM_INSTANCE_CPU_RESERVED_CORES =
      "compute.googleapis.com/instance/cpu/reserved_cores";
  // Delta CPU usage for all cores, in seconds. To compute the per-core CPU utilization fraction,
  // divide this value by (end-start)*N, where end and start define this value's time
  // interval and N is `compute.googleapis.com/instance/cpu/reserved_cores` at the end of the
  // interval.
  public static final String COMPUTE_GOOGLEAPIS_COM_INSTANCE_CPU_USAGE_TIME =
      "compute.googleapis.com/instance/cpu/usage_time";
  // The fraction of the allocated CPU that is currently in use on the instance. This value can be
  // greater than 1.0 on some machine types that allow bursting.
  public static final String COMPUTE_GOOGLEAPIS_COM_INSTANCE_CPU_UTILIZATION =
      "compute.googleapis.com/instance/cpu/utilization";
  // Delta count of bytes read from disk.
  public static final String COMPUTE_GOOGLEAPIS_COM_INSTANCE_DISK_READ_BYTES_COUNT =
      "compute.googleapis.com/instance/disk/read_bytes_count";
  // Delta count of disk read IO operations.
  public static final String COMPUTE_GOOGLEAPIS_COM_INSTANCE_DISK_READ_OPS_COUNT =
      "compute.googleapis.com/instance/disk/read_ops_count";
  // Delta count of bytes in throttled read operations.
  public static final String COMPUTE_GOOGLEAPIS_COM_INSTANCE_DISK_THROTTLED_READ_BYTES_COUNT =
      "compute.googleapis.com/instance/disk/throttled_read_bytes_count";
  // Delta count of throttled read operations.
  public static final String COMPUTE_GOOGLEAPIS_COM_INSTANCE_DISK_THROTTLED_READ_OPS_COUNT =
      "compute.googleapis.com/instance/disk/throttled_read_ops_count";
  // Delta count of bytes in throttled write operations.
  public static final String COMPUTE_GOOGLEAPIS_COM_INSTANCE_DISK_THROTTLED_WRITE_BYTES_COUNT =
      "compute.googleapis.com/instance/disk/throttled_write_bytes_count";
  // Delta count of throttled write operations.
  public static final String COMPUTE_GOOGLEAPIS_COM_INSTANCE_DISK_THROTTLED_WRITE_OPS_COUNT =
      "compute.googleapis.com/instance/disk/throttled_write_ops_count";
  // Delta count of bytes written to disk.
  public static final String COMPUTE_GOOGLEAPIS_COM_INSTANCE_DISK_WRITE_BYTES_COUNT =
      "compute.googleapis.com/instance/disk/write_bytes_count";
  // Delta count of disk write IO operations.
  public static final String COMPUTE_GOOGLEAPIS_COM_INSTANCE_DISK_WRITE_OPS_COUNT =
      "compute.googleapis.com/instance/disk/write_ops_count";
  // Delta count of bytes received from network.
  public static final String COMPUTE_GOOGLEAPIS_COM_INSTANCE_NETWORK_RECEIVED_BYTES_COUNT =
      "compute.googleapis.com/instance/network/received_bytes_count";
  // Delta count of packets received from network.
  public static final String COMPUTE_GOOGLEAPIS_COM_INSTANCE_NETWORK_RECEIVED_PACKETS_COUNT =
      "compute.googleapis.com/instance/network/received_packets_count";
  // Delta count of bytes sent over network.
  public static final String COMPUTE_GOOGLEAPIS_COM_INSTANCE_NETWORK_SENT_BYTES_COUNT =
      "compute.googleapis.com/instance/network/sent_bytes_count";
  // Delta count of packets sent over network.
  public static final String COMPUTE_GOOGLEAPIS_COM_INSTANCE_NETWORK_SENT_PACKETS_COUNT =
      "compute.googleapis.com/instance/network/sent_packets_count";
  // How long the VM has been running, in seconds.
  public static final String COMPUTE_GOOGLEAPIS_COM_INSTANCE_UPTIME =
      "compute.googleapis.com/instance/uptime";
  // Number of cores of CPU reserved for the container. If no core limit is set, this will be
  // zero.
  public static final String CONTAINER_GOOGLEAPIS_COM_CONTAINER_CPU_RESERVED_CORES =
      "container.googleapis.com/container/cpu/reserved_cores";
  // Cumulative CPU usage on all cores in seconds. This number divided by the elapsed time
  // represents usage as a number of cores, regardless of any core limit that might be set.
  public static final String CONTAINER_GOOGLEAPIS_COM_CONTAINER_CPU_USAGE_TIME =
      "container.googleapis.com/container/cpu/usage_time";
  // The percentage of the allocated CPU that is currently in use on the container. If no core
  // limit is set, then this metric is not set.
  public static final String CONTAINER_GOOGLEAPIS_COM_CONTAINER_CPU_UTILIZATION =
      "container.googleapis.com/container/cpu/utilization";
  // Total number of bytes of capacity on the disk identified by label ‘device_name’.
  public static final String CONTAINER_GOOGLEAPIS_COM_CONTAINER_DISK_BYTES_TOTAL =
      "container.googleapis.com/container/disk/bytes_total";
  // Total number of bytes used on the disk identified by label ‘device_name’.
  public static final String CONTAINER_GOOGLEAPIS_COM_CONTAINER_DISK_BYTES_USED =
      "container.googleapis.com/container/disk/bytes_used";
  // Memory limit of the container in bytes.
  public static final String CONTAINER_GOOGLEAPIS_COM_CONTAINER_MEMORY_BYTES_TOTAL =
      "container.googleapis.com/container/memory/bytes_total";
  // Memory usage in bytes, broken down by type: evictable and non-evictable.
  public static final String CONTAINER_GOOGLEAPIS_COM_CONTAINER_MEMORY_BYTES_USED =
      "container.googleapis.com/container/memory/bytes_used";
  // Number of page faults, broken down by type: major and minor.
  public static final String CONTAINER_GOOGLEAPIS_COM_CONTAINER_MEMORY_PAGE_FAULT_COUNT =
      "container.googleapis.com/container/memory/page_fault_count";
  // Number of seconds since the container started.
  public static final String CONTAINER_GOOGLEAPIS_COM_CONTAINER_UPTIME =
      "container.googleapis.com/container/uptime";
  // The number of vCPUs currently being used by this Dataflow job. This is the current number of
  // workers times the number of vCPUs per worker.
  public static final String DATAFLOW_GOOGLEAPIS_COM_JOB_CURRENT_NUM_VCPUS =
      "dataflow.googleapis.com/job/current_num_vcpus";
  // Duration that the current run of this pipeline has been in the Running state so far, in
  // seconds. When a run completes, this stays at the duration of that run until the next
  // run starts.
  public static final String DATAFLOW_GOOGLEAPIS_COM_JOB_ELAPSED_TIME =
      "dataflow.googleapis.com/job/elapsed_time";
  // Number of elements added to the pcollection so far.
  public static final String DATAFLOW_GOOGLEAPIS_COM_JOB_ELEMENT_COUNT =
      "dataflow.googleapis.com/job/element_count";
  // Number of errors that happened so far.
  public static final String DATAFLOW_GOOGLEAPIS_COM_JOB_ERROR_COUNT =
      "dataflow.googleapis.com/job/error_count";
  // An estimated number of bytes added to the pcollection so far. Dataflow calculates the average
  // encoded size of elements in a pcollection and mutiplies it by the number of
  // elements.
  public static final String DATAFLOW_GOOGLEAPIS_COM_JOB_ESTIMATED_BYTE_COUNT =
      "dataflow.googleapis.com/job/estimated_byte_count";
  // Has this job failed.
  public static final String DATAFLOW_GOOGLEAPIS_COM_JOB_IS_FAILED =
      "dataflow.googleapis.com/job/is_failed";
  // Current state of this pipeline (e.g.: RUNNING, DONE, CANCELLED, FAILED, ...). Not reported
  // while the pipeline is not running.
  public static final String DATAFLOW_GOOGLEAPIS_COM_JOB_STATUS =
      "dataflow.googleapis.com/job/status";
  // The current maximum duration that an item of data has been awaiting processing, in seconds.
  public static final String DATAFLOW_GOOGLEAPIS_COM_JOB_SYSTEM_LAG =
      "dataflow.googleapis.com/job/system_lag";
  // The total GB seconds of memory allocated to this Dataflow job.
  public static final String DATAFLOW_GOOGLEAPIS_COM_JOB_TOTAL_MEMORY_USAGE_TIME =
      "dataflow.googleapis.com/job/total_memory_usage_time";
  // The total GB seconds for all persistent disk used by all workers associated with this
  // Dataflow job.
  public static final String DATAFLOW_GOOGLEAPIS_COM_JOB_TOTAL_PD_USAGE_TIME =
      "dataflow.googleapis.com/job/total_pd_usage_time";
  // The total vCPU seconds used by this Dataflow job.
  public static final String DATAFLOW_GOOGLEAPIS_COM_JOB_TOTAL_VCPU_TIME =
      "dataflow.googleapis.com/job/total_vcpu_time";
  // Count of Datastore API calls.
  public static final String DATASTORE_GOOGLEAPIS_COM_API_REQUEST_COUNT =
      "datastore.googleapis.com/api/request_count";
  // Distribution of sizes of read entities, grouped by type.
  public static final String DATASTORE_GOOGLEAPIS_COM_ENTITY_READ_SIZES =
      "datastore.googleapis.com/entity/read_sizes";
  // Distribution of sizes of written entities, grouped by op.
  public static final String DATASTORE_GOOGLEAPIS_COM_ENTITY_WRITE_SIZES =
      "datastore.googleapis.com/entity/write_sizes";
  // Count of Datastore index writes.
  public static final String DATASTORE_GOOGLEAPIS_COM_INDEX_WRITE_COUNT =
      "datastore.googleapis.com/index/write_count";
  // Percentage of io utilization
  public static final String FIREBASEDATABASE_GOOGLEAPIS_COM_IO_UTILIZATION =
      "firebasedatabase.googleapis.com/io/utilization";
  // The number of outstanding connections
  public static final String FIREBASEDATABASE_GOOGLEAPIS_COM_NETWORK_ACTIVE_CONNECTIONS =
      "firebasedatabase.googleapis.com/network/active_connections";
  // Indicates if the Firebase database has been disabled for network overages
  public static final String FIREBASEDATABASE_GOOGLEAPIS_COM_NETWORK_DISABLED_FOR_OVERAGES =
      "firebasedatabase.googleapis.com/network/disabled_for_overages";
  // The total outgoing bytes sent aggregated and reset monthly
  public static final String FIREBASEDATABASE_GOOGLEAPIS_COM_NETWORK_MONTHLY_SENT =
      "firebasedatabase.googleapis.com/network/monthly_sent";
  // The monthly network limit for the Firebase database
  public static final String FIREBASEDATABASE_GOOGLEAPIS_COM_NETWORK_MONTHLY_SENT_LIMIT =
      "firebasedatabase.googleapis.com/network/monthly_sent_limit";
  // The number of operations
  public static final String FIREBASEDATABASE_GOOGLEAPIS_COM_NETWORK_OPERATIONS_COUNT =
      "firebasedatabase.googleapis.com/network/operations_count";
  // The outgoing bandwidth usage for Firebase database
  public static final String FIREBASEDATABASE_GOOGLEAPIS_COM_NETWORK_SENT_BYTES_COUNT =
      "firebasedatabase.googleapis.com/network/sent_bytes_count";
  // Indicates if the Firebase database has been disabled for overages
  public static final String FIREBASEDATABASE_GOOGLEAPIS_COM_STATUS_DISABLED_FOR_OVERAGES =
      "firebasedatabase.googleapis.com/status/disabled_for_overages";
  // Indicates if the Firebase database has been disabled for storage overages
  public static final String FIREBASEDATABASE_GOOGLEAPIS_COM_STORAGE_DISABLED_FOR_OVERAGES =
      "firebasedatabase.googleapis.com/storage/disabled_for_overages";
  // The storage limit for the Firebase database
  public static final String FIREBASEDATABASE_GOOGLEAPIS_COM_STORAGE_LIMIT =
      "firebasedatabase.googleapis.com/storage/limit";
  // The size of the Firebase database storage.
  public static final String FIREBASEDATABASE_GOOGLEAPIS_COM_STORAGE_TOTAL_BYTES =
      "firebasedatabase.googleapis.com/storage/total_bytes";
  // The outgoing bandwidth usage for Firebase Hosting
  public static final String FIREBASEHOSTING_GOOGLEAPIS_COM_NETWORK_SENT_BYTES_COUNT =
      "firebasehosting.googleapis.com/network/sent_bytes_count";
  // The size of the Firebase Hosting storage.
  public static final String FIREBASEHOSTING_GOOGLEAPIS_COM_STORAGE_TOTAL_BYTES =
      "firebasehosting.googleapis.com/storage/total_bytes";
  // Number of bytes in all log entries ingested.
  public static final String LOGGING_GOOGLEAPIS_COM_BYTE_COUNT =
      "logging.googleapis.com/byte_count";
  // Number of log entries that did not contribute to user defined metrics.
  public static final String LOGGING_GOOGLEAPIS_COM_DROPPED_LOG_ENTRY_COUNT =
      "logging.googleapis.com/dropped_log_entry_count";
  // Number of log entries that contributed to user-defined metrics.
  public static final String LOGGING_GOOGLEAPIS_COM_LOG_ENTRY_COUNT =
      "logging.googleapis.com/log_entry_count";
  // Fraction of the allocated CPU that is currently in use.
  public static final String ML_GOOGLEAPIS_COM_TRAINING_CPU_UTILIZATION =
      "ml.googleapis.com/training/cpu/utilization";
  // Fraction of the allocated memory that is currently in use.
  public static final String ML_GOOGLEAPIS_COM_TRAINING_MEMORY_UTILIZATION =
      "ml.googleapis.com/training/memory/utilization";
  // Total byte size of the unacknowledged messages (a.k.a. backlog messages) in a subscription.
  public static final String PUBSUB_GOOGLEAPIS_COM_SUBSCRIPTION_BACKLOG_BYTES =
      "pubsub.googleapis.com/subscription/backlog_bytes";
  // Cumulative cost of operations per subscription, measured in bytes. This is used to measure
  // utilization for quotas.
  public static final String PUBSUB_GOOGLEAPIS_COM_SUBSCRIPTION_BYTE_COST =
      "pubsub.googleapis.com/subscription/byte_cost";
  // Cumulative count of configuration changes per subscription, grouped by operation type and
  // result.
  public static final String PUBSUB_GOOGLEAPIS_COM_SUBSCRIPTION_CONFIG_UPDATES_COUNT =
      "pubsub.googleapis.com/subscription/config_updates_count";
  // Cumulative count of ModifyAckDeadline message operations, grouped by result.
  public static final String
      PUBSUB_GOOGLEAPIS_COM_SUBSCRIPTION_MOD_ACK_DEADLINE_MESSAGE_OPERATION_COUNT =
          "pubsub.googleapis.com/subscription/mod_ack_deadline_message_operation_count";
  // Cumulative count of ModifyAckDeadline requests, grouped by result.
  public static final String PUBSUB_GOOGLEAPIS_COM_SUBSCRIPTION_MOD_ACK_DEADLINE_REQUEST_COUNT =
      "pubsub.googleapis.com/subscription/mod_ack_deadline_request_count";
  // Number of messages delivered to a subscription's push endpoint, but not yet acknowledged.
  public static final String PUBSUB_GOOGLEAPIS_COM_SUBSCRIPTION_NUM_OUTSTANDING_MESSAGES =
      "pubsub.googleapis.com/subscription/num_outstanding_messages";
  // Number of unacknowledged messages (a.k.a. backlog messages) in a subscription.
  public static final String PUBSUB_GOOGLEAPIS_COM_SUBSCRIPTION_NUM_UNDELIVERED_MESSAGES =
      "pubsub.googleapis.com/subscription/num_undelivered_messages";
  // Age (in seconds) of the oldest unacknowledged message (a.k.a. backlog message) in a
  // subscription.
  public static final String PUBSUB_GOOGLEAPIS_COM_SUBSCRIPTION_OLDEST_UNACKED_MESSAGE_AGE =
      "pubsub.googleapis.com/subscription/oldest_unacked_message_age";
  // Cumulative count of acknowledge message operations, grouped by result.
  public static final String PUBSUB_GOOGLEAPIS_COM_SUBSCRIPTION_PULL_ACK_MESSAGE_OPERATION_COUNT =
      "pubsub.googleapis.com/subscription/pull_ack_message_operation_count";
  // Cumulative count of acknowledge requests, grouped by result.
  public static final String PUBSUB_GOOGLEAPIS_COM_SUBSCRIPTION_PULL_ACK_REQUEST_COUNT =
      "pubsub.googleapis.com/subscription/pull_ack_request_count";
  // Cumulative count of pull message operations, grouped by result.
  public static final String PUBSUB_GOOGLEAPIS_COM_SUBSCRIPTION_PULL_MESSAGE_OPERATION_COUNT =
      "pubsub.googleapis.com/subscription/pull_message_operation_count";
  // Cumulative count of pull requests, grouped by result.
  public static final String PUBSUB_GOOGLEAPIS_COM_SUBSCRIPTION_PULL_REQUEST_COUNT =
      "pubsub.googleapis.com/subscription/pull_request_count";
  // Cumulative count of push attempts, grouped by result.
  public static final String PUBSUB_GOOGLEAPIS_COM_SUBSCRIPTION_PUSH_REQUEST_COUNT =
      "pubsub.googleapis.com/subscription/push_request_count";
  // Distribution of push request latencies (in microseconds), grouped by result.
  public static final String PUBSUB_GOOGLEAPIS_COM_SUBSCRIPTION_PUSH_REQUEST_LATENCIES =
      "pubsub.googleapis.com/subscription/push_request_latencies";
  // Cumulative count of StreamingPull acknowledge message operations, grouped by result.
  public static final String
      PUBSUB_GOOGLEAPIS_COM_SUBSCRIPTION_STREAMING_PULL_ACK_MESSAGE_OPERATION_COUNT =
          "pubsub.googleapis.com/subscription/streaming_pull_ack_message_operation_count";
  // Cumulative count of streaming pull message operations, grouped by result.
  public static final String
      PUBSUB_GOOGLEAPIS_COM_SUBSCRIPTION_STREAMING_PULL_MESSAGE_OPERATION_COUNT =
          "pubsub.googleapis.com/subscription/streaming_pull_message_operation_count";
  // Cumulative count of StreamingPull ModifyAckDeadline operations, grouped by result.
  public static final String
      PUBSUB_GOOGLEAPIS_COM_SUBSCRIPTION_STREAMING_PULL_MOD_ACK_DEADLINE_MESSAGE_OPERATION_COUNT =
          "pubsub.googleapis.com/subscription/streaming_pull_mod_ack_deadline_message_operation_count";
  // Cost of operations per topic, measured in bytes. This is used to measure utilization for
  // quotas.
  public static final String PUBSUB_GOOGLEAPIS_COM_TOPIC_BYTE_COST =
      "pubsub.googleapis.com/topic/byte_cost";
  // Cumulative count of configuration changes per topic, grouped by operation type and result.
  public static final String PUBSUB_GOOGLEAPIS_COM_TOPIC_CONFIG_UPDATES_COUNT =
      "pubsub.googleapis.com/topic/config_updates_count";
  // Distribution of publish message sizes (in bytes).
  public static final String PUBSUB_GOOGLEAPIS_COM_TOPIC_MESSAGE_SIZES =
      "pubsub.googleapis.com/topic/message_sizes";
  // Cumulative count of publish message operations, grouped by result.
  public static final String PUBSUB_GOOGLEAPIS_COM_TOPIC_SEND_MESSAGE_OPERATION_COUNT =
      "pubsub.googleapis.com/topic/send_message_operation_count";
  // Cumulative count of publish requests, grouped by result.
  public static final String PUBSUB_GOOGLEAPIS_COM_TOPIC_SEND_REQUEST_COUNT =
      "pubsub.googleapis.com/topic/send_request_count";
  // Current number of best routes received by router.
  public static final String ROUTER_GOOGLEAPIS_COM_BEST_RECEIVED_ROUTES_COUNT =
      "router.googleapis.com/best_received_routes_count";
  // Current number of routes received on a bgp session.
  public static final String ROUTER_GOOGLEAPIS_COM_BGP_RECEIVED_ROUTES_COUNT =
      "router.googleapis.com/bgp/received_routes_count";
  // Current number of routes sent on a bgp session.
  public static final String ROUTER_GOOGLEAPIS_COM_BGP_SENT_ROUTES_COUNT =
      "router.googleapis.com/bgp/sent_routes_count";
  // Number of BGP sessions on the router that are down.
  public static final String ROUTER_GOOGLEAPIS_COM_BGP_SESSIONS_DOWN_COUNT =
      "router.googleapis.com/bgp_sessions_down_count";
  // Number of BGP sessions on the router that are up.
  public static final String ROUTER_GOOGLEAPIS_COM_BGP_SESSIONS_UP_COUNT =
      "router.googleapis.com/bgp_sessions_up_count";
  // Indicator for successful bgp session establishment.
  public static final String ROUTER_GOOGLEAPIS_COM_BGP_SESSION_UP =
      "router.googleapis.com/bgp/session_up";
  // Router status, up or down
  public static final String ROUTER_GOOGLEAPIS_COM_ROUTER_UP = "router.googleapis.com/router_up";
  // Current number of routes sent by router.
  public static final String ROUTER_GOOGLEAPIS_COM_SENT_ROUTES_COUNT =
      "router.googleapis.com/sent_routes_count";
  // Uncompressed request bytes received by Cloud Spanner.
  public static final String SPANNER_GOOGLEAPIS_COM_API_RECEIVED_BYTES_COUNT =
      "spanner.googleapis.com/api/received_bytes_count";
  // Rate of Cloud Spanner API requests.
  public static final String SPANNER_GOOGLEAPIS_COM_API_REQUEST_COUNT =
      "spanner.googleapis.com/api/request_count";
  // Uncompressed response bytes sent by Cloud Spanner.
  public static final String SPANNER_GOOGLEAPIS_COM_API_SENT_BYTES_COUNT =
      "spanner.googleapis.com/api/sent_bytes_count";
  // Utilization of provisioned CPU, between 0 and 1.
  public static final String SPANNER_GOOGLEAPIS_COM_INSTANCE_CPU_UTILIZATION =
      "spanner.googleapis.com/instance/cpu/utilization";
  // Total number of nodes.
  public static final String SPANNER_GOOGLEAPIS_COM_INSTANCE_NODE_COUNT =
      "spanner.googleapis.com/instance/node_count";
  // Storage used in bytes.
  public static final String SPANNER_GOOGLEAPIS_COM_INSTANCE_STORAGE_USED_BYTES =
      "spanner.googleapis.com/instance/storage/used_bytes";
  // Delta count of API calls, grouped by the API method name and response code.
  public static final String STORAGE_GOOGLEAPIS_COM_API_REQUEST_COUNT =
      "storage.googleapis.com/api/request_count";
  // Delta count of bytes received over the network, grouped by the API method name and response
  // code.
  public static final String STORAGE_GOOGLEAPIS_COM_NETWORK_RECEIVED_BYTES_COUNT =
      "storage.googleapis.com/network/received_bytes_count";
  // Delta count of bytes sent over the network, grouped by the API method name and response code.
  public static final String STORAGE_GOOGLEAPIS_COM_NETWORK_SENT_BYTES_COUNT =
      "storage.googleapis.com/network/sent_bytes_count";
  // Ingress packets dropped for tunnel.
  public static final String VPN_GOOGLEAPIS_COM_NETWORK_DROPPED_RECEIVED_PACKETS_COUNT =
      "vpn.googleapis.com/network/dropped_received_packets_count";
  // Egress packets dropped for tunnel.
  public static final String VPN_GOOGLEAPIS_COM_NETWORK_DROPPED_SENT_PACKETS_COUNT =
      "vpn.googleapis.com/network/dropped_sent_packets_count";
  // Ingress bytes for tunnel.
  public static final String VPN_GOOGLEAPIS_COM_NETWORK_RECEIVED_BYTES_COUNT =
      "vpn.googleapis.com/network/received_bytes_count";
  // Egress bytes for tunnel.
  public static final String VPN_GOOGLEAPIS_COM_NETWORK_SENT_BYTES_COUNT =
      "vpn.googleapis.com/network/sent_bytes_count";
  // Indicates successful tunnel establishment if > 0.
  public static final String VPN_GOOGLEAPIS_COM_TUNNEL_ESTABLISHED =
      "vpn.googleapis.com/tunnel_established";
}
