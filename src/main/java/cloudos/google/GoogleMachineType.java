package cloudos.google;

import cloudos.Providers;
import cloudos.provider.ProviderMachineType;
import java.util.Date;

public class GoogleMachineType extends ProviderMachineType {

  public GoogleMachineType() {
    setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
  }

  public GoogleMachineType(String id, Providers provider, String project, String key,
      String description, String group, String region, Object object, Date includedDate,
      Boolean enabled, Double cpu, Double memory) {
    super(id, provider, project, key, description, group, region, object, includedDate, enabled,
        cpu, memory);
    setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
  }

}
