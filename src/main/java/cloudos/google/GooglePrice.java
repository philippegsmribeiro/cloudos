package cloudos.google;

import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GooglePrice {

  private String version;
  private String date;
  private Double price;
  private Double sustainedUseBase;
  private Map<String, Double> sustainedUseTiers;

}
