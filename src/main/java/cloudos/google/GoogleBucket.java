package cloudos.google;

import cloudos.Providers;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "google_bucket")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GoogleBucket {

  @Id
  private String id;
  private Providers providers;
  private String name;
  private String createdTime;
  private String location;
  private String type;

  private String project;
  private Object object;

}
