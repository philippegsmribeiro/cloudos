package cloudos.google;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface GoogleBucketRepository extends MongoRepository<GoogleBucket, String> {}
