package cloudos.google;

import cloudos.Providers;
import cloudos.models.CloudosCredential;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.Transient;

/**
 * Google credential.
 */
@Data
public class GoogleCredential extends CloudosCredential {

    private String project;

    @JsonIgnore
    private String jsonCredential;

    @JsonIgnore
    public String getJsonCredential() {
        return jsonCredential;
    }

    @JsonProperty
    public void setJsonCredential(String jsonCredential) {
        this.jsonCredential = jsonCredential;
    }

    @Transient
    private String configFilePath;

    /**
     * Default constructor.
     */
    public GoogleCredential() {
        this.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
    }

    /**
     * Constructor receiving keys.
     *
     * @param project        identifier
     * @param jsonCredential json with the credentials information
     */
    public GoogleCredential(String project, String jsonCredential) {
        this();
        this.project = project;
        this.jsonCredential = jsonCredential;
        setDecrypted(true);
    }

}
