package cloudos;

import javax.validation.Valid;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Define the main controller for the CloudOS page. This is responsible for the main functionalities
 * for the index.html page.
 *
 * @author Philippe Ribeiro
 */
// @Controller
@Log4j2
public class CloudosController extends WebMvcConfigurerAdapter {

  /* We will store the contact messages into MongoDb */
  @Autowired private ContactRepository repository;

  // /**
  // * Override the addViewControllers so the CloudosController can render the index.html page
  // *
  // * @param registry: A ViewControllerRegistry object.
  // */
  // @Override
  // public void addViewControllers(ViewControllerRegistry registry) {
  // registry.addViewController("/").setViewName("index");
  // registry.addViewController("/index").setViewName("index");
  // }

  /**
   * Define the router for the root page, which is rendered as index.html. A `Contact` object is
   * passed as an argument for the index.html contact form.
   *
   * @param model: A model, which contains a map between the objects and the elements in the
   *     website.
   * @return String: The name of the html page to be fetched.
   */
  @GetMapping("/")
  public String index(Model model) {
    model.addAttribute("contact", new Contact());
    return "index";
  }

  /**
   * Check if the Contact object is valid and does not contain errors. It if does not, save the
   * contact message into the contacts collection in mongo.
   *
   * @param contact: A Contact object, submitted from the `contact` form.
   * @param bindingResult: Contains the validity of the contact form submission.
   * @return
   */
  @PostMapping("/")
  public String contactMessage(@Valid Contact contact, BindingResult bindingResult) {
    // Store the contact info in the 'contact' collection if contact is valid
    if (!bindingResult.hasErrors()) {
      log.info(contact);
      this.repository.save(contact);
    }
    return "index";
  }
}
