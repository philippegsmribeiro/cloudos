package cloudos;

import cloudos.security.WebSecurityConfig;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = CalendarController.CALENDAR)
public class CalendarController {

  public static final String CALENDAR = WebSecurityConfig.API_PATH + "/calendar";
}
