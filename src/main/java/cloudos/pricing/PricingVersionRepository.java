package cloudos.pricing;

import cloudos.Providers;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface PricingVersionRepository extends MongoRepository<PricingVersion, String> {

  List<PricingVersion> findByProviderAndType(Providers provider, PricingType pricingType);

}
