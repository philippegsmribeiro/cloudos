package cloudos.pricing;

import cloudos.exceptions.NotImplementedException;
import cloudos.models.Instance;

public interface PricingManagement {

  /**
   * Fetch and store the fetched data into database.
   *
   * @param type of pricing
   * @throws PricingException if something goes wrong
   */
  void loadPricingData(PricingType type) throws PricingException;

  /**
   * Go to the provider and return the version of the pricing list/info.
   *
   * @return version of the info
   * @throws PricingException if something goes wrong
   */
  String getActualPricingVersion(PricingType type) throws PricingException;

  /**
   * Get the pricing info for an instance.
   *
   * @param instance to get pricing
   */
  PricingValue getPricingForInstance(Instance instance);

  PricingValue getPricingForInstanceType(String machineType, String region,
                                         Boolean spotInstance, String imageProject)
      throws NotImplementedException;

}