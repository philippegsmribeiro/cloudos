package cloudos.pricing;

import java.util.Date;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * AmazonPricingContent is used to unmarshal the information in the pricing information obtained
 * from the Amazon AWS pricing API.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AmazonPricingContent {

  private String offerTermCode;
  private String sku;
  private Date effectiveDate;
  private Map<String, PriceDimensions> priceDimensions;
  private Object termAttributes;


  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public class PriceDimensions {
    private String rateCode;
    private String description;
    private String beginRange;
    private String endRange;
    private String unit;
    private Map<String, String> pricePerUnit;

  }
}
