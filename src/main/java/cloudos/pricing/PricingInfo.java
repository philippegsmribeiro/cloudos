package cloudos.pricing;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import cloudos.Providers;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "pricing_info")
public class PricingInfo implements Serializable {

  private static final long serialVersionUID = 218342849521827474L;

  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  private String id;
  private Providers provider;
  private String name;
  private String machineType;
  private String region;
  private String image;
  private String content;
  @Indexed
  private String pricingVersion;
  private Date referenceDate;
  private Date loadingDate;

}
