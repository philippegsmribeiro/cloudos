package cloudos.pricing;

import cloudos.google.GoogleInstanceRepository;
import cloudos.google.integration.GoogleComputeIntegration;
import cloudos.google.integration.GooglePriceList;
import cloudos.google.integration.GooglePriceList.GoogleService;
import cloudos.models.Instance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GooglePricingManagement implements PricingManagement {

  @Autowired
  GoogleInstanceRepository googleInstanceRepository;
  @Autowired
  GoogleComputeIntegration googleIntegration;
  @Autowired
  GooglePriceList googlePriceList;

  @Override
  public String getActualPricingVersion(PricingType type) throws PricingException {
    try {
      return googlePriceList.getVersion();
    } catch (Exception e) {
      throw new PricingException("Error getting the version", e);
    }
  }

  @Override
  public void loadPricingData(PricingType type) throws PricingException {
    try {
      googlePriceList.loadPricing(getType(type));
    } catch (Exception e) {
      throw new PricingException("Error loading the pricing", e);
    }
  }

  /**
   * Map a PricingType with the Aws Type.
   * 
   * @param type pricing type
   * @return aws type
   */
  private GoogleService getType(PricingType type) {
    switch (type) {
      case INSTANCE:
        return GoogleService.CP_COMPUTEENGINE_VMIMAGE;
      case STORAGE:
        return GoogleService.CP_COMPUTEENGINE_STORAGE;
      default:
        return null;
    }
  }

  @Override
  public PricingValue getPricingForInstance(Instance instance) {
    return getPricingForInstanceType(instance.getMachineType(), instance.getRegion(),
        instance.isSpot(), instance.getImageProject());
  }

  /**
   * Return the pricing for the instance type on that region.
   * 
   * @param machineType type of instance
   * @param region of instance
   * @param spotInstance true if spot (PREEMPTIBLE)
   * @return pricing
   */
  @Override
  public PricingValue getPricingForInstanceType(String machineType, String region,
      Boolean spotInstance, String imageProject) {
    return googlePriceList.getPrice(GoogleService.CP_COMPUTEENGINE_VMIMAGE,
        machineType.toUpperCase(), region, spotInstance);
  }
}
