package cloudos.pricing;

import cloudos.amazon.AmazonPricing;
import cloudos.models.Instance;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AmazonPricingManagement implements PricingManagement {

  @Autowired
  private AmazonPricing amazonPricing;

  @Override
  public void loadPricingData(PricingType type) throws PricingException {
    try {
      amazonPricing.getAwsPricing(getType(type));
    } catch (Exception e) {
      log.error("Error when fetch and store amazon pricing", e);
      throw new PricingException("Error loading the pricing", e);
    }
  }

  /**
   * Map a PricingType with the Aws Type.
   *
   * @param type pricing type
   * @return aws type
   */
  private String getType(PricingType type) {
    switch (type) {
      case INSTANCE:
        return AmazonPricing.AMAZON_EC2;
      case STORAGE:
        return AmazonPricing.AMAZON_S3;
      default:
        return null;
    }
  }

  @Override
  public String getActualPricingVersion(PricingType type) throws PricingException {
    try {
      return amazonPricing.getAwsPricingVersion(false, getType(type));
    } catch (Exception e) {
      log.error("Error getting amazon pricing version", e);
      throw new PricingException("Error getting amazon pricing version", e);
    }
  }

  @Override
  public PricingValue getPricingForInstance(Instance instance) {
    return amazonPricing.getPricingForInstance(instance.getMachineType(), instance.getRegion(),
        instance.getImageProject());
  }

  /**
   * Return the pricing for the instance type on that region.
   *
   * @param machineType type of instance
   * @param region of instance
   * @param spotInstance true if spot (PREEMPTIBLE)
   * @return pricing
   */
  @Override
  public PricingValue getPricingForInstanceType(String machineType, String region,
                                                Boolean spotInstance, String imageProject) {
    return amazonPricing.getPricingForInstance(machineType, region, imageProject);
  }

}
