package cloudos.pricing;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import cloudos.Providers;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "pricing_version")
public class PricingVersion implements Serializable {

  private static final long serialVersionUID = 7172146547131435821L;

  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  private String id;
  private Providers provider;
  private String url;
  private String version;
  private String content;
  private Date referenceDate;
  private Date loadingDate;
  private PricingType type;

}
