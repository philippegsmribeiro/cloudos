package cloudos.pricing;

import cloudos.exceptions.NotImplementedException;
import cloudos.microsoft.AzureOffer;
import cloudos.microsoft.AzureOfferService;
import cloudos.microsoft.AzurePricingService;
import cloudos.models.Instance;

import java.io.IOException;
import java.util.List;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AzurePricingManagement implements PricingManagement {

  private static final String CURRENCY = "USD";
  private static final String REGION_INFO = "US";

  @Value("${azure.client}")
  private String client;

  @Value("${azure.tenant}")
  private String tenant;

  @Value("${azure.key}")
  private String key;

  @Autowired
  private AzurePricingService azurePricingService;

  @Autowired
  private AzureOfferService azureOfferService;

  @Override
  public void loadPricingData(PricingType type) throws PricingException {
    try {
      log.info("fech azures offers ...");
      List<AzureOffer> azureOffers = azureOfferService.listAllAzureOffers();

      log.info(String.format("fetched azure offers size: %s", azureOffers.size()));

      for (AzureOffer azureOffer : azureOffers) {
        for (String azureOfferNumber : azureOffer.getOffersFullNumber()) {
          log.info(String.format("fetching azure offer: %s | %s", azureOffer.getOfferName(),
              azureOfferNumber));

          azurePricingService.fetchPricingDataIntoDatabase(CURRENCY, azureOfferNumber, REGION_INFO,
              azureOffer.isRetired());
        }
      }

    } catch (IOException e) {
      log.error("erro fetch / load azure pricing data ", e);
    }

  }

  @Override
  public String getActualPricingVersion(PricingType type) throws PricingException {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PricingValue getPricingForInstance(Instance instance) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PricingValue getPricingForInstanceType(String machineType, String region,
      Boolean spotInstance, String imageProject) throws NotImplementedException {
    // TODO Auto-generated method stub
    return null;
  }
}
