package cloudos.pricing;

import cloudos.Providers;
import cloudos.config.ThreadingConfig;
import cloudos.exceptions.NotImplementedException;
import cloudos.models.Instance;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PricingManagementService {

  @Autowired
  private PricingVersionRepository pricingVersionRepository;
  @Autowired
  private AmazonPricingManagement amazonProviderManagement;
  @Autowired
  private GooglePricingManagement googleProviderManagement;
  @Autowired
  @Qualifier(ThreadingConfig.MONITOR)
  AsyncTaskExecutor taskExecutor;

  /**
   * Get the specific management for the provider.
   *
   * @param provider - choose provider
   * @return specific PricingManagement
   * @throws NotImplementedException if not implemented yet.
   */
  private PricingManagement getManagement(Providers provider) throws NotImplementedException {
    switch (provider) {
      case AMAZON_AWS:
        return amazonProviderManagement;
      case GOOGLE_COMPUTE_ENGINE:
        return googleProviderManagement;
      default:
        break;
    }
    throw new NotImplementedException();
  }

  /**
   * Update the pricing info for a provider.
   *
   * @param provider to be update
   * @throws NotImplementedException if not implemented for provider
   * @throws PricingException if something goes wrong
   */
  public void updatePricingForProvider(Providers provider)
      throws NotImplementedException, PricingException {
    PricingType[] types = {PricingType.INSTANCE};
    for (PricingType pricingType : types) {
      log.info("Retrieving pricing {} for provider: {}", pricingType, provider);
      executeUpdating(provider, pricingType);
    }
  }

  private void executeUpdating(Providers provider, PricingType pricingType)
      throws PricingException, NotImplementedException {
    // if pricing table/info version has changed
    if (this.hasPricingVersionChanged(provider, pricingType)) {
      log.info("Retrieving pricing for provider: {}", provider);
      // TODO: add performance threads, etc.
      getManagement(provider).loadPricingData(pricingType);
    }
  }

  /**
   * Verify if the pricing has change for the provider.
   *
   * @param provider to be verified
   * @return true if pricing has change, false otherwise
   * @throws PricingException if something goes wrong
   * @throws NotImplementedException if not implemented yet
   */
  protected boolean hasPricingVersionChanged(Providers provider, PricingType pricingType)
      throws PricingException, NotImplementedException {
    List<PricingVersion> versions =
        pricingVersionRepository.findByProviderAndType(provider, pricingType);
    if (CollectionUtils.isNotEmpty(versions)) {
      // get the latest
      PricingVersion pricingVersion =
          versions.stream().sorted((a, b) -> b.getVersion().compareTo(a.getVersion()))
              .collect(Collectors.toList()).get(0);
      // compare
      return !pricingVersion.getVersion()
          .equals(getManagement(provider).getActualPricingVersion(pricingType));
    }
    return true;
  }

  /**
   * Return the pricing for the instance type on that region.
   *
   * @param machineType type of instance
   * @param region of instance
   * @param spotInstance true if spot (PREEMPTIBLE)
   * @return pricing
   */
  public PricingValue getPricingForInstanceType(String machineType, String region,
      Boolean spotInstance, String imageProject, Providers provider)
      throws NotImplementedException {

    return getManagement(provider).getPricingForInstanceType(machineType, region, spotInstance,
        imageProject);
  }

  public PricingValue getPriceForInstance(Instance instance) throws NotImplementedException {
    return getManagement(instance.getProvider()).getPricingForInstance(instance);
  }

}
