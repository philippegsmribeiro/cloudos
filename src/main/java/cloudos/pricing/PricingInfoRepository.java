package cloudos.pricing;

import cloudos.Providers;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface PricingInfoRepository extends MongoRepository<PricingInfo, String> {

  PricingInfo findByPricingVersionAndName(String pricingVersion, String name);

  PricingInfo findByPricingVersionAndMachineTypeAndRegion(String pricingVersion, String machineType,
      String region);

  PricingInfo findByPricingVersionAndMachineTypeAndRegionAndImage(String pricingVersion,
      String machineType, String region, String image);

  List<PricingInfo> findByMachineTypeAndProvider(String machineType, Providers provider);

}
