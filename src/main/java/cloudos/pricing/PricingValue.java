package cloudos.pricing;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PricingValue {

  private String currency;
  private Double value;
  private PricingValueUnit unit;
}
