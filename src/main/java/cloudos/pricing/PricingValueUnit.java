package cloudos.pricing;

public enum PricingValueUnit {
  HRS, 
  DAYS, 
  MINS, 
  SECS
}