package cloudos.pricing;

/**
 * Exception when getting pricing info fails.
 * 
 */
public class PricingException extends Exception {

  private static final long serialVersionUID = -6283521504310814784L;

  public PricingException() {
    super();
  }

  public PricingException(String message, Throwable e) {
    super(message, e);
  }

}
