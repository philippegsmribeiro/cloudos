package cloudos;

import cloudos.exceptions.NotImplementedException;
import cloudos.models.CloudosSecurityGroup;
import cloudos.models.CloudosSecurityGroupRepository;
import cloudos.securitygroup.AmazonSecurityGroupManagement;
import cloudos.securitygroup.AzureSecurityGroupManagement;
import cloudos.securitygroup.CloudSecurityGroupException;
import cloudos.securitygroup.GoogleSecurityGroupManagement;
import cloudos.securitygroup.SecurityGroupManagement;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Created by gleimar on 20/05/2017. */
@Service
public class SecurityGroupManagerService {

  @Autowired
  private AmazonSecurityGroupManagement amazonSecurityGroupManagement;

  @Autowired
  private AzureSecurityGroupManagement azureSecurityGroupManagement;

  @Autowired
  private GoogleSecurityGroupManagement googleSecurityGroupManagement;

  @Autowired
  private CloudosSecurityGroupRepository cloudosSecurityGroupRepository;

  /**
   * Multiplex between the different security group management based on the provider.
   *
   * @param providers the name of the provider
   * @return a security group management
   * @throws NotImplementedException if the manager is not implemented for the given provider
   */
  @SuppressWarnings("Duplicates")
  private SecurityGroupManagement getManagement(Providers providers)
      throws NotImplementedException {
    switch (providers) {
      case AMAZON_AWS:
        return amazonSecurityGroupManagement;
      case GOOGLE_COMPUTE_ENGINE:
        return googleSecurityGroupManagement;
      case MICROSOFT_AZURE:
        return azureSecurityGroupManagement;
      default:
        break;
    }
    throw new NotImplementedException();
  }

  /**
   * Create a new Cloudos security group.
   *
   * @param cloudosSecurityGroup the request containing the security group information
   * @return a newly created security group or null
   * @throws NotImplementedException if not implemented for the given provider
   * @throws CloudSecurityGroupException if failed to create the security group
   */
  public CloudosSecurityGroup create(CloudosSecurityGroup cloudosSecurityGroup)
      throws NotImplementedException, CloudSecurityGroupException {

    if (cloudosSecurityGroup == null) {
      throw new CloudSecurityGroupException("No cloudos security group entered.");
    }

    return this.getManagement(cloudosSecurityGroup.getProvider()).create(cloudosSecurityGroup);
  }

  /**
   * The delete function will delete the security group in the cloud provider.
   *
   * <p>
   * However, in cloudos database, it will be set as deleted, filing the field deleted.
   *
   * @param cloudosSecurityGroup the request containing the security group information
   * @return a deleted created security group or null
   * @throws NotImplementedException if not implemented for the given provider
   */
  public void delete(CloudosSecurityGroup cloudosSecurityGroup)
      throws CloudSecurityGroupException, NotImplementedException {

    if (cloudosSecurityGroup == null) {
      throw new CloudSecurityGroupException("No cloudos security group entered.");
    }

    this.getManagement(cloudosSecurityGroup.getProvider()).delete(cloudosSecurityGroup);
  }

  /**
   * List all security group (included the deleted).
   *
   * @param provider the name of the provider
   * @param groupName the name of the security group
   * @return a list of security groups
   */
  public List<CloudosSecurityGroup> list(Providers provider, String groupName) {
    return this.cloudosSecurityGroupRepository.findByNameAndProvider(groupName, provider);
  }

  /**
   * List all the security groups based only on the provider.
   *
   * @param provider the name of the provider
   * @return a list of security groups
   */
  public List<CloudosSecurityGroup> list(Providers provider) {
    return this.cloudosSecurityGroupRepository.findByProvider(provider);
  }

  /**
   * Get a security group based on its id.
   *
   * @param id the security group id
   * @return a security group if found, null otherwise
   */
  public CloudosSecurityGroup getSecurityGroup(String id) {
    return this.cloudosSecurityGroupRepository.findOne(id);
  }

  /**
   * Verify if security group has ssh port inbound rule enabled and add it if not.
   * 
   * @param provider to check
   * @param securityGroupName to check
   * @param region to check
   * @throws NotImplementedException if not implemented
   * @throws CloudSecurityGroupException if something goes wrong
   */
  public void verifySshAccessForSecurityGroup(Providers provider, String securityGroupName,
      String region)
      throws NotImplementedException, CloudSecurityGroupException {
    this.getManagement(provider).verifySshAccessForSecurityGroup(region, securityGroupName);
  }

}
