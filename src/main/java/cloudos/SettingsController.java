package cloudos;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cloudos.encryptionKeys.AmazonKMSServiceException;
import cloudos.exceptions.InvalidRequestException;
import cloudos.models.CloudosTenant;
import cloudos.user.UserProfile;
import cloudos.security.WebSecurityConfig;
import cloudos.tenant.TenantManagerService;
import cloudos.user.Invite;
import cloudos.user.InviteRequest;
import cloudos.user.InviteService;
import cloudos.user.UserUpdateRequest;
import cloudos.user.User;
import cloudos.user.UserRequest;
import cloudos.user.UserService;
import lombok.extern.log4j.Log4j2;

/**
 * Controller class to handle Tenant's profile edition, accounts operations, billing information,
 * security and manage users.
 *
 * @author Rogério Souza
 *
 */
@RestController
@RequestMapping(value = SettingsController.SETTINGS)
@Log4j2
public class SettingsController {

  public static final String SETTINGS = WebSecurityConfig.API_PATH + "/settings";

  @Autowired
  private TenantManagerService tenantManagerService;

  @Autowired
  private UserService userService;
  
  @Autowired
  private CredentialManagerService credentialManagerService;

  @Autowired
  private InviteService inviteService;

  /**
   * Create a new CloudosTenant.
   *
   * @param cloudosTenant - A CloudosTenant, containing most of the information needed.
   * @return HttpStatus.CREATED is successful.
   */
  @RequestMapping(value = "/save_tenant", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> saveTenant(@RequestBody CloudosTenant cloudosTenant) {

    if (cloudosTenant == null) {
      return new ResponseEntity<>(new InvalidRequestException("Request object is invalid"),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }

    try {

      cloudosTenant = tenantManagerService.update(cloudosTenant);

      if (cloudosTenant != null) {
        return new ResponseEntity<>(cloudosTenant, HttpStatus.OK);
      }

      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (Exception e) {
      log.error("Error", e);
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Updates the user profile and preferences.
   *
   * @param username the username of the user profile object to be saved
   * @return UserProfile - the found user profile object.
   */
  @RequestMapping(value = "/update_user_profile/{username:.+}", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> updateUserProfile(@PathVariable String username,
      @RequestBody UserProfile userProfile) {

    if (StringUtils.isBlank(username)) {
      return new ResponseEntity<>(new InvalidRequestException("Username is invalid"),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }

    try {

      userProfile = userService.updateUserProfile(username, userProfile);

      if (userProfile != null) {
        return new ResponseEntity<>(userProfile, HttpStatus.OK);
      }

      return new ResponseEntity<>(new InvalidRequestException("User Profile not found"),
          HttpStatus.INTERNAL_SERVER_ERROR);

    } catch (UsernameNotFoundException | InvalidRequestException | AmazonKMSServiceException e) {
      log.error("Error", e);
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Retrieve a UserProfile by username.
   *
   * @param username the username of the user object to be searched.
   * @return UserProfile - the found user profile object.
   */
  @RequestMapping(value = "/get_user_profile/{username:.+}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getUserProfileByUsername(@PathVariable String username) {

    if (StringUtils.isBlank(username)) {
      return new ResponseEntity<>(new InvalidRequestException("Username is invalid"),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }

    try {

      UserProfile userProfile = userService.loadUserProfileByUsername(username);
      if (userProfile != null) {
        return new ResponseEntity<>(userProfile, HttpStatus.OK);
      }
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

    } catch (UsernameNotFoundException | AmazonKMSServiceException e) {
      log.error("Error", e);
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  /**
   * Retrieve a CloudosTenant by username.
   *
   * @param username the username of the cloudosTenant user object to be searched.
   * @return CloudosTenant - the found tenant object.
   */
  @RequestMapping(value = "/get_tenant_by_username/{username:.+}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getTenantByUsername(@PathVariable String username) {

    if (StringUtils.isBlank(username)) {
      return new ResponseEntity<>(new InvalidRequestException("Username is invalid"),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }

    try {

      CloudosTenant cloudosTenant = tenantManagerService.getTenantByUsername(username);
      if (cloudosTenant != null) {
        return new ResponseEntity<>(cloudosTenant, HttpStatus.OK);
      }
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

    } catch (Exception e) {
      log.error("Error", e);
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  
  /**
   * Retrieve the user from Mongo and checks if this is the first login or not.
   *
   * @param username the username of the cloudosTenant user object to be searched.
   * @return true or false.
   */
  @RequestMapping(value = "/is_user_first_access/{username:.+}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> isUserFirstLogin(@PathVariable String username) {

    if (StringUtils.isBlank(username)) {
      return new ResponseEntity<>(new InvalidRequestException("Username is invalid"),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }

    try {

      return new ResponseEntity<>(userService.isUserFirstAccess(username), HttpStatus.OK);

    } catch (Exception e) {
      log.error("Error", e);
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }  
  
  /**
   * Checks if there are credentials on database, returning true or false.
   * 
   * @return true if it has credentials, false otherwise.
   */
  @RequestMapping(value = "/has_credentials", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> hasCredentials() {

    try {

      return new ResponseEntity<>(credentialManagerService.hasCredentials(), HttpStatus.OK);

    } catch (Exception e) {
      log.error("Error", e);
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }    

  /**
   * Updates the given username password.
   *
   * @param userUpdateRequest - the request object with required attributes.
   */
  @RequestMapping(value = "/update_password", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> updatePassword(@RequestBody UserUpdateRequest userUpdateRequest) {

    if (userUpdateRequest == null) {
      return new ResponseEntity<>(new InvalidRequestException("Request object is invalid"),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }

    try {
      userService.updatePassword(userUpdateRequest.getUsername(), userUpdateRequest.getOldPassword(),
          userUpdateRequest.getNewPassword());
    } catch (UsernameNotFoundException | InvalidRequestException | AmazonKMSServiceException e) {
      log.error("Error", e);
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  /**
   * Allow an existing user to invite other users to the cloudtown platform.
   * 
   * @param inviteRequest the invite request
   * @return HTTP 200 and the invite, if successful, HTTP 500 otherwise.
   */
  @RequestMapping(value = "/user/invite", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> inviteUser(@RequestBody InviteRequest inviteRequest) {
    if (inviteRequest == null) {
      return new ResponseEntity<>(new InvalidRequestException("Request object is invalid"),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }
    try {
      Invite invite = this.inviteService.create(inviteRequest);
      if (invite != null) {
        return new ResponseEntity<>(invite, HttpStatus.OK);
      }
    } catch (AmazonKMSServiceException e) {
      log.error("Error", e) ;
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * List all the current invitations, as long as the invitation is not expired.
   *
   * @return HTTP 200 and the a list of invites, if successful, HTTP 500 otherwise.
   */
  @RequestMapping(value = "/user/invite", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> listInvites() {
    List<Invite> invites = this.inviteService.list();
    if (invites != null) {
      return new ResponseEntity<>(invites, HttpStatus.OK);
    }
    return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
  }

  @RequestMapping(value = "/user/invite/accept/{id}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> acceptInvite(@PathVariable String id) {
    try {
      //
      Invite invite = this.inviteService.describe(id);
      if (invite != null) {
        if (!this.inviteService.isExpired(invite)) {
          // we will invalidate the invite once the user creates the profile.
          return new ResponseEntity<>(invite, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
      }
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } catch (Exception ex) {
      log.error(ex);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


  /**
   * Delete an existing invitation.
   *
   * @param id the invite's id
   * @return HTTP 200 if successful, HTTP 500 otherwise.
   */
  @RequestMapping(value = "/user/invite/{id}", method = RequestMethod.DELETE)
  @ResponseBody
  public ResponseEntity<?> deleteInvite(@PathVariable String id) {
    if (this.inviteService.delete(id)) {
      return new ResponseEntity<>(HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  /**
   * Rest endpoint that allows the user to update the email to where the invitation will be
   * sent to.
   *
   * @param id the invite's id
   * @param inviteRequest the invite request with the updated email
   * @return HTTP 200 and the invite, if successful, HTTP 500 otherwise.
   */
  @RequestMapping(value = "/user/invite/{id}", method = RequestMethod.PUT)
  @ResponseBody
  public ResponseEntity<?> editInvite(@PathVariable String id,
                                      @RequestBody InviteRequest inviteRequest) {
    try {
      Invite invite = this.inviteService.edit(id, inviteRequest);
      if (invite != null) {
        return new ResponseEntity<>(invite, HttpStatus.OK);
      }
    } catch (AmazonKMSServiceException e) {
      log.error(e);
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * Rest endpoint to describe a particular invitation.
   *
   * @param id the invite's id
   * @return HTTP 200 and the invite, if successful, HTTP 500 otherwise.
   */
  @RequestMapping(value = "/user/invite/{id}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> describeInvite(@PathVariable String id) {
    Invite invite = this.inviteService.describe(id);
    if (invite != null) {
      return new ResponseEntity<>(invite, HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @RequestMapping(value = "/user", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> addUser(@RequestBody UserRequest userRequest) {
    try {
      // check if the invitation is valid
      Invite invite = this.inviteService.describe(userRequest.getInvitationId());
      if (invite != null) {
        // use the profile to creat the new user
        User user = this.userService.save(userRequest.getUser());
        if (user != null) {
          // success
          return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>("Could not create the user", HttpStatus.BAD_REQUEST);
      }
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } catch (Exception ex) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @RequestMapping(value = "/user", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> listUsers() {
    List<User> users = this.userService.retrieveAllUsers();
    if (users != null) {
      return new ResponseEntity<>(users, HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> describeUser(@PathVariable String id) {
    User user = this.userService.findById(id);
    if (user != null) {
      return new ResponseEntity<>(user, HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
  @ResponseBody
  public ResponseEntity<?> deleteUser(@PathVariable String id) {
    User user = this.userService.findById(id);
    if (user != null) {
      // delete the user
      this.userService.delete(user);
      // we need to log that user out now ...
      return new ResponseEntity<>(HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  @RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
  @ResponseBody
  public ResponseEntity<?> editUser(@PathVariable String id, @RequestBody UserRequest userRequest) {
    User user = this.userService.findById(id);
    if (user != null) {
      User updated = userRequest.getUser();
      updated.setPassword(user.getPassword());
      updated.setUsername(user.getUsername());
      updated.setEmail(user.getEmail());
      updated.setId(user.getId());

      try {
        updated = this.userService.save(updated);
        return new ResponseEntity<>(updated, HttpStatus.OK);
      } catch (AmazonKMSServiceException | InvalidRequestException e) {
        log.error(e.getMessage());
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
      }
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }
}
