package cloudos.exceptions;

public class InvalidRequestException extends Exception {
  private static final long serialVersionUID = 6785755216233065329L;

  public InvalidRequestException(String message) {
    super(message);
  }
}
