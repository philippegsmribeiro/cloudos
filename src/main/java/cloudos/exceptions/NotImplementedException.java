package cloudos.exceptions;

public class NotImplementedException extends Exception {
  private static final long serialVersionUID = 146966005260484792L;

  public NotImplementedException() {
    super("Not implemented");
  }
}
