package cloudos;

import cloudos.data.InstanceData;
import cloudos.exceptions.NotImplementedException;
import cloudos.models.AbstractInstance;
import cloudos.models.CloudosDatapoint;
import cloudos.models.CloudosResponse;
import cloudos.models.ResourceFetchRequest;
import cloudos.queue.QueueManagerService;
import cloudos.resources.ResourcesService;
import cloudos.security.WebSecurityConfig;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/** Created by philipperibeiro on 3/20/17. */
@RestController
@RequestMapping(value = CloudosResourcesController.RESOURCES)
@Log4j2
public class CloudosResourcesController {

  public static final String RESOURCES = WebSecurityConfig.API_PATH + "/resources";

  @Autowired
  private ResourcesService resourcesService;
  
  @Autowired
  private QueueManagerService queueManagerService;

  public CloudosResourcesController() {}

  /**
   * Sync the instances.
   *
   * @return a ResponseEntity
   */
  @RequestMapping(value = "/sync", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> sync() {
    resourcesService.syncInstances();
    return new ResponseEntity<>(HttpStatus.OK);
  }

  /**
   * List the resources according with the filter request.
   *
   * @param fetchRequest the resources fetch request
   * @return a ResponseEntity
   */
  @RequestMapping(value = "/list", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> listResources(@RequestBody ResourceFetchRequest fetchRequest) {
    return new ResponseEntity<>(resourcesService.listResources(fetchRequest), HttpStatus.OK);
  }
  
  /**
   * Get details from the resource.
   *
   * @param resource the resources fetch request
   * @return a ResponseEntity
   */
  @RequestMapping(value = "/list/{resource}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> filterResources(@PathVariable String resource) {
    try {
      if (StringUtils.isBlank(resource)) {
        return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.PRECONDITION_FAILED)
            .message("Resource request is invalid").build(), HttpStatus.PRECONDITION_FAILED);
      } else {
        AbstractInstance response = resourcesService.retrieveResource(resource);
        if (response != null) {
          return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
          log.info("Resource " + resource + " not found");
          return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.NOT_FOUND)
              .message("Resource " + resource + " not found").build(), HttpStatus.NOT_FOUND);
        }
      }
    } catch (NotImplementedException e) {
      log.error("Error", e);
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.NOT_IMPLEMENTED)
          .message(e.getMessage()).build(), HttpStatus.NOT_IMPLEMENTED);
    } catch (Exception e) {
      log.error("Error", e);
      return new ResponseEntity<>(CloudosResponse.builder().status(HttpStatus.INTERNAL_SERVER_ERROR)
          .message(e.getMessage()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * The datapoints endpoint is responsible for fetching data related to a single instance. Given
   * its resource id and a InstanceUtil request, attempts to find all the datapoints associated with
   * this particular instance.
   *
   * @param resource the resource id
   * @param data the InstanceUtil request
   * @return a list of CloudosDatapoints if successful, INTERNAL_SERVER_ERROR otherwise
   */
  @RequestMapping(value = "/list/{resource}/datapoints", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> getDatapoints(@PathVariable String resource,
      @RequestBody InstanceData data) {
    log.info("Getting datapoints for resource `{}`", resource);
    List<CloudosDatapoint> datapoints;
    try {
      datapoints = this.resourcesService.getDatapoints(resource, data);
      if (datapoints != null) {
        return new ResponseEntity<>(datapoints, HttpStatus.OK);
      }
      return new ResponseEntity<HttpStatus>(HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (NotImplementedException e) {
      return new ResponseEntity<>(e, HttpStatus.NOT_IMPLEMENTED);
    }
  }

  /**
   * Given a resource, returns its logs.
   *
   * @param resource the name or ID of the resource
   * @return a stream to the log
   */
  @RequestMapping(value = "/log/{resource}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getResourceLog(@PathVariable String resource) {
    return new ResponseEntity<HttpStatus>(HttpStatus.NOT_IMPLEMENTED);
  }

  /**
   * Create a new Alert for the Resource.
   *
   * @param resource the name or ID of the resource
   * @return the status of the request
   */
  @RequestMapping(value = "/alert/{resource}/create/", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> createAlert(@PathVariable String resource) {
    return new ResponseEntity<HttpStatus>(HttpStatus.NOT_IMPLEMENTED);
  }

  /**
   * List all the alerts for a particular resource.
   *
   * @param resource the name or ID of the resource
   * @return a list of alerts
   */
  @RequestMapping(value = "/alert/{resource/list", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> listAlert(@PathVariable String resource) {
    return new ResponseEntity<HttpStatus>(HttpStatus.NOT_IMPLEMENTED);
  }

  /**
   * Get the total cost of the cloud.
   *
   * @param cloud the cloud name or ID
   * @return the datapoint chart regarding the cost of the cloud, including all its resources.
   */
  @RequestMapping(value = "/cost/cloud/{cloud}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getCloudCost(@PathVariable String cloud) {
    return new ResponseEntity<HttpStatus>(HttpStatus.NOT_IMPLEMENTED);
  }

  /**
   * Get the current cost of the resource.
   *
   * @param resource the name or ID of the instance
   * @return the datapoint chart regarding the cost of the resource
   */
  @RequestMapping(value = "/cost/{resource}/resource", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getResourceCost(@PathVariable String resource) {
    return new ResponseEntity<HttpStatus>(HttpStatus.NOT_IMPLEMENTED);
  }

  /**
   * Create a new properties for the resource.
   *
   * @param resource the name or ID of the instance
   * @param property the property parameter to be set.
   * @return the status of the request
   */
  @RequestMapping(value = "/properties/{resource}/create", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> createProperties(@PathVariable String resource,
      @RequestBody String property) {
    return new ResponseEntity<HttpStatus>(HttpStatus.NOT_IMPLEMENTED);
  }

  /**
   * Get details from the resource.
   *
   * @param resource the resources fetch request
   * @return a ResponseEntity
   */
  @RequestMapping(value = "/reinstallClient/{resource}", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> reinstallClient(@PathVariable String resource) {
    try {
      AbstractInstance instance = resourcesService.retrieveResource(resource);
      if (instance != null) {
        queueManagerService.sendReInstallClient(instance);
        return new ResponseEntity<>(HttpStatus.OK);
      } else {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }
    } catch (NotImplementedException e) {
      return new ResponseEntity<>(e, HttpStatus.NOT_IMPLEMENTED);
    }
  }
}
