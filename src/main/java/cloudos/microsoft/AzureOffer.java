package cloudos.microsoft;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@Builder
@NoArgsConstructor
public class AzureOffer {

  private String offerName;
  private List<String> offersNumbers;
  private boolean isRetired;
  private String offerPrefix;

  public AzureOffer(String offerName, String offersNumbers, boolean isRetired, String offerPrefix) {
    super();
    this.offerName = offerName;
    this.isRetired = isRetired;
    this.offerPrefix = offerPrefix;

    this.initOffersNumbers(offersNumbers);
  }

  private void initOffersNumbers(String offersNumbers) {
    this.offersNumbers = new ArrayList<>();

    StringTokenizer st = new StringTokenizer(offersNumbers, ",");
    while (st.hasMoreTokens()) {
      this.offersNumbers.add(st.nextToken());
    }
  }

  /**
   * Return the full description of the offer. Ex.: MS-AZR-0003P
   *
   * @return
   */
  public List<String> getOffersFullNumber() {
    List<String> offersFullNumber = new ArrayList<>();

    for (String offersNumber : this.offersNumbers) {
      offersFullNumber.add(this.getOfferPrefix() + offersNumber);
    }

    return offersFullNumber;
  }

}
