package cloudos.microsoft;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import cloudos.models.AbstractInstance;
import cloudos.utils.MeasureConverter;
import lombok.AllArgsConstructor;
import lombok.Data;

/** Created by gleimar on 24/11/2016. */
@Document(collection = "azure_instance")
@Data
@AllArgsConstructor
public class AzureInstance extends AbstractInstance {

  private int cores;
  private String label;
  private int maxDataDiskCount;
  private int memoryInMb;
  private boolean supportedByVirtualMachines;
  private boolean supportedByWebWorkerRoles;
  private int virtualMachineResourceDiskSizeInMb;
  private int webWorkerResourceDiskSizeInMb;

  public static final ArrayList<String> features =
      new ArrayList<>(
          Arrays.asList(
              "cores",
              "maxDataDiskCount",
              "memoryInMb",
              "virtualMachineResourceDiskSizeInMb",
              "webWorkerResourceDiskSizeInMb",
              "supportedByVirtualMachines",
              "supportedByWebWorkerRoles"));

  /** Default constructor. */
  public AzureInstance() {
    super();
  }

  public List<Double> getFeatureVector() {
    List<Double> featureVector = new ArrayList<Double>();

    featureVector.add((double) this.cores);
    featureVector.add((double) this.maxDataDiskCount);
    featureVector.add(MeasureConverter.convertMbToGb(this.memoryInMb));
    featureVector.add(MeasureConverter.convertMbToGb(this.virtualMachineResourceDiskSizeInMb));
    featureVector.add(MeasureConverter.convertMbToGb(this.webWorkerResourceDiskSizeInMb));
    double virtualMachine = (this.supportedByVirtualMachines == true) ? 1.0 : 0.0;
    featureVector.add(virtualMachine);
    double workerRole = (this.supportedByWebWorkerRoles == true) ? 1.0 : 0.0;
    featureVector.add(workerRole);

    // We are done
    return featureVector;
  }

  public double extractFeatureValue(String feature, String value) {
    // TODO Auto-generated method stub
    return 0;
  }
}
