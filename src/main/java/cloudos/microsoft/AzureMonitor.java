package cloudos.microsoft;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import lombok.extern.log4j.Log4j2;

/** Created by gleimar on 12/03/2017. */
@Log4j2
public class AzureMonitor {

  protected String client;
  protected String tenant;
  protected String key;
  protected String subscription;
  private AzureRest azureRest;

  public enum AggregationType {
    AVERAGE("Average"),
    TOTAL("Total");

    protected String aggregationType;

    AggregationType(String aggregationType) {
      this.aggregationType = aggregationType;
    }
  }

  public enum MetricName {
    NETWORK_IN("Network In"),
    NETWORK_OUT("Network Out"),
    PERCENTAGE_CPU("Percentage CPU"),
    DISK_READ_BYTES("Disk Read Bytes"),
    DISK_WRITE_BYTES("Disk Write Bytes"),
    DISK_READ_OPERATIONS_SEC("Disk Read Operations/Sec");

    protected String metricName;

    MetricName(String metricName) {
      this.metricName = metricName;
    }
  }

  public AzureMonitor(String client, String tenant, String key, String subscription)
      throws IOException {
    this.client = client;
    this.tenant = tenant;
    this.key = key;
    this.subscription = subscription;

    this.initAzureConnection();
  }

  private void initAzureConnection() throws IOException {
    this.azureRest = new AzureRest(this.client, this.tenant, this.key);
  }

  /**
   * Get data point with the metriName information in the timeGrain duration
   *
   * <p>Ex.: PT1M ( one-minute duration)
   *
   * @param resourceUri
   * @param metricName
   * @param aggregationType
   * @param startDate
   * @param endDate
   * @param timeGrain
   * @return
   * @throws IOException
   */
  public AzureRest.MetricValueResource[] getDatapoints(
      final String resourceUri,
      final MetricName metricName,
      final AggregationType aggregationType,
      final LocalDate startDate,
      final LocalDate endDate,
      final String timeGrain)
      throws IOException {

    String filterQuery =
        "name.value eq '%s' and aggregationType eq '%s' and startTime eq %s and endTime eq %s and timeGrain eq duration'%s'";
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    filterQuery =
        String.format(
            filterQuery,
            metricName.metricName,
            aggregationType.aggregationType,
            formatter.format(startDate),
            formatter.format(endDate),
            timeGrain);

    AzureRest.MetricValueResource[] metricValueResources =
        this.azureRest.metricValueResource(resourceUri, filterQuery);

    return metricValueResources;
  }
}
