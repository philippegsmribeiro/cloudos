package cloudos.microsoft;

import cloudos.microsoft.AzureRest.RateCard;

import java.io.IOException;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class AzurePricingService {

  private static final String LOCALE = "en-US";

  @Autowired private AzurePricingRepository azurePricingRepository;

  @Value("${azure.client}")
  private String client;

  @Value("${azure.tenant}")
  private String tenant;

  @Value("${azure.key}")
  private String key;

  public AzurePricingService() {
    super();
  }

  /**
   * Constructor.
   *
   * @param azurePricingRepository
   * @param client
   * @param tenant
   * @param key
   */
  public AzurePricingService(
      AzurePricingRepository azurePricingRepository, String client, String tenant, String key) {
    super();
    this.azurePricingRepository = azurePricingRepository;
    this.client = client;
    this.tenant = tenant;
    this.key = key;
  }

  /**
   * Fetch the pricing in the Azure and store in the database.
   *
   * @param currency
   * @param offerDurableId
   * @param regionInfo
   * @param retired
   * @throws IOException
   */
  public void fetchPricingDataIntoDatabase(
      String currency, String offerDurableId, String regionInfo, Boolean retired)
      throws IOException {

    AzureRest azureRest = new AzureRest(this.client, this.tenant, this.key);

    log.info("fetch new pricing azure ...");

    RateCard rateCard = azureRest.searchRateCard(offerDurableId, currency, LOCALE, regionInfo);

    if (rateCard != null) {
      AzurePricing azurePricing =
          AzureMapperUtils.convertToMongoDataPricing(rateCard, offerDurableId, regionInfo, retired);

      log.info("save new pricing azure ...");

      azurePricingRepository.save(azurePricing);
    }
  }
}
