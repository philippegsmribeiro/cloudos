package cloudos.microsoft;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface AzurePricingRepository
    extends MongoRepository<AzurePricing, String> {

  AzurePricing findByCurrency(String currency);

  AzurePricing findByCurrencyAndActive(String currency, boolean active);

  @Query(value = "{ 'meters.meterSubCategory': ?0 }")
  List<AzurePricing> findByMeterSubCategory(String meterSubCategory);
}
