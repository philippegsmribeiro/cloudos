package cloudos.microsoft;

import cloudos.models.Instance;

import org.springframework.data.mongodb.repository.MongoRepository;

/** Interface for collection azure_instance. */
public interface AzureInstanceRepository extends MongoRepository<AzureInstance, String> {

  AzureInstance findByInstance(Instance instance);
}
