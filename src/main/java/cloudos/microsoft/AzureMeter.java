package cloudos.microsoft;

import java.math.BigDecimal;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AzureMeter {

  private String meterId;
  private String meterName;
  private String meterCategory;
  private String meterSubCategory;
  private String unit;
  private Map<String, String> meterRates;
  private String effectiveDate;
  private BigDecimal includedQuantity;
  private String meterRegion;

}
