package cloudos.microsoft;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

/**
 * Azure do not provide a API do load all offers.
 *
 * <p>Instead, he provide a HTML page with a list of : 'Curremt Offers' and 'Retired Offers'
 *
 * <p>https://azure.microsoft.com/en-us/support/legal/offer-details/
 *
 * @author gleimar
 */
@Component
public class AzureOfferService {

  private static final String PREFIX_AZURE_OFFER_ID = "MS-AZR-";
  private static final String PREFIX_AZURE_OFFER_DE_ID = "MS-AZR-DE";
  private List<AzureOffer> listAzureOffers;

  public AzureOfferService() {

    listAzureOffers =
        Arrays.asList(
            new AzureOffer("Pay-As-You-Go", "0003P", false, PREFIX_AZURE_OFFER_ID),
            new AzureOffer("Azure Dynamics", "0033P", false, PREFIX_AZURE_OFFER_ID),
            new AzureOffer("Support Plans", "0041P, 0042P, 0043P", false, PREFIX_AZURE_OFFER_ID),
            new AzureOffer("Free Trial", "0044P", false, PREFIX_AZURE_OFFER_ID),
            new AzureOffer(
                "Visual Studio Professional subscribers", "0059P", false, PREFIX_AZURE_OFFER_ID),
            new AzureOffer(
                "Visual Studio Test Professional subscribers",
                "0060P",
                false,
                PREFIX_AZURE_OFFER_ID),
            new AzureOffer("MSDN Platforms subscribers", "0062P", false, PREFIX_AZURE_OFFER_ID),
            new AzureOffer(
                "Visual Studio Enterprise subscribers", "0063P", false, PREFIX_AZURE_OFFER_ID),
            new AzureOffer(
                "Visual Studio Enterprise (BizSpark) subscribers",
                "0064P",
                false,
                PREFIX_AZURE_OFFER_ID),
            new AzureOffer(
                "Visual Studio Enterprise (MPN) subscribers",
                "0029P",
                false,
                PREFIX_AZURE_OFFER_ID),
            new AzureOffer(
                "Visual Studio Dev Essentials members", "0022P", false, PREFIX_AZURE_OFFER_ID),
            new AzureOffer("Pay-As-You-Go Dev/Test", "0023P", false, PREFIX_AZURE_OFFER_ID),
            new AzureOffer("Enterprise Dev/Test", "0148P", false, PREFIX_AZURE_OFFER_ID),
            new AzureOffer("Action Pack", "0025P", false, PREFIX_AZURE_OFFER_ID),
            new AzureOffer(
                "Microsoft Azure Sponsored Offer", "0036P", false, PREFIX_AZURE_OFFER_ID),
            new AzureOffer("Promotional Offer", "0070P-0089P", false, PREFIX_AZURE_OFFER_ID),
            new AzureOffer("Azure Pass", "0120P-0130P", false, PREFIX_AZURE_OFFER_ID),
            new AzureOffer("Azure in Open Licensing", "0111p", false, PREFIX_AZURE_OFFER_ID),
            new AzureOffer("Microsoft Imagine", "0144P", false, PREFIX_AZURE_OFFER_ID),
            new AzureOffer("BizSpark Plus", "0149P", false, PREFIX_AZURE_OFFER_ID),
            new AzureOffer("Azure in CSP", "0145P", false, PREFIX_AZURE_OFFER_ID),
            new AzureOffer("Azure Germany Pay-As-You-Go", "0003P", false, PREFIX_AZURE_OFFER_DE_ID),
            new AzureOffer(
                "Azure Germany Support Plans",
                "0041P, 0042P, 0043P",
                false,
                PREFIX_AZURE_OFFER_DE_ID),
            new AzureOffer("Azure MSDN Premium", "0005P", true, PREFIX_AZURE_OFFER_ID),
            new AzureOffer("Azure MSDN â€“ Visual Studio ", "0010P", true, PREFIX_AZURE_OFFER_ID),
            new AzureOffer(
                "Azure MSDN â€“ Visual Studio Premium", "0011P", true, PREFIX_AZURE_OFFER_ID),
            new AzureOffer(
                "Azure MSDN â€“ Visual Studio Ultimate", "0012P", true, PREFIX_AZURE_OFFER_ID),
            new AzureOffer(
                "MPN Silver Cloud Platform Competency", "0027P", true, PREFIX_AZURE_OFFER_ID),
            new AzureOffer(
                "MPN Gold Cloud Platform Competency", "0028P", true, PREFIX_AZURE_OFFER_ID),
            new AzureOffer("BizSpark Plus", "0034P", true, PREFIX_AZURE_OFFER_ID),
            new AzureOffer("6-Month Plan", "0037P", true, PREFIX_AZURE_OFFER_ID),
            new AzureOffer("6-Month Plan (Prepaid)", "0038P", true, PREFIX_AZURE_OFFER_ID),
            new AzureOffer("12-Month Commitment Offer", "0026P", true, PREFIX_AZURE_OFFER_ID),
            new AzureOffer("12-Month Plan", "0039P", true, PREFIX_AZURE_OFFER_ID),
            new AzureOffer("12-Month Plan (Prepaid)", "0040P", true, PREFIX_AZURE_OFFER_ID),
            new AzureOffer("MPN Action Pack", "0035P", true, PREFIX_AZURE_OFFER_ID),
            new AzureOffer(
                "Visual Studio Premium with MSDN (benefit)", "0061P", true, PREFIX_AZURE_OFFER_ID),
            new AzureOffer(
                "Backup Storage for Windows Server", "0090P", true, PREFIX_AZURE_OFFER_ID));
  }

  /**
   * List all azure offers (retired or not)
   *
   * <p>Waring: This list is static. Azure do not provide a API to retrieve the values online.
   *
   * @return
   */
  public List<AzureOffer> listAllAzureOffers() {
    return listAzureOffers;
  }
}
