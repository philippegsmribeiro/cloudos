package cloudos.microsoft;

import com.microsoft.windowsazure.Configuration;
import com.microsoft.windowsazure.core.utils.KeyStoreType;
import com.microsoft.windowsazure.exception.ServiceException;
import com.microsoft.windowsazure.management.ManagementClient;
import com.microsoft.windowsazure.management.ManagementService;
import com.microsoft.windowsazure.management.RoleSizeOperations;
import com.microsoft.windowsazure.management.configuration.ManagementConfiguration;
import com.microsoft.windowsazure.management.models.RoleSizeListResponse;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

/**
 * This class is responsible for make CLASSIC rest call do Azure API
 *
 * <p>Warning: This class will be replaced by a new ARM Azure implementation when is done
 *
 * <p>Created by gleimar on 24/11/2016.
 */
@Component
@Log4j2
public class AzureManagementClassic {

  private static final String HTTPS_MANAGEMENT_CORE_WINDOWS_NET =
      "https://management.core.windows.net/";

  @Value("${azure.classic.subscription}")
  private String subscriptionId;

  @Value("${azure.classic.passwordKey}")
  private String keyStorePassword;

  private static final String KEY_STORE_LOCATION = "/WindowsAzureKeyStore.jks";

  private Path pathKey;

  public AzureManagementClassic() {
    this.loadKey();
  }

  public AzureManagementClassic(String subscriptionId, String keyStorePassword) {
    this();

    this.subscriptionId = subscriptionId;
    this.keyStorePassword = keyStorePassword;
  }

  public Path getPathKey() {
    return pathKey;
  }

  private void loadKey() {
    InputStream inputStreamKey = this.getClass().getResourceAsStream(KEY_STORE_LOCATION);
    if (inputStreamKey != null) {

      try {
        File tempFile = File.createTempFile("pks", "key");

        pathKey = tempFile.toPath();

        java.nio.file.Files.copy(inputStreamKey, pathKey, StandardCopyOption.REPLACE_EXISTING);

      } catch (IOException e) {
        log.error("", e);
      }
    }
  }

  /**
   * Return a list of roleSize that is a description of the resources available in the Azure Cloud.
   *
   * <p>Example of information that RoleSize contain: number of CPU cores, memory, etc
   *
   * @return
   * @throws ServiceException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws IOException
   * @throws URISyntaxException
   */
  public List<RoleSizeListResponse.RoleSize> getRolesSize()
      throws ServiceException, ParserConfigurationException, SAXException, IOException,
          URISyntaxException {

    Configuration config =
        ManagementConfiguration.configure(
            new URI(HTTPS_MANAGEMENT_CORE_WINDOWS_NET),
            subscriptionId,
            this.getPathKey().toString(), // the file path to the JKS
            this.keyStorePassword, // the password for the JKS
            KeyStoreType.jks // flags that I'm using a JKS keystore
            );

    // create a management client to call the API
    ManagementClient client = ManagementService.create(config);

    RoleSizeOperations roleSizesOperations = client.getRoleSizesOperations();
    RoleSizeListResponse roleSizes = roleSizesOperations.list();

    return roleSizes.getRoleSizes();
  }
}
