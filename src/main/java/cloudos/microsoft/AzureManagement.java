package cloudos.microsoft;

import com.microsoft.azure.AzureEnvironment;
import com.microsoft.azure.CloudException;
import com.microsoft.azure.PagedList;
import com.microsoft.azure.credentials.ApplicationTokenCredentials;
import com.microsoft.azure.management.Azure;
import com.microsoft.azure.management.compute.ImageReference;
import com.microsoft.azure.management.compute.PowerState;
import com.microsoft.azure.management.compute.VirtualMachine;
import com.microsoft.azure.management.compute.VirtualMachineImage;
import com.microsoft.azure.management.compute.VirtualMachineOffer;
import com.microsoft.azure.management.compute.VirtualMachinePublisher;
import com.microsoft.azure.management.compute.VirtualMachineSizeTypes;
import com.microsoft.azure.management.compute.VirtualMachineSku;
import com.microsoft.azure.management.network.Network;
import com.microsoft.azure.management.network.Network.DefinitionStages.WithCreate;
import com.microsoft.azure.management.resources.ResourceGroup;
import com.microsoft.azure.management.resources.fluentcore.arm.Region;
import com.microsoft.azure.management.resources.fluentcore.model.Creatable;
import com.microsoft.azure.management.resources.fluentcore.model.CreatedResources;
import com.microsoft.azure.management.resources.fluentcore.utils.ResourceNamer;
import com.microsoft.azure.management.storage.StorageAccount;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lombok.extern.log4j.Log4j2;

import org.apache.commons.lang3.StringUtils;

import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;

/**
 * Management the azure cloud resources.
 *
 * @author gleimar
 */
@Log4j2
public class AzureManagement {

  private static Level AZURE_LOG_LEVEL = HttpLoggingInterceptor.Level.NONE;

  private String client;
  private String tenant;
  private String key;
  private ApplicationTokenCredentials credentials;
  private Azure azure;
  private Region region;

  private CreatedResources<VirtualMachine> virtualMachines;

  /**
   * @param client The client parameter supplied by azure account.
   * @param tenant The tenant parameter supplied by azure account.
   * @param key The ley parameter supplied by azure account.
   * @param region The region of the resource
   * @throws CloudException When has a problem with the cloud connection
   * @throws IOException
   */
  public AzureManagement(String client, String tenant, String key, Region region)
      throws CloudException, IOException {

    assert client != null;
    assert tenant != null;
    assert key != null;

    this.client = client;
    this.tenant = tenant;
    this.key = key;
    this.region = region;

    this.initCredential();
    this.initAzureConnection();
  }

  /**
   * A constructor with default region.
   *
   * <p>{@link #AzureManagement(String, String, String, Region)}
   */
  public AzureManagement(String client, String tenant, String key)
      throws CloudException, IOException {
    this(client, tenant, key, Region.US_EAST);
  }

  private void initAzureConnection() throws CloudException, IOException {

    this.azure =
        Azure.configure()
            .withLogLevel(AZURE_LOG_LEVEL)
            .authenticate(this.credentials)
            .withDefaultSubscription();
  }

  private void initCredential() {
    credentials = new ApplicationTokenCredentials(client, tenant, key, AzureEnvironment.AZURE);
  }

  /**
   * Get the count of virtual machines that exists in the resourceGroupName provided.
   *
   * @param resourceGroupName
   * @return
   */
  public int getVirtualMachineCount(String resourceGroupName) {
    int count = 0;
    for (VirtualMachine virtualMachine : this.azure.virtualMachines().list()) {
      count += (virtualMachine.resourceGroupName().equalsIgnoreCase(resourceGroupName) ? 1 : 0);
    }

    return count;
  }

  public void printResourceGroup() {
    for (ResourceGroup resourceGroup : this.azure.resourceGroups().list()) {
      log.info(resourceGroup.name() + " " + resourceGroup.id());
    }
  }

  /**
   * @param imageType For example:KnownLinuxVirtualMachineImage.UBUNTU_SERVER_16_04_LTS
   * @param instanceType The size of Virtual Machine. Example: VirtualMachineSizeTypes.STANDARD_A0
   * @param minCount
   * @param maxCount
   * @param resourceGroupName The name of resource
   * @param usernameVM The username of the user to be create in vm
   * @param passwordVM The username of the user to be create in vm
   */
  public void createInstance(
      ImageReference imageType,
      VirtualMachineSizeTypes instanceType,
      Integer minCount,
      Integer maxCount,
      String resourceGroupName,
      String usernameVM,
      String passwordVM) {

    assert this.azure != null;

    ResourceGroup resourceGroup = generateResourceGroupDefinition(resourceGroupName);

    Creatable<Network> creatableNetwork =
        generateDefaultNetworkConfigurationDefinition(resourceGroupName);

    StorageAccount.DefinitionStages.WithCreate creatableStorageAccount =
        generateStorageDefinition(resourceGroup);

    List<Creatable<VirtualMachine>> creatableVirtualMachines = new ArrayList<>();

    for (int i = 0; i < maxCount; i++) {

      VirtualMachine.DefinitionStages.WithCreate creatableVirtualMachine =
          this.azure
              .virtualMachines()
              .define(ResourceNamer.randomResourceName("cos", 10))
              .withRegion(region)
              .withExistingResourceGroup(resourceGroup)
              .withNewPrimaryNetwork(creatableNetwork)
              .withPrimaryPrivateIpAddressDynamic()
              .withoutPrimaryPublicIpAddress()
              .withSpecificLinuxImageVersion(imageType)
              .withRootUserName(usernameVM)
              .withPassword(passwordVM)
              .withSize(instanceType)
              .withNewStorageAccount(creatableStorageAccount);

      creatableVirtualMachines.add(creatableVirtualMachine);
    }

    log.info("Creating " + maxCount + " virtual machines");

    virtualMachines = this.azure.virtualMachines().create(creatableVirtualMachines);

    log.info("Virtual machines created");

    this.logInformationVitualMachines();

    this.startVirtualMachines(resourceGroupName);
  }

  /**
   * Terminate and deallocat all resource of the resource group created Be careful. All the items
   * created in the resource group will be deleted (all vm's, storage, etc).
   *
   * @param resourceGroupName - The name of the resource group
   */
  public void terminate(String resourceGroupName) {
    log.info("sizeResourcesGroups:" + azure.resourceGroups().list().size());

    for (ResourceGroup rGroup : this.azure.resourceGroups().list()) {

      if (!rGroup.name().equalsIgnoreCase(resourceGroupName)) {
        continue;
      }

      log.info("Resource found: " + rGroup.name());

      this.azure.resourceGroups().delete(rGroup.name());
    }

    log.info("sizeResourcesGroups after deleted:" + azure.resourceGroups().list().size());

    for (ResourceGroup rGroup : this.azure.resourceGroups().list()) {

      if (!rGroup.name().equalsIgnoreCase(resourceGroupName)) {
        continue;
      }

      log.info("Resource found: " + rGroup.name());

      this.azure.resourceGroups().delete(rGroup.name());
    }
  }

  /**
   * Starts all the virtual machines presents in the resource group name provided.
   *
   * @param resourceGroupName
   */
  private void startVirtualMachines(String resourceGroupName) {
    for (VirtualMachine virtualMachine : virtualMachines) {

      if (!virtualMachine.resourceGroupName().equalsIgnoreCase(resourceGroupName)) {
        continue;
      }

      log.info(
          String.format(
              "start machine %s status: %s", virtualMachine.id(), virtualMachine.powerState()));

      if (virtualMachine.powerState().equals(PowerState.DEALLOCATED)) {
        log.info("starting vm:" + virtualMachine.id());

        virtualMachine.start();

        String provisioningState = virtualMachine.provisioningState();

        log.info("provisioningState:" + provisioningState);

        while (!(provisioningState.equalsIgnoreCase("Succeeded")
            || provisioningState.equalsIgnoreCase("Failed")
            || provisioningState.equalsIgnoreCase("Cancelled"))) {

          try {
            Thread.sleep(1000);
          } catch (InterruptedException e) {
            log.error("sleep", e);
          }

          provisioningState = virtualMachine.provisioningState();

          log.info("provisioningState:" + provisioningState);
        }
      }
    }
  }

  /** Print log information about virtual machine. */
  private void logInformationVitualMachines() {
    for (VirtualMachine virtualMachine : virtualMachines) {
      log.info(virtualMachine.id() + " " + virtualMachine.powerState());
    }
  }

  private StorageAccount.DefinitionStages.WithCreate generateStorageDefinition(
      ResourceGroup resourceGroup) {

    String tmp = ResourceNamer.randomResourceName("stg" + resourceGroup.name(), 24);

    StorageAccount.DefinitionStages.WithCreate creatableStorageAccount =
        this.azure
            .storageAccounts()
            .define(tmp)
            .withRegion(region)
            .withExistingResourceGroup(resourceGroup);

    return creatableStorageAccount;
  }

  private ResourceGroup generateResourceGroupDefinition(String resourceGroupName) {

    ResourceGroup resourceGroup =
        this.azure.resourceGroups().define(resourceGroupName).withRegion(region).create();
    return resourceGroup;
  }

  private WithCreate generateDefaultNetworkConfigurationDefinition(String resourceGroupName) {

    Network.DefinitionStages.WithCreate creatableNetwork =
        this.azure
            .networks()
            .define(ResourceNamer.randomResourceName("ntw" + resourceGroupName, 24))
            .withRegion(region)
            .withExistingResourceGroup(resourceGroupName)
            .withAddressSpace("172.16.0.0/16");

    return creatableNetwork;
  }

  /**
   * Return a list of ImageReference with the machine image description.
   *
   * <p>The ImageReference has 3 information: Publisher Offer Sku Ex.: "Canonical", "UbuntuServer",
   * "16.04.0-LTS" Where: Publisher: Canonical Offer: UbuntuServer Sku:16.04.0-LTS
   *
   * @param region Indicates the Region. Ex.:Region.US_EAST
   * @param publisher A filter to use to select only images of the specified publisher. If is blank
   *     or null, all publisher will be returned
   * @return
   */
  public List<ImageReference> getAllMachineImages(Region region, String publisher) {

    assert region != null;

    List<ImageReference> listImageReferente = new ArrayList<>();

    PagedList<VirtualMachinePublisher> listByRegion =
        this.azure.virtualMachineImages().publishers().listByRegion(region);

    for (VirtualMachinePublisher vmp : listByRegion) {

      if (StringUtils.isNotBlank(publisher) && !vmp.name().equalsIgnoreCase(publisher)) {
        continue;
      }

      for (VirtualMachineOffer offer : vmp.offers().list()) {
        for (VirtualMachineSku sku : offer.skus().list()) {
          for (VirtualMachineImage image : sku.images().list()) {

            ImageReference ir =
                new ImageReference()
                    .withPublisher(vmp.name())
                    .withOffer(offer.name())
                    .withSku(sku.name())
                    .withVersion(image.version());

            listImageReferente.add(ir);
          }
        }
      }
    }

    return listImageReferente;
  }

  /**
   * Stop virtual machines, but still incurring compute charges.
   *
   * @return
   * @param resourceGroupName
   */
  public void stop(String resourceGroupName) {

    for (VirtualMachine virtualMachine : this.azure.virtualMachines().list()) {

      if (!virtualMachine.resourceGroupName().equalsIgnoreCase(resourceGroupName)) {
        continue;
      }

      log.info(
          String.format("Vm %s Powerstate %s", virtualMachine.id(), virtualMachine.powerState()));

      PowerState powerState = virtualMachine.powerState();

      // fixme: powerState return null when the virtual machine is stoped.
      powerState = (powerState == null) ? PowerState.DEALLOCATED : powerState;
      if (!powerState.equals(PowerState.DEALLOCATED)) {
        virtualMachine.powerOff();
      }
    }
  }

  /** @param resourceGroupName. */
  public void restart(String resourceGroupName) {

    for (VirtualMachine virtualMachine : this.azure.virtualMachines().list()) {

      if (!virtualMachine.resourceGroupName().equalsIgnoreCase(resourceGroupName)) {
        continue;
      }

      log.info(
          String.format("Vm %s Powerstate %s", virtualMachine.id(), virtualMachine.powerState()));

      PowerState powerState = virtualMachine.powerState();

      // fixme: powerState return null when the virtual machine is stoped.
      powerState = (powerState == null) ? PowerState.DEALLOCATED : powerState;

      if (powerState.equals(PowerState.DEALLOCATED)) {
        virtualMachine.start();

      } else if (powerState.equals(PowerState.RUNNING)) {
        virtualMachine.restart();
      }

      log.info(
          String.format(
              "Vm %s ProvisioningState %s",
              virtualMachine.id(), virtualMachine.provisioningState()));
    }
  }

  /**
   * Start all vm of azure account.
   *
   * @param resourceGroupName
   */
  public void start(String resourceGroupName) {

    for (VirtualMachine virtualMachine : this.azure.virtualMachines().list()) {

      if (!virtualMachine.resourceGroupName().equalsIgnoreCase(resourceGroupName)) {
        continue;
      }

      log.info(
          String.format("Vm %s Powerstate %s", virtualMachine.id(), virtualMachine.powerState()));

      PowerState powerState = virtualMachine.powerState();

      // fixme: powerState return null when the virtual machine is stoped.
      powerState = (powerState == null) ? PowerState.DEALLOCATED : powerState;

      if (powerState.equals(PowerState.DEALLOCATED)) {
        virtualMachine.start();
      }

      log.info(
          String.format("Vm %s PowerState %s", virtualMachine.id(), virtualMachine.powerState()));
    }
  }

  public Region[] getRegions() {
    assert this.azure != null;

    for (Region region : Region.values()) {
      log.info(String.format("Region: %s", region));
    }

    return Region.values();
  }

  /**
   * Return all the virtual machines that exists in the actual account.
   *
   * @return
   */
  public PagedList<VirtualMachine> getListVirtualMachines() {
    return this.azure.virtualMachines().list();
  }
}
