package cloudos.microsoft;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document(collection = "azure_pricing")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AzurePricing {

  @Id
  private String id;

  private String currency;
  private String locale;
  private Boolean isTaxIncluded;
  private List<AzureMeter> meters;
  private Boolean active;
  private String offerDurableId;
  private String regionInfo;
  private Boolean retired;


  public AzurePricing(
      String currency,
      String locale,
      Boolean isTaxIncluded,
      String offerDurableId,
      String regionInfo,
      Boolean retired) {

    this();

    this.currency = currency;
    this.locale = locale;
    this.isTaxIncluded = isTaxIncluded;
    this.offerDurableId = offerDurableId;
    this.regionInfo = regionInfo;
    this.retired = retired;
  }

  public AzurePricing(
      String currency, String locale, Boolean isTaxIncluded, List<AzureMeter> meters) {
    this();

    this.currency = currency;
    this.locale = locale;
    this.isTaxIncluded = isTaxIncluded;
    this.meters = meters;
  }
}
