package cloudos.microsoft;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import com.microsoft.windowsazure.exception.ServiceException;
import com.microsoft.windowsazure.management.models.RoleSizeListResponse;

import lombok.extern.log4j.Log4j2;

/** Created by gleimar on 24/11/2016. */
@Component
@Log4j2
public class AzureInstanceService {

  @Autowired
  private AzureManagementClassic azureManagementClassic;
  @Autowired
  AzureMapperUtils azureMapperUtils;

  public void fetchInstanceDataIntoDatabase() {
    try {
      List<RoleSizeListResponse.RoleSize> rolesSize = azureManagementClassic.getRolesSize();

      for (RoleSizeListResponse.RoleSize roleSize : rolesSize) {
        azureMapperUtils.convertToMongoAzureInstance(roleSize);
      }

    } catch (ServiceException | ParserConfigurationException | SAXException | IOException | URISyntaxException e) {
      log.error("", e);
    }
  }
}
