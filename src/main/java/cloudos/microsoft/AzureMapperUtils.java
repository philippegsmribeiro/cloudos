package cloudos.microsoft;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.microsoft.windowsazure.management.models.RoleSizeListResponse;

import cloudos.Providers;
import cloudos.instances.InstanceService;
import cloudos.microsoft.AzureRest.Meter;
import cloudos.microsoft.AzureRest.RateCard;

/**
 * A utililty class to convert rest object to mongo object.
 *
 * @author gleimar
 */
@Service
public class AzureMapperUtils {

  @Resource
  InstanceService instanceService;
  @Resource
  AzureInstanceRepository azureInstanceRepository;

  public AzureInstance convertToMongoAzureInstance(RoleSizeListResponse.RoleSize roleSize) {
    AzureInstance azureInstance = null;
    // verify if already exists
    cloudos.models.Instance cloudOsInstance =
        instanceService.findByProviderAndProviderId(Providers.MICROSOFT_AZURE, roleSize.getName());
    if (cloudOsInstance != null) {
      // get from mongo and update
      azureInstance = azureInstanceRepository.findByInstance(cloudOsInstance);
    } else {
      // create the new register
      azureInstance = new AzureInstance();
      cloudOsInstance = new cloudos.models.Instance();
      cloudOsInstance.setIncludedDate(new Date());
      cloudOsInstance.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
      cloudOsInstance.setProviderId(roleSize.getName());
      azureInstance.setInstance(cloudOsInstance);
    }
    // update info

    // update custom info
    azureInstance.setCores(roleSize.getCores());
    azureInstance.setLabel(roleSize.getLabel());
    azureInstance.setMaxDataDiskCount(roleSize.getMaxDataDiskCount());
    azureInstance.setMemoryInMb(roleSize.getMemoryInMb());
    azureInstance.setSupportedByVirtualMachines(roleSize.isSupportedByVirtualMachines());
    azureInstance.setSupportedByWebWorkerRoles(roleSize.isSupportedByWebWorkerRoles());
    azureInstance
        .setVirtualMachineResourceDiskSizeInMb(roleSize.getVirtualMachineResourceDiskSizeInMb());
    azureInstance.setWebWorkerResourceDiskSizeInMb(roleSize.getWebWorkerResourceDiskSizeInMb());

    // save
    instanceService.save(cloudOsInstance);
    azureInstanceRepository.save(azureInstance);
    return azureInstance;
  }

  /**
   * Convert the parameters provided into the object to store in the database.
   *
   * @param meter
   * @return
   */
  public static AzureMeter convertToMongoDataMeter(Meter meter) {

    return new AzureMeter(meter.meterId, meter.meterName, meter.meterCategory,
        meter.meterSubCategory, meter.unit, meter.meterRates, meter.effectiveDate,
        meter.includedQuantity, meter.meterRegion);
  }

  /**
   * Convert the parameters provided into the object to store in the database.
   *
   * @param rateCard
   * @param offerDurableId
   * @param regionInfo
   * @param retired
   * @return
   */
  public static AzurePricing convertToMongoDataPricing(RateCard rateCard, String offerDurableId,
      String regionInfo, Boolean retired) {
    AzurePricing azurePricing = new AzurePricing(rateCard.currency, rateCard.locale,
        rateCard.isTaxIncluded, offerDurableId, regionInfo, retired);

    List<AzureMeter> azureMeters = new ArrayList<>();

    for (Meter meter : rateCard.meters) {
      azureMeters.add(convertToMongoDataMeter(meter));
    }

    azurePricing.setMeters(azureMeters);

    return azurePricing;
  }
}
