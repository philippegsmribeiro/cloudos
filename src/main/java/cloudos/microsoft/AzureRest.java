package cloudos.microsoft;

import cloudos.utils.ReflectionToJson;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.microsoft.azure.AzureEnvironment;
import com.microsoft.azure.credentials.ApplicationTokenCredentials;
import com.microsoft.azure.credentials.AzureTokenCredentialsInterceptor;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * A utility class to improve "rest call" that not have in the default SDK provided by microsoft.
 *
 * @author gleimar
 */
@Log4j2
public class AzureRest {

  private static final String ARM_ENDPOINT = "https://management.azure.com/";
  private static final HttpLoggingInterceptor.Level LOG_HTTP = HttpLoggingInterceptor.Level.BODY;

  private String defaultSubscription;

  private ApplicationTokenCredentials credentials;

  /**
   * Make a azure connection.
   *
   * @param client
   * @param tenant
   * @param key
   * @param subscription
   */
  public AzureRest(String client, String tenant, String key, String subscription) {

    this.defaultSubscription = subscription;

    this.initCredential(client, tenant, key);
  }

  /**
   * Make a azure connection with default subscription.
   *
   * @param client
   * @param tenant
   * @param key
   * @throws IOException
   */
  public AzureRest(String client, String tenant, String key) throws IOException {

    this.initCredential(client, tenant, key);

    this.defaultSubscription = getDefaultSubcription();
  }

  /**
   * Make a azure connection with credetial.
   *
   * @param credentials
   */
  public AzureRest(ApplicationTokenCredentials credentials) {
    this.credentials = credentials;
  }

  private void initCredential(String client, String tenant, String key) {
    credentials = new ApplicationTokenCredentials(client, tenant, key, AzureEnvironment.AZURE);
  }

  public String getDefaultSubcription() throws IOException {
    if (this.defaultSubscription == null) {
      this.defaultSubscription = searchDefaultSubscription();
    }
    return this.defaultSubscription;
  }

  /**
   * Get de default subscription of the actual azure connection.
   *
   * @return
   * @throws IOException
   */
  private String searchDefaultSubscription() throws IOException {

    Retrofit retrofit = buildRetrofit();

    AzureRestCall aRestCall = retrofit.create(AzureRestCall.class);

    Call<SubscriptionResponse> subscriptions = aRestCall.subscriptions();
    SubscriptionResponse subscriptionResponse = subscriptions.execute().body();

    return subscriptionResponse.value[0].subscriptionId;
  }

  /**
   * @param offerDurableId A valid Offer ID code (e.g., MS-AZR-0026P). See Microsoft Azure Offer
   *        Details for more information on the list of available Offer IDs, country/region
   *        availability, and billing currency. The Offer ID parameter consists of the “MS-AZR-“
   *        prefix, plus the Offer ID number. Ex.: 'MS-AZR-0003p'
   *        <p>
   *        For a list of offerDurableId: <a href=
   *        "https://azure.microsoft.com/en-us/support/legal/offer-details/">https://azure.microsoft.com/en-us/support/legal/offer-details/</a>
   * @param currency The currency in which the resource rates need to be provided. Ex.:'USD'
   * @param locale The culture in which the resource metadata needs to be localized. Ex.:'en-US'
   * @param regionInfo The 2 letter ISO code where the offer was purchased. Ex.:'US'
   * @return
   * @throws IOException
   */
  public RateCard searchRateCard(String offerDurableId, String currency, String locale,
      String regionInfo) throws IOException {

    Retrofit retrofit = buildRetrofit();

    AzureRestCall aRestCall = retrofit.create(AzureRestCall.class);

    String stringGetRequest =
        "/subscriptions/%s/providers/Microsoft.Commerce/RateCard?api-version=2015-06-01-preview&$filter=OfferDurableId eq '%s' and Currency eq '%s' and Locale eq '%s' and RegionInfo eq '%s'";

    stringGetRequest = String.format(stringGetRequest, this.getDefaultSubcription(), offerDurableId,
        currency, locale, regionInfo);

    Call<RateCard> searchRateCard = aRestCall.searchRateCard(stringGetRequest);

    RateCard rateCardResponse = searchRateCard.execute().body();

    return rateCardResponse;
  }

  /**
   * Lists the metric values for a resource. <br>
   * <br>
   * The $filter is used to reduce the set of metric data returned. Some common properties for this
   * expression will be:<br>
   * name.value, aggregationType, startTime, endTime, timeGrain. The filter expression uses these
   * properties with comparison operators (eg. eq, gt, lt)<br>
   * and multiple expressions can be combined with parentheses and 'and/or' operators. <br>
   * <br>
   * NOTE: When a metrics query comes in with multiple metrics, but with no aggregation types
   * defined, the service will pick the Primary aggregation type of the first metrics to be used as
   * the default aggregation type for all the metrics. <br>
   * More information: https://docs.microsoft.com/en-us/rest/api/monitor/metrics <br>
   *
   * @param resourceUri Example of resourceUri :
   *        /subscriptions/1aa211a2-3b81-4a80-bc2f-7d387fab9dd0/resourceGroups/regrkafka/providers/Microsoft.Compute/virtualMachines/cosnodea
   *        <br>
   * @param filter Some example filter expressions are: <br>
   *        - $filter=(name.value eq 'RunsSucceeded') and aggregationType eq 'Total' and startTime
   *        eq 2016-02-20 and endTime eq 2016-02-21 and timeGrain eq duration'PT1M', <br>
   *        - $filter=(name.value eq 'RunsSucceeded') and (aggregationType eq 'Total' or
   *        aggregationType eq 'Average') and startTime eq 2016-02-20 and endTime eq 2016-02-21 and
   *        timeGrain eq duration'PT1H', <br>
   *        - $filter=(name.value eq 'ActionsCompleted' or name.value eq 'RunsSucceeded') and
   *        (aggregationType eq 'Total' or aggregationType eq 'Average') and startTime eq 2016-02-20
   *        and endTime eq 2016-02-21 and timeGrain eq duration'PT1M'. <br>
   *        <br>
   * @return
   * @throws IOException
   */
  public MetricValueResource[] metricValueResource(String resourceUri, String filter)
      throws IOException {
    Retrofit retrofit = buildRetrofit();

    AzureRestCall aRestCall = retrofit.create(AzureRestCall.class);

    String stringGetRequest =
        "%s/providers/microsoft.insights/metrics?api-version=2016-09-01&$filter=%s";

    stringGetRequest = String.format(stringGetRequest, resourceUri, filter);

    Call<MetricValueResourceResponse> metricValueResourceResponseCall =
        aRestCall.metricValueResource(stringGetRequest);

    Response<MetricValueResourceResponse> execute = metricValueResourceResponseCall.execute();

    MetricValueResourceResponse metricValueResourceResponse = execute.body();

    if (metricValueResourceResponse == null) {
      log.info(String.format("Error Message: %s", execute.message()));
    }

    return metricValueResourceResponse == null ? null : metricValueResourceResponse.value;
  }

  /**
   * Lists the metric definitions for the resourceUri parameter. <br>
   * More information : https://docs.microsoft.com/en-us/rest/api/monitor/metricdefinitions <br>
   * Example of resourceUri :
   * /subscriptions/1aa211a2-3b81-4a80-bc2f-7d387fab9dd0/resourceGroups/regrkafka/providers/Microsoft.Compute/virtualMachines/cosnodea
   *
   * @param resourceUri
   * @return
   * @throws IOException
   */
  public MetricDefinition[] metricDefinitionForResource(String resourceUri) throws IOException {
    Retrofit retrofit = buildRetrofit();

    AzureRestCall aRestCall = retrofit.create(AzureRestCall.class);

    String stringGetRequest =
        "%s/providers/microsoft.insights/metricdefinitions?api-version=2016-03-01";

    stringGetRequest = String.format(stringGetRequest, resourceUri);

    Call<MetricDefinitionResponse> metricDefinitionResponseCall =
        aRestCall.metricDefinitionForResource(stringGetRequest);

    MetricDefinitionResponse metricDefinitionResponse =
        metricDefinitionResponseCall.execute().body();

    return metricDefinitionResponse.value;
  }

  /**
   * Get information about the virtual machine
   *
   * <p>
   * One of those information is a list of extension that exists in then virtual machine
   *
   * <p>
   * For mode detail :
   * https://docs.microsoft.com/en-us/rest/api/virtualmachinescalesets/get-the-instance-view-of-a-vm
   *
   * @param resourceGroup
   * @param vmInstanceId
   * @return
   * @throws IOException
   */
  public InstanceViewOfVirtualMachine getInstanceViewOfVirtualMachine(String resourceGroup,
      String vmInstanceId) throws IOException {
    Retrofit retrofit = buildRetrofit();

    AzureRestCall aRestCall = retrofit.create(AzureRestCall.class);

    String stringGetRequest =
        "subscriptions/%s/resourceGroups/%s/providers/Microsoft.Compute/VirtualMachines/%s/instanceView?api-version=2016-04-30-preview";

    stringGetRequest =
        String.format(stringGetRequest, this.getDefaultSubcription(), resourceGroup, vmInstanceId);

    Call<InstanceViewOfVirtualMachine> instanceViewOfVirtualMachine =
        aRestCall.getInstanceViewOfVirtualMachine(stringGetRequest);

    InstanceViewOfVirtualMachine viewOfVirtualMachine =
        instanceViewOfVirtualMachine.execute().body();

    return viewOfVirtualMachine;
  }

  /**
   * Build a default retrofit client for management calls.
   *
   * @return
   */
  private Retrofit buildRetrofit() {
    OkHttpClient.Builder httpClientBuilder = new Builder();
    httpClientBuilder.addInterceptor(new HttpLoggingInterceptor().setLevel(LOG_HTTP));
    httpClientBuilder.addInterceptor(new AzureTokenCredentialsInterceptor(credentials));

    OkHttpClient client = httpClientBuilder.build();

    Retrofit retrofit = new Retrofit.Builder().baseUrl(ARM_ENDPOINT)
        .addConverterFactory(GsonConverterFactory.create()).client(client).build();
    return retrofit;
  }

  public interface AzureRestCall {
    @GET("/subscriptions?api-version=2014-04-01")
    Call<SubscriptionResponse> subscriptions();

    @GET
    Call<RateCard> searchRateCard(@Url String url);

    @GET
    Call<MetricDefinitionResponse> metricDefinitionForResource(@Url String url);

    @GET
    Call<MetricValueResourceResponse> metricValueResource(@Url String url);

    @GET
    Call<InstanceViewOfVirtualMachine> getInstanceViewOfVirtualMachine(@Url String url);
  }

  public static class Extension {
    public final String name;
    public final String type;

    public Extension(String name, String type) {
      this.name = name;
      this.type = type;
    }
  }

  public static class InstanceViewOfVirtualMachine {
    public final Extension[] extensions;

    public InstanceViewOfVirtualMachine(Extension[] extensions) {
      this.extensions = extensions;
    }

    @Override
    public String toString() {
      return new Gson().toJson(this);
    }
  }

  @AllArgsConstructor
  public static class DataPoint {
    public final String timeStamp;
    public final String average;
    public final String minimum;
    public final String maximum;
    public final String total;
    public final String count;

    @Override
    public String toString() {
      return ReflectionToJson.toString(this);
    }
  }

  @AllArgsConstructor
  public static class MetricValueResource {
    public String id;
    public String type;
    public Map<String, String> name;
    public String unit;
    public DataPoint[] data;

    @Override
    public String toString() {
      return ReflectionToJson.toString(this);
    }
  }

  public static class MetricValueResourceResponse {
    public MetricValueResource[] value;
  }

  @AllArgsConstructor
  public static class MetricDefinition {
    public final String resourceUri;
    public final String resourceId;
    public final String unit;
    public final Map<String, String> name;

    @Override
    public String toString() {
      return ReflectionToJson.toString(this);
    }
  }

  public static class MetricDefinitionResponse {

    private MetricDefinition[] value;
  }

  @AllArgsConstructor
  public static class RateCard {
    @SerializedName("Currency")
    public final String currency;

    @SerializedName("Locale")
    public final String locale;

    @SerializedName("IsTaxIncluded")
    public final Boolean isTaxIncluded;

    @SerializedName("Meters")
    public final Meter[] meters;
  }

  @AllArgsConstructor
  public static class Meter {
    @SerializedName("MeterId")
    public final String meterId;

    @SerializedName("MeterName")
    public final String meterName;

    @SerializedName("MeterCategory")
    public final String meterCategory;

    @SerializedName("MeterSubCategory")
    public final String meterSubCategory;

    @SerializedName("Unit")
    public final String unit;

    @SerializedName("MeterRates")
    public final Map<String, String> meterRates;

    @SerializedName("EffectiveDate")
    public final String effectiveDate;

    @SerializedName("IncludedQuantity")
    public final BigDecimal includedQuantity;

    @SerializedName("MeterRegion")
    public final String meterRegion;
  }

  @AllArgsConstructor
  public static class SubscriptionResponse {
    public Subscription[] value;
  }

  @AllArgsConstructor
  public static class Subscription {
    public final String subscriptionId;
    public final String displayName;
  }
}
