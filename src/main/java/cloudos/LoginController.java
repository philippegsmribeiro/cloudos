package cloudos;

import java.security.Principal;
import java.util.Collections;
import java.util.Map;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LoginController {

  /*
   * Add the login redirect url
   */
  @RequestMapping("/login")
  public String index() {
    return "login";
  }

  /*
   * Add the login redirect url
   */
  @RequestMapping(value = "/login", method = RequestMethod.POST)
  public Principal login(Principal user) {
    return user;
  }

  @RequestMapping("/token")
  @ResponseBody
  public Map<String, String> token(HttpSession session) {
    return Collections.singletonMap("token", session.getId());
  }

  @RequestMapping("/logout")
  public void logout() {}
}
