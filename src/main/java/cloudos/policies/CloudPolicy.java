package cloudos.policies;

import cloudos.Providers;
import cloudos.config.CloudOSJson;
import cloudos.models.network.CloudosSubnet;
import cloudos.models.network.CloudosVpc;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;

@CloudOSJson
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonSubTypes({
    @JsonSubTypes.Type(value = SpotBidderPolicy.class, name = "spotbidder"),
    @JsonSubTypes.Type(value = AutoscalerPolicy.class, name = "autoscaler")
})
@ApiModel(
    value = "CloudPolicy",
    discriminator = "type",
    subTypes = { AutoscalerPolicy.class, SpotBidderPolicy.class}
)
public abstract class CloudPolicy {

  @Id
  protected String id;
  protected String cloudGroupId;
  protected Providers provider;
  protected String name;
  protected String region;
  protected String description;
  protected CloudosVpc vpc;
  protected CloudosSubnet subnet;

}
