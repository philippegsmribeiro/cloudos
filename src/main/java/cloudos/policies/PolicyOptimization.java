package cloudos.policies;

public enum PolicyOptimization {

  AVAILABILITY,
  COST_SAVINGS

}
