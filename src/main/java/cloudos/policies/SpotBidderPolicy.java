package cloudos.policies;

import io.swagger.annotations.ApiModel;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "cloud_spot_bidder_policies")
@ApiModel(value = "SpotBidderPolicy", parent = CloudPolicy.class,
    description = "Create a new SpotBidder Policy")
public class SpotBidderPolicy extends CloudPolicy {

  private Double spotInstancesPercentage;
  private List<String> spotTypes;
  private PolicyOptimization optimization;
  private String availabilityZones;

}
