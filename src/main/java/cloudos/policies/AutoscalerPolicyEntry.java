package cloudos.policies;

import cloudos.machinelearning.autoscaler.ResourceCategory;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AutoscalerPolicyEntry {

  private ResourceCategory resourceCategory;
  private Double threshold;
  private String instanceType;
  private String imageType;
  private Boolean monitoring;
  private Boolean instanceProtection;
  private Integer startingGroupSize;

}
