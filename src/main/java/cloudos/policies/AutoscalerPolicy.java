package cloudos.policies;

import io.swagger.annotations.ApiModel;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "cloud_autoscale_policies")
@ApiModel(value = "AutoscalerPolicy", parent = CloudPolicy.class,
    description = "Create a new Autoscaler Policy")
public class AutoscalerPolicy extends CloudPolicy {

  // The AutoscalerPolicy is a list of Autoscaler policy entries
  private List<AutoscalerPolicyEntry> autoscalePolicies;

}
