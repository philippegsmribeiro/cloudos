package cloudos.policies;

import cloudos.Providers;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

interface CloudPolicyRepository extends MongoRepository<CloudPolicy, String> {

  List<CloudPolicy> findByProvider(Providers provider);
  
  List<CloudPolicy> findByRegion(String region);
  
  List<CloudPolicy> findByCloudGroupId(String cloudGroupId);

}
