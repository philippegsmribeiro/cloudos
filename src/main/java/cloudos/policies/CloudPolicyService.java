package cloudos.policies;

import cloudos.models.Instance;
import cloudos.models.cloudgroup.CloudGroup;
import cloudos.models.cloudgroup.CloudGroupService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import javax.validation.constraints.NotNull;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Cloud Policy service is responsible for managing cloud policies across the CloudTown's platform.
 */
@Service
public class CloudPolicyService {

  @Autowired
  private CloudPolicyRepository cloudPolicyRepository;

  /* Add the Cloud Group Service so we can manipulate the Cloud Groups */
  @Autowired
  private CloudGroupService cloudGroupService;

  /**
   * Save a new CloudPolicy, either a Autoscaler or SpotBidder policy.
   *
   * @param cloudGroupId the Cloud Group id
   * @param cloudPolicy the Cloud Policy to be added
   * @return a saved cloud policy
   * @throws NoSuchElementException in case the type of the cloud policy is not correct
   */
  public CloudPolicy save(@NotNull String cloudGroupId, @NotNull CloudPolicy cloudPolicy)
      throws NoSuchElementException {
    // find the cloud group
    CloudGroup cloudGroup = cloudGroupService.describeCloudGroup(cloudGroupId);
    if (cloudGroup == null) {
      return null;
    }
    // set the cloud group to its policy
    cloudPolicy.setCloudGroupId(cloudGroupId);
    cloudPolicy = this.cloudPolicyRepository.save(cloudPolicy);
    // update the cloud group
    if (cloudPolicy instanceof AutoscalerPolicy) {
      cloudGroup.setAutoscalerPolicy((AutoscalerPolicy) cloudPolicy);
    } else if (cloudPolicy instanceof SpotBidderPolicy) {
      cloudGroup.setSpotBidderPolicy((SpotBidderPolicy) cloudPolicy);
    } else {
      throw new NoSuchElementException("Invalid subclass of the type CloudPolicy");
    }
    // update the cloud group
    this.cloudGroupService.updateCloudGroup(cloudGroup);
    return cloudPolicy;
  }

  /**
   * List all the cloud policies frm a particular cloud group.
   *
   * @param cloudGroupId the cloud group
   * @return a list of cloud policies.
   */
  public List<CloudPolicy> list(@NotNull String cloudGroupId) {
    return this.cloudPolicyRepository.findByCloudGroupId(cloudGroupId);
  }


  /**
   * List all autoscaler policies.
   * 
   * @param cloudGroupId the cloud group
   * @return list of policies
   */
  public List<AutoscalerPolicy> listAutoscalerPolicy(@NotNull String cloudGroupId) {
    List<CloudPolicy> allPolicies = this.cloudPolicyRepository.findByCloudGroupId(cloudGroupId);
    if (CollectionUtils.isNotEmpty(allPolicies)) {
      return allPolicies.stream()
                        .filter(policy -> policy instanceof AutoscalerPolicy)
                        .map(policy -> (AutoscalerPolicy) policy)
                        .collect(Collectors.toList());
    }
    return Collections.emptyList();
  }
  
  /**
   * List all SpotBidder policies.
   * 
   * @param cloudGroupId the cloud group
   * @return list of policies
   */
  public List<SpotBidderPolicy> listSpotBidderPolicy(@NotNull String cloudGroupId) {
    List<CloudPolicy> allPolicies = this.cloudPolicyRepository.findByCloudGroupId(cloudGroupId);
    if (CollectionUtils.isNotEmpty(allPolicies)) {
      return allPolicies.stream()
                        .filter(policy -> policy instanceof SpotBidderPolicy)
                        .map(policy -> (SpotBidderPolicy) policy)
                        .collect(Collectors.toList());
    }
    return Collections.emptyList();
  }

  /**
   * Describe a cloud policy, if it exists.
   *
   * @param cloudGroupId the ID of the cloud group
   * @param id the ID of the policy
   * @return the cloud policy, if it exists
   */
  public CloudPolicy describe(@NotNull String cloudGroupId, @NotNull String id) {
    CloudPolicy cloudPolicy = this.cloudPolicyRepository.findOne(id);
    if (cloudPolicy != null && cloudPolicy.getCloudGroupId().equals(cloudGroupId)) {
      return cloudPolicy;
    }
    return null;
  }

  /**
   * Update an existing cloud policy.
   *
   * @param cloudPolicy the cloud policy to be updated
   * @return the updated cloud policy
   */
  public CloudPolicy update(@NotNull String cloudGroupId, @NotNull String id,
                            @NotNull CloudPolicy cloudPolicy) {
    // find the cloud group
    CloudPolicy policy = this.describe(cloudGroupId, id);
    if (cloudPolicy == null) {
      return null;
    }
    // use the new policy, after setting its ID to the old policy
    cloudPolicy.setId(policy.getId());
    return this.cloudPolicyRepository.save(cloudPolicy);
  }

  /**
   * Delete the policy, and update the cloud group.
   * 
   * @param cloudGroupId the ID of the cloud group
   * @param id the ID of the policy
   * @return the status of the delete operation
   */
  public boolean delete(@NotNull String cloudGroupId, @NotNull String id) {
    // find the cloud group
    CloudGroup cloudGroup = this.cloudGroupService.describeCloudGroup(cloudGroupId);
    if (cloudGroup == null) {
      return false;
    }
    CloudPolicy cloudPolicy = this.cloudPolicyRepository.findOne(id);
    if (cloudPolicy == null) {
      return false;
    }
    // delete the policy
    this.cloudPolicyRepository.delete(cloudPolicy);
    // update the cloud group
    if (cloudPolicy instanceof AutoscalerPolicy) {
      cloudGroup.setAutoscalerPolicy(null);
    } else if (cloudPolicy instanceof SpotBidderPolicy) {
      cloudGroup.setSpotBidderPolicy(null);
    } else {
      return false;
    }
    // update the cloud group
    this.cloudGroupService.updateCloudGroup(cloudGroup);
    return true;
  }


  /**
   * Retrieve all applicable autoscaler policies for the cloudgroup/instance.
   * 
   * @param instance analysed
   * @return list of autoscaler policy entry
   */
  public List<AutoscalerPolicyEntry> retrieveApplicableAutoscalerPolicies(Instance instance) {
    List<AutoscalerPolicyEntry> applicablePolicies = new ArrayList<>();

    List<AutoscalerPolicy> policies = this.listAutoscalerPolicy(instance.getCloudGroupId());
    if (CollectionUtils.isNotEmpty(policies)) {
      for (AutoscalerPolicy policy : policies) {

        // if policy has provider, it has to be the same of the instance
        if (policy.getProvider() != null
            && policy.getProvider() != instance.getProvider()) {
          continue;
        }

        // if policy has region, it has to be the same of the instance
        if (policy.getRegion() != null
            && !policy.getRegion().equals(instance.getRegion())) {
          continue;
        }

        // TODO: validate vpc and subnet


        List<AutoscalerPolicyEntry> autoscalePolicyEntries =
            policy.getAutoscalePolicies();
        if (CollectionUtils.isNotEmpty(autoscalePolicyEntries)) {
          for (AutoscalerPolicyEntry entry : autoscalePolicyEntries) {

            // if entry has instanceType, it has to be the same of the instance
            if (entry.getInstanceType() != null
                && !entry.getInstanceType().equals(instance.getMachineType())) {
              continue;
            }

            // if entry has imageType, it has to be the same of the instance
            if (entry.getImageType() != null
                && !entry.getImageType().equals(instance.getImageType())) {
              continue;
            }

            // TODO: verify validation for others

            applicablePolicies.add(entry);
          }
        }
      }
    }
    return applicablePolicies;
  }
  
  /**
   * Retrieve all applicable spotBidder policies for the cloudgroup/instance.
   * 
   * @param instance analysed
   * @return list of spotbidder policy entry
   */
  public List<SpotBidderPolicy> retrieveApplicableSpotBidderPolicies(Instance instance) {
    List<SpotBidderPolicy> applicablePolicies = new ArrayList<>();
    
    List<SpotBidderPolicy> policies = this.listSpotBidderPolicy(instance.getCloudGroupId());
    if (CollectionUtils.isNotEmpty(policies)) {
      for (SpotBidderPolicy policy : policies) {
        
        // if policy has provider, it has to be the same of the instance
        if (policy.getProvider() != null
            && policy.getProvider() != instance.getProvider()) {
          continue;
        }
        
        // if policy has region, it has to be the same of the instance
        if (policy.getRegion() != null
            && !policy.getRegion().equals(instance.getRegion())) {
          continue;
        }
        
        // TODO: validate vpc and subnet
        
        applicablePolicies.add(policy);
      }
    }
    return applicablePolicies;
  }
}
