package cloudos.machinelearning;

import aima.core.search.csp.Assignment;
import aima.core.search.csp.CSP;
import aima.core.search.csp.DomainRestoreInfo;
import aima.core.search.csp.SolutionStrategy;
import aima.core.search.csp.Variable;

/**
 * Implement the Backtracking Algorithm to solve constraint problems.
 *
 * @author philipperibeiro
 */
public class BacktrackingStrategy extends SolutionStrategy {

  /**
   * Define the solve method, responsible for solving the CSP problem.
   *
   * @param csp the CSP problem to be solved
   */
  @Override
  public Assignment solve(CSP csp) {
    return recursiveBackTrackingSearch(csp, new Assignment());
  }

  /**
   * Implement the recursive Backtracking Algorithm to solve CSP problems.
   *
   * @param csp the current CSP problem to be solved
   * @param assignment the current assignment
   * @return An assignment that solves the CSP
   */
  private Assignment recursiveBackTrackingSearch(CSP csp, Assignment assignment) {
    Assignment result = null;
    if (assignment.isComplete(csp.getVariables())) {
      result = assignment;
    } else {
      Variable var = this.selectUnassignedVariable(assignment, csp);
      for (Object value : this.orderDomainValues(var, assignment, csp)) {
        assignment.setAssignment(var, value);
        this.fireStateChanged(assignment, csp);
        if (assignment.isConsistent(csp.getConstraints(var))) {
          DomainRestoreInfo info = this.inference(var, assignment, csp);
          if (!info.isEmpty()) {
            this.fireStateChanged(csp);
          }
          if (!info.isEmptyDomainFound()) {
            result = this.recursiveBackTrackingSearch(csp, assignment);
            if (result != null) {
              break;
            }
          }
          info.restoreDomains(csp);
        }
        assignment.removeAssignment(var);
      }
    }
    return result;
  }

  /**
   * Define the inference, which checks if the current assignment is possible.
   *
   * @param var the current variable to be assigned
   * @param assignment the current assignment
   * @param csp the CSP to be solved
   * @return A way to restore the current Domain
   */
  protected DomainRestoreInfo inference(Variable var, Assignment assignment, CSP csp) {
    return new DomainRestoreInfo().compactify();
  }

  /**
   * Put the Domain values in order.
   *
   * @param var the current variable being assigned
   * @param assignment the current assignment state
   * @param csp the current CSP to be solved
   * @return an Iterable object
   */
  protected Iterable<?> orderDomainValues(Variable var, Assignment assignment, CSP csp) {
    return csp.getDomain(var);
  }

  /**
   * Selects one unassigned variable to the backtracking algorithm can proceed.
   *
   * @param assignment the current assignment state
   * @param csp the current CSP to be solved
   * @return an unassigned variable
   */
  protected Variable selectUnassignedVariable(Assignment assignment, CSP csp) {
    for (Variable var : csp.getVariables()) {
      if (!(assignment.hasAssignmentFor(var))) {
        return var;
      }
    }
    return null;
  }
}
