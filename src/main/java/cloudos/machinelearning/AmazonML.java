package cloudos.machinelearning;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

import cloudos.amazon.AmazonInstance;
import cloudos.amazon.AmazonInstanceRepository;
import cloudos.pricing.AmazonPricingManagement;
import cloudos.pricing.PricingValue;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

/**
 * AmazonML.
 *
 * @author philipperibeiro
 */
@Log4j2
@Data
@NoArgsConstructor
public class AmazonML {

  private static final String ONDEMAND_INSTANCE = "OnDemand";
  private static final String RESERVED_INSTANCE = "Reserved";
  private static final String PRODUCT_FAMILY = "Compute Instance";

  private ArrayList<String> featureNames = new ArrayList<>();

  @Autowired
  private AmazonInstanceRepository instanceRepository;
  @Autowired
  AmazonPricingManagement pricingManagement;

  /**
   * @param instanceRepository
   * @param pricingManagement
   */
  public AmazonML(AmazonInstanceRepository instanceRepository,
      AmazonPricingManagement pricingManagement) {
    this.instanceRepository = instanceRepository;
    this.pricingManagement = pricingManagement;
  }

  /**
   * Get a sample from the MongoDB `amazon_instances` collection, instead of fetching all the
   * entries
   *
   * @param start: The starting point of the query
   * @param end: the ending point of the query
   * @return List<AmazonInstance>: A list of AmazonInstance objects.
   */
  public List<AmazonInstance> getSample(int start, int end) {
    return this.instanceRepository.findTop100ByProductFamily(PRODUCT_FAMILY,
        new PageRequest(start, end));
  }

  /** Given a list of */
  public Map<Double, List<Double>> featureExtractor(List<AmazonInstance> instances) {
    log.info("--------------- Feature Extractor -------------------");
    /* Define a vector between the observed points and the y-pricing */
    Map<Double, List<Double>> map = new TreeMap<>();

    /* Loop over all the instances, since we want to find the weights */
    for (AmazonInstance instance : instances) {
      List<Double> featureVector = instance.getFeatureVector();
      if (!featureVector.isEmpty()) {
        PricingValue pricingForInstance =
            pricingManagement.getPricingForInstance(instance.getInstance());
        if (pricingForInstance != null) {
          map.put(pricingForInstance.getValue(), featureVector);
        }
      }
    }
    log.info("-----------------------------------------------------");
    return map;
  }

  /** */
  public SimpleRegression modeling(List<Vector2D> points) {
    log.info("-------------------- Modeling -----------------------");
    SimpleRegression simpleRegression = new SimpleRegression(true);
    for (Vector2D point : points) {
      simpleRegression.addData(point.getX(), point.getY());
    }
    log.info("-----------------------------------------------------");
    return simpleRegression;
  }

  public OLSMultipleLinearRegression model(Map<Double, List<Double>> points) {
    // the model function components are the distances to current estimated price,
    // they should be as close as possible to the specified price
    OLSMultipleLinearRegression regression = new OLSMultipleLinearRegression();
    Set<Double> keys = points.keySet();
    // create the y matrix
    double[] y = keys.stream().mapToDouble(d -> d).toArray();
    // create a x Matrix
    double[][] x = new double[keys.size()][];
    // build the x-matrix
    for (int i = 0; i < y.length; i++) {
      double[] current = points.get(y[i]).stream().mapToDouble(d -> d).toArray();
      x[i] = current;
    }
    regression.newSampleData(y, x);
    return regression;
  }

}
