package cloudos.machinelearning;

import static org.apache.commons.text.CharacterPredicates.LETTERS;
import cloudos.machinelearning.autoscaler.PredictionException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import lombok.extern.log4j.Log4j2;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.text.RandomStringGenerator;
import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.SplitTestAndTrain;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.NormalizerMinMaxScaler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by philipperibeiro on 7/5/17.
 *
 * <p>Define the CloudosModel, which is the type of all the CloudOS related machine learning models.
 */
@Service
@Log4j2
public abstract class CloudosModel {

  /* We split the data set 0.7 training and 0.3 testing */
  private static final double PERCENTAGE_TRAIN = 0.7;

  private static final String DELIMITER = ",";

  private static final int SKIP_LINES = 0;

  protected static final String MODEL_TIMESTAMP = "yyyyMMddHHmm'.zip'";
  protected static final String IMAGE_TIMESTAMP = "yyyyMMddHHmm'.jpeg'";

  protected static Gson gson =
      new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();

  @Autowired
  protected ModelsRepository modelsRepository;

  protected static RandomStringGenerator generator = new RandomStringGenerator.Builder()
      .withinRange('a', 'z')
      .filteredBy(LETTERS)
      .build();

  /* Once the model is loaded, the data will be passed as an argument to the model for prediction */
  public abstract void predict(final String region, Pair<String, INDArray> vector)
      throws PredictionException;

  /* Used to implement the machine learning training and testing */
  public abstract void train(boolean plot, String... args);

  /**
   * Save a MultiLayer Neural Network model to a file, so it can be read later.
   *
   * @param network the MultiLayerNetwork to be saved
   * @param path the destination of the file to be saved
   * @throws IOException whenever an I/O issues raises up
   */
  public void saveModel(Model network, String path) throws IOException {
    // Save the model.
    // Where to save the network. Note: the file is in .zip format - can be opened externally
    File file = new File(path);
    if (file.exists()) {
      if (!file.delete()) {
        throw new FileNotFoundException(String.format("File at the path %s already exists", path));
      }
    }
    //Updater: i.e., the state for Momentum, RMSProp, Adagrad etc. Save this if you want to train your network more in the future
    ModelSerializer.writeModel(network, file, true);
  }

  /**
   * Load a MultiLayerNetwork model previously saved in a file.
   *
   * @param path the source of the file that contains the model to be loaded
   * @param isMultiLayerNetwork if the model is a MultiLayerNetwork
   * @return the loaded MultiLayerNetwork
   * @throws IOException whenever an I/O issues raises up
   */
  public Model loadModel(String path, boolean isMultiLayerNetwork) throws IOException {
    File file = new File(path);
    if (!file.exists()) {
      throw new FileNotFoundException(String.format("File at the path %s does not exist", path));
    }
    // load the model, but first check the type of the Network saved.
    Model network;
    if (isMultiLayerNetwork) {
      network = ModelSerializer.restoreMultiLayerNetwork(file);
    } else {
      network = ModelSerializer.restoreComputationGraph(file);
    }
    return network;
  }

  /**
   * A helper method to read in and split data files for training and testing.
   *
   * @param trainset the train data set file
   * @param testset the test data set file
   * @param batchSize the batch size
   * @param labelIndexFrom the beginning of the label index
   * @param labelIndexTo the end of the label index
   * @param normalize whether we should normalize the data set or not
   */
  protected Pair<DataSet, DataSet> prepareRegressionDatasets(String trainset,
                                                             String testset,
                                                             int batchSize,
                                                             int labelIndexFrom,
                                                             int labelIndexTo,
                                                             boolean normalize)
      throws IOException, InterruptedException {

    RecordReader recordTrain = new CSVRecordReader();
    recordTrain.initialize(new FileSplit(new File(trainset)));
    DataSetIterator trainIter =
        new RecordReaderDataSetIterator(recordTrain, batchSize, labelIndexFrom, labelIndexTo,true);

    // load the test/evaluation data:
    RecordReader recordTest = new CSVRecordReader();
    recordTest.initialize(new FileSplit(new File(testset)));
    DataSetIterator testIter = new RecordReaderDataSetIterator(recordTest, batchSize, labelIndexFrom,
        labelIndexTo, true);


    DataSet trainData = trainIter.next();
    DataSet testData = testIter.next();

    if (normalize) {
      return this.normalize(trainData, testData);
    }

    // create the dataset pair
    return new ImmutablePair<>(trainData, testData);
  }

  /**
   * Given a single data set file, split the data set into training and testing. The allocation
   * is 70% for training and the remaining for testing.
   *
   * @param dataset the data set file
   * @param batchSize the batch size
   * @param labelIndexFrom the beginning of the label index
   * @param labelIndexTo the end of the label index
   * @param normalize whether we should normalize the data set or not
   * @return a pair containing the training and testing data sets
   * @throws IOException if failed to open the file
   * @throws InterruptedException if the iterator is interrupted during parsing
   */
  protected Pair<DataSet, DataSet> prepareRegressionDatasets(String dataset,
                                                             int batchSize,
                                                             int labelIndexFrom,
                                                             int labelIndexTo,
                                                             boolean normalize)
      throws IOException, InterruptedException {

    RecordReader recordReader = new CSVRecordReader(SKIP_LINES, DELIMITER);
    recordReader.initialize(new FileSplit(new File(dataset)));
    //reader,label index,number of possible labels
    DataSetIterator iterator = new RecordReaderDataSetIterator(recordReader, batchSize,
        labelIndexFrom, labelIndexTo, true);
    //get the dataset using the record reader. The datasetiterator handles vectorization
    DataSet next = iterator.next();
    // split the dataset between train and test
    SplitTestAndTrain testAndTrain = next.splitTestAndTrain(PERCENTAGE_TRAIN);

    DataSet trainData = testAndTrain.getTrain();
    DataSet testData = testAndTrain.getTest();

    if (normalize) {
      return this.normalize(trainData, testData);
    }

    // create the dataset pair
    return new ImmutablePair<>(trainData, testData);
  }

  /**
   *
   * Given a single data set file, split the data set into training and testing. The allocation
   * is 70% for training and the remaining for testing.
   *
   * @param dataset the data set file
   * @param batchSize the batch size
   * @param labelIndex the label index
   * @param numPossiblelabels the total possible number of labels
   * @return a pair containing the training and testing data sets
   * @throws IOException if failed to open the file
   * @throws InterruptedException if the iterator is interrupted during parsing
   */
  protected Pair<DataSet, DataSet> prepareClassificationDatasets(String dataset,
                                                                 int batchSize,
                                                                 int labelIndex,
                                                                 int numPossiblelabels)
      throws IOException, InterruptedException {

    RecordReader recordReader = new CSVRecordReader(SKIP_LINES, DELIMITER);
    recordReader.initialize(new FileSplit(new File(dataset)));
    //reader,label index,number of possible labels
    DataSetIterator iterator = new RecordReaderDataSetIterator(recordReader, batchSize,
        labelIndex, numPossiblelabels);

    //get the dataset using the record reader. The datasetiterator handles vectorization
    DataSet next = iterator.next();
    // split the dataset between train and test
    SplitTestAndTrain testAndTrain = next.splitTestAndTrain(PERCENTAGE_TRAIN);

    // create the dataset pair
    return new ImmutablePair<>(testAndTrain.getTrain(), testAndTrain.getTest());
  }
  /**
   * Apply normalization to the data set, so all the values are within a (0,1) range.
   *
   * @param trainData the training data set
   * @param testData the test data set
   * @return a pair containing the normalized training and testing data sets
   */
  private Pair<DataSet, DataSet> normalize(DataSet trainData, DataSet testData) {
    //Normalize the training data
    NormalizerMinMaxScaler normalizer = new NormalizerMinMaxScaler(0, 1);
    normalizer.fitLabel(true);
    normalizer.fit(trainData);              //Collect training data statistics

    normalizer.transform(trainData);
    normalizer.transform(testData);

    // create the dataset pair
    return new ImmutablePair<>(trainData, testData);
  }
}
