package cloudos.machinelearning.models;

import cloudos.Providers;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * CloudGraphCost is used to hold the cost details for a particular region whenever we perform
 * a cost comparison between providers.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CloudGraphCost {

  private CloudGraph cloudGraph;
  private double cost;
  private String region;
  private Providers provider;
  private int numberInstances;
  private int reservedInstances;
  private int ondemandinstances;
  private int spotInstances;

}
