package cloudos.machinelearning.models;

import cloudos.Providers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Hold the information regarding the cloud equivalence table. Currently, we are supporting only
 * Amazon AWS and Google Compute Engine.
 *
 * @author Philippe Ribeiro
 */
@Component
public final class EquivalentRegion {

  /* hold the equivalent regions for Google Cloud Engine */
  @Value("#{PropertySplitter.map('${cloudos.ml.equivalence.google}')}")
  private Map<String, String> googleEquivalenceMap;

  /* hold the equivalent regions for Amazon AWS */
  @Value("#{PropertySplitter.map('${cloudos.ml.equivalence.aws}')}")
  private Map<String, String> amazonEquivalenceMap;

  /**
   * Get the equivalence map for Google Cloud Engine.
   *
   * @return the google equivalence map
   */
  public Map<String, String> getGoogleEquivalenceMap() {
    return this.googleEquivalenceMap;
  }

  /**
   * Get the equivalence map for Amazon AWS.
   *
   * @return the aws equivalence map
   */
  public Map<String, String> getAmazonEquivalenceMap() {
    return this.amazonEquivalenceMap;
  }

  /**
   * Find the equivalent region between two providers. The equivalent region is defined as the
   * region that is the closest from the first in a different cloud provider.
   *
   * @param region the region of the first provider
   * @param provider the provider to find the equivalent region
   * @return the equivalent region
   */
  public String getEquivalentRegion(String region, Providers provider) {
    String equivalentRegion = null;

    switch (provider) {
      case AMAZON_AWS:
        if (amazonEquivalenceMap.containsKey(region)) {
          equivalentRegion = amazonEquivalenceMap.get(region);
        }
        break;
      case GOOGLE_COMPUTE_ENGINE:
        if (googleEquivalenceMap.containsKey(region)) {
          equivalentRegion = googleEquivalenceMap.get(region);
        }
        break;
      default:
        // nothing to do
        break;
    }
    return equivalentRegion;
  }
}
