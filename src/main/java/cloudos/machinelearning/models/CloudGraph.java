package cloudos.machinelearning.models;

import aima.core.util.datastructure.LabeledGraph;
import cloudos.Providers;
import cloudos.models.Instance;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * CloudGraph holds the information about the instances in a particular region. The labeled graph
 * holds the instances while providing information regarding the provider and region.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CloudGraph {

  private LabeledGraph<Instance, Instance> graph;
  private String region;
  private Providers provider;

}
