package cloudos.machinelearning.tasks;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cloudos.Providers;
import cloudos.data.InstanceData;
import cloudos.data.InstanceDataMetric;
import cloudos.data.InstanceDataType;
import cloudos.elasticsearch.fields.SharedBeatFields;
import cloudos.elasticsearch.model.DateHistogramDataPoint;
import cloudos.elasticsearch.model.KibanaField;
import cloudos.elasticsearch.model.KibanaSearchParameters;
import cloudos.elasticsearch.services.MetricbeatService;
import cloudos.exceptions.NotImplementedException;
import cloudos.instances.InstanceService;
import cloudos.models.CloudosDatapoint;
import cloudos.models.Instance;
import cloudos.resources.AmazonResourcesService;
import cloudos.resources.GoogleResourcesService;
import cloudos.resources.ResourcesManagement;
import lombok.extern.log4j.Log4j2;

/** Task responsible to verify and analyze if an instance becomes unresponsive. */
@Service
@Log4j2
public class InstanceAnalyzerTask {

  @Autowired
  InstanceService instanceService;

  @Autowired
  MetricbeatService metricbeatService;

  @Autowired
  AmazonResourcesService amazonResourcesService;

  @Autowired
  GoogleResourcesService googleResourcesService;

  private static final long TIME_WINDOWS = 1 * 60 * 24;
  private static final int TIME_FRAME = 60 * 10;

  /**
   * Get management class.
   *
   * @param providers filter
   * @return Management class
   * @throws NotImplementedException if not implemented
   */
  private ResourcesManagement getManagement(Providers providers) throws NotImplementedException {
    switch (providers) {
      case AMAZON_AWS:
        return amazonResourcesService;
      case GOOGLE_COMPUTE_ENGINE:
        return googleResourcesService;
      default:
        break;
    }
    throw new NotImplementedException();
  }

  /**
   * Analyze the instances for the provider.
   *
   * @param provider to analyzer
   */
  public void analyze(Providers provider) {

    final List<Instance> instances = instanceService.findAllActiveInstancesByProvider(provider);
    for (Instance instance : instances) {
      try {
        // get es metrics
        KibanaSearchParameters kibanaSearchParameters = new KibanaSearchParameters();
        KibanaField hostnameField =
            new KibanaField(SharedBeatFields.BEAT_HOSTNAME, Arrays.asList(instance.getName()));
        kibanaSearchParameters.setFilterFields(Arrays.asList(hostnameField));
        List<DateHistogramDataPoint> metricsDataPoints =
            metricbeatService.getSystemCpuSystemPctDateHistogramAvg(kibanaSearchParameters);
        log.info(metricsDataPoints.size());

        // get provider metrics
        InstanceData instanceData = new InstanceData(provider, InstanceDataMetric.CPU_UTILIZATION,
            InstanceDataType.AVERAGE, TIME_WINDOWS, TIME_FRAME);
        final List<CloudosDatapoint> providerMetrics =
            this.getManagement(provider).findDatapoints(instance, instanceData);

        log.info(providerMetrics.size());

        // TODO: add the machine learn

        boolean instanceUnresponsive = false;

        if (instanceUnresponsive) {
          // notify user
          // TODO: create new instance and terminate the other one
        }

      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
}
