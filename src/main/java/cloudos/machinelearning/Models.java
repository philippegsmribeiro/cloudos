package cloudos.machinelearning;

public enum Models {
  AUTOSCALER("autoscaler"),
  SPOTBIDDER("spotbidder"),
  LOADBALANCER("loadbalancer"),
  MULTIPLEXER("multiplexer");

  private String name;

  Models(String name) {
    this.name = name;
  }

  /** Define the inner enum Task, which specifies the tasks associated with each model. */
  public enum Task {
    PRODUCTION("production"),
    DEVELOPMENT("development"),
    TEST("test"),
    DATA("data"),
    AWS("aws"),
    GOOGLE("google");

    private String task;

    Task(String task) {
      this.task = task;
    }

    @Override
    public String toString() {
      return this.task;
    }
  }

  @Override
  public String toString() {
    return this.name;
  }
}
