package cloudos.machinelearning.spotbidder;

import cloudos.Providers;
import cloudos.instances.InstanceService;
import cloudos.machinelearning.CloudosModel;
import cloudos.machinelearning.Models;
import cloudos.machinelearning.ModelsRepository;
import cloudos.machinelearning.autoscaler.PredictionException;
import cloudos.machinelearning.autoscaler.PredictionException.PredictionExceptionType;
import cloudos.models.Instance;
import cloudos.models.spotinstances.SpotBidderPricing;
import cloudos.policies.CloudPolicyService;
import cloudos.policies.SpotBidderPolicy;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Pair;
import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.GravesLSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.stats.StatsListener;
import org.deeplearning4j.ui.storage.InMemoryStatsStorage;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.springframework.beans.factory.annotation.Value;

/**
 * Implement the SpotBidder model, which is responsible for predicting the spot price of AWS spot
 * instances, based on the product type, region and instance type.
 */
@Log4j2
public class SpotBidder extends CloudosModel {

  private static final String AWS_PROVIDER = "aws";
  private static final String GOOGLE_PROVIDER = "google";
  private static final String BIDDER_MODEL = "bidder.zip";

  @Value("${cloudos.ml.spotbidder.batch_size}")
  private int batchSize = 64;

  @Value("${cloudos.ml.spotbidder.seed}")
  private int seed = 12345;

  @Value("${cloudos.ml.spotbidder.learning_rate}")
  private double learningRate = 0.001;

  // Number of epochs (full passes of the data)
  @Value("${cloudos.ml.spotbidder.num_epochs}")
  private int nEpochs = 100;

  @Value("${cloudos.ml.spotbidder.num_iterations}")
  private int numIterations = 1;

  @Value("${cloudos.ml.spotbidder}")
  private String spotBidderPath;

  @Value("${cloudos.ml.spotbidder.l2_regularization}")
  private double l2Regulatization = 1e-1;

  @Value("${cloudos.ml.spotbidder.label_index}")
  private int labelIndex = 4;

  @Value("${cloudos.ml.spotbidder.network_layer_length}")
  private int networkLayerLength = 20;

  @Value("${cloudos.ml.spotbidder.rms_decay}")
  private double rmsDecay = 0.95;

  @Value("${cloudos.ml.spotbidder.lstm_layer1_size}")
  private int lstmLayer1Size = 256;

  @Value("${cloudos.ml.spotbidder.lstm_layer2_size}")
  private int lstmLayer2Size = 256;

  @Value("${cloudos.ml.spotbidder.dense_layer_size}")
  private int denseLayerSize = 32;

  @Value("${cloudos.ml.spotbidder.dropout_ratio}")
  private double dropoutRatio = 0.2;

  @Value("${cloudos.ml.spotbidder.split_ratio}")
  private double splitRatio = 0.7;

  private String bidderModel;

  private String path;

  private MultiLayerNetwork bidder;

  private final SpotBidderPricingRepository spotBidderPricingRepository;
  private final InstanceService instanceService;
  private final CloudPolicyService cloudPolicyService;

  /**
   * Constructor that set the path for the repository.
   *
   * @param spotBidderPath the path to the SpotBidder model
   * @param path the final path
   * @param region the region the model belongs to.
   * @param repository the models repository
   * @param spotBidderPricingRepository the spot bidder pricing repository
   */
  public SpotBidder(final String spotBidderPath, final String path, final String region,
      ModelsRepository repository, SpotBidderPricingRepository spotBidderPricingRepository,
      InstanceService instanceService, CloudPolicyService cloudPolicyService)
      throws IOException {

    this.spotBidderPath = spotBidderPath;
    this.path = path;
    this.modelsRepository = repository;
    this.spotBidderPricingRepository = spotBidderPricingRepository;
    this.instanceService = instanceService;
    this.cloudPolicyService = cloudPolicyService;

    // check if the root path exists
    if (!cloudos.utils.FileUtils.exits(this.spotBidderPath)) {
      log.warn("Spot models repository does not exist in {}... creating", this.spotBidderPath);
      File file = new File(this.spotBidderPath);
      if (!file.mkdirs()) {
        throw new IOException("Could not create the directory " + this.spotBidderPath);
      }
    }
    // attempt the model based on the region
    try {
      this.model(region);
    } catch (Exception e) {
      log.error(e.getMessage());
    }
  }

  /**
   * Load the pre-trained model for the SpotBidder.
   *
   * @param region the name of the region the spot instance belongs to
   * @throws Exception if case the file could not be found
   */
  public void model(String region) throws Exception {

    // check if the root path exists
    if (!cloudos.utils.FileUtils.exits(this.spotBidderPath)) {
      cloudos.utils.FileUtils.create(this.spotBidderPath);
    }

    // download the bidder model
    this.modelsRepository.download(Models.SPOTBIDDER, Models.Task.AWS, region, BIDDER_MODEL);
    this.bidderModel =
        Paths.get(this.spotBidderPath, Models.Task.AWS.toString(), region, BIDDER_MODEL).toString();

    // load the price prediction model
    log.info("*********** Loading spot bidder model: {} ***********", this.bidderModel);

    // load the regression model
    this.bidder = (MultiLayerNetwork) this.loadModel(this.bidderModel, true);
  }

  /**
   * Train the SpotBidder model.
   *
   * @param visualization if we want to display the training visualization
   * @param args a list of arguments to be given to the models
   */
  @Override
  public void train(boolean visualization, String... args) {

    try {
      // ------------------------------------------------------------------------------------
      // Train the decider predictor model
      log.info("*********Start training SpotBidder PriceRNN Model***********");
      if (args.length < 6) {
        log.warn("There must be at least five parameters for training");
        return;
      }
      // check the type of the region
      String provider;
      if (AWS_PROVIDER.equals(args[1])) {
        provider = AWS_PROVIDER;
      } else if (GOOGLE_PROVIDER.equals(args[1])) {
        provider = GOOGLE_PROVIDER;
      } else {
        log.warn("Provider {} is not supported. Nothing to do.", args[1]);
        return;
      }
      // this.trainPriceNNModel(args[0], args[1], visualization, provider, args[3]);
      this.trainPriceRNNModel(args[0], visualization, provider, args[2], args[3], args[4], args[5]);
      log.info("*********Done training SpotBidder PriceNN model************");

    } catch (Exception e) {
      log.error(e.getMessage());
    }
  }

  /**
   * Predict price based on region, instance type, and system provider.
   *
   * @param region the region the instance is located
   * @param vector the INDArray to be predicted
   * @throws PredictionException if spotbidder is not executed
   */
  @Override
  public void predict(final String region, Pair<String, INDArray> vector)
      throws PredictionException {
    // get the instance to be decided...
    Instance instance = this.instanceService.findByProviderId(vector.getKey());

    // check if the instance exists
    if (instance == null) {
      String message = String.format("Instance %s does not exist.", vector.getKey());
      log.warn(message);
      throw new PredictionException(PredictionExceptionType.INCOMPLETE_DATA, message);
    }

    // find the policies
    List<SpotBidderPolicy> applicablePolicies =
        cloudPolicyService.retrieveApplicableSpotBidderPolicies(instance);
    // TODO: apply the rules

    // @TODO: Fix the predict to be more like the model below
    INDArray price = this.bidder.rnnTimeStep(vector.getValue());
    log.info("Predicted prices is: {}", price);
    price = this.bidder.output(vector.getValue(), false);
    log.info("Predicted prices is: {}", price);

  }

  /**
   * Train a Spot bidder model with time series and pricing as outputs.
   *
   * @param filename the train data set
   * @param visualize if we should call the visualizer callback to watch progress
   * @param provider the provider the models belongs to
   * @param region the region in the provider the data belongs too
   * @param availabilityZone the availability zone of the spot instances
   * @param productType the type of the instance product
   * @param instanceType the type of the instance
   * @throws IOException whenever there's an error opening the file
   * @throws InterruptedException wheenver the model training / testing is interrupted
   */
  private void trainPriceRNNModel(String filename, boolean visualize, String provider,
      String region, String availabilityZone, String productType, String instanceType)
      throws IOException, InterruptedException {

    Nd4j.ENFORCE_NUMERICAL_STABILITY = true;

    log.info("Creating dataset iterator from file {}", filename);
    SpotBidderIterator iterator =
        new SpotBidderIterator(filename, batchSize, networkLayerLength, splitRatio);

    log.info("Load test dataset ...");
    List<Pair<INDArray, INDArray>> test = iterator.getTestDataSet();

    log.info("Build LSTM networks....");
    MultiLayerNetwork network = this.buildLstmNetwork(iterator.inputColumns(),
        iterator.totalOutcomes(), networkLayerLength);

    if (visualize) {
      // Initialize the user interface backend
      UIServer uiServer = UIServer.getInstance();

      // Configure where the network information (gradients, activations, score vs. time etc) is to
      // be stored
      // Then add the StatsListener to collect this information from the network, as it trains
      StatsStorage statsStorage = new InMemoryStatsStorage();
      // Alternative: new FileStatsStorage(File) - see UIStorageExample
      int listenerFrequency = 1;
      network.setListeners(new StatsListener(statsStorage, listenerFrequency));

      // Attach the StatsStorage instance to the UI: this allows the contents of the StatsStorage to
      // be visualized
      uiServer.attach(statsStorage);
    } else {
      network.setListeners(new ScoreIterationListener(20));
    }

    // save the pricing for this spot instance
    SpotBidderPricing bidderPricing =
        SpotBidderPricing.builder().availabilityZone(availabilityZone).product(productType)
            .region(region).provider(Providers.AMAZON_AWS).instanceType(instanceType)
            .maxPrice(iterator.getMaxNum()).minPrice(iterator.getMinNum()).build();

    // ------------------------------------------------------------------------------------
    // Save the decider model in the SpotBidder repository
    String root =
        Paths.get(this.spotBidderPath, provider, region, availabilityZone, productType).toString();

    // model path is 'spotbidder' + 'provider' + 'region' + 'availabilityZone' + 'instanceType'
    String model = String.format("%s.zip", instanceType);
    String spotBidderPath = Paths.get(root, model).toString();

    File fileModel = new File(spotBidderPath);
    if (fileModel.exists()) {
      log.info("Model in the path {} already exists... skipping", spotBidderPath);
      return;
    }


    log.info("Saving the spot bidder pricing {}", bidderPricing);
    this.spotBidderPricingRepository.save(bidderPricing);

    // train the data set
    log.info("Training....");
    for (int i = 0; i < nEpochs; i++) {
      DataSet dataSet;
      while (iterator.hasNext()) {
        dataSet = iterator.next();
        network.fit(dataSet);
      }
      iterator.reset(); // reset the iterator
      network.rnnClearPreviousState(); // clear previous state
    }

    log.info(
        "****************Training Spot Bidder model finished for "
            + "provider {}, region {}, product {}, availability zone {} and instance type {} "
            + "********************",
        provider, region, availabilityZone, productType, instanceType);

    File file = new File(root);
    if (!file.exists()) {
      if (!file.mkdirs()) {
        throw new IOException("Could not create the root directory in " + root);
      }
    }
    log.info("****************Saving the Spot Bidder model to the file {}********************",
        spotBidderPath);
    this.saveModel(network, spotBidderPath);
    this.modelsRepository.upload(Models.SPOTBIDDER, Models.Task.DEVELOPMENT, spotBidderPath);

    log.info("Testing....");
    INDArray max = Nd4j.create(new int[] {1}, 'f');
    max.putScalar(new int[] {0}, iterator.getMaxNum());
    INDArray min = Nd4j.create(new int[] {1}, 'f');
    min.putScalar(new int[] {0}, iterator.getMinNum());

    double[] predicts = new double[test.size()];
    double[] actuals = new double[test.size()];

    // execute the tests
    for (int i = 0; i < test.size(); i++) {
      predicts[i] = network.rnnTimeStep(test.get(i).getKey()).getRow(networkLayerLength - 1)
          .mul(max.sub(min)).add(min).getDouble(0);
      actuals[i] = test.get(i).getValue().getDouble(0);
    }

    log.info("Printing out predictions and actual values...");
    log.info("=============================================");
    log.info("{}, {}, {}, {}: Predict, Actual", region, availabilityZone, productType,
        instanceType);

    for (int i = 0; i < predicts.length; i++) {
      log.info("{}, {}", predicts[i], actuals[i]);
    }

    log.info("=============================================");
    log.info("Done...");

  }

  /**
   * Build the neural network for the spot bidder.
   *
   * @param nIn the number of input parameters
   * @param nOut the number of output parameters
   * @param exampleLength the length of the time series
   * @return a MultiLayerNetwork for the spot bidder
   */
  private MultiLayerNetwork buildLstmNetwork(int nIn, int nOut, int exampleLength) {

    MultiLayerConfiguration conf =
        new NeuralNetConfiguration.Builder().seed(seed).iterations(numIterations)
            .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
            .weightInit(WeightInit.XAVIER).updater(Updater.RMSPROP).rmsDecay(rmsDecay)
            .learningRate(learningRate).regularization(true).l2(l2Regulatization).list()
            .layer(0,
                new GravesLSTM.Builder().nIn(nIn).nOut(lstmLayer1Size).activation(Activation.TANH)
                    .gateActivationFunction(Activation.HARDSIGMOID).dropOut(dropoutRatio).build())
            .layer(1,
                new GravesLSTM.Builder().nIn(lstmLayer1Size).nOut(lstmLayer2Size)
                    .activation(Activation.TANH).gateActivationFunction(Activation.HARDSIGMOID)
                    .dropOut(dropoutRatio).build())
            .layer(2,
                new DenseLayer.Builder().nIn(lstmLayer2Size).nOut(denseLayerSize)
                    .activation(Activation.RELU).build())
            .layer(3,
                new RnnOutputLayer.Builder().nIn(denseLayerSize).nOut(nOut)
                    .activation(Activation.IDENTITY).lossFunction(LossFunctions.LossFunction.MSE)
                    .build())
            .backpropType(BackpropType.TruncatedBPTT).tBPTTForwardLength(exampleLength)
            .tBPTTBackwardLength(exampleLength).pretrain(false).backprop(true).build();

    MultiLayerNetwork net = new MultiLayerNetwork(conf);
    net.init();
    net.setListeners(new ScoreIterationListener(20));
    return net;
  }


  /**
   * Set the value for the spot bidder path.
   *
   * @param spotBidderPath the path to the spotbidder repository
   * @return the SpotBidder
   */
  public SpotBidder withSpotBidderPath(final String spotBidderPath) {
    this.spotBidderPath = spotBidderPath;
    return this;
  }

  /**
   * Get the current network for this class.
   *
   * @return the MultiLayerNetwork for the spot bidder
   */
  public MultiLayerNetwork getNetwork() {
    return this.bidder;
  }
}
