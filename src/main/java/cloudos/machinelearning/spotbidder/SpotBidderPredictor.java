package cloudos.machinelearning.spotbidder;

import cloudos.models.spotinstances.CloudSpotPricingRequest;

/**
 * Define the spot bidder manager interface to manage spot instances.
 *
 * @author philipperibeiro
 */
public interface SpotBidderPredictor {

  /**
   * Predict the cost of the spot instances for a given region.
   *
   * @param pricingRequest the spot pricing request
   * @return the predicted cost
   */
  double predict(CloudSpotPricingRequest pricingRequest);
}
