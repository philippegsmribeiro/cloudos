package cloudos.machinelearning.spotbidder;

import cloudos.models.spotinstances.SpotBidderPricing;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

interface SpotBidderPricingRepository extends MongoRepository<SpotBidderPricing, String> {

  List<SpotBidderPricing> findByAvailabilityZone(String availabilityZone);

  List<SpotBidderPricing> findByRegion(String region);

  List<SpotBidderPricing> findByProduct(String product);

  List<SpotBidderPricing> findByInstanceType(String instanceType);

  SpotBidderPricing findByRegionAndAvailabilityZoneAndProductAndInstanceType(String region,
      String availabilityZone, String product, String instanceType);

}
