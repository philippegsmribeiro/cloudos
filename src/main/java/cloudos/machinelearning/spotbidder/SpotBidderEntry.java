package cloudos.machinelearning.spotbidder;

import com.opencsv.bean.CsvBindByPosition;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SpotBidderEntry {

  @CsvBindByPosition(position = 0)
  private String timestamp; // timestamp

  @CsvBindByPosition(position = 1)
  private String availabilityZone; // define the availability zone

  @CsvBindByPosition(position = 2)
  private String instanceType; // the type of the image

  @CsvBindByPosition(position = 3)
  private String productDescription; // define the product type

  @CsvBindByPosition(position = 4)
  private double spotPrice; // define the spotPrice of the instance

}
