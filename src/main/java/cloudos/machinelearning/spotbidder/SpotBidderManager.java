package cloudos.machinelearning.spotbidder;

import cloudos.Providers;
import cloudos.exceptions.NotImplementedException;
import cloudos.instances.InstanceRequestException;
import cloudos.instances.spotinstances.CloudosSpotInstanceManager;
import cloudos.models.AbstractInstance;
import cloudos.models.CloudActionRequest;
import cloudos.models.CloudActionResponse;
import cloudos.models.spotinstances.CloudSpotCancelRequest;
import cloudos.models.spotinstances.CloudSpotCancelResponse;
import cloudos.models.spotinstances.CloudSpotCreateRequest;
import cloudos.models.spotinstances.CloudSpotCreateResponse;
import cloudos.models.spotinstances.CloudSpotDescribeRequest;
import cloudos.models.spotinstances.CloudSpotDescribeResponse;
import cloudos.models.spotinstances.CloudSpotPricingRequest;
import cloudos.models.spotinstances.CloudSpotPricingResponse;
import cloudos.models.spotinstances.CloudSpotTerminateResponse;
import cloudos.queue.QueueListener;
import cloudos.queue.QueueManagerService;
import cloudos.queue.message.spotbidder.SpotCreateRequestMessage.SpotCreateRequestMessageConsumer;
import cloudos.queue.message.spotbidder.SpotPricingPredictRequestMessage.SpotPricingPredictRequestMessageConsumer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;

import lombok.extern.log4j.Log4j2;

import org.apache.commons.collections4.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Manage the bidding process for spot instances in the supported cloud providers.
 */
@Service
@Log4j2
public class SpotBidderManager {

  /* The number of times the SpotBidder manager will attempt to fulfill the request */
  private static final int MAX_NUMBER_ATTEMPTS = 3;

  /* Define the SpotBidderPredictors */
  @Autowired
  private AmazonSpotBidderPredictor amazonSpotBidderPredictor;

  @Autowired
  private GoogleSpotBidderPredictor googleSpotBidderPredictor;

  @Autowired
  private CloudosSpotInstanceManager spotInstanceManager;

  @Autowired
  private QueueListener queueListener;
  @Autowired
  private QueueManagerService queueManagerService;

  /**
   * Setup the queue listeners.
   */
  @PostConstruct
  public void setUpQueueListener() {
    // listener for spotpricing messages
    this.queueListener.addConsumer((SpotPricingPredictRequestMessageConsumer) message -> {

      try {
        // requesting pricing
        CloudSpotPricingResponse predictSpotPrice =
            this.predictSpotPrice(message.getSpotPricingRequest());
        // responding
        queueManagerService.sendSpotPricingPredictResponseMessage(predictSpotPrice,
            message.getAutoscaleRequestId());
      } catch (NotImplementedException e) {
        log.error("Not implemented!", e);
      }

    });

    // listener for create instances
    this.queueListener.addConsumer((SpotCreateRequestMessageConsumer) message -> {

      try {
        CloudSpotCreateResponse response = this.create(message.getCloudSpotCreateRequest());

        // responding
        queueManagerService.sendSpotCreateResponseMessage(response,
            message.getAutoscaleRequestId());
      } catch (NotImplementedException e) {
        log.error("Not implemented!", e);
      }

    });
  }

  /**
   * Obtain a predicted pricing for the instance in the respective cloud provider.
   *
   * @param pricingRequest the spot instance pricing request
   * @return the predicted price for the instance
   */
  public CloudSpotPricingResponse predictSpotPrice(@NotNull CloudSpotPricingRequest pricingRequest)
      throws NotImplementedException {
    SpotBidderPredictor spotBidderPredictor = this.getPredictor(pricingRequest.getProvider());
    if (spotBidderPredictor != null) {
      // get the price for the spot instance
      double price = spotBidderPredictor.predict(pricingRequest);
      return CloudSpotPricingResponse.builder()
          .availabilityZone(pricingRequest.getAvailabilityZone())
          .instanceTypes(pricingRequest.getInstanceTypes())
          // the timestamp is now
          .productDescription(pricingRequest.getProductDescription()).timestamp(new Date())
          .price(price).provider(pricingRequest.getProvider()).region(pricingRequest.getRegion())
          .id(pricingRequest.getId()) // the response ID is equal to the request id
          .build();
    }
    return null;
  }


  /**
   * Create a group of spot instances based on the pricing prediction and the parameters given by
   * the create request object.
   *
   * @param spotCreateRequest the spot create request
   * @return a response of the spot create request
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  public CloudSpotCreateResponse create(CloudSpotCreateRequest spotCreateRequest)
      throws NotImplementedException {
    // attempt to create the spot instances defined in the request
    List<AbstractInstance> requestedInstances = new ArrayList<>();
    int fulfilled = 0;
    try {
      int attempts = 1;
      do {
        log.info("Attempt number {} to fulfill the spot create request", attempts);
        // create the spot instances
        List<? extends AbstractInstance> instances =
            this.spotInstanceManager.createInstances(spotCreateRequest);
        if (instances != null) {
          // merge the two lists
          requestedInstances = ListUtils.union(requestedInstances, instances);
          fulfilled += requestedInstances.size();
          if (fulfilled >= spotCreateRequest.getCount()) {
            break;
          }
          // remove that many instances from the request.
          spotCreateRequest.setCount(spotCreateRequest.getCount() - fulfilled);
        }
        attempts++;
      } while (attempts <= MAX_NUMBER_ATTEMPTS);
    } catch (InstanceRequestException e) {
      log.error(e.getMessage());
    }

    CloudSpotCreateResponse response = CloudSpotCreateResponse.builder()
        .provider(spotCreateRequest.getProvider()).spotPrice(spotCreateRequest.getSpotPrice())
        .region(spotCreateRequest.getRegion()).availabilityZone(spotCreateRequest.getZone())
        .spotInstanceType(spotCreateRequest.getSpotInstanceType()).created(fulfilled)
        .minCount(spotCreateRequest.getMinCount()).target(spotCreateRequest.getCount())
        .duration(spotCreateRequest.getDuration()).instances(requestedInstances)
        /* .id(spotCreateRequest.getId()) */.build();

    // set the status of the request
    if (fulfilled >= spotCreateRequest.getCount()) {
      response.setStatus(SpotBidderRequestStatus.FULFILLED);
    } else if (spotCreateRequest.getMinCount() <= fulfilled) {
      response.setStatus(SpotBidderRequestStatus.PARTIALLY_FULFILLED);
    } else if (fulfilled > 0) {
      // could at least create some instances
      response.setStatus(SpotBidderRequestStatus.INCOMPLETE);
    } else {
      // completed failed
      response.setStatus(SpotBidderRequestStatus.FAILED);
    }

    return response;
  }

  /**
   * Terminate the spot request that was previous created.
   *
   * @param request the cloud action request
   * @return a list of the action responses
   */
  public CloudSpotTerminateResponse terminate(CloudActionRequest request) {
    try {
      List<? extends CloudActionResponse<?>> responses =
          this.spotInstanceManager.terminate(request);
      if (responses != null) {
        return CloudSpotTerminateResponse.builder().provider(request.getProvider())
            .region(request.getRegion()).actionResponses(responses).build();
      }
    } catch (NotImplementedException | InstanceRequestException e) {
      log.error(e.getMessage());
    }

    return null;
  }

  /**
   * Cancel an existing spot request.
   *
   * @param request the cancel spot request
   * @return return a spot cancel response
   */
  public CloudSpotCancelResponse cancelSpotRequest(CloudSpotCancelRequest request) {
    try {
      boolean status = this.spotInstanceManager.cancelSpotRequest(request);
      return CloudSpotCancelResponse.builder().id(request.getId()).provider(request.getProvider())
          .region(request.getRegion()).status(status).build();
    } catch (NotImplementedException e) {
      log.error(e.getMessage());
    }
    return null;
  }

  /**
   * Describe the state of a spot instance request.
   *
   * @param request the spot describe request
   * @return a list of instances
   */
  public CloudSpotDescribeResponse describeInstances(CloudSpotDescribeRequest request) {
    try {
      List<?> instances = this.spotInstanceManager.describeInstances(request);
      if (instances != null) {
        return CloudSpotDescribeResponse.builder().id(request.getId()).instances(instances)
            .provider(request.getProvider()).region(request.getRegion()).build();
      }
    } catch (NotImplementedException e) {
      log.error(e.getMessage());
    }
    return null;
  }

  /**
   * Get the autoscaler manager based on the provider.
   *
   * @param providers the name of the provider
   * @return a provider manager
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  @SuppressWarnings("Duplicates")
  private SpotBidderPredictor getPredictor(Providers providers) throws NotImplementedException {
    switch (providers) {
      case AMAZON_AWS:
        return amazonSpotBidderPredictor;
      case GOOGLE_COMPUTE_ENGINE:
        return googleSpotBidderPredictor;
      default:
        break;
    }
    throw new NotImplementedException();
  }
}
