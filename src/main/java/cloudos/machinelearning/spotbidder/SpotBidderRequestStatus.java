package cloudos.machinelearning.spotbidder;

/**
 * Define all the status of the SpotBidder create request.
 * 1. FULFILLED: all the requested spot instances were created.
 * 2. PARTIALLY_FULFILLED: could meet a number more than the min count, but not the target.
 * 3. INCOMPLETE: could only fulfill a set of instances smaller than the min count.
 * 4. FAILED: failed completed to request even a single spot instance.
 */
public enum SpotBidderRequestStatus {
  FULFILLED,
  PARTIALLY_FULFILLED,
  INCOMPLETE,
  FAILED
}
