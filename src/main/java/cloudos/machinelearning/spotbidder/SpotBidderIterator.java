package cloudos.machinelearning.spotbidder;

import com.opencsv.bean.CsvToBeanBuilder;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import lombok.extern.log4j.Log4j2;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;

@Log4j2
public class SpotBidderIterator implements DataSetIterator {

  private final int VECTOR_SIZE = 1;
  private int miniBatchSize;
  private int exampleLength;

  private double minNum = Double.MAX_VALUE;
  private double maxNum = Double.MIN_VALUE;

  private LinkedList<Integer> exampleStartOffset = new LinkedList<>();

  private List<SpotBidderEntry> train;
  private List<Pair<INDArray, INDArray>> test;

  /**
   * Constructor for the SpotBidderIterator.
   *
   * @param filename the name of the data file
   * @param miniBatchSize the size of the mini batch
   * @param exampleLength the length of the time series
   * @param splitRatio the split ratio between training and testing data
   */
  public SpotBidderIterator(String filename, int miniBatchSize, int exampleLength,
      double splitRatio) {
    // read the entries from the file
    List<SpotBidderEntry> spotBidderEntryList = this.readSpotBidderFromFile(filename);
    this.miniBatchSize = miniBatchSize;
    this.exampleLength = exampleLength;
    int split = (int) Math.round(spotBidderEntryList.size() * splitRatio);
    log.info("The split point is {}", split);
    train = spotBidderEntryList.subList(0, split);
    test = this.generateTestDataSet(spotBidderEntryList.subList(split, spotBidderEntryList.size()));
  }

  /**
   * We initialize the offsets in order to determine how many indexes we will have for the next
   * iterator.
   */
  private void initializeOffsets() {
    exampleStartOffset.clear();
    int window = exampleLength + 1;
    for (int i = 0; i < train.size() - window; i++) {
      exampleStartOffset.add(i);
    }
  }

  /**
   * Return the test data set.
   *
   * @return a list of values and labels
   */
  public List<Pair<INDArray, INDArray>> getTestDataSet() {
    return test;
  }

  public double getMaxNum() {
    return maxNum;
  }

  public double getMinNum() {
    return minNum;
  }

  /**
   * Generate a subset of the data as test data.
   *
   * @param spotBidderEntries a list of SpotBidderEntries
   * @return a list between the entry and the label
   */
  private List<Pair<INDArray, INDArray>> generateTestDataSet(
      List<SpotBidderEntry> spotBidderEntries) {
    int window = exampleLength + 1;
    List<Pair<INDArray, INDArray>> test = new ArrayList<>();

    for (int i = 0; i < spotBidderEntries.size() - window; i++) {
      INDArray input = Nd4j.create(new int[] {exampleLength, VECTOR_SIZE}, 'f');
      for (int j = i; j < i + exampleLength; j++) {
        SpotBidderEntry entry = spotBidderEntries.get(j);
        input.putScalar(new int[] {j - i, 0}, (entry.getSpotPrice() - minNum) / (maxNum - minNum));
      }

      SpotBidderEntry entry = spotBidderEntries.get(i + exampleLength);
      INDArray label = Nd4j.create(new int[] {1}, 'f');
      // add the label for the test
      label.putScalar(new int[] {0}, entry.getSpotPrice());

      test.add(new ImmutablePair<>(input, label));

    }
    return test;
  }

  /**
   * Read the SpotBidderEntry objects from the file.
   *
   * @param filename the name of the data file
   * @return a list of SpotBidderEntry
   */
  private List<SpotBidderEntry> readSpotBidderFromFile(String filename) {
    List<SpotBidderEntry> spotBidderEntries = new ArrayList<>();

    try {
      List<SpotBidderEntry> entries = new CsvToBeanBuilder(new FileReader(filename))
          .withType(SpotBidderEntry.class).build().parse();

      for (SpotBidderEntry entry : entries) {
        maxNum = Math.max(maxNum, entry.getSpotPrice());
        minNum = Math.min(minNum, entry.getSpotPrice());
        spotBidderEntries.add(entry);
      }

      log.info("The maximum and minimum spot prices are {} and {}.", maxNum, minNum);

    } catch (IOException ex) {
      log.error(ex.getMessage());
    }
    return spotBidderEntries;
  }

  /**
   * Get the label for the spot price.
   *
   * @param data a spot entry
   * @return a double value for the label
   */
  private double feedLabel(SpotBidderEntry data) {
    return (data.getSpotPrice() - minNum) / (maxNum - minNum);
  }

  /**
   * Return the next DataSet element from the iterator.
   *
   * @param num the position of the iterator
   * @return the current DataSet iterator
   */
  @Override
  public DataSet next(int num) {
    if (this.exampleStartOffset.size() == 0) {
      throw new NoSuchElementException();
    }
    // get the minimum batch size
    int actualMiniBatchSize = Math.min(num, exampleStartOffset.size());
    // get the input value
    INDArray input = Nd4j.create(new int[] {actualMiniBatchSize, VECTOR_SIZE, exampleLength}, 'f');
    INDArray label = Nd4j.create(new int[] {actualMiniBatchSize, 1, exampleLength}, 'f');

    for (int index = 0; index < actualMiniBatchSize; index++) {
      int startIdx = exampleStartOffset.removeFirst();
      int endIdx = startIdx + exampleLength;
      SpotBidderEntry current = train.get(startIdx);
      SpotBidderEntry next;

      for (int i = startIdx; i < endIdx; i++) {
        next = train.get(i + 1);
        int c = i - startIdx;
        input.putScalar(new int[] {index, 0, c},
            (current.getSpotPrice() - minNum) / (maxNum - minNum));
        label.putScalar(new int[] {index, 0, c}, feedLabel(next));

        // move the pointer over
        current = next;
      }

      if (exampleStartOffset.size() == 0) {
        break;
      }
    }

    return new DataSet(input, label);
  }

  @Override
  public int totalExamples() {
    return train.size() - exampleLength - 1;
  }

  @Override
  public int inputColumns() {
    return VECTOR_SIZE;
  }

  @Override
  public int totalOutcomes() {
    return 1;
  }

  @Override
  public boolean resetSupported() {
    return false;
  }

  @Override
  public boolean asyncSupported() {
    return false;
  }

  @Override
  public void reset() {
    this.initializeOffsets();
  }

  @Override
  public int batch() {
    return this.miniBatchSize;
  }

  @Override
  public int cursor() {
    return this.totalExamples() - exampleStartOffset.size();
  }

  @Override
  public int numExamples() {
    return this.totalExamples();
  }

  @Override
  public void setPreProcessor(DataSetPreProcessor dataSetPreProcessor) {
    throw new UnsupportedOperationException("Not Implemented");
  }

  @Override
  public DataSetPreProcessor getPreProcessor() {
    throw new UnsupportedOperationException("Not Implemented");
  }

  @Override
  public List<String> getLabels() {
    throw new UnsupportedOperationException("Not Implemented");
  }

  @Override
  public boolean hasNext() {
    return this.exampleStartOffset.size() > 0;
  }

  @Override
  public DataSet next() {
    return this.next(miniBatchSize);
  }
}
