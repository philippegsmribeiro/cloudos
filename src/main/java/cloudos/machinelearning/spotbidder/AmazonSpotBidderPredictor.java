package cloudos.machinelearning.spotbidder;

import cloudos.instances.spotinstances.AmazonSpotInstanceManager;
import cloudos.models.spotinstances.CloudSpotPricingRequest;

import com.amazonaws.services.ec2.model.SpotPrice;
import com.amazonaws.util.CollectionUtils;

import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.validation.constraints.NotNull;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The AmazonSpotBidderManager is used to manage Amazon AWS Spot instances, including prediction,
 * managing preemptions and requesting instances. We implement it as a interface in order to
 * standardize between different providers.
 */
@Service
public class AmazonSpotBidderPredictor implements SpotBidderPredictor {

  private static int PRICING_WINDOW = 24;

  private static double AWS_PRICING_FACTOR = 0.05;

  /* Use the spot bidder map to hold spot bidder models */
  private static ConcurrentHashMap<String, SpotBidder> spotBidderMap = new ConcurrentHashMap<>();

  /* Define the spot instance managers for Amazon and Google */
  @Autowired
  private AmazonSpotInstanceManager amazonSpotInstanceManager;


  /**
   * Predict a current pricing for the spot instance as specified in the request.
   *
   * @param pricingRequest the spot instance pricing request
   * @return the predicted price for the spot instance
   */
  @Override
  @SuppressWarnings("unchecked")
  public double predict(@NotNull CloudSpotPricingRequest pricingRequest) {

    // TODO: Replace this method by the SpotBidder models once they are ready

    // create a request for the last 24 hrs.
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.HOUR_OF_DAY, -PRICING_WINDOW);

    // set the time window
    pricingRequest.setStartTime(calendar.getTime());
    pricingRequest.setEndTime(new Date());

    List<SpotPrice> pricing =
        (List<SpotPrice>) this.amazonSpotInstanceManager.pricing(pricingRequest);
    if (!CollectionUtils.isNullOrEmpty(pricing)) {
      // sort the list by time stamp
      pricing.sort(Comparator.comparing(SpotPrice::getTimestamp));
      // get the latest element, so we now the most recent pricing
      SpotPrice spotPrice = pricing.get(pricing.size() - 1);
      double currentPrice = Double.valueOf(spotPrice.getSpotPrice());
      // right now we will return a price 5% above the bidding price.
      return this.pricingFactor(currentPrice);
    }
    // return a very large cost to make it prohibitive.
    return Double.MAX_VALUE;
  }

  /**
   * The vectorize method allows converting a instance type into a INDArray vector that can be used
   * by the neural network for prediction.
   *
   * @param instanceType the instance type
   * @return a INDArray vector for the instance type
   */
  private INDArray vectorize(String instanceType) {
    // TODO: Implement me
    return null;
  }

  /**
   * The pricing factor is a function that determines how much above the current spot pricing will
   * the bid be.
   *
   * @param price the current price
   * @return the current bidding increase factor
   */
  public double pricingFactor(double price) {
    // TODO: Implement the probability method
    return price + price * AWS_PRICING_FACTOR;
  }
}
