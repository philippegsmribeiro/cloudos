package cloudos.machinelearning.spotbidder;

import cloudos.Providers;
import cloudos.instances.spotinstances.GoogleSpotInstanceManager;
import cloudos.models.spotinstances.CloudSpotPricingRequest;
import cloudos.pricing.PricingValue;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The GoogleSpotBidderManager is used to manage Google Engine Preemtible instances, including
 * prediction, managing preemptions and requesting instances. We implement it as a interface in
 * order to standardize between different providers.
 */
@Service
public class GoogleSpotBidderPredictor implements SpotBidderPredictor {

  @Autowired
  GoogleSpotInstanceManager googleSpotInstanceManager;

  @Override
  public double predict(CloudSpotPricingRequest pricingRequest) {
    // just one
    List<?> pricing = googleSpotInstanceManager
        .pricing(new CloudSpotPricingRequest(null, Providers.GOOGLE_COMPUTE_ENGINE,
            pricingRequest.getRegion(), null, null, null, pricingRequest.getInstanceTypes(), null));
    if (CollectionUtils.isNotEmpty(pricing)) {
      Object pricingObject = pricing.get(0);
      if (pricingObject instanceof PricingValue) {
        return ((PricingValue) pricingObject).getValue();
      }
    }
    return 0;
  }
  
}
