package cloudos.machinelearning;

import cloudos.amazon.AmazonInstanceRepository;
import cloudos.pricing.AmazonPricingManagement;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;

import aima.core.search.csp.Variable;

/**
 * Created by philipperibeiro on 12/9/16.
 *
 * <p>Define the General constraint to be used in the CloudOS project.
 */
@Log4j2
public abstract class CloudConstraint {

  /* Specify the scope of this constraint */
  protected List<Variable> scope;

  /* Mongo Access for the Amazon Instance repository */
  @Autowired
  protected static AmazonInstanceRepository instanceRepository;

  @Autowired
  protected static AmazonPricingManagement pricingManagement;

  /** Define the Default Constructor. */
  public CloudConstraint() {
    this.scope = new ArrayList<>();
  }

  /**
   * Define the construtor for the CloudConstraint class.
   *
   * @param instance the repository of Amazon instances
   * @param pricingRepository the repository of the Amazon pricing
   */
  public CloudConstraint(
      AmazonInstanceRepository instance, AmazonPricingManagement pricingRepository) {
    this.setAmazonInstanceRepository(instance);
    this.setPricingManagement(pricingRepository);
    this.scope = new ArrayList<>();
  }

  /**
   * Set the AmazonInstanceRepository.
   *
   * @param repository an AmazonInstanceRepository repository
   */
  public void setAmazonInstanceRepository(final AmazonInstanceRepository repository) {
    instanceRepository = repository;
  }

  /**
   * Set the AmazonInstancePricingRepository.
   *
   * @param repository an AmazonInstanceRepository repository
   */
  public void setPricingManagement(final AmazonPricingManagement repository) {
    pricingManagement = repository;
  }

  /**
   * Get the current constraint scope.
   *
   * @return a list of variables
   */
  public List<Variable> getScope() {
    return this.scope;
  }
}
