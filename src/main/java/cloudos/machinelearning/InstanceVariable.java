package cloudos.machinelearning;

import java.util.ArrayList;
import java.util.List;

import aima.core.search.csp.Variable;
import cloudos.models.AbstractInstance;
import lombok.Data;

/** Created by philipperibeiro on 12/7/16. */
@Data
public class InstanceVariable extends Variable {

  protected AbstractInstance instance;
  protected String instanceType;
  protected List<Double> scaledFeatureVector;
  protected double totalPrice;
  protected boolean keep;

  /**
   * Constructor that allows to use the Variable.
   *
   * @param name the name of the variable
   */
  public InstanceVariable(final String name) {
    super(name);
    this.instance = null;
    this.scaledFeatureVector = new ArrayList<>();
    this.totalPrice = 0;
  }

  /**
   * Constructor allows all the parameters to be specified.
   *
   * @param name the name of the instance
   * @param instance the provider of the instance
   * @param instanceType the type of the instance
   */
  public InstanceVariable(final String name, AbstractInstance instance, final String instanceType) {
    super(name);
    this.instance = instance;
    this.instanceType = instanceType;
    this.scaledFeatureVector = new ArrayList<>();
    this.totalPrice = 0;
  }

}
