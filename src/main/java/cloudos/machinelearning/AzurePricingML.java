package cloudos.machinelearning;

import java.util.List;

import org.apache.spark.ml.feature.LabeledPoint;
import org.springframework.beans.factory.annotation.Autowired;

import cloudos.microsoft.AzureInstance;
import cloudos.microsoft.AzureInstanceRepository;

/** Created by philipperibeiro on 11/26/16. */
public class AzurePricingML extends InstancePricingML<AzureInstance> {

  @Autowired private AzureInstanceRepository instanceRepository;

  // @Autowired private AzurePricingRepository pricingRepository;

  private static final String VM_SUFFIX = " VM";

  /** Default constructor. */
  public AzurePricingML() {
    super();
  }

  /**
   * Get a sample from the `azure_instance` collection. Since there are only 86 entries we return
   * all of them.
   *
   * @param start the starting index of the query
   * @param end the ending index of the query
   */
  @Override
  public List<AzureInstance> getSample(int start, int end) {
    return this.instanceRepository.findAll();
  }

  @Override
  public LabeledPoint getLabeledPoint(AzureInstance instance) {
    // TODO Auto-generated method stub
    return null;
  }
}
