package cloudos.machinelearning.nlp;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.validation.constraints.NotNull;

import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.glove.Glove;
import org.deeplearning4j.text.sentenceiterator.BasicLineIterator;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;

import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

/** Created by philipperibeiro on 3/2/17. */
@Log4j2
@NoArgsConstructor
public class GloVeService {

  private static double ALPHA = 0.5;

  private static double LEARNING_RATE = 0.1;

  private static int EPOCHS = 25;

  private static int CUTOFF = 100;

  private static int BATCH_SIZE = 1000;

  private static boolean SHUFFLE = true;

  private static boolean SYMMETRIC = true;

  /**
   * @param filepath
   * @return
   */
  public Glove build(String filepath) throws FileNotFoundException {
    return this.build(filepath, ALPHA, LEARNING_RATE, EPOCHS, CUTOFF, BATCH_SIZE, SHUFFLE,
        SYMMETRIC);
  }

  /**
   * @param filepath
   * @param alpha
   * @param learningRate
   * @param epochs
   * @param cutoff
   * @param batchSize
   * @param shuffle
   * @param symmetric
   * @return
   */
  public Glove build(String filepath, double alpha, double learningRate, int epochs, int cutoff,
      int batchSize, boolean shuffle, boolean symmetric) throws FileNotFoundException {

    // creating SentenceIterator wrapping our training corpus
    SentenceIterator iter = new BasicLineIterator(filepath);

    // Split on white spaces in the line to get words
    TokenizerFactory t = new DefaultTokenizerFactory();
    t.setTokenPreProcessor(new CommonPreprocessor());

    Glove glove = new Glove.Builder().iterate(iter).tokenizerFactory(t).alpha(alpha)
        .learningRate(learningRate)

        // number of epochs for training
        .epochs(25)

        // cutoff for weighting function
        .xMax(100)

        // training is done in batches taken from training corpus
        .batchSize(batchSize)

        // if set to true, batches wil lbe shuffled before training
        .shuffle(shuffle)

        // if set to true word pairs will be build in both directions, LTR and RTL
        .symmetric(symmetric).build();

    glove.fit();

    return glove;
  }

  /**
   * Write the word vectors to a file
   *
   * @param glove: A Glove object
   * @param filepath: The file output path
   * @throws IOException: If it can't write to the file
   */
  public void writeWordVectors(@NotNull Glove glove, @NotNull String filepath) throws IOException {
    log.info("Saving vectors....");
    WordVectorSerializer.writeWordVectors(glove, filepath);
  }
}
