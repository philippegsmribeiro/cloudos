package cloudos.machinelearning.nlp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.constraints.NotNull;

import lombok.extern.log4j.Log4j2;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.GravesBidirectionalLSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.stats.StatsListener;
import org.deeplearning4j.ui.storage.InMemoryStatsStorage;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.indexing.NDArrayIndex;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/** Created by philipperibeiro on 3/4/17. */
@Service
@Log4j2
public class SentimentRNNService {

  @Value("${cloudos.ml.data.folder}")
  private String dataPath;

  @Value("${cloudos.ml.data.sentiment.url}")
  private String dataUrl;

  @Value("${cloudos.ml.data.word.glove}")
  private String wordVectorsPath;

  @Value("{cloudos.ml.data.sentiment.output}")
  private String output;

  /**
   * **************************************************** Define the default hyperparameter *
   * ****************************************************
   */
  // number of examples in each minibatch
  private static int BATCH_SIZE = 100;

  // Size of the word vectors. 300 in the Google News model
  private static int VECTOR_SIZE = 300;

  // Number of epochs (full passes of training data) to train on
  private static int N_EPOCHS = 1;

  // truncate reviews with length (# words) greater than this
  private static int TRUNCATE_REVIEWS_TO_LENGTH = 256;

  // define the number of classes being classified (just positive and negative for now).
  private static int CLASSES = 2;

  private static double LEARNING_RATE = 2e-2;

  public SentimentRNNService() {}

  public SentimentRNNService(String dataPath, String dataUrl, String wordVectorsPath,
      String output) {
    this.dataPath = dataPath;
    this.dataUrl = dataUrl;
    this.wordVectorsPath = wordVectorsPath;
    this.output = output;
  }

  public MultiLayerConfiguration getMultiLayerConfiguration() {
    MultiLayerConfiguration conf =
        new NeuralNetConfiguration.Builder().updater(Updater.ADAM).adamMeanDecay(0.9)
            .adamVarDecay(0.999).regularization(true).l2(1e-5).weightInit(WeightInit.XAVIER)
            .gradientNormalization(GradientNormalization.ClipElementWiseAbsoluteValue)
            .gradientNormalizationThreshold(1.0).learningRate(LEARNING_RATE).list()
            .layer(0,
                new GravesBidirectionalLSTM.Builder().nIn(VECTOR_SIZE)
                    .nOut(TRUNCATE_REVIEWS_TO_LENGTH).activation(Activation.TANH).build())
            .layer(1,
                new RnnOutputLayer.Builder().activation(Activation.SOFTMAX)
                    .lossFunction(LossFunctions.LossFunction.MCXENT).nIn(TRUNCATE_REVIEWS_TO_LENGTH)
                    .nOut(CLASSES).build())
            .pretrain(false).backprop(true).build();

    return conf;
  }

  public void trainNeuralNetwork(MultiLayerConfiguration conf, boolean saveModel)
      throws IOException {
    MultiLayerNetwork net = new MultiLayerNetwork(conf);
    net.init();
    net.setListeners(new ScoreIterationListener(1));

    // DataSetIterators for training and testing respectively
    // DataSetIterators for training and testing respectively
    WordVectors wordVectors = WordVectorSerializer.loadTxtVectors(new File(this.wordVectorsPath));
    SentimentIterator train = new SentimentIterator(this.dataPath, wordVectors, BATCH_SIZE,
        TRUNCATE_REVIEWS_TO_LENGTH, true);
    SentimentIterator test = new SentimentIterator(this.dataPath, wordVectors, BATCH_SIZE,
        TRUNCATE_REVIEWS_TO_LENGTH, false);

    log.info("Starting training....");
    for (int i = 0; i < N_EPOCHS; i++) {
      net.fit(train);
      train.reset();
      log.info("Epoch " + i + " completed. Starting evalution: ");

      // Run evaluation. This is on 25k reviews, so can take some time.
      Evaluation evaluation = new Evaluation();
      while (test.hasNext()) {
        DataSet t = test.next();
        INDArray features = t.getFeatureMatrix();
        INDArray lables = t.getLabels();
        INDArray inMask = t.getFeaturesMaskArray();
        INDArray outMask = t.getLabelsMaskArray();
        INDArray predicted = net.output(features, false, inMask, outMask);

        evaluation.evalTimeSeries(lables, predicted, outMask);
      }
      test.reset();

      log.info(evaluation.stats());
    }

    // After training: load a single example and generate predictions
    File firstPositiveReviewFile =
        new File(FilenameUtils.concat(this.dataPath, "logs/sentiment/test/pos/java_103cf"));
    String firstPositiveReview = FileUtils.readFileToString(firstPositiveReviewFile);

    INDArray features =
        test.loadFeaturesFromString(firstPositiveReview, TRUNCATE_REVIEWS_TO_LENGTH);
    INDArray networkOutput = net.output(features);
    int timeSeriesLength = networkOutput.size(CLASSES);
    INDArray probabilitiesAtLastWord = networkOutput.get(NDArrayIndex.point(0), NDArrayIndex.all(),
        NDArrayIndex.point(timeSeriesLength - 1));

    log.info("\n\n-------------------------------");
    log.info("First positive review: \n" + firstPositiveReview);
    log.info("\n\nProbabilities at last time step:");
    log.info("p(negative): " + probabilitiesAtLastWord.getDouble(0));
    log.info("p(positive): " + probabilitiesAtLastWord.getDouble(1));

    log.info("----- Example complete -----");
    if (saveModel) {
      this.save(net, this.output, true);
    }
  }

  /**
   * @param net
   * @param destination
   * @param saveUpdater
   * @throws IOException
   */
  public void save(@NotNull MultiLayerNetwork net, @NotNull String destination, boolean saveUpdater)
      throws IOException {
    File location = new File(destination);
    // create the destination
    if (!location.exists()) {
      cloudos.utils.FileUtils.create(destination);
    }
    String fileName = new SimpleDateFormat("sentiment_model_yyyyMMddHHmm'.zip'").format(new Date());
    File model = new File(destination, fileName);
    ModelSerializer.writeModel(net, model, saveUpdater);
  }

  /**
   * @param filepath
   * @return
   * @throws IOException
   */
  public MultiLayerNetwork load(@NotNull String filepath) throws IOException {
    File model = new File(filepath);
    if (!model.exists()) {
      // Load the model
      throw new FileNotFoundException("Could not find file: " + filepath);
    }
    return ModelSerializer.restoreMultiLayerNetwork(model);
  }

  /**
   * Display the Network model
   *
   * @param net
   */
  public void visualizeNetwork(MultiLayerNetwork net, DataSetIterator trainData) {
    // Initialize the user interface backend
    UIServer uiServer = UIServer.getInstance();

    // Configure where the network information (gradients, score vs. time etc) is to be stored.
    // Here: store in memory.
    StatsStorage statsStorage = new InMemoryStatsStorage(); // Alternative: new
                                                            // FileStatsStorage(File), for saving
                                                            // and loading later

    // Attach the StatsStorage instance to the UI: this allows the contents of the StatsStorage to
    // be visualized
    uiServer.attach(statsStorage);

    // Then add the StatsListener to collect this information from the network, as it trains
    int listenerFrequency = 1;
    net.setListeners(new StatsListener(statsStorage, listenerFrequency));

    // Start training:
    net.fit(trainData);

    // Finally: open your browser and go to http://localhost:9000/train
  }
}
