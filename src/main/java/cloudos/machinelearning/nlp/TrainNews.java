package cloudos.machinelearning.nlp;

import java.io.File;

/**
 * Created by philipperibeiro on 3/4/17.
 */

import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.GravesLSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.DataSet;
import org.nd4j.linalg.lossfunctions.LossFunctions;

public class TrainNews {

  public static String userDirectory = "";
  public static String DATA_PATH = "";
  public static String WORD_VECTORS_PATH = "";
  public static WordVectors wordVectors;
  private static TokenizerFactory tokenizerFactory;

  public void train(String userDirectory) throws Exception {
    DATA_PATH = userDirectory + "LabelledNews";
    WORD_VECTORS_PATH = userDirectory + "NewsWordVector.txt";

    int batchSize = 50; // Number of examples in each minibatch
    int nEpochs = 100; // Number of epochs (full passes of training data) to train on
    int truncateReviewsToLength = 300; // Truncate reviews with length (# words) greater than this

    // DataSetIterators for training and testing respectively
    // Using AsyncDataSetIterator to do data loading in a separate thread; this may improve
    // performance vs. waiting for data to load
    wordVectors = WordVectorSerializer.loadTxtVectors(new File(WORD_VECTORS_PATH));

    TokenizerFactory tokenizerFactory = new DefaultTokenizerFactory();
    tokenizerFactory.setTokenPreProcessor(new CommonPreprocessor());

    NewsIterator iTrain = new NewsIterator.Builder().dataDirectory(DATA_PATH)
        .wordVectors(wordVectors).batchSize(batchSize).truncateLength(truncateReviewsToLength)
        .tokenizerFactory(tokenizerFactory).train(true).build();

    NewsIterator iTest = new NewsIterator.Builder().dataDirectory(DATA_PATH)
        .wordVectors(wordVectors).batchSize(batchSize).tokenizerFactory(tokenizerFactory)
        .truncateLength(truncateReviewsToLength).train(false).build();

    int inputNeurons = wordVectors.getWordVector(wordVectors.vocab().wordAtIndex(0)).length; // 100
                                                                                             // in
                                                                                             // our
                                                                                             // case
    int outputs = iTrain.getLabels().size();

    tokenizerFactory = new DefaultTokenizerFactory();
    tokenizerFactory.setTokenPreProcessor(new CommonPreprocessor());
    // Set up network configuration
    MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
        .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT).iterations(1)
        .updater(Updater.RMSPROP).regularization(true).l2(1e-5).weightInit(WeightInit.XAVIER)
        .gradientNormalization(GradientNormalization.ClipElementWiseAbsoluteValue)
        .gradientNormalizationThreshold(1.0).learningRate(0.0018).list()
        .layer(0,
            new GravesLSTM.Builder().nIn(inputNeurons).nOut(200).activation(Activation.SOFTSIGN)
                .build())
        .layer(1,
            new RnnOutputLayer.Builder().activation(Activation.SOFTMAX)
                .lossFunction(LossFunctions.LossFunction.MCXENT).nIn(200).nOut(outputs).build())
        .pretrain(false).backprop(true).build();

    MultiLayerNetwork net = new MultiLayerNetwork(conf);
    net.init();
    net.setListeners(new ScoreIterationListener(1));

    System.out.println("Starting training");
    for (int i = 0; i < nEpochs; i++) {
      net.fit(iTrain);
      iTrain.reset();
      System.out.println("Epoch " + i + " complete. Starting evaluation:");

      // Run evaluation. This is on 25k reviews, so can take some time
      Evaluation evaluation = new Evaluation();
      while (iTest.hasNext()) {
        DataSet t = iTest.next();
        INDArray features = t.getFeatureMatrix();
        INDArray lables = t.getLabels();
        // System.out.println("labels : " + lables);
        INDArray inMask = t.getFeaturesMaskArray();
        INDArray outMask = t.getLabelsMaskArray();
        INDArray predicted = net.output(features, false);

        // System.out.println("predicted : " + predicted);
        evaluation.evalTimeSeries(lables, predicted, outMask);
      }
      iTest.reset();

      System.out.println(evaluation.stats());
    }

    ModelSerializer.writeModel(net, userDirectory + "NewsModel.net", true);
    System.out.println("----- Example complete -----");
  }

}
