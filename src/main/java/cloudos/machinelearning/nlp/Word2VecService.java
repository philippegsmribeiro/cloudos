package cloudos.machinelearning.nlp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.validation.constraints.NotNull;

import lombok.extern.log4j.Log4j2;

import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.text.sentenceiterator.BasicLineIterator;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/** Created by philipperibeiro on 2/28/17. */
@Service
@Log4j2
public class Word2VecService {

  /** Define the default values for the Word2Vec */
  private static int MIN_WORD_FREQUENCY = 5;

  private static int ITERATIONS = 1;

  private static int LAYER_SIZE = 100;

  private static int SEED = 42;

  private static int WINDOW_SIZE = 5;

  @Value("${cloudos.ml.data.folder}")
  private String dataPath;

  @Value("${cloudos.ml.data.sentiment.url}")
  private String dataUrl;

  @Value("${cloudos.ml.data.sentiment.url}")
  private String wordVectorsPath;

  public Word2VecService() {}

  /**
   * Constructor for the Word2VecService. It initializes the
   *
   * @param dataPath
   * @param dataUrl
   */
  public Word2VecService(String dataPath, String dataUrl, String wordVectorsPath) {
    this.dataPath = dataPath;
    this.dataUrl = dataUrl;
    this.wordVectorsPath = wordVectorsPath;
  }

  /**
   * Build a Word2Vec object, given a file as training sample.
   *
   * @param filepath: The file path sample.
   * @return A Word2Vec object
   * @throws FileNotFoundException: In the case the file was not found
   */
  public Word2Vec build(String filepath) throws FileNotFoundException {
    return this.build(filepath, MIN_WORD_FREQUENCY, ITERATIONS, LAYER_SIZE, SEED, WINDOW_SIZE);
  }

  /**
   * @param filepath
   * @param wordFrequency
   * @param iterations
   * @param layerSize
   * @param seed
   * @param windowSize
   * @return
   * @throws FileNotFoundException
   */
  public Word2Vec build(
      String filepath, int wordFrequency, int iterations, int layerSize, int seed, int windowSize)
      throws FileNotFoundException {
    log.info("Load & Vectorize Sentences.... ");

    // strip white spaces before and after each line
    SentenceIterator iter = new BasicLineIterator(filepath);

    // Split on white spaces in the line to get words
    TokenizerFactory tokenizerFactory = new DefaultTokenizerFactory();

    /**
     * CommonPreprocessor will apply the following regex to each token: [\d\.:,"'\(\)\[\]|/?!;]+ So,
     * effectively all numbers, punctuation symbols and some special symbols are stripped off.
     * Additionally it forces lower case for all tokens.
     */
    tokenizerFactory.setTokenPreProcessor(new CommonPreprocessor());

    log.info("Building model....");
    Word2Vec word2Vec =
        new Word2Vec.Builder()
            .minWordFrequency(wordFrequency)
            .iterations(iterations)
            .layerSize(layerSize)
            .seed(seed)
            .windowSize(windowSize)
            .iterate(iter)
            .tokenizerFactory(tokenizerFactory)
            .build();

    log.info("Fitting Word2Vec model...");
    word2Vec.fit();

    return word2Vec;
  }

  /**
   * Write the word vectors to a file
   *
   * @param word2Vec: A Word2Vec object
   * @param filepath: The file output path
   * @throws IOException: If it can't write to the file
   */
  public void writeWordVectors(@NotNull Word2Vec word2Vec, @NotNull String filepath)
      throws IOException {
    log.info("Saving vectors....");
    WordVectorSerializer.writeWord2VecModel(word2Vec, filepath);
  }

  /**
   * Read the vectors into memory, producing a new Word2Vec model
   *
   * @param source: The source file for the vectors
   * @return Word2Vec: A Word2Vec representation
   * @throws IOException: If file cannot be read or does not exist.
   */
  public Word2Vec read(@NotNull String source) throws IOException {
    File model = new File(source);
    return WordVectorSerializer.readWord2VecModel(model);
  }
}
