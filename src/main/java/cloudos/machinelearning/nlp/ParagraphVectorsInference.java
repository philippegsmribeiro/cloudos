package cloudos.machinelearning.nlp;

import java.io.File;

import lombok.extern.log4j.Log4j2;

/**
 * Created by philipperibeiro on 3/4/17.
 */

import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.paragraphvectors.ParagraphVectors;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.ops.transforms.Transforms;

/**
 * This is example code for dl4j ParagraphVectors inference use implementation. In this example we
 * load previously built model, and pass raw sentences, probably never seen before, to get their
 * vector representation.
 *
 *
 * *************************************************************************************************
 * PLEASE NOTE: THIS EXAMPLE REQUIRES DL4J/ND4J VERSIONS >= 0.6.0 TO COMPILE SUCCESSFULLY
 * *************************************************************************************************
 *
 *
 */
@Log4j2
public class ParagraphVectorsInference {

  public void inferer(String path) throws Exception {
    String filepath = path + File.separator + "simple.pv";

    File file = new File(filepath);

    // ClassPathResource resource = new ClassPathResource("/paravec/simple.pv");
    TokenizerFactory t = new DefaultTokenizerFactory();
    t.setTokenPreProcessor(new CommonPreprocessor());

    // we load externally originated model
    ParagraphVectors vectors = WordVectorSerializer.readParagraphVectors(file);
    vectors.setTokenizerFactory(t);
    vectors.getConfiguration().setIterations(10); // please note, we set iterations to 1 here, just
                                                  // to speedup inference

    /*
     * // here's alternative way of doing this, word2vec model can be used directly // PLEASE NOTE:
     * you can't use Google-like model here, since it doesn't have any Huffman tree information
     * shipped. ParagraphVectors vectors = new ParagraphVectors.Builder()
     * .useExistingWordVectors(word2vec) .build();
     */
    // we have to define tokenizer here, because restored model has no idea about it


    INDArray inferredVectorA = vectors.inferVector("This is my world .");
    INDArray inferredVectorA2 = vectors.inferVector("This is my world .");
    INDArray inferredVectorB = vectors.inferVector("This is my way .");

    // high similarity expected here, since in underlying corpus words WAY and WORLD have really
    // close context
    log.info("Cosine similarity A/B: {}", Transforms.cosineSim(inferredVectorA, inferredVectorB));

    // equality expected here, since inference is happening for the same sentences
    log.info("Cosine similarity A/A2: {}", Transforms.cosineSim(inferredVectorA, inferredVectorA2));
  }
}
