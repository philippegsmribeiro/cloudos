package cloudos.machinelearning.nlp;

import cloudos.machinelearning.nlp.tools.LabelSeeker;
import cloudos.machinelearning.nlp.tools.MeansBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import lombok.extern.log4j.Log4j2;

import org.deeplearning4j.models.embeddings.inmemory.InMemoryLookupTable;
import org.deeplearning4j.models.paragraphvectors.ParagraphVectors;
import org.deeplearning4j.models.word2vec.VocabWord;
import org.deeplearning4j.text.documentiterator.FileLabelAwareIterator;
import org.deeplearning4j.text.documentiterator.LabelAwareIterator;
import org.deeplearning4j.text.documentiterator.LabelledDocument;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.primitives.Pair;

/**
 * Created by philipperibeiro on 3/4/17.
 */

/**
 * This is basic example for documents classification done with DL4j ParagraphVectors. The overall
 * idea is to use ParagraphVectors in the same way we use LDA: topic space modelling.
 * <p>
 * In this example we assume we have few labeled categories that we can use for training, and few
 * unlabeled documents. And our goal is to determine, which category these unlabeled documents fall
 * into
 * </p>
 * <p>
 * Please note: This example could be improved by using learning cascade for higher accuracy, but
 * that's beyond basic example paradigm.
 * </p>
 */
@Log4j2
public class ParagraphVectorsClassifier {

  ParagraphVectors paragraphVectors;
  LabelAwareIterator iterator;
  TokenizerFactory tokenizerFactory;

  public void classify(String path) throws Exception {

    ParagraphVectorsClassifier app = new ParagraphVectorsClassifier();
    app.makeParagraphVectors(path);
    app.checkUnlabeledData(path);
  }

  public void makeParagraphVectors(String path) throws Exception {
    String filepath = path + File.separator + "labeled";

    File file = new File(filepath);

    // build a iterator for our dataset
    iterator = new FileLabelAwareIterator.Builder().addSourceFolder(file).build();

    tokenizerFactory = new DefaultTokenizerFactory();
    tokenizerFactory.setTokenPreProcessor(new CommonPreprocessor());

    // ParagraphVectors training configuration
    paragraphVectors = new ParagraphVectors.Builder().learningRate(0.025).minLearningRate(0.001)
        .batchSize(100).epochs(20).iterate(iterator).trainWordVectors(true)
        .tokenizerFactory(tokenizerFactory).build();

    // Start model training
    paragraphVectors.fit();
  }

  public void checkUnlabeledData(String path) throws FileNotFoundException {
    /*
     * At this point we assume that we have model built and we can check which categories our
     * unlabeled document falls into. So we'll start loading our unlabeled documents and checking
     * them
     */
    String filepath = path + File.separator + "unlabeled";

    File file = new File(filepath);
    FileLabelAwareIterator unClassifiedIterator =
        new FileLabelAwareIterator.Builder().addSourceFolder(file).build();

    /*
     * Now we'll iterate over unlabeled data, and check which label it could be assigned to Please
     * note: for many domains it's normal to have 1 document fall into few labels at once, with
     * different "weight" for each.
     */
    MeansBuilder meansBuilder = new MeansBuilder(
        (InMemoryLookupTable<VocabWord>) paragraphVectors.getLookupTable(), tokenizerFactory);
    LabelSeeker seeker = new LabelSeeker(iterator.getLabelsSource().getLabels(),
        (InMemoryLookupTable<VocabWord>) paragraphVectors.getLookupTable());

    while (unClassifiedIterator.hasNextDocument()) {
      LabelledDocument document = unClassifiedIterator.nextDocument();
      INDArray documentAsCentroid = meansBuilder.documentAsVector(document);
      List<Pair<String, Double>> scores = seeker.getScores(documentAsCentroid);

      /*
       * please note, document.getLabel() is used just to show which document we're looking at now,
       * as a substitute for printing out the whole document name. So, labels on these two documents
       * are used like titles, just to visualize our classification done properly
       */
      log.info("Document '" + document.getContent() + "' falls into the following categories: ");
      for (Pair<String, Double> score : scores) {
        log.info("        " + score.getFirst() + ": " + score.getSecond());
      }
    }

  }
}
