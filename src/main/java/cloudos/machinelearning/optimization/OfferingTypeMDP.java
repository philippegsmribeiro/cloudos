package cloudos.machinelearning.optimization;

import org.deeplearning4j.gym.StepReply;
import org.deeplearning4j.rl4j.learning.NeuralNetFetchable;
import org.deeplearning4j.rl4j.mdp.MDP;
import org.deeplearning4j.rl4j.mdp.toy.SimpleToyState;
import org.deeplearning4j.rl4j.network.dqn.IDQN;
import org.deeplearning4j.rl4j.space.ArrayObservationSpace;
import org.deeplearning4j.rl4j.space.DiscreteSpace;
import org.deeplearning4j.rl4j.space.ObservationSpace;
import org.json.JSONObject;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class OfferingTypeMDP implements MDP<OfferingTypeState, Integer, DiscreteSpace> {
  
  final private int maxStep;
  //TODO 10 steps toy (always +1 reward2 actions), toylong (1000 steps), toyhard (7 actions, +1 only if actiion = (step/100+step)%7, and toyStoch (like last but reward has 0.10 odd to be somewhere else).
  @Getter
  private DiscreteSpace actionSpace = new DiscreteSpace(2);
  
  @Getter
  private ObservationSpace<OfferingTypeState> observationSpace = new ArrayObservationSpace(new int[] {1});
  private SimpleToyState simpleToyState;
  
  @Setter
  private NeuralNetFetchable<IDQN> fetchable;

  /* The Action Space is either On-Demand or Spot ... for now */
  
  private OfferingTypeState state;
  
  /**
   *
   */
  public OfferingTypeMDP(int maxStep) {
   this.maxStep = maxStep;
  }
  

  @Override
  public ObservationSpace<OfferingTypeState> getObservationSpace() {
    return observationSpace;
  }

  @Override
  public DiscreteSpace getActionSpace() {
    return actionSpace;
  }

  @Override
  public OfferingTypeState reset() {
    if (fetchable != null) {
      printTest(maxStep);
    }
    
    return state = new OfferingTypeState(0, 0);
  }

  private void printTest(int maxStep) {
    INDArray input = Nd4j.create(maxStep, 1);
    for (int i = 0; i < maxStep; i++) {
      input.putRow(i, Nd4j.create(new OfferingTypeState(i, i).toArray()));
    }
    INDArray output = fetchable.getNeuralNet().output(input);
    log.info(output.toString());
  }

  @Override
  public void close() {

  }

  @Override
  public StepReply<OfferingTypeState> step(Integer a) {
    double reward = (state.getStep() % 2 == 0) ? 1 - a : a;
    state = new OfferingTypeState(this.state.getI() + 1, this.state. getStep() + 1);
    return new StepReply<>(state, reward, isDone(), new JSONObject("{}"));
  }

  @Override
  public boolean isDone() {
    return state.getStep() == maxStep;
  }

  public OfferingTypeMDP newInstance() {
    OfferingTypeMDP offeringTypeMDP =  new OfferingTypeMDP(maxStep);
    offeringTypeMDP.setFetchable(this.fetchable);
    return offeringTypeMDP;
  }
}
