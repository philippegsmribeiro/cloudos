package cloudos.machinelearning.optimization;

import org.deeplearning4j.rl4j.space.Encodable;

import lombok.Value;

@Value
public class OfferingTypeState implements Encodable {

  double i;
  int step;

  @Override
  public double[] toArray() {
    double[] ar = new double[1];
    ar[0] = (i + 20);
    return ar;
  }
}
