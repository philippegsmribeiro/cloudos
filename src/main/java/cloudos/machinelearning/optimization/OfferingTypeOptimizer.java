package cloudos.machinelearning.optimization;

import java.io.IOException;

import org.deeplearning4j.rl4j.learning.Learning;
import org.deeplearning4j.rl4j.learning.sync.qlearning.QLearning;
import org.deeplearning4j.rl4j.learning.sync.qlearning.discrete.QLearningDiscreteDense;
import org.deeplearning4j.rl4j.network.dqn.DQNFactoryStdDense;
import org.deeplearning4j.rl4j.network.dqn.IDQN;
import org.deeplearning4j.rl4j.space.DiscreteSpace;
import org.deeplearning4j.rl4j.util.DataManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Implement a Offering Type Optimizer, a Deep Q-Learning Network (DQL) that's going to be
 * used to optimize the cost based on the type of the offering (spot, on-demand, reserved).
 */
@Service
public class OfferingTypeOptimizer {

  @Value("${cloudos.ml.optimizer.offering_type.l2_regularization}")
  private double l2Regularization;

  @Value("${cloudos.ml.optimizer.offering_type.learning_rate}")
  private double learningRate;

  @Value("${cloudos.ml.optimizer.offering_type.num_layers}")
  private int numLayers;

  @Value("${cloudos.ml.optimizer.offering_type.num_hidden_nodes}")
  private int numHiddenNodes;

  // @TODO: later, move all this input to the properties file
  private static QLearning.QLConfiguration OFFERING_TYPE_RL =
      new QLearning.QLConfiguration(
          123,   //Random seed
          100000,//Max step By epoch
          80000, //Max step
          10000, //Max size of experience replay
          32,    //size of batches
          100,   //target update (hard)
          0,     //num step noop warmup
          0.05,  //reward scaling
          0.99,  //gamma
          10.0,  //td-error clipping
          0.1f,  //min epsilon
          2000,  //num step for eps greedy anneal
          true   //double DQN
      );

  /* Define the DQN Network */
  private DQNFactoryStdDense.Configuration OFFERING_TYPE_NETWORK =
      DQNFactoryStdDense.Configuration.builder()
        .l2(0.01)
        .learningRate(1e-2)
        .numLayer(3)
        .numHiddenNodes(16)
        .build();


  public void train() throws IOException {

    // record the training data in rl4j-data in a new folder
    DataManager manager = new DataManager();

    // define the MDP (Markov-Decision-Process)
    OfferingTypeMDP mdp = new OfferingTypeMDP(20);

    // define the training method
    Learning<OfferingTypeState, Integer, DiscreteSpace, IDQN> dql =
        new QLearningDiscreteDense(mdp, OFFERING_TYPE_NETWORK, OFFERING_TYPE_RL, manager);
  
    //enable some logging for debug purposes on toy mdp
    mdp.setFetchable(dql);
    
    // start the training
    dql.train();

    // good practice
    mdp.close();
  }

  public void save() throws IOException {
    // Nothing to do now ...
  }

  public void load() throws IOException {
    // Nothing to do now ...
  }

}
