package cloudos.machinelearning;

import aima.core.search.csp.Assignment;
import aima.core.search.csp.Constraint;
import aima.core.search.csp.Variable;

public class CapacityConstraint extends CloudConstraint implements Constraint {

  /* For this toy constraint problem, there are two variables */
  private Variable var1;
  private Variable var2;

  /**
   * Constructor takes the two constraints as params.
   *
   * @param var1 the first variable
   * @param var2 the second variable
   */
  public CapacityConstraint(Variable var1, Variable var2) {
    super();
    this.var1 = var1;
    this.var2 = var2;
    this.scope.add(var1);
    this.scope.add(var2);
  }

  /**
   * Check if the assignment satisfies the constraint.
   *
   * @param assignment the given assignment
   * @return whether the assignment satisfies the constraint or not
   */
  @Override
  public boolean isSatisfiedWith(Assignment assignment) {
    // we can upcast and downcast inside this method
    Object value = assignment.getAssignment(this.var1);
    return value == null || !value.equals(assignment.getAssignment(this.var2));
  }
}
