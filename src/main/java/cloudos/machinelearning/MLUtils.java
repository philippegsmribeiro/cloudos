package cloudos.machinelearning;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.util.Precision;
import org.apache.spark.ml.linalg.Vector;

import com.google.common.collect.Lists;

import cloudos.data.InstanceDataMetric;
import cloudos.models.CloudosDatapoint;
import lombok.extern.log4j.Log4j2;

@Log4j2
public final class MLUtils {

  private static double HDD_WEIGHT = 1.0;

  private static int IMAGE_HEIGHT = 800;

  private static int IMAGE_WIDTH = 800;

  /* Set the number of decimal places to 3 for double values */
  private static int DOUBLE_ROUND_SCALE = 3;

  private static final char DEFAULT_SEPARATOR = ';';

  private static final String LABEL = "LABEL";

  private static final String CPU_UTILIZATION = "CPU_UTILIZATION";

  private static final String MEMORY_USAGE = "MEMORY_USAGE";

  private static final String DISK_USAGE = "DISK_USAGE";

  private static final String DISK_READ_BYTES = "DISK_READ_BYTES";

  private static final String DISK_WRITE_BYTES = "DISK_WRITE_BYTES";

  private static final String NETWORK_RECEIVED_BYTES = "NETWORK_RECEIVED_BYTES";

  private static final String NETWORK_SENT_BYTES = "NETWORK_SENT_BYTES";

  /** Make the MLUtils class static by making the MLUtils constructor private; */
  private MLUtils() {}

  /**
   * Save all the data points to a final CSV file
   *
   * @param datapoints: The list of data points to be saved.
   * @param csvFile: The csv file the data points will be stored.
   */
  public static void saveDatapoints(
      @NotNull List<CloudosDatapoint> datapoints, @NotNull final String csvFile) {
    if (CollectionUtils.isEmpty(datapoints)) {
      log.warn("List is data points is empty");
      return;
    }

    // add try with resoruces
    try (FileWriter writer = new FileWriter(csvFile)) {

      List<String> labels =
          Arrays.asList(
              LABEL,
              CPU_UTILIZATION,
              MEMORY_USAGE,
              DISK_USAGE,
              DISK_READ_BYTES,
              DISK_WRITE_BYTES,
              NETWORK_RECEIVED_BYTES,
              NETWORK_SENT_BYTES);
      // for header
      // CsvUtils.writeLine(writer, Arrays.asList ("CPU_UTILIZATION", "LABEL"));

      double feature = datapoints.get(0).getValue();

      // Y_t = X_t + 1
      for (int i = 1; i < datapoints.size(); i++) {
        CloudosDatapoint datapoint = datapoints.get(i);
        Map<InstanceDataMetric, Double> mapVector = getVectorMap();
        mapVector.put(
            datapoint.getMetric(), Precision.round(datapoint.getValue(), DOUBLE_ROUND_SCALE));
        List<Double> ds = new ArrayList<>();
        ds.add(Precision.round(feature, DOUBLE_ROUND_SCALE));
        // add the label
        ds.add(datapoint.getValue());
        feature = datapoint.getValue();

        List<String> strings = Lists.transform(ds, Object::toString);

        // write to the file
        cloudos.utils.CsvUtils.writeLine(writer, strings, DEFAULT_SEPARATOR);
      }

      writer.flush();

    } catch (IOException e) {
      log.error(e.getMessage());
    }
  }

  /**
   * Build a default organization of the datapoint vector x_1.
   *
   * @return Map<InstanceDataMetric, Double>: A map between the Instance Data Metric and its value.
   */
  public static Map<InstanceDataMetric, Double> getVectorMap() {
    Map<InstanceDataMetric, Double> map = new LinkedHashMap<>();

    // add the vector ordering, initializing it to zero
    map.put(InstanceDataMetric.CPU_UTILIZATION, 0.0);
    map.put(InstanceDataMetric.MEMORY_USAGE, 0.0);
    map.put(InstanceDataMetric.DISK_USAGE, 0.0);
    map.put(InstanceDataMetric.DISK_READ_BYTES, 0.0);
    map.put(InstanceDataMetric.DISK_WRITE_BYTES, 0.0);
    map.put(InstanceDataMetric.NETWORK_RECEIVED_BYTES, 0.0);
    map.put(InstanceDataMetric.NETWORK_SENT_BYTES, 0.0);

    return map;
  }

  /**
   * Convert a list of lists to an array of list.
   *
   * @param list: A list of lists to be converted.
   * @param <T>: The type of the element being converted.
   * @return
   */
  @SuppressWarnings("unchecked")
  public static <T> List<T>[] toArray(@NotNull List<List<T>> list) {
    List<T>[] arrayList = (List<T>[]) new List[list.size()];
    for (int i = 0; i < list.size(); i++) {
      arrayList[i] = list.get(i);
    }
    return arrayList;
  }

  /**
   * Takes a String representing an instance storage, and convert that to an actual double such
   * converts the string into a double value.
   *
   * @param value: The String representing an instance storage, such as 24 x 2000 HDD or 1 x 80 SSD
   * @return a double number, 0 by default
   */
  public static double getStorage(String value) {
    double storage = 0.0;
    if (value == null || value.isEmpty()) {
      return storage;
    }
    try {
      // replace all the non-alphanumeric elements
      if (value.contains("SSD")) {
        String expression = value.replaceAll("SSD", "");
        storage = calculateStorage(expression) * HDD_WEIGHT;
      } else if (value.contains("HDD")) {
        String expression = value.replaceAll("HDD", "");
        storage = calculateStorage(expression) * HDD_WEIGHT;
      } else if (value.contains("EBS only")) {
        // @TODO: Better evaluation for the EBS only function.
        storage = 0.0;
      } else if (StringUtils.isAlphanumeric(value)) {
        return calculateStorage(value);
      }
    } catch (Exception ex) {
      log.error(ex);
      storage = 0.0;
    }
    return storage;
  }

  /**
   * @param entry
   * @return
   */
  public static Double convertPrice(String entry) {
    Double price = null;
    try {
      price = Double.parseDouble(entry);
    } catch (Exception ex) {
      price = null;
    }
    return price;
  }
  /**
   * Takes a String, either as a number represented as string, or a number such as 24 x 2000 and
   * returns the real number
   *
   * @param value
   * @return
   */
  public static double calculateStorage(String value) {
    if (value == null || value.isEmpty()) {
      return 0.0;
    }
    if (value.contains("x") || value.contains("X")) {
      String[] parts = value.trim().split("x|X");
      if (parts.length == 1) {
        return Double.parseDouble(parts[0]);
      } else if (parts.length == 2) {
        return Double.parseDouble(parts[0]) * Double.parseDouble(parts[1]);
      }
    } else if (StringUtils.isAlphanumericSpace(value)) {
      return Double.parseDouble(value);
    }
    return 0.0;
  }

  /**
   * @param a
   * @param b
   * @return
   */
  public static double dot(Vector a, Vector b) {
    double[] x = a.toArray();
    double[] y = b.toArray();
    if (x.length != y.length) {
      throw new IllegalArgumentException("The vectors dimensions don't agree");
    }
    double sum = 0.0;
    for (int i = 0; i < x.length; i++) {
      sum = sum + (x[i] * y[i]);
    }
    return sum;
  }
  /**
   * @param beta
   * @param round
   * @return
   */
  public static String getFunction(double[] beta, int round) {
    StringBuilder builder = new StringBuilder();
    for (int i = 0; i < beta.length; i++) {
      double coeff = Precision.round(beta[i], round);
      if (i == 0) {
        if (i == beta.length - 1) {
          builder.append(String.format("%s", coeff));
        } else {
          builder.append(String.format("%s + ", coeff));
        }
      } else {
        if (i == beta.length - 1) {
          builder.append(String.format("%s*X[%s]", coeff, i));
        } else {
          builder.append(String.format("%s*X[%s] + ", coeff, i));
        }
      }
    }
    return builder.toString();
  }

  /**
   * Get the scalar (non-weighted) value for a vector
   *
   * @param vector
   * @return
   */
  public static double getScalar(List<Double> vector) {
    double total = 0;
    for (Double value : vector) {
      total += value;
    }
    return total;
  }

  /**
   * @param vector
   * @param value
   * @return
   */
  public static List<Double> elementWiseMultiplication(List<Double> vector, final double value) {
    List<Double> scalarVector = new ArrayList<>();
    if (value >= 1.0) {
      for (Double element : vector) {
        scalarVector.add(element * value);
      }
    }
    return scalarVector;
  }

  /** @param matrix */
  public static void printMatrix(double[][] matrix) {
    Arrays.stream(matrix)
        .forEach(
            (row) -> {
              System.out.print("[");
              Arrays.stream(row).forEach((el) -> System.out.print(" " + el + " "));
              System.out.println("]");
            });
  }
}
