package cloudos.machinelearning;

import static org.apache.commons.text.CharacterPredicates.DIGITS;
import static org.apache.commons.text.CharacterPredicates.LETTERS;

import au.com.bytecode.opencsv.CSVReader;

import cloudos.machinelearning.autoscaler.AutoscaleLabels;
import cloudos.machinelearning.autoscaler.AutoscalerVector;
import cloudos.machinelearning.autoscaler.DatapointVector;
import cloudos.machinelearning.autoscaler.MetricbeatVector;
import cloudos.models.SparkJob;

import com.google.code.externalsorting.ExternalSort;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import lombok.extern.log4j.Log4j2;

import org.apache.commons.io.FileUtils;
import org.apache.commons.text.RandomStringGenerator;
import org.apache.spark.api.java.JavaSparkContext;

import scala.Tuple2;

/**
 * Created by philipperibeiro on 7/10/17.
 * The FeatureExtractor interface is used to extract feature vectors from input source datas.
 */
@Log4j2
public abstract class FeatureExtractor extends SparkJob {

  protected static Gson gson =
      new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();

  protected static final int VECTOR_INDEX = 2;

  protected static final String CSV_EXTENSION = ".csv";

  protected static final String UTF_8 = "UTF-8";

  protected static final char SEPARATOR = ',';

  protected static final char QUOTE_CHAR = '"';

  protected static final int START_LINE = 0;

  protected static final String CLASSIFIER_FILE = "autoscale_classifier.csv";

  protected static RandomStringGenerator generator = new RandomStringGenerator.Builder()
      .withinRange('0', 'z')
      .filteredBy(LETTERS, DIGITS)
      .build();

  public FeatureExtractor() {
    super();
  }

  /**
   * Assign the JavaSparkContext to the context.
   *
   * @param sc a JavaSparkContext context
   */
  public FeatureExtractor(JavaSparkContext sc) {
    super(sc);
  }

  public abstract void readStream(final String path, final String destination);

  public abstract void extract(final String source, final String destination) throws IOException;

  public abstract void merge(final String csvInput, final String output) throws IOException;

  /**
   * Given two DatapointVector, merge both of them into a single one.
   *
   * @param x the first DatapointVector to be merged
   * @param y the second DatapointVector to be merged
   * @return a merged DatapointVector
   */
  public AutoscalerVector merge(@NotNull AutoscalerVector x, @NotNull AutoscalerVector y,
      Class<?> cls) throws ClassNotFoundException {

    List<Double> merged = new ArrayList<>();
    for (int i = 0; i < x.getVector().size() && i < y.getVector().size(); i++) {
      merged.add(x.getVector().get(i) + y.getVector().get(i));
    }
    if (cls == MetricbeatVector.class) {
      return new MetricbeatVector(x.getTimestamp(), x.getName(), merged);
    } else if (cls == DatapointVector.class) {
      return new DatapointVector(x.getTimestamp(), x.getName(), merged);
    } else {
      throw new ClassNotFoundException("Clould not find the type for " + cls.getName());
    }
  }

  /**
   * Implement a Comparator for the Tuple2 (Long, String). We want to merge the metric beat vector
   * by the time stamp and by the name of the instance.
   */
  public static class FeatureComparator implements Comparator<Tuple2<Long, String>>, Serializable {

    private static final long serialVersionUID = 1L;

    @Override
    public int compare(Tuple2<Long, String> o1, Tuple2<Long, String> o2) {
      if (o1._1().compareTo(o2._1()) < 0) {
        return -1;
      } else if (o1._1().compareTo(o2._1()) == 0) {
        if (o1._2().compareTo(o2._2()) < 0) {
          return -1;
        } else if (o1._2().compareTo(o2._2()) > 0) {
          return 1;
        }
        return 0;
      }
      return 1;
    }
  }

  /**
   * Assign the autoscaler labels given the vectors stored in the filepath directory. The output is
   * written to the destination file.
   *
   * @param filepath the filepath to the source file containing the vectors
   * @param destination the destination for the file containing the labeled files
   */
  public void labels(final String filepath, final String destination) {
    try (CSVReader reader =
        new CSVReader(new FileReader(filepath), SEPARATOR, QUOTE_CHAR, START_LINE)) {
      String[] line;
      // add the try catch block in case of failure reading the file.
      // TODO: Add the labels for the resource amount
      File classifier = new File(destination, CLASSIFIER_FILE);
      // delete the previous created file
      if (classifier.exists()) {
        classifier.delete();
      }

      FileOutputStream outputStream = new FileOutputStream(classifier);
      while ((line = reader.readNext()) != null) {
        List<Double> vector = new ArrayList<>();
        /*
         * Since we ignore the timestamp and the host name, we can exclude them from the metric beat
         * vector.
         */
        for (int i = VECTOR_INDEX; i < line.length; i++) {
          vector.add(Double.parseDouble(line[i]));
        }
        // create a new Datapoint vector
        AutoscalerVector autoscalerVector = new AutoscalerVector();
        autoscalerVector.setTimestamp(Long.parseLong(line[0]));
        autoscalerVector.setName(line[1]);
        autoscalerVector.setVector(vector);

        AutoscaleLabels label = autoscalerVector.label(vector.get(0));
        String row = String.format("%s,%s\n", label.getValue(), autoscalerVector.vectorToString());
        outputStream.write(row.getBytes());
      }
      outputStream.close();
    } catch (IOException e1) {
      log.error(e1.getMessage());
    }
  }

  /**
   * Merge all the files in a folder, sort and merge the file into the 'merged' output.
   *
   * @param path the directory path
   * @param merged the merged output file
   * @throws IOException whenever an I/O issues raises up
   */
  public void mergeFiles(final String path, final String merged) throws IOException {
    List<File> files = Files.walk(Paths.get(path)).filter(Files::isRegularFile).map(Path::toFile)
        .collect(Collectors.toList());
    ExternalSort.mergeSortedFiles(files, new File(merged));
  }

  /**
   * Save the content in the Map into the output file.
   *
   * @param map the input source data to be added
   * @param output the output file
   * @throws IOException whenever an I/O issues raises up
   */
  protected void save(final Map<Tuple2<Long, String>, AutoscalerVector> map, final String output)
      throws IOException {
    // remove the old output file
    File file = new File(output);
    if (file.exists()) {
      if (file.isDirectory()) {
        FileUtils.deleteDirectory(file);
      } else {
        if (!file.delete()) {
          throw new IOException("Failed to create the directory for path " + output);
        }
      }
    }

    FileOutputStream outputStream = new FileOutputStream(output);

    map.forEach((k, v) -> {
      byte[] strToBytes = v.toString().getBytes();
      try {
        outputStream.write(strToBytes);
      } catch (IOException e) {
        log.error(e.getMessage());
      }
    });

    outputStream.close();
  }
}
