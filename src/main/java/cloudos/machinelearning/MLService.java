package cloudos.machinelearning;

import java.lang.reflect.Type;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import aima.core.util.datastructure.LabeledGraph;
import cloudos.Providers;
import cloudos.dashboard.Provider;
import cloudos.exceptions.NotImplementedException;
import cloudos.instances.InstanceService;
import cloudos.machinelearning.models.CloudGraph;
import cloudos.machinelearning.models.CloudGraphCost;
import cloudos.machinelearning.models.EquivalentRegion;
import cloudos.models.Instance;
import cloudos.optimizer.models.CloudEquivalenceRequest;
import cloudos.pricing.AmazonPricingContent;
import cloudos.pricing.PricingInfo;
import cloudos.pricing.PricingManagementService;
import cloudos.pricing.PricingValue;
import cloudos.provider.ProviderMachineType;
import cloudos.provider.ProvidersService;
import edu.emory.mathcs.backport.java.util.Collections;
import lombok.extern.log4j.Log4j2;

/**
 * The ML Service is responsible for handling functionality across the core ML system for
 * CloudTown.
 *
 * @author Philippe Ribeiro
 */
@Service
@Log4j2
public class MLService {

  private static final String DEFAULT_IMAGE_PROJECT = "Linux";

  private static final String DEFAULT_CURRENCY = "USD";

  private static final int CORES_PER_CPU = 2;

  private static Gson gson = new Gson();

  @Autowired
  private EquivalentRegion equivalentRegion;

  @Autowired
  private PricingManagementService pricingManagementService;

  @Autowired
  private ProvidersService providersService;

  @Autowired
  private InstanceService instanceService;

  /* Comparator for Machine Types based on the CPU size */
  public Comparator<ProviderMachineType> cpuComparator =
      (Comparator<ProviderMachineType>) (m1, m2) -> {
        if (m1 == null && m2 == null) {
          return 0;
        }
        if (m1 == null) {
          return -1;
        }
        if (m2 == null) {
          return 1;
        }
        return Double.compare(m1.getCpu(), m2.getCpu());
      };

  /* Comparator for Machine Types based on the memory size */
  public Comparator<ProviderMachineType> memoryComparator =
      (Comparator<ProviderMachineType>) (m1, m2) -> {
        if (m1 == null && m2 == null) {
          return 0;
        }
        if (m1 == null) {
          return -1;
        }
        if (m2 == null) {
          return 1;
        }
        return Double.compare(m1.getMemory(), m2.getMemory());
      };

  private static Map<Providers, List<ProviderMachineType>> cpuCapacityTable = new HashMap<>();

  private static Map<Providers, List<ProviderMachineType>> memoryCapacityTable = new HashMap<>();

  /**
   * Build the table based on the computer unit capacity.
   */
  private void buildComputeUnitTable() {
    Map<Providers, List<ProviderMachineType>> retrieveMachineTypes =
        this.providersService.retrieveMachineTypes();
    for (Entry<Providers, List<ProviderMachineType>> entrySet : retrieveMachineTypes.entrySet()) {
      // for each provider we will build the table
      Providers provider = entrySet.getKey();
      List<ProviderMachineType> machineTypes = entrySet.getValue();
      if (CollectionUtils.isNotEmpty(machineTypes)) {
        // sort the machine types by the provider
        Collections.sort(machineTypes, cpuComparator);
        cpuCapacityTable.put(provider, machineTypes);

        // sort the machine types by the memory
        Collections.sort(machineTypes, memoryComparator);
        memoryCapacityTable.put(provider, machineTypes);
      }
    }
  }

  /**
   * Given two machine types, find the price difference between them.
   *
   * @param first the first machine type to be analyzed
   * @param second the second machine type to be analyzed
   * @return the difference between them
   */
  public double getPriceDifference(@NotNull ProviderMachineType first ,
                                  @NotNull ProviderMachineType second) {

    double priceDifference = 0;
    if (!first.getProvider().equals(second.getProvider())) {
      return priceDifference;
    }
    try {

      // check if image project is google or aws
      PricingValue firstPrice = this.pricingManagementService.getPricingForInstanceType(
          first.getKey(),
          first.getRegion(),
          false,
          DEFAULT_IMAGE_PROJECT,
          first.getProvider()
      );

      PricingValue secondPrice = this.pricingManagementService.getPricingForInstanceType(
          second.getKey(),
          second.getRegion(),
          false,
          DEFAULT_IMAGE_PROJECT,
          second.getProvider()
      );

      log.info("First Price: {}", firstPrice);
      log.info("Second Price: {}", secondPrice);

      if (firstPrice != null && secondPrice != null) {
        // find the difference between the two instances
        priceDifference = firstPrice.getValue() - secondPrice.getValue();
      }
    } catch (NotImplementedException e) {
      log.error(e);
    }

    return priceDifference;
  }

  /**
   * Given the request, find all the instances belonging to that combination of providers
   * and region, providing that they aren't deleted.
   *
   * @param request the cloud equivalence request
   * @return a cloud graph cost if valid, null otherwise
   */
  public CloudGraphCost cloudCostComparison(@NotNull CloudEquivalenceRequest request) {
    // 1. find all the instances that aren't terminated
    List<Instance> instances = this.instanceService.findAllActiveInstancesByProviderAndRegion(
        request.getFromProvider(), request.getRegion()
    );

    // check status of the instances
    if (CollectionUtils.isNotEmpty(instances)) {
      CloudGraph graph = this.buildCloudGraph(instances,
                                              request.getRegion(),
                                              request.getFromProvider());
      CloudGraph equivalent = this.getEquivalentCloud(graph, request.getToProvider());
      if (graph != null) {
        return this.getCloudGraphCost(equivalent);
      }
    }
    return null;
  }

  /**
   * Attempt to find the suggested next instance boundary.
   *
   * @param providerId the name of the instance to be searched
   * @param isLowerBound if we are looking for a smaller instance
   * @param isCpuBound if we are looking for CPU usage
   * @return ProviderMachineType if we can suggest one
   */
  public ProviderMachineType findMachineTypeBound(@NotNull String providerId, boolean isLowerBound,
                                                  boolean isCpuBound) {

    // load the table
    if (cpuCapacityTable.size() == 0 || memoryCapacityTable.size() == 0) {
      this.buildComputeUnitTable();
    }

    // find the instance first
    ProviderMachineType machineType = null;
    Instance instance = this.instanceService.findByProviderId(providerId);
    if (instance != null) {
      ProviderMachineType currentMachineType = null;
      try {
        currentMachineType = this.providersService.retrieveMachineType(instance.getProvider(),
            instance.getMachineType(), instance.getRegion());
      } catch (NotImplementedException e) {
        log.error("NotImplementedException", e);
      }
      if (currentMachineType != null) {
        // we found the current machine type ...
        List<ProviderMachineType> types;
        if (isCpuBound) {
          types = cpuCapacityTable.get(instance.getProvider());
        } else {
          types = memoryCapacityTable.get(instance.getProvider());
        }

        if (types != null) {
          // find the range of the machine type boundary
          int position = -1;
          for (int i = 0; i < types.size(); i++) {
            if (types.get(i).getKey().equals(currentMachineType.getKey())
                && types.get(i).getRegion().equals(currentMachineType.getRegion())) {
              position = i;
              break;
            }
          }
          if (position == -1) {
            return null;
          } else if (isLowerBound && position == 0) {
            // case 1. We are looking for the lower boundary, and it's the smallest instance
            machineType = currentMachineType;
          } else if (!isLowerBound && position == types.size() - 1) {
            // case 2. We are looking for the upper boundary, and it's the largest instance
            machineType = currentMachineType;
          } else if (isLowerBound) {
            // return the next lowest
            for (int i = position - 1; i >= 0; i++) {
              if (!types.get(i).getKey().equals(currentMachineType.getKey())) {
                machineType = types.get(i);
                break;
              }
            }
          } else {
            for (int i = position + 1; i < types.size(); i++) {
              if (!types.get(i).getKey().equals(currentMachineType.getKey())) {
                machineType = types.get(i);
                break;
              }
            }
          }
        }
      }
    }
    return machineType;
  }

  /**
   * Create a cloud graph with the the instances given.
   *
   * @param instances a list of all the instances
   * @param region the region the instances belong to
   * @param provider the provider the instances belong to
   * @return a newly build CloudGraph with the existing instances
   */
  private CloudGraph buildCloudGraph(@NotNull List<Instance> instances,
                                     String region,
                                     Providers provider) {

    LabeledGraph<Instance, Instance> labeledGraph = new LabeledGraph<>();
    // create 5 instances and add to the graph
    for (Instance instance : instances) {
      // add the instance to the graph
      labeledGraph.addVertex(instance);
    }
    // return a new CloudGraph
    return CloudGraph.builder()
                  .region(region)
                  .provider(provider)
                  .graph(labeledGraph)
                  .build();
  }

  /**
   * Obtain the cloud graph equivalence between different providers.
   *
   * @param graph the graph representing the instances in a single region for the first provider
   * @param provider the another provider that we are looking for its equivalent
   * @return the equivalent graph
   */
  @SuppressWarnings("unchecked")
  public CloudGraph getEquivalentCloud(CloudGraph graph, Providers provider) {
    //1. find the closest region for the provider B
    String region = this.equivalentRegion.getEquivalentRegion(graph.getRegion(), graph.getProvider());

    // check if the region returned is valid
    if (region == null) {
      return null;
    }

    // create an empty equivalent graph
    CloudGraph equivalentGraph = new CloudGraph(new LabeledGraph<>(), region, provider);

    //2. find the cloud equivalent for each one of the instances
    for (Instance instance : graph.getGraph().getVertexLabels()) {
      // find the equivalent instance for that instance
      Instance another = this.getEquivalentInstance(instance, region, provider);
      // check if valid
      if (another != null) {
        equivalentGraph.getGraph().addVertex(another);
      }
    }

    return  equivalentGraph;
  }

  /**
   * Find the equivalent instance for a particular instance in a different cloud provider.
   *
   * @param instance the instance to be searched for equivalent
   * @param region the region the equivalent instance belongs to
   * @param provider the provider the equivalent instance belongs to
   * @return the equivalent instance if valid, null otherwise
   */
  public Instance getEquivalentInstance(Instance instance, String region, Providers provider) {
    // find the compute unit for the current instance
    double instanceCu = this.computeUnit(provider, instance.getMachineType(), instance.getRegion());
    log.info("The CU of the instance {} is {}", instance.getName(), instanceCu);
    if (instanceCu > 0) {
      // find all the instances in the given region
      List<ProviderMachineType> providerMachineTypes = null;
      try {
        if (provider.equals(Providers.AMAZON_AWS)) {
          providerMachineTypes = this.providersService.retrieveMachineTypes(provider).stream()
              .filter(mt -> mt.getRegion().equals(region)).collect(Collectors.toList());
        } else if (provider.equals(Providers.GOOGLE_COMPUTE_ENGINE)) {
          providerMachineTypes = this.providersService.retrieveMachineTypes(provider);
        } else {
          return null;
        }
      } catch (NotImplementedException e) {
        log.error("NotImplementedException", e);
        return null;
      }

      // attempt to find the instance that's the closest in terms of computing units.
      double equivalenceDistance = Double.MAX_VALUE;
      ProviderMachineType candidate = null;
      for (ProviderMachineType providerMachineType : providerMachineTypes) {
        double cu = this.computeUnit(providerMachineType);
        // obtain the absolute distance between then. The closest related should have a score
        // close to zero
        double distance = Math.abs(instanceCu - cu);
        if (equivalenceDistance > distance) {
          candidate = providerMachineType;
          equivalenceDistance = distance;
        }
      }

      if (candidate != null) {
        try {
          // TODO: build a mapping between AWS and Google
          String imageProject;
          if (provider.equals(Providers.AMAZON_AWS)) {
            imageProject = instance.getImageProject();
          }
          else if (provider.equals(Providers.GOOGLE_COMPUTE_ENGINE)) {
            imageProject = DEFAULT_IMAGE_PROJECT;
          }
          else {
            throw new NotImplementedException();
          }

          // check if image project is google or aws
          PricingValue value = this.pricingManagementService.getPricingForInstanceType(
              candidate.getKey(),
              region,
              instance.isSpot(),
              imageProject,
              provider
          );

          // check if the pricing value if valid
          if (value == null) {
            return null;
          }

          // create a equivalent instance
          return Instance.builder()
              .cost(value.getValue())
              .deleted(false)
              .spot(instance.isSpot())
              //.tag(instance.getTag())
              .resourceGroup(instance.getResourceGroup())
              .reserved(instance.isReserved())
              .registered(false)
              .region(candidate.getRegion())
              .provider(candidate.getProvider())
              .machineType(candidate.getKey())
              .imageType(instance.getImageType())
              .imageProject(instance.getImageProject()).build();

        } catch (NotImplementedException e) {
          log.error(e.getMessage());
        }
      }
    }
    return null;
  }

  /**
   * Obtain the pricing value for a aws pricing info.
   *
   * @param info the pricing info object
   * @return the price extracted from the pricing info
   */
  public double getAmazonPricing(@NotNull PricingInfo info) {
    // TODO: Support spot instances
    Type stringStringMap = new TypeToken<AmazonPricingContent>(){}.getType();
    AmazonPricingContent pricingContent = gson.fromJson(info.getContent(), stringStringMap);
    if (pricingContent != null) {
      Map<String, AmazonPricingContent.PriceDimensions> dimensionsMap =
          pricingContent.getPriceDimensions();
      if (dimensionsMap != null) {
        for (AmazonPricingContent.PriceDimensions dimension : dimensionsMap.values()) {
          return Double.valueOf(dimension.getPricePerUnit().get(DEFAULT_CURRENCY));
        }
      }
    }
    return Double.MIN_VALUE;
  }

  /**
   * Obtain the pricing value for a google pricing info.
   *
   * @param info the pricing info object
   * @param region the region the instance belongs
   * @return the price extracted from the pricing info
   */
  public double getGooglePricing(@NotNull PricingInfo info, @NotNull String region) {
    // TODO: Support spot instances
    Type stringStringMap = new TypeToken<Map<String, Object>>(){}.getType();
    Map<String, Object> pricing = gson.fromJson(info.getContent(), stringStringMap);
    // check the region and return the double value
    if (pricing.containsKey(region)) {
      return (Double)pricing.get(region);
    }
    return Double.MIN_VALUE;
  }

  /**
   * Calculate the computing unit for a particular instance.
   *
   * @param instanceType the instance type the computing unit is being calculated
   * @return the CU unit for the instance
   */
  public double computeUnit(Providers provider, String instanceType, String region) {
    // let's define the compute unit (CU) as (2 * CPU cores + GB Ram)
    double cu = 0.0;
    ProviderMachineType machineType = null;
    try {
      machineType = this.providersService.retrieveMachineType(provider, instanceType, region);
    } catch (NotImplementedException e) {
      log.error("NotImplementedException", e);
    }
    if (machineType != null) {
      // if the provider is Google Compute Engine, the RAM memory is given in MB instead of GB
      cu = CORES_PER_CPU * machineType.getCpu() + machineType.getMemory();
    }
    return cu;
  }

  /**
   * Calculate the computing unit for a particular instance.
   *
   * @param machineType the instance type the computing unit is being calculated
   * @return the CU unit for the instance
   */
  public double computeUnit(@NotNull ProviderMachineType machineType) {
    return CORES_PER_CPU * machineType.getCpu() + machineType.getMemory();
  }

  /**
   * Obtain the cost of the cloud graph.
   *
   * @param graph the cloud graph we wish to obtain the cost.
   * @return a CloudGraphCost that represents the graph and the cost
   */
  public CloudGraphCost getCloudGraphCost(@NotNull CloudGraph graph) {
    // set the number of instances from the number of vertex
    int numberInstances = graph.getGraph().getVertexLabels().size();
    int reservedInstances = 0;
    int ondemandinstances = 0;
    int spotInstances = 0;
    double cost = 0;
    // build the graph
    for (Instance instance : graph.getGraph().getVertexLabels()) {
      // check the instance type and obtain the cost
      cost += instance.getCost();
      // increment the instance based on its type
      if (instance.isSpot()) {
        ++spotInstances;
      } else if (instance.isReserved()) {
        ++reservedInstances;
      } else {
        ++ondemandinstances;
      }
    }
    // return the newly created graph cost
    return CloudGraphCost.builder()
                          .cost(cost)
                          .cloudGraph(graph)
                          .provider(graph.getProvider())
                          .numberInstances(numberInstances)
                          .region(graph.getRegion())
                          .ondemandinstances(ondemandinstances)
                          .spotInstances(spotInstances)
                          .reservedInstances(reservedInstances).build();
  }

  /**
   * Obtain the type of the instance, given its name.
   *
   * @param providerId the providerId of the instance
   * @return the type of the instance if found, null otherwise
   */
  public ProviderMachineType getMachineType(String providerId) {
    Instance instance = this.instanceService.findByProviderId(providerId);
    if (instance != null) {
      try {
        return this.providersService.retrieveMachineType(instance.getProvider(),
            instance.getMachineType(), instance.getRegion());
      } catch (NotImplementedException e) {
        log.error("NotImplementedException", e);
      }
    }
    return null;
  }
}
