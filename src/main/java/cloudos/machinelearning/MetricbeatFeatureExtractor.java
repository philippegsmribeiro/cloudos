package cloudos.machinelearning;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.springframework.stereotype.Service;

import au.com.bytecode.opencsv.CSVReader;
import cloudos.machinelearning.autoscaler.AutoscalerVector;
import cloudos.machinelearning.autoscaler.MetricbeatVector;
import cloudos.models.Hit;
import cloudos.models.Metricbeat;
import lombok.extern.log4j.Log4j2;
import scala.Tuple2;

/**
 * Created by philipperibeiro on 7/10/17.
 *
 * <p>The class MetricbeatFeatureExtractor is responsible for extracting CSV files containing the
 * metric beats information. The utilities are used to parse, merge and sort large scale files. The
 * metric beat vectors are merged by the key <timestamp, name>.
 */
@Service
@Log4j2
public class MetricbeatFeatureExtractor extends FeatureExtractor implements Serializable {

  /** Default constructor the FeatureExtractor class. */
  public MetricbeatFeatureExtractor() {
    super();
  }

  /** Default Map function for the Spark job. */
  static PairFunction<MetricbeatVector, Tuple2<Long, String>, MetricbeatVector> mapFunc =
      (PairFunction<MetricbeatVector, Tuple2<Long, String>, MetricbeatVector>)
          x -> {
            // make the timestamp and hostname to be the primary key of the tuple.
            Tuple2<Long, String> key = new Tuple2<>(x.getTimestamp(), x.getName());
            return new Tuple2<>(key, x);
          };

  /** Default Reduce function for the Spark job. */
  static Function2<MetricbeatVector, MetricbeatVector, MetricbeatVector> reduceFunc =
      (Function2<MetricbeatVector, MetricbeatVector, MetricbeatVector>)
          (x, y) -> {
            List<Double> merged = new ArrayList<>();
            for (int i = 0; i < x.getVector().size() && i < y.getVector().size(); i++) {
              merged.add(x.getVector().get(i) + y.getVector().get(i));
            }
            return new MetricbeatVector(x.getTimestamp(), x.getName(), merged);
          };

  /**
   * Merge a list of MetricbeatVectors into a single MetricbeatVector so we don't have sparse
   * vectors.
   *
   * @param vectorList a list of MetricbeatVector to be merged
   * @return a merged MetricbeatVectors
   */
  private MetricbeatVector merge(@NotNull List<MetricbeatVector> vectorList) {
    if (CollectionUtils.isEmpty(vectorList)) {
      return null;
    }

    MetricbeatVector merged = vectorList.get(0);
    for (int i = 1; i < vectorList.size(); i++) {
      merged = this.merge(merged, vectorList.get(i));
    }

    return merged;
  }

  /**
   * Given two MetricbeatVector, merge both of them into a single one.
   *
   * @param x the first MetricbeatVector to be merged
   * @param y the second MetricbeatVector to be merged
   * @return a merged MetricbeatVector
   */
  private MetricbeatVector merge(@NotNull MetricbeatVector x, @NotNull MetricbeatVector y) {
    List<Double> merged = new ArrayList<>();
    for (int i = 0; i < x.getVector().size() && i < y.getVector().size(); i++) {
      merged.add(x.getVector().get(i) + y.getVector().get(i));
    }
    return new MetricbeatVector(x.getTimestamp(), x.getName(), merged);
  }

  /**
   * Merge the content of the csvInput into the output file. This method is used whenever Spark
   * cannot handle the load of the job due to the size of the file.
   *
   * @param csvInput the input CSV file to be merged
   * @param output the merged CSV file
   * @throws IOException whenever an I/O issues raises up
   */
  public void mergeLocal(final String csvInput, final String output) throws IOException {
    CSVReader reader = null;
    try {
      List<MetricbeatVector> metricbeats = new LinkedList<>();
      reader = new CSVReader(new FileReader(csvInput), SEPARATOR, QUOTE_CHAR, START_LINE);
      String[] line;
      while ((line = reader.readNext()) != null) {
        List<Double> vector = new ArrayList<>();
        // add the try catch block in case of failure reading the file.
        try {
          /*
           * Since we ignore the timestamp and the host name, we can exclude them from the metric
           * beat vector.
           */
          for (int i = VECTOR_INDEX; i < line.length; i++) {
            vector.add(Double.parseDouble(line[i]));
          }
          // create a new MetricbeatVector
          MetricbeatVector metricbeatVector = new MetricbeatVector();
          metricbeatVector.setTimestamp(Long.parseLong(line[0]));
          metricbeatVector.setName(line[1]);
          metricbeatVector.setVector(vector);

          metricbeats.add(metricbeatVector);
        } catch (Exception ex) {
          log.error(ex.getMessage());
        }
      }

      //////////////////////////////////////////////////////
      // Perform a local MapReduce job, instead of relying
      // on Spark for the job.
      //////////////////////////////////////////////////////

      // 1. Map
      Map<Tuple2<Long, String>, List<MetricbeatVector>> map =
          new TreeMap<>(new FeatureComparator());
      metricbeats.forEach(
          metricbeatVector -> {
            Tuple2<Long, String> key =
                new Tuple2<>(metricbeatVector.getTimestamp(), metricbeatVector.getName());
            if (map.containsKey(key)) {
              map.get(key).add(metricbeatVector);
            } else {
              List<MetricbeatVector> list = new LinkedList<>();
              list.add(metricbeatVector);
              map.put(key, list);
            }
          });

      Map<Tuple2<Long, String>, AutoscalerVector> reduced = new TreeMap<>(new FeatureComparator());
      // 2. Reduce
      map.forEach(
          (k, v) -> {
            MetricbeatVector merged = this.merge(v);
            if (merged != null) {
              reduced.put(k, merged);
            }
          });
      // save the file to the output directory
      this.save(reduced, output);
      // close the file
      reader.close();

    } catch (IOException e) {
      log.error(e.getMessage());
    }
  }

  /**
   * Merge the content of the csvInput into the output file. Uses Spark to accomplish the job.
   *
   * @param csvInput the input CSV file to be merged
   * @param output the merged CSV file
   * @throws IOException whenever an I/O issues raises up
   */
  @Override
  public void merge(final String csvInput, final String output) throws IOException {

    // check if the file doesn't exist
    if (!new File(csvInput).exists()) {
      throw new IOException(String.format("File '%s' does not exist", csvInput));
    }

    JavaRDD<MetricbeatVector> rdd_records =
        context
            .textFile(csvInput)
            .map(
                (Function<String, MetricbeatVector>)
                    line -> {
                      // Split the CSV line
                      String[] x = line.split(MetricbeatVector.DELIMITER);
                      List<Double> vector = new ArrayList<>();
                      for (int i = VECTOR_INDEX; i < x.length; i++) {
                        vector.add(Double.parseDouble(x[i]));
                      }

                      // create a new MetricbeatVector
                      MetricbeatVector metricbeatVector = new MetricbeatVector();
                      metricbeatVector.setTimestamp(Long.parseLong(x[0]));
                      metricbeatVector.setName(x[1]);
                      metricbeatVector.setVector(vector);

                      return metricbeatVector;
                    })
            .repartition(10);

    JavaPairRDD<Tuple2<Long, String>, MetricbeatVector> result =
        rdd_records.mapToPair(mapFunc).reduceByKey(reduceFunc);

    result.saveAsTextFile(output);
  }

  /**
   * Scan a source directory that contains metric beat hits, extract the vectors and merge into a
   * single file. The destination file will be later used to be sorted and merged again.
   *
   * @param source the source directory
   * @param destination the destination file
   * @throws IOException whenever an I/O issues raises up
   */
  @Override
  public void extract(final String source, final String destination) throws IOException {
    // read all the files in the folder.
    List<File> files =
        Files.walk(Paths.get(source))
            .filter(Files::isRegularFile)
            .map(Path::toFile)
            .collect(Collectors.toList());

    // extract the feature vectors from the elasticsearch
    for (File file : files) {
      // read the file and convert that to a csv file
      String tempfile = generator.generate(10).concat(CSV_EXTENSION);
      Path filePath = Paths.get(file.getParent(), tempfile);
      this.readStream(file.getAbsolutePath(), filePath.toString());

      // delete the old file
      if (!file.delete()) {
        throw new IOException("Failed to delete the file " + file.getAbsolutePath());
      }
    }

    // merge the large files
    this.mergeFiles(source, destination);
  }

  /**
   * Read a file stream containing metric beat hits, and extract the '_source' feature into a
   * MetricBeatVector. The output is then written to an output file to be later processes.
   *
   * @param path the input file containing metric beat hits
   * @param destination the output file with feature vectors extracted
   */
  @Override
  public void readStream(final String path, final String destination) {
    LineIterator it = null;
    try {
      FileOutputStream outputStream = new FileOutputStream(destination);
      it = FileUtils.lineIterator(new File(path), UTF_8);
      while (it.hasNext()) {
        // do something with line
        String line = it.nextLine();
        Type type = this.getType(Hit.class, Metricbeat.class);
        Hit<Metricbeat> hit = gson.fromJson(line, type);
        if (hit.getSource() != null) {
          MetricbeatVector vector = new MetricbeatVector(hit.getSource());
          // write to the output file
          byte[] strToBytes = vector.toString().getBytes();
          outputStream.write(strToBytes);
        }
      }
      outputStream.close();
    } catch (IOException e) {
      log.error(e.getMessage());
    } finally {
      LineIterator.closeQuietly(it);
    }
  }

  /**
   * Read the content of the path file into a List of Metricbeats.
   *
   * @param path the input file containing metric beat hits
   * @return a list of Metribeat
   */
  public List<Metricbeat> readMetricbeats(final String path) {
    LineIterator it = null;
    List<Metricbeat> metricbeats = new ArrayList<>();
    try {
      it = FileUtils.lineIterator(new File(path), UTF_8);
      while (it.hasNext()) {
        String line = it.nextLine();
        Type type = getType(Hit.class, Metricbeat.class);
        Hit<Metricbeat> hit = gson.fromJson(line, type);
        metricbeats.add(hit.getSource());
      }
    } catch (IOException e) {
      log.error(e.getMessage());
    } finally {
      LineIterator.closeQuietly(it);
    }

    return metricbeats;
  }

  /**
   * Implement a Type converter.
   *
   * @param rawClass the class that uses the generic type
   * @param parameter the type T of the generic class
   * @return the ParameterizedType
   */
  public static Type getType(Class<?> rawClass, Class<?> parameter) {
    return new ParameterizedType() {
      @Override
      public Type[] getActualTypeArguments() {
        return new Type[] {parameter};
      }

      @Override
      public Type getRawType() {
        return rawClass;
      }

      @Override
      public Type getOwnerType() {
        return null;
      }
    };
  }
}
