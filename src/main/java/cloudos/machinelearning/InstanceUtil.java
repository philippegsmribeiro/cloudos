package cloudos.machinelearning;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import cloudos.Providers;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

/**
 * The InstanceUtil is responsible for obtaining instance related information such as max IOPS and
 * bandwidth.
 */
@Log4j2
public class InstanceUtil {

  /* Define the singleton obejct */
  private static InstanceUtil singleton = new InstanceUtil();

  private static Gson gson = new Gson();

  private static Map<String, InstanceEntry> map = new HashMap<>();

  private static final String FILENAME = "cloudos/network_benchmark.json";

  /** Set the InstanceUtil constructor private. */
  private InstanceUtil() {
    // load the instances
  }

  /** Load the instances information stored in the network_benchmark.json file. */
  private static void loadInstances() {
    BufferedReader br = null;
    try {
      Resource resource = new ClassPathResource(FILENAME);
      br = new BufferedReader(new InputStreamReader(resource.getInputStream()));
    } catch (IOException e) {
      log.error(e.getMessage());
      return;
    }
    Type type = new TypeToken<List<InstanceEntry>>() {}.getType();
    List<InstanceEntry> instances = gson.fromJson(br, type);
    // if the list if valid, load the entries in the map.
    if (CollectionUtils.isNotEmpty(instances)) {
      instances.forEach(
          instance -> {
            map.put(instance.getInstanceType(), instance);
          });
    }
  }

  /**
   * Get the information about the instance, given its instance type.
   *
   * @param instanceType the type of the instance
   * @return a InstanceEntry that matches the instance type or null
   */
  public InstanceEntry getInstanceEntry(String instanceType) {
    // check if the map is empty
    if (map.isEmpty()) {
      loadInstances();
    }

    if (map.containsKey(instanceType)) {
      return map.get(instanceType);
    }
    return null;
  }

  /**
   * Get the singleton instance of the InstanceUtil.
   *
   * @return a instance of InstanceUtil
   */
  public static InstanceUtil getInstance() {
    return singleton;
  }

  /**
   * The InstanceEntry class is used to represent the entries for all the instances, for our
   * supported cloud providers, the information about bandwidth and IOPS.
   */
  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public class InstanceEntry {

    @SerializedName(value = "provider")
    public Providers provider;

    @SerializedName(value = "instance_type")
    public String instanceType;

    @SerializedName(value = "max_bandwidth")
    public Double maxBandwidth;

    @SerializedName(value = "expected_bandwidth")
    public Double expectedBandwidth;

    @SerializedName(value = "max_iops")
    public Double maxIops;

    @SerializedName(value = "expected_iops")
    public Double expectedIops;

  }
}
