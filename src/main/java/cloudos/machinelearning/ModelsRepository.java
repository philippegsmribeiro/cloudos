package cloudos.machinelearning;

import cloudos.Providers;
import cloudos.amazon.AWSCredentialService;
import cloudos.amazon.S3;
import cloudos.utils.FileUtils;
import cloudos.utils.ProfileUtils;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.annotation.PostConstruct;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ModelsRepository {

  private static final String AWS_PROVIDER = "aws";

  private static final String GOOGLE_PROVIDER = "google";

  @Autowired AWSCredentialService awsCredentialService;

  /* Define the home directory for the ml products for CloudOS */
  @Value("${cloudos.ml.home}")
  String home;

  @Autowired 
  ProfileUtils profileUtils;

  @Value("${cloudos.ml.s3.region}")
  String s3Region;

  @Value("${cloudos.ml.s3.access_key_id}")
  String s3KeyId;

  @Value("${cloudos.ml.s3.secret_key}")
  String s3SecretKey;

  private S3 s3;

  /* Define the name of the bucket that will store the models */
  private String repository;

  /** Initialize the repository. */
  @PostConstruct
  public void init() {
    // set the repository name
    this.setRepositoryName();

    // check if the S3 bucket
    if (this.s3 == null) {
      BasicAWSCredentials credentials = new BasicAWSCredentials(s3KeyId, s3SecretKey);
      this.s3 = new S3(s3Region, new AWSStaticCredentialsProvider(credentials));
    }

    // check if the destination root exists
    if (!FileUtils.exits(home)) {
      FileUtils.create(home);
    }

    // initialize the repository; check if the root bucket exists
    if (!this.s3.bucketExists(repository)) {
      if (this.s3.createBucket(repository) == null) {
        log.error("Failed to create repository in the bucket {}", repository);
      }

      // create all the subdirectories in the repository
      for (Models model : Models.values()) {
        for (Models.Task task : Models.Task.values()) {
          Path filePath = Paths.get(model.toString(), task.toString());
          // create the sub-folders for each model
          if (this.s3.createFolder(repository, filePath.toString()) != null) {
            log.warn("Failed to create folder {} in the repository {}", model, repository);
          }
        }
      }
    }

    // upload the default models for each repository
  }

  /**
   * Upload a source file to a model's task.
   *
   * @param model The name of the model
   * @param task The task the model belongs to.
   * @param source The source file
   */
  public void upload(Models model, Models.Task task, final String source) {
    Path path = Paths.get(source);
    String key = path.getFileName().toString();
    String destination = Paths.get(model.toString(), task.toString(), key).toString();

    log.info("Uploading file {} to {} in bucket {}", key, destination, repository);
    this.s3.uploadFile(repository, destination, source);
  }

  /**
   * Uploads the model to the specified bucket, which is dependant on the provider
   *
   * @param model The name of the machine learning model
   * @param task The name of the task
   * @param provider the provider name
   * @param region the region of the provider
   * @param source the source file
   */
  public void upload(Models model, Models.Task task, Providers provider, final String region,
                     final String source)
  {
    String providerName;
    if (Providers.AMAZON_AWS.equals(provider)) {
      providerName = AWS_PROVIDER;
    } else if (Providers.GOOGLE_COMPUTE_ENGINE.equals(provider)) {
      providerName = GOOGLE_PROVIDER;
    } else {
      log.warn("Invalid provider " + provider);
      return;
    }
    Path path = Paths.get(source);
    String key = path.getFileName().toString();
    String destination = Paths.get(model.toString(), task.toString(), providerName, region,
         key).toString();

    log.info("Uploading file {} to {} in the bucket {}", key, destination, repository);
    this.s3.uploadFile(repository, destination, source);
  }

  /**
   * Downloads a model based on the region the model belongs to.
   *
   * @param model The name of the machine learning model
   * @param task The name of the task
   * @param key The name of the file
   * @param region the region of the provider
   * @throws Exception if something goes wrong
   */
  public void download(Models model, Models.Task task, final String region, final String key)
      throws Exception {

    String keyPath = Paths.get(region, key).toString();

    log.info("Downloading key path: {}", keyPath);
    // invoke the current download implementation
    this.download(model, task, keyPath);
  }

  /**
   * Download the model to the particular model and task definition.
   *
   * @param model The name of the machine learning model
   * @param task The name of the task
   * @param key The name of the file
   * @throws Exception if something goes wrong
   */
  public void download(Models model, Models.Task task, final String key) throws Exception {
    // download the file from the model
    String path = Paths.get(model.toString(), task.toString(), key).toString();
    String root = Paths.get(this.home, model.toString(), task.toString()).toString();

    // check if the destination root exists
    if (!FileUtils.exits(root)) {
      FileUtils.create(root);
    }

    String destination = Paths.get(this.home, path).toString();

    // check if the hash of the file matches with the hash of the model in the S3 repository.
    // if a file with the same name exists, then remove the previous file
    if (FileUtils.exits(destination)) {
      try {
        // check if the hash MD5 matches
        String md5 = FileUtils.getMd5Hash(destination);
        if (md5 != null && md5.equals(this.s3.getObjectMd5(repository, path))) {
          // file already exists
          return;
        }
        FileUtils.delete(new File(destination));
      } catch (IOException e) {
        log.error(e.getMessage());
        return;
      }
    }
    // download the file from the repository to the destination
    this.s3.downloadFile(repository, path, destination, true, false);
  }

  /** Set the repository name, depending on the profile. */
  private void setRepositoryName() {
    this.repository =
        String.format("cloudos-models-%s", profileUtils.getMainProfile());
  }

  public String getRepository() {
    return this.repository;
  }
}
