package cloudos.machinelearning;

import cloudos.models.AbstractInstance;

import java.util.List;

import lombok.extern.log4j.Log4j2;

import org.apache.commons.math3.util.Pair;
import org.apache.spark.ml.feature.LabeledPoint;
import org.apache.spark.ml.linalg.Vectors;
import org.apache.spark.ml.regression.GeneralizedLinearRegression;
import org.apache.spark.ml.regression.GeneralizedLinearRegressionModel;
import org.apache.spark.ml.regression.LinearRegression;
import org.apache.spark.ml.regression.LinearRegressionModel;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

/** Created by philipperibeiro on 11/26/16. */
@Log4j2
public abstract class InstancePricingML<T extends AbstractInstance> {

  protected static final int DEFAULT_NUMBER_ITERACTIONS = 100;
  protected static final double DEFAULT_STEP_SIZE = 0.00000001;
  protected static final double DEFAULT_REGULAR_PARAM = 0.3;
  protected static final double DEFAULT_ELASTIC_NET_PARAM = 0.8;
  protected static final String GENERALIZED_FAMILY = "gaussian";
  protected static final String GENERALIZED_LINK = "identity";

  protected int numberIterations;
  protected double stepSize;
  protected LinearRegression linearRegression;
  protected GeneralizedLinearRegression glr;

  /** Default constructor. */
  public InstancePricingML() {
    this.numberIterations = DEFAULT_NUMBER_ITERACTIONS;
    this.stepSize = DEFAULT_STEP_SIZE;

    this.linearRegression =
        new LinearRegression()
            .setMaxIter(DEFAULT_NUMBER_ITERACTIONS)
            .setRegParam(DEFAULT_REGULAR_PARAM)
            .setElasticNetParam(DEFAULT_ELASTIC_NET_PARAM);

    this.glr =
        new GeneralizedLinearRegression()
            .setFamily(GENERALIZED_FAMILY)
            .setLink(GENERALIZED_LINK)
            .setMaxIter(DEFAULT_NUMBER_ITERACTIONS)
            .setRegParam(DEFAULT_REGULAR_PARAM);
  }

  /**
   * Set the number of interations.
   *
   * @param numberIterations the number of iterations
   */
  public void setNumberIterations(int numberIterations) {
    this.numberIterations = numberIterations;
  }

  public int getNumberIterations() {
    return numberIterations;
  }

  public void setStepSize(double stepSize) {
    this.stepSize = stepSize;
  }

  public double getStepSize() {
    return this.stepSize;
  }

  /**
   * Get a LabeledPoint from the features and the label.
   *
   * @param featureVector the feature vector
   * @param pricing the pricing label
   * @return a new LabeledPoint
   */
  public LabeledPoint getLabeledPoint(
      List<Double> featureVector, List<Pair<String, Double>> pricing) {
    if ((!featureVector.isEmpty()) && (!pricing.isEmpty())) {
      double[] vector = featureVector.stream().mapToDouble(d -> d).toArray();
      double price = pricing.get(0).getSecond();
      return new LabeledPoint(price, Vectors.dense(vector));
    }
    return null;
  }

  /**
   * Method to overriden by the child class.
   *
   * @param instance an instance child class
   * @return a Labeled point object
   */
  public abstract LabeledPoint getLabeledPoint(T instance);

  /**
   * Apply the training data to a LinearRegressionModel.
   *
   * @param training a dataset containing the training data
   * @return a LinearRegressionModel
   */
  public LinearRegressionModel linearModeling(Dataset<Row> training) {
    return this.linearRegression.fit(training);
  }

  /**
   * Apply the training data to a GeneralizedLinearRegressionModel.
   *
   * @param training a dataset containing the training data.
   * @return a GeneralizedLinearRegressionModel
   */
  public GeneralizedLinearRegressionModel generalizedModeling(Dataset<Row> training) {
    return this.glr.fit(training);
  }

  /**
   * Get a (end - start) number of samples from the mongo repository.
   *
   * @param start the start of the sample
   * @param end the end of the sample
   * @return a List of AmazonInstance samples
   */
  public abstract List<? extends AbstractInstance> getSample(int start, int end);
}
