package cloudos.machinelearning;

import aima.core.search.csp.CSP;
import aima.core.search.csp.Constraint;
import aima.core.search.csp.Domain;
import aima.core.search.csp.Variable;

import java.util.List;

/**
 * Implement the CloudCSP, which has as a goal to find the optimal cloud deployment.
 *
 * @author philipperibeiro
 */
public class CloudCSP extends CSP {

  /**
   * Implement the Cloud Constraint Search Problem.
   *
   * @param variables a list of Variables
   * @param domain the domain of the constraint
   * @param constraints a list of constraints
   */
  public CloudCSP(List<Variable> variables, Domain domain, List<Constraint> constraints) {
    /* add the variabless of the problem */
    for (Variable variable : variables) {
      this.addVariable(variable);
    }

    /* Assign the domain to the variables */
    for (Variable variable : variables) {
      this.setDomain(variable, domain);
    }

    /* Add the constraints to the problem */
    for (Constraint constraint : constraints) {
      this.addConstraint(constraint);
    }
  }
}
