package cloudos.machinelearning;

import cloudos.amazon.AmazonInstance;
import cloudos.amazon.AmazonInstanceRepository;
import cloudos.pricing.AmazonPricingManagement;
import cloudos.pricing.PricingValue;

import lombok.extern.log4j.Log4j2;

import aima.core.search.csp.Assignment;
import aima.core.search.csp.Constraint;
import aima.core.search.csp.Variable;

/**
 * Define the feature vector constraint to be fulfilled by our CloudCSP. For the cloud optimizer
 * algorithm.
 *
 * <p>Created by philipperibeiro on 12/9/16.
 */
@Log4j2
public class FeatureConstraint extends CloudConstraint implements Constraint {

  /* Cloud optimization problems are single variable */
  private Variable variable;
  private double price;

  /**
   * Define the constructor for the FeatureConstraint. It takes one variable.
   *
   * @param variable the variable to be assigned
   */
  public FeatureConstraint(Variable variable, double price) {
    super();
    this.variable = variable;
    this.scope.add(this.variable);
    this.price = price;
  }

  /**
   * Define the constructor for the FeatureConstraint. It takes one variable and the repository.
   *
   * @param variable the variable to be assigned
   * @param price the cost of the instance
   * @param instanceRepository an interface to access the Amazon Instance Repository
   * @param pricing the repository containing the pricing of Amazon instances
   */
  public FeatureConstraint(
      Variable variable,
      double price,
      AmazonInstanceRepository instanceRepository,
      AmazonPricingManagement pricing) {
    super(instanceRepository, pricing);
    this.variable = variable;
    this.price = price;
    this.scope.add(this.variable);
  }

  /**
   * Implement the condition to satisfy this constraint.
   *
   * @param assignment the current Assignment
   * @return true if the assignment for this variable is satisfied, false otherwise
   */
  @Override
  public boolean isSatisfiedWith(Assignment assignment) {
    // we can upcast and downcast inside this method
    Object value = assignment.getAssignment(this.variable);

    // Check the type to see if we are allowed to downcast to the Instance Variable type
    if (value == null) {
      return true;
    } else if (this.variable instanceof InstanceVariable) {
      // this is save to downcast to AmazonInstance Type ...
      // @TODO: Should I do it using reflection?
      if (((InstanceVariable) this.variable).isKeep()) {
        assignment.setAssignment(this.variable, ((InstanceVariable) this.variable).instanceType);
        return true;
      }
      if (((InstanceVariable) this.variable).getInstance() instanceof AmazonInstance) {
        AmazonInstance instance = (AmazonInstance) ((InstanceVariable) this.variable).getInstance();
        AmazonInstance other = instanceRepository.findOneByAttributesInstanceType((String) value);

        PricingValue priceInstance =
            pricingManagement.getPricingForInstance(instance.getInstance());
        PricingValue priceOther = pricingManagement.getPricingForInstance(other.getInstance());

        // Find only the instances that are cheaper than the current one
        log.error("TODO: finish this part!");

        double x = MLUtils.getScalar(instance.getFeatureVector());
        double y = MLUtils.getScalar(other.getFeatureVector());

      }
    }
    return false;
  }
}
