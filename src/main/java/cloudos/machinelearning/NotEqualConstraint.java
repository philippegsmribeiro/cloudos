package cloudos.machinelearning;

import aima.core.search.csp.Assignment;
import aima.core.search.csp.Constraint;
import aima.core.search.csp.Variable;

import java.util.ArrayList;
import java.util.List;

/**
 * Define the NotEqualConstraint which defines that two variables cannot be equal.
 *
 * @author philipperibeiro
 */
public class NotEqualConstraint implements Constraint {

  private Variable var1;
  private Variable var2;
  private List<Variable> scope;

  /**
   * Constructor takes 2 variables in order to compare if they are equal.
   *
   * @param var1 the first variable to be compared
   * @param var2 the second variable to be compared
   */
  public NotEqualConstraint(Variable var1, Variable var2) {
    this.var1 = var1;
    this.var2 = var2;
    this.scope = new ArrayList<>();
    this.scope.add(var1);
    this.scope.add(var2);
  }

  /**
   * Define the scope of the variables.
   *
   * @return a list of variables within this scope
   */
  @Override
  public List<Variable> getScope() {
    return this.scope;
  }

  /**
   * Defines the constraint satisfaction based on this assignment.
   *
   * @param assignment the current assignment
   * @return true if the constraint is satisfied, false otherwise
   */
  @Override
  public boolean isSatisfiedWith(Assignment assignment) {
    Object value = assignment.getAssignment(this.var1);
    return value == null || !value.equals(assignment.getAssignment(this.var2));
  }
}
