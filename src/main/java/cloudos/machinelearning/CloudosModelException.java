package cloudos.machinelearning;

/**
 * Created by philipperibeiro on 7/7/17.
 *
 * <p>Define a custom Exception for CloudosModels. This exception will be related to all the Machine
 * Learning / Deep Learning related activities in the machinelearning package.
 */
public class CloudosModelException extends Exception {

  /**
   * Constructor for the CloudosModelException.
   *
   * @param message the exception message
   */
  public CloudosModelException(String message) {
    super(message);
  }
}
