package cloudos.machinelearning;

import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.com.bytecode.opencsv.CSVReader;
import cloudos.instances.InstanceService;
import cloudos.machinelearning.autoscaler.AutoscalerVector;
import cloudos.machinelearning.autoscaler.DatapointVector;
import cloudos.models.CloudosDatapoint;
import cloudos.models.CloudosDatapointsRepository;
import cloudos.models.Instance;
import lombok.extern.log4j.Log4j2;
import scala.Tuple2;

/**
 * Created by philipperibeiro on 7/21/17.
 *
 * <p>
 * Extract the feature vector for the Autoscaler from the data points returned by the cloud
 * providers.
 */
@Service
@Log4j2
public class DatapointFeatureExtrator extends FeatureExtractor implements Serializable {

  @Autowired
  CloudosDatapointsRepository datapointsRepository;

  @Autowired
  InstanceService instanceService;

  /* Define the percentage normalizer */
  private static final double PERCENTAGE = 100.0;

  private static InstanceUtil instanceUtil = InstanceUtil.getInstance();

  /*
   * The instanceCache is used to store a association between the instance Id and the instance,
   * hence avoiding fetching from the Mongo collection every time.
   */
  protected static Map<String, Instance> instancesCache = new ConcurrentHashMap<>();

  /**
   * The Inner static class DatapointAggregator is responsible for aggregating data points that have
   * the same timestamp into a single list of data points.
   */
  public static class DatapointAggregator implements Serializable {

    private Set<CloudosDatapoint> datapoints;

    /**
     * Takes a CloudosDatapoint as constructor argument.
     *
     * @param datapoint a data point to be added to the aggregator
     */
    public DatapointAggregator(@NotNull CloudosDatapoint datapoint) {
      this.datapoints = new HashSet<>();
      this.datapoints.add(datapoint);
    }

    /**
     * Add the CloudosDatapoint to the list of data points.
     *
     * @param datapoint a CloudosDatapoint data point
     * @return a CloudosDatapoint with the data points added
     */
    public DatapointAggregator add(@NotNull CloudosDatapoint datapoint) {
      this.datapoints.add(datapoint);
      return this;
    }

    /**
     * Merge the data points of two DatapointAggregator into a single one.
     *
     * @param datapointAggregator another DatapointAggregator to be merged
     * @return a CloudosDatapoint with the data points added
     */
    public DatapointAggregator merge(@NotNull DatapointAggregator datapointAggregator) {
      this.datapoints.addAll(datapointAggregator.getDatapoints());
      return this;
    }

    /**
     * Return the list of all the data points for this aggregator.
     *
     * @return a set of data points
     */
    public Set<CloudosDatapoint> getDatapoints() {
      return this.datapoints;
    }

    /**
     * Return the string representation of this object.
     *
     * @return the string representation of this object
     */
    @Override
    public String toString() {
      return StringUtils.join(this.datapoints, ",");
    }
  }

  /** Create a new DatapointAggregator. */
  public static Function<CloudosDatapoint, DatapointAggregator> createAgg =
      (Function<CloudosDatapoint, DatapointAggregator>) DatapointAggregator::new;

  /** Add a new CloudosDatapoint to the aggregator. */
  public static Function2<DatapointAggregator, CloudosDatapoint, DatapointAggregator> addAgg =
      (Function2<DatapointAggregator, CloudosDatapoint, DatapointAggregator>) DatapointAggregator::add;

  /** Merge two DatapointAggregator objects into a single one. */
  public static Function2<DatapointAggregator, DatapointAggregator, DatapointAggregator> combine =
      (Function2<DatapointAggregator, DatapointAggregator, DatapointAggregator>) DatapointAggregator::merge;

  /** Default constructor the DashboardJob class. */
  public DatapointFeatureExtrator() {
    super();
  }

  /**
   * Open a file and read a string of data points to the given destination.
   *
   * @param path the source path of the file
   * @param destination the destination of the streamed file
   */
  @Override
  public void readStream(String path, String destination) {}

  /**
   * Merge the vectors in a csv file and write it the output file.
   *
   * @param csvInput an unmerged csv file
   * @param output the destination of the merged csv file
   * @throws IOException if it files to do any I/O operation
   */
  @Override
  public void merge(String csvInput, String output) throws IOException {
    try (CSVReader reader =
        new CSVReader(new FileReader(csvInput), SEPARATOR, QUOTE_CHAR, START_LINE)) {

      List<DatapointVector> datapointVectors = new LinkedList<>();
      String[] line;
      while ((line = reader.readNext()) != null) {
        List<Double> vector = new ArrayList<>();
        // add the try catch block in case of failure reading the file.
        try {
          /**
           * Since we ignore the timestamp and the host name, we can exclude them from the metric
           * beat vector.
           */
          for (int i = VECTOR_INDEX; i < line.length; i++) {
            vector.add(Double.parseDouble(line[i]));
          }
          // create a new Datapoint vector
          DatapointVector datapointVector = new DatapointVector();
          datapointVector.setTimestamp(Long.parseLong(line[0]));
          datapointVector.setName(line[1]);
          datapointVector.setVector(vector);

          datapointVectors.add(datapointVector);
        } catch (Exception ex) {
          log.error(ex.getMessage());
        }
      }

      //////////////////////////////////////////////////////
      // Perform a local MapReduce job, instead of relying
      // on Spark for the job.
      //////////////////////////////////////////////////////

      // 1. Map
      Map<Tuple2<Long, String>, List<AutoscalerVector>> map =
          new TreeMap<>(new FeatureComparator());

      datapointVectors.forEach(datapointVector -> {
        Tuple2<Long, String> key =
            new Tuple2<>(datapointVector.getTimestamp(), datapointVector.getName());
        if (map.containsKey(key)) {
          map.get(key).add(datapointVector);
        } else {
          List<AutoscalerVector> list = new LinkedList<>();
          list.add(datapointVector);
          map.put(key, list);
        }
      });

      Map<Tuple2<Long, String>, AutoscalerVector> reduced = new TreeMap<>(new FeatureComparator());

      // 2. Reduce
      map.forEach((k, v) -> {
        AutoscalerVector merged = null;
        try {
          merged = this.merge(v);
        } catch (ClassNotFoundException e) {
          log.error(e.getMessage());
        }
        if (merged != null) {
          reduced.put(k, merged);
        }
      });
      // save the file to the output directory
      this.save(reduced, output);
      // close the file
    }
  }

  /**
   * Extract the feature vector from the given source file to the destination file.
   *
   * @param source the source file to have its vectors extracted
   * @param destination the destination file to where the vectors will be written
   * @throws IOException if it files to do any I/O operation
   */
  @Override
  public void extract(String source, String destination) throws IOException {}

  /**
   * Merge a list of MetricbeatVectors into a single MetricbeatVector so we don't have sparse
   * vectors.
   *
   * @param vectorList a list of MetricbeatVector to be merged
   * @return a merged MetricbeatVectors
   */
  public AutoscalerVector merge(@NotNull List<AutoscalerVector> vectorList)
      throws ClassNotFoundException {
    if (CollectionUtils.isEmpty(vectorList)) {
      return null;
    }

    AutoscalerVector merged = vectorList.get(0);
    for (int i = 1; i < vectorList.size(); i++) {
      merged = this.merge(merged, vectorList.get(i), DatapointVector.class);
    }

    return merged;
  }

  /**
   * Given a list of data points, aggregate, merge and sort the data points into a merged vector
   * that can be analyzed by the Autoscaler.
   *
   * @param datapoints a list of data points
   * @return a map between the instance id and the merged list of data points
   */
  public Map<Tuple2<Long, String>, DatapointVector> extract(
      @NotNull List<CloudosDatapoint> datapoints) {
    Map<Tuple2<Long, String>, DatapointVector> datapointsMap = new LinkedHashMap<>();

    // build an RDD from the data points
    JavaRDD<CloudosDatapoint> rdd = context.parallelize(datapoints);
    // the key is the tuple <timestamp, instance-id>
    JavaPairRDD<Tuple2<Long, String>, CloudosDatapoint> result = rdd
        .mapToPair((PairFunction<CloudosDatapoint, Tuple2<Long, String>, CloudosDatapoint>) x -> {
          Tuple2<Long, String> key = new Tuple2<>(x.getTimestamp().getTime(), x.getCloudId());
          return new Tuple2<>(key, x);
        }).sortByKey(new FeatureComparator()); // sort by the timestamp ...

    // aggregate by the data point aggregator.
    JavaPairRDD<Tuple2<Long, String>, DatapointAggregator> aggregated =
        result.combineByKey(createAgg, addAgg, combine);

    // now merge the DatapointAggregator's AutoscalerVector into a single AutoscalerVector.
    List<Tuple2<Tuple2<Long, String>, DatapointAggregator>> list = aggregated.collect();
    list.forEach(tuple -> {
      try {
        // add a new entry
        List<AutoscalerVector> vectors = new ArrayList<>();
        // for each data point, extract its vector
        for (CloudosDatapoint datapoint : tuple._2().getDatapoints()) {
          vectors.add(this.normalize(datapoint));
        }

        DatapointVector datapointVector = (DatapointVector) this.merge(vectors);

        // merge the vector with the existing merged vector
        if (datapointsMap.containsKey(tuple._1())) {
          DatapointVector current = datapointsMap.get(tuple._1());
          List<AutoscalerVector> vectorList = Arrays.asList(datapointVector, current);
          // merge once more
          datapointVector = (DatapointVector) this.merge(vectorList);
        }

        datapointsMap.put(tuple._1(), datapointVector);

      } catch (ClassNotFoundException e) {
        // nothing to do...
        log.error(e.getMessage());
      }
    });
    return datapointsMap;
  }

  /**
   * Read the list of data points stored under datapointList, extract the vector in each datapoint
   * and write the result to the destination file, which will be later merged.
   *
   * @param datapointList a list of datapoints
   * @param destination the destination where the result file will be written to.
   */
  public void extract(@NotNull List<CloudosDatapoint> datapointList, @NotNull String destination) {

    if (CollectionUtils.isEmpty(datapointList)) {
      log.warn("Datapoints list is empty.");
      return;
    }

    log.info("Writing datapoints to file {}", destination);

    try (FileOutputStream outputStream = new FileOutputStream(destination)) {
      for (CloudosDatapoint datapoint : datapointList) {
        DatapointVector datapointVector = this.normalize(datapoint);
        if (datapointVector != null) {
          outputStream.write(datapointVector.toString().getBytes());
        }
      }
    } catch (IOException e) {
      log.error(e.getMessage());
    }
  }

  /**
   * Normalize the vector before attempting to use it anywhere.
   *
   * @param datapoint a CloudosDatapoint to be normalized
   * @return a normalized DatapointVector
   */
  public DatapointVector normalize(CloudosDatapoint datapoint) {

    // declare the instance
    Instance instance;

    DatapointVector datapointVector = new DatapointVector(datapoint);
    log.info(datapointVector);
    // check if the instance was found in the cache
    if (!instancesCache.containsKey(datapoint.getCloudId())) {
      // obtain the instance the data point came from...
      instance = instanceService.findByProviderAndProviderId(datapoint.getProvider(),
          datapoint.getCloudId());
      if (instance != null) {
        // add it to the cache
        instancesCache.put(datapoint.getCloudId(), instance);
      }
    } else {
      // read from the cache
      instance = instancesCache.get(datapoint.getCloudId());
    }

    if (instance == null) {
      return null;
    }

    String instanceType = instance.getMachineType();

    // get the current instance type
    InstanceUtil.InstanceEntry instanceEntry = instanceUtil.getInstanceEntry(instanceType);

    // nothing to do ... data point is invalid
    if (instanceEntry == null) {
      return datapointVector;
    }

    for (int i = 0; i < datapointVector.getVector().size(); i++) {
      double score;
      if (i <= 2) {
        score = datapointVector.getVector().get(i) / PERCENTAGE;
        datapointVector.getVector().set(i, score);
      } else if (i < 5) {
        score = datapointVector.getVector().get(i) / instanceEntry.getExpectedIops();
        datapointVector.getVector().set(i, score);
      } else {
        score = datapointVector.getVector().get(i) / instanceEntry.getExpectedBandwidth();
        datapointVector.getVector().set(i, score);
      }
    }

    return datapointVector;
  }
}
