package cloudos.machinelearning;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.util.Pair;
import org.apache.spark.ml.feature.LabeledPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

import cloudos.amazon.AmazonInstance;
import cloudos.amazon.AmazonInstanceRepository;
import cloudos.pricing.AmazonPricingManagement;

/** Created by philipperibeiro on 11/23/16. */
public class AmazonPricingML extends InstancePricingML<AmazonInstance> {

  private static final String ONDEMAND_INSTANCE = "OnDemand";
  private static final String RESERVED_INSTANCE = "Reserved";
  private static final String PRODUCT_FAMILY = "Compute Instance";

  @Autowired
  private AmazonInstanceRepository instanceRepository;

  @Autowired
  private AmazonPricingManagement pricingManagement;

  /** Default constructor. */
  public AmazonPricingML() {
    super();
  }

  /**
   * Constructor version 3.
   *
   * @param instanceRepository an AmazonInstanceRepository to fetch from the `aws_instances`
   *        collection
   * @param pricingManagement an AmazonInstancePricingRepository to fetch from the
   *        `aws_instances_pricing` collection
   */
  public AmazonPricingML(AmazonInstanceRepository instanceRepository,
      AmazonPricingManagement pricingManagement) {
    /* initialize the Spark utility */
    this.instanceRepository = instanceRepository;
    this.pricingManagement = pricingManagement;
  }

  /**
   * Constructor version 4.
   *
   * @param instanceRepository an AmazonInstanceRepository to fetch from the `aws_instances`
   *        collection
   * @param pricingManagement an AmazonInstancePricingRepository to fetch from the
   *        `aws_instances_pricing` collection
   * @param numberIterations the number of iterations for the Linear Regression
   * @param stepSize the number of steps for the Linear Regression
   */
  public AmazonPricingML(AmazonInstanceRepository instanceRepository,
      AmazonPricingManagement pricingManagement, int numberIterations, double stepSize) {
    /* initialize the Spark utility */
    this.numberIterations = numberIterations;
    this.stepSize = stepSize;
    this.instanceRepository = instanceRepository;
    this.pricingManagement = pricingManagement;
  }

  /**
   * Apply the feature extractor and obtains a Labeled point containg the label and feature vector.
   *
   * @param instance an Amazon Instance which we will convert to a Labeled Point after extracting
   *        its features
   * @return a LabeledPoint representing the label and the feature vector
   */
  @Override
  public LabeledPoint getLabeledPoint(AmazonInstance instance) {
    /* Loop over all the instances, since we want to find the weights */
    List<Double> featureVector = instance.getFeatureVector();
    List<Pair<String, Double>> pricing =
        this.getInstancePrice(instance.getSku(), ONDEMAND_INSTANCE);
    return this.getLabeledPoint(featureVector, pricing);
  }

  /**
   * Get the Instance Pricing, given its sku and term.
   *
   * @param sku the Amazon AWS instance's sku
   * @param term the contract term. It could be either "OnDemand", "SpotInstance" or "Reserved"
   * @return a List containing the pairs of (name, price)
   */
  @SuppressWarnings("Duplicates")
  public List<Pair<String, Double>> getInstancePrice(final String sku, final String term) {
    List<Pair<String, Double>> prices = new ArrayList<>();
    if ((sku == null || sku.isEmpty()) || (term == null || term.isEmpty())) {
      return prices;
    }
    return prices;
  }

  /**
   * Get a (end - start) number of samples from the mongo repository.
   *
   * @param start the start of the sample
   * @param end the end of the sample
   * @return a List of AmazonInstance samples
   */
  @Override
  public List<AmazonInstance> getSample(int start, int end) {
    return this.instanceRepository.findTop100ByProductFamily(PRODUCT_FAMILY,
        new PageRequest(start, end));
  }
}
