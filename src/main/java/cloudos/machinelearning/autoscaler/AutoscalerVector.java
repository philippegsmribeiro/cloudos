package cloudos.machinelearning.autoscaler;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.nd4j.linalg.api.buffer.DataBuffer;
import org.nd4j.linalg.api.buffer.util.DataTypeUtil;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import com.google.common.collect.Lists;
import com.google.common.primitives.Doubles;

import lombok.AllArgsConstructor;
import lombok.Data;

/** Created by philipperibeiro on 7/21/17. */
@Data
@AllArgsConstructor
public class AutoscalerVector implements Serializable, Comparable<AutoscalerVector> {

  /* Use comma as the default delimiter */
  public static final String DELIMITER = ",";

  public static Comparator<AutoscalerVector> AutoscaleVectorComparator = Comparator.naturalOrder();

  /* Define a vector that will hold the features */
  protected List<Double> vector;

  /* define the timestamp as a long timestamp */
  protected Long timestamp;

  /* Get the name of the host */
  protected String name;

  protected static final double DOWNSCALE_BOUND = 0.20;

  protected static final double MIDDLE_BOUND = 0.50;

  /**
   * Default constructor for the AutoscalerVector class Initialize all the attributes with default
   * values.
   */
  public AutoscalerVector() {
    this.vector = new ArrayList<>();
    this.name = "";
    this.timestamp = 0L;
  }

  /**
   * Constructor for the AutoscalerVector class and pass the attributes as arguments.
   *
   * @param timestamp the vector timestamp
   * @param name the name of the instance the vector was generated
   * @param vector the vector holding the vector as doubles.
   */
  public AutoscalerVector(Long timestamp, String name, List<Double> vector) {
    this.setTimestamp(timestamp);
    this.setName(name);
    this.setVector(vector);
  }

  /**
   * Obtain the label for the autoscaler classifier based on the score given. the score could be a
   * function based on the input vector or just on the CPU/memory usage.
   *
   * @param score the score to be converted to a label.
   * @return the label defining whether we need to scale up or down.
   */
  public AutoscaleLabels label(double score) {
    if (score >= 0 && score <= DOWNSCALE_BOUND) {
      return AutoscaleLabels.DOWNSCALE;
    } else if (score > DOWNSCALE_BOUND && score <= MIDDLE_BOUND) {
      return AutoscaleLabels.MAINTAIN;
    } else {
      return AutoscaleLabels.UPSCALE;
    }
  }

  /**
   * Get a matrix of Autoscale vectors and convert them to a matrix (batch) of data points to be
   * converted into an INDArray.
   *
   * @param vectors a list of Autoscaler vectors
   * @return an INDArray batch built from the matrix obtained from the list of Autoscaler vectors.
   */
  public static INDArray getMatrix(@NotNull List<? extends AutoscalerVector> vectors) {
    double[][] matrixDouble = new double[vectors.size()][];
    List<double[]> batch = new ArrayList<>();
    for (AutoscalerVector vector : vectors) {
      double[] row = vector.getVector().stream().mapToDouble(d -> d).toArray();
      batch.add(row);
    }
    // cast the list of doubles to a matrix of doubles
    matrixDouble = batch.toArray(matrixDouble);

    // set the type to double to avoid loss of precision
    DataTypeUtil.setDTypeForContext(DataBuffer.Type.DOUBLE);
    return Nd4j.create(matrixDouble);
  }

  /**
   * Convert a AutoscalerVector to a INDArray.
   *
   * @param vector the AutoscalerVector to be converted
   * @return an INDArray row built from the AutoscalerVector.
   */
  public static INDArray getRow(@NotNull AutoscalerVector vector) {
    //identity function, Java unboxes automatically to get the double value
    double[] data = vector.getVector().stream().mapToDouble(d -> d).toArray();
    return Nd4j.create(data);
  }

  /**
   * Return a INDArray vector from the current vector.
   *
   * @return an INDArray for the current vector
   */
  public INDArray getINDArrayVector() {
    double[] row = Doubles.toArray(this.getVector());
    return Nd4j.create(row);
  }

  /**
   * Convert the vector into a string line.
   *
   * @return the vector converted to a string
   */
  public String vectorToString() {
    List<String> strings = Lists.transform(this.vector, Object::toString);
    return strings.stream().collect(Collectors.joining(DELIMITER));
  }


  /**
   * Implement the compareTo method in order to make it easy to compare between AutoscaleVector
   * objects.
   *
   * @param another another AutoscaleVector to be compared
   * @return -1 if this is smaller, 0 if they are equal or 1 if another is smaller
   */
  @Override
  public int compareTo(AutoscalerVector another) {
    int value = this.getTimestamp().compareTo(another.getTimestamp());
    if (value == 0) {
      return this.getName().compareTo(another.getName());
    }
    return value;
  }
}
