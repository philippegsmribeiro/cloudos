package cloudos.machinelearning.autoscaler;

import cloudos.models.CloudosDatapoint;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;
import org.nd4j.linalg.api.ndarray.INDArray;

public interface AutoscalerManager {

  /**
   * Analyze the data points by parsing, normalizing and producing the INDArray to the
   * autoscaler.
   *
   * @param vectorMap a map between the region and its INDArray matrix
   */
  void analyze(Map<String, List<Pair<String, INDArray>>> vectorMap);

  /**
   * Filter the list of data points into a map between the region and its respective data points.
   * This separation is necessarily since the autoscaling is done by region.
   *
   * @param datapoints a list of data points to be filtered
   * @return a map between the cloud region and a map between the instance id and its data points
   */
  default Map<String, Map<String, List<CloudosDatapoint>>> filter(
      List<CloudosDatapoint> datapoints) {
    Map<String, Map<String, List<CloudosDatapoint>>> datapointsMap = new HashMap<>();
    // filter the datapoints by region
    datapoints.forEach(datapoint -> {
      // get the region
      String region = datapoint.getRegion();
      // if does not contain the entry, create a new list
      if (!datapointsMap.containsKey(region)) {
        // build as LinkedList since the number of data points may be huge...
        datapointsMap.put(region, new HashMap<>());
      }
      Map<String, List<CloudosDatapoint>> map = datapointsMap.get(region);
      if (!map.containsKey(datapoint.getCloudId())) {
        map.put(datapoint.getCloudId(), new LinkedList<>());
      }
      List<CloudosDatapoint> datapointList = map.get(datapoint.getCloudId());
      datapointList.add(datapoint);
      map.put(datapoint.getCloudId(), datapointList);
    });
    return datapointsMap;
  }

}
