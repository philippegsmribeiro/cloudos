package cloudos.machinelearning.autoscaler;

import cloudos.insights.InsightsService;
import cloudos.instances.CloudManagerService;
import cloudos.instances.InstanceService;
import cloudos.machinelearning.ModelsRepository;
import cloudos.models.cloudgroup.CloudGroupService;
import cloudos.policies.CloudPolicyService;
import cloudos.provider.ProvidersService;
import cloudos.queue.QueueManagerService;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Pair;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AmazonAutoscalerManager implements AutoscalerManager {

  /* Build a map between the Autoscaler and the region in the provider */
  private static Map<String, Autoscaler> autoscalerMap = new ConcurrentHashMap<>();

  @Value("${cloudos.ml.autoscaler}")
  private String autoscalerPath;

  /* The Machine Type repository will used to fetch information about the instance */
  @Autowired
  ProvidersService providersService;

  @Autowired
  ModelsRepository modelsRepository;

  @Autowired
  InstanceService instanceService;

  @Autowired
  CloudManagerService cloudManagerService;

  @Autowired
  QueueManagerService queueManagerService;

  @Autowired
  CloudGroupService cloudGroupService;
  
  @Autowired
  private InsightsService insightsService;
  
  @Autowired
  CloudPolicyService cloudPolicyService;

  /**
   * Analyze data points related to Amazon instances.
   *
   * @param vectorMap a map between the region and its INDArray matrix
   */
  @Override
  @SuppressWarnings("Duplicates")
  public void analyze(Map<String, List<Pair<String, INDArray>>> vectorMap) {
    log.info("############ Analyzing data points for Amazon AWS ###############");
    vectorMap.forEach((region, vectorList) -> {
      Autoscaler autoscaler;
      if (!autoscalerMap.containsKey(region)) {
        // create a new Autoscaler for that particular region
        log.info("Creating a new autoscaler for the region {}", region);
        autoscaler = new Autoscaler(this.providersService,
            this.modelsRepository,
            this.instanceService,
            this.cloudManagerService,
            this.autoscalerPath,
            this.queueManagerService,
            this.cloudGroupService,
            this.insightsService,
            this.cloudPolicyService);
        autoscalerMap.put(region, autoscaler);
      } else {
        // fetch the autoscaler for that region
        autoscaler = autoscalerMap.get(region);
      }
      // just predict :-)
      vectorList.forEach(vector -> {
        try {
          autoscaler.predict(region, vector);
        } catch (PredictionException e) {
          log.error(e);
        }
      });
    });
    log.info("#####################################");
  }
}
