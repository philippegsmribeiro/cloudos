package cloudos.machinelearning.autoscaler;

import cloudos.data.InstanceDataMetric;
import cloudos.machinelearning.MLUtils;
import cloudos.models.CloudosDatapoint;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

/** Created by philipperibeiro on 7/21/17. */
public class DatapointVector extends AutoscalerVector {

  /** Default constructor. */
  public DatapointVector() {
    super();
  }

  /**
   * Constructor for the DatapointVector class. Pass the attributes as arguments.
   *
   * @param timestamp the vector timestamp
   * @param name the name of the instance the vector was generated
   * @param vector the vector holding the vector as doubles.
   */
  public DatapointVector(Long timestamp, String name, List<Double> vector) {
    super(timestamp, name, vector);
  }

  /**
   * Constructor for the DatapointVector class. Extract the vector from the CloudosDatapoint.
   *
   * @param datapoint a CloudosDatapoint object
   */
  public DatapointVector(@NotNull CloudosDatapoint datapoint) {
    this.extractVector(datapoint);
  }

  /**
   * Extract the vector from the CloudosDatapoint.
   *
   * @param datapoint the datapoint obtained from the cloud providers.
   */
  public void extractVector(@NotNull CloudosDatapoint datapoint) {

    this.timestamp = datapoint.getTimestamp().getTime();
    this.name = datapoint.getCloudId();
    // obtain the vector
    Map<InstanceDataMetric, Double> map = MLUtils.getVectorMap();
    InstanceDataMetric metric = datapoint.getMetric();

    switch (metric) {
      case CPU_UTILIZATION:
        map.put(InstanceDataMetric.CPU_UTILIZATION, datapoint.getValue());
        break;
      case MEMORY_USAGE:
        map.put(InstanceDataMetric.MEMORY_USAGE, datapoint.getValue());
        break;
      case DISK_USAGE:
        map.put(InstanceDataMetric.DISK_USAGE, datapoint.getValue());
        break;
      case DISK_READ_BYTES:
        map.put(InstanceDataMetric.DISK_READ_BYTES, datapoint.getValue());
        break;
      case DISK_WRITE_BYTES:
        map.put(InstanceDataMetric.DISK_WRITE_BYTES, datapoint.getValue());
        break;
      case NETWORK_RECEIVED_BYTES:
        map.put(InstanceDataMetric.NETWORK_RECEIVED_BYTES, datapoint.getValue());
        break;
      case NETWORK_SENT_BYTES:
        map.put(InstanceDataMetric.NETWORK_SENT_BYTES, datapoint.getValue());
        break;
    }

    // set the vector after normalizing it.
    this.setVector(new ArrayList<>(map.values()));
  }
}
