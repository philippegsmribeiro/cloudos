package cloudos.machinelearning.autoscaler;

import cloudos.data.InstanceDataMetric;
import cloudos.machinelearning.MLUtils;
import cloudos.models.Metricbeat;
import cloudos.models.elasticsearch.CPU;
import cloudos.models.elasticsearch.Diskio;
import cloudos.models.elasticsearch.Filesystem;
import cloudos.models.elasticsearch.Memory;
import cloudos.models.elasticsearch.Network;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

/**
 * Created by philipperibeiro on 7/11/17.
 *
 * <p>Define the feature vector from the Metricbeat entry.
 */
public class MetricbeatVector extends AutoscalerVector {

  private static final String SYSTEM = "system";

  private static final String PROCESS = "process";

  private static final String CORE = "core";

  private static final String SOCKET = "socket";

  private static final String FSSTAT = "fsstat";

  private static final String CPU = "cpu";

  private static final String DISK_IO = "diskio";

  private static final String MEMORY = "memory";

  private static final String NETWORK = "network";

  private static final String FILESYSTEM = "filesystem";

  /** Default constructor. */
  public MetricbeatVector() {
    super();
  }

  /**
   * Constructor for the MetricbeatVector class. Pass the attributes as arguments.
   *
   * @param timestamp the timestamp of the metricbeat hit
   * @param name the name of the instance
   * @param vector the feature vector
   */
  public MetricbeatVector(Long timestamp, String name, List<Double> vector) {
    super(timestamp, name, vector);
  }

  /**
   * Given a Metricbeat, extract the feature vector information from the object.
   *
   * @param metricbeat a Metricbeat index
   */
  public MetricbeatVector(@NotNull Metricbeat metricbeat) {
    this.extractVector(metricbeat);
  }

  /**
   * Given a Metricbeat, extract the feature vector from the hit.
   *
   * @param metricbeat a Metricbeat index
   */
  public void extractVector(@NotNull Metricbeat metricbeat) {
    this.timestamp = metricbeat.timestamp.getTime();
    this.name = metricbeat.beat.name;

    Map<InstanceDataMetric, Double> map = MLUtils.getVectorMap();
    if (metricbeat.metricset != null && metricbeat.metricset.module.equals(SYSTEM)) {
      double total;
      switch (metricbeat.metricset.name) {
        case PROCESS:
        case CORE:
        case SOCKET:
        case FSSTAT:
          break;
        case CPU:
          CPU cpu = metricbeat.system.cpu;
          map.put(InstanceDataMetric.CPU_UTILIZATION, cpu.user.pct);
          break;
        case DISK_IO:
          Diskio diskio = metricbeat.system.diskio;
          total = diskio.read.bytes + diskio.write.bytes;
          if (total != 0) {
            map.put(InstanceDataMetric.DISK_READ_BYTES, diskio.read.bytes / total);
            map.put(InstanceDataMetric.DISK_WRITE_BYTES, diskio.write.bytes / total);
          }
          break;
        case MEMORY:
          Memory memory = metricbeat.system.memory;
          map.put(InstanceDataMetric.MEMORY_USAGE, memory.actual.used.pct);
          break;
        case NETWORK:
          Network network = metricbeat.system.network;
          total = network.in.bytes + network.out.bytes;
          if (total != 0) {
            map.put(InstanceDataMetric.NETWORK_RECEIVED_BYTES, network.in.bytes / total);
            map.put(InstanceDataMetric.NETWORK_SENT_BYTES, network.out.bytes / total);
          }
          break;
        case FILESYSTEM:
          Filesystem filesystem = metricbeat.system.filesystem;
          map.put(InstanceDataMetric.DISK_USAGE, filesystem.used.pct);
          break;
      }
    }

    this.setVector(new ArrayList<>(map.values()));
  }
}
