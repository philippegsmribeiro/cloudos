package cloudos.machinelearning.autoscaler;

/**
 * Define a list of resources categories for the Autoscaler.
 */
public enum ResourceCategory {
  CPU_USAGE,
  MEMORY_USAGE,
  DISK_USAGE,
  DISK_READ,
  DISK_WRITE,
  NETWORK_RECEIVED,
  NETWORK_SENT,
  ALL,
}
