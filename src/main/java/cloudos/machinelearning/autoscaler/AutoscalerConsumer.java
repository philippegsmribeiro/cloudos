package cloudos.machinelearning.autoscaler;

import cloudos.Providers;
import cloudos.exceptions.NotImplementedException;
import cloudos.machinelearning.DatapointFeatureExtrator;
import cloudos.models.CloudosDatapoint;
import cloudos.provider.ProvidersService;
import cloudos.queue.QueueListener;
import cloudos.queue.message.DatapointMessage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import scala.Tuple2;

/**
 * The AutoscalerConsumer is responsible for consuming the data points related to resources usage in
 * the cloud providers. The AutoscalerConsumer multiplexes between the providers, process the data
 * points by separating them by region before sending the map between regions and data points to the
 * respective provider autoscaler manager.
 */
@Service
@Log4j2
public class AutoscalerConsumer {

  @Autowired
  QueueListener queueListener;

  /* The Machine Type repository will used to fetch information about the instance */
  @Autowired
  ProvidersService providersService;

  @Autowired
  AmazonAutoscalerManager amazonAutoscalerManager;

  @Autowired
  GoogleAutoscalerManager googleAutoscalerManager;

  @Autowired
  DatapointFeatureExtrator datapointFeatureExtrator;


  /**
   * Set up the listener that will consume the data point messages sent to the queue.
   */
  @PostConstruct
  public void setUpListener() {
    // add listener to the queue
    queueListener.addConsumer((DatapointMessage.DatapointMessageConsumer) datapointMessage -> {
      log.info("********* Consuming message {}", datapointMessage);
      List<CloudosDatapoint> datapoints = datapointMessage.getDatapoints();
      if (CollectionUtils.isNotEmpty(datapoints)) {
        try {
          // obtain the correct autoscaler manager
          AutoscalerManager manager = getManagement(datapoints.get(0).getProvider());

          // filter the data points by region
          Map<String, Map<String, List<CloudosDatapoint>>> datapointsMap =
              manager.filter(datapoints);

          // obtain the map between the region and the INDArray
          Map<String, List<Pair<String, INDArray>>> vectorMap = this.vectorize(datapointsMap);

          // send the vector map for analysis
          manager.analyze(vectorMap);
        } catch (NotImplementedException e) {
          log.error(e.getMessage());
        }
      }
    });
  }

  /**
   * Vectorize the data points into a INDArray so it can be fed into the autoscaler of the
   * respective region for the respective cloud provider.
   *
   * @param regionsDatapointsMap the map between the data points and the region
   * @return a map between the region for the cloud provider and the INDArray
   */
  private Map<String, List<Pair<String, INDArray>>> vectorize(
      Map<String, Map<String, List<CloudosDatapoint>>> regionsDatapointsMap) {

    Map<String, List<Pair<String, INDArray>>> vectorMap = new HashMap<>();
    // for debugging purpose
    regionsDatapointsMap.forEach((region, datapointsMap) -> {

      datapointsMap.forEach((providerId, datapoints) -> {

        log.info("Processing datapoints {} for region {}: ", datapoints, region);

        // map between the <timestamp, instance-id> and the merged list of vectors
        Map<Tuple2<Long, String>, DatapointVector> map =
            this.datapointFeatureExtrator.extract(datapoints);

        // list is already sorted and normalized
        List<AutoscalerVector> vectors = new LinkedList<>(map.values());
        // merge vectors that have the same cloud-id
        try {

          DatapointVector vector = (DatapointVector) this.datapointFeatureExtrator.merge(vectors);
          log.info("Datapoint vector: {}", vector);
          // forward the INDArray to the right provider
          INDArray batch = AutoscalerVector.getRow(vector);
          Pair<String, INDArray> pair = new ImmutablePair<>(providerId, batch);

          List<Pair<String, INDArray>> list;
          if (vectorMap.containsKey(region)) {
            list = vectorMap.get(region);
          } else {
            list = new ArrayList<>();
          }
          list.add(pair);
          vectorMap.put(region, list);

        } catch (ClassNotFoundException e) {
          log.error(e.getMessage());
        }
      });
    });

    return vectorMap;
  }

  /**
   * Get the autoscaler manager based on the provider.
   *
   * @param providers the name of the provider
   * @return a provider manager
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  @SuppressWarnings("Duplicates")
  private AutoscalerManager getManagement(Providers providers) throws NotImplementedException {
    switch (providers) {
      case AMAZON_AWS:
        return amazonAutoscalerManager;
      case GOOGLE_COMPUTE_ENGINE:
        return googleAutoscalerManager;
      default:
        break;
    }
    throw new NotImplementedException();
  }
}
