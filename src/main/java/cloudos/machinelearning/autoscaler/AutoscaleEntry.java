package cloudos.machinelearning.autoscaler;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import com.opencsv.bean.CsvBindByPosition;

import cloudos.utils.ReflectionToJson;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AutoscaleEntry {

  @CsvBindByPosition(position = 0)
  private Double cpuUsage;

  @CsvBindByPosition(position = 1)
  private Double memoryUsage;

  @CsvBindByPosition(position = 2)
  private Double diskUsage;

  @CsvBindByPosition(position = 3)
  private Double diskRead;

  @CsvBindByPosition(position = 4)
  private Double diskWrite;

  @CsvBindByPosition(position = 5)
  private Double networkReceived;

  @CsvBindByPosition(position = 6)
  private Double networkSent;

  private static final Map<Integer, ResourceCategory> featureMapIndex = new HashMap<Integer,
      ResourceCategory>() {
    {

      put(0, ResourceCategory.CPU_USAGE);
      put(1, ResourceCategory.MEMORY_USAGE);
      put(2, ResourceCategory.DISK_USAGE);
      put(3, ResourceCategory.DISK_READ);
      put(4, ResourceCategory.DISK_WRITE);
      put(5, ResourceCategory.NETWORK_RECEIVED);
      put(6, ResourceCategory.NETWORK_SENT);
    }
  };

  @Override
  public String toString() {
    return ReflectionToJson.toString(this);
  }

  /**
   * Get the value of the property based on the an index map between the value and the
   * category.
   *
   * @param index the index value of the property field
   * @return the double value of the property field
   */
  public Double getValue(int index) {
    ResourceCategory category = featureMapIndex.get(index);
    if (category == null) {
      throw new NoSuchElementException();
    }
    switch (category) {
      case CPU_USAGE:
        return this.cpuUsage;
      case MEMORY_USAGE:
        return this.memoryUsage;
      case DISK_USAGE:
        return this.diskUsage;
      case DISK_READ:
        return this.diskRead;
      case DISK_WRITE:
        return this.diskWrite;
      case NETWORK_RECEIVED:
        return this.networkReceived;
      case NETWORK_SENT:
        return this.networkSent;
      default:
        throw new NoSuchElementException();
    }
  }
}
