package cloudos.machinelearning.autoscaler;

/**
 * Prediction exception.
 * 
 */
public class PredictionException extends Exception {

  private static final long serialVersionUID = -2137505039933052061L;

  private PredictionExceptionType type;
  
  public PredictionException(PredictionExceptionType type, String message) {
    super(message);
    this.type = type;
  }
  
  public PredictionException(PredictionExceptionType type, String message, Exception exception) {
    super(message, exception);
    this.type = type;
  }
  
  public PredictionExceptionType getType() {
    return type;
  }
  
  public enum PredictionExceptionType {
    /** Data not complete to execute the prediction. */
    INCOMPLETE_DATA,
    /** Prediction will not generate any action.  */
    NOTHING_TO_D0    
  }
}
