package cloudos.machinelearning.autoscaler;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.primitives.Pair;

import com.opencsv.bean.CsvToBeanBuilder;

import lombok.extern.log4j.Log4j2;

/**
 * Implementation of the Autoscale Resources Iterator. This iterator will parse over the data for
 * the autoscaler, which will determine the amount of resources needed for the operations of a
 * cloud provider.
 */
@Log4j2
public class AutoscalerResourcesIterator implements DataSetIterator {

  /* category and its index */
  private final Map<ResourceCategory, Integer> featureMapIndex = new HashMap<ResourceCategory, Integer>() {
    {

      put(ResourceCategory.CPU_USAGE, 0);
      put(ResourceCategory.MEMORY_USAGE, 1);
      put(ResourceCategory.DISK_USAGE, 2);
      put(ResourceCategory.DISK_READ, 3);
      put(ResourceCategory.DISK_WRITE, 4);
      put(ResourceCategory.NETWORK_RECEIVED, 5);
      put(ResourceCategory.NETWORK_SENT, 6);
    }
  };

  private final int VECTOR_SIZE = 7;

  private DataSetPreProcessor preProcessor;
  private ResourceCategory category;
  private int miniBatchSize; // mini-batch size
  private int exampleLength = 20; // default 20, say, 20 time steps
  private int predictLength = 1; // default 10, say, 10 time steps ahead prediction

  /* mini-batch offset */
  private LinkedList<Integer> exampleStartOffsets = new LinkedList<>();

  private double[] minArray = new double[VECTOR_SIZE];
  private double[] maxArray = new double[VECTOR_SIZE];

  /* autoscale dataset for training */
  private List<AutoscaleEntry> train;

  /* adjusted autoscale dataset for testing */
  private List<Pair<INDArray, INDArray>> test;

  /**
   * Constructor for the AutoscalerResourcesIterator class. Allows the user to specify
   * the filename, the mini batch size, the example length and the split ratio.
   *
   * @param filename the name of the file
   * @param miniBatchSize the size of the mini batch
   * @param exampleLength the length of the example
   * @param splitRatio the split ratio
   * @param category the category of the prediction
   */
  public AutoscalerResourcesIterator(String filename,
                                     int miniBatchSize,
                                     int exampleLength,
                                     double splitRatio,
                                     ResourceCategory category) {

    List<AutoscaleEntry> entries = this.readAutoscaleEntriesFromFile(filename);
    this.miniBatchSize = miniBatchSize;
    this.exampleLength = exampleLength;
    this.category = category;
    if (entries != null) {
      int split = (int) Math.round(entries.size() * splitRatio);
      train = entries.subList(0, split);
      test = this.generateTestDataSet(entries.subList(split, entries.size()));
      this.initializeOffsets();
    }
  }

  /**
   * Return the label of a given entry.
   *
   * @param data the autoscale entry data
   * @return the double value of the label
   */
  private double feedLabel(AutoscaleEntry data) {
    double value;
    switch (category) {
      case CPU_USAGE:
        value = (data.getCpuUsage() - this.getMinNum(ResourceCategory.CPU_USAGE)) /
            (this.getMaxNum(ResourceCategory.CPU_USAGE) - this.getMinNum(ResourceCategory.CPU_USAGE));
        break;
      case MEMORY_USAGE:
        value = (data.getMemoryUsage() - this.getMinNum(ResourceCategory.MEMORY_USAGE)) /
            (this.getMaxNum(ResourceCategory.MEMORY_USAGE) - this.getMinNum(ResourceCategory.MEMORY_USAGE));
        break;
      case DISK_USAGE:
        value = (data.getDiskUsage() - this.getMinNum(ResourceCategory.DISK_USAGE)) /
            (this.getMaxNum(ResourceCategory.DISK_USAGE) - this.getMinNum(ResourceCategory.DISK_USAGE));
        break;
      case DISK_READ:
        value = (data.getDiskRead() - this.getMinNum(ResourceCategory.DISK_READ)) /
            (this.getMaxNum(ResourceCategory.DISK_READ) - this.getMinNum(ResourceCategory.DISK_READ));
        break;
      case DISK_WRITE:
        value = (data.getDiskWrite() - this.getMinNum(ResourceCategory.DISK_WRITE)) /
            (this.getMaxNum(ResourceCategory.DISK_WRITE) - this.getMinNum(ResourceCategory.DISK_WRITE));
        break;
      case NETWORK_RECEIVED:
        value = (data.getNetworkReceived() - this.getMinNum(ResourceCategory.NETWORK_RECEIVED)) /
            (this.getMaxNum(ResourceCategory.NETWORK_RECEIVED) -
                this.getMinNum(ResourceCategory.NETWORK_RECEIVED));
        break;
      case NETWORK_SENT:
        value = (data.getNetworkSent() - this.getMinNum(ResourceCategory.NETWORK_SENT)) /
            (this.getMaxNum(ResourceCategory.NETWORK_SENT) - this.getMinNum(ResourceCategory.NETWORK_SENT));
        break;
        default:
          throw new NoSuchElementException();
    }
    // the value to be returned
    return value;
  }

  /**
   * Return the next element in the dataset.
   *
   * @param num the index position in the iterator
   * @return a dataset element
   */
  @Override
  public DataSet next(int num) {
    if (this.exampleStartOffsets.size() == 0)
      throw new NoSuchElementException();
    int actualMiniBatchSize = Math.min(num, this.exampleStartOffsets.size());
    INDArray input = Nd4j.create(new int[] {actualMiniBatchSize, VECTOR_SIZE, exampleLength}, 'f');
    INDArray label;

    if (this.category.equals(ResourceCategory.ALL))
      label = Nd4j.create(new int[] {actualMiniBatchSize, VECTOR_SIZE, exampleLength}, 'f');
    else
      label = Nd4j.create(new int[] {actualMiniBatchSize, predictLength, exampleLength}, 'f');

    for (int index = 0; index < actualMiniBatchSize; index++) {
      int startIdx = this.exampleStartOffsets.removeFirst();
      int endIdx = startIdx + exampleLength;
      AutoscaleEntry curData = this.train.get(startIdx);
      AutoscaleEntry nextData;

      // loop over the range
      for (int i = startIdx; i < endIdx; i++) {
        int current = i - startIdx;
        input.putScalar(new int[] {index, 0, current},
            (curData.getCpuUsage() - this.getMinNum(ResourceCategory.CPU_USAGE)) /
                (this.getMaxNum(ResourceCategory.CPU_USAGE)
                    - this.getMinNum(ResourceCategory.CPU_USAGE)));
        input.putScalar(new int[] {index, 1, current},
            (curData.getMemoryUsage() - this.getMinNum(ResourceCategory.MEMORY_USAGE)) /
                (this.getMaxNum(ResourceCategory.MEMORY_USAGE)
                    - this.getMinNum(ResourceCategory.MEMORY_USAGE)));
        input.putScalar(new int[] {index, 2, current},
            (curData.getDiskUsage() - this.getMinNum(ResourceCategory.DISK_USAGE)) /
                (this.getMaxNum(ResourceCategory.DISK_USAGE)
                    - this.getMinNum(ResourceCategory.DISK_USAGE)));
        input.putScalar(new int[] {index, 3, current},
            (curData.getDiskRead() - this.getMinNum(ResourceCategory.DISK_READ)) /
                (this.getMaxNum(ResourceCategory.DISK_READ)
                    - this.getMinNum(ResourceCategory.DISK_READ)));
        input.putScalar(new int[] {index, 4, current},
            (curData.getDiskWrite() - this.getMinNum(ResourceCategory.DISK_WRITE)) /
                (this.getMaxNum(ResourceCategory.DISK_WRITE)
                    - this.getMinNum(ResourceCategory.DISK_WRITE)));
        input.putScalar(new int[] {index, 5, current},
            (curData.getNetworkReceived() - this.getMinNum(ResourceCategory.NETWORK_RECEIVED)) /
                (this.getMaxNum(ResourceCategory.NETWORK_RECEIVED)
                    - this.getMinNum(ResourceCategory.NETWORK_RECEIVED)));
        input.putScalar(new int[] {index, 6, current},
            (curData.getNetworkSent() - this.getMinNum(ResourceCategory.NETWORK_SENT)) /
                (this.getMaxNum(ResourceCategory.NETWORK_SENT)
                    - this.getMinNum(ResourceCategory.NETWORK_SENT)));

        nextData = train.get(i + 1);
        if (this.category.equals(ResourceCategory.ALL)) {
          input.putScalar(new int[] {index, 0, current},
              (nextData.getCpuUsage() - this.getMinNum(ResourceCategory.CPU_USAGE)) /
                  (this.getMaxNum(ResourceCategory.CPU_USAGE)
                      - this.getMinNum(ResourceCategory.CPU_USAGE)));
          input.putScalar(new int[] {index, 1, current},
              (nextData.getMemoryUsage() - this.getMinNum(ResourceCategory.MEMORY_USAGE)) /
                  (this.getMaxNum(ResourceCategory.MEMORY_USAGE)
                      - this.getMinNum(ResourceCategory.MEMORY_USAGE)));
          input.putScalar(new int[] {index, 2, current},
              (nextData.getDiskUsage() - this.getMinNum(ResourceCategory.DISK_USAGE)) /
                  (this.getMaxNum(ResourceCategory.DISK_USAGE)
                      - this.getMinNum(ResourceCategory.DISK_USAGE)));
          input.putScalar(new int[] {index, 3, current},
              (nextData.getDiskRead() - this.getMinNum(ResourceCategory.DISK_READ)) /
                  (this.getMaxNum(ResourceCategory.DISK_READ)
                      - this.getMinNum(ResourceCategory.DISK_READ)));
          input.putScalar(new int[] {index, 4, current},
              (nextData.getDiskWrite() - this.getMinNum(ResourceCategory.DISK_WRITE)) /
                  (this.getMaxNum(ResourceCategory.DISK_WRITE)
                      - this.getMinNum(ResourceCategory.DISK_WRITE)));
          input.putScalar(new int[] {index, 5, current},
              (nextData.getNetworkReceived() - this.getMinNum(ResourceCategory.NETWORK_RECEIVED)) /
                  (this.getMaxNum(ResourceCategory.NETWORK_RECEIVED)
                      - this.getMinNum(ResourceCategory.NETWORK_RECEIVED)));
          input.putScalar(new int[] {index, 6, current},
              (nextData.getNetworkSent() - this.getMinNum(ResourceCategory.NETWORK_SENT)) /
                  (this.getMaxNum(ResourceCategory.NETWORK_SENT)
                      - this.getMinNum(ResourceCategory.NETWORK_SENT)));
        } else {
          label.putScalar(new int[] {index, 0, current}, this.feedLabel(nextData));
        }
        curData = nextData;
      }
      if (this.exampleStartOffsets.size() == 0)
        break;
    }

    return new DataSet(input, label);
  }

  @Override
  public int totalExamples() {
    return train.size() - exampleLength - predictLength;
  }

  @Override
  public int inputColumns() {
    return VECTOR_SIZE;
  }

  @Override
  public int totalOutcomes() {
    if (this.category.equals(ResourceCategory.ALL))
      return VECTOR_SIZE;
    else
      return predictLength;
  }

  @Override
  public DataSetPreProcessor getPreProcessor() {
    throw new UnsupportedOperationException("Not Implemented");
  }

  @Override
  public List<String> getLabels() {
    throw new UnsupportedOperationException("Not Implemented");
  }

  @Override
  public boolean resetSupported() {
    return true;
  }

  @Override
  public boolean asyncSupported() {
    return false;
  }

  @Override
  public void reset() {
    this.initializeOffsets();
  }

  @Override
  public int batch() {
    return this.miniBatchSize;
  }

  @Override
  public int cursor() {
    return this.totalExamples() - this.exampleStartOffsets.size();
  }

  @Override
  public int numExamples() {
    return this.totalExamples();
  }

  @Override
  public void setPreProcessor(DataSetPreProcessor dataSetPreProcessor) {
    throw new UnsupportedOperationException("Not Implemented");
  }

  @Override
  public void remove() {
    throw new UnsupportedOperationException("Not supported");
  }

  /**
   * Initialize the mini-batch offsets
   */
  private void initializeOffsets() {
    this.exampleStartOffsets.clear();
    int window = exampleLength + predictLength;
    for (int i = 0; i < train.size() - window; i++) {
      this.exampleStartOffsets.add(i);
    }
  }

  /**
   * Return a list containing the test data set.
   *
   * @return a list containing the test data set
   */
  public List<Pair<INDArray, INDArray>> getTestDataSet() {
    return test;
  }

  public double[] getMinArray() {
    return this.minArray;
  }

  public double[] getMaxArray() {
    return this.maxArray;
  }

  public double getMaxNum(ResourceCategory category) {
    return this.maxArray[this.featureMapIndex.get(category)];
  }

  public double getMinNum(ResourceCategory category) {
    return this.minArray[this.featureMapIndex.get(category)];
  }

  /**
   * Returns {@code true} if the iteration has more elements.
   * (In other words, returns {@code true} if {@link #next} would
   * return an element rather than throwing an exception.)
   *
   * @return {@code true} if the iteration has more elements
   */
  @Override
  public boolean hasNext() {
    return this.exampleStartOffsets.size() > 0;
  }

  /**
   * Returns the next element in the iteration.
   *
   * @return the next element in the iteration
   * @throws NoSuchElementException if the iteration has no more elements
   */
  @Override
  public DataSet next() {
    return this.next(this.miniBatchSize);
  }

  @SuppressWarnings("unchecked")
  private List<AutoscaleEntry> readAutoscaleEntriesFromFile(String filename) {
    List<AutoscaleEntry> autoscaleEntries = new ArrayList<>();
    try {
      // initialize for the min and max arrays
      for (int i = 0; i < maxArray.length; i++) {
        maxArray[i] = Double.MIN_VALUE;
        minArray[i] = Double.MAX_VALUE;
      }

      // obtain the list of entries
      List<AutoscaleEntry> entries = new CsvToBeanBuilder(new FileReader(filename))
                                                  .withType(AutoscaleEntry.class)
                                                  .build()
                                                  .parse();

      // find the maximum and minimum range
      for (AutoscaleEntry entry : entries) {
        for (int i = 0; i < VECTOR_SIZE; i++) {
          double value = entry.getValue(i);
          maxArray[i] = Math.max(maxArray[i], value);
          minArray[i] = Math.min(minArray[i], value);
        }
        autoscaleEntries.add(entry);
      }
    } catch (IOException ex) {
      log.error(ex.getMessage());
    }
    return autoscaleEntries;
  }

  /**
   * Convert a list of data points into a test set for the Autoscale Resources.
   *
   * @param autoscaleEntries A list of Autoscale data points to be converted into INDArray
   * @return a list of pairs between test data points and labels
   */
  private List<Pair<INDArray,INDArray>> generateTestDataSet(List<AutoscaleEntry> autoscaleEntries) {

    int window = this.exampleLength + predictLength;
    List<Pair<INDArray, INDArray>> test =  new ArrayList<>();
    for (int i = 0; i < autoscaleEntries.size() - window; i++) {
      INDArray input = Nd4j.create(new int[] {exampleLength, VECTOR_SIZE}, 'f');
      for (int j = i; j < i + exampleLength; j++) {
        AutoscaleEntry entry = autoscaleEntries.get(j);
        input.putScalar(new int[] {j - i, 0},
            (entry.getCpuUsage() - this.getMinNum(ResourceCategory.CPU_USAGE)) /
            (this.getMaxNum(ResourceCategory.CPU_USAGE) -
                this.getMinNum(ResourceCategory.CPU_USAGE)));
        input.putScalar(new int[] {j - i, 1},
            (entry.getMemoryUsage() - this.getMinNum(ResourceCategory.MEMORY_USAGE)) /
                (this.getMaxNum(ResourceCategory.MEMORY_USAGE) -
                    this.getMinNum(ResourceCategory.MEMORY_USAGE)));
        input.putScalar(new int[] {j - i, 2},
            (entry.getDiskUsage() - this.getMinNum(ResourceCategory.DISK_USAGE)) /
                (this.getMaxNum(ResourceCategory.DISK_USAGE) -
                    this.getMinNum(ResourceCategory.DISK_USAGE)));
        input.putScalar(new int[] {j - i, 3},
            (entry.getDiskRead() - this.getMinNum(ResourceCategory.DISK_READ)) /
                (this.getMaxNum(ResourceCategory.DISK_READ) -
                    this.getMinNum(ResourceCategory.DISK_READ)));
        input.putScalar(new int[] {j - i, 4},
            (entry.getDiskWrite() - this.getMinNum(ResourceCategory.DISK_WRITE)) /
                (this.getMaxNum(ResourceCategory.DISK_WRITE) -
                    this.getMinNum(ResourceCategory.DISK_WRITE)));
        input.putScalar(new int[] {j - i, 5},
            (entry.getNetworkReceived() - this.getMinNum(ResourceCategory.NETWORK_RECEIVED)) /
                (this.getMaxNum(ResourceCategory.NETWORK_RECEIVED) -
                    this.getMinNum(ResourceCategory.NETWORK_RECEIVED)));
        input.putScalar(new int[] {j - i, 6},
            (entry.getNetworkSent() - this.getMinNum(ResourceCategory.NETWORK_SENT)) /
                (this.getMaxNum(ResourceCategory.NETWORK_SENT) -
                    this.getMinNum(ResourceCategory.NETWORK_SENT)));
      }

      AutoscaleEntry entry = autoscaleEntries.get(i + exampleLength);
      INDArray label;
      if (category.equals(ResourceCategory.ALL)) {
        // ordering is set as 'f', faster construct
        label = Nd4j.create(new int[] {VECTOR_SIZE}, 'f');
        label.putScalar(new int[] {0}, entry.getCpuUsage());
        label.putScalar(new int[] {1}, entry.getMemoryUsage());
        label.putScalar(new int[] {2}, entry.getDiskUsage());
        label.putScalar(new int[] {3}, entry.getDiskRead());
        label.putScalar(new int[] {4}, entry.getDiskWrite());
        label.putScalar(new int[] {5}, entry.getNetworkReceived());
        label.putScalar(new int[] {6}, entry.getNetworkSent());
      } else {
        label = Nd4j.create(new int[] {1}, 'f');
        switch (category) {
          case CPU_USAGE:
            label.putScalar(new int[] {0}, entry.getCpuUsage());
            break;
          case MEMORY_USAGE:
            label.putScalar(new int[] {0}, entry.getMemoryUsage());
            break;
          case DISK_USAGE:
            label.putScalar(new int[] {0}, entry.getDiskUsage());
            break;
          case DISK_READ:
            label.putScalar(new int[] {0}, entry.getDiskRead());
            break;
          case DISK_WRITE:
            label.putScalar(new int[] {0}, entry.getDiskWrite());
            break;
          case NETWORK_RECEIVED:
            label.putScalar(new int[] {0}, entry.getNetworkReceived());
            break;
          case NETWORK_SENT:
            label.putScalar(new int[] {0}, entry.getNetworkSent());
            break;
          default:
            throw new NoSuchElementException();
        }
      }
      test.add(new Pair<>(input, label));
    }
    return test;
  }
}
