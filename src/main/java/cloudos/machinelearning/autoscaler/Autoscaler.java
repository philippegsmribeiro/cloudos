package cloudos.machinelearning.autoscaler;

import cloudos.Providers;
import cloudos.insights.InsightsService;
import cloudos.instances.CloudManagerService;
import cloudos.instances.InstanceService;
import cloudos.machinelearning.CloudosModel;
import cloudos.machinelearning.Models;
import cloudos.machinelearning.ModelsRepository;
import cloudos.machinelearning.PlotUtil;
import cloudos.machinelearning.autoscaler.PredictionException.PredictionExceptionType;
import cloudos.models.ActionType;
import cloudos.models.CloudActionRequest;
import cloudos.models.CloudCreateRequest;
import cloudos.models.Instance;
import cloudos.models.InstanceStatus;
import cloudos.models.autoscaler.AutoscaleRequest;
import cloudos.models.cloudgroup.CloudGroup;
import cloudos.models.cloudgroup.CloudGroupService;
import cloudos.notifications.NotificationType;
import cloudos.policies.AutoscalerPolicyEntry;
import cloudos.policies.CloudPolicyService;
import cloudos.provider.ProvidersService;
import cloudos.queue.QueueManagerService;
import io.jsonwebtoken.lang.Collections;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import javax.validation.constraints.NotNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Pair;
import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.datavec.api.util.ClassPathResource;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.ComputationGraphConfiguration;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.GravesLSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.springframework.beans.factory.annotation.Value;


/**
 * Created by philipperibeiro on 7/5/17.
 *
 * <p>Implement the Autoscaler Model, which is responsible for monitoring an existing cloud
 * infrastructure. Whenever the resources demand requires the increase or decrease of the amount of
 * resources, the Autoscaler model will first decide whether there's the need to scale or not, and
 * then determine the amount of resources need.
 */
@Log4j2
public class Autoscaler extends CloudosModel {

  private static final int PRINT_ITERATIONS = 10;
  private static final int NUM_INPUTS = 7;
  private static final int NUM_TRAIN_POINTS = 500;
  private static final String DECIDER_MODEL_PREFIX = "decider";
  private static final String SCALER_MODEL_PREFIX = "scaler";

  private static final String SCALER_MODEL = "scaler.zip";
  private static final String DECIDER_MODEL = "decider.zip";

  private static File baseDir;
  private static File baseTrainDir;
  private static File featuresDirTrain;
  private static File labelsDirTrain;
  private static File baseTestDir;
  private static File featuresDirTest;
  private static File labelsDirTest;

  private static int numOfVariables = 0; // in csv.

  @Value("${cloudos.ml.autoscaler.batch_size}")
  private int batchSize = 50;

  @Value("${cloudos.ml.autoscaler.seed}")
  private int seed = 123;

  @Value("${cloudos.ml.autoscaler.learning_rate}")
  private double learningRate = 0.005;

  @Value("${cloudos.ml.autoscaler.momentum}")
  private double momentum = 0.9;

  @Value("${cloudos.ml.autoscaler.label_index}")
  private int labelIndex = 0;

  @Value("${cloudos.ml.autoscaler.num_possible_labels}")
  private int numOfPossibleLabels = 3;

  // Number of epochs (full passes of the data)
  @Value("${cloudos.ml.autoscaler.num_epochs}")
  private int nEpochs = 10;

  @Value("${cloudos.ml.autoscaler.num_iterations}")
  private int numIterations = 1;

  @Value("${cloudos.ml.spotbidder.dropout_ratio}")
  private double dropoutRatio = 0.2;

  @Value("${cloudos.ml.autoscaler}")
  private String autoscalerPath;

  private String deciderModel;

  private String scalerModel;

  /* The classifier is used to determine if the cloud infrastructure needs to be scaled up or not */
  MultiLayerNetwork decider;

  /* The regressor model is responsible for predicting the amount of resources */
  MultiLayerNetwork scaler;

  /* The Machine Type repository will used to fetch information about the instance */
  private final ProvidersService providersService;
  private final InstanceService instanceService;
  private final CloudManagerService cloudManagerService;
  private final QueueManagerService queueManagerService;
  private final CloudGroupService cloudGroupService;
  private final InsightsService insightsService;
  private final CloudPolicyService cloudPolicyService;


  /**
   * Constructor for the Autoscaler that takes the provider's instance types.
   *
   * @param providerMachineTypeRepository a repository containing all the instance types
   */
  public Autoscaler(ProvidersService providersService,
                    ModelsRepository modelsRepository,
                    InstanceService instanceService,
                    CloudManagerService cloudManagerService,
                    String autoscalerPath,
                    QueueManagerService queueManagerService,
                    CloudGroupService cloudGroupService,
                    InsightsService insightsService,
                    CloudPolicyService cloudPolicyService) {

    this.providersService = providersService;
    this.modelsRepository = modelsRepository;
    this.instanceService = instanceService;
    this.cloudManagerService = cloudManagerService;
    this.autoscalerPath = autoscalerPath;
    this.queueManagerService = queueManagerService;
    this.cloudGroupService = cloudGroupService;
    this.insightsService = insightsService;
    this.cloudPolicyService = cloudPolicyService;

    // load the model
    try {
      this.model();
    } catch (IOException e) {
      log.error(e.getMessage());
    }
  }

  /**
   * Allow for initialization of the training set.
   *
   * @param fileName the name of the file to be fetched.
   */
  private static void initBaseFile(String fileName) {
    try {
      baseDir = new ClassPathResource(fileName).getFile();
      baseTrainDir = new File(baseDir, "multiTimestepTrain");
      featuresDirTrain = new File(baseTrainDir, "features");
      labelsDirTrain = new File(baseTrainDir, "labels");
      baseTestDir = new File(baseDir, "multiTimestepTest");
      featuresDirTest = new File(baseTestDir, "features");
      labelsDirTest = new File(baseTestDir, "labels");
    } catch (IOException e) {
      throw new Error(e);
    }
  }

  /**
   * Import a number of arguments necessary to import the pre-trained models.
   *
   * @throws IOException in case failed to load the model
   */
  public void model() throws IOException {
    // check if the root path exists
    if (!cloudos.utils.FileUtils.exits(this.autoscalerPath)) {
      cloudos.utils.FileUtils.create(this.autoscalerPath);
    }
    // we will assume the Application already downloaded the models.
    this.deciderModel =
        Paths.get(this.autoscalerPath, Models.Task.PRODUCTION.toString(), DECIDER_MODEL).toString();
    this.scalerModel =
        Paths.get(this.autoscalerPath, Models.Task.PRODUCTION.toString(), SCALER_MODEL).toString();

    // load the classifier model
    log.info("*********** Loading classifier model: {} ***********", this.deciderModel);
    this.decider = (MultiLayerNetwork) this.loadModel(this.deciderModel, true);
    log.info("*********** Loading regressor model: {} ***********", this.scalerModel);
    // load the regressor model
    this.scaler = (MultiLayerNetwork) this.loadModel(this.scalerModel, true);
  }

  /**
   * Given an INDArray as input for the prediction, load the model and obtain the output for the
   * Decider and the Autoscaler models.
   *
   * @param region the region the instance is located
   * @param vector the INDArray to be predicted
   * @throws PredictionException if autoscaler is not executed
   */
  @Override
  public void predict(final String region, Pair<String, INDArray> vector)
      throws PredictionException {

    // validate data
    
    // get the instance to be decided...
    Instance instance = this.instanceService.findByProviderId(vector.getKey());

    // check if the instance exists
    if (instance == null) {
      String message = String.format("Instance %s does not exist.", vector.getKey());
      log.warn(message);
      throw new PredictionException(PredictionExceptionType.INCOMPLETE_DATA, message);
    }

    // check if it is running
    if (instance.getStatus() != null && !instance.getStatus().equals(InstanceStatus.RUNNING)) {
      String message = String.format("Instance %s is not running. Nothing to do.",
          instance.toDescriptionString());
      log.warn(message);
      throw new PredictionException(PredictionExceptionType.INCOMPLETE_DATA, message);
    }

    // check if it in a cloudgroup
    if (instance.getCloudGroupId() == null) {
      String message =
          String.format("Instance %s does not have a cloud group.", instance.toDescriptionString());
      log.warn(message);
      throw new PredictionException(PredictionExceptionType.INCOMPLETE_DATA, message);
    }

    // check if cloudgroup exists
    CloudGroup cloudGroup = this.cloudGroupService.describeCloudGroup(instance.getCloudGroupId());
    if (cloudGroup == null) {
      String message = String.format("Could not find cloud group %s for instance %s.",
          instance.getCloudGroupId(), instance.toDescriptionString());
      log.warn(message);
      throw new PredictionException(PredictionExceptionType.INCOMPLETE_DATA, message);
    }

    // find the policies
    List<AutoscalerPolicyEntry> applicablePolicies =
        cloudPolicyService.retrieveApplicableAutoscalerPolicies(instance);
    // TODO: apply the rules
    
    int[] predictions = this.decider.predict(vector.getValue());

    log.info("Vector: {}", vector);
    log.info("Prediction: {}", Arrays.toString(predictions));

    // there should be only one label in the predict
    int[] table = new int[3];
    // get a simple majority for now ...
    for (int prediction : predictions) {
      table[prediction]++;
    }

    int maxAt = 0;
    // find the element with most votes
    for (int i = 0; i < table.length; i++) {
      maxAt = table[i] > table[maxAt] ? i : maxAt;
    }

    // make a decision with regards to the number of resources.
    switch (AutoscaleLabels.values()[maxAt]) {
      case DOWNSCALE:
        List<Instance> instances = this.instanceService
            .findAllActiveAndNotStoppedInstancesByProviderAndRegion(instance.getProvider(), region);

        if (Collections.isEmpty(instances) || instances.size() <= 1) {
          log.warn("Minimum number of instances in the region {} met. Nothing to do.", region);
        } else {
          // remove the instance predicted of not being used ...
          // since we are downscaling, we will apply the second set of scalability
          log.info("Scaler predictions: {}", this.scaler.output(vector.getValue(), false));
          log.info("Downscaling for the region {} in provider {}...", region,
              instance.getProvider());
          this.downscale(instance);
        }
        break;
      case MAINTAIN:
        log.info("Nothing to do at this time in the region {}", region);
        break;
      case UPSCALE:
        log.info("Scaler predictions: {}", this.scaler.output(vector.getValue(), false));
        log.info("Upscaling for the region {} in provider {}...", region, instance.getProvider());
        this.upscale(instance, cloudGroup);
        break;
      default:
        log.error("Nothing to do at this time in the region {}", region);
        break;
    }
  }

  /**
   * Downscale by removing the instance from the provider in the given region.
   *
   * @param instance the instance to be removed
   */
  private void downscale(@NotNull Instance instance) {
    log.info("Downscaling instance {}", instance);
    String name = "";
    if (instance.getProvider().equals(Providers.AMAZON_AWS)) {
      name = instance.getProviderId();
    } else if (instance.getProvider().equals(Providers.GOOGLE_COMPUTE_ENGINE)) {
      name = instance.getName();
    }
    CloudActionRequest cloudActionRequest = new CloudActionRequest(
        instance.getProvider(),
        java.util.Collections.singletonList(name),
        instance.getRegion(),
        instance.getZone(),
        instance.getSecurityGroup(),
        true,
        ActionType.TERMINATE
    );
    cloudActionRequest.setFireNotification(true);
    cloudActionRequest.setNotificationType(NotificationType.AUTOSCALER);

    AutoscaleRequest request = AutoscaleRequest.builder()
        .actionRequest(cloudActionRequest)
        .label(AutoscaleLabels.DOWNSCALE)
        .cloudGroupId(instance.getCloudGroupId())
        .instanceInfo(instance.getName())
        .readonly(insightsService.isReadOnlyMode())
        .build();

    log.info("Sending request message {}", request);
    // send the message to the optimizer
    this.queueManagerService.sendAutoscaleMessage(request);
  }

  /**
   * Upscale by increasing the number of instances in the cloud.
   *
   * @param instance a copy of the instance to be created
   */
  protected void upscale(@NotNull Instance instance, @NotNull CloudGroup cloudGroup) {
    log.info("Upscaling instance {}", instance.toDescriptionString());
    CloudCreateRequest cloudCreateRequest = new CloudCreateRequest(
        instance.getProvider(),
        instance.getRegion(),
        instance.getZone(),
        generator.generate(8).toLowerCase(),
        instance.getMachineType(),
        instance.getImageType(),
        instance.getImageProject(),
        1,
        1,
        instance.getSecurityGroup(),
        instance.getKeyName(),
        null,
        instance.getCloudGroupId(),
        false
    );
    AutoscaleRequest request = AutoscaleRequest.builder()
                                  .cloudCreateRequest(cloudCreateRequest)
                                  .label(AutoscaleLabels.UPSCALE)
                                  .cloudGroupId(instance.getCloudGroupId())
                                  .instanceInfo(instance.getName())
                                  .readonly(insightsService.isReadOnlyMode())
                                  .build();
    // send the message to the optimizer
    this.queueManagerService.sendAutoscaleMessage(request);

  }

  /**
   * Train a new Autoscaler model For the first version, we will build a model with two input
   * vectors: CPU and memory usage. Both are numbers between 0, 1 (inclusive), which also directly
   * translate to the probability of needing to scale, giving CPU or memory usage. Our first model
   * we will implement a neural network with three layers: A ReLU activation function layer A
   * Softmax layer A negative log likelihood layer for error and prediction.
   *
   * <p>The output is a label y which contains the following values: 0, when there's the need to
   * scale down 1, when the instance doesn't need to be scaled at all 2, the instance need to be
   * scaled up. Implement a classification model using the following configuration: One model with a
   * MultiLayerConfiguration network One single directional RNN using LSTM One bidirectional RNN
   * using LSTM.
   *
   * @param args the train input file
   * @param plot if the user wants to plot the results into charts
   */
  @Override
  public void train(boolean plot, String... args) {
    //------------------------------------------------------------------------------------
    // There are two neural networks to be implemented. The first is responsible for
    // deciding whether we need to scale or not, while the second network decides on the
    // amount of resources need.
    //------------------------------------------------------------------------------------
    try {

      //------------------------------------------------------------------------------------
      // Train the decider predictor model
      log.info("****************Training Autoscaler model starting********************");
      // this.trainDecideModel(plot, args[0], args[1]);

      //------------------------------------------------------------------------------------
      // Train the resource predictor model
      this.multiTaskScalerModel(plot, args[2]);

      log.info("****************Training Autoscaler model finished********************");

    } catch (Exception e) {
      log.error(e.getMessage());
    }
  }

  /**
   * Implement the decider model. Given the label for deciding whether to scale or not, we use it to
   * determine we need to scale or not.
   *
   * @param plot whether the results of the model should be plotted.
   * @param trainset the file path to the training data set
   * @param testset the file path to the test data set
   */
  public void trainDecideModel(boolean plot, String trainset, String testset) {

    Nd4j.ENFORCE_NUMERICAL_STABILITY = true;

    int numOutputs = 3;
    int numHiddenNodes = 200;

    try {

      log.info("****************Starting to training model DECIDER********************");
      // load the training data:
      RecordReader recordTrain = new CSVRecordReader();
      recordTrain.initialize(new FileSplit(new File(trainset)));
      DataSetIterator trainIter =
          new RecordReaderDataSetIterator(recordTrain, this.batchSize, 0, 3);

      // load the test/evaluation data:
      RecordReader recordTest = new CSVRecordReader();
      recordTest.initialize(new FileSplit(new File(testset)));
      DataSetIterator testIter = new RecordReaderDataSetIterator(recordTest, this.batchSize, 0, 3);

      log.info("Building Autoscaler decider classification model...");
      /*
       * Dense layer: fully connected feed forward layer trainable by backpropagation.
       * Our Initial Network has one hidden layer.
       */
      MultiLayerConfiguration configuration = new NeuralNetConfiguration.Builder()
              .seed(12345)
              .iterations(3)
              .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
              .weightInit(WeightInit.XAVIER)
              .learningRate(0.005)
              .updater(Updater.RMSPROP)
              .gradientNormalization(GradientNormalization.ClipElementWiseAbsoluteValue)  //Not always required, but helps with this data set
              .gradientNormalizationThreshold(0.5)
              .regularization(true)
              .l2(0.001)
              .list()
              .layer(
                  0,
                  new GravesLSTM.Builder()
                      .nIn(NUM_INPUTS)
                      .nOut(numHiddenNodes)
                      .activation(Activation.LEAKYRELU)
                      .build())
              .layer(
                  1,
                  new GravesLSTM.Builder()
                      .nIn(numHiddenNodes)
                      .nOut(numHiddenNodes)
                      .activation(Activation.LEAKYRELU)
                      .build())
              .layer(
                  2,
                  new GravesLSTM.Builder()
                      .nIn(numHiddenNodes)
                      .nOut(numHiddenNodes)
                      .activation(Activation.LEAKYRELU)
                      .build())
              .layer(
                  3,
                  new GravesLSTM.Builder()
                      .nIn(numHiddenNodes)
                      .nOut(numHiddenNodes)
                      .activation(Activation.LEAKYRELU)
                      .build())
              .layer(
                  4,
                  new RnnOutputLayer.Builder(LossFunctions.LossFunction.MCXENT)
                      .activation(Activation.SOFTMAX)
                      .nIn(numHiddenNodes)
                      .nOut(numOutputs)
                      .build())
              .pretrain(false)
              .backprop(true)
              .build();

      MultiLayerNetwork network = new MultiLayerNetwork(configuration);
      network.init();
      //Print score every 10 parameter updates
      network.setListeners(new ScoreIterationListener(PRINT_ITERATIONS));
      for (int i = 0; i < this.nEpochs; i++) {
        network.fit(trainIter);
      }

      log.info("Evaluate model.... ");
      Evaluation eval = new Evaluation(numOutputs);

      // evaluate the test model
      while (testIter.hasNext()) {
        DataSet test = testIter.next();
        INDArray features = test.getFeatureMatrix();
        INDArray labels = test.getLabels();
        INDArray predicted = network.output(features, false);

        eval.eval(labels, predicted);
      }

      log.info("Stats: {}", eval.stats());
      //------------------------------------------------------------------------------------
      // Training is complete. Code that follows is for plotting the data & predictions only
      if (plot) {
        this.plotTraining(network, recordTrain, recordTest, trainset, testset);
      }

      log.info("****************Training DECIDER model finished********************");

      //------------------------------------------------------------------------------------
      // Save the decider model in the Autoscaler repository

      String deciderModel =
          String.format(
              "%s_%s",
              DECIDER_MODEL_PREFIX, new SimpleDateFormat(MODEL_TIMESTAMP).format(new Date()));
      String deciderFilepath = Paths.get(this.autoscalerPath, deciderModel).toString();
      log.info(
          "****************Saving the DECIDER model to the file {}********************",
          deciderFilepath);
      this.saveModel(network, deciderFilepath);
      this.modelsRepository.upload(Models.AUTOSCALER, Models.Task.DEVELOPMENT, deciderFilepath);

    } catch (IOException | InterruptedException e) {
      log.error(e.getMessage());
    }
  }

  /**
   * Implements the MultiTask Scaler model, where the predicted a vector x to another vector y of
   * resources needed.
   *
   * @param plot whether the results of the model should be plotted.
   * @param datafile the file path to the training data set.
   * @throws IOException in case failed to open any of the files
   * @throws InterruptedException in case the process was interrupted during training
   */
  public void multiTaskScalerModel(boolean plot, String datafile) throws IOException {

    log.info("****************Starting to train MultiTask Scaler model ********************");

    Nd4j.ENFORCE_NUMERICAL_STABILITY = true;

    // ----- Train the network, evaluating the test set performance at each epoch -----
    int epochs = 1;
    double splitRatio = 0.9;
    int batchSize = 64;
    int exampleLength = 22;

    log.info("Create dataset iterator...");
    // let's just predict over CPU usage
    ResourceCategory category = ResourceCategory.CPU_USAGE;

    // build the Resources Iterator
    AutoscalerResourcesIterator iterator = new AutoscalerResourcesIterator(
        datafile, batchSize, exampleLength, splitRatio, category
    );

    log.info("Load test dataset...");
    List<org.nd4j.linalg.primitives.Pair<INDArray, INDArray>> test = iterator.getTestDataSet();
    // do the training, and then generate and print samples from network
    MultiLayerNetwork network = this.buildMultiTaskNetwork(iterator.inputColumns(),
                                                           iterator.totalOutcomes());

    log.info("Training....");
    for (int i = 0; i < epochs; i++) {
      while (iterator.hasNext()) {
        // fit model using mini-batch data
        network.fit(iterator.next());
      }
      // reset iterator
      iterator.reset();
      // clear previous state
      network.rnnClearPreviousState();
    }

    log.info("Testing...");
    if (category.equals(ResourceCategory.ALL)) {
      INDArray max = Nd4j.create(iterator.getMaxArray());
      INDArray min = Nd4j.create(iterator.getMinArray());
      this.predictAllCategory(network, test, max, min, category, exampleLength);
    } else {
      double max = iterator.getMaxNum(category);
      double min = iterator.getMinNum(category);
      this.predictOneCategory(network, test, max, min, category, exampleLength);
    }

    log.info("Done!");

    //------------------------------------------------------------------------------------
    String scalerModel =
        String.format(
            "%s_%s",
            SCALER_MODEL_PREFIX, new SimpleDateFormat(MODEL_TIMESTAMP).format(new Date()));
    String scalerFilepath = Paths.get(this.autoscalerPath, scalerModel).toString();
    log.info(
        "****************Saving the SCALER model to the file {}********************",
        scalerFilepath);
    this.saveModel(network, scalerFilepath);
    this.modelsRepository.upload(Models.AUTOSCALER, Models.Task.DEVELOPMENT, scalerFilepath);
    //------------------------------------------------------------------------------------
    // Training is complete. Code that follows is for plotting the data & predictions only

    log.info("----- Finish executing the MultiTask Scaler model -----");
  }

  /**
   * Perform a prediction over a single category for the autoscaler.
   *
   * @param network the neural network to be used for the prediction
   * @param test the test data info
   * @param max the max value for the entries
   * @param min the min value for the entries
   * @param category the resource category
   * @param exampleLength the length of the neural network
   */
  private void predictOneCategory(MultiLayerNetwork network,
                                  List<org.nd4j.linalg.primitives.Pair<INDArray, INDArray>> test,
                                  double max,
                                  double min,
                                  ResourceCategory category,
                                  int exampleLength) {

    double[] predicts = new double[test.size()];
    double[] actuals = new double[test.size()];
    for (int i = 0; i < test.size(); i++) {
      predicts[i] = network.rnnTimeStep(test.get(i).getKey()).getDouble(exampleLength - 1) *
          (max - min) + min;
      actuals[i] = test.get(i).getValue().getDouble(0);
    }
    log.info("Print out Predictions and Actual Values...");
    log.info("Predict, Actual");
    for (int i = 0; i < predicts.length; i++) {
      log.info("{}, {}", predicts[i], actuals[i]);
    }
    log.info("Plot...");
  }

  /**
   * Perform a prediction over all the categories for the autoscaler.
   *
   * @param network the neural network to be used for the prediction
   * @param test the test data info
   * @param max the max INDArray value
   * @param min the min INDArray value
   * @param category the prediction category
   * @param exampleLength the length of the example
   */
  private void predictAllCategory(MultiLayerNetwork network,
                                  List<org.nd4j.linalg.primitives.Pair<INDArray, INDArray>> test,
                                  INDArray max,
                                  INDArray min,
                                  ResourceCategory category,
                                  int exampleLength) {

    INDArray[] predicts = new INDArray[test.size()];
    INDArray[] actuals = new INDArray[test.size()];
    for (int i = 0; i < test.size(); i++) {
      predicts[i] = network.rnnTimeStep(test.get(i).getKey()).getRow(exampleLength - 1).
            mul(max.sub(min)).add(min);
      actuals[i] = test.get(i).getValue();
    }
    log.info("Print out Predictions and Actual values...");
    log.info("Predict\tActual");
    for (int i = 0; i < predicts.length; i++) {
      log.info("{}\t{}", predicts[i], actuals[i]);
    }
    log.info("Plot...");
    for (int n = 0; n < Autoscaler.NUM_INPUTS; n++) {
      double[] pred = new double[predicts.length];
      double[] actual = new double[actuals.length];
      for (int i = 0; i < predicts.length; i++) {
        pred[i] = predicts[i].getDouble(n);
        actual[i] = predicts[i].getDouble(n);
      }
      String name;
      switch (n) {
        case 0:
          name = ResourceCategory.CPU_USAGE.name();
          break;
        case 1:
          name = ResourceCategory.MEMORY_USAGE.name();
          break;
        case 2:
          name = ResourceCategory.DISK_USAGE.name();
          break;
        case 3:
          name = ResourceCategory.DISK_READ.name();
          break;
        case 4:
          name = ResourceCategory.DISK_WRITE.name();
          break;
        case 5:
          name = ResourceCategory.NETWORK_RECEIVED.name();
          break;
        case 6:
          name = ResourceCategory.NETWORK_SENT.name();
          break;
          default:
            throw new NoSuchElementException();
      }
      // plot the results
    }
  }

  /**
   * Build the neural network for the autoscaler.
   *
   * @param nIn the number of input parameters
   * @param nOut the number of output parameters
   * @return a MultiLayerNetwork for the autoscaler
   */
  private MultiLayerNetwork buildMultiTaskNetwork(int nIn, int nOut) {

    final double learningRate = 0.05;
    final int lstmLayer1Size = 256;
    final int lstmLayer2Size = 256;
    final int denseLayerSize = 32;
    final double droupoutRatio = 0.2;
    final int truncatedBPTTLength = 22;

    /*
     * Define the first neural network for the Autoscaler Resources
     */
    MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                  .seed(this.seed)
                  .iterations(this.numIterations)
                  .learningRate(learningRate)
                  .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                  .weightInit(WeightInit.XAVIER)
                  .updater(Updater.RMSPROP)
                  .regularization(true)
                  .l2(1e-4)
                  .list()
                  .layer(0, new GravesLSTM.Builder()
                      .nIn(nIn)
                      .nOut(lstmLayer1Size)
                      .activation(Activation.TANH)
                      .gateActivationFunction(Activation.HARDSIGMOID)
                      .dropOut(droupoutRatio)
                      .build())
                  .layer(1, new GravesLSTM.Builder()
                      .nIn(lstmLayer1Size)
                      .nOut(lstmLayer2Size)
                      .activation(Activation.TANH)
                      .gateActivationFunction(Activation.HARDSIGMOID)
                      .dropOut(droupoutRatio)
                      .build())
                  .layer(2, new GravesLSTM.Builder()
                      .nIn(lstmLayer2Size)
                      .nOut(denseLayerSize)
                      .activation(Activation.RELU)
                      .build())
                  .layer(3, new RnnOutputLayer.Builder()
                      .nIn(denseLayerSize)
                      .nOut(nOut)
                      .activation(Activation.IDENTITY)
                      .lossFunction(LossFunctions.LossFunction.MSE)
                      .build())
                  .backpropType(BackpropType.Standard)
                  .pretrain(false)
                  .backprop(true)
                  .build();

    MultiLayerNetwork network = new MultiLayerNetwork(conf);
    network.init();
    network.setListeners(new ScoreIterationListener(100));
    return network;
  }

  /**
   * Build the neural network for the autoscaler.
   *
   * @param nIn the number of input parameters
   * @param nOut the number of output parameters
   * @return a ComputationGraphConfiguration for the autoscaler
   */
  private ComputationGraph buildMultiTaskGraph(int nIn, int nOut) {

    int lstmLayerSize = 256;

    ComputationGraphConfiguration conf = new NeuralNetConfiguration.Builder()
        .learningRate(0.01)
        .seed(12345)
        .regularization(true)
        .l2(0.001)
        .weightInit(WeightInit.XAVIER)
        .graphBuilder()
        .addInputs("input") // there's a single input layer
        .addLayer("L1",
              new GravesLSTM.Builder()
                  .nIn(nIn)
                  .nOut(nIn)
                  .activation(Activation.TANH)
                  .gateActivationFunction(Activation.HARDSIGMOID)
                  .dropOut(dropoutRatio).build(), "input")
        // add the CPU prediction layer
        .addLayer("cpu",
            new GravesLSTM.Builder()
                .nIn(nIn)
                .nOut(lstmLayerSize)
                .activation(Activation.TANH)
                .gateActivationFunction(Activation.HARDSIGMOID)
                .dropOut(dropoutRatio).build(), "L1")
        // add the memory prediction layer
        .addLayer("memory",
            new GravesLSTM.Builder()
                .nIn(nIn)
                .nOut(lstmLayerSize)
                .activation(Activation.TANH)
                .gateActivationFunction(Activation.HARDSIGMOID)
                .dropOut(dropoutRatio).build(), "L1")
        // add the disk usage prediction usage
        .addLayer("disk_usage",
            new GravesLSTM.Builder()
                .nIn(nIn)
                .nOut(lstmLayerSize)
                .activation(Activation.TANH)
                .gateActivationFunction(Activation.HARDSIGMOID)
                .dropOut(dropoutRatio).build(), "L1")
        // add the disk read prediction layer
        .addLayer("disk_read",
            new GravesLSTM.Builder()
                .nIn(nIn)
                .nOut(lstmLayerSize)
                .activation(Activation.TANH)
                .gateActivationFunction(Activation.HARDSIGMOID)
                .dropOut(dropoutRatio).build(), "L1")
        // add the disk write prediction layer
        .addLayer("disk_write",
            new GravesLSTM.Builder()
                .nIn(nIn)
                .nOut(lstmLayerSize)
                .activation(Activation.TANH)
                .gateActivationFunction(Activation.HARDSIGMOID)
                .dropOut(dropoutRatio).build(), "L1")
        // add the network received prediction layer
        .addLayer("network_received",
            new GravesLSTM.Builder()
                .nIn(nIn)
                .nOut(lstmLayerSize)
                .activation(Activation.TANH)
                .gateActivationFunction(Activation.HARDSIGMOID)
                .dropOut(dropoutRatio).build(), "L1")
        // add the network sent prediction layer
        .addLayer("network_sent",
            new GravesLSTM.Builder()
                .nIn(nIn)
                .nOut(lstmLayerSize)
                .activation(Activation.TANH)
                .gateActivationFunction(Activation.HARDSIGMOID)
                .dropOut(dropoutRatio).build(), "L1")
        .addLayer("cpu_out",
            new RnnOutputLayer.Builder()
                .nIn(lstmLayerSize)
                .nOut(nOut)
                .activation(Activation.IDENTITY).lossFunction(LossFunctions.LossFunction.MSE)
                .build(), "cpu")
        .addLayer("memory_out",
            new RnnOutputLayer.Builder()
                .nIn(lstmLayerSize)
                .nOut(nOut)
                .activation(Activation.IDENTITY).lossFunction(LossFunctions.LossFunction.MSE)
                .build(), "memory")
        .addLayer("disk_usage_out",
            new RnnOutputLayer.Builder()
                .nIn(lstmLayerSize)
                .nOut(nOut)
                .activation(Activation.IDENTITY).lossFunction(LossFunctions.LossFunction.MSE)
                .build(), "disk_usage")
        .addLayer("disk_read_out",
            new RnnOutputLayer.Builder()
                .nIn(lstmLayerSize)
                .nOut(nOut)
                .activation(Activation.IDENTITY).lossFunction(LossFunctions.LossFunction.MSE)
                .build(), "disk_read")
        .addLayer("disk_write_out",
            new RnnOutputLayer.Builder()
                .nIn(lstmLayerSize)
                .nOut(nOut)
                .activation(Activation.IDENTITY).lossFunction(LossFunctions.LossFunction.MSE)
                .build(), "disk_write")
        .addLayer("network_received_out",
            new RnnOutputLayer.Builder()
                .nIn(lstmLayerSize)
                .nOut(nOut)
                .activation(Activation.IDENTITY).lossFunction(LossFunctions.LossFunction.MSE)
                .build(), "network_received")
        .addLayer("network_sent_out",
            new RnnOutputLayer.Builder()
                .nIn(lstmLayerSize)
                .nOut(nOut)
                .activation(Activation.IDENTITY).lossFunction(LossFunctions.LossFunction.MSE)
                .build(), "network_sent")
        .setOutputs("cpu_out", "memory_out", "disk_usage_out", "disk_read_out",
                    "disk_write_out", "network_received_out", "network_sent_out")
        .pretrain(false)
        .backprop(true)
        .build();

    ComputationGraph net = new ComputationGraph(conf);
    net.init();
    net.setListeners(new ScoreIterationListener(1));

    return net;
  }

  /**
   * Plot the result of the training dataset into a respective image file.
   *
   * @param network the neural network
   * @param recordTrain the training record
   * @param recordTest the test record
   * @param filenameTrain the name of the train dataset
   * @param filenameTest the name of the test dataset
   */
  private void plotTraining(
      final MultiLayerNetwork network,
      final RecordReader recordTrain,
      final RecordReader recordTest,
      final String filenameTrain,
      final String filenameTest) {

    //------------------------------------------------------------------------------------
    // Training is complete. Code that follows is for plotting the data & predictions only
    double xMin = -15;
    double xMax = 15;
    double yMin = -15;
    double yMax = 15;

    //Let's evaluate the predictions at every point in the x/y input space, and plot this in
    // the background
    int nPointsPerAxis = 100;
    double[][] evalPoints = new double[nPointsPerAxis * nPointsPerAxis][NUM_INPUTS];
    int count = 0;
    for (int i = 0; i < nPointsPerAxis; i++) {
      for (int j = 0; j < nPointsPerAxis; j++) {
        double x = i * (xMax - xMin) / (nPointsPerAxis - 1) + xMin;
        double y = j * (yMax - yMin) / (nPointsPerAxis - 1) + yMin;

        evalPoints[count][0] = x;
        evalPoints[count][1] = y;

        count++;
      }
    }

    INDArray allXYPoints = Nd4j.create(evalPoints);
    INDArray predictionsAtXYPoints = network.output(allXYPoints);

    // Get all of the training data in a single array, and plot it:
    try {
      recordTrain.initialize(new FileSplit(new File(filenameTrain)));

      recordTrain.reset();

      DataSetIterator trainIter =
          new RecordReaderDataSetIterator(
              recordTrain, NUM_TRAIN_POINTS, labelIndex, numOfPossibleLabels);
      DataSet dataSet = trainIter.next();

      String trainFilename =
          String.format("train_%s", new SimpleDateFormat(IMAGE_TIMESTAMP).format(new Date()));
      String trainFilePath = Paths.get(this.autoscalerPath, trainFilename).toString();
      PlotUtil.plotTrainingData(
          dataSet.getFeatures(),
          dataSet.getLabels(),
          allXYPoints,
          predictionsAtXYPoints,
          nPointsPerAxis,
          trainFilePath,
          false);

      // Get test data, run the test data through the network to generate predictions, and plot those predictions:
      recordTest.initialize(new FileSplit(new File(filenameTest)));
      recordTest.reset();
      int nTestPoints = 100;
      DataSetIterator testIter =
          new RecordReaderDataSetIterator(recordTest, nTestPoints, labelIndex, numOfPossibleLabels);
      dataSet = testIter.next();
      INDArray testPredicted = network.output(dataSet.getFeatures());
      String testFilename =
          String.format("test_%s", new SimpleDateFormat(IMAGE_TIMESTAMP).format(new Date()));
      String testFilePath = Paths.get(this.autoscalerPath, testFilename).toString();
      PlotUtil.plotTestData(
          dataSet.getFeatures(),
          dataSet.getLabels(),
          testPredicted,
          allXYPoints,
          predictionsAtXYPoints,
          nPointsPerAxis,
          testFilePath,
          false);
    } catch (IOException | InterruptedException e) {
      log.error(e.getMessage());
    }
  }

  /**
   * Set the value for the AutoscalerPath.
   *
   * @param autoscalerPath the path to the autoscaler repository
   * @return the Autoscaler
   */
  public Autoscaler withAutoscalerPath(final String autoscalerPath) {
    this.autoscalerPath = autoscalerPath;
    return this;
  }

  /**
   * Set the number of variables in a list of parameters.
   *
   * @param rawStrings a line of parameters
   */
  private void setNumOfVariables(List<String> rawStrings) {
    numOfVariables = rawStrings.get(0).split(",").length;
  }

  public MultiLayerNetwork getDecider() {
    return decider;
  }

  public MultiLayerNetwork getScaler() {
    return scaler;
  }
}
