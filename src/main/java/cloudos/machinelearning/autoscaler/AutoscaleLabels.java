package cloudos.machinelearning.autoscaler;

/**
 * Created by philipperibeiro on 7/5/17.
 *
 * <p>Define the labels to be used when processing the Autoscale model.
 */
public enum AutoscaleLabels {
  DOWNSCALE("0"),
  MAINTAIN("1"),
  UPSCALE("2");

  private String value;

  AutoscaleLabels(String value) {
    this.value = value;
  }

  public String getValue() {
    return this.value;
  }
}
