package cloudos.machinelearning.autoscaler;

import cloudos.data.InstanceDataMetric;
import cloudos.machinelearning.MLUtils;
import cloudos.models.CloudosDatapoint;
import cloudos.models.SparkJob;

import com.mongodb.DBObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import lombok.extern.log4j.Log4j2;

import org.apache.commons.collections4.ListUtils;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import scala.Tuple2;

/**
 * Created by philipperibeiro on 7/1/17.
 *
 * <p>Execute Autoscaler related Spark jobs.
 */
@Log4j2
public class AutoscalerJob extends SparkJob {

  // TODO: Add external sort for large datasets
  private static final int BATCH_SIZE = 100;

  private static final int PARTITION_SIZE = 10;

  // create a cache for documents.
  private static List<Document> documents = new LinkedList<>();

  @Autowired MongoTemplate mongoTemplate;

  /** Default constructor the DashboardJob class. */
  public AutoscalerJob() {
    super();
  }

  /**
   * Constructor allows the specification of the input and output collections in the MongoDB.
   *
   * @param inputCollection the name of the input collection
   * @param outputCollection the name of the output collection
   */
  public AutoscalerJob(String inputCollection, String outputCollection) {

    super(inputCollection, outputCollection);
  }

  /**
   * Assign the JavaSparkContext to the context.
   *
   * @param sc a JavaSparkContext context
   */
  public AutoscalerJob(JavaSparkContext sc) {
    super(sc);
  }

  /** Build a map between the metric set and its value*/
  static Function<CloudosDatapoint, LabeledPoint> mapFunc =
      datapoint -> {
        Map<InstanceDataMetric, Double> vectorMap = MLUtils.getVectorMap();
        // Normalize this value
        vectorMap.put(datapoint.getMetric(), datapoint.getValue());
        List<Double> rows = new ArrayList<>(vectorMap.values());
        double[] vector = rows.stream().mapToDouble(d -> d).toArray();

        return new LabeledPoint(datapoint.getValue(), Vectors.dense(vector));
      };

  /** Build a map between the input given in the faile and its labeled point */
  static Function<String, LabeledPoint> mapFileFunc =
      dataline -> {
        String[] parts = dataline.split(",");
        // label, cpu, mem
        double[] v = new double[parts.length - 1];
        for (int i = 1; i < parts.length; i++) {
          v[i - 1] = Double.parseDouble(parts[i]);
        }
        return new LabeledPoint(Integer.parseInt(parts[0]), Vectors.dense(v));
      };

  /**
   * Get a list of datapoints, partition and sort them. After that, return an iterator of the data
   * points.
   *
   * @param datapoints list of data points
   * @return an iterator of batches sorted by timestamp
   */
  public Iterator<Iterator<Tuple2<Long, Iterable<CloudosDatapoint>>>> readFromDatapoints(
      List<CloudosDatapoint> datapoints) {
    log.info("=========== Reading from a list of datapoints ===========");
    // 1. First we slice the list of datapoints into batches.
    List<List<CloudosDatapoint>> output = ListUtils.partition(datapoints, BATCH_SIZE);
    // 2. Now for each sublist we can can build an RDD from it
    List<Iterator<Tuple2<Long, Iterable<CloudosDatapoint>>>> iterators = new LinkedList<>();
    // 3. Now for each sublist, build an RDD from it
    for (List<CloudosDatapoint> list : output) {
      JavaRDD<CloudosDatapoint> datapointRDD = context.parallelize(list);
      // 5. convert the map to a list of pairs.
      JavaPairRDD<Long, Iterable<CloudosDatapoint>> rdd = this.sortAndAggregate(datapointRDD);
      Iterator<Tuple2<Long, Iterable<CloudosDatapoint>>> iterator = rdd.collect().iterator();
      // add the list as an iterator
      iterators.add(iterator);
    }
    log.info("=========================================");
    return iterators.iterator();
  }

  /**
   * Obtain the Labeled Points from a File.
   *
   * @param filepath the data source file
   * @return a new labeled RDD
   */
  public JavaRDD<LabeledPoint> getLabeledPointsFromFile(String filepath) {
    /* Get labeled point from given file name in base directory */
    JavaRDD<String> data = context.textFile(filepath);
    JavaRDD<LabeledPoint> parsedData = data.map(mapFileFunc);
    return parsedData.cache();
  }

  /**
   * Obtain the Labeled Points from a list of cloudos data points.
   *
   * @param datapoints a list of data points
   * @return a new labeled RDD
   */
  public JavaRDD<LabeledPoint> getLabeledPointsFromDB(List<CloudosDatapoint> datapoints) {

    JavaRDD<CloudosDatapoint> data = context.parallelize(datapoints);
    JavaRDD<LabeledPoint> labeledPointJavaRDD = data.map(mapFunc);
    return labeledPointJavaRDD.cache();
  }

  /**
   * Define the range for the autoscale label.
   *
   * @param score the current score for the CPU utilization
   * @return amatching label
   */
  private static double label(double score) {
    if (0 <= score && score <= 0.20) {
      return -1;
    } else if (0.20 < score && score <= 0.66) {
      return 0;
    } else {
      return 1;
    }
  }

  /**
   * Read all the data points from the collection. The collection must exist.
   *
   * @return an iterator of batches sorted by timestamp
   */
  public Iterator<Iterator<Tuple2<Long, Iterable<CloudosDatapoint>>>> readFromCollection() {
    log.info("=========== Reading from a collection of datapoints ===========");
    JavaRDD<Document> rdd = this.read().repartition(PARTITION_SIZE);
    rdd.foreachPartition(
        (VoidFunction<Iterator<Document>>)
            partition -> {
              partition.forEachRemaining(
                  document -> {
                    documents.add(document);
                  });
            });
    List<CloudosDatapoint> datapoints = new LinkedList<>();
    documents.forEach(
        document -> {
          DBObject parse = (DBObject) com.mongodb.util.JSON.parse(document.toJson());
          CloudosDatapoint read = mongoTemplate.getConverter().read(CloudosDatapoint.class, parse);
          datapoints.add(read);
        });

    // clean the static cache
    documents.clear();
    log.info("=========================================");
    return this.readFromDatapoints(datapoints);
  }

  /**
   * Set the MongoTemplate.
   *
   * @param mongoTemplate a MongoTemplate
   * @return the current autoscale job
   */
  public AutoscalerJob withMongoTemplate(MongoTemplate mongoTemplate) {
    this.mongoTemplate = mongoTemplate;
    return this;
  }

  /**
   * Sort the Datapoint RDD by timestamp.
   *
   * @param rdd JavaRDD of CloudosDatapoints
   * @return a Java Pair RDD of timestamp (long, Iterable)
   */
  private JavaPairRDD<Long, Iterable<CloudosDatapoint>> sortAndAggregate(
      JavaRDD<CloudosDatapoint> rdd) {
    return rdd.mapToPair(x -> new Tuple2<>(x.getTimestamp().getTime(), x)).groupByKey().sortByKey();
  }
}
