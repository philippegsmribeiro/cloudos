package cloudos.machinelearning.insights;

import org.apache.commons.lang3.tuple.Pair;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.ComputationGraphConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.GravesLSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import cloudos.machinelearning.CloudosModel;

public class InsightsResizingModel extends CloudosModel {

  private int tbpttLength = 50;
  private int lstmLayerSize = 200;
  private int nEpochs = 100;
  private int miniBatchSize = 32;
  private int exampleLength = 1000;
  private double learningRate = 0.1;
  private double l2Regulatization = 0.001;

  /**
   *
   * @return
   */
  private ComputationGraph buildNeuralNetwork() {

    ComputationGraphConfiguration configuration = new NeuralNetConfiguration.Builder()
            .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
            .iterations(1)
            .learningRate(this.learningRate)
            .seed(12345)
            .regularization(true)
            .l2(l2Regulatization)
            .weightInit(WeightInit.XAVIER)
            .graphBuilder()
            .addInputs("input")
            // First Layer, name "first", with inputs from the input called "input"
            .addLayer("first",
                new GravesLSTM.Builder().nIn(7).nOut(lstmLayerSize)
                    .updater(Updater.RMSPROP)
                    .activation(Activation.TANH)
                    .build(), "input")
            // Second layer, name "second", with inputs from the layer called "first"
            .addLayer("second",
                new GravesLSTM.Builder().nIn(lstmLayerSize).nOut(lstmLayerSize)
                    .updater(Updater.RMSPROP)
                    .activation(Activation.TANH)
                    .build(), "first")
            .addLayer("outputLayer",
                new RnnOutputLayer.Builder(LossFunctions.LossFunction.MCXENT)
                    .activation(Activation.SOFTMAX)
                    .updater(Updater.RMSPROP)
                    .nIn(lstmLayerSize).nOut(1)
                    .build(), "second")
            // List the output. For a computationGraph with Multiple Outputs, this also defines
            // the input array orders
            .setOutputs("outputLayer")
            .backpropType(BackpropType.TruncatedBPTT)
            .tBPTTForwardLength(tbpttLength)
            .tBPTTBackwardLength(tbpttLength)
            .pretrain(false)
            .backprop(true)
            .build();

    ComputationGraph network = new ComputationGraph(configuration);
    network.init();
    network.setListeners(new ScoreIterationListener(1));

    return network;
  }

  @Override
  public void predict(String region, Pair<String, INDArray> vector) {

  }

  @Override
  public void train(boolean plot, String... args) {

  }
}
