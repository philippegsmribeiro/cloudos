package cloudos.machinelearning;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import lombok.extern.log4j.Log4j2;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.GrayPaintScale;
import org.jfree.chart.renderer.PaintScale;
import org.jfree.chart.renderer.xy.XYBlockRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.PaintScaleLegend;
import org.jfree.data.xy.DefaultXYZDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.data.xy.XYZDataset;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.RectangleInsets;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.api.ops.impl.indexaccum.IMax;
import org.nd4j.linalg.factory.Nd4j;

/**
 * Created by philipperibeiro on 7/7/17.
 *
 * <p>Simple plotting methods for the Machine Learning System.
 */
@Log4j2
public class PlotUtil {

  private static final String JPEG_FORMAT = "jpeg";

  private static final String PNG_FORMAT = "png";

  /**
   * Save image to a given file in a given format.
   *
   * @param panel
   * @param filename
   */
  private static boolean saveImage(JPanel panel, final String filename, final String format) {
    Dimension size = panel.getSize();
    BufferedImage image = new BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_RGB);
    Graphics2D g2 = image.createGraphics();
    panel.paint(g2);
    try {
      ImageIO.write(image, format, new File(filename));
      return true;
    } catch (Exception e) {
      log.error(e.getMessage());
      return false;
    }
  }

  /**
   * Plot the training data. Assume 2d input, classification output
   *
   * @param features Training data features
   * @param labels Training data labels (one-hot representation)
   * @param backgroundIn sets of x,y points in input space, plotted in the background
   * @param backgroundOut results of network evaluation at points in x,y points in space
   * @param nDivisions Number of points (per axis, for the backgroundIn/backgroundOut arrays)
   * @param filename: The name of the image file to be created.
   * @param pngFormat: Whether the format is PNG.
   */
  public static void plotTrainingData(
      INDArray features,
      INDArray labels,
      INDArray backgroundIn,
      INDArray backgroundOut,
      int nDivisions,
      String filename,
      boolean pngFormat) {
    double[] mins = backgroundIn.min(0).data().asDouble();
    double[] maxs = backgroundIn.max(0).data().asDouble();

    XYZDataset backgroundData = createBackgroundData(backgroundIn, backgroundOut);
    JPanel panel =
        new ChartPanel(
            createChart(
                backgroundData, mins, maxs, nDivisions, createDataSetTrain(features, labels)));

    JFrame f = new JFrame();
    f.add(panel);
    f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    f.pack();
    f.setTitle("Training Data");

    f.setVisible(true);

    if (pngFormat) {
      if (!saveImage(panel, filename, PNG_FORMAT)) {
        log.warn("Failed to create image in the file {}", filename);
      }
    } else {
      if (!saveImage(panel, filename, JPEG_FORMAT)) {
        log.warn("Failed to create image in the file {}", filename);
      }
    }
  }

  /**
   * Create a simple series for the chart
   *
   * @param seriesCollection
   * @param data
   * @param offset
   * @param name
   */
  public static void createSeries(
      XYSeriesCollection seriesCollection, INDArray data, int offset, String name) {
    int nRows = data.shape()[2];
    XYSeries series = new XYSeries(name);
    for (int i = 0; i < nRows; i++) {
      series.add(i + offset, data.getDouble(i));
    }
    seriesCollection.addSeries(series);
  }

  /**
   * Plot the training data. Assume 2d input, classification output
   *
   * @param features Training data features
   * @param labels Training data labels (one-hot representation)
   * @param predicted Network predictions, for the test points
   * @param backgroundIn sets of x,y points in input space, plotted in the background
   * @param backgroundOut results of network evaluation at points in x,y points in space
   * @param nDivisions Number of points (per axis, for the backgroundIn/backgroundOut arrays)
   * @param filename: The name of the image file to be created.
   * @param pngFormat: Whether the format is PNG.
   */
  public static void plotTestData(
      INDArray features,
      INDArray labels,
      INDArray predicted,
      INDArray backgroundIn,
      INDArray backgroundOut,
      int nDivisions,
      String filename,
      boolean pngFormat) {

    double[] mins = backgroundIn.min(0).data().asDouble();
    double[] maxs = backgroundIn.max(0).data().asDouble();

    XYZDataset backgroundData = createBackgroundData(backgroundIn, backgroundOut);
    JPanel panel =
        new ChartPanel(
            createChart(
                backgroundData,
                mins,
                maxs,
                nDivisions,
                createDataSetTest(features, labels, predicted)));

    JFrame f = new JFrame();
    f.add(panel);
    f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    f.pack();
    f.setTitle("Test Data");

    f.setVisible(true);

    if (pngFormat) {
      saveImage(panel, filename, PNG_FORMAT);
    } else {
      saveImage(panel, filename, JPEG_FORMAT);
    }
  }

  /**
   * Create data for the background data set
   *
   * @param backgroundIn
   * @param backgroundOut
   * @return
   */
  private static XYZDataset createBackgroundData(INDArray backgroundIn, INDArray backgroundOut) {
    int nRows = backgroundIn.rows();
    double[] xValues = new double[nRows];
    double[] yValues = new double[nRows];
    double[] zValues = new double[nRows];
    for (int i = 0; i < nRows; i++) {
      xValues[i] = backgroundIn.getDouble(i, 0);
      yValues[i] = backgroundIn.getDouble(i, 1);
      zValues[i] = backgroundOut.getDouble(i);
    }

    DefaultXYZDataset dataset = new DefaultXYZDataset();
    dataset.addSeries("Series 1", new double[][] {xValues, yValues, zValues});
    return dataset;
  }

  /**
   * Training data
   *
   * @param features
   * @param labels
   * @return
   */
  private static XYDataset createDataSetTrain(INDArray features, INDArray labels) {
    int nRows = features.rows();

    int nClasses = labels.columns();

    XYSeries[] series = new XYSeries[nClasses];
    for (int i = 0; i < series.length; i++) {
      series[i] = new XYSeries("Class " + String.valueOf(i));
    }
    INDArray argMax = Nd4j.getExecutioner().exec(new IMax(labels), 1);
    for (int i = 0; i < nRows; i++) {
      int classIdx = (int) argMax.getDouble(i);
      series[classIdx].add(features.getDouble(i, 0), features.getDouble(i, 1));
    }

    XYSeriesCollection c = new XYSeriesCollection();
    for (XYSeries s : series) {
      c.addSeries(s);
    }
    return c;
  }

  /**
   * Test data
   *
   * @param features
   * @param labels
   * @param predicted
   * @return
   */
  private static XYDataset createDataSetTest(
      INDArray features, INDArray labels, INDArray predicted) {
    int nRows = features.rows();

    int nClasses = labels.columns();

    XYSeries[] series = new XYSeries[nClasses * nClasses]; //new XYSeries("Data");
    for (int i = 0; i < nClasses * nClasses; i++) {
      int trueClass = i / nClasses;
      int predClass = i % nClasses;
      String label = "actual=" + trueClass + ", pred=" + predClass;
      series[i] = new XYSeries(label);
    }
    INDArray actualIdx = Nd4j.getExecutioner().exec(new IMax(labels), 1);
    INDArray predictedIdx = Nd4j.getExecutioner().exec(new IMax(predicted), 1);
    for (int i = 0; i < nRows; i++) {
      int classIdx = (int) actualIdx.getDouble(i);
      int predIdx = (int) predictedIdx.getDouble(i);
      int idx = classIdx * nClasses + predIdx;
      series[idx].add(features.getDouble(i, 0), features.getDouble(i, 1));
    }

    XYSeriesCollection c = new XYSeriesCollection();
    for (XYSeries s : series) {
      c.addSeries(s);
    }
    return c;
  }

  /**
   * Given a number of data points, allow for the creating of a customized chart
   *
   * @param dataset: The multidimensional dataset to be plotted.
   * @param mins: The min values
   * @param maxs: The max values
   * @param nPoints: The number of points
   * @param xyData: The x-y dataset.
   * @return: A new JFreeChart
   */
  private static JFreeChart createChart(
      XYZDataset dataset, double[] mins, double[] maxs, int nPoints, XYDataset xyData) {
    NumberAxis xAxis = new NumberAxis("X");
    xAxis.setRange(mins[0], maxs[0]);

    NumberAxis yAxis = new NumberAxis("Y");
    yAxis.setRange(mins[1], maxs[1]);

    XYBlockRenderer renderer = new XYBlockRenderer();
    renderer.setBlockWidth((maxs[0] - mins[0]) / (nPoints - 1));
    renderer.setBlockHeight((maxs[1] - mins[1]) / (nPoints - 1));
    PaintScale scale = new GrayPaintScale(0, 1.0);
    renderer.setPaintScale(scale);
    XYPlot plot = new XYPlot(dataset, xAxis, yAxis, renderer);
    plot.setBackgroundPaint(Color.lightGray);
    plot.setDomainGridlinesVisible(false);
    plot.setRangeGridlinesVisible(false);
    plot.setAxisOffset(new RectangleInsets(5, 5, 5, 5));
    JFreeChart chart = new JFreeChart("", plot);
    chart.getXYPlot().getRenderer().setSeriesVisibleInLegend(0, false);

    NumberAxis scaleAxis = new NumberAxis("Probability (class 0)");
    scaleAxis.setAxisLinePaint(Color.white);
    scaleAxis.setTickMarkPaint(Color.white);
    scaleAxis.setTickLabelFont(new Font("Dialog", Font.PLAIN, 7));
    PaintScaleLegend legend = new PaintScaleLegend(new GrayPaintScale(), scaleAxis);
    legend.setStripOutlineVisible(false);
    legend.setSubdivisionCount(20);
    legend.setAxisLocation(AxisLocation.BOTTOM_OR_LEFT);
    legend.setAxisOffset(5.0);
    legend.setMargin(new RectangleInsets(5, 5, 5, 5));
    legend.setFrame(new BlockBorder(Color.red));
    legend.setPadding(new RectangleInsets(10, 10, 10, 10));
    legend.setStripWidth(10);
    legend.setPosition(RectangleEdge.LEFT);
    chart.addSubtitle(legend);

    ChartUtilities.applyCurrentTheme(chart);

    plot.setDataset(1, xyData);
    XYLineAndShapeRenderer renderer2 = new XYLineAndShapeRenderer();
    renderer2.setBaseLinesVisible(false);
    plot.setRenderer(1, renderer2);

    plot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);

    return chart;
  }
}
