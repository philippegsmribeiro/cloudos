package cloudos.provider;

import cloudos.Providers;
import cloudos.google.GoogleImageType;
import cloudos.google.GoogleMachineType;
import cloudos.google.GoogleRegion;
import cloudos.google.integration.GoogleComputeIntegration;
import cloudos.keys.cloudoskey.CloudosKey;
import cloudos.keys.cloudoskey.CloudosKeyService;
import com.google.cloud.compute.Image;
import com.google.cloud.compute.MachineType;
import com.google.cloud.compute.Region;
import com.google.cloud.compute.ZoneId;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class GoogleProviderManagement implements ProviderManagement {

  @Autowired
  GoogleComputeIntegration googleIntegration;
  @Autowired
  ProviderRegionRepository regionRepository;
  @Autowired
  ProviderImageTypeRepository imageTypeRepository;
  @Autowired
  ProviderMachineTypeRepository machineTypeRepository;
  @Autowired
  CloudosKeyService cloudosKeyService;
  // TODO: review it later
  Gson gson = new Gson();

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.ResourcesManagement#retrieveMachineTypes()
   */
  @Override
  public List<cloudos.provider.ProviderMachineType> retrieveMachineTypes() {
    List<cloudos.provider.ProviderMachineType> findByProvider =
        machineTypeRepository.findByProvider(Providers.GOOGLE_COMPUTE_ENGINE);
    if (CollectionUtils.isNotEmpty(findByProvider)) {
      return sortMachineTypes(findByProvider);
    }
    //
    syncMachineTypes();
    findByProvider = machineTypeRepository.findByProvider(Providers.GOOGLE_COMPUTE_ENGINE);
    if (CollectionUtils.isNotEmpty(findByProvider)) {
      return sortMachineTypes(findByProvider);
    }
    return Collections.emptyList();
  }

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.ProviderManagement#syncMachineTypes()
   */
  @Override
  public void syncMachineTypes() {
    // go to provider
    Iterable<MachineType> listPublicImages = googleIntegration.listMachineType();
    List<cloudos.provider.ProviderMachineType> list = new ArrayList<>();
    List<String> descriptionList = new ArrayList<>();
    for (MachineType machineType : listPublicImages) {
      if (machineType.getDeprecationStatus() == null) {
        GoogleMachineType googleMachineType = new GoogleMachineType();
        googleMachineType.setDescription(
            machineType.getDescription() + " - " + machineType.getMachineTypeId().getType());
        googleMachineType.setKey(machineType.getMachineTypeId().getType());
        googleMachineType.setCpu(machineType.getCpus().doubleValue());
        googleMachineType.setMemory(machineType.getMemoryMb().doubleValue() / 1024);
        googleMachineType
            .setRegion(this.getRegionFromZoneString(machineType.getMachineTypeId().getZone()));
        googleMachineType.setObject(gson.fromJson(gson.toJson(machineType), Object.class));
        googleMachineType.setGroup("Cpu size: " + machineType.getCpus());
        if (!descriptionList.contains(googleMachineType.getKey() + googleMachineType.getRegion())) {
          list.add(googleMachineType);
          descriptionList.add(googleMachineType.getKey() + googleMachineType.getRegion());
        }
      }
    }
    // if found
    if (CollectionUtils.isNotEmpty(list)) {
      // delete the old ones
      List<cloudos.provider.ProviderMachineType> findByProvider =
          machineTypeRepository.findByProvider(Providers.GOOGLE_COMPUTE_ENGINE);
      if (CollectionUtils.isNotEmpty(findByProvider)) {
        machineTypeRepository.delete(findByProvider);
      }
      // save the new ones
      try {
        machineTypeRepository.save(list);
      } catch (DuplicateKeyException e) {
        // if data already there... ignore error.
        log.warn("Duplicate key: {}", list);
      }
    }
  }

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.ResourcesManagement#retrieveImagesTypes()
   */
  @Override
  public List<cloudos.provider.ProviderImageType> retrieveImagesTypes() {
    List<ProviderImageType> findByProvider =
        imageTypeRepository.findByProvider(Providers.GOOGLE_COMPUTE_ENGINE);
    if (CollectionUtils.isNotEmpty(findByProvider)) {
      return sortImageTypes(findByProvider);
    }
    //
    syncImagesTypes();
    findByProvider = imageTypeRepository.findByProvider(Providers.GOOGLE_COMPUTE_ENGINE);
    if (CollectionUtils.isNotEmpty(findByProvider)) {
      return sortImageTypes(findByProvider);
    }
    return Collections.emptyList();
  }

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.ProviderManagement#syncImagesTypes()
   */
  @Override
  public void syncImagesTypes() {
    Iterable<Image> listPublicImages = googleIntegration.listPublicImages();
    List<ProviderImageType> list = new ArrayList<>();
    for (Image image : listPublicImages) {
      if (image.getDeprecationStatus() == null) {
        GoogleImageType imageType = new GoogleImageType();
        String description = image.getDescription();
        imageType.setDescription(description.substring(description.indexOf(", ") + 2).trim());
        imageType.setGroup(description.substring(0, description.indexOf(",")).trim());
        imageType.setProject(image.getImageId().getProject());
        imageType.setKey(image.getImageId().getImage());
        LinkedTreeMap<?, ?> fromJson =
            (LinkedTreeMap<?, ?>) gson.fromJson(gson.toJson(image), Object.class);
        fromJson.remove("options");
        imageType.setObject(fromJson);
        list.add(imageType);
      }
    }
    if (CollectionUtils.isNotEmpty(list)) {
      // delete the old ones
      List<ProviderImageType> findByProvider =
          imageTypeRepository.findByProvider(Providers.GOOGLE_COMPUTE_ENGINE);
      if (CollectionUtils.isNotEmpty(findByProvider)) {
        imageTypeRepository.delete(findByProvider);
      }
      // save the new ones
      try {
        imageTypeRepository.save(list);
      } catch (DuplicateKeyException e) {
        // if data already there... ignore error.
        log.warn("Duplicate key: {}", list);
      }
    }
  }

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.ResourcesManagement#retrieveRegions()
   */
  @Override
  public List<cloudos.provider.ProviderRegion> retrieveRegionsWithZones() {
    List<cloudos.provider.ProviderRegion> findByProvider =
        regionRepository.findByProvider(Providers.GOOGLE_COMPUTE_ENGINE);
    if (CollectionUtils.isNotEmpty(findByProvider)) {
      return sortRegions(findByProvider);
    }
    // if not found - force a sync
    syncRegions();
    // try to get again
    findByProvider = regionRepository.findByProvider(Providers.GOOGLE_COMPUTE_ENGINE);
    //
    if (CollectionUtils.isNotEmpty(findByProvider)) {
      return sortRegions(findByProvider);
    }
    return Collections.emptyList();
  }

  @Override
  public List<ProviderRegion> retrieveRegions() {
    List<cloudos.provider.ProviderRegion> retrieveRegions = this.retrieveRegionsWithZones();
    // group by regions
    Map<String, ProviderRegion> map = new HashMap<>();
    for (ProviderRegion providerRegion : retrieveRegions) {
      String region = providerRegion.getGroup();
      if (!map.containsKey(region)) {
        ProviderRegion groupRegion = ProviderRegion.builder()
                                                   .region(region)
                                                   .description(region)
                                                   .key(region)
                                                   .provider(Providers.GOOGLE_COMPUTE_ENGINE)
                                                   .build();
        map.put(region, groupRegion);
      }
    }
    return new ArrayList<>(map.values());
  }

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.ProviderManagement#syncRegions()
   */
  @Override
  public void syncRegions() {
    // go to the provider and get the list of regions
    Iterable<Region> listPublicImages = googleIntegration.listRegions();
    List<cloudos.provider.ProviderRegion> list = new ArrayList<>();
    for (Region region : listPublicImages) {
      if (region.getDeprecationStatus() == null) {
        List<ZoneId> zones = region.getZones();
        for (ZoneId zoneId : zones) {
          GoogleRegion googleRegion = new GoogleRegion();
          googleRegion.setDescription(zoneId.getZone());
          googleRegion.setKey(zoneId.getZone());
          googleRegion.setGroup(region.getRegionId().getRegion());
          googleRegion.setObject(region);
          googleRegion.setRegion(region.getRegionId().getRegion());
          googleRegion.setObject(gson.fromJson(gson.toJson(region), Object.class));
          list.add(googleRegion);
        }
      }
    }
    // if found
    if (CollectionUtils.isNotEmpty(list)) {
      // delete the old ones
      List<cloudos.provider.ProviderRegion> findByProvider =
          regionRepository.findByProvider(Providers.GOOGLE_COMPUTE_ENGINE);
      if (CollectionUtils.isNotEmpty(findByProvider)) {
        regionRepository.delete(findByProvider);
      }
      // save the new ones
      try {
        regionRepository.save(list);
      } catch (DuplicateKeyException e) {
        // if data already there... ignore error.
        log.warn("Duplicate key: {}", list);
      }
    }
  }

  @Override
  public List<ProviderKey> retrieveKeys() {
    final List<CloudosKey> findByProvider =
        cloudosKeyService.findActiveKeysByProvider(Providers.GOOGLE_COMPUTE_ENGINE);
    List<ProviderKey> list = new ArrayList<>();
    for (CloudosKey cloudosKey : findByProvider) {
      ProviderKey key = ProviderKey.builder()
                                   .provider(Providers.GOOGLE_COMPUTE_ENGINE)
                                   .description(cloudosKey.getKeyName())
                                   .key(cloudosKey.getKeyName())
                                   .region(cloudosKey.getRegion())
                                   .enabled(true)
                                   .build();
      list.add(key);
    }
    return list;
  }

  @Override
  public void syncKeys() {
    // to nothing as google doesnt keep the keys
  }

  @Override
  public List<ProviderSecurityGroup> retrieveSecurityGroups() {
    ProviderSecurityGroup sg = ProviderSecurityGroup.builder()
                                                    .provider(Providers.GOOGLE_COMPUTE_ENGINE)
                                                    .key("default")
                                                    .build();
    return Arrays.asList(sg);
  }

  @Override
  public void syncSecurityGroups() {
    // to nothing as google doesnt keep the security groups
  }

  /**
   * Auxiliary method to get a regions from a zone. TODO: improve this method to not go to mongo
   * everytime.
   *
   * @param zone name
   * @return provider region
   */
  public cloudos.provider.ProviderRegion getRegionFromZone(String zone) {
    ProviderRegion region =
        regionRepository.findByProviderAndKey(Providers.GOOGLE_COMPUTE_ENGINE, zone);
    if (region == null) {
      syncRegions();
      region = regionRepository.findByProviderAndKey(Providers.GOOGLE_COMPUTE_ENGINE, zone);
    }
    return region;
  }

  /**
   * Auxiliary method which gets the region base on the string of the zone name.
   *
   * @param zone name
   * @return region name
   */
  public String getRegionFromZoneString(String zone) {
    if (StringUtils.isNotBlank(zone)) {
      if (zone.contains("-")) {
        return zone.substring(0, zone.lastIndexOf("-"));
      }
    }
    return zone;
  }

  /**
   * Get the provider image type object from a project and image (incomplete - without version).
   *
   * @param imageProject project
   * @param image name
   * @return ProviderImageType
   */
  public ProviderImageType getFullImageKey(String imageProject, final String image) {
    if (CollectionUtils
        .isEmpty(imageTypeRepository.findByProvider(Providers.GOOGLE_COMPUTE_ENGINE))) {
      // if not load yet
      syncImagesTypes();
    }
    List<ProviderImageType> list =
        imageTypeRepository.findByProviderAndProject(Providers.GOOGLE_COMPUTE_ENGINE, imageProject);
    if (CollectionUtils.isNotEmpty(list)) {
      List<ProviderImageType> images = list.stream().filter(t -> t.getKey().startsWith(image))
          .sorted((h1, h2) -> -1 * h1.getIncludedDate().compareTo(h2.getIncludedDate()))
          .collect(Collectors.toList());
      if (CollectionUtils.isNotEmpty(images)) {
        return images.get(0);
      }
    }
    return null;
  }

  @Override
  public ProviderMachineType retrieveMachineType(String machineType, String region) {
    if (machineTypeRepository.countByProvider(Providers.GOOGLE_COMPUTE_ENGINE) == 0) {
      syncMachineTypes();
    }
    return machineTypeRepository.findByKeyAndProviderAndRegion(machineType,
        Providers.GOOGLE_COMPUTE_ENGINE, region);
  }
  
  @Override
  public void updateKeys(CloudosKey cloudosKey) {
    // nothing to do - gcloud doesnt keep the keys
  }
}
