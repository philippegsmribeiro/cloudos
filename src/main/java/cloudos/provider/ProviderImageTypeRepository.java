package cloudos.provider;

import cloudos.Providers;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

interface ProviderImageTypeRepository extends MongoRepository<ProviderImageType, String> {

  List<ProviderImageType> findByProvider(Providers provider);

  ProviderImageType findByKey(String key);

  List<ProviderImageType> findByProviderAndProject(Providers provider, String project);
}
