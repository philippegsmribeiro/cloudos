package cloudos.provider;

import cloudos.Providers;
import java.util.Date;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@EqualsAndHashCode(callSuper = true)
@Document(collection = "provider_machine_type")
public class ProviderMachineType extends ProviderElement {

  private Double cpu;
  private Double memory;
  
  public ProviderMachineType() {
  }
  
  /**
   * Constructor with all fields for the builder.
   * @param id internal id
   * @param provider provider of element
   * @param project project of provider
   * @param key key identifier (must be unique for region and provider)
   * @param description description of the element
   * @param group view group
   * @param region provider region
   * @param object custom object from provider
   * @param includedDate date when it was included on the application
   * @param enabled true if enabled
   * @param cpu cpu size
   * @param memory memory size
   */
  @Builder
  public ProviderMachineType(String id, Providers provider, String project, String key,
      String description, String group, String region, Object object, Date includedDate,
      Boolean enabled,  Double cpu,  Double memory) {
    super(id, provider, project, key, description, group, region, object, includedDate, enabled);
    setCpu(cpu);
    setMemory(memory);
  }
}
