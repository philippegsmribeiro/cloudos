package cloudos.provider;

import cloudos.Providers;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

interface ProviderSecurityGroupRepository extends MongoRepository<ProviderSecurityGroup, String> {

  List<ProviderSecurityGroup> findByProvider(Providers provider);

}
