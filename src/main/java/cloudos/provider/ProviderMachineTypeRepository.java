package cloudos.provider;

import cloudos.Providers;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

interface ProviderMachineTypeRepository extends MongoRepository<ProviderMachineType, String> {

  List<ProviderMachineType> findByProvider(Providers provider);

  List<ProviderMachineType> findByProviderAndRegion(Providers provider, String region);

  ProviderMachineType findByKeyAndProviderAndRegion(String key, Providers provider, String region);

  int countByProvider(Providers provider);
}
