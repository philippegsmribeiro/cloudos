package cloudos.provider;

import cloudos.Providers;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

interface ProviderKeyRepository extends MongoRepository<ProviderKey, String> {

  List<ProviderKey> findByProvider(Providers provider);
  
  Long deleteByProvider(Providers provider);
}
