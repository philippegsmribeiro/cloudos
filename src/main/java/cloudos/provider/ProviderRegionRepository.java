package cloudos.provider;

import cloudos.Providers;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

interface ProviderRegionRepository extends MongoRepository<ProviderRegion, String> {

  List<ProviderRegion> findByProvider(Providers provider);
  
  ProviderRegion findByProviderAndKey(Providers provider, String key);
}
