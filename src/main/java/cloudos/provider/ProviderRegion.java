package cloudos.provider;

import cloudos.Providers;
import java.util.Date;
import lombok.Builder;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "provider_region")
public class ProviderRegion extends ProviderElement {

  public ProviderRegion() {
  }
  
  @Builder
  public ProviderRegion(String id, Providers provider, String project, String key,
      String description, String group, String region, Object object, Date includedDate,
      Boolean enabled) {
    super(id, provider, project, key, description, group, region, object, includedDate, enabled);
  }
  
}
