package cloudos.provider;

import cloudos.Providers;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@Data
@AllArgsConstructor
@CompoundIndexes({@CompoundIndex(unique = true, dropDups = true, name = "provider_region_key",
    def = "{'provider' : 1, 'region' : 1, 'key': -1}")})
public class ProviderElement {

  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  private String id;
  @Indexed
  private Providers provider;
  private String project;
  private String key;
  private String description;
  private String group;
  private String region;
  // do not send that to frontend
  @JsonIgnore
  private Object object;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  private Date includedDate;

  private Boolean enabled;

  public ProviderElement() {
    includedDate = new Date();
  }

}
