package cloudos.provider;

import cloudos.Providers;
import cloudos.exceptions.NotImplementedException;
import cloudos.keys.cloudoskey.CloudosKey;
import cloudos.models.CloudosSecurityGroupRepository;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ProvidersService {

  @Resource
  private GoogleProviderManagement googleResourcesManagement;
  @Resource
  private AmazonProviderManagement amazonResourcesManagement;
  @Resource
  private CloudosSecurityGroupRepository cloudosSecurityGroupRepository;

  /**
   * Get management class.
   *
   * @param providers the name of the provider
   * @return a provider manager
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  @SuppressWarnings("Duplicates")
  private ProviderManagement getManagement(Providers providers) throws NotImplementedException {
    switch (providers) {
      case AMAZON_AWS:
        return amazonResourcesManagement;
      case GOOGLE_COMPUTE_ENGINE:
        return googleResourcesManagement;
      default:
        break;
    }
    throw new NotImplementedException();
  }

  /**
   * Retrieve a map with all regions by providers.
   *
   * @return a map between the provider and its regions
   */
  public Map<Providers, List<ProviderRegion>> retrieveRegions() {
    Map<Providers, List<ProviderRegion>> values = new HashMap<>();
    for (Providers provider : retrieveSupportedProviders()) {
      try {
        log.info("getting regions: " + provider);
        values.put(provider, getManagement(provider).retrieveRegions());
      } catch (NotImplementedException e) {
        // ignore it
        values.put(provider, Collections.emptyList());
      }
    }
    return values;
  }

  /**
   * Retrieve regions of a provider.
   *
   * @param provider the name of the provider
   * @return a list containing all the regions for the provider
   * @throws NotImplementedException if it hasn't been implemented for the given provider
   */
  public List<ProviderRegion> retrieveRegions(Providers provider) throws NotImplementedException {
    return getManagement(provider).retrieveRegions();
  }

  /**
   * Retrieve a map with all regions by providers.
   *
   * @return a map between the provider and its regions
   */
  public Map<Providers, List<ProviderRegion>> retrieveRegionsWithZones() {
    Map<Providers, List<ProviderRegion>> values = new HashMap<>();
    for (Providers provider : retrieveSupportedProviders()) {
      try {
        log.info("getting regions: " + provider);
        values.put(provider, getManagement(provider).retrieveRegionsWithZones());
      } catch (NotImplementedException e) {
        // ignore it
        values.put(provider, Collections.emptyList());
      }
    }
    return values;
  }

  /**
   * Retrieve regions of a provider.
   *
   * @param provider the name of the provider
   * @return a list containing all the regions for the provider
   * @throws NotImplementedException if it hasn't been implemented for the given provider
   */
  public List<ProviderRegion> retrieveRegionsWithZones(Providers provider)
      throws NotImplementedException {
    return getManagement(provider).retrieveRegionsWithZones();
  }

  /**
   * Retrieve a map with all images by providers.
   *
   * @return a map between the provider and its supported images
   */
  public Map<Providers, List<ProviderImageType>> retrieveImages() {
    Map<Providers, List<ProviderImageType>> values = new HashMap<>();
    for (Providers provider : retrieveSupportedProviders()) {
      try {
        log.info("getting images: " + provider);
        values.put(provider, getManagement(provider).retrieveImagesTypes());
      } catch (NotImplementedException e) {
        // ignore it
        values.put(provider, Collections.emptyList());
      }
    }
    return values;
  }

  /**
   * Retrieve images of a provider.
   *
   * @param provider the name of the provider
   * @return a list containing all the images for the provider
   * @throws NotImplementedException if it hasn't been implemented for the given provider
   */
  public List<ProviderImageType> retrieveImages(Providers provider) throws NotImplementedException {
    return getManagement(provider).retrieveImagesTypes();
  }

  /**
   * Retrieve a map with all machineTypes by providers.
   *
   * @return a map between the provider and the instance (machine) types supported
   */
  public Map<Providers, List<ProviderMachineType>> retrieveMachineTypes() {
    Map<Providers, List<ProviderMachineType>> values = new HashMap<>();
    for (Providers provider : retrieveSupportedProviders()) {
      try {
        log.info("getting machinetypes: " + provider);
        values.put(provider, getManagement(provider).retrieveMachineTypes());
      } catch (NotImplementedException e) {
        // ignore it
        values.put(provider, Collections.emptyList());
      }
    }
    return values;
  }

  /**
   * Retrieve machineTypes of a provider.
   *
   * @param provider the name of the provider
   * @return a list containing all the machine types for the provider
   * @throws NotImplementedException if it hasn't been implemented for the given provider
   */
  public List<ProviderMachineType> retrieveMachineTypes(Providers provider)
      throws NotImplementedException {
    return getManagement(provider).retrieveMachineTypes();
  }

  /**
   * Retrieve keys of a provider.
   *
   * @param provider the name of the provider
   * @return a list containing all the keys for the provider
   * @throws NotImplementedException if it hasn't been implemented for the given provider
   */
  public List<ProviderKey> retrieveKeys(Providers provider) throws NotImplementedException {
    return getManagement(provider).retrieveKeys();
  }

  /**
   * Retrieve security groups of a provider.
   *
   * @param provider the name of the provider
   * @return a list containing all the security groups for the provider
   * @throws NotImplementedException if it hasn't been implemented for the given provider
   */
  public List<ProviderSecurityGroup> retrieveSecurityGroups(Providers provider)
      throws NotImplementedException {
    return getManagement(provider).retrieveSecurityGroups();
  }

  /**
   * Retrieve the machine type for the provider and key.
   * 
   * @param provider of machine
   * @param machineType key
   * @param region of machine
   * @return ProviderMachineType
   * @throws NotImplementedException if it hasn't been implemented for the given provider
   */
  public ProviderMachineType retrieveMachineType(Providers provider, String machineType,
      String region) throws NotImplementedException {
    return getManagement(provider).retrieveMachineType(machineType, region);
  }

  /**
   * Force reload of the keys.
   * 
   * @param provider of keys
   * @param savedKey new key
   * @throws NotImplementedException if it hasn't been implemented for the given provider
   */
  public void reloadKeys(Providers provider, CloudosKey savedKey) throws NotImplementedException {
    getManagement(provider).updateKeys(savedKey);
  }

  /**
   * Retrieve the list of providers supported by the application.
   * 
   * @return
   */
  public List<Providers> retrieveSupportedProviders() {
    return Arrays.asList(Providers.AMAZON_AWS, Providers.GOOGLE_COMPUTE_ENGINE);
  }
}
