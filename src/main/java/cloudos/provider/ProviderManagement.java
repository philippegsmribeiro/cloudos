package cloudos.provider;

import cloudos.keys.cloudoskey.CloudosKey;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Provider - Resources Management for operations about the resources, like machinetypes,
 * imagetypes, regions, etc.
 */
public interface ProviderManagement {

  /**
   * Retrieve all machine types available.
   *
   * @return a list containing all the machine types
   */
  List<ProviderMachineType> retrieveMachineTypes();

  /**
   * Retrieve all image types available.
   *
   * @return a list containing all the image types
   */
  List<ProviderImageType> retrieveImagesTypes();

  /**
   * Retrieve all regions available with zones.
   *
   * @return a list containing all the regions
   */
  List<ProviderRegion> retrieveRegionsWithZones();

  /**
   * Retrieve all regions.
   *
   * @return a list containing all the regions
   */
  List<ProviderRegion> retrieveRegions();

  /**
   * Retrieve all keys available.
   *
   * @return a list containing all the keys
   */
  List<ProviderKey> retrieveKeys();

  /**
   * Retrieve all security groups available.
   *
   * @return a list containing all the security groups
   */
  List<ProviderSecurityGroup> retrieveSecurityGroups();

  /**
   * Sort the list for returning.
   *
   * @param list a list of machine types
   * @return a sorted list of machine types
   */
  default List<ProviderMachineType> sortMachineTypes(List<ProviderMachineType> list) {
    Collections.sort(list, new Comparator<ProviderMachineType>() {
      @Override
      public int compare(ProviderMachineType o1, ProviderMachineType o2) {
        int compare = (o1.getCpu() != null ? o1.getCpu().compareTo(o2.getCpu()) : 0);
        if (compare == 0) {
          compare = o1.getDescription().compareTo(o2.getDescription());
        }
        return compare;
      }
    });
    return list;
  }

  /**
   * Sort the list for returning.
   *
   * @param list a list of image types
   * @return a sorted list of image types
   */
  default List<ProviderImageType> sortImageTypes(List<ProviderImageType> list) {
    Collections.sort(list, new Comparator<ProviderImageType>() {
      @Override
      public int compare(ProviderImageType o1, ProviderImageType o2) {
        int compare = (o1.getGroup() != null ? o1.getGroup().compareTo(o2.getGroup()) : 0);
        if (compare == 0) {
          compare = o1.getDescription().compareTo(o2.getDescription());
        }
        return compare;
      }
    });
    return list;
  }

  /**
   * Sort the list for returning.
   *
   * @param list a list of regions
   * @return a sorted list of regions
   */
  default List<ProviderRegion> sortRegions(List<ProviderRegion> list) {
    Collections.sort(list, new Comparator<ProviderRegion>() {
      @Override
      public int compare(ProviderRegion o1, ProviderRegion o2) {
        return o1.getDescription().compareTo(o2.getDescription());
      }
    });
    return list;
  }

  /** Sync the provider regions on cloudOS with the ones from the provider. */
  void syncRegions();

  /** Sync the provider machine types on cloudOS with the ones from the provider. */
  void syncMachineTypes();

  /** Sync the provider image types on cloudOS with the ones from the provider. */
  void syncImagesTypes();

  /** Sync the provider keys on cloudOS with the ones from the provider. */
  void syncKeys();

  /** Sync the provider security groups on cloudOS with the ones from the provider. */
  void syncSecurityGroups();

  /** Discover the instances of the account. */
  default void discovery() {
    // force re-sync
    syncImagesTypes();
    syncKeys();
    syncMachineTypes();
    syncRegions();
    syncSecurityGroups();
  }

  /**
   * Retrieve the machine type by key.
   * 
   * @param machineType key
   * @param region location of machine
   * @return providermachinetype
   */
  ProviderMachineType retrieveMachineType(String machineType, String region);

  /**
   * Update the provider keys with a new (created) key.
   * @param cloudosKey new key
   */
  void updateKeys(CloudosKey cloudosKey);
}
