package cloudos.provider;

import cloudos.Providers;
import java.util.Date;
import lombok.Builder;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "provider_key")
public class ProviderKey extends ProviderElement {

  @Builder
  public ProviderKey(String id, Providers provider, String project, String key, String description,
      String group, String region, Object object, Date includedDate, Boolean enabled) {
    super(id, provider, project, key, description, group, region, object, includedDate, enabled);
  }
  
}
