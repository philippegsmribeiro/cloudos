package cloudos.provider;

import cloudos.AmazonService;
import cloudos.Providers;
import cloudos.amazon.AmazonImageType;
import cloudos.amazon.AmazonInstanceDistros;
import cloudos.amazon.AmazonMachineType;
import cloudos.amazon.AmazonRegion;
import cloudos.config.ThreadingConfig;
import cloudos.instances.AmazonInstanceManagement;
import cloudos.keys.AmazonKeyManagement;
import cloudos.keys.cloudoskey.CloudosKey;
import cloudos.models.Instance;
import cloudos.utils.FileUtils;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.model.AvailabilityZone;
import com.amazonaws.services.ec2.model.DescribeImagesRequest;
import com.amazonaws.services.ec2.model.DescribeImagesResult;
import com.amazonaws.services.ec2.model.Image;
import com.amazonaws.services.ec2.model.KeyPairInfo;
import com.amazonaws.services.ec2.model.Region;
import com.amazonaws.services.ec2.model.SecurityGroup;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AmazonProviderManagement implements ProviderManagement {

  private static final String MEMORY2 = "memory";
  private static final String VCPU2 = "vcpu";
  private static final String INSTANCE_FAMILY = "instanceFamily";
  private static final String INSTANCE_TYPE = "instanceType";
  private static final String ATTRIBUTES = "attributes";
  private static final String COMPUTE_INSTANCE = "Compute Instance";
  private static final String PRODUCT_FAMILY = "productFamily";
  private static final String PRODUCTS = "products";
  private static final String CURRENT_VERSION_URL = "currentVersionUrl";
  private static final String REGION_CODE = "regionCode";
  private static final String REGIONS = "regions";
  private static final String ROOT = "root";

  private final Logger logger = LoggerFactory.getLogger(AmazonInstanceManagement.class);

  private Gson gson = new Gson();

  @Autowired
  AmazonService amazonService;
  @Autowired
  ProviderRegionRepository regionRepository;
  @Autowired
  ProviderKeyRepository keyRepository;
  @Autowired
  ProviderSecurityGroupRepository securityGroupRepository;
  @Autowired
  ProviderImageTypeRepository imageTypeRepository;
  @Autowired
  ProviderMachineTypeRepository machineTypeRepository;
  @Autowired
  AmazonKeyManagement amazonKeyManagement;
  @Autowired
  @Qualifier(ThreadingConfig.MONITOR)
  AsyncTaskExecutor taskExecutor;

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.ResourcesManagement#retrieveMachineTypes()
   */
  @Override
  public List<cloudos.provider.ProviderMachineType> retrieveMachineTypes() {
    List<ProviderMachineType> findByProvider =
        machineTypeRepository.findByProvider(Providers.AMAZON_AWS);
    if (CollectionUtils.isNotEmpty(findByProvider)) {
      return sortMachineTypes(findByProvider);
    }
    syncMachineTypes();
    // TODO: fix test
    findByProvider = machineTypeRepository.findByProvider(Providers.AMAZON_AWS);
    if (CollectionUtils.isNotEmpty(findByProvider)) {
      return sortMachineTypes(findByProvider);
    }
    return Collections.emptyList();
  }

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.ProviderManagement#syncMachineTypes()
   */
  @Override
  public void syncMachineTypes() {
    Map<String, String> urls = getUrls();
    if (MapUtils.isNotEmpty(urls)) {
      for (Entry<String, String> entry : urls.entrySet()) {

        String region = entry.getKey();
        List<cloudos.provider.ProviderMachineType> list =
            readMachineTypesFromJson(region, entry.getValue());
        if (CollectionUtils.isNotEmpty(list)) {
          List<ProviderMachineType> findByProvider =
              machineTypeRepository.findByProviderAndRegion(Providers.AMAZON_AWS, region);
          if (CollectionUtils.isNotEmpty(findByProvider)) {
            machineTypeRepository.delete(findByProvider);
          }
          try {
            machineTypeRepository.save(list);
          } catch (DuplicateKeyException e) {
            // if data already there... ignore error.
            log.warn("Duplicate key: {}", list);
          }
        }
      }
    }
  }

  private static final String OFFERS_PRICING_BASE_URL = "https://pricing.us-east-1.amazonaws.com";
  private static final String AMAZON_EC2_URL =
      "/offers/v1.0/aws/AmazonEC2/current/region_index.json";

  /**
   * Get the regions urls from the aws json.
   *
   * @return Map with the regions and urls
   */
  private Map<String, String> getUrls() {
    Map<String, String> regionUrlsMap = new HashMap<>();
    try {
      InputStreamReader result =
          cloudos.utils.FileUtils.readUrlContentToReader(OFFERS_PRICING_BASE_URL + AMAZON_EC2_URL);
      Map<?, ?> fromJson = gson.fromJson(result, Map.class);
      Map<?, ?> regionsMap = (Map<?, ?>) fromJson.get(REGIONS);
      Iterator<?> iterator = regionsMap.values().iterator();
      while (iterator.hasNext()) {
        Map<?, ?> object = (Map<?, ?>) iterator.next();
        String region = (String) object.get(REGION_CODE);
        String regionUrl = (String) object.get(CURRENT_VERSION_URL);
        regionUrlsMap.put(region, regionUrl);
      }
    } catch (Exception e) {
      logger.error("Error extracting the machine types from json url", e);
    }
    return regionUrlsMap;
  }


  /**
   * Read the machine types from the json of offers.
   *
   * @param region aws
   * @param regionUrl json
   * @return list of ProviderMachineType
   */
  public List<ProviderMachineType> readMachineTypesFromJson(String region, String regionUrl) {
    List<String> listB = new ArrayList<>();
    List<ProviderMachineType> list = new ArrayList<>();
    try {
      InputStreamReader resultRegion =
          cloudos.utils.FileUtils.readUrlContentToReader(OFFERS_PRICING_BASE_URL + regionUrl);
      Map<?, ?> fromJsonRegion = gson.fromJson(resultRegion, Map.class);
      Iterator<?> iterator2 = ((Map<?, ?>) fromJsonRegion.get(PRODUCTS)).values().iterator();
      while (iterator2.hasNext()) {
        Map<?, ?> object2 = (Map<?, ?>) iterator2.next();
        String object3 = (String) object2.get(PRODUCT_FAMILY);
        // String sku = (String) object2.get("sku");
        if (COMPUTE_INSTANCE.equals(object3)) {
          try {
            Map<?, ?> mapAttibutes = (Map<?, ?>) object2.get(ATTRIBUTES);
            // String location = (String) mapAttibutes.get("location");
            // String locationType = (String) mapAttibutes.get("locationType");
            String instanceType = (String) mapAttibutes.get(INSTANCE_TYPE);
            // String currentGeneration = (String) mapAttibutes.get("currentGeneration");
            String instanceFamily = (String) mapAttibutes.get(INSTANCE_FAMILY);
            String vcpu = (String) mapAttibutes.get(VCPU2);
            // String physicalProcessor = (String) mapAttibutes.get("physicalProcessor");
            // String clockSpeed = (String) mapAttibutes.get("clockSpeed");
            String memory = (String) mapAttibutes.get(MEMORY2);
            // String storage = (String) mapAttibutes.get("storage");
            // String networkPerformance = (String) mapAttibutes.get("networkPerformance");
            // String processorArchitecture = (String) mapAttibutes.get("processorArchitecture");
            // String tenancy = (String) mapAttibutes.get("tenancy");
            // String operatingSystem = (String) mapAttibutes.get("operatingSystem");
            // String licenseModel = (String) mapAttibutes.get("licenseModel");
            // String usagetype = (String) mapAttibutes.get("usagetype");
            // String operation = (String) mapAttibutes.get("operation");
            // String ecu = (String) mapAttibutes.get("ecu");
            // String enhancedNetworkingSupported =
            // (String) mapAttibutes.get("enhancedNetworkingSupported");
            // String normalizationSizeFactor = (String)
            // mapAttibutes.get("normalizationSizeFactor");
            // String preInstalledSw = (String) mapAttibutes.get("preInstalledSw");
            // String processorFeatures = (String) mapAttibutes.get("processorFeatures");
            AmazonMachineType machineType = new AmazonMachineType();
            machineType.setGroup(instanceFamily);
            machineType.setKey(instanceType);
            machineType.setObject(mapAttibutes);
            machineType.setCpu(Double.parseDouble(vcpu));
            machineType.setMemory(Double.parseDouble(memory.split(" ")[0].replace(",", "")));
            machineType.setRegion(region);
            machineType.setDescription(
                String.format("vCPUs: %s - Memory: %s - %s", vcpu, memory, instanceType));
            String key =
                String.format("#%s#%s", machineType.getDescription(), machineType.getGroup());
            if (!listB.contains(key)) {
              list.add(machineType);
              listB.add(key);
            }
          } catch (Exception e) {
            logger.error("Error parsing this machine type: {}", object2, e);
          }
        }
      }
    } catch (Exception e) {
      logger.error("Error extracting the machine types from json url", e);
    }
    return list;
  }

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.ResourcesManagement#retrieveImagesTypes()
   */
  @Override
  public List<cloudos.provider.ProviderImageType> retrieveImagesTypes() {
    List<ProviderImageType> findByProvider =
        imageTypeRepository.findByProvider(Providers.AMAZON_AWS);
    if (CollectionUtils.isNotEmpty(findByProvider)) {
      return sortImageTypes(findByProvider);
    }
    syncImagesTypes();
    findByProvider = imageTypeRepository.findByProvider(Providers.AMAZON_AWS);
    if (CollectionUtils.isNotEmpty(findByProvider)) {
      return sortImageTypes(findByProvider);
    }
    return Collections.emptyList();
  }

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.ProviderManagement#syncImagesTypes()
   */
  @Override
  public void syncImagesTypes() {
    // get self owned images
    Iterable<Region> listRegions =
        amazonService.getClient(Regions.US_EAST_1.getName()).describeRegions().getRegions();
    List<ProviderImageType> list = new ArrayList<>();


    for (Region region : listRegions) {
      DescribeImagesRequest describeImagesRequest = new DescribeImagesRequest();
      describeImagesRequest.withOwners("self");
      DescribeImagesResult listOwnImages =
          amazonService.getClient(region.getRegionName()).describeImages(describeImagesRequest);
      for (Image image : listOwnImages.getImages()) {
        AmazonImageType imageType = new AmazonImageType();
        imageType
            .setDescription(StringUtils.isNotEmpty(image.getDescription()) ? image.getDescription()
                : image.getName());
        imageType.setGroup(imageType.getDescription().split(" ")[0]);
        imageType.setKey(image.getImageId());
        // imageType.setObject(image);
        imageType.setRegion(region.getRegionName());
        list.add(imageType);
      }
    }
    // get suggested amis from the json file
    list.addAll(readAmisFromJson());
    if (CollectionUtils.isNotEmpty(list)) {
      List<ProviderImageType> findByProvider =
          imageTypeRepository.findByProvider(Providers.AMAZON_AWS);
      if (CollectionUtils.isNotEmpty(findByProvider)) {
        imageTypeRepository.delete(findByProvider);
      }
      try {
        imageTypeRepository.save(list);
      } catch (DuplicateKeyException e) {
        // if data already there... ignore error.
        log.warn("Duplicate key: {}", list);
      }
    }
  }

  /** Get ami from the json file TODO: Should be changed to get from mongo. */
  private List<ProviderImageType> readAmisFromJson() {
    List<ProviderImageType> list = new ArrayList<>();
    try {
      byte[] readAllBytes = Files.readAllBytes(Paths.get(FileUtils
          .getFileFromResourcesFile("aws/amis.json", this.getClass().getClassLoader()).toURI()));
      List<?> fromJsonRegion =
          (List<?>) new Gson().fromJson(new String(readAllBytes), Object.class);
      for (Object object : fromJsonRegion) {
        logger.debug("########## Reading \n" + object.toString());
        List<?> amilist = (List<?>) ((LinkedTreeMap<?, ?>) object).get("amiList");
        String region = (String) ((LinkedTreeMap<?, ?>) object).get("region");
        for (Object object2 : amilist) {
          String ami = (String) ((LinkedTreeMap<?, ?>) object2).get("imageId64");
          String title = (String) ((LinkedTreeMap<?, ?>) object2).get("title");
          String platform = (String) ((LinkedTreeMap<?, ?>) object2).get("platform");
          // String description = (String) ((LinkedTreeMap<?, ?>)
          // object2).get("description");
          AmazonImageType imageType = new AmazonImageType();
          imageType.setDescription(title);
          imageType.setGroup(platform.substring(0, 1).toUpperCase() + platform.substring(1));
          imageType.setKey(ami);
          // imageType.setObject(object2);
          imageType.setRegion(region);
          list.add(imageType);
        }
        logger.debug(object.toString());
      }
    } catch (Exception e) {
      // TODO: handle exception
      logger.error("Error reading json", e);
    }
    return list;
  }

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.ResourcesManagement#retrieveRegions()
   */
  @Override
  public List<cloudos.provider.ProviderRegion> retrieveRegionsWithZones() {
    List<cloudos.provider.ProviderRegion> findByProvider =
        regionRepository.findByProvider(Providers.AMAZON_AWS);
    if (CollectionUtils.isNotEmpty(findByProvider)) {
      return sortRegions(findByProvider);
    }
    // if not found - force a sync
    syncRegions();
    // try to get again
    findByProvider = regionRepository.findByProvider(Providers.AMAZON_AWS);
    //
    if (CollectionUtils.isNotEmpty(findByProvider)) {
      return sortRegions(findByProvider);
    }
    return Collections.emptyList();
  }

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.ProviderManagement#retrieveRegions()
   */
  @Override
  public List<ProviderRegion> retrieveRegions() {
    List<cloudos.provider.ProviderRegion> retrieveRegions = this.retrieveRegionsWithZones();
    // group by regions
    Map<String, ProviderRegion> map = new HashMap<>();
    for (ProviderRegion providerRegion : retrieveRegions) {
      String region = providerRegion.getGroup();
      if (!map.containsKey(region)) {
        ProviderRegion groupRegion = ProviderRegion.builder()
                                                   .region(region)
                                                   .description(region)
                                                   .key(region)
                                                   .provider(Providers.AMAZON_AWS)
                                                   .build();
        map.put(region, groupRegion);
      }
    }
    return new ArrayList<>(map.values());
  }

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.ProviderManagement#syncRegions()
   */
  @Override
  public void syncRegions() {
    // go to the provider
    Iterable<Region> listRegions =
        amazonService.getClient(Regions.US_EAST_1.getName()).describeRegions().getRegions();
    List<cloudos.provider.ProviderRegion> list = new ArrayList<>();
    for (Region region : listRegions) {
      //
      List<AvailabilityZone> describeAvailabilityZones = amazonService
          .getClient(region.getRegionName()).describeAvailabilityZones().getAvailabilityZones();
      for (AvailabilityZone availabilityZone : describeAvailabilityZones) {
        AmazonRegion imageType = new AmazonRegion();
        imageType.setGroup(region.getRegionName());
        imageType.setDescription(availabilityZone.getZoneName());
        imageType.setKey(availabilityZone.getZoneName());
        imageType.setObject(availabilityZone);
        imageType.setRegion(availabilityZone.getRegionName());
        list.add(imageType);
      }
    }
    if (CollectionUtils.isNotEmpty(list)) {
      // delete the old ones
      List<ProviderRegion> findByProvider = regionRepository.findByProvider(Providers.AMAZON_AWS);
      regionRepository.delete(findByProvider);
      // save the new ones
      try {
        regionRepository.save(list);
      } catch (DuplicateKeyException e) {
        // if data already there... ignore error.
        log.warn("Duplicate key: {}", list);
      }
    }
  }

  /** Auxiliary method to get a regions from a zone. */
  public cloudos.provider.ProviderRegion getRegionFromZone(String zone) {
    ProviderRegion findByProviderAndKey =
        regionRepository.findByProviderAndKey(Providers.AMAZON_AWS, zone);
    if (findByProviderAndKey == null) {
      syncRegions();
      findByProviderAndKey = regionRepository.findByProviderAndKey(Providers.AMAZON_AWS, zone);
    }
    return findByProviderAndKey;
  }

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.ProviderManagement#retrieveKeys()
   */
  @Override
  public List<ProviderKey> retrieveKeys() {
    List<ProviderKey> findByProvider = keyRepository.findByProvider(Providers.AMAZON_AWS);
    if (CollectionUtils.isNotEmpty(findByProvider)) {
      return amazonKeyManagement.classifyAccordingPrivateKey(findByProvider);
    }
    //
    syncKeys();
    findByProvider = keyRepository.findByProvider(Providers.AMAZON_AWS);
    if (CollectionUtils.isNotEmpty(findByProvider)) {
      return amazonKeyManagement.classifyAccordingPrivateKey(findByProvider);
    }
    return Collections.emptyList();
  }

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.ProviderManagement#syncKeys()
   */
  @Override
  public void syncKeys() {
    List<ProviderKey> list = new ArrayList<>();
    Iterable<Region> listPublicImages =
        amazonService.getClient(Regions.US_EAST_1.getName()).describeRegions().getRegions();
    for (Region image : listPublicImages) {
      //
      List<KeyPairInfo> providerList =
          amazonService.getClient(image.getRegionName()).describeKeyPairs().getKeyPairs();
      for (KeyPairInfo securityGroup : providerList) {
        ProviderKey key = ProviderKey.builder()
                                     .provider(Providers.AMAZON_AWS)
                                     .description(securityGroup.getKeyName())
                                     .key(securityGroup.getKeyName())
                                     .region(image.getRegionName())
                                     .build();
        list.add(key);
      }
    }
    if (CollectionUtils.isNotEmpty(list)) {
      List<ProviderKey> findByProvider = keyRepository.findByProvider(Providers.AMAZON_AWS);
      if (CollectionUtils.isNotEmpty(findByProvider)) {
        keyRepository.delete(findByProvider);
      }
      try {
        List<ProviderKey> saved = keyRepository.save(list);
        amazonKeyManagement.updateKeys(saved);
      } catch (DuplicateKeyException e) {
        // if data already there... ignore error.
        log.warn("Duplicate key: {}", list);
      }
    }
  }

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.ProviderManagement#retrieveSecurityGroups()
   */
  @Override
  public List<ProviderSecurityGroup> retrieveSecurityGroups() {
    List<ProviderSecurityGroup> findByProvider =
        securityGroupRepository.findByProvider(Providers.AMAZON_AWS);
    if (CollectionUtils.isNotEmpty(findByProvider)) {
      return findByProvider;
    }
    syncSecurityGroups();
    findByProvider = securityGroupRepository.findByProvider(Providers.AMAZON_AWS);
    if (CollectionUtils.isNotEmpty(findByProvider)) {
      return findByProvider;
    }
    return Collections.emptyList();
  }

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.ProviderManagement#syncSecurityGroups()
   */
  @Override
  public void syncSecurityGroups() {
    List<ProviderSecurityGroup> list = new ArrayList<>();
    Iterable<Region> listPublicImages =
        amazonService.getClient(Regions.US_EAST_1.getName()).describeRegions().getRegions();
    for (Region image : listPublicImages) {
      //
      List<SecurityGroup> providerList = amazonService.getClient(image.getRegionName())
          .describeSecurityGroups().getSecurityGroups();
      for (SecurityGroup sg : providerList) {
        ProviderSecurityGroup securityGroup = ProviderSecurityGroup.builder()
                                                                   .provider(Providers.AMAZON_AWS)
                                                                   .description(sg.getGroupName())
                                                                   .key(sg.getGroupId())
                                                                   .region(image.getRegionName())
                                                                   .build();
        list.add(securityGroup);
      }
    }
    if (CollectionUtils.isNotEmpty(list)) {
      List<ProviderSecurityGroup> findByProvider =
          securityGroupRepository.findByProvider(Providers.AMAZON_AWS);
      if (CollectionUtils.isNotEmpty(findByProvider)) {
        securityGroupRepository.delete(findByProvider);
      }
      try {
        securityGroupRepository.save(list);
      } catch (DuplicateKeyException e) {
        // if data already there... ignore error.
        log.warn("Duplicate key: {}", list);
      }
    }
  }

  /** Retrieve the username, based on the machine type rules. */
  public String retrieveUserBasedOnTheMachineTypeForInstance(Instance instance) {
    ProviderImageType findByKey = imageTypeRepository.findByKey(instance.getImageType());
    AmazonInstanceDistros distro = null;
    if (findByKey == null) {
      // get on the aws
      DescribeImagesRequest describeImagesRequest = new DescribeImagesRequest();
      describeImagesRequest.withImageIds(instance.getImageType());
      DescribeImagesResult listOwnImages =
          amazonService.getClient(instance.getRegion()).describeImages(describeImagesRequest);
      for (Image image : listOwnImages.getImages()) {
        String description = StringUtils.isNotEmpty(image.getDescription()) ? image.getDescription()
            : image.getName();
        distro = AmazonInstanceDistros.findByType(description.split(" ")[0].toLowerCase());
      }
    } else {
      distro = AmazonInstanceDistros.findByType(findByKey.getGroup().toLowerCase());
    }
    if (distro != null) {
      return distro.getUser();
    }
    return ROOT;
  }

  @Override
  public ProviderMachineType retrieveMachineType(String machineType, String region) {
    if (machineTypeRepository.countByProvider(Providers.AMAZON_AWS) == 0) {
      syncMachineTypes();
    }
    return machineTypeRepository.findByKeyAndProviderAndRegion(machineType, Providers.AMAZON_AWS,
        region);
  }

  @Override
  public void updateKeys(CloudosKey cloudosKey) {
    ProviderKey key = ProviderKey.builder()
                                 .provider(Providers.AMAZON_AWS)
                                 .description(cloudosKey.getKeyName())
                                 .key(cloudosKey.getKeyName())
                                 .region(cloudosKey.getRegion())
                                 .enabled(true)
                                 .build();
    keyRepository.save(key);
  }
}
