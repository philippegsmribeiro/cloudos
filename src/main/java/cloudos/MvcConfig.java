package cloudos;

import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import cloudos.utils.export.converter.CsvHttpMessageConverter;

@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {

  @Override
  public void addViewControllers(ViewControllerRegistry registry) {
    // forward the / to the index.html of the static dir
    registry.addViewController("/").setViewName("forward:/index.html");
    registry.addViewController("/index").setViewName("forward:/index.html");
  } 

  @Override
  public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
    converters.add(new CsvHttpMessageConverter<>());
  }

}
