package cloudos.insights;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "insights_settings")
public class InsightsSettings {
  
  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  private String id;
  
  private boolean isReadOnlyMode;
  
  private ResizingRecommendationPolicy resizingRecommendationPolicy;

}
