package cloudos.insights;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "cloudos_insights_message")
public class InsightsMessage {

  @Id
  private String id;
  private String message;
  private String hash;
  private Date timestamp;

}
