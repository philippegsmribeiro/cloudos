package cloudos.insights;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface InsightsSettingsRepository
    extends MongoRepository<InsightsSettings, String> {

}
