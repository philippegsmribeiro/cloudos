package cloudos.insights;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;

import cloudos.machinelearning.MLService;
import cloudos.models.Metricbeat;
import cloudos.models.elasticsearch.Filesystem;
import cloudos.models.elasticsearch.Process;
import cloudos.provider.ProviderMachineType;
import cloudos.provider.ProvidersService;
import cloudos.queue.QueueListener;
import cloudos.queue.QueueManagerService;
import cloudos.queue.message.beats.MetricbeatMessage;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class ResizingRecommendationService {

  @Autowired
  private QueueListener queueListener;

  @Autowired
  private InsightsService insightsService;

  @Autowired
  private QueueManagerService queueManagerService;

  @Autowired
  private MLService mlService;

  @Autowired
  private MetricbeatRepository metricbeatRepository;

  @Autowired
  private ProvidersService providersService;

  @Autowired
  private InsightsRepository insightsRepository;

  // the metric beat timestamp string pattern
  private static String PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

  private static DateFormat df = new SimpleDateFormat(PATTERN);

  private static String SHA_256 = "SHA-256";

  private static int TIME_WINDOW = 4;

  // define the JSON deserializer
  private static JsonDeserializer<Date> dateJsonDeserializer = (json, typeOfT, context) -> {
    try {
      return json == null ? null : df.parse(json.getAsString());
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return null;
  };

  private static Gson gson =
      new GsonBuilder().registerTypeAdapter(Date.class, dateJsonDeserializer).create();

  /**
   * Set up the listener that will consume the metric beats data points. They will be used to
   * determine the resizing recommendations to be adopted.
   */
  @PostConstruct
  public void setUpListener() {
    // add listener to the queue
    queueListener.addConsumer((MetricbeatMessage.MetricbeatMessageConsumer) metricbeatMessage -> {
      log.info("********* Consuming Metricbeat message message {}", metricbeatMessage);
      String jsonHit = metricbeatMessage.getHitJson();
      log.info("===========================================");
      // convert the metricbeat JSON into a Metricbeat object
      Metricbeat metricbeat = gson.fromJson(jsonHit, Metricbeat.class);
      // always save the metricbeat for model trainings
      this.metricbeatRepository.save(metricbeat);

      switch (MetricbeatModules.lookupByName(metricbeat.metricset.module)) {
        case SYSTEM:
          this.processSystemMetricset(metricbeat);
          break;
        default:
          log.warn("Module {} is not supported", metricbeat.metricset.module);
          break;
      }
      log.info("===========================================");
    });
  }

  /**
   * Based on the information given by the System metric beat, we will analyze the needs of the
   * given instance. As of now, we will generate notification messages based on the information
   * obtained.
   *
   * @param metricbeat the metricbeat entry to be analyzed
   */
  public void processSystemMetricset(@NotNull Metricbeat metricbeat) {
    // For now, we will only support insights for Process, CPU, Memory and Filesystem.
    log.info("Metricbeat: {}", metricbeat);
    switch (MetricbeatSystemModules.lookupByName(metricbeat.metricset.name)) {
      case PROCESS:
        // analyze the process metric
        this.analyzeProcess(metricbeat);
        break;
      case CPU:
        // analyze the CPU metric
        this.analyzeCPU(metricbeat);
        break;
      case MEMORY:
        // analyze the memory metric
        this.analyzeMemory(metricbeat);
        break;
      case FILESYSTEM:
        // analyze the filesystem
        this.analyzeFileSystem(metricbeat);
        break;
      case CORE:
        // analyze the Core metric
      case LOAD:
        // analyze the Load metric
      case RAID:
        // analyze the RAID metric
      case DISKIO:
        // analyze the DiskIO metric
      case FSSTAT:
        // analyze the fsstat metric
      case SOCKET:
        // analyze the socket metric
      case UPTIME:
        // analyze the update metric
      case NETWORK:
        // analyze the network metric
      case PROCESS_SUMMARY:
        // analyze the process summary metric
      default:
        log.warn("Metricset {} not supported", metricbeat.metricset.name);
        break;
    }
  }

  /**
   * Analyze the CPU entry. In case the CPU usage is above a certain threshold, an alert
   * notification will be generated.
   *
   * @param metricbeat the metricbeat entry
   */
  public void analyzeCPU(@NotNull Metricbeat metricbeat) {
    // for now, simply create a new message based on the input given
    double cpu_usage = metricbeat.system.cpu.system.pct + metricbeat.system.cpu.user.pct;
    String score = this.formatMessage(cpu_usage);
    if (cpu_usage >= this.insightsService.getResizingRecommendationPolicy().getCpuUpperBound()) {
      // create a new CPU Insight message
      String message = String.format("Instance %s has a high CPU utilization at %s.",
          metricbeat.beat.name, score);
      ProviderMachineType currentMachine = this.mlService.getMachineType(metricbeat.beat.name);
      ProviderMachineType machineType = this.mlService.findMachineTypeBound(metricbeat.beat.name,
          false, true);
      if (currentMachine != null && machineType != null) {
        double costSavings = this.mlService.getPriceDifference(currentMachine, machineType);
        if (costSavings > 0) {
          message = message + String.format(" Consider using the instance type %s instead." +
                  "You could be saving %.02f%s by switching.",
              machineType.getKey(), costSavings, "%");
        }
      }
      String hash = this.hashMessage(message);
      InsightsMessage insightsMessage = this.insightsRepository.findByHash(hash);
      if (insightsMessage == null || this.isFourhoursOld(insightsMessage.getTimestamp())) {
        // create a new entry and send it to the user
        this.saveInsightMessage(message, hash);
        this.queueManagerService.sendCpuInsightMessage(message);
      }
    } else if (cpu_usage <= this.insightsService
        .getResizingRecommendationPolicy().getCpuLowerBound()) {
      // create a low CPU utilization
      ProviderMachineType currentMachine = this.mlService.getMachineType(metricbeat.beat.name);
      ProviderMachineType machineType = this.mlService.findMachineTypeBound(metricbeat.beat.name,
          true, true);
      String message = String.format("Instance %s has a low CPU utilization at %s.",
          metricbeat.beat.name, score);
      if (currentMachine != null && machineType != null) {
        double costSavings = this.mlService.getPriceDifference(currentMachine, machineType);
        if (costSavings > 0) {
          message = message + String.format(" Consider using the instance type %s instead." +
                  "You could be saving %.02f%s by switching.",
              machineType.getKey(), costSavings, "%");
        }
      }
      String hash = this.hashMessage(message);
      InsightsMessage insightsMessage = this.insightsRepository.findByHash(hash);
      if (insightsMessage == null || this.isFourhoursOld(insightsMessage.getTimestamp())) {
        // create a new entry and send it to the user
        this.saveInsightMessage(message, hash);
        this.queueManagerService.sendCpuInsightMessage(message);
      }
    }
  }

  /**
   * Analyze the Memory entry. In case the Memory usage is above a certain threshold, an alert
   * notification will be generated.
   *
   * @param metricbeat the metricbeat entry
   */
  public void analyzeMemory(@NotNull Metricbeat metricbeat) {
    if (metricbeat.system.memory.used.pct >=
        this.insightsService.getResizingRecommendationPolicy().getMemoryUpperBound()) {
      // create a new message
      String score = this.formatMessage(metricbeat.system.memory.used.pct);
      String message = String.format("Instance %s has a high memory utilization at %s.",
          metricbeat.beat.name, score);
      ProviderMachineType currentMachine = this.mlService.getMachineType(metricbeat.beat.name);
      ProviderMachineType machineType = this.mlService.findMachineTypeBound(metricbeat.beat.name,
          false, false);
      log.info(this.insightsService.getResizingRecommendationPolicy().getMemoryUpperBound());
      if (currentMachine != null && machineType != null) {
        double costSavings = this.mlService.getPriceDifference(currentMachine, machineType);
        if (costSavings > 0) {
          message = message + String.format(" Consider using the instance type %s instead." +
                  "You could be saving %.02f%s by switching.",
              machineType.getKey(), costSavings, "%");
        }
      }
      String hash = this.hashMessage(message);
      InsightsMessage insightsMessage = this.insightsRepository.findByHash(hash);
      if (insightsMessage == null || this.isFourhoursOld(insightsMessage.getTimestamp())) {
        // create a new entry and send it to the user
        this.saveInsightMessage(message, hash);
        this.queueManagerService.sendMemoryInsightMessage(message);
      }
    } else if (metricbeat.system.memory.used.pct <=
        this.insightsService.getResizingRecommendationPolicy().getMemoryLowerBound()) {
      String score = this.formatMessage(metricbeat.system.memory.used.pct);
      String message = String.format("Instance %s has a low memory utilization at %s.",
          metricbeat.beat.name, score);
      ProviderMachineType currentMachine = this.mlService.getMachineType(metricbeat.beat.name);
      ProviderMachineType machineType = this.mlService.findMachineTypeBound(metricbeat.beat.name,
          true, false);
      if (currentMachine != null && machineType != null) {
        double costSavings = this.mlService.getPriceDifference(currentMachine, machineType);
        if (costSavings > 0) {
          message = message + String.format(" Consider using the instance type %s instead." +
                  "You could be saving %.02f%s by switching.",
              machineType.getKey(), costSavings, "%");
        }
      }
      String hash = this.hashMessage(message);
      InsightsMessage insightsMessage = this.insightsRepository.findByHash(hash);
      if (insightsMessage == null || this.isFourhoursOld(insightsMessage.getTimestamp())) {
        // create a new entry and send it to the user
        this.saveInsightMessage(message, hash);
        this.queueManagerService.sendMemoryInsightMessage(message);
      }
    }
  }

  /**
   * Analyze the process entry. In case the CPU or Memory usage is above a certain threshold, an
   * alert notification will be generated.
   *
   * @param metricbeat the metricbeat entry
   */
  public void analyzeProcess(@NotNull Metricbeat metricbeat) {
    Process process = metricbeat.system.process;
    if (process.cpu.total.pct >= this.insightsService.getResizingRecommendationPolicy()
        .getProcessCpuUpperBound()) {
      // create message
      String score = this.formatMessage(process.cpu.total.pct);
      String message =
          String.format("Process %s in instance %s has a high CPU utilization at %s.",
              process.name, metricbeat.beat.name, score);
      String hash = this.hashMessage(message);
      InsightsMessage insightsMessage = this.insightsRepository.findByHash(hash);
      if (insightsMessage == null || this.isFourhoursOld(insightsMessage.getTimestamp())) {
        // create a new entry and send it to the user
        this.saveInsightMessage(message, hash);
        this.queueManagerService.sendProcessInsightMessage(message);
      }
    }
    if (process.memory.rss.pct >= this.insightsService.getResizingRecommendationPolicy()
        .getProcessMemoryUpperBound()) {
      String score = this.formatMessage(process.memory.rss.pct);
      String message =
          String.format("Process %s in instance %s has a high memory utilization at %s.",
              process.name, metricbeat.beat.name, score);
      String hash = this.hashMessage(message);
      InsightsMessage insightsMessage = this.insightsRepository.findByHash(hash);
      if (insightsMessage == null || this.isFourhoursOld(insightsMessage.getTimestamp())) {
        // create a new entry and send it to the user
        this.saveInsightMessage(message, hash);
        this.queueManagerService.sendProcessInsightMessage(message);
      }
    }
  }

  /**
   * Analyze the filesystem entry. In case the usage is above a certain threshold, an alert
   * notification will be generated.
   *
   * @param metricbeat the metricbeat entry
   */
  public void analyzeFileSystem(@NotNull Metricbeat metricbeat) {
    Filesystem filesystem = metricbeat.system.filesystem;
    if (filesystem.used.pct >= this.insightsService.getResizingRecommendationPolicy()
        .getFileSystemUpperBound()) {
      // create the message ...
      String score = this.formatMessage(filesystem.used.pct);
      String message =
          String.format("Filesystem %s in instance %s has a high disk utilization at %s",
              filesystem.device_name, metricbeat.beat.name, score);
      String hash = this.hashMessage(message);
      InsightsMessage insightsMessage = this.insightsRepository.findByHash(hash);
      if (insightsMessage == null || this.isFourhoursOld(insightsMessage.getTimestamp())) {
        // create a new entry and send it to the user
        this.saveInsightMessage(message, hash);
        this.queueManagerService.sendFilesystemInsightMessage(message);
      }
    }
  }

  /**
   * Hash the message by using SHA-256, so to avoid redundancies.
   *
   * @param message the message being hashed
   * @return the hashed message
   */
  private String hashMessage(@NotNull String message) {
    MessageDigest messageDigest;
    try {
      messageDigest = MessageDigest.getInstance(SHA_256);
      messageDigest.update(message.getBytes());
      return new String(messageDigest.digest());
    } catch (NoSuchAlgorithmException e) {
      log.error(e.getMessage());
      return null;
    }
  }

  /**
   * Check if the current timestamp is more than hours old.
   *
   * @param timestamp the timestamp since the last message was sent.
   * @return true if more than 4 hours old, false otherwise
   */
  private boolean isFourhoursOld(@NotNull Date timestamp) {
    Date now = new Date();
    DateTime date = new DateTime(now);
    now = date.minusHours(TIME_WINDOW).toDate();
    return timestamp.before(now);
  }

  /**
   * Save a new InsightMessage to the repository.
   *
   * @param message the message being sent to the notifications
   * @param hash the message hash
   */
  private void saveInsightMessage(String message, String hash) {
    InsightsMessage insightsMessage = InsightsMessage.builder()
        .message(message)
        .hash(hash)
        .timestamp(new Date())
        .build();
    this.insightsRepository.save(insightsMessage);
  }

  /**
   * Convert a number between 0 and 1 into a percentage.
   *
   * @param score the score value to be formated.
   * @return a string representation
   */
  private String formatMessage(double score) {
      return String.format("%,.2f%s", score * 100, "%");
  }
}
