package cloudos.insights;

import cloudos.security.WebSecurityConfig;
import javax.validation.constraints.NotNull;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Insights Controller.
 *
 */
@RestController
@RequestMapping(value = InsightsController.INSIGHTS)
@Log4j2
public class InsightsController {

  public static final String INSIGHTS = WebSecurityConfig.API_PATH + "/insights";

  @Autowired
  InsightsService insightsService;

  /**
   * Retrieve if it is in readOnlyMode.
   *
   * @return ResponseEntity for the request
   */
  @GetMapping("/readOnly")
  @ResponseBody
  public ResponseEntity<Boolean> isReadOnlyMode() {
    boolean isReadOnlyMode = insightsService.isReadOnlyMode();
    log.info("Insights Mode: {} ", isReadOnlyMode);
    return new ResponseEntity<>(isReadOnlyMode, HttpStatus.OK);
  }

  /**
   * Set the readOnlyMode.
   *
   * @param bool true for readOnly, false otherwise
   * @return ResponseEntity for the request
   */
  @PostMapping("/readOnly/{bool}")
  @ResponseBody
  public ResponseEntity<Boolean> setReadOnlyMode(@NotNull @PathVariable("bool") Boolean bool) {
    log.info("Setting Insights Mode: {} ", bool);
    insightsService.setReadOnlyMode(bool);
    return this.isReadOnlyMode();
  }

  /**
   * Retrieve the resizing recommendation policy.
   * 
   * @return ResizingRecommendationPolicy
   */
  @GetMapping("/policy/resizingRecommendation")
  @ResponseBody
  public ResponseEntity<ResizingRecommendationPolicy> getResizingRecommendationPolicy() {
    ResizingRecommendationPolicy policy = insightsService.getResizingRecommendationPolicy();
    log.info("Policy: {} ", policy);
    return new ResponseEntity<>(policy, HttpStatus.OK);
  }

  /**
   * Set the resizing recommendation policy.
   * 
   * @param policy to be saved/updated
   * @return ResizingRecommendationPolicy
   */
  @PostMapping("/policy/resizingRecommendation")
  @ResponseBody
  public ResponseEntity<?> setResizingRecommendationPolicy(
      @RequestBody ResizingRecommendationPolicy policy) {
    log.info("Policy: {} ", policy);
    try {
      insightsService.setResizingRecommendationPolicy(policy);
      ResizingRecommendationPolicy savedPolicy = insightsService.getResizingRecommendationPolicy();
      return new ResponseEntity<>(savedPolicy, HttpStatus.OK);
    } catch (InvalidSettingsException e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
  }

}
