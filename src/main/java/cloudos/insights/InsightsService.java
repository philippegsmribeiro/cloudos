package cloudos.insights;

import java.util.List;
import javax.annotation.PostConstruct;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class InsightsService {

  @Autowired
  InsightsSettingsRepository insigthsSettingsRepository;

  @Value("${insights.defaultLowerBound:0.2}")
  double lowerBound = 0.2;
  @Value("${insights.defaultUpperBound:0.7}")
  double upperBound = 0.7;

  /**
   * Load the default settings.
   */
  @PostConstruct
  private void load() {
    List<InsightsSettings> findAll = insigthsSettingsRepository.findAll();
    if (CollectionUtils.isEmpty(findAll)) {
      // save default
      ResizingRecommendationPolicy resizingRecommendationPolicy =
          ResizingRecommendationPolicy.builder()
              .cpuLowerBound(lowerBound)
              .cpuUpperBound(upperBound)
              .fileSystemUpperBound(upperBound)
              .memoryLowerBound(lowerBound)
              .memoryUpperBound(upperBound)
              .processCpuUpperBound(upperBound)
              .processMemoryUpperBound(upperBound)
              .build();

      InsightsSettings settings = InsightsSettings.builder().isReadOnlyMode(true)
          .resizingRecommendationPolicy(resizingRecommendationPolicy).build();
      insigthsSettingsRepository.save(settings);
    }
  }

  /**
   * Return the settings from the db.
   * 
   * @return InsigthsSettings
   */
  private InsightsSettings getSettings() {
    return insigthsSettingsRepository.findAll().get(0);
  }

  /**
   * Returns if application is on readonly mode.
   * 
   * @return true if readonly, false otherwise.
   */
  public boolean isReadOnlyMode() {
    return getSettings().isReadOnlyMode();
  }

  /**
   * Set the application to be on readonly mode.
   * 
   * @param readOnly true if readonly, false otherwise.
   */
  public void setReadOnlyMode(boolean readOnly) {
    log.info("Setting application readonly mode to {}.", readOnly);
    InsightsSettings settings = getSettings();
    settings.setReadOnlyMode(readOnly);
    insigthsSettingsRepository.save(settings);
  }

  /**
   * Set policy/params of resizingRecommendation.
   * 
   * @param resizingRecommendationPolicy policy
   * @throws InvalidSettingsException if settings are invalid
   */
  public void setResizingRecommendationPolicy(
      ResizingRecommendationPolicy resizingRecommendationPolicy) throws InvalidSettingsException {
    if (resizingRecommendationPolicy.getCpuLowerBound() == null
        || resizingRecommendationPolicy.getCpuUpperBound() == null
        || resizingRecommendationPolicy.getFileSystemUpperBound() == null
        || resizingRecommendationPolicy.getMemoryLowerBound() == null
        || resizingRecommendationPolicy.getMemoryUpperBound() == null
        || resizingRecommendationPolicy.getProcessCpuUpperBound() == null
        || resizingRecommendationPolicy.getProcessMemoryUpperBound() == null) {
      throw new InvalidSettingsException("All fields for the policy has to be fill.");
    }
    InsightsSettings settings = getSettings();
    settings.setResizingRecommendationPolicy(resizingRecommendationPolicy);
    insigthsSettingsRepository.save(settings);
  }

  /**
   * Get policy/params of resizingRecommendation.
   * 
   * @return ResizingRecommendationPolicy
   */
  public ResizingRecommendationPolicy getResizingRecommendationPolicy() {
    return getSettings().getResizingRecommendationPolicy();
  }
}
