package cloudos.insights;

import com.google.common.collect.Maps;
import java.util.Map;

public enum MetricbeatSystemModules {

  CORE("core"),
  CPU("cpu"),
  DISKIO("diskio"),
  FILESYSTEM("filesystem"),
  FSSTAT("fsstat"),
  LOAD("load"),
  MEMORY("memory"),
  NETWORK("network"),
  PROCESS("process"),
  PROCESS_SUMMARY("process_summary"),
  RAID("raid"),
  SOCKET("socket"),
  UPTIME("uptime");


  private String metricset;

  private static final Map<String, MetricbeatSystemModules> nameIndex =
      Maps.newHashMapWithExpectedSize(MetricbeatSystemModules.values().length);

  static {
    for (MetricbeatSystemModules system : MetricbeatSystemModules.values()) {
      nameIndex.put(system.getMetricset(), system);
    }
  }

  MetricbeatSystemModules(String metricset) {
    this.metricset = metricset;
  }

  public String getMetricset() {
    return this.metricset;
  }

  public static MetricbeatSystemModules lookupByName(String name) {
    return nameIndex.get(name);
  }

  @Override
  public String toString() {
    return metricset;
  }

}
