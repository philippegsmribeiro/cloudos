package cloudos.insights;

import cloudos.models.Metricbeat;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MetricbeatRepository extends MongoRepository<Metricbeat, String> {
}
