package cloudos.insights;

import com.google.common.collect.Maps;
import java.util.Map;

public enum MetricbeatModules {

  AEROSPIKE("aerospike"),
  APACHE("apache"),
  CEPH("ceph"),
  COUCHBASE("couchbase"),
  DOCKER("docker"),
  DROPWIZARD("dropwizard"),
  ELASTICSEARCH("elasticsearch"),
  ETCD("etcd"),
  GOLANG("golang"),
  GRAPHITE("graphite"),
  HAPROXY("haproxy"),
  HTTP("http"),
  JOLOKIA("jolokia"),
  KAFKA("kafka"),
  KIBANA("kibana"),
  KUBERNETES("kubernetes"),
  LOGSTASH("logstash"),
  MEMCACHED("memcached"),
  MONGODB("mongodb"),
  MYSQL("mysql"),
  PHP_FPM("php_fpm"),
  POSTGRESQL("postgresql"),
  PROMETHEUS("prometheus"),
  RABBITMQ("rabbitmq"),
  REDIS("redis"),
  SYSTEM("system"),
  UWSGI("uwsgi"),
  VSPHERE("vsphere"),
  WINDOWS("windows"),
  ZOOKEEPER("zookeeper");

  private String module;

  private static final Map<String, MetricbeatModules> nameIndex =
      Maps.newHashMapWithExpectedSize(MetricbeatModules.values().length);

  static {
    for (MetricbeatModules module : MetricbeatModules.values()) {
      nameIndex.put(module.getModule(), module);
    }
  }

  MetricbeatModules(String module) {
    this.module = module;
  }

  public String getModule() {
    return this.module;
  }

  @Override
  public String toString() {
    return module;
  }

  public static MetricbeatModules lookupByName(String name) {
    return nameIndex.get(name);
  }
}
