package cloudos.insights;

import java.util.Date;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface InsightsRepository extends MongoRepository<InsightsMessage, String> {

  InsightsMessage findByHash(String hash);
  List<InsightsMessage> findByTimestampBefore(Date date);
}
