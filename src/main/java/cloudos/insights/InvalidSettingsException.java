package cloudos.insights;

/**
 * Invalid Settings exception.
 * 
 */
public class InvalidSettingsException extends Exception {

  private static final long serialVersionUID = -6533420962791567251L;

  public InvalidSettingsException(String message) {
    super(message);
  }

}
