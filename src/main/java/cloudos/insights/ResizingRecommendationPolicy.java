package cloudos.insights;

import cloudos.config.CloudOSJson;
import cloudos.utils.ReflectionToJson;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@CloudOSJson
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResizingRecommendationPolicy {

  private Double cpuUpperBound;
  private Double cpuLowerBound;

  private Double memoryUpperBound;
  private Double memoryLowerBound;

  private Double processCpuUpperBound;
  private Double processMemoryUpperBound;

  private Double fileSystemUpperBound;
  
  @Override
  public String toString() {
    return ReflectionToJson.toString(this);
  }
}
