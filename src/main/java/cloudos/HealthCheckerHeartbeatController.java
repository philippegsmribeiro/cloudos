package cloudos;

import cloudos.elasticsearch.model.ChartParameters;
import cloudos.elasticsearch.model.KibanaSearchParametersTransformer;
import cloudos.elasticsearch.services.HeartbeatService;
import cloudos.security.WebSecurityConfig;

import java.net.UnknownHostException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Implement the HealthChecker methods from HeartbeatService.
 *
 * @author Rogerio Souza
 */
@Controller
@RestController
@RequestMapping(value = HealthCheckerHeartbeatController.HEALTHCHECKER)
public class HealthCheckerHeartbeatController {

  public static final String HEALTHCHECKER =
      WebSecurityConfig.API_PATH + "/healthchecker/heartbeat";

  @Autowired private HeartbeatService heartbeatService;

  @Autowired private KibanaSearchParametersTransformer transformer;

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by DURATION_US
   * heartbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/duration_us_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getDurationUsAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return heartbeatService.getDurationUsAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by RESOLVE_RTT_US
   * heartbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/resolve_rtt_us_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getResolveRttUsAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return heartbeatService.getResolveRttUsAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by ICMP_RTT_US
   * heartbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/icmp_rtt_us_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getIcmpRttUsAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return heartbeatService.getIcmpRttUsAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by TCP_CONNECT_RTT_US
   * heartbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/tcp_connect_rtt_us_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getTcpConnectRttUsAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return heartbeatService.getTcpConnectRttUsAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by SOCKS5_CONNECT_RTT_US
   * heartbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/socks5_connect_rtt_us_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSocks5ConnectRttUsAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return heartbeatService.getSocks5ConnectRttUsAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by TLS_HANDSHAKE_RTT_US
   * heartbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/tls_handshake_rtt_us_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getTlsHandshakeRttUsAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return heartbeatService.getTlsHandshakeRttUsAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by HTTP_RTT_US
   * heartbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/http_rtt_us_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getHttpRttUsAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return heartbeatService.getHttpRttUsAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by VALIDATE_RTT_US
   * heartbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/validate_rtt_us_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getValidateRttUsAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return heartbeatService.getValidateRttUsAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by RESPONSE_STATUS
   * heartbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/response_status_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getResponseStatusAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return heartbeatService.getResponseStatusAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by UP heartbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/up_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getUpAvg(@RequestBody ChartParameters chartParameters) throws UnknownHostException {
    return heartbeatService.getUpAvg(transformer.apply(chartParameters));
  }
}
