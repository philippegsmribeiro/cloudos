package cloudos.services;

import cloudos.Providers;
import cloudos.models.services.CloudosService;
import cloudos.models.services.ServiceCategory;
import cloudos.models.services.ServiceStatus;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;


/**
 * Store all the Cloudos Service in a Mongo collection.
 * 
 * @author Alex Calagua
 */

public interface CloudosServiceRepository
    extends MongoRepository<CloudosService, String>, QueryDslPredicateExecutor<CloudosService> {

  List<CloudosService> findByProvidersAndServiceCategory(Providers providers,
      ServiceCategory serviceCategory);

  List<CloudosService> findByCodeAndServiceCategory(String code, ServiceCategory serviceCategory);

  CloudosService findByCodeAndProvidersAndServiceCategory(String code, Providers provider,
      ServiceCategory serviceCategory);

  List<CloudosService> findByCodeAndProviders(String code, Providers provider);
  
  List<CloudosService> findByCodeInAndProviders(List<String> codes, Providers provider);

  CloudosService findById(String id);

  List<CloudosService> findByServiceStatus(ServiceStatus serviceStatus);

  List<CloudosService> findByCodeAndServiceCategoryIn(String serviceOther, List<ServiceCategory> collect);

}
