package cloudos.services;

import cloudos.Providers;
import cloudos.models.services.CloudosService;
import cloudos.models.services.QCloudosService;
import cloudos.models.services.ServiceCategory;
import cloudos.models.services.ServiceException;
import cloudos.models.services.ServiceRequest;
import cloudos.models.services.ServiceResponse;
import cloudos.models.services.ServiceStatus;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

/**
 * Service implementation for Cloudos services.
 * 
 * @author Alex Calagua
 *
 */
@Service
@Log4j2
public class CloudosServiceService {
  private static final String MESSAGE_ERRO_REQUEST = "%s request can not be null";
  public static final String SERVICE_OTHER = "OTHER";

  @Autowired
  private CloudosServiceRepository cloudosServiceRepository;

  /**
   * Method that create an service.
   * 
   * @param serviceRequest request to create an the Cloudos Service.
   * @return ServiceResponse Class representing the creation of Cloudos Service.
   * @throws ServiceException if something goes wrong
   */
  public ServiceResponse createService(ServiceRequest serviceRequest) throws ServiceException {
    return this.createService(serviceRequest, true);
  }

  /**
   * Method that create an service.
   * 
   * @param serviceRequest request to create an the Cloudos Service.
   * @param throwErrorEnabled enables the error launch.
   * @return ServiceResponse Class representing the creation of Cloudos Service.
   * @throws ServiceException if something goes wrong
   */
  public ServiceResponse createService(ServiceRequest serviceRequest, boolean throwErrorEnabled)
      throws ServiceException {
    ServiceResponse serviceResponse = null;
    this.validateCloudosService(serviceRequest);

    boolean isExists = this.validateCloudosServiceExists(serviceRequest);

    if (isExists) {
      if (throwErrorEnabled) {
        log.error("Service already exists", serviceRequest.getCode());
        throw new ServiceException("Service already exists");
      }
    } else {
      CloudosService cloudosService = CloudosService.builder().code(serviceRequest.getCode())
          .providers(serviceRequest.getProviders())
          .serviceCategory(serviceRequest.getServiceCategory()).created(new Date())
          .name(serviceRequest.getName()).serviceStatus(ServiceStatus.ACTIVE).build();
      this.checkServiceOtherByCategory(serviceRequest);
      CloudosService serviceCreated = cloudosServiceRepository.save(cloudosService);
      serviceResponse = ServiceResponse.builder().service(serviceCreated)
          .response("Service successfully created").build();
    }
    return serviceResponse;
  }

  /**
   * Method that create services.
   * 
   * @param servicesRequests list of request to create an the Cloudos Service.
   * @param throwErrorEnabled enables the error launch.
   * @return List ServiceResponse Class representing the creation of Cloudos Service.
   * @throws ServiceException if something goes wrong
   */
  public List<ServiceResponse> createServices(List<ServiceRequest> servicesRequests,
      boolean throwErrorEnabled) throws ServiceException {

    List<ServiceResponse> responses = new ArrayList<>();

    if (CollectionUtils.isNotEmpty(servicesRequests)) {

      log.info("collecting services!");
      Set<String> collectCodes =
          servicesRequests.stream().map(t -> t.getCode()).collect(Collectors.toSet());

      log.info("finding existing services!");
      List<CloudosService> listExistingCloudosServices =
          cloudosServiceRepository.findByCodeInAndProviders(new ArrayList<>(collectCodes),
              servicesRequests.get(0).getProviders());

      List<ServiceRequest> validServicesRequests = new ArrayList<>();

      // validate all
      servicesRequests.forEach(t -> {
        try {
          validateCloudosService(t);
          validServicesRequests.add(t);
        } catch (ServiceException e) {
          log.error("Error validating service!", e);
        }
      });

      Set<String> uniqueServices =
          listExistingCloudosServices.stream().map(t -> t.getCode()).collect(Collectors.toSet());
      List<ServiceRequest> notExistingServices = validServicesRequests.stream()
          .filter(t -> !uniqueServices.contains(t.getCode())).collect(Collectors.toList());

      Set<CloudosService> listToSave = new HashSet<>();

      if (CollectionUtils.isNotEmpty(notExistingServices)) {

        notExistingServices.forEach(t -> {

          CloudosService cloudosService = CloudosService.builder().code(t.getCode())
              .providers(t.getProviders()).serviceCategory(t.getServiceCategory()).name(t.getName())
              .serviceStatus(ServiceStatus.ACTIVE).build();

          listToSave.add(cloudosService);

        });

        Set<ServiceCategory> notExistingServiceCategories = notExistingServices.stream()
            .map(t -> t.getServiceCategory()).collect(Collectors.toSet());

        List<CloudosService> cloudosServiceOthers =
            cloudosServiceRepository.findByCodeAndServiceCategoryIn(SERVICE_OTHER,
                new ArrayList<>(notExistingServiceCategories));

        Set<ServiceCategory> existingServiceCategories = cloudosServiceOthers.stream()
            .map(t -> t.getServiceCategory()).collect(Collectors.toSet());

        Collection<ServiceCategory> newServiceCategories =
            CollectionUtils.subtract(notExistingServiceCategories, existingServiceCategories);

        Providers provider = servicesRequests.get(0).getProviders();

        newServiceCategories.forEach(t -> {

          CloudosService cloudosServiceOther =
              CloudosService.builder().code(SERVICE_OTHER).providers(provider).serviceCategory(t)
                  .name(SERVICE_OTHER).serviceStatus(ServiceStatus.ACTIVE).build();
          listToSave.add(cloudosServiceOther);
        });

        Date created = new Date();
        listToSave.forEach(t -> t.setCreated(created));
        cloudosServiceRepository.save(listToSave);
      }
    }
    return responses;
  }

  /**
   * Method that validates if the service already exists.
   * 
   * @param serviceRequest request to validate an the Cloudos Service.
   * @throws ServiceException if something goes wrong.
   */
  private boolean validateCloudosServiceExists(ServiceRequest serviceRequest) {
    List<CloudosService> listCloudosServices = cloudosServiceRepository
        .findByCodeAndProviders(serviceRequest.getCode(), serviceRequest.getProviders());
    return listCloudosServices != null && !listCloudosServices.isEmpty();
  }

  /**
   * Method that validates the entity ServiceRequest.
   * 
   * @param serviceRequest request to create an the service.
   * @throws ServiceException if something goes wrong
   */
  private void validateCloudosService(ServiceRequest serviceRequest) throws ServiceException {
    if (StringUtils.isBlank(serviceRequest.getCode())) {
      String message = String.format(MESSAGE_ERRO_REQUEST, "Code");
      log.error("Error: {} ServiceRequest: {}", message, serviceRequest);
      throw new ServiceException(message);
    }
    if (serviceRequest.getProviders() == null) {
      String message = String.format(MESSAGE_ERRO_REQUEST, "Provider");
      log.error("Error: {} ServiceRequest: {}", message, serviceRequest);
      throw new ServiceException(message);
    }
    if (serviceRequest.getServiceCategory() == null) {
      String message = String.format(MESSAGE_ERRO_REQUEST, "Category");
      log.error("Error: {} ServiceRequest: {}", message, serviceRequest);
      throw new ServiceException(message);
    }
    if (StringUtils.isBlank(serviceRequest.getName())) {
      String message = String.format(MESSAGE_ERRO_REQUEST, "Name");
      log.error("Error: {} ServiceRequest: {}", message, serviceRequest);
      throw new ServiceException(message);
    }
  }

  /**
   * Check if another code exists for a given category.
   * 
   * @param serviceRequest request to check if exist service other.
   */
  private void checkServiceOtherByCategory(ServiceRequest serviceRequest) {
    List<CloudosService> cloudosServiceOthers = cloudosServiceRepository
        .findByCodeAndServiceCategory(SERVICE_OTHER, serviceRequest.getServiceCategory());

    if (cloudosServiceOthers == null || cloudosServiceOthers.isEmpty()) {
      CloudosService cloudosService =
          CloudosService.builder().code(SERVICE_OTHER).providers(serviceRequest.getProviders())
              .serviceCategory(serviceRequest.getServiceCategory()).created(new Date())
              .name(SERVICE_OTHER).serviceStatus(ServiceStatus.ACTIVE).build();
      cloudosServiceRepository.save(cloudosService);
    }
  }

  /**
   * Method that looks for a service by the provider and category.
   * 
   * @param serviceRequest to perform the service search.
   * @return List the CloudosService.
   */
  public List<CloudosService> findByIdProvidersAndIdServiceCategory(ServiceRequest serviceRequest) {
    List<CloudosService> result = cloudosServiceRepository.findByProvidersAndServiceCategory(
        serviceRequest.getProviders(), serviceRequest.getServiceCategory());
    return result;
  }


  /**
   * Method that looks for a service by id.
   * 
   * @param id to perform the service search.
   * @return List the CloudosService.
   */
  public CloudosService findById(String id) {
    CloudosService result = cloudosServiceRepository.findById(id);
    return result;
  }

  /**
   * Method that return all service active.
   * 
   * @return List the CloudosService.
   */
  public List<CloudosService> findAllActive() {
    List<CloudosService> result =
        cloudosServiceRepository.findByServiceStatus(ServiceStatus.ACTIVE);
    return result;
  }


  /**
   * Method that update an Cloudos Service.
   * 
   * @param serviceRequest request to update an the Cloudos Service.
   * @return ServiceResponse Class representing the updated of Cloudos Service.
   * @throws ServiceException if something goes wrong.
   */
  public ServiceResponse updateService(ServiceRequest serviceRequest) throws ServiceException {
    CloudosService cloudosService = cloudosServiceRepository.findById(serviceRequest.getId());

    if (cloudosService == null) {
      log.error(String.format("Cloudos service with id %s does not exist", serviceRequest.getId()));
      throw new ServiceException(
          String.format("Cloudos service with id %s does not exist", serviceRequest.getId()));
    } else if (serviceRequest.getCode().equals(SERVICE_OTHER)) {
      if (serviceRequest.getServiceStatus().equals(ServiceStatus.INACTIVE)) {
        log.error("Other service can not be disabled", serviceRequest);
        throw new ServiceException("Other service can not be disabled");
      }
    }
    CloudosService cloudosServiceUpdate = CloudosService.builder().id(serviceRequest.getId())
        .code(serviceRequest.getCode()).providers(serviceRequest.getProviders())
        .serviceCategory(serviceRequest.getServiceCategory()).name(serviceRequest.getName())
        .serviceStatus(serviceRequest.getServiceStatus()).created(serviceRequest.getCreated())
        .build();

    return ServiceResponse.builder().service(cloudosServiceRepository.save(cloudosServiceUpdate))
        .response("Service successfully updated").build();
  }

  /**
   * Method that delete an Cloudos service by Provider and Code.
   * 
   * @param id Cloudos Service Id.
   * @return ServiceResponse Class representing the deleted of Cloudos Service.
   * @throws ServiceException if something goes wrong.
   * 
   */
  public ServiceResponse deleteService(String id) throws ServiceException {
    CloudosService cloudosService = cloudosServiceRepository.findById(id);

    if (cloudosService == null) {
      log.error(String.format("Cloudos service with id %s does not exist", id));
      throw new ServiceException(String.format("Cloudos service with id %s does not exist", id));
    } else if (cloudosService.getCode().equals(SERVICE_OTHER)) {
      log.error("Other service can not be deleted", cloudosService.getCode());
      throw new ServiceException("Other service can not be deleted");
    } else {
      cloudosServiceRepository.delete(cloudosService);
    }
    return ServiceResponse.builder().service(cloudosService)
        .response("Service successfully deleted").build();
  }

  /**
   * Method that returns a list of CloudosService.
   * 
   * @param serviceRequest Request generic by search services.
   * @return list the CloudosService
   */
  public List<CloudosService> getServicesByFilter(ServiceRequest serviceRequest)
      throws ServiceException {
    Predicate filters = getFiltersQ(serviceRequest);
    Sort sort = new Sort(new Order(Sort.Direction.ASC, "name").ignoreCase());
    return (List<CloudosService>) cloudosServiceRepository.findAll(filters, sort);
  }

  /**
   * Method that mount a filter for CloudosService.
   * 
   * @param serviceRequest Request generic by search services.
   * @return Predicate Conditions the filter
   */
  private Predicate getFiltersQ(ServiceRequest serviceRequest) {
    BooleanBuilder builder = new BooleanBuilder();
    QCloudosService cloudosService = new QCloudosService("cloudosService");
    if (StringUtils.isNotBlank(serviceRequest.getCode())) {
      builder.and(cloudosService.code.startsWith(serviceRequest.getCode()));
    }
    if (serviceRequest.getProviders() != null) {
      builder.and(cloudosService.providers.eq(serviceRequest.getProviders()));
    }
    if (serviceRequest.getServiceCategory() != null) {
      builder.and(cloudosService.serviceCategory.eq(serviceRequest.getServiceCategory()));
    }
    if (StringUtils.isNotBlank(serviceRequest.getName())) {
      builder.and(cloudosService.name.startsWith(serviceRequest.getName()));
    }
    return builder;
  }

}

