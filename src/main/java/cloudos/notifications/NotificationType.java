package cloudos.notifications;

public enum NotificationType {
  HEALTHCHECKER,
  AUTOSCALER,
  DEPLOYMENTS,
  CLIENT_INSTALL,
  LOGS,
  ALERT,
  INSIGHTS,
  GENERAL
}
