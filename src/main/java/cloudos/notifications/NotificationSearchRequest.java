package cloudos.notifications;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
public class NotificationSearchRequest {

  private Integer page;
  private Integer pageSize;
  private NotificationCategory category;
  private Boolean justUnread;

  /**
   * Constructor using fields.
   * 
   * @param page actual page
   * @param pageSize number of elements per page
   * @param category notification category
   * @param justUnread true for just unread message, false for just read, null for all
   */
  public NotificationSearchRequest(Integer page, Integer pageSize, NotificationCategory category,
      Boolean justUnread) {
    super();
    this.page = page;
    this.pageSize = pageSize;
    this.category = category;
    this.justUnread = justUnread;
  }

}
