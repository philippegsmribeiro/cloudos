package cloudos.notifications;

public enum NotificationChannel {
  MONITORING,
  USER_ACTION
}
