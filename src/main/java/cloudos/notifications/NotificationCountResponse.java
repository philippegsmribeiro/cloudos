package cloudos.notifications;

import org.springframework.http.HttpStatus;

import cloudos.models.CloudosResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NotificationCountResponse extends CloudosResponse {

  /**
   * Constructor.
   * 
   * @param status httpstatus
   * @param message status
   * @param total all notification
   * @param totalUnread unread
   * @param totalError error
   * @param totalInfo info
   * @param totalWarning warning
   * @param totalSuccess success
   */
  public NotificationCountResponse(HttpStatus status, String message, Long total, Long totalUnread,
      Long totalError, Long totalInfo, Long totalWarning, Long totalSuccess) {
    super(status, message);
    this.total = total;
    this.totalUnread = totalUnread;
    this.totalError = totalError;
    this.totalInfo = totalInfo;
    this.totalWarning = totalWarning;
    this.totalSuccess = totalSuccess;
  }

  private Long total;
  private Long totalUnread;
  private Long totalError;
  private Long totalInfo;
  private Long totalWarning;
  private Long totalSuccess;

}
