package cloudos.notifications;

import org.springframework.http.HttpStatus;

import cloudos.models.CloudosResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NotificationResponse extends CloudosResponse {

  public NotificationResponse(HttpStatus status, String message, Notification notification) {
    super(status, message);
    this.notification = notification;
  }

  Notification notification;

}
