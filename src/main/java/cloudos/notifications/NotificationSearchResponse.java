package cloudos.notifications;

import java.util.List;

import org.springframework.http.HttpStatus;

import cloudos.models.CloudosResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NotificationSearchResponse extends CloudosResponse {

  /**
   * Constructor.
   * 
   * @param status httpstatus
   * @param message response message
   * @param content list of notifications
   * @param totalElements number total of elements
   * @param page actual page
   * @param pageSize element per page
   */
  public NotificationSearchResponse(HttpStatus status, String message, List<Notification> content,
      long totalElements, Integer page, Integer pageSize) {
    super(status, message);
    this.notifications = content;
    this.totalElements = totalElements;
    this.page = page;
    this.pageSize = pageSize;
  }

  private List<Notification> notifications;
  private Long totalElements;
  private Integer page;
  private Integer pageSize;

}
