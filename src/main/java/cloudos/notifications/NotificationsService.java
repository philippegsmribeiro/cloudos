package cloudos.notifications;

import cloudos.deploy.deployments.Deployment;
import cloudos.deploy.deployments.DeploymentStatus;
import cloudos.exceptions.InvalidRequestException;
import cloudos.insights.InsightsService;
import cloudos.models.AbstractInstance;
import cloudos.models.CloudActionRequest;
import cloudos.models.Instance;
import cloudos.models.InstanceStatus;
import cloudos.models.alerts.GenericAlert;
import cloudos.queue.QueueListener;
import cloudos.queue.QueueManagerService;
import cloudos.queue.message.AlertCreatedMessage.AlertCreatedMessageConsumer;
import cloudos.queue.message.AlertDeletedMessage.AlertDeletedMessageConsumer;
import cloudos.queue.message.AlertUpdatedMessage.AlertUpdatedMessageConsumer;
import cloudos.queue.message.ClientInstallMessage.ClientInstallMessageConsumer;
import cloudos.queue.message.CloudActionRequestMessage.CloudActionRequestMessageConsumer;
import cloudos.queue.message.DeploymentMessage.DeploymentMessageConsumer;
import cloudos.queue.message.InstanceCreatedMessage.InstanceCreatedMessageConsumer;
import cloudos.queue.message.InstanceDeletedMessage.InstanceDeletedMessageConsumer;
import cloudos.queue.message.InstanceKeyNotFoundMessage.InstanceKeyNotFoundConsumer;
import cloudos.queue.message.InstanceUpdatedMessage.InstanceUpdatedMessageConsumer;
import cloudos.queue.message.autoscale.AutoscaleMessage;
import cloudos.queue.message.autoscale.AutoscaleMessage.AutoscaleMessageConsumer;
import cloudos.queue.message.insights.CpuInsightMessage.CpuInsightMessageConsumer;
import cloudos.queue.message.insights.FilesystemInsightMessage.FilesystemInsightMessageConsumer;
import cloudos.queue.message.insights.MemoryInsightMessage.MemoryInsightMessageConsumer;
import cloudos.queue.message.insights.ProcessInsightMessage.ProcessInsightMessageConsumer;
import cloudos.security.RequestScope;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/** Created by philipperibeiro on 3/30/17. */
@Service
@Log4j2
public class NotificationsService {

  public static final String ALERT_UPDATED_TITLE = "Alert updated.";
  public static final String ALERT_UPDATED_MESSAGE = "Alert %s has updated";

  public static final String ALERT_DELETED_TITLE = "Alert deleted.";
  public static final String ALERT_DELETED_MESSAGE = "Alert %s has been deleted.";

  public static final String ALERT_CREATED_TITLE = "Alert Created.";
  public static final String ALERT_CREATED_MESSAGE = "Alert %s was created.";

  public static final String PROCESS_INSIGHT_TITLE = "Process Insight";
  public static final String CPU_INSIGHT_TITLE = "CPU Insight";
  public static final String FILESYSTEM_INSIGHT_TITLE = "Filesystem Insight";
  public static final String MEMORY_INSIGHT_TITLE = "Memory Insight";

  public static final String AUTOSCALER_TITLE = "Autoscaler";
  public static final String AUTOSCALER_MESSAGE =
      "Autoscaler detected for cloudgroud %s, related to instance %s. The action will be %s.";
  public static final String AUTOSCALER_READONLY_MESSAGE =
      "Autoscaler detected for cloudgroud %s, related to instance %s. The action would be %s. "
          + "However, no action will be taken as application is on readonly mode.";



  @Autowired
  NotificationRepository notificationRepository;

  @Autowired
  QueueListener queueListener;

  @Autowired
  QueueManagerService queueManagerService;

  @Autowired
  RequestScope requestScope;

  @Autowired
  InsightsService insightsService;


  /**
   * Set up the consumer after the creating.
   */
  @PostConstruct
  public void setUpListener() {
    // add listener to the queues

    // instance created
    queueListener.addConsumer((InstanceCreatedMessageConsumer) message -> {
      createNewInstanceNotification(message.getInstance());
    });
    // instance updated
    queueListener.addConsumer((InstanceUpdatedMessageConsumer) message -> {
      createInstanceUpdateNotification(message.getNewInstance(), message.getOldInstanceStatus());
    });
    // instance deleted
    queueListener.addConsumer((InstanceDeletedMessageConsumer) message -> {
      createDeletedInstanceNotification(message.getInstance());
    });
    // action requested
    queueListener.addConsumer((CloudActionRequestMessageConsumer) message -> {
      createActionRequestNotification(message.getRequest(), message.getUser());
    });
    // deployment
    queueListener.addConsumer((DeploymentMessageConsumer) message -> {
      createDeploymentNotification(message.getDeployment(), message.getDescription());
    });
    // client installation
    queueListener.addConsumer((ClientInstallMessageConsumer) message -> {
      createClientInstallNotification(message.getDeployment(), message.getDescription());
    });
    // key not found
    queueListener.addConsumer((InstanceKeyNotFoundConsumer) message -> {
      createInstanceKeyNotFoundNotification(message.getKeyName(), message.getCloudOsInstance());
    });
    // alert created
    queueListener.addConsumer((AlertCreatedMessageConsumer) message -> {
      createNewAlertNotification(message.getAlert());
    });
    // alert deleted
    queueListener.addConsumer((AlertDeletedMessageConsumer) message -> {
      createDeletedAlertNotification(message.getAlert());
    });
    // alert updated
    queueListener.addConsumer((AlertUpdatedMessageConsumer) message -> {
      createAlertUpdateNotification(message.getAlert());
    });
    // size recommendation process
    queueListener.addConsumer((ProcessInsightMessageConsumer) message -> {
      createProcessInsightNotification(message.getMessage());
    });
    // size recommendation cpu
    queueListener.addConsumer((CpuInsightMessageConsumer) message -> {
      createCpuInsightNotification(message.getMessage());
    });
    // size recommendation memory
    queueListener.addConsumer((MemoryInsightMessageConsumer) message -> {
      createMemoryInsightNotification(message.getMessage());
    });
    // size recommendation filesystem
    queueListener.addConsumer((FilesystemInsightMessageConsumer) message -> {
      createFileSystemInsightNotification(message.getMessage());
    });
    // autoscaler
    queueListener.addConsumer((AutoscaleMessageConsumer) message -> {
      createAutoscalerMessage(message);
    });
  }

  /**
   * Create a new notification based on the autoscaler execution.
   *
   * @param message autoscaler message
   * @return notification id
   */
  protected String createAutoscalerMessage(AutoscaleMessage message) {
    // autoscaler
    NotificationCategory category = NotificationCategory.WARNING;
    String messageText = String.format(AUTOSCALER_MESSAGE,
        message.getAutoscaleRequest().getCloudGroupId(),
        message.getAutoscaleRequest().getInstanceInfo(), message.getAutoscaleRequest().getLabel());

    if (message.getAutoscaleRequest().isReadonly()) {
      // if was in readonly mode
      category = NotificationCategory.INFO;
      messageText = String.format(AUTOSCALER_READONLY_MESSAGE,
          message.getAutoscaleRequest().getCloudGroupId(),
          message.getAutoscaleRequest().getInstanceInfo(),
          message.getAutoscaleRequest().getLabel());
    }

    // create a new notifcation
    Notification notification =
        Notification.builder().category(category).type(NotificationType.AUTOSCALER)
            .title(AUTOSCALER_TITLE).channel(NotificationChannel.MONITORING).message(messageText)
            .creationDate(new Date()).build();
    this.notificationRepository.save(notification);
    this.sendMessage(notification);
    log.info("Created new Autoscaler Notification");
    return notification.getId();
  }

  /**
   * Create a new notification based on the insights obtained from the filesystem utilization of the
   * instance.
   *
   * @param message ta Filesystem insight message
   * @return notification id
   */
  protected String createFileSystemInsightNotification(@NotNull String message) {
    // create a new notifcation
    Notification notification = Notification.builder().category(NotificationCategory.WARNING)
        .type(NotificationType.INSIGHTS).title(FILESYSTEM_INSIGHT_TITLE)
        .channel(NotificationChannel.MONITORING).message(message).creationDate(new Date()).build();
    this.notificationRepository.save(notification);
    this.sendMessage(notification);
    log.info("Created new Filesystem Insight Notification");
    return notification.getId();
  }

  /**
   * Create a new notification based on the insights obtained from the memory utilization of the
   * instance.
   *
   * @param message a message that contains the insight information regarding memory utilization
   * @return notification id
   */
  protected String createMemoryInsightNotification(@NotNull String message) {
    // build the message to be displayed.
    Notification notification = Notification.builder().category(NotificationCategory.WARNING)
        .type(NotificationType.INSIGHTS).title(MEMORY_INSIGHT_TITLE)
        .channel(NotificationChannel.MONITORING).message(message).creationDate(new Date()).build();
    this.notificationRepository.save(notification);
    this.sendMessage(notification);
    log.info("Created new Memory Insight Notification");
    return notification.getId();
  }

  /**
   * Create a new notification based on the insights obtained from the CPU utilization of the
   * instance.
   *
   * @param message a message that contains the insight information regarding CPU utilization
   * @return notification id
   */
  protected String createCpuInsightNotification(@NotNull String message) {
    // build the message to be displayed.
    Notification notification = Notification.builder().category(NotificationCategory.WARNING)
        .type(NotificationType.INSIGHTS).title(CPU_INSIGHT_TITLE)
        .channel(NotificationChannel.MONITORING).message(message).creationDate(new Date()).build();
    this.notificationRepository.save(notification);
    this.sendMessage(notification);
    log.info("Created new CPU Insight Notification");
    return notification.getId();
  }

  /**
   * Create a new notification based on the insights obtained from the processes running in the
   * instance.
   *
   * @param message a message that contains the process insight information
   * @return notification id
   */
  protected String createProcessInsightNotification(@NotNull String message) {
    // build the message to be displayed.
    Notification notification = Notification.builder().category(NotificationCategory.WARNING)
        .type(NotificationType.INSIGHTS).title(PROCESS_INSIGHT_TITLE)
        .channel(NotificationChannel.MONITORING).message(message).creationDate(new Date()).build();
    this.notificationRepository.save(notification);
    this.sendMessage(notification);
    log.info("Created new Process Insight Notification");
    return notification.getId();
  }

  /**
   * Notification when a new alert is create.
   *
   * @param alert - alert that was created
   * @return notification id
   */
  protected String createNewAlertNotification(GenericAlert alert) {
    String message = String.format(ALERT_CREATED_MESSAGE, alert.getAlarmName());
    Notification notification = Notification.builder().category(NotificationCategory.INFO)
        .type(NotificationType.ALERT).channel(NotificationChannel.MONITORING)
        .title(ALERT_CREATED_TITLE).message(message).creationDate(new Date()).build();
    notificationRepository.save(notification);
    this.sendMessage(notification);
    log.info("createNewAlertNotification");
    return notification.getId();
  }

  /**
   * Notification when a alert is not present anymore on the provider.
   *
   * @param alert - deleted alert
   * @return notification id
   */
  protected String createDeletedAlertNotification(GenericAlert alert) {
    String message = String.format(ALERT_DELETED_MESSAGE, alert.getAlarmName());

    Notification notification = Notification.builder().category(NotificationCategory.INFO)
        .type(NotificationType.ALERT).channel(NotificationChannel.MONITORING)
        .title(ALERT_DELETED_TITLE).message(message).creationDate(new Date()).build();
    notificationRepository.save(notification);
    this.sendMessage(notification);
    log.info("createDeletedAlertNotification");
    return notification.getId();
  }

  /**
   * Notification when an alert has changed.
   *
   * @param alert - alert updated
   * @return notification id
   */
  protected String createAlertUpdateNotification(GenericAlert alert) {
    String message = String.format(ALERT_UPDATED_MESSAGE, alert.getAlarmName());
    Notification notification = Notification.builder().category(NotificationCategory.INFO)
        .type(NotificationType.ALERT).channel(NotificationChannel.MONITORING)
        .title(ALERT_UPDATED_TITLE).message(message).creationDate(new Date()).build();
    notificationRepository.save(notification);
    this.sendMessage(notification);
    log.info("createAlertUpdateNotification");
    return notification.getId();
  }

  /**
   * Notification when an instance has changed the status.
   *
   * @param instance - instance updated
   * @param oldInstanceStatus - old status
   */
  protected void createInstanceUpdateNotification(AbstractInstance instance,
      InstanceStatus oldInstanceStatus) {
    String message = String.format("Instance %s has updated the status from %s to %s.",
        instance.getInstance().getName(), oldInstanceStatus, instance.getInstance().getStatus());
    Notification notification = Notification.builder().category(NotificationCategory.INFO)
        .type(NotificationType.HEALTHCHECKER).channel(NotificationChannel.MONITORING)
        .title("Instance status updated.").message(message).creationDate(new Date()).build();
    notificationRepository.save(notification);
    this.sendMessage(notification);
    log.info("createInstanceUpdateNotification");
  }

  /**
   * Notification when a new instance is discovered.
   *
   * @param instance - instance discovered
   */
  protected void createNewInstanceNotification(AbstractInstance instance) {
    String message =
        String.format("Instance %s was find on the provider by the monitoring service.",
            instance.getInstance().getName());
    String message2 =
        String.format("This instance status is %s", instance.getInstance().getStatus());
    Notification notification = Notification.builder().category(NotificationCategory.INFO)
        .type(NotificationType.HEALTHCHECKER).channel(NotificationChannel.MONITORING)
        .title("Instance imported.").message(message + " " + message2).creationDate(new Date())
        .build();
    notificationRepository.save(notification);
    this.sendMessage(notification);
    log.info("createNewInstanceNotification");
  }

  /**
   * Notification when an action is requested for the instances.
   *
   * @param request - request requested
   * @param userName - user who requested
   */
  protected void createActionRequestNotification(CloudActionRequest request, String userName) {
    String message = String.format("%s Instance %s requested by %s.", request.getType(),
        Arrays.toString(request.getNames().toArray()), userName);
    Notification notification = Notification.builder().category(NotificationCategory.INFO)
        .type(NotificationType.HEALTHCHECKER).channel(NotificationChannel.USER_ACTION)
        .title("Action requested.").message(message).creationDate(new Date()).build();
    notificationRepository.save(notification);
    this.sendMessage(notification);
    log.info("createActionRequestNotification");
  }

  /**
   * Notification when a instance is not present anymore on the provider.
   *
   * @param instance - deleted instance
   */
  protected void createDeletedInstanceNotification(Instance instance) {
    Notification notification = Notification.builder().category(NotificationCategory.INFO)
        .type(NotificationType.HEALTHCHECKER).channel(NotificationChannel.MONITORING)
        .title("Instance deleted.")
        .message(String.format("Instance %s not found in the provider.", instance.getName()))
        .creationDate(new Date()).build();
    notificationRepository.save(notification);
    this.sendMessage(notification);
    log.info("createDeletedInstanceNotification");
  }

  /**
   * Notification when a deployment changes status.
   *
   * @param deployment - created deployment
   * @param description - message
   */
  protected void createDeploymentNotification(Deployment deployment, String description) {
    String title = "Deployment Status Updated.";
    String message = description;
    NotificationCategory category = NotificationCategory.INFO;
    if (DeploymentStatus.ERROR.equals(deployment.getStatus())) {
      category = NotificationCategory.ERROR;
      title = "Deployment Failed.";
      message = String.format("Deployment %s Failed. Please see the log for more details.",
          deployment.getId());
    } else if (DeploymentStatus.FINISHED.equals(deployment.getStatus())) {
      category = NotificationCategory.SUCCESS;
      title = "Deployment Completed.";
      message = String.format("Deployment %s Completed. Please see the log for more details.",
          deployment.getId());
    }
    Notification notification = Notification.builder().category(category)
        .type(NotificationType.DEPLOYMENTS).channel(NotificationChannel.MONITORING).title(title)
        .message(message).creationDate(new Date()).build();
    notificationRepository.save(notification);
    this.sendMessage(notification);
    log.info("createDeploymentNotification");
  }

  /**
   * Notification when a client install changes status.
   *
   * @param deployment - client installation deployment
   * @param description - message
   */
  protected void createClientInstallNotification(Deployment deployment, String description) {
    String title = description;
    String message = String.format("Client Install for instance %s Started.",
        deployment.getInstances().get(0).getInstance().toDescriptionString());
    NotificationCategory category = NotificationCategory.INFO;
    if (DeploymentStatus.ERROR.equals(deployment.getStatus())) {
      category = NotificationCategory.ERROR;
      title = "Client Install Failed.";
      message = String.format("Client Install for instance %s Failed.",
          deployment.getInstances().get(0).getInstance().toDescriptionString());
    } else if (DeploymentStatus.FINISHED.equals(deployment.getStatus())) {
      category = NotificationCategory.SUCCESS;
      title = "Client Install Completed.";
      message = String.format("Client Install for instance %s Completed.",
          deployment.getInstances().get(0).getInstance().toDescriptionString());
    }
    Notification notification = Notification.builder().category(category)
        .type(NotificationType.CLIENT_INSTALL).channel(NotificationChannel.MONITORING).title(title)
        .message(message).creationDate(new Date()).build();
    notificationRepository.save(notification);
    this.sendMessage(notification);
    log.info("createClientInstallNotification");
  }

  /**
   * Notification when a key or private key is missing on cloudos.
   *
   * @param keyName - keyname got from the provider info
   * @param instance - instance which the key was not found
   */
  protected void createInstanceKeyNotFoundNotification(String keyName, Instance instance) {
    List<Notification> findByTypeAndInstanceId = notificationRepository
        .findByTypeAndInstanceId(NotificationType.CLIENT_INSTALL, instance.getId());
    if (CollectionUtils.isEmpty(findByTypeAndInstanceId)) {
      String title = "Key pair not found!";
      String textKeyNotFound = String.format("Key pair '%s' not found in the cloudOS.", keyName);
      String textInstance =
          String.format("Instance '%s' was created using this key.", instance.getName());
      String textImportKey = "Please import the key.";
      String message = String.join("<br/>", textKeyNotFound, textInstance, textImportKey);
      if (instance.getKeyName() != null) {
        title = "Private key for key pair not found!";
        String textPrivateKeyNotFound =
            String.format("Private key for key pair '%s' not found in the cloudOS.", keyName);
        String textPrivateKeyExplanation =
            "The private key is needed to cloudos access and monitoring the instance.";
        message =
            String.join("<br/>", textPrivateKeyNotFound, textPrivateKeyExplanation, textImportKey);
      }
      Notification notification = Notification.builder().category(NotificationCategory.WARNING)
          .type(NotificationType.CLIENT_INSTALL).channel(NotificationChannel.MONITORING)
          .instanceId(instance.getId()).title(title).message(message).creationDate(new Date())
          .build();
      notificationRepository.save(notification);
      this.sendMessage(notification);
      log.info("createInstanceKeyNotFoundNotification");
    } else {
      log.info("createInstanceKeyNotFoundNotification-notification already exists.");
    }
  }

  private void sendMessage(Notification notification) {
    log.info("send message for the queue");
    queueManagerService.sendNotificationMessage(notification);
  }

  /**
   * List the notifications.
   *
   * @param request NotificationSearchRequest
   * @return Page result
   */
  public Page<Notification> listNotification(NotificationSearchRequest request) {
    Pageable pageable = new PageRequest(request.getPage(), request.getPageSize(), Direction.DESC,
        Notification.CREATION_DATE);
    if (request.getCategory() != null) {
      // return filtering by category
      return notificationRepository.findByCategory(request.getCategory(), pageable);
    }
    if (request.getJustUnread() == null) {
      // return all
      return notificationRepository.findAll(pageable);
    }
    if (request.getJustUnread()) {
      // return unread by the user
      String userName = requestScope.getUserName();
      return notificationRepository.findByReadUsersNotContaining(userName, pageable);
    } else {
      // return read by the user
      String userName = requestScope.getUserName();
      return notificationRepository.findByReadUsersContaining(userName, pageable);
    }

  }

  /**
   * Mark a notification as read.
   *
   * @param notification read
   * @return the Notification
   */
  public Notification markNotificationAsRead(Notification notification) {
    if (notification.getReadUsers() == null) {
      notification.setReadUsers(new ArrayList<>());
      notification.setReadDate(new Date());
    }
    String userName = requestScope.getUserName();
    if (!notification.getReadUsers().contains(userName)) {
      notification.getReadUsers().add(userName);
    }
    return notificationRepository.save(notification);
  }

  /**
   * Mark a notification as unread.
   *
   * @param notification marked as unread.
   * @return the notification
   */
  public Notification markNotificationAsUnread(Notification notification) {
    String userName = requestScope.getUserName();
    if (notification.getReadUsers() != null && notification.getReadUsers().contains(userName)) {
      notification.getReadUsers().remove(userName);
    }
    return notificationRepository.save(notification);
  }

  /**
   * Retrieve a notification by the Id.
   *
   * @param notificationId - id
   * @return the notification
   */
  public Notification retrieveNotification(String notificationId) {
    return notificationRepository.findOne(notificationId);
  }

  /**
   * Count the number of unread messages for the user.
   *
   * @return number of unread notifications
   */
  public Long countUnread() {
    String userName = requestScope.getUserName();
    return notificationRepository.countByReadUsersNotContaining(userName);
  }

  /**
   * Count the number of notifications.
   *
   * @return NotificationCountResponse
   */
  public NotificationCountResponse countNotifications() {
    String userName = requestScope.getUserName();
    Long unread = notificationRepository.countByReadUsersNotContaining(userName);
    Long error = notificationRepository.countByCategory(NotificationCategory.ERROR);
    Long info = notificationRepository.countByCategory(NotificationCategory.INFO);
    Long warning = notificationRepository.countByCategory(NotificationCategory.WARNING);
    Long success = notificationRepository.countByCategory(NotificationCategory.SUCCESS);
    Long all = notificationRepository.count();
    return new NotificationCountResponse(HttpStatus.OK, "", all, unread, error, info, warning,
        success);
  }

  /**
   * Delete notification.
   *
   * @param notificationId to delete.
   * @throws InvalidRequestException if failed to delete the Notification
   */
  public void deleteNotification(String notificationId) throws InvalidRequestException {
    Notification notification = notificationRepository.findOne(notificationId);
    if (notification == null) {
      throw new InvalidRequestException("Notification does not exist.");
    }
    notificationRepository.delete(notificationId);
  }

  /**
   * Delete notification marked as "READ" will be automatically deleted after 24hrs and Alerts
   * notification will deleted automatically after 7 days..
   */
  public void processDeletionNotification() {
    List<Notification> notifications = notificationRepository.findAll();
    for (Notification notification : notifications) {
      boolean result = false;

      if (NotificationType.ALERT.equals(notification.getType())) {
        result = Days.daysBetween(new DateTime(notification.getReadDate()), new DateTime())
            .getDays() >= 7 ? true : false;
      } else if (!result && notification.getReadDate() != null) {
        result = Hours.hoursBetween(new DateTime(notification.getReadDate()), new DateTime())
            .getHours() >= 24 ? true : false;
      }
      if (result) {
        notificationRepository.delete(notification.getId());
      }
    }
  }
}
