package cloudos.notifications;

import cloudos.exceptions.InvalidRequestException;
import cloudos.security.WebSecurityConfig;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/** Created by philipperibeiro on 3/30/17. */
@RestController
@RequestMapping(value = NotificationsController.NOTIFICATIONS)
@Log4j2
public class NotificationsController {

  public static final String NOTIFICATIONS = WebSecurityConfig.API_PATH + "/notifications";

  @Autowired
  NotificationsService notificationsService;

  public NotificationsController() {}

  public NotificationsController(NotificationsService notificationsService) {
    super();
    this.notificationsService = notificationsService;
  }

  /**
   * Return a list of notification.
   *
   * @param request notificationsearchrequest
   * @return notificationsearchresponse
   */
  @RequestMapping(value = "/list", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> listNotifications(@RequestBody NotificationSearchRequest request) {
    Page<Notification> page = notificationsService.listNotification(request);
    NotificationSearchResponse searchResponse = new NotificationSearchResponse(HttpStatus.OK, "",
        page.getContent(), page.getTotalElements(), request.getPage(), request.getPageSize());
    return new ResponseEntity<>(searchResponse, HttpStatus.OK);
  }

  /**
   * Return a total of notifications.
   *
   * @return long with the total number
   */
  @RequestMapping(value = "/countNotifications", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> countNotifications() {
    return new ResponseEntity<>(notificationsService.countNotifications(), HttpStatus.OK);
  }


  /**
   * Return a total of unread notification for the user.
   *
   * @return long with the total number
   */
  @RequestMapping(value = "/countUnread", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> countUnread() {
    return new ResponseEntity<>(notificationsService.countUnread(), HttpStatus.OK);
  }

  /**
   * Mark a notification as read.
   *
   * @param notificationId identifier
   * @return NotificationResponse
   */
  @RequestMapping(value = "/markAsRead/{notificationId}", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> markAsRead(@PathVariable String notificationId) {
    log.info("Mark as Read Notification {}.", notificationId);
    Notification notification = notificationsService.retrieveNotification(notificationId);
    if (notification != null) {
      // found it
      notification = notificationsService.markNotificationAsRead(notification);
      NotificationResponse response = new NotificationResponse(HttpStatus.OK, "", notification);
      return new ResponseEntity<>(response, HttpStatus.OK);
    }
    NotificationResponse response =
        new NotificationResponse(HttpStatus.NOT_FOUND, "Notification not found!", null);
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

  /**
   * Mark a notification as read.
   *
   * @param notificationId identifier
   * @return NotificationResponse
   */
  @RequestMapping(value = "/markAsUnread/{notificationId}", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> markAsUnread(@PathVariable String notificationId) {
    log.info("Mark as Unread Notification {}.", notificationId);
    Notification notification = notificationsService.retrieveNotification(notificationId);
    if (notification != null) {
      // found it
      notification = notificationsService.markNotificationAsUnread(notification);
      NotificationResponse response = new NotificationResponse(HttpStatus.OK, "", notification);
      return new ResponseEntity<>(response, HttpStatus.OK);
    }
    NotificationResponse response =
        new NotificationResponse(HttpStatus.NOT_FOUND, "Notification not found!", null);
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

  /**
   * Delete a notification.
   *
   * @param notificationId identifier
   * @return NotificationResponse
   */
  @RequestMapping(value = "/{notificationId}", method = RequestMethod.DELETE)
  @ResponseBody
  public ResponseEntity<?> deleteNotification(@PathVariable String notificationId) {
    log.info("Notification has deleted.", notificationId);
    if (notificationId == null) {
      return new ResponseEntity<>("Notification request is invalid",
          HttpStatus.PRECONDITION_FAILED);
    }
    try {
      notificationsService.deleteNotification(notificationId);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (InvalidRequestException e) {
      log.error(e);
      return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
    }

  }
}
