package cloudos.notifications;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface NotificationRepository extends MongoRepository<Notification, String> {

  Page<Notification> findByCategory(NotificationCategory category, Pageable pageable);

  Long countByCategory(NotificationCategory category);
  
  Page<Notification> findByReadUsersNotContaining(String user, Pageable pageable);

  Long countByReadUsersNotContaining(String user);

  Page<Notification> findByReadUsersContaining(String user, Pageable pageable);

  Long countByReadUsersContaining(String user);

  List<Notification> findByTypeAndInstanceId(NotificationType type, String instanceId);
}
