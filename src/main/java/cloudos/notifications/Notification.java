package cloudos.notifications;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Getter
@Setter
@Builder
@AllArgsConstructor
@Document(collection = "notifications")
public class Notification {

  public static String CREATION_DATE = "creationDate";

  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  private String id;

  private NotificationCategory category;
  private NotificationType type;
  private String title;
  private String message;
  private NotificationChannel channel;
  private Date creationDate;
  private Date readDate;
  private List<String> readUsers;
  private String instanceId;

  public Notification() {
    id = new ObjectId().toHexString();
    creationDate = new Date();
  }
}
