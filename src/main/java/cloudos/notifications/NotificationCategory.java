package cloudos.notifications;

public enum NotificationCategory {
  WARNING,
  INFO,
  ERROR,
  SUCCESS
}
