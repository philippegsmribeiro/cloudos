package cloudos.targetgroups;

import org.springframework.stereotype.Service;

import cloudos.models.AzureTargetGroup;

@Service
public class AzureTargetGroupManagement implements TargetGroupManagement<AzureTargetGroup> {

  @Override
  public AzureTargetGroup create(AzureTargetGroup cloudosTargetGroup) throws TargetGroupException {
    throw new TargetGroupException("TARGET GROUP PROVIDER NOT IMPLEMENTED");
  }

  @Override
  public AzureTargetGroup modify(AzureTargetGroup cloudosTargetGroup) throws TargetGroupException {
    throw new TargetGroupException("TARGET GROUP PROVIDER NOT IMPLEMENTED");
  }

  @Override
  public AzureTargetGroup describe(AzureTargetGroup cloudosTargetGroup) throws TargetGroupException {
    throw new TargetGroupException("TARGET GROUP PROVIDER NOT IMPLEMENTED");
  }

  @Override
  public void delete(AzureTargetGroup cloudosTargetGroup) throws TargetGroupException {
    throw new TargetGroupException("TARGET GROUP PROVIDER NOT IMPLEMENTED");
  }

}
