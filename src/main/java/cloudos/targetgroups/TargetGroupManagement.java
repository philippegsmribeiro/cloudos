package cloudos.targetgroups;

import cloudos.models.CloudosTargetGroup;

/**
 * Interface for the target group managements services.
 *
 * @author Rogério Souza
 *
 */
public interface TargetGroupManagement<T extends CloudosTargetGroup> {

  /**
   * Creates a new target group.
   *
   * @param cloudosTargetGroup - The target group request with all available attributes.
   * @return The created target group.
   * @throws TargetGroupException - If any data validation or communication error is raised.
   */
  T create(T cloudosTargetGroup) throws TargetGroupException;

  /**
   * Deletes a target group.
   *
   * @param cloudosTargetGroup - The target group request with all available attributes.
   * @throws TargetGroupException - If any data validation or communication error is raised.
   */
  T modify(T cloudosTargetGroup) throws TargetGroupException;

  /**
   * Retrieves a target group.
   *
   * @param cloudosTargetGroup - The target group request with all available attributes.
   * @return The updated target group.
   * @throws TargetGroupException - If any data validation or communication error is raised.
   */
  T describe(T cloudosTargetGroup) throws TargetGroupException;

  /**
   * Modifies a target group.
   *
   * @param cloudosTargetGroup - The target group request with all available attributes.
   * @throws TargetGroupException - If any data validation or communication error is raised.
   */
  void delete(T cloudosTargetGroup) throws TargetGroupException;



}
