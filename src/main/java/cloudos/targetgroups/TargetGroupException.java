package cloudos.targetgroups;

/**
 * To be used as the target group exception
 * 
 * @author Rogério Souza
 *
 */
public class TargetGroupException extends Exception {
  private static final long serialVersionUID = -2828557088217365549L;

  public TargetGroupException(String message) {
    super(message);
  }

  public TargetGroupException(String message, Throwable cause) {
    super(message, cause);
  }
}
