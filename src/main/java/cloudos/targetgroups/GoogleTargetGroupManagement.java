package cloudos.targetgroups;

import cloudos.google.integration.GoogleComputeIntegration;
import cloudos.models.GoogleTargetGroup;

import com.google.api.services.compute.model.InstanceGroup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GoogleTargetGroupManagement implements TargetGroupManagement<GoogleTargetGroup> {

  @Autowired
  GoogleComputeIntegration googleComputeIntegration;

  @Override
  public GoogleTargetGroup create(GoogleTargetGroup cloudosTargetGroup)
      throws TargetGroupException {
    try {
      // TODO: revisit it - not sure if target group is the way to go for google
      InstanceGroup instanceGroup = this.googleComputeIntegration.createInstanceGroup(cloudosTargetGroup.getRegion(), cloudosTargetGroup.getName(), "");

    } catch (Exception e) {
      // TODO: handle exception
    }


    throw new TargetGroupException("TARGET GROUP PROVIDER NOT IMPLEMENTED");
  }

  @Override
  public GoogleTargetGroup modify(GoogleTargetGroup cloudosTargetGroup)
      throws TargetGroupException {
    throw new TargetGroupException("TARGET GROUP PROVIDER NOT IMPLEMENTED");
  }

  @Override
  public GoogleTargetGroup describe(GoogleTargetGroup cloudosTargetGroup)
      throws TargetGroupException {
    throw new TargetGroupException("TARGET GROUP PROVIDER NOT IMPLEMENTED");
  }

  @Override
  public void delete(GoogleTargetGroup cloudosTargetGroup) throws TargetGroupException {
    throw new TargetGroupException("TARGET GROUP PROVIDER NOT IMPLEMENTED");
  }



}
