package cloudos.targetgroups;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.elasticloadbalancingv2.AmazonElasticLoadBalancing;
import com.amazonaws.services.elasticloadbalancingv2.AmazonElasticLoadBalancingClientBuilder;
import com.amazonaws.services.elasticloadbalancingv2.model.AmazonElasticLoadBalancingException;
import com.amazonaws.services.elasticloadbalancingv2.model.CreateTargetGroupRequest;
import com.amazonaws.services.elasticloadbalancingv2.model.CreateTargetGroupResult;
import com.amazonaws.services.elasticloadbalancingv2.model.DeleteTargetGroupRequest;
import com.amazonaws.services.elasticloadbalancingv2.model.DeleteTargetGroupResult;
import com.amazonaws.services.elasticloadbalancingv2.model.DescribeTargetGroupsRequest;
import com.amazonaws.services.elasticloadbalancingv2.model.ModifyTargetGroupRequest;
import com.amazonaws.services.elasticloadbalancingv2.model.TargetGroup;

import cloudos.amazon.AWSCredentialService;
import cloudos.models.AmazonTargetGroup;
import cloudos.models.CloudosTargetGroupRepository;

/**
 * Implements all Amazon API for target group services.
 * 
 * @author Rogério Souza
 *
 */
@Service
public class AmazonTargetGroupManagement implements TargetGroupManagement<AmazonTargetGroup> {

  public static final Logger logger = LogManager.getLogger(AmazonTargetGroupManagement.class);

  @Autowired
  private CloudosTargetGroupRepository cloudosTargetGroupRepository;

  @Autowired
  private AWSCredentialService awsCredentialService;

  /**
   * HashMap that will store all AWS API clients by region.
   */
  private Map<String, AmazonElasticLoadBalancing> clientsByRegion = new HashMap<>();

  /**
   * Creates a new AmazonTargetGroupManagement client if it does not exist on the HashMap specifying
   * which region should the object belongs to.
   *
   * @param region - The region.
   * @return A AmazonElasticLoadBalancing - the client created/retrieved by region.
   */
  private AmazonElasticLoadBalancing getClient(String region) throws TargetGroupException {

    if (StringUtils.isEmpty(region)) {
      throw new TargetGroupException("Region is required to create/retrieve a AWS client.");
    }

    AmazonElasticLoadBalancing client = clientsByRegion.get(region);

    if (client != null) {
      return client;
    }

    client = AmazonElasticLoadBalancingClientBuilder.standard()
        .withCredentials(awsCredentialService.getAWSCredentials()).withRegion(region).build();

    clientsByRegion.put(region, client);

    return client;

  }

  /**
   * Creates a new target group.
   * 
   * @param targetGroup - The target group request with attributes.
   * @return The created target group.
   * @throws TargetGroupException - If any data validation or communication error is raised.
   */
  @Override
  public AmazonTargetGroup create(@NotNull AmazonTargetGroup targetGroup)
      throws TargetGroupException {

    CreateTargetGroupRequest createTargetGroupRequest = new CreateTargetGroupRequest()
        .withHealthCheckIntervalSeconds(targetGroup.getHealthCheckIntervalSeconds())
        .withHealthCheckPath(targetGroup.getHealthCheckPath())
        .withHealthCheckPort(targetGroup.getHealthCheckPort())
        .withHealthCheckProtocol(targetGroup.getHealthCheckProtocol())
        .withHealthCheckTimeoutSeconds(targetGroup.getHealthCheckTimeoutSeconds())
        .withHealthyThresholdCount(targetGroup.getHealthyThresholdCount())
        .withName(targetGroup.getName()).withPort(targetGroup.getPort())
        .withProtocol(targetGroup.getProtocol()).withTargetType(targetGroup.getTargetType())
        .withUnhealthyThresholdCount(targetGroup.getUnhealthyThresholdCount())
        .withVpcId(targetGroup.getVpcId());

    logger.info("Creating target group: {}", createTargetGroupRequest.getName());
    try {
      CreateTargetGroupResult result =
          this.getClient(targetGroup.getRegion()).createTargetGroup(createTargetGroupRequest);

      if (result == null) {
        throw new TargetGroupException("Error while creating target group.");
      }

      if (CollectionUtils.isEmpty(result.getTargetGroups())) {
        throw new TargetGroupException("No target group was found after creation.");
      }

      targetGroup.setTargetGroupArn(result.getTargetGroups().get(0).getTargetGroupArn());

      cloudosTargetGroupRepository.insert(targetGroup);

    } catch (AmazonElasticLoadBalancingException e) {
      logger.error(
          "Caught an AmazonServiceException, which means the client encountered a serious internal problem while trying to communicate with TargetGroup.");
      logger.error("Error Message: {}", e.getMessage());
      throw new TargetGroupException(e.getMessage());
    }
    return targetGroup;
  }


  /**
   * Deletes a target group by its Amazon Resource Name (ARN)
   *
   * @param targetGroup - The target group request with attributes.
   * @throws TargetGroupException - If any data validation or communication error is raised.
   */
  @Override
  public void delete(@NotNull AmazonTargetGroup targetGroup) throws TargetGroupException {

    if (StringUtils.isEmpty(targetGroup.getTargetGroupArn())) {
      throw new TargetGroupException("Target Group ARN is null.");
    }

    logger.info("Deleting target group: {}", targetGroup.getTargetGroupArn());
    try {
      DeleteTargetGroupRequest deleteTargetGroupRequest =
          new DeleteTargetGroupRequest().withTargetGroupArn(targetGroup.getTargetGroupArn());
      DeleteTargetGroupResult result =
          this.getClient(targetGroup.getRegion()).deleteTargetGroup(deleteTargetGroupRequest);
      if (result == null) {
        throw new TargetGroupException("Error while deleting target group.");
      }
      cloudosTargetGroupRepository.delete(targetGroup);
    } catch (AmazonElasticLoadBalancingException e) {
      logger.error(
          "Caught an AmazonServiceException, which means the client encountered a serious internal problem while trying to communicate with TargetGroup.");
      logger.error("Error Message: {}", e.getMessage());
      throw new TargetGroupException(e.getMessage());
    }
  }

  /**
   * Retrieves a target group by its Amazon Resource Name (ARN)
   *
   * @param targetGroup - The target group request with attributes.
   * @return The updated target group.
   * @throws TargetGroupException - If any data validation or communication error is raised.
   */
  @Override
  public AmazonTargetGroup describe(@NotNull AmazonTargetGroup targetGroup)
      throws TargetGroupException {

    if (StringUtils.isEmpty(targetGroup.getTargetGroupArn())) {
      throw new TargetGroupException("Target Group ARN is null.");
    }

    logger.info("Describing target group: {}", targetGroup.getTargetGroupArn());
    DescribeTargetGroupsRequest request =
        new DescribeTargetGroupsRequest().withTargetGroupArns(targetGroup.getTargetGroupArn());

    try {
      List<TargetGroup> targetGroups =
          this.getClient(targetGroup.getRegion()).describeTargetGroups(request).getTargetGroups();

      if (targetGroups == null || targetGroups.size() == 0) {
        throw new TargetGroupException("Target Group not found.");
      }

      TargetGroup awsTargetGroup = targetGroups.get(0);

      targetGroup = AmazonTargetGroup.builder().targetGroupArn(awsTargetGroup.getTargetGroupArn())
          .protocol(awsTargetGroup.getProtocol()).port(awsTargetGroup.getPort())
          .targetType(awsTargetGroup.getTargetType()).vpcId(awsTargetGroup.getVpcId())
          .name(targetGroup.getName()).region(targetGroup.getRegion())
          .provider(targetGroup.getProvider())
          .healthCheckIntervalSeconds(awsTargetGroup.getHealthCheckIntervalSeconds())
          .healthCheckPath(awsTargetGroup.getHealthCheckPath())
          .healthCheckPort(awsTargetGroup.getHealthCheckPort())
          .healthCheckProtocol(awsTargetGroup.getHealthCheckProtocol())
          .healthCheckTimeoutSeconds(awsTargetGroup.getHealthCheckTimeoutSeconds())
          .healthyThresholdCount(awsTargetGroup.getHealthyThresholdCount())
          .unhealthyThresholdCount(awsTargetGroup.getUnhealthyThresholdCount()).build();


      return targetGroup;

    } catch (AmazonElasticLoadBalancingException e) {
      logger.error(
          "Caught an AmazonServiceException, which means the client encountered a serious internal problem while trying to communicate with TargetGroup.");
      logger.error("Error Message: {}", e.getMessage());
      throw new TargetGroupException(e.getMessage());
    }
  }

  /**
   * Modifies the health checks used when evaluating the health state of the targets in the
   * specified target group.
   * 
   * @param targetGroup - The target group request with attributes.
   * @return The updated target group.
   * @throws TargetGroupException - If any data validation or communication error is raised.
   */
  @Override
  public AmazonTargetGroup modify(@NotNull AmazonTargetGroup targetGroup)
      throws TargetGroupException {

    if (StringUtils.isEmpty(targetGroup.getTargetGroupArn())) {
      throw new TargetGroupException("Target Group ARN is null.");
    }

    ModifyTargetGroupRequest modifyTargetGroupRequest = new ModifyTargetGroupRequest()
        .withHealthCheckIntervalSeconds(targetGroup.getHealthCheckIntervalSeconds())
        .withTargetGroupArn(targetGroup.getTargetGroupArn())
        .withHealthCheckPath(targetGroup.getHealthCheckPath())
        .withHealthCheckPort(targetGroup.getHealthCheckPort())
        .withHealthCheckProtocol(targetGroup.getHealthCheckProtocol())
        .withHealthCheckTimeoutSeconds(targetGroup.getHealthCheckTimeoutSeconds())
        .withHealthyThresholdCount(targetGroup.getHealthyThresholdCount())
        .withUnhealthyThresholdCount(targetGroup.getUnhealthyThresholdCount());

    logger.info("Modifying target group: {}", modifyTargetGroupRequest.getTargetGroupArn());
    try {
      this.getClient(targetGroup.getRegion()).modifyTargetGroup(modifyTargetGroupRequest);
      cloudosTargetGroupRepository.save(targetGroup);
    } catch (AmazonElasticLoadBalancingException e) {
      logger.error(
          "Caught an AmazonServiceException, which means the client encountered a serious internal problem while trying to communicate with TargetGroup.");
      logger.error("Error Message: {}", e.getMessage());
      throw new TargetGroupException(e.getMessage());
    }
    return targetGroup;
  }

}
