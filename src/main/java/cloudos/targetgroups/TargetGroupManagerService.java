package cloudos.targetgroups;

import cloudos.Providers;
import cloudos.exceptions.NotImplementedException;
import cloudos.models.CloudosTargetGroup;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Handles the target group managements services by provider.
 *
 * @author Rogério Souza
 *
 */
@Service
public class TargetGroupManagerService {

  @Autowired
  private AmazonTargetGroupManagement amazonTargetGroupManagement;

  @Autowired
  private GoogleTargetGroupManagement googleTargetGroupManagement;

  @Autowired
  private AzureTargetGroupManagement azureTargetGroupManagement;


  /**
   * Multiplex between the different target group management based on the provider.
   *
   * @param providers -The name of the provider
   * @param provider - The region of the target group
   * @return a target group management
   * @throws NotImplementedException if the manager is not implemented for the given provider
   */
  @SuppressWarnings("rawtypes")
  private TargetGroupManagement getManagement(Providers provider, String region)
      throws NotImplementedException {
    switch (provider) {
      case AMAZON_AWS:
        return amazonTargetGroupManagement;
      case GOOGLE_COMPUTE_ENGINE:
        return googleTargetGroupManagement;
      case MICROSOFT_AZURE:
        return azureTargetGroupManagement;
      default:
        break;
    }
    throw new NotImplementedException();
  }

  /**
   * Creates a new target group.
   *
   * @param cloudosTargetGroup - The target group request with all available attributes.
   * @return The created target group.
   * @throws TargetGroupException - If any data validation or communication error is raised.
   */
  public CloudosTargetGroup create(CloudosTargetGroup cloudosTargetGroup)
      throws TargetGroupException, NotImplementedException {
    validateRequiredAttributes(cloudosTargetGroup);
    return this.getManagement(cloudosTargetGroup.getProvider(), cloudosTargetGroup.getRegion())
        .create(cloudosTargetGroup);
  }

  /**
   * Retrieves a target group.
   *
   * @param cloudosTargetGroup - The target group request with all available attributes.
   * @return The updated target group.
   * @throws TargetGroupException - If any data validation or communication error is raised.
   */
  public CloudosTargetGroup describe(CloudosTargetGroup cloudosTargetGroup)
      throws TargetGroupException, NotImplementedException {
    validateRequiredAttributes(cloudosTargetGroup);
    return this.getManagement(cloudosTargetGroup.getProvider(), cloudosTargetGroup.getRegion())
        .describe(cloudosTargetGroup);
  }

  /**
   * Modifies a target group.
   *
   * @param cloudosTargetGroup - The target group request with all available attributes.
   * @return The updated target group.
   * @throws TargetGroupException - If any data validation or communication error is raised.
   */
  public CloudosTargetGroup modify(CloudosTargetGroup cloudosTargetGroup)
      throws TargetGroupException, NotImplementedException {
    validateRequiredAttributes(cloudosTargetGroup);
    return this.getManagement(cloudosTargetGroup.getProvider(), cloudosTargetGroup.getRegion())
        .modify(cloudosTargetGroup);
  }

  /**
   * Deletes a target group.
   *
   * @param cloudosTargetGroup - The target group request with all available attributes.
   * @throws TargetGroupException - If any data validation or communication error is raised.
   */
  public void delete(CloudosTargetGroup cloudosTargetGroup)
      throws TargetGroupException, NotImplementedException {
    validateRequiredAttributes(cloudosTargetGroup);
    this.getManagement(cloudosTargetGroup.getProvider(), cloudosTargetGroup.getRegion())
        .delete(cloudosTargetGroup);
  }

  /**
   * Validates if the target group and region is not null/empty.
   *
   * @param cloudosTargetGroup
   * @throws TargetGroupException
   */
  private void validateRequiredAttributes(CloudosTargetGroup cloudosTargetGroup)
      throws TargetGroupException {
    if (cloudosTargetGroup == null) {
      throw new TargetGroupException("Target group is null.");
    }

    if (StringUtils.isEmpty(cloudosTargetGroup.getRegion())) {
      throw new TargetGroupException("Region is null.");
    }

    if (cloudosTargetGroup.getProvider() == null) {
      throw new TargetGroupException("Provider is null.");
    }
  }



}
