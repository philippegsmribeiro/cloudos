package cloudos;

import cloudos.security.WebSecurityConfig;
import cloudos.storage.StorageService;
import com.google.gson.Gson;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = FileUploadController.FILE_UPLOAD)
public class FileUploadController {

  public static final String FILE_UPLOAD = WebSecurityConfig.API_PATH + "/files";

  @Autowired StorageService storageService;

  @RequestMapping(value = "/upload", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> uploadNewFile(@RequestParam("file") MultipartFile file) {
    storageService.store(file, true);
    return new ResponseEntity<>(new Gson().toJson(file.getOriginalFilename()), HttpStatus.OK);
  }

  @RequestMapping(value = "/list", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> listUploadedFiles() throws IOException {
    // storageService
    // .loadAll()
    // .map(path ->
    // MvcUriComponentsBuilder
    // .fromMethodName(FileUploadController.class, "serveFile", path.getFileName().toString())
    // .deployment().toString();
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @RequestMapping(value = "/{filename}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

    Resource file = storageService.loadAsResource(filename);
    return ResponseEntity.ok()
        .header(
            HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
        .body(file);
  }

  // @PostMapping("/files")
  // public String handleFileUpload(@RequestParam("file") MultipartFile file,
  // RedirectAttributes redirectAttributes) {
  //
  // storageService.store(file);
  // redirectAttributes.addFlashAttribute("message",
  // "You successfully uploaded " + file.getOriginalFilename() + "!");
  //
  // return "redirect:/deployments";
  // }
  //
  // @SuppressWarnings("rawtypes")
  // @ExceptionHandler(StorageFileNotFoundException.class)
  // public ResponseEntity handleStorageFileNotFound(StorageFileNotFoundException exc) {
  // return ResponseEntity.notFound().deployment();
  // }

}
