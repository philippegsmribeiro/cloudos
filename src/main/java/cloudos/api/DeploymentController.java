package cloudos.api;

import cloudos.deploy.DeployConfigurationFileException;
import cloudos.deploy.DeploymentManagerService;
import cloudos.deploy.deployments.Deployment;
import cloudos.deploy.deployments.DeploymentStatus;
import cloudos.security.WebSecurityConfig;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = DeploymentController.DEPLOYMENT)
@Log4j2
public class DeploymentController {

  public static final String DEPLOYMENT = WebSecurityConfig.API_PATH + "/deployments";

  @Autowired 
  DeploymentManagerService deploymentManagerService;

  /**
   * Return a list of deployments, filtered by status and pagination.
   *
   * @param page to show
   * @param pageSize number of items
   * @param status to filter
   * @return ResponseEntity for the request
   */
  @RequestMapping(value = "/list/{page}/{pageSize}/{status}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> listDeployments(
      @PathVariable Integer page,
      @PathVariable Integer pageSize,
      @PathVariable DeploymentStatus status) {
    Page<Deployment> listDeployments =
        deploymentManagerService.listDeployments(page, pageSize, status);
    // added for performance reasons
    // set the list to null to avoid the lazy call to mongo to get the instances when converting
    // the object to json.
    for (Deployment deployment : listDeployments.getContent()) {
      deployment.setInstances(null);
    }
    return new ResponseEntity<>(listDeployments, HttpStatus.OK);
  }

  /**
   * Return a list of deployments, filtered by pagination.
   *
   * @param page to show
   * @param pageSize number of items
   * @return ResponseEntity for the request
   */
  @RequestMapping(value = "/list/{page}/{pageSize}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> listDeployments(
      @PathVariable Integer page, @PathVariable Integer pageSize) {
    return listDeployments(page, pageSize, null);
  }

  /**
   * Create a deployment.
   *
   * @param deployment object
   * @return ResponseEntity for the request
   */
  @RequestMapping(value = "/create", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> createDeployment(@RequestBody Deployment deployment) {
    log.info(String.format("--------- Create deployment: %s --------", deployment));
    try {
      Deployment savedDeployment = deploymentManagerService.createDeployment(deployment, true);
      return new ResponseEntity<>(savedDeployment, HttpStatus.OK);
    } catch (DeployConfigurationFileException e) {
      e.printStackTrace();
      return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
    }
  }

  /**
   * Restart a deployment.
   *
   * @param deploymentId id
   * @return ResponseEntity for the request
   */
  @RequestMapping(value = "/{deploymentId}/restart", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> restartDeployment(@PathVariable String deploymentId) {
    log.info(String.format("--------- Restart deployment: %s --------", deploymentId));
    Deployment deployment = deploymentManagerService.retrieveDeployment(deploymentId);
    if (deployment != null) {
      deploymentManagerService.startDeploying(deploymentId);
      return new ResponseEntity<>(deployment, HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  /**
   * Stop a deployment.
   *
   * @param deploymentId id
   * @return ResponseEntity for the request
   */
  @RequestMapping(value = "/{deploymentId}/stop", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> stopDeployment(@PathVariable String deploymentId) {
    log.info(String.format("--------- Stop deployment: %s --------", deploymentId));
    Deployment deployment = deploymentManagerService.retrieveDeployment(deploymentId);
    if (deployment != null) {
      deployment = deploymentManagerService.stopDeploying(deployment);
      return new ResponseEntity<>(deployment, HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  /**
   * Retrieve a particular deployment.
   *
   * @param deploymentId id
   * @return ResponseEntity for the request
   */
  @RequestMapping(value = "/{deploymentId}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> retrieveDeployment(@PathVariable String deploymentId) {
    log.info(String.format("--------- Retrieve deployment: %s --------", deploymentId));
    return new ResponseEntity<>(
        deploymentManagerService.retrieveDeployment(deploymentId), HttpStatus.OK);
  }
}
