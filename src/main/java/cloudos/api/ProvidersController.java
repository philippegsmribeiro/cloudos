package cloudos.api;

import cloudos.Providers;
import cloudos.provider.ProvidersService;
import cloudos.security.WebSecurityConfig;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = ProvidersController.PROVIDERS)
@Log4j2
public class ProvidersController {


  public static final String PROVIDERS = WebSecurityConfig.API_PATH + "/providers";

  static final String REGIONS = "/regions";
  static final String REGIONS_BY_PROVIDER = "/regions/{provider}";
  static final String REGIONS_AVAIBILITY_ZONES = "/regions/avaibility_zones";
  static final String REGIONS_AVAIBILITY_ZONES_BY_PROVIDER = "/regions/{provider}/avaibility_zones";
  static final String IMAGES = "/images";
  static final String IMAGES_BY_PROVIDER = "/images/{provider}";
  static final String MACHINE_TYPES = "/machineTypes";
  static final String MACHINE_BY_PROVIDER = "/machineTypes/{provider}";
  static final String KEYS_BY_PROVIDER = "/keys/{provider}";
  static final String SECURITY_GROUPS_BY_PROVIDER = "/securityGroups/{provider}";

  private final ProvidersService providersService;

  @Autowired
  public ProvidersController(ProvidersService providersService) {
    this.providersService = providersService;
  }

  /**
   * Retrieve a list of providers.
   *
   * @return response for the request
   */
  @GetMapping
  @ResponseBody
  public ResponseEntity<?> retrieveProviders() {
    try {
      return ResponseEntity.ok(providersService.retrieveSupportedProviders());
    } catch (Exception e) {
      log.error("Error getting the providers", e);
    }
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
  }

  /**
   * Retrieve a list of all regions with zones.
   *
   * @return response for the request
   */
  @GetMapping(REGIONS_AVAIBILITY_ZONES)
  @ResponseBody
  public ResponseEntity<?> retrieveRegionsWithZones() {
    try {
      log.info("returning regions with zones");
      return ResponseEntity.ok(providersService.retrieveRegionsWithZones());
    } catch (Exception e) {
      log.error("Error getting the regions", e);
    }
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
  }

  /**
   * Retrieve a list of all regions.
   *
   * @return response for the request
   */
  @GetMapping(REGIONS)
  @ResponseBody
  public ResponseEntity<?> retrieveRegions() {
    try {
      log.info("returning regions with zones");
      return ResponseEntity.ok(providersService.retrieveRegions());
    } catch (Exception e) {
      log.error("Error getting the regions", e);
    }
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
  }

  /**
   * Retrieve a list of all regions of one provider.
   *
   * @return response for the request
   */
  @GetMapping(REGIONS_BY_PROVIDER)
  @ResponseBody
  public ResponseEntity<?> retrieveRegions(@PathVariable Providers provider) {
    log.info("returning regions " + provider);
    try {
      return ResponseEntity.ok(providersService.retrieveRegions(provider));
    } catch (Exception e) {
      log.error("Error getting the regions", e);
    }
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
  }

  /**
   * Retrieve a list of all regions with zones of one provider.
   *
   * @return response for the request
   */
  @GetMapping(REGIONS_AVAIBILITY_ZONES_BY_PROVIDER)
  @ResponseBody
  public ResponseEntity<?> retrieveRegionsAvaibilityZones(@PathVariable Providers provider) {
    log.info("returning regions avaibility zones " + provider);
    try {
      return ResponseEntity.ok(providersService.retrieveRegionsWithZones(provider));
    } catch (Exception e) {
      log.error("Error getting the regions", e);
    }
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
  }

  /**
   * Retrieve a list of all images.
   *
   * @return response for the request
   */
  @GetMapping(IMAGES)
  @ResponseBody
  public ResponseEntity<?> retrieveImages() {
    try {
      log.info("returning images");
      return new ResponseEntity<>(providersService.retrieveImages(), HttpStatus.OK);
    } catch (Exception e) {
      log.error("Error getting the images", e);
    }
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
  }

  /**
   * Retrieve a list of all images of one provider.
   *
   * @param provider the provider to be fetched
   * @return response for the request
   */
  @GetMapping(IMAGES_BY_PROVIDER)
  @ResponseBody
  public ResponseEntity<?> retrieveImages(@PathVariable Providers provider) {
    log.info("returning images " + provider);
    try {
      log.info("returning images");
      return new ResponseEntity<>(providersService.retrieveImages(provider), HttpStatus.OK);
    } catch (Exception e) {
      log.error("Error getting the images", e);
    }
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
  }

  /**
   * Retrieve a list of all machine types.
   *
   * @return response for the request
   */
  @GetMapping(MACHINE_TYPES)
  @ResponseBody
  public ResponseEntity<?> retrieveMachineTypes() {
    log.info("returning machineTypes");
    try {
      log.info("returning images");
      return new ResponseEntity<>(providersService.retrieveMachineTypes(), HttpStatus.OK);
    } catch (Exception e) {
      log.error("Error getting the machine types", e);
    }
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
  }

  /**
   * Retrieve a list of all machinetypes of one provider.
   *
   * @param provider the name of the provider
   * @return response for the request
   */
  @GetMapping(MACHINE_BY_PROVIDER)
  @ResponseBody
  public ResponseEntity<?> retrieveMachineTypes(@PathVariable Providers provider) {
    log.info("returning machineTypes " + provider);
    try {
      log.info("returning images");
      return new ResponseEntity<>(providersService.retrieveMachineTypes(provider), HttpStatus.OK);
    } catch (Exception e) {
      log.error("Error getting the machine types", e);
    }
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
  }

  /**
   * Retrieve a list of all keys of one provider.
   *
   * @param provider the name of the provider
   * @return response for the request
   */
  @GetMapping(KEYS_BY_PROVIDER)
  @ResponseBody
  public ResponseEntity<?> retrieveKeys(@PathVariable Providers provider) {
    log.info("returning keys " + provider);
    try {
      return new ResponseEntity<>(providersService.retrieveKeys(provider), HttpStatus.OK);
    } catch (Exception e) {
      log.error("Error getting keys", e);
    }
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
  }

  /**
   * Retrieve a list of all security groups of one provider.
   *
   * @param provider the name of the provider
   * @return response for the request
   */
  @GetMapping(SECURITY_GROUPS_BY_PROVIDER)
  @ResponseBody
  public ResponseEntity<?> retrieveSecurityGroups(@PathVariable Providers provider) {
    log.info("returning security groups " + provider);
    try {
      return new ResponseEntity<>(providersService.retrieveSecurityGroups(provider), HttpStatus.OK);
    } catch (Exception e) {
      log.error("Error getting security group", e);
    }
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
  }
}
