package cloudos.api;

import cloudos.Providers;
import cloudos.SecurityGroupManagerService;
import cloudos.exceptions.InvalidRequestException;
import cloudos.exceptions.NotImplementedException;
import cloudos.instances.CloudManagerService;
import cloudos.instances.InstanceRequestException;
import cloudos.keys.KeyManagerService;
import cloudos.keys.KeyManagerServiceException;
import cloudos.keys.cloudoskey.CloudosKey;
import cloudos.models.AbstractInstance;
import cloudos.models.CloudActionRequest;
import cloudos.models.CloudCreateKeyRequest;
import cloudos.models.CloudCreateRequest;
import cloudos.models.CloudosSecurityGroup;
import cloudos.models.network.CloudosSubnet;
import cloudos.models.network.CloudosSubnetRequest;
import cloudos.models.network.CloudosVpc;
import cloudos.models.network.CloudosVpcRequest;
import cloudos.models.network.SubnetUpdateRequest;
import cloudos.models.network.VpcUpdateRequest;
import cloudos.network.CloudosSubnetManagement;
import cloudos.network.CloudosVpcService;
import cloudos.notifications.NotificationType;
import cloudos.security.WebSecurityConfig;
import cloudos.securitygroup.CloudSecurityGroupException;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/** Created by philipperibeiro on 12/16/16. */
@RestController
@RequestMapping(value = CloudManagerController.CLOUDMANAGER)
@Log4j2
public class CloudManagerController {

  public static final String CLOUDMANAGER = WebSecurityConfig.API_PATH + "/cloud_manager";

  @Autowired
  private CloudManagerService cloudManagerService;

  @Autowired
  private KeyManagerService keyManagerService;

  @Autowired
  private SecurityGroupManagerService securityGroupManagerService;

  @Autowired
  private CloudosVpcService vpcService;

  @Autowired
  private CloudosSubnetManagement subnetManagement;

  /**
   * The method createInstance is responsible for creating instances in the given cloud provider. It
   * maps to the url '/cloud/create'.
   *
   * @param request A CloudCreateRequest, containing most of the information needed
   * @return HttpStatus.CREATED is successful
   */
  @RequestMapping(value = "/create", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> createInstances(@RequestBody CloudCreateRequest request) {
    try {
      List<? extends AbstractInstance> instances = cloudManagerService.createInstances(request);

      if (instances != null) {
        return new ResponseEntity<>(instances, HttpStatus.CREATED);
      }
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    } catch (NotImplementedException e) {
      log.error("Error", e);
      return new ResponseEntity<>(e, HttpStatus.NOT_IMPLEMENTED);
    } catch (InstanceRequestException e) {
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * The method stopInstances is responsible for terminating the instances specified in the request.
   * It maps to the `/cloud_manager/terminate` URL.
   *
   * @param request A CloudCreateRequest, containing most of the information needed
   * @return HttpStatus.CREATED is successful
   */
  @RequestMapping(value = "/terminate", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> terminateInstances(@RequestBody CloudActionRequest request) {
    try {
      request.setFireNotification(true);
      request.setNotificationType(NotificationType.HEALTHCHECKER);
      return new ResponseEntity<>(cloudManagerService.terminateInstances(request), HttpStatus.OK);
    } catch (NotImplementedException e) {
      return new ResponseEntity<>(e, HttpStatus.NOT_IMPLEMENTED);
    } catch (InvalidRequestException e) {
      return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
    } catch (InstanceRequestException e) {
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * The method stopInstances is responsible for listing all the instances specified in the request.
   * It maps to the `/cloud_manager/list` URL.
   *
   * @param request a CloudActionRequest
   * @return HttpStatus.OK is successful
   */
  @RequestMapping(value = "/list", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> listInstances(@RequestBody CloudActionRequest request) {
    try {
      return new ResponseEntity<>(cloudManagerService.listInstances(request), HttpStatus.OK);
    } catch (NotImplementedException e) {
      log.error("Error", e);
      return new ResponseEntity<>(e, HttpStatus.NOT_IMPLEMENTED);
    } catch (InstanceRequestException e) {
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * The method start is responsible for starting the instances specified in the request. It maps to
   * the `/cloud_manager/start` URL.
   *
   * @param request a CloudActionRequest
   * @return the status of the request
   */
  @RequestMapping(value = "/start", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> startInstances(@RequestBody CloudActionRequest request) {
    try {
      request.setFireNotification(true);
      request.setNotificationType(NotificationType.HEALTHCHECKER);
      return new ResponseEntity<>(cloudManagerService.startInstances(request), HttpStatus.OK);
    } catch (NotImplementedException e) {
      return new ResponseEntity<>(e, HttpStatus.NOT_IMPLEMENTED);
    } catch (InvalidRequestException e) {
      return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
    } catch (InstanceRequestException e) {
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * The method stopInstances is responsible for stopping the instances specified in the request. It
   * maps to the `/cloud_manager/stop` URL.
   *
   * @param request a CloudActionRequest
   * @return The status of the request
   */
  @RequestMapping(value = "/stop", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> stopInstances(@RequestBody CloudActionRequest request) {
    try {
      request.setFireNotification(true);
      request.setNotificationType(NotificationType.HEALTHCHECKER);
      return new ResponseEntity<>(cloudManagerService.stopInstances(request), HttpStatus.OK);
    } catch (NotImplementedException e) {
      return new ResponseEntity<>(e, HttpStatus.NOT_IMPLEMENTED);
    } catch (InvalidRequestException e) {
      return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
    } catch (InstanceRequestException e) {
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * The method rebootInstances is responsible for rebooting the instances specified in the request.
   * It maps to the `/cloud_manager/reboot` URL.
   *
   * @param request a CloudActionRequest
   * @return the status of the request
   */
  @RequestMapping(value = "/reboot", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> rebootInstances(@RequestBody CloudActionRequest request) {
    try {
      request.setFireNotification(true);
      request.setNotificationType(NotificationType.HEALTHCHECKER);
      return new ResponseEntity<>(cloudManagerService.rebootInstances(request), HttpStatus.OK);
    } catch (NotImplementedException e) {
      return new ResponseEntity<>(e, HttpStatus.NOT_IMPLEMENTED);
    } catch (InvalidRequestException e) {
      return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
    } catch (InstanceRequestException e) {
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Method to create a key.
   *
   * @param request a CloudCreateKeyRequest
   * @return the status of the request
   */
  @RequestMapping(value = "/create_keys", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> createKeys(@RequestBody CloudCreateKeyRequest request) {
    try {
      CloudosKey cloudosKey = this.keyManagerService.generateKey(request.getKeyName(),
          request.getProvider(), request.getRegion());

      return new ResponseEntity<>(cloudosKey, HttpStatus.OK);

    } catch (NotImplementedException e) {
      return new ResponseEntity<>(e, HttpStatus.NOT_IMPLEMENTED);
    } catch (KeyManagerServiceException e) {
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Method to save the private key.
   *
   * @param cloudosKey - with the private key
   * @return saved cloudosKey
   */
  @RequestMapping(value = "/save_private_key", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> savePrivateKey(@RequestBody CloudosKey cloudosKey) {
    try {
      cloudosKey = keyManagerService.savePrivateKey(cloudosKey);
      return new ResponseEntity<>(cloudosKey, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


  /**
   * Method to list a key.
   *
   * @param region to list
   * @return the status of the request
   */
  @RequestMapping(value = "/list_keys", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> listKeys(@RequestParam("region") String region) {

    return new ResponseEntity<>(this.keyManagerService.findKey(region), HttpStatus.OK);
  }

  /**
   * Method to list a key.
   *
   * @param providers to filter
   * @return the status of the request
   */
  @RequestMapping(value = "/list_keys_grouped/{providers}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> listKeys(@PathVariable Providers providers) {

    return new ResponseEntity<>(this.keyManagerService.findKeyByProviderGroupedByRegion(providers),
        HttpStatus.OK);
  }

  /**
   * Method to list a key.
   *
   * @param providers to filter
   * @return the status of the request
   */
  @RequestMapping(value = "/list_keys/{providers}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> listKeysByProvider(@PathVariable Providers providers) {
    return new ResponseEntity<>(this.keyManagerService.findKeyByProvider(providers), HttpStatus.OK);
  }

  /**
   * Delete a key.
   *
   * @param cloudosKeyId key id
   * @return the status of the request
   */
  @RequestMapping(value = "/delete_keys", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> deleteKeys(@RequestParam("cloudosKeyId") String cloudosKeyId) {
    try {
      CloudosKey cloudosKey = this.keyManagerService.deleteKey(cloudosKeyId);

      return new ResponseEntity<>(cloudosKey, HttpStatus.OK);

    } catch (NotImplementedException e) {
      return new ResponseEntity<>(e, HttpStatus.NOT_IMPLEMENTED);
    } catch (KeyManagerServiceException e) {
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Create a security group.
   *
   * @param cloudosSecurityGroup a CloudCreateSecurityGroupRequest
   * @return the status of the request
   */
  @RequestMapping(value = "/create_security_group", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> createSecurityGroup(
      @RequestBody CloudosSecurityGroup cloudosSecurityGroup) {
    try {
      cloudosSecurityGroup = this.securityGroupManagerService.create(cloudosSecurityGroup);

      return new ResponseEntity<>(cloudosSecurityGroup, HttpStatus.OK);

    } catch (NotImplementedException e) {
      log.error("create_security_group", e);

      return new ResponseEntity<>(e, HttpStatus.NOT_IMPLEMENTED);
    } catch (CloudSecurityGroupException e) {
      log.error("create_security_group", e);

      return new ResponseEntity<>(e, HttpStatus.EXPECTATION_FAILED);
    }
  }

  /**
   * List the security groups.
   *
   * @param providers to filter
   * @return the status of the request
   */
  @RequestMapping(value = "/list_security_group/{providers}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> listSecurityGroup(@PathVariable Providers providers) {

    List<CloudosSecurityGroup> securityGroups = this.securityGroupManagerService.list(providers);

    return new ResponseEntity<>(securityGroups, HttpStatus.OK);
  }

  /**
   * Delete a security group.
   *
   * @param cloudosSecurityGroup a CloudCreateSecurityGroupRequest
   * @return the status of the request
   */
  @RequestMapping(value = "/delete_security_group", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> deleteSecurityGroup(
      @RequestBody CloudosSecurityGroup cloudosSecurityGroup) {
    try {
      this.securityGroupManagerService.delete(cloudosSecurityGroup);

      return new ResponseEntity<>(true, HttpStatus.OK);
    } catch (CloudSecurityGroupException e) {
      log.error("create_security_group", e);

      return new ResponseEntity<>(e, HttpStatus.EXPECTATION_FAILED);
    } catch (NotImplementedException e) {
      log.error("create_security_group", e);

      return new ResponseEntity<>(e, HttpStatus.NOT_IMPLEMENTED);
    }
  }

  /**
   * Fetch a given security group based on its id.
   *
   * @param id the security group id
   * @return the status of the request
   */
  @RequestMapping(value = "/get_security_group/{id}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getSecurityGroup(@PathVariable("id") String id) {
    CloudosSecurityGroup securityGroup = this.securityGroupManagerService.getSecurityGroup(id);

    return new ResponseEntity<>(securityGroup, HttpStatus.OK);
  }

  /**
   * Fetch a list of all the instances based on its availability zone.
   *
   * @param region The given region to be fetched
   * @return the status of the request
   */
  @RequestMapping(value = "/availability_zones", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> getAvailabilityZones(@RequestParam("region") String region) {
    return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
  }

  /**
   * Rest API for the vpc service. It return all the instances which have not been deleted,
   * independently of the cloud provider.
   *
   * @return the status of the request
   */
  @RequestMapping(value = "/vpc", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> getVpcGroups(@RequestParam("provider") Providers provider,
                                        @RequestParam("region") String region) {

    log.info("VPC Request for provider {} and region {}", provider, region);
    // return all the CloudosVpc entries, independently of the provider.
    List<CloudosVpc> vpcList;
    try {
      vpcList = this.vpcService.list(provider, region);
    } catch (NotImplementedException e) {
      return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
    if (vpcList != null) {
      return new ResponseEntity<>(vpcList, HttpStatus.OK);
    }
    return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
  }

  /**
   * Get a particular VPC if it exists.
   *
   * @param vpcId The id of the given VPC
   * @return the status of the request
   */
  @RequestMapping(value = "/vpc/{vpcId}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> describeVpcGroup(@RequestParam("provider") Providers provider,
                                            @RequestParam("region") String region,
                                            @PathVariable("vpcId") String vpcId) {
    log.info("Describe VPC for provider {} with id {}", provider, vpcId);
    try {
      CloudosVpc vpc = this.vpcService.describe(provider, region, vpcId);
      if (vpc != null) {
        return new ResponseEntity<>(vpc, HttpStatus.OK);
      }
    } catch (NotImplementedException e) {
      log.error(e);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  /**
   * Rest API to create a new VPC given the information in the request, such as the provider and
   * region where the VPC will be created.
   *
   * @param cloudosVpcRequest The CloudosVpcRequest that contains the information about the current
   *        request
   * @return the status of the request
   */
  @RequestMapping(value = "/vpc", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> createVpcGroup(@RequestBody CloudosVpcRequest cloudosVpcRequest) {
    log.info("Cloudos VPC Request: {}", cloudosVpcRequest);
    if (cloudosVpcRequest != null) {
      try {
        CloudosVpc vpc = this.vpcService.create(cloudosVpcRequest);
        if (vpc == null) {
          // I will throw an exception that will be handled by the catch block.
          throw new Exception("Could not create the given VPC");
        }
        return new ResponseEntity<>(vpc, HttpStatus.OK);
      } catch (NotImplementedException e) {
        log.error(e.getMessage());
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
      } catch (Exception e) {
        log.error(e.getMessage());
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
      }
    }
    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
  }

  @RequestMapping(value = "/vpc/{vpcId}", method = RequestMethod.PUT)
  @ResponseBody
  public ResponseEntity<?> updateVpcGroup(@PathVariable("vpcId") String vpcId,
                                          @RequestBody VpcUpdateRequest request) {
    log.info("Cloudos VPC Update request: {}", request);
    if (request != null) {
      try {
        CloudosVpc vpc = this.vpcService.update(request);
        log.info("Newly updated VPC is {}", vpc);
        if (vpc != null) {
          return new ResponseEntity<>(vpc, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
      } catch (NotImplementedException e) {
        log.error(e.getMessage());
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
      }
    }
    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
  }

  /**
   * Rest API for the vpc service. It deletes the Vpc given in the request. If the VPC's provider
   * has not been implemented or not found, then returns an error.
   *
   * @param vpcId The id of a CloudosVpc request that contains the information about the current
   *        request.
   * @return the status of the request
   */
  @RequestMapping(value = "/vpc/{vpcId}", method = RequestMethod.DELETE)
  @ResponseBody
  public ResponseEntity<?> deleteVpcGroup(@RequestParam("provider") Providers provider,
                                          @RequestParam("region") String region,
                                          @PathVariable("vpcId") String vpcId) {
    try {
      CloudosVpc cloudosVpc = this.vpcService.describe(provider, region, vpcId);
      // Vpc was found
      if (cloudosVpc != null) {
        this.vpcService.delete(cloudosVpc);
        return new ResponseEntity<>(HttpStatus.OK);
      }
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } catch (NotImplementedException e) {
      log.error(e.getMessage());
      return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
  }

  /**
   * Create a subnet based on the cloud provider given by the user.
   *
   * @param vpcId the CloudosVpc id
   * @param request the request containing the subnet information
   * @return the status of the request
   */
  @RequestMapping(value = "/vpc/{vpcId}/subnet", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> createSubnet(@PathVariable("vpcId") String vpcId,
                                        @RequestParam("region") String region,
                                        @RequestBody CloudosSubnetRequest request) {
    try {
      CloudosVpc cloudosVpc = this.vpcService.describe(request.getProvider(), region, vpcId);

      // Vpc was found
      if (cloudosVpc != null) {
        CloudosSubnet subnet = this.subnetManagement.create(request, cloudosVpc);
        return new ResponseEntity<>(subnet, HttpStatus.OK);
      }
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } catch (NotImplementedException ex) {
      return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    } catch (Exception e) {
      log.error(e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * List all the subnets in a particular VPC.
   *
   * @param vpcId the CloudosVpc id
   * @return the status of the request
   */
  @RequestMapping(value = "/vpc/{vpcId}/subnet", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> listSubnets(@RequestParam("provider") Providers provider,
                                       @RequestParam("region") String region,
                                       @PathVariable("vpcId") String vpcId) {
    try {
      CloudosVpc cloudosVpc = this.vpcService.describe(provider, region, vpcId);
      // Vpc was found
      if (cloudosVpc != null) {
        List<CloudosSubnet> subnets = this.subnetManagement.list(cloudosVpc);
        return new ResponseEntity<>(subnets, HttpStatus.OK);
      }
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } catch (Exception e) {
      log.error(e.getMessage());
      return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
  }

  /**
   * Delete a particular subnet, based on its VPC.
   *
   * @param vpcId the CloudosVpc id
   * @return the status of the request
   */
  @RequestMapping(value = "/vpc/{vpcId}/subnet/{subnetId}", method = RequestMethod.DELETE)
  @ResponseBody
  public ResponseEntity<?> deleteSubnet(@RequestParam("provider") Providers provider,
                                        @RequestParam("region") String region,
                                        @PathVariable("vpcId") String vpcId,
                                        @PathVariable("subnetId") String subnetId) {
    try {
      CloudosVpc cloudosVpc = this.vpcService.describe(provider, region, vpcId);
      // Vpc was found
      if (cloudosVpc != null) {
        CloudosSubnet subnet = this.subnetManagement.describe(provider, region, vpcId, subnetId);
        if (subnet != null) {
          this.subnetManagement.delete(subnet, region);
          return new ResponseEntity<>(HttpStatus.OK);
        }
      }
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } catch (Exception e) {
      log.error(e.getMessage());
      return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
  }

  /**
   * Describe a particular subnet, if it exists. The VPC the subnet belongs to must be given.
   *
   * @param vpcId the CloudosVpc id
   * @return the status of the request
   */
  @RequestMapping(value = "/vpc/{vpcId}/subnet/{subnetId}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> describeSubnet(@RequestParam("provider") Providers provider,
                                          @RequestParam("region") String region,
                                          @PathVariable("vpcId") String vpcId,
                                          @PathVariable("subnetId") String subnetId) {
    try {
      CloudosVpc cloudosVpc = this.vpcService.describe(provider, region, vpcId);
      // Vpc was found
      if (cloudosVpc != null) {
        CloudosSubnet subnet = this.subnetManagement.describe(provider, region, vpcId, subnetId);
        if (subnet != null) {
          return new ResponseEntity<>(subnet, HttpStatus.OK);
        }
      }
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } catch (Exception e) {
      log.error(e.getMessage());
      return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
  }

  /**
   * Update a given subnet, if it exists. The VPC the subnet belongs to must be given.
   *
   * @param vpcId the CloudosVpc id
   * @param subnetId the subnet id
   * @return the status of the request
   */
  @RequestMapping(value = "/vpc/{vpcId}/subnet/{subnetId}", method = RequestMethod.PUT)
  @ResponseBody
  public ResponseEntity<?> updateSubnet(@PathVariable("vpcId") String vpcId,
                                        @PathVariable("subnetId") String subnetId,
                                        @RequestBody SubnetUpdateRequest request) {

    try {
      CloudosVpc cloudosVpc = this.vpcService.describe(request.getProvider(), request.getRegion(),
          vpcId);
      if (cloudosVpc != null) {
        CloudosSubnet subnet = this.subnetManagement.describe(request.getProvider(),
            request.getRegion(), vpcId, subnetId);
        if (subnet != null) {
          CloudosSubnet updated = this.subnetManagement.update(request);
          if (updated != null) {
            return new ResponseEntity<>(updated, HttpStatus.OK);
          }
        }
      }
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } catch (Exception e) {
      log.error(e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
