package cloudos.loadbalancer;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cloudos.Providers;
import cloudos.exceptions.NotImplementedException;
import cloudos.models.CloudosLoadBalancer;
import cloudos.models.CloudosLoadBalancerListener;

/**
 * Handles the load balancer managements services by provider
 * 
 * @author Rogério Souza
 *
 */
@Service
public class LoadBalancerManagerService {

  @Autowired
  private AmazonLoadBalancerManagement amazonLoadBalancerManagement;

  /**
   * Multiplex between the different load balancer management based on the provider.
   *
   * @param provider -The name of the provider
   * @param region - The region of the load balancer
   * @return a load balancer management
   * @throws NotImplementedException if the manager is not implemented for the given provider
   */
  @SuppressWarnings("rawtypes")
  private LoadBalancerManagement getManagement(Providers provider, String region)
      throws NotImplementedException {
    switch (provider) {
      case AMAZON_AWS:
        return amazonLoadBalancerManagement;
      default:
        break;
    }
    throw new NotImplementedException();
  }

  /**
   * Creates a new load balancer.
   * 
   * @param cloudosLoadBalancer - The load balancer request with attributes.
   * @return The created load balancer.
   * @throws LoadBalancerException - If any data validation or communication error is raised.
   */
  public CloudosLoadBalancer create(CloudosLoadBalancer cloudosLoadBalancer)
      throws LoadBalancerException, NotImplementedException {
    validateRequiredAttributes(cloudosLoadBalancer);
    return this.getManagement(cloudosLoadBalancer.getProvider(), cloudosLoadBalancer.getRegion())
        .create(cloudosLoadBalancer);
  }

  /**
   * Retrieves a load balancer.
   *
   * @param cloudosLoadBalancer - The load balancer request with attributes.
   * @return The updated load balancer.
   * @throws LoadBalancerException - If any data validation or communication error is raised.
   */
  public CloudosLoadBalancer describe(CloudosLoadBalancer cloudosLoadBalancer)
      throws LoadBalancerException, NotImplementedException {
    validateRequiredAttributes(cloudosLoadBalancer);
    return this.getManagement(cloudosLoadBalancer.getProvider(), cloudosLoadBalancer.getRegion())
        .describe(cloudosLoadBalancer);
  }

  /**
   * Deletes a load balancer.
   *
   * @param cloudosLoadBalancer - The load balancer request with attributes.
   * @throws LoadBalancerException - If any data validation or communication error is raised.
   */
  public void delete(CloudosLoadBalancer cloudosLoadBalancer)
      throws LoadBalancerException, NotImplementedException {
    validateRequiredAttributes(cloudosLoadBalancer);
    this.getManagement(cloudosLoadBalancer.getProvider(), cloudosLoadBalancer.getRegion())
        .delete(cloudosLoadBalancer);
  }

  /**
   * Sets the type of IP addresses used by the subnets of the specified Application Load Balancer or
   * Network Load Balancer.
   * 
   * @param cloudosLoadBalancer - The load balancer request with attributes.
   * @return the modified load balancer.
   * @throws LoadBalancerException
   */
  public CloudosLoadBalancer setIpAddressType(CloudosLoadBalancer cloudosLoadBalancer)
      throws LoadBalancerException, NotImplementedException {
    validateRequiredAttributes(cloudosLoadBalancer);
    return this.getManagement(cloudosLoadBalancer.getProvider(), cloudosLoadBalancer.getRegion())
        .setIpAddressType(cloudosLoadBalancer);
  }


  /**
   * Associates the specified security groups with the specified Application Load Balancer.
   * 
   * @param cloudosLoadBalancer - The load balancer request with attributes.
   * @return the modified load balancer.
   * @throws LoadBalancerException
   */
  public CloudosLoadBalancer setSecurityGroups(CloudosLoadBalancer cloudosLoadBalancer)
      throws LoadBalancerException, NotImplementedException {
    validateRequiredAttributes(cloudosLoadBalancer);
    return this.getManagement(cloudosLoadBalancer.getProvider(), cloudosLoadBalancer.getRegion())
        .setSecurityGroups(cloudosLoadBalancer);
  }

  /**
   * Enables the Availability Zone for the specified subnets for the specified Application Load
   * Balancer.
   * 
   * @param cloudosLoadBalancer - The load balancer request with attributes.
   * @return the modified load balancer.
   * @throws LoadBalancerException
   */
  public CloudosLoadBalancer setSubnets(CloudosLoadBalancer cloudosLoadBalancer)
      throws LoadBalancerException, NotImplementedException {
    validateRequiredAttributes(cloudosLoadBalancer);
    return this.getManagement(cloudosLoadBalancer.getProvider(), cloudosLoadBalancer.getRegion())
        .setSubnets(cloudosLoadBalancer);
  }

  /**
   * Creates a listener for the specified Load Balancer.
   * 
   * @param cloudosLoadBalancer - The load balancer request with attributes.
   * @param cloudosLoadBalancerListener - The listener request with attributes.
   * @throws LoadBalancerException
   */
  public void createListener(CloudosLoadBalancer cloudosLoadBalancer,
      CloudosLoadBalancerListener cloudosLoadBalancerListener)
      throws LoadBalancerException, NotImplementedException {
    validateRequiredAttributes(cloudosLoadBalancer);
    this.getManagement(cloudosLoadBalancer.getProvider(), cloudosLoadBalancer.getRegion())
        .createListener(cloudosLoadBalancer, cloudosLoadBalancerListener);
  }

  /**
   * Deletes the specified listener.
   * 
   * @param cloudosLoadBalancer - The load balancer request with attributes.
   * @param cloudosLoadBalancerListener - The listener request with attributes.
   * @throws LoadBalancerException
   */
  public void deleteListener(CloudosLoadBalancer cloudosLoadBalancer,
      CloudosLoadBalancerListener cloudosLoadBalancerListener)
      throws LoadBalancerException, NotImplementedException {
    validateRequiredAttributes(cloudosLoadBalancer);
    this.getManagement(cloudosLoadBalancer.getProvider(), cloudosLoadBalancer.getRegion())
        .deleteListener(cloudosLoadBalancer, cloudosLoadBalancerListener);
  }

  /**
   * Validates if the load balancer and region is not null/empty.
   * 
   * @param cloudosLoadBalancer
   * @throws LoadBalancerException
   */
  private void validateRequiredAttributes(CloudosLoadBalancer cloudosLoadBalancer)
      throws LoadBalancerException {
    if (cloudosLoadBalancer == null) {
      throw new LoadBalancerException("Load Balancer Request is null.");
    }

    if (StringUtils.isEmpty(cloudosLoadBalancer.getRegion())) {
      throw new LoadBalancerException("Region is null.");
    }

    if (cloudosLoadBalancer.getProvider() == null) {
      throw new LoadBalancerException("Provider is null.");
    }

  }



}
