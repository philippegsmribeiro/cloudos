package cloudos.loadbalancer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.elasticloadbalancing.model.ApplySecurityGroupsToLoadBalancerRequest;
import com.amazonaws.services.elasticloadbalancing.model.ApplySecurityGroupsToLoadBalancerResult;
import com.amazonaws.services.elasticloadbalancing.model.ConfigureHealthCheckRequest;
import com.amazonaws.services.elasticloadbalancing.model.CreateLoadBalancerListenersRequest;
import com.amazonaws.services.elasticloadbalancing.model.CreateLoadBalancerListenersResult;
import com.amazonaws.services.elasticloadbalancing.model.DeleteLoadBalancerListenersRequest;
import com.amazonaws.services.elasticloadbalancing.model.DeleteLoadBalancerListenersResult;
import com.amazonaws.services.elasticloadbalancing.model.LoadBalancerDescription;
import com.amazonaws.services.elasticloadbalancingv2.AmazonElasticLoadBalancing;
import com.amazonaws.services.elasticloadbalancingv2.AmazonElasticLoadBalancingClientBuilder;
import com.amazonaws.services.elasticloadbalancingv2.model.CreateListenerRequest;
import com.amazonaws.services.elasticloadbalancingv2.model.CreateListenerResult;
import com.amazonaws.services.elasticloadbalancingv2.model.CreateLoadBalancerRequest;
import com.amazonaws.services.elasticloadbalancingv2.model.CreateLoadBalancerResult;
import com.amazonaws.services.elasticloadbalancingv2.model.DeleteListenerRequest;
import com.amazonaws.services.elasticloadbalancingv2.model.DeleteLoadBalancerRequest;
import com.amazonaws.services.elasticloadbalancingv2.model.DeleteLoadBalancerResult;
import com.amazonaws.services.elasticloadbalancingv2.model.DescribeLoadBalancersRequest;
import com.amazonaws.services.elasticloadbalancingv2.model.LoadBalancer;
import com.amazonaws.services.elasticloadbalancingv2.model.SetIpAddressTypeRequest;
import com.amazonaws.services.elasticloadbalancingv2.model.SetIpAddressTypeResult;
import com.amazonaws.services.elasticloadbalancingv2.model.SetSecurityGroupsRequest;
import com.amazonaws.services.elasticloadbalancingv2.model.SetSecurityGroupsResult;
import com.amazonaws.services.elasticloadbalancingv2.model.SetSubnetsRequest;
import com.amazonaws.services.elasticloadbalancingv2.model.SetSubnetsResult;

import cloudos.amazon.AWSCredentialService;
import cloudos.models.AmazonLoadBalancer;
import cloudos.models.AmazonLoadBalancerListener;
import cloudos.models.AmazonLoadBalancerType;
import cloudos.models.CloudosLoadBalancerRepository;

/**
 * Implements all Amazon API for load balancer services.
 * 
 * @author Rogério Souza
 *
 */
@Service
public class AmazonLoadBalancerManagement
    implements LoadBalancerManagement<AmazonLoadBalancer, AmazonLoadBalancerListener> {

  public static final Logger logger = LogManager.getLogger(AmazonLoadBalancerManagement.class);

  @Autowired
  private CloudosLoadBalancerRepository cloudosLoadBalancerRepository;

  @Autowired
  private AWSCredentialService awsCredentialService;

  /**
   * HashMap that will store all AWS API clients by region
   * (com.amazonaws.services.elasticloadbalancingv2.AmazonElasticLoadBalancing).
   */
  private Map<String, AmazonElasticLoadBalancing> clientsByRegion = new HashMap<>();

  /**
   * HashMap that will store all AWS API clients by region
   * (com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancing).
   */
  private Map<String, com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancing> classicClientsByRegion =
      new HashMap<>();

  /**
   * Creates a new com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancing client if
   * it does not exist on the HashMap specifying which region should the object belongs to.
   *
   * @param region - The region.
   * @return A AmazonElasticLoadBalancing - the client created/retrieved by region.
   */
  private AmazonElasticLoadBalancing getClient(@NotNull String region)
      throws LoadBalancerException {

    if (StringUtils.isBlank(region)) {
      throw new LoadBalancerException("Region is required to create/retrieve a AWS client.");
    }

    AmazonElasticLoadBalancing client = clientsByRegion.get(region);

    if (client != null) {
      return client;
    }

    client = AmazonElasticLoadBalancingClientBuilder.standard()
        .withCredentials(awsCredentialService.getAWSCredentials()).withRegion(region).build();

    clientsByRegion.put(region, client);

    return client;

  }

  /**
   * Creates a new com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancing client if
   * it does not exist on the HashMap specifying which region should the object belongs to.
   *
   * @param region - The region.
   * @return A AmazonElasticLoadBalancing - the client created/retrieved by region.
   */
  private com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancing getClassicClient(
      @NotNull String region) throws LoadBalancerException {

    if (StringUtils.isBlank(region)) {
      throw new LoadBalancerException("Region is required to create/retrieve a AWS client.");
    }

    com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancing client =
        classicClientsByRegion.get(region);

    if (client != null) {
      return client;
    }

    client = com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancingClientBuilder
        .standard().withCredentials(awsCredentialService.getAWSCredentials()).withRegion(region)
        .build();

    classicClientsByRegion.put(region, client);

    return client;

  }

  /**
   * Creates a new load balancer.
   * 
   * @param loadBalancer - The load balancer request with attributes.
   * @return The created load balancer.
   * @throws LoadBalancerException - If any data validation or communication error is raised.
   */
  @Override
  public AmazonLoadBalancer create(@NotNull AmazonLoadBalancer loadBalancer)
      throws LoadBalancerException {

    if (loadBalancer.getType() == null) {
      throw new LoadBalancerException("The Type is null in the load balancer request.");
    }

    if (loadBalancer.getType().equals(AmazonLoadBalancerType.Classic)) {
      return createClassicLoadBalancer(loadBalancer);
    }

    CreateLoadBalancerRequest createLoadBalancerRequest = new CreateLoadBalancerRequest()
        .withName(loadBalancer.getName()).withScheme(loadBalancer.getScheme())
        .withSecurityGroups(loadBalancer.getSecurityGroups()).withSubnets(loadBalancer.getSubnets())
        .withType(loadBalancer.getType().toString());

    logger.info("Creating load balancer: {}", createLoadBalancerRequest.getName());
    try {
      CreateLoadBalancerResult result =
          this.getClient(loadBalancer.getRegion()).createLoadBalancer(createLoadBalancerRequest);

      if (result == null) {
        logger.error("Error while creating load balancer. The CreateLoadBalancerResult is null.");
        throw new LoadBalancerException(
            "Error while creating load balancer. The CreateLoadBalancerResult is null.");
      }

      if (CollectionUtils.isEmpty(result.getLoadBalancers())) {
        throw new LoadBalancerException("No load balancer was found after creation.");
      }

      loadBalancer.setLoadBalancerArn(result.getLoadBalancers().get(0).getLoadBalancerArn());

      return cloudosLoadBalancerRepository.insert(loadBalancer);

    } catch (Exception e) {
      logger.error("Caught an Exception while trying to communicate with LoadBalancer.");
      logger.error("Error Message: {}", e.getMessage());
      throw new LoadBalancerException(e.getMessage());
    }

  }

  /**
   * Creates a new CLASSIC load balancer.
   *
   * @param loadBalancer - The load balancer request with attributes.
   * @return the load balancer creation result or null if an error occurs
   * @throws LoadBalancerException
   */
  private AmazonLoadBalancer createClassicLoadBalancer(@NotNull AmazonLoadBalancer loadBalancer)
      throws LoadBalancerException {

    logger.info("Creating classic load balancer: {}", loadBalancer.getName());

    try {

      com.amazonaws.services.elasticloadbalancing.model.CreateLoadBalancerRequest createLoadBalancerRequest =
          new com.amazonaws.services.elasticloadbalancing.model.CreateLoadBalancerRequest()
              .withLoadBalancerName(loadBalancer.getName()).withScheme(loadBalancer.getScheme())
              .withSecurityGroups(loadBalancer.getSecurityGroups())
              .withSubnets(loadBalancer.getSubnets())
              .withAvailabilityZones(loadBalancer.getAvailabilityZones())
              .withListeners(convertToClassicListener(loadBalancer.getListeners()));

      this.getClassicClient(loadBalancer.getRegion()).createLoadBalancer(createLoadBalancerRequest);

      loadBalancer = cloudosLoadBalancerRepository.insert(loadBalancer);

      this.configureClassicHealthCheck(loadBalancer);

      return loadBalancer;

    } catch (Exception e) {
      logger.error("Caught an Exception while trying to communicate with LoadBalancer.");
      logger.error("Error Message: {}", e.getMessage());
      throw new LoadBalancerException(e.getMessage());
    }
  }

  /**
   * Specifies the health check settings to use when evaluating the health state of your EC2
   * instances.
   * 
   * @param loadBalancer - The load balancer request with attributes.
   * @return the updated load balancer or null if an error occurs.
   * @throws LoadBalancerException
   */
  private AmazonLoadBalancer configureClassicHealthCheck(@NotNull AmazonLoadBalancer loadBalancer)
      throws LoadBalancerException {

    if (StringUtils.isBlank(loadBalancer.getName())) {
      throw new LoadBalancerException(
          "The Load Balancer Name is null in the load balancer request.");
    }

    logger.info("Creating classic load balancer: {}", loadBalancer.getName());

    try {

      com.amazonaws.services.elasticloadbalancing.model.ConfigureHealthCheckRequest request =
          new ConfigureHealthCheckRequest().withHealthCheck(loadBalancer.getHealthCheck())
              .withLoadBalancerName(loadBalancer.getName());

      this.getClassicClient(loadBalancer.getRegion()).configureHealthCheck(request);

      return cloudosLoadBalancerRepository.save(loadBalancer);

    } catch (Exception e) {
      logger.error("Caught an Exception while trying to communicate with LoadBalancer.");
      logger.error("Error Message: {}", e.getMessage());
      throw new LoadBalancerException(e.getMessage());
    }
  }

  /**
   * Converts the list objects from AmazonLoadBalancerListener to
   * com.amazonaws.services.elasticloadbalancing.model.Listener
   * 
   * @param listeners - The list of AmazonLoadBalancerListener objects to be converted.
   * @return
   */
  private List<com.amazonaws.services.elasticloadbalancing.model.Listener> convertToClassicListener(
      List<AmazonLoadBalancerListener> listeners) {
    List<com.amazonaws.services.elasticloadbalancing.model.Listener> classicListeners =
        new ArrayList<>();
    for (AmazonLoadBalancerListener listener : listeners) {
      com.amazonaws.services.elasticloadbalancing.model.Listener classicListener =
          new com.amazonaws.services.elasticloadbalancing.model.Listener()
              .withInstancePort(listener.getInstancePort())
              .withInstanceProtocol(listener.getInstanceProtocol())
              .withLoadBalancerPort(listener.getLoadBalancerPort())
              .withProtocol(listener.getProtocol())
              .withSSLCertificateId(listener.getSSLCertificateId());
      classicListeners.add(classicListener);
    }
    return classicListeners;
  }


  /**
   * Deletes a load balancer by its Amazon Resource Name (ARN)
   *
   * @param loadBalancer - The load balancer request with attributes.
   * @throws LoadBalancerException - If any data validation or communication error is raised.
   */
  @Override
  public void delete(@NotNull AmazonLoadBalancer loadBalancer) throws LoadBalancerException {

    if (loadBalancer.getType() == null) {
      throw new LoadBalancerException("The type is null in the load balancer request.");
    }

    if (StringUtils.isBlank(loadBalancer.getId())) {
      throw new LoadBalancerException("The ID is null in the load balancer request.");
    }

    if (loadBalancer.getType().equals(AmazonLoadBalancerType.Classic)) {
      deleteClassicLoadBalancer(loadBalancer);
      return;
    }

    if (StringUtils.isBlank(loadBalancer.getLoadBalancerArn())) {
      throw new LoadBalancerException(
          "The Load Balancer Amazon Resource Name (ARN) is null in the request.");
    }

    logger.info("Deleting load balancer: {}", loadBalancer.getLoadBalancerArn());
    try {

      DeleteLoadBalancerRequest deleteLoadBalancerRequest =
          new DeleteLoadBalancerRequest().withLoadBalancerArn(loadBalancer.getLoadBalancerArn());

      DeleteLoadBalancerResult result =
          this.getClient(loadBalancer.getRegion()).deleteLoadBalancer(deleteLoadBalancerRequest);

      if (result == null) {
        throw new LoadBalancerException("Error while deleting load balancer.");
      }

      cloudosLoadBalancerRepository.delete(loadBalancer);

    } catch (Exception e) {
      logger.error("Caught an Exception while trying to communicate with LoadBalancer.");
      logger.error("Error Message: {}", e.getMessage());
      throw new LoadBalancerException(e.getMessage());
    }
  }

  /**
   * Deletes a load balancer by its name
   *
   * @param loadBalancer the load balancer name
   * @return the load balancer deletion result or null if an error occurs
   * @throws LoadBalancerException
   */
  private void deleteClassicLoadBalancer(@NotNull AmazonLoadBalancer loadBalancer)
      throws LoadBalancerException {

    if (StringUtils.isBlank(loadBalancer.getName())) {
      throw new LoadBalancerException(
          "The Load Balancer Name is null in the load balancer request.");
    }

    logger.info("Deleting classic load balancer: {}", loadBalancer.getName());

    try {

      com.amazonaws.services.elasticloadbalancing.model.DeleteLoadBalancerRequest deleteLoadBalancerRequest =
          new com.amazonaws.services.elasticloadbalancing.model.DeleteLoadBalancerRequest(
              loadBalancer.getName());

      this.getClassicClient(loadBalancer.getRegion()).deleteLoadBalancer(deleteLoadBalancerRequest);

      cloudosLoadBalancerRepository.delete(loadBalancer);

    } catch (Exception e) {
      logger.error("Caught an Exception while trying to communicate with LoadBalancer.");
      logger.error("Error Message: {}", e.getMessage());
      throw new LoadBalancerException(e.getMessage());
    }
  }

  /**
   * Retrieves a load balancer by its Amazon Resource Name (ARN)
   *
   * @param loadBalancer - The load balancer request with attributes.
   * @return The updated load balancer.
   * @throws LoadBalancerException - If any data validation or communication error is raised.
   */
  @Override
  public AmazonLoadBalancer describe(@NotNull AmazonLoadBalancer loadBalancer)
      throws LoadBalancerException {

    if (loadBalancer.getType() == null) {
      throw new LoadBalancerException("The Type is null in the load balancer request.");
    }

    if (loadBalancer.getType().equals(AmazonLoadBalancerType.Classic)) {
      return describeClassicLoadBalancer(loadBalancer);
    }

    if (StringUtils.isBlank(loadBalancer.getLoadBalancerArn())) {
      throw new LoadBalancerException(
          "The Load Balancer Amazon Resource Name (ARN) is null in the request.");
    }

    logger.info("Describing load balancer: {}", loadBalancer.getLoadBalancerArn());
    DescribeLoadBalancersRequest request =
        new DescribeLoadBalancersRequest().withLoadBalancerArns(loadBalancer.getLoadBalancerArn());

    try {
      List<LoadBalancer> loadBalancers = this.getClient(loadBalancer.getRegion())
          .describeLoadBalancers(request).getLoadBalancers();

      if (CollectionUtils.isEmpty(loadBalancers)) {
        logger.error("Error while describing load balancer. Load Balancer not found.");
        throw new LoadBalancerException(
            "Error while describing load balancer. Load Balancer not found.");
      }

      LoadBalancer awsLoadBalancer = loadBalancers.get(0);

      loadBalancer =
          AmazonLoadBalancer.builder().loadBalancerArn(awsLoadBalancer.getLoadBalancerArn())
              .name(awsLoadBalancer.getLoadBalancerName())
              .ipAddressType(awsLoadBalancer.getIpAddressType()).scheme(awsLoadBalancer.getScheme())
              .securityGroups(awsLoadBalancer.getSecurityGroups()).build();


      return loadBalancer;

    } catch (Exception e) {
      logger.error("Caught an Exception while trying to communicate with LoadBalancer.");
      logger.error("Error Message: {}", e.getMessage());
      throw new LoadBalancerException(e.getMessage());
    }

  }

  /**
   * Retrieves a load balancer by its Name
   *
   * @param loadBalancer - The load balancer request with attributes.
   * @return the load balancer or null.
   * @throws LoadBalancerException
   */
  private AmazonLoadBalancer describeClassicLoadBalancer(@NotNull AmazonLoadBalancer loadBalancer)
      throws LoadBalancerException {

    if (StringUtils.isBlank(loadBalancer.getName())) {
      throw new LoadBalancerException(
          "The Load Balancer Name is null in the load balancer request.");
    }

    logger.info("Describing load balancer: {}", loadBalancer.getName());

    try {

      com.amazonaws.services.elasticloadbalancing.model.DescribeLoadBalancersRequest request =
          new com.amazonaws.services.elasticloadbalancing.model.DescribeLoadBalancersRequest()
              .withLoadBalancerNames(loadBalancer.getName());

      List<LoadBalancerDescription> loadBalancers = this.getClassicClient(loadBalancer.getRegion())
          .describeLoadBalancers(request).getLoadBalancerDescriptions();

      if (CollectionUtils.isEmpty(loadBalancers)) {
        logger.error("Error while describing load balancer. Load Balancer not found.");
        throw new LoadBalancerException(
            "Error while describing load balancer. Load Balancer not found.");
      }

      LoadBalancerDescription awsLoadBalancer = loadBalancers.get(0);

      loadBalancer = AmazonLoadBalancer.builder().name(awsLoadBalancer.getLoadBalancerName())
          .scheme(awsLoadBalancer.getScheme()).securityGroups(awsLoadBalancer.getSecurityGroups())
          .build();

      return loadBalancer;

    } catch (Exception e) {
      logger.error("Caught an Exception while trying to communicate with LoadBalancer.");
      logger.error("Error Message: {}", e.getMessage());
      throw new LoadBalancerException(e.getMessage());
    }

  }


  /**
   * Sets the type of IP addresses used by the subnets of the specified Application Load Balancer or
   * Network Load Balancer.
   * 
   * @param loadBalancer - The load balancer request with attributes.
   * @return the modified load balancer.
   * @throws LoadBalancerException
   */
  @Override
  public AmazonLoadBalancer setIpAddressType(@NotNull AmazonLoadBalancer loadBalancer)
      throws LoadBalancerException {

    validateNetworkApplicationLoadBalancerRequest(loadBalancer);

    if (StringUtils.isBlank(loadBalancer.getIpAddressType())) {
      throw new LoadBalancerException("The IP Address Type is null in the request.");
    }

    logger.info("Modyfing IP address type of load balancer: {}", loadBalancer.getLoadBalancerArn());
    try {

      SetIpAddressTypeRequest request =
          new SetIpAddressTypeRequest().withLoadBalancerArn(loadBalancer.getLoadBalancerArn())
              .withIpAddressType(loadBalancer.getIpAddressType());

      SetIpAddressTypeResult result =
          this.getClient(loadBalancer.getRegion()).setIpAddressType(request);

      if (result == null) {
        logger.error("Error while setting IP address type. The SetIpAddressTypeResult is null.");
        throw new LoadBalancerException(
            "Error while setting IP address type. The SetIpAddressTypeResult is null.");
      }

      cloudosLoadBalancerRepository.save(loadBalancer);

      return loadBalancer;
    } catch (Exception e) {
      logger.error("Caught an Exception while trying to communicate with LoadBalancer.");
      logger.error("Error Message: {}", e.getMessage());
      throw new LoadBalancerException(e.getMessage());
    }
  }


  /**
   * Associates the specified security groups with the specified Application Load Balancer.
   * 
   * @param loadBalancer - The load balancer request with attributes.
   * @param securityGroups - The IDs of the security groups.
   * @return the modified load balancer.
   * @throws LoadBalancerException
   */
  @Override
  public AmazonLoadBalancer setSecurityGroups(@NotNull AmazonLoadBalancer loadBalancer)
      throws LoadBalancerException {

    if (loadBalancer.getType() == null) {
      throw new LoadBalancerException("The type is null in the load balancer request.");
    }

    if (loadBalancer.getType().equals(AmazonLoadBalancerType.Network)) {
      throw new LoadBalancerException(
          "Note that you can't specify a security group for a Network Load Balancer. This service does not support Network Load Balancers.");
    }

    if (CollectionUtils.isEmpty(loadBalancer.getSecurityGroups())) {
      throw new LoadBalancerException("The Security Groups list is empty in the request.");
    }

    if (loadBalancer.getType().equals(AmazonLoadBalancerType.Classic)) {
      return setClassicSecurityGroups(loadBalancer);
    }

    if (StringUtils.isBlank(loadBalancer.getLoadBalancerArn())) {
      throw new LoadBalancerException(
          "The Load Balancer Amazon Resource Name (ARN) is null in the request.");
    }

    logger.info("Modyfing security groups of load balancer: {}", loadBalancer.getLoadBalancerArn());
    try {

      SetSecurityGroupsRequest setSecurityGroupsRequest =
          new SetSecurityGroupsRequest().withLoadBalancerArn(loadBalancer.getLoadBalancerArn())
              .withSecurityGroups(loadBalancer.getSecurityGroups());

      SetSecurityGroupsResult result =
          this.getClient(loadBalancer.getRegion()).setSecurityGroups(setSecurityGroupsRequest);

      if (result == null) {
        logger.error("Error while setting security groups. The SetSecurityGroupsResult is null.");
        throw new LoadBalancerException(
            "Error while setting security groups. The SetSecurityGroupsResult is null.");
      }

      cloudosLoadBalancerRepository.save(loadBalancer);

      return loadBalancer;
    } catch (Exception e) {
      logger.error("Caught an Exception while trying to communicate with LoadBalancer.");
      logger.error("Error Message: {}", e.getMessage());
      throw new LoadBalancerException(e.getMessage());
    }
  }

  /**
   * Associates the specified security groups with the specified Classic Load Balancer.
   * 
   * @param loadBalancer - The load balancer request with attributes.
   * @param securityGroups - The IDs of the security groups.
   * @return the modified load balancer.
   * @throws LoadBalancerException
   */
  public AmazonLoadBalancer setClassicSecurityGroups(@NotNull AmazonLoadBalancer loadBalancer)
      throws LoadBalancerException {

    if (StringUtils.isBlank(loadBalancer.getName())) {
      throw new LoadBalancerException(
          "The Load Balancer Name is null in the load balancer request.");
    }

    logger.info("Modyfing security groups of load balancer: {}", loadBalancer.getLoadBalancerArn());
    try {

      ApplySecurityGroupsToLoadBalancerRequest request =
          new ApplySecurityGroupsToLoadBalancerRequest()
              .withLoadBalancerName(loadBalancer.getName())
              .withSecurityGroups(loadBalancer.getSecurityGroups());

      ApplySecurityGroupsToLoadBalancerResult result = this
          .getClassicClient(loadBalancer.getRegion()).applySecurityGroupsToLoadBalancer(request);

      cloudosLoadBalancerRepository.save(loadBalancer);

      return loadBalancer;
    } catch (Exception e) {
      logger.error("Caught an Exception while trying to communicate with LoadBalancer.");
      logger.error("Error Message: {}", e.getMessage());
      throw new LoadBalancerException(e.getMessage());
    }
  }

  /**
   * Enables the Availability Zone for the specified subnets for the specified Application Load
   * Balancer.
   * 
   * @param loadBalancer - The load balancer request with attributes.
   * @param subnets - The IDs of the subnets. You must specify subnets from at least two
   *        Availability Zones. You can specify only one subnet per Availability Zone.
   * @return the modified load balancer.
   * @throws LoadBalancerException
   */
  @Override
  public AmazonLoadBalancer setSubnets(@NotNull AmazonLoadBalancer loadBalancer)
      throws LoadBalancerException {

    validateNetworkApplicationLoadBalancerRequest(loadBalancer);

    if (loadBalancer.getType().equals(AmazonLoadBalancerType.Network)) {
      throw new LoadBalancerException(
          "Note that you can't change the subnets for a Network Load Balancer. The service does not support Network Load Balancers.");
    }

    if (CollectionUtils.isEmpty(loadBalancer.getSubnets())) {
      throw new LoadBalancerException("The Subnets list is empty in the request.");
    }

    logger.info("Modyfing subnets of load balancer: {}", loadBalancer.getLoadBalancerArn());
    try {

      SetSubnetsRequest setSubnetsRequest =
          new SetSubnetsRequest().withLoadBalancerArn(loadBalancer.getLoadBalancerArn())
              .withSubnets(loadBalancer.getSubnets());

      SetSubnetsResult result =
          this.getClient(loadBalancer.getRegion()).setSubnets(setSubnetsRequest);

      if (result == null) {
        logger.error("Error while setting subnets. The SetIpAddressTypeResult is null.");
        throw new LoadBalancerException(
            "Error while setting subnets. The SetIpAddressTypeResult is null.");
      }

      cloudosLoadBalancerRepository.save(loadBalancer);

      return loadBalancer;

    } catch (Exception e) {
      logger.error("Caught an Exception while trying to communicate with LoadBalancer.");
      logger.error("Error Message: {}", e.getMessage());
      throw new LoadBalancerException(e.getMessage());
    }
  }

  /**
   * Creates a listener for the specified Application Load Balancer or Network Load Balancer.
   * 
   * @param loadBalancer - The load balancer request with attributes.
   * @param amazonLoadBalancerListener - The listener request with attributes.
   * @return the listener creation result or null if an error occurs.
   * @throws LoadBalancerException
   */
  @Override
  public AmazonLoadBalancerListener createListener(@NotNull AmazonLoadBalancer loadBalancer,
      @NotNull AmazonLoadBalancerListener amazonLoadBalancerListener) throws LoadBalancerException {

    if (loadBalancer.getType() == null) {
      throw new LoadBalancerException("The type is null in the load balancer request.");
    }

    if (amazonLoadBalancerListener == null) {
      throw new LoadBalancerException("The Listener is null in the request.");
    }

    if (loadBalancer.getType().equals(AmazonLoadBalancerType.Classic)) {
      return createClassicListener(loadBalancer, amazonLoadBalancerListener);
    }

    if (StringUtils.isBlank(loadBalancer.getLoadBalancerArn())) {
      throw new LoadBalancerException(
          "The Load Balancer Amazon Resource Name (ARN) is null in the request.");
    }

    CreateListenerRequest request =
        new CreateListenerRequest().withLoadBalancerArn(loadBalancer.getLoadBalancerArn())
            .withPort(amazonLoadBalancerListener.getPort())
            .withProtocol(amazonLoadBalancerListener.getProtocol())
            .withSslPolicy(amazonLoadBalancerListener.getSslPolicy())
            .withCertificates(amazonLoadBalancerListener.getCertificates())
            .withDefaultActions(amazonLoadBalancerListener.getDefaultActions());

    logger.info("Creating listener of load balancer: {}", loadBalancer.getLoadBalancerArn());
    try {
      CreateListenerResult result =
          this.getClient(loadBalancer.getRegion()).createListener(request);

      if (result == null) {
        logger.error(
            "Error while creating load balancer listener. The CreateListenerResult is null.");
        throw new LoadBalancerException(
            "Error while creating load balancer listener. The CreateListenerResult is null.");
      }

      if (CollectionUtils.isEmpty(result.getListeners())) {
        throw new LoadBalancerException("No load balancer listener was found after creation.");
      }

      amazonLoadBalancerListener.setListenerArn(result.getListeners().get(0).getListenerArn());

      if (CollectionUtils.isEmpty(loadBalancer.getListeners())) {
        loadBalancer.setListeners(new ArrayList<>());
      }

      loadBalancer.getListeners().add(amazonLoadBalancerListener);

      cloudosLoadBalancerRepository.save(loadBalancer);

      return amazonLoadBalancerListener;
    } catch (Exception e) {
      logger.error("Caught an Exception while trying to communicate with LoadBalancer.");
      logger.error("Error Message: {}", e.getMessage());
      throw new LoadBalancerException(e.getMessage());
    }
  }

  /**
   * Creates a classic load balancer listener for the specified classic load balancer.
   * 
   * @param loadBalancer - The load balancer request with attributes.
   * @param amazonLoadBalancerListener - The listener request with attributes.
   * @return the listener creation result or null if an error occurs.
   * @throws LoadBalancerException
   */
  private AmazonLoadBalancerListener createClassicListener(@NotNull AmazonLoadBalancer loadBalancer,
      @NotNull AmazonLoadBalancerListener amazonLoadBalancerListener) throws LoadBalancerException {

    if (StringUtils.isBlank(loadBalancer.getName())) {
      throw new LoadBalancerException(
          "The Load Balancer Name is null in the load balancer request.");
    }

    CreateLoadBalancerListenersRequest request = new CreateLoadBalancerListenersRequest()
        .withLoadBalancerName(loadBalancer.getName()).withListeners(
            convertToClassicListener(Collections.singletonList(amazonLoadBalancerListener)));

    logger.info("Creating listener of load balancer: {}", loadBalancer.getLoadBalancerArn());
    try {
      CreateLoadBalancerListenersResult result =
          this.getClassicClient(loadBalancer.getRegion()).createLoadBalancerListeners(request);

      if (result == null) {
        logger.error(
            "Error while creating load balancer listener. The CreateListenerResult is null.");
        throw new LoadBalancerException(
            "Error while creating load balancer listener. The CreateListenerResult is null.");
      }

      if (CollectionUtils.isEmpty(loadBalancer.getListeners())) {
        loadBalancer.setListeners(new ArrayList<>());
      }

      cloudosLoadBalancerRepository.save(loadBalancer);

      return amazonLoadBalancerListener;
    } catch (Exception e) {
      logger.error("Caught an Exception while trying to communicate with LoadBalancer.");
      logger.error("Error Message: {}", e.getMessage());
      throw new LoadBalancerException(e.getMessage());
    }
  }

  /**
   * Deletes a classic load balancer listener from the specified Application Load Balancer or
   * Network Load Balancer.
   * 
   * @param loadBalancer - The load balancer request with attributes.
   * @return the listener delete result or null if an error occurs.
   * @throws LoadBalancerException
   */
  @Override
  public AmazonLoadBalancer deleteListener(@NotNull AmazonLoadBalancer loadBalancer,
      @NotNull AmazonLoadBalancerListener amazonLoadBalancerListener) throws LoadBalancerException {

    if (loadBalancer.getType() == null) {
      throw new LoadBalancerException("The type is null in the load balancer request.");
    }

    if (amazonLoadBalancerListener == null) {
      throw new LoadBalancerException("The Listener is null in the request.");
    }

    if (loadBalancer.getType().equals(AmazonLoadBalancerType.Classic)) {
      return deleteClassicListener(loadBalancer, amazonLoadBalancerListener);
    }

    if (StringUtils.isBlank(loadBalancer.getLoadBalancerArn())) {
      throw new LoadBalancerException(
          "The Load Balancer Amazon Resource Name (ARN) is null in the request.");
    }

    logger.info("Deleting listener : {}", amazonLoadBalancerListener.getListenerArn());
    try {

      DeleteListenerRequest deleteListenerRequest =
          new DeleteListenerRequest().withListenerArn(amazonLoadBalancerListener.getListenerArn());

      this.getClient(loadBalancer.getRegion()).deleteListener(deleteListenerRequest);

      if (CollectionUtils.isEmpty(loadBalancer.getListeners())) {
        loadBalancer.setListeners(new ArrayList<>());
      }

      loadBalancer.getListeners().remove(amazonLoadBalancerListener);

      return cloudosLoadBalancerRepository.save(loadBalancer);

    } catch (Exception e) {
      logger.error("Caught an Exception while trying to communicate with LoadBalancer.");
      logger.error("Error Message: {}", e.getMessage());
      throw new LoadBalancerException(e.getMessage());
    }
  }

  /**
   * Deleter a classic load balancer listener from the specified classic Load balancer.
   * 
   * @param loadBalancer - The load balancer request with attributes.
   * @param amazonLoadBalancerListener - The listener request with attributes.
   * @return the updated load balancer.
   * @throws LoadBalancerException
   */
  private AmazonLoadBalancer deleteClassicListener(@NotNull AmazonLoadBalancer loadBalancer,
      @NotNull AmazonLoadBalancerListener amazonLoadBalancerListener) throws LoadBalancerException {

    if (StringUtils.isBlank(loadBalancer.getName())) {
      throw new LoadBalancerException(
          "The Load Balancer Name is null in the load balancer request.");
    }

    DeleteLoadBalancerListenersRequest request =
        new DeleteLoadBalancerListenersRequest().withLoadBalancerName(loadBalancer.getName())
            .withLoadBalancerPorts(amazonLoadBalancerListener.getLoadBalancerPort());

    logger.info("Creating listener of load balancer: {}", loadBalancer.getLoadBalancerArn());
    try {
      DeleteLoadBalancerListenersResult result =
          this.getClassicClient(loadBalancer.getRegion()).deleteLoadBalancerListeners(request);

      if (result == null) {
        logger.error(
            "Error while creating load balancer listener. The CreateListenerResult is null.");
        throw new LoadBalancerException(
            "Error while creating load balancer listener. The CreateListenerResult is null.");
      }

      loadBalancer.getListeners().remove(amazonLoadBalancerListener);

      return cloudosLoadBalancerRepository.save(loadBalancer);

    } catch (Exception e) {
      logger.error("Caught an Exception while trying to communicate with LoadBalancer.");
      logger.error("Error Message: {}", e.getMessage());
      throw new LoadBalancerException(e.getMessage());
    }
  }

  private void validateNetworkApplicationLoadBalancerRequest(AmazonLoadBalancer loadBalancer)
      throws LoadBalancerException {
    if (loadBalancer.getType() == null) {
      throw new LoadBalancerException("The type is null in the load balancer request.");
    }

    if (loadBalancer.getType().equals(AmazonLoadBalancerType.Classic)) {
      throw new LoadBalancerException("The service does not support CLASSIC Load Balancers.");
    }

    if (StringUtils.isBlank(loadBalancer.getLoadBalancerArn())) {
      throw new LoadBalancerException(
          "The Load Balancer Amazon Resource Name (ARN) is null in the request.");
    }
  }

  // TODO: CLASSIC LOAD BALANCER
  /*
   * attachLoadBalancerToSubnets deregisterInstancesFromLoadBalancer detachLoadBalancerFromSubnets
   * registerInstancesWithLoadBalancer
   */

}
