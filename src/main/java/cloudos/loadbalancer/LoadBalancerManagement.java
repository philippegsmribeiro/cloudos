package cloudos.loadbalancer;

import cloudos.models.CloudosLoadBalancer;
import cloudos.models.CloudosLoadBalancerListener;

/**
 * Interface for the load balancer managements services.
 * 
 * @author Rogério Souza
 *
 */
public interface LoadBalancerManagement<T extends CloudosLoadBalancer, E extends CloudosLoadBalancerListener> {

  /**
   * Creates a new load balancer.
   * 
   * @param cloudosLoadBalancer - The load balancer request with attributes.
   * @return The created load balancer.
   * @throws LoadBalancerException - If any data validation or communication error is raised.
   */
  T create(T cloudosLoadBalancer) throws LoadBalancerException;

  /**
   * Retrieves a load balancer.
   *
   * @param cloudosLoadBalancer - The load balancer request with attributes.
   * @return The updated load balancer.
   * @throws LoadBalancerException - If any data validation or communication error is raised.
   */
  T describe(T cloudosLoadBalancer) throws LoadBalancerException;

  /**
   * Delete a load balancer.
   * 
   * @param cloudosLoadBalancer - The load balancer request with attributes.
   * @throws LoadBalancerException - If any data validation or communication error is raised.
   */
  void delete(T cloudosLoadBalancer) throws LoadBalancerException;

  /**
   * Sets the type of IP addresses used by the subnets of the specified Application Load Balancer or
   * Network Load Balancer.
   * 
   * @param loadBalancer - The load balancer request with attributes.
   * @return the modified load balancer.
   * @throws LoadBalancerException
   */
  T setIpAddressType(T cloudosLoadBalancer) throws LoadBalancerException;

  /**
   * Associates the specified security groups with the specified Application Load Balancer.
   * 
   * @param loadBalancer - The load balancer request with attributes.
   * @return the modified load balancer.
   * @throws LoadBalancerException
   */
  T setSecurityGroups(T cloudosLoadBalancer) throws LoadBalancerException;

  /**
   * Enables the Availability Zone for the specified subnets for the specified Application Load
   * Balancer.
   * 
   * @param loadBalancer - The load balancer request with attributes.
   * @return the modified load balancer.
   * @throws LoadBalancerException
   */
  T setSubnets(T cloudosLoadBalancer) throws LoadBalancerException;

  /**
   * Creates a listener for the specified Load Balancer.
   * 
   * @param loadBalancer - The load balancer request with attributes.
   * @param cloudosLoadBalancerListener - The listener request with attributes.
   * @throws LoadBalancerException
   */
  E createListener(T cloudosLoadBalancer, E cloudosLoadBalancerListener)
      throws LoadBalancerException;

  /**
   * Deletes the specified listener.
   * 
   * @param loadBalancer - The load balancer request with attributes.
   * @param cloudosLoadBalancerListener - The listener request with attributes.
   * @throws LoadBalancerException
   */
  public T deleteListener(T cloudosLoadBalancer, E cloudosLoadBalancerListener)
      throws LoadBalancerException;



}
