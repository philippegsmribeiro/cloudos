package cloudos.loadbalancer;

/**
 * To be used as the target group exception
 * 
 * @author Rogério Souza
 *
 */
public class LoadBalancerException extends Exception {
  private static final long serialVersionUID = -2828557088217365549L;

  public LoadBalancerException(String message) {
    super(message);
  }

  public LoadBalancerException(String message, Throwable cause) {
    super(message, cause);
  }
}
