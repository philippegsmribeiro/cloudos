package cloudos.credentials;

import cloudos.Providers;
import cloudos.encryptionKeys.AmazonKMSService;
import cloudos.encryptionKeys.AmazonKMSServiceException;
import cloudos.models.AzureCredential;
import cloudos.models.CloudosCredential;
import cloudos.models.CloudosCredentialRepository;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Created by gleimar on 21/05/2017. */
@Service
public class AzureCredentialManagement implements CredentialManagement<AzureCredential> {

  private final Logger logger = LoggerFactory.getLogger(AzureCredentialManagement.class);

  @Autowired
  private CloudosCredentialRepository cloudosCredentialRepository;

  @Autowired
  private AmazonKMSService amazonKMSService;

  @Override
  public CloudosCredential save(AzureCredential cloudosCredential)
      throws CloudCredentialActionException {
    if (StringUtils.isBlank(cloudosCredential.getClient())) {
      throw new CloudCredentialActionException("The client field need to be informed");
    }
    if (StringUtils.isBlank(cloudosCredential.getKey())) {
      throw new CloudCredentialActionException("The key field access key need to be informed");
    }
    if (StringUtils.isBlank(cloudosCredential.getTenant())) {
      throw new CloudCredentialActionException("The tenant field access key need to be informed");
    }

    encryptSensitiveData(cloudosCredential);

    return cloudosCredentialRepository.save(cloudosCredential);
  }


  @Override
  public boolean validateCredential(AzureCredential cloudosCredential) {
    // TODO Auto-generated method stub
    return true;
  }

  @Override
  public void updateActiveCredential(AzureCredential savedCredential) {
    // TODO Auto-generated method stub
  }

  /**
   * Encrypt credential's sensitive data based on Amazon customer master key (CMK) using AES algorithm.
   *
   * @param cloudosCredential - The credential to have sensitive data encrypted.
   * @return - The cloudosCredential with encrypted sensitive data.
   */
  @Override
  public AzureCredential encryptSensitiveData(AzureCredential cloudosCredential) {

    try {

      if (cloudosCredential.isDecrypted()) {

        String client = amazonKMSService.encryptMessage(
            amazonKMSService.getCredentialEncryptedDataKey(), cloudosCredential.getClient());
        cloudosCredential.setClient(client);

        String key = amazonKMSService.encryptMessage(amazonKMSService.getCredentialEncryptedDataKey(),
            cloudosCredential.getKey());
        cloudosCredential.setKey(key);

        cloudosCredential.setDecrypted(false);

      }

    } catch (AmazonKMSServiceException e) {
      logger.error("Error while encrypting AzureCredential:");
      logger.error(e.getMessage());
    }

    return cloudosCredential;
  }

  /**
   * Decrypt credential's sensitive data based on Amazon customer master key (CMK) using AES algorithm.
   *
   * @param cloudosCredential - The credential to have sensitive data decrypted.
   * @return - The cloudosCredential with encrypted sensitive data.
   */
  @Override
  public AzureCredential decryptSensitiveData(AzureCredential cloudosCredential) {

    try {

      if (!cloudosCredential.isDecrypted()) {

        String client = amazonKMSService.decryptMessage(
            amazonKMSService.getCredentialEncryptedDataKey(), cloudosCredential.getClient());
        cloudosCredential.setClient(client);

        String key = amazonKMSService.decryptMessage(amazonKMSService.getCredentialEncryptedDataKey(),
            cloudosCredential.getKey());
        cloudosCredential.setKey(key);

        cloudosCredential.setDecrypted(true);

      }

    } catch (AmazonKMSServiceException e) {
      logger.error("Error while decrypting AzureCredential:");
      logger.error(e.getMessage());
    }

    return cloudosCredential;
  }

  @Override
  public boolean hasCredential() {
    List<CloudosCredential> findByProvider =
        cloudosCredentialRepository.findByProvider(Providers.MICROSOFT_AZURE);
    return CollectionUtils.isNotEmpty(findByProvider);
  }
  
  @Override
  public void loadConfigFile(AzureCredential cloudosCredential) {
  }  
}
