package cloudos.credentials;

import cloudos.models.CloudosCredential;

/** Created by gleimar on 20/05/2017. */
public class CloudCredentialActionRequest {

  private CloudosCredential cloudosCredential;

  public CloudosCredential getCloudosCredential() {
    return cloudosCredential;
  }

  public void setCloudosCredential(CloudosCredential cloudosCredential) {
    this.cloudosCredential = cloudosCredential;
  }
}
