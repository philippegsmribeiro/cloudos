package cloudos.credentials;

import cloudos.amazon.AmazonS3Service;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cloudos.AmazonService;
import cloudos.Providers;
import cloudos.amazon.AWSCredentialService;
import cloudos.amazon.AWSCredentialSingleton;
import cloudos.amazon.CloudWatchService;
import cloudos.encryptionKeys.AmazonKMSService;
import cloudos.encryptionKeys.AmazonKMSServiceException;
import cloudos.models.AwsCredential;
import cloudos.models.CloudosCredential;
import cloudos.models.CloudosCredentialRepository;

/** Created by gleimar on 20/05/2017. */
@Service
public class AmazonCredentialManagement implements CredentialManagement<AwsCredential> {

  private final Logger logger = LoggerFactory.getLogger(AmazonCredentialManagement.class);

  @Autowired
  private CloudosCredentialRepository cloudosCredentialRepository;
  @Autowired
  private AmazonService amazonService;
  @Autowired
  private CloudWatchService cloudWatchService;
  @Autowired
  private AmazonS3Service amazonS3Service;
  @Autowired
  private AWSCredentialService awsCredentialService;

  @Autowired
  private AmazonKMSService amazonKMSService;

  @Override
  public CloudosCredential save(AwsCredential cloudosCredential)
      throws CloudCredentialActionException {
    if (StringUtils.isBlank(cloudosCredential.getAccessKeyId())) {
      logger.error("The access key need to be informed");
      throw new CloudCredentialActionException("The access key need to be informed");
    }
    if (StringUtils.isBlank(cloudosCredential.getSecretAccessKey())) {
      logger.error("The secret access key need to be informed");
      throw new CloudCredentialActionException("The secret access key need to be informed");
    }

    encryptSensitiveData(cloudosCredential);

    return this.cloudosCredentialRepository.save(cloudosCredential);
  }

  @Override
  public boolean validateCredential(AwsCredential cloudosCredential) {
    return amazonService.validateCredential(cloudosCredential);
  }

  @Override
  public void updateActiveCredential(AwsCredential savedCredential) {
    if (savedCredential != null) {
      awsCredentialService.changeCredentials(savedCredential);
      AWSCredentialSingleton.changeCredentials(savedCredential);
    } else {
      // deleting
      // TODO: block access?
    }
    // cleanup maps
    amazonService.resetMaps();
    cloudWatchService.resetMaps();
    amazonS3Service.resetMaps();
  }

  /**
   * Encrypt credential's sensitive data based on Amazon customer master key (CMK) using AES algorithm.
   *
   * @param cloudosCredential - The credential to have sensitive data encrypted.
   * @return - The cloudosCredential with encrypted sensitive data.
   * @throws throws CloudCredentialActionException
   */
  @Override
  public AwsCredential encryptSensitiveData(AwsCredential cloudosCredential) {

    try {

      if (cloudosCredential.isDecrypted()) {

        String accessKeyId = amazonKMSService.encryptMessage(
            amazonKMSService.getCredentialEncryptedDataKey(), cloudosCredential.getAccessKeyId());
        cloudosCredential.setAccessKeyId(accessKeyId);

        String secreAccessKey = amazonKMSService.encryptMessage(
            amazonKMSService.getCredentialEncryptedDataKey(), cloudosCredential.getSecretAccessKey());
        cloudosCredential.setSecretAccessKey(secreAccessKey);

        cloudosCredential.setDecrypted(false);

      }

    } catch (AmazonKMSServiceException e) {
      logger.error("Error while encrypting AwsCredential:");
      logger.error(e.getMessage());
    }

    return cloudosCredential;
  }

  /**
   * Decrypt credential's sensitive data based on Amazon customer master key (CMK) key using AES algorithm.
   *
   * @param cloudosCredential - The credential to have sensitive data decrypted.
   * @return - The cloudosCredential with encrypted sensitive data.
   */
  @Override
  public AwsCredential decryptSensitiveData(AwsCredential cloudosCredential) {

    try {

      if (!cloudosCredential.isDecrypted()) {

        String accessKeyId = amazonKMSService.decryptMessage(
            amazonKMSService.getCredentialEncryptedDataKey(), cloudosCredential.getAccessKeyId());

        cloudosCredential.setAccessKeyId(accessKeyId);

        String secreAccessKey = amazonKMSService.decryptMessage(
            amazonKMSService.getCredentialEncryptedDataKey(), cloudosCredential.getSecretAccessKey());

        cloudosCredential.setSecretAccessKey(secreAccessKey);

        cloudosCredential.setDecrypted(true);

      }

    } catch (AmazonKMSServiceException e) {
      logger.error("Error while decrypting AwsCredential:");
      logger.error(e.getMessage());
    }
    return cloudosCredential;
  }

  @Override
  public boolean hasCredential() {
    List<CloudosCredential> findByProvider =
        cloudosCredentialRepository.findByProvider(Providers.AMAZON_AWS);
    return CollectionUtils.isNotEmpty(findByProvider);
  }
  
  @Override
  public void loadConfigFile(AwsCredential cloudosCredential) {
  }    
}
