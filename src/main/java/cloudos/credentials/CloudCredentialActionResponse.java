package cloudos.credentials;

import cloudos.models.CloudosCredential;
import cloudos.models.CloudosResponse;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

/**
 * Action Response.
 *
 * @author flavio
 */
@Data
@NoArgsConstructor
public class CloudCredentialActionResponse extends CloudosResponse {

  public CloudCredentialActionResponse(
      HttpStatus status, String message, CloudosCredential cloudosCredential) {
    super(status, message);
    this.cloudosCredential = cloudosCredential;
  }

  private CloudosCredential cloudosCredential;

}
