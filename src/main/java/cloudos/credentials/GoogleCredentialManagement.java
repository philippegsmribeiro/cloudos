package cloudos.credentials;

import cloudos.Providers;
import cloudos.encryptionKeys.AmazonKMSService;
import cloudos.encryptionKeys.AmazonKMSServiceException;
import cloudos.google.GoogleCredential;
import cloudos.google.integration.GoogleComputeIntegration;
import cloudos.google.integration.GoogleMetricsIntegration;
import cloudos.google.integration.GoogleStorageIntegration;
import cloudos.models.CloudosCredential;
import cloudos.models.CloudosCredentialRepository;
import cloudos.storage.StorageService;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Created by gleimar on 21/05/2017. */
@Service
public class GoogleCredentialManagement implements CredentialManagement<GoogleCredential> {

  private final Logger logger = LoggerFactory.getLogger(GoogleCredentialManagement.class);

  @Autowired
  private CloudosCredentialRepository cloudosCredentialRepository;

  @Autowired
  private GoogleComputeIntegration googleComputeIntegration;

  @Autowired
  private GoogleMetricsIntegration googleMetricsIntegration;

  @Autowired
  private GoogleStorageIntegration googleStorageIntegration;

  @Autowired
  private StorageService storageService;

  @Autowired
  private AmazonKMSService amazonKmsService;

  @Override
  public CloudosCredential save(GoogleCredential cloudosCredential)
      throws CloudCredentialActionException {
    if (StringUtils.isBlank(cloudosCredential.getJsonCredential())) {
      throw new CloudCredentialActionException("The json credential field need to be informed");
    }
    if (StringUtils.isBlank(cloudosCredential.getProject())) {
      throw new CloudCredentialActionException("The project field access key need to be informed");
    }

    encryptSensitiveData(cloudosCredential);

    return cloudosCredentialRepository.save(cloudosCredential);
  }

  @Override
  public boolean validateCredential(GoogleCredential cloudosCredential) {
    boolean validateCredential = googleComputeIntegration.validateCredential(cloudosCredential);
    boolean validateCredentialMetric =
        googleMetricsIntegration.validateCredential(cloudosCredential);
    boolean validateCredentialStorage =
        googleStorageIntegration.validateCredential(cloudosCredential);
    return validateCredential && validateCredentialMetric && validateCredentialStorage;
  }

  @Override
  public void loadConfigFile(GoogleCredential cloudosCredential)
      throws CloudCredentialActionException {

    if (!StringUtils.isBlank(cloudosCredential.getJsonCredential())) {
      return;
    }

    if (StringUtils.isBlank(cloudosCredential.getConfigFilePath())) {
      throw new CloudCredentialActionException("Config file not found!");
    }

    // has an uploaded file
    try {
      Path filePath = storageService.load(cloudosCredential.getConfigFilePath());
      cloudosCredential.setConfigFilePath(filePath.toString());

      String jsonCredential = new String(Files.readAllBytes(filePath), Charset.forName("UTF-8"));

      if (StringUtils.isBlank(jsonCredential)) {
        throw new CloudCredentialActionException("Config file not found!");
      }

      cloudosCredential.setJsonCredential(jsonCredential);

    } catch (Exception e) {
      throw new CloudCredentialActionException("Config file not found!");
    }
  }

  @Override
  public void updateActiveCredential(GoogleCredential savedCredential) {
    if (savedCredential != null) {
      googleComputeIntegration.updateCredential(savedCredential);
      googleMetricsIntegration.updateCredential(savedCredential);
      googleStorageIntegration.updateCredential(savedCredential);
    } else {
      // TODO: block access?
    }
  }

  /**
   * Encrypt credential's sensitive data based on Amazon customer master key (CMK) using AES
   * algorithm.
   *
   * @param cloudosCredential - The credential to have sensitive data encrypted.
   * @return - The cloudosCredential with encrypted sensitive data.
   */
  @Override
  public GoogleCredential encryptSensitiveData(GoogleCredential cloudosCredential) {

    try {

      if (cloudosCredential.isDecrypted()) {

        String encrypted =
            amazonKmsService.encryptMessage(amazonKmsService.getCredentialEncryptedDataKey(),
                cloudosCredential.getJsonCredential());
        cloudosCredential.setJsonCredential(encrypted);

        cloudosCredential.setDecrypted(false);

      }

    } catch (AmazonKMSServiceException e) {
      logger.error("Error while encrypting GoogleCredential:");
      logger.error(e.getMessage());
    }

    return cloudosCredential;
  }

  /**
   * Decrypt credential's sensitive data based on a secret key using AES algorithm.
   *
   * @param cloudosCredential - The credential to have sensitive data decrypted.
   * @return - The cloudosCredential with encrypted sensitive data.
   */
  @Override
  public GoogleCredential decryptSensitiveData(GoogleCredential cloudosCredential) {
    try {

      if (!cloudosCredential.isDecrypted()) {

        String decrypted =
            amazonKmsService.decryptMessage(amazonKmsService.getCredentialEncryptedDataKey(),
                cloudosCredential.getJsonCredential());
        cloudosCredential.setJsonCredential(decrypted);

        cloudosCredential.setDecrypted(true);

      }

    } catch (AmazonKMSServiceException e) {
      logger.error("Error while decrypting GoogleCredential:");
      logger.error(e.getMessage());
    }

    return cloudosCredential;
  }

  @Override
  public boolean hasCredential() {
    List<CloudosCredential> findByProvider =
        cloudosCredentialRepository.findByProvider(Providers.GOOGLE_COMPUTE_ENGINE);
    return CollectionUtils.isNotEmpty(findByProvider);
  }
}
