package cloudos.credentials;

import cloudos.models.CloudosCredential;

/** Created by gleimar on 20/05/2017. */
public interface CredentialManagement<T extends CloudosCredential> {

  public CloudosCredential save(T cloudosCredential) throws CloudCredentialActionException;

  public void updateActiveCredential(T savedCredential);

  public boolean validateCredential(T cloudosCredential);

  /**
   * Encrypt credential's sensitive data based on Amazon customer master key (CMK) using AES
   * algorithm.
   *
   * @param cloudosCredential - The credential to have sensitive data encrypted.
   * @return - The cloudosCredential with encrypted sensitive data.
   */
  public T encryptSensitiveData(T cloudosCredential) throws CloudCredentialActionException;

  /**
   * Decrypt credential's sensitive data based on Amazon customer master key (CMK) using AES
   * algorithm.
   *
   * @param cloudosCredential - The credential to have sensitive data encrypted.
   * @return - The cloudosCredential with encrypted sensitive data.
   */
  public T decryptSensitiveData(T cloudosCredential) throws CloudCredentialActionException;

  /**
   * Validate if there is a credential for the provider.
   * 
   * @return true if there is, false otherwise
   */
  public boolean hasCredential();

  /**
   * Load configuration from an uploaded file.
   * 
   */
  public void loadConfigFile(T cloudosCredential) throws CloudCredentialActionException;
}
