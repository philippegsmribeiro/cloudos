package cloudos.credentials;

/** Created by gleimar on 21/05/2017. */
public class CloudCredentialActionException extends Exception {
  public CloudCredentialActionException(String message) {
    super(message);
  }

  public CloudCredentialActionException(String message, Throwable cause) {
    super(message, cause);
  }
}
