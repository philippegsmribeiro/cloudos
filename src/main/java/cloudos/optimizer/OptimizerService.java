package cloudos.optimizer;

import static java.sql.Date.valueOf;

import cloudos.Providers;
import cloudos.config.ThreadingConfig;
import cloudos.costanalysis.GenericBillingReportRepository;
import cloudos.exceptions.InvalidRequestException;
import cloudos.exceptions.NotImplementedException;
import cloudos.instances.CloudManagerService;
import cloudos.instances.InstanceRequestException;
import cloudos.instances.InstanceService;
import cloudos.machinelearning.spotbidder.SpotBidderRequestStatus;
import cloudos.models.CloudActionRequest;
import cloudos.models.CloudCreateRequest;
import cloudos.models.Instance;
import cloudos.models.autoscaler.AutoscaleRequest;
import cloudos.models.autoscaler.AutoscaleRequestRepository;
import cloudos.models.autoscaler.AutoscaleStatus;
import cloudos.models.cloudgroup.CloudGroup;
import cloudos.models.cloudgroup.CloudGroupService;
import cloudos.models.costanalysis.GenericBillingReport;
import cloudos.models.spotinstances.CloudSpotCreateRequest;
import cloudos.models.spotinstances.CloudSpotPricingRequest;
import cloudos.models.spotinstances.CloudSpotPricingResponse;
import cloudos.models.spotinstances.SpotProductDescription;
import cloudos.pricing.PricingManagementService;
import cloudos.pricing.PricingValue;
import cloudos.pricing.PricingValueUnit;
import cloudos.queue.QueueListener;
import cloudos.queue.QueueManagerService;
import cloudos.queue.message.autoscale.AutoscaleMessage;
import cloudos.queue.message.autoscale.AutoscaleMessage.AutoscaleMessageConsumer;
import cloudos.queue.message.spotbidder.SpotCreateResponseMessage;
import cloudos.queue.message.spotbidder.SpotCreateResponseMessage.SpotCreateResponseMessageConsumer;
import cloudos.queue.message.spotbidder.SpotPricingPredictResponseMessage;
import cloudos.queue.message.spotbidder.SpotPricingPredictResponseMessage.SpotPricingPredictResponseMessageConsumer;
import cloudos.utils.ProfileUtils;
import com.amazonaws.services.ec2.model.SpotInstanceType;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class OptimizerService {

  @Autowired
  private GenericBillingReportRepository genericBillingReportRepository;

  @Autowired
  private PricingManagementService pricingManagementService;

  @Autowired
  private InstanceService instanceService;

  @Autowired
  private QueueListener queueListener;

  @Autowired
  private QueueManagerService queueManagerService;

  @Autowired
  private CloudManagerService cloudManagerService;

  @Autowired
  private AutoscaleRequestRepository autoscaleRequestRepository;

  @Autowired
  private CloudGroupService cloudGroupService;

  @Autowired
  private ProfileUtils profileUtils;

  @Autowired
  @Qualifier(ThreadingConfig.OPTIMIZER)
  AsyncTaskExecutor taskExecutor;

  /**
   * Setup the consumer for the messages from the services.
   */
  @PostConstruct
  public void setup() {
    // listener for autoscaler messages
    this.queueListener.addConsumer((AutoscaleMessageConsumer) message -> {
      if (!profileUtils.isTest()) {
        // execute in a new thread
        taskExecutor.execute(() -> {
          consumeAutoscalerMessage(message);
        });
      } else {
        consumeAutoscalerMessage(message);
      }
    });

    // listener for spotpricing messages
    this.queueListener.addConsumer((SpotPricingPredictResponseMessageConsumer) message -> {
      if (!profileUtils.isTest()) {
        // execute in a new thread
        taskExecutor.execute(() -> {
          consumeSpotPricingPredictResponseMessage(message);
        });
      } else {
        consumeSpotPricingPredictResponseMessage(message);
      }
    });

    // listener for spot create response
    this.queueListener.addConsumer((SpotCreateResponseMessageConsumer) message -> {
      if (!profileUtils.isTest()) {
        // execute in a new thread
        taskExecutor.execute(() -> {
          consumeSpotCreateResponseMessage(message);
        });
      } else {
        consumeSpotCreateResponseMessage(message);
      }
    });

  }

  /**
   * Consume an Autoscaler message.
   *
   * @param message AutoscaleMessage to upscale or downscale.
   */
  private void consumeAutoscalerMessage(AutoscaleMessage message) {
    AutoscaleRequest request = message.getAutoscaleRequest();
    log.info("Consuming consumeAutoscalerMessage {}", request);

    // dont execute autoscaler on readonly mode
    if (!message.getAutoscaleRequest().isReadonly()) {
      switch (request.getLabel()) {
        case DOWNSCALE:
          this.downscale(request);
          break;
        case UPSCALE:
          this.upscale(request);
          break;
        default:
          log.warn("Invalid option. Nothing to do");
          break;
      }
    } else {
      log.info("Autoscaler not executed because application is on readonly mode!");
    }
  }

  /**
   * Consume a SpotPricingPredictResponse message.
   *
   * @param message with the pricing response
   */
  private void consumeSpotPricingPredictResponseMessage(SpotPricingPredictResponseMessage message) {
    log.info("Consuming consumeSpotPricingPredictResponseMessage {}",
        message.getSpotPricingResponse());
    try {
      //
      if (message.getAutoscaleRequestId() == null) {
        log.info("####################################");
        log.info("SPOT PRICING WITHOUT REQUESTID");
        log.info("####################################");
        return;
      }

      // autoscaler request id
      AutoscaleRequest autoscaleRequest =
          autoscaleRequestRepository.findOne(message.getAutoscaleRequestId());
      if (autoscaleRequest != null) {

        log.info("####################################");
        log.info("SPOT PRICING WITH REQUESTID {}", autoscaleRequest.getId());
        log.info("SPOT PRICING RESPONSE {}", message.getSpotPricingResponse());
        log.info("####################################");

        // response
        CloudSpotPricingResponse spotPricingResponse = message.getSpotPricingResponse();
        CloudCreateRequest req = autoscaleRequest.getCloudCreateRequest();
        PricingValue pricingForInstanceType = pricingManagementService.getPricingForInstanceType(
            req.getMachineType(), req.getRegion(), false, req.getImageProject(), req.getProvider());

        Double onDemandPrice = Double.MAX_VALUE;
        if (pricingForInstanceType != null && pricingForInstanceType.getValue() != 0) {
          onDemandPrice = pricingForInstanceType.getValue();
        }
        Double spotPrice = Double.MAX_VALUE;
        if (spotPricingResponse != null && spotPricingResponse.getPrice() != 0) {
          spotPrice = spotPricingResponse.getPrice();
        }

        CloudGroup cloudGroup =
            this.cloudGroupService.describeCloudGroup(autoscaleRequest.getCloudGroupId());

        log.info("####################################");
        log.info("Checking state for Cloud Group {}", cloudGroup);
        log.info("####################################");
        if (cloudGroup == null) {
          log.warn("Cloud Group {} does not exist", autoscaleRequest.getCloudGroupId());
        } else if (cloudGroup.getIsBlocked() != null && !cloudGroup.getIsBlocked().get()) {
          log.warn("Cloud group {} is not blocked.", cloudGroup);
        } else {

          log.info("####################################");
          log.info("PRICING: OD:{} vs SP:{}", onDemandPrice, spotPrice);
          log.info("####################################");

          // if onDemand has better price
          if (onDemandPrice.compareTo(spotPrice) <= 0) {

            log.info("####################################");
            log.info("Creating OnDemand instance(s)");
            log.info("####################################");

            autoscaleRequest.setStatus(AutoscaleStatus.AUTOSCALING_WITH_ON_DEMAND);
            autoscaleRequestRepository.save(autoscaleRequest);

            // create on demand
            req.setCloudGroup(cloudGroup.getId());
            List<?> instances = cloudManagerService.createInstances(req);
            instances.forEach(inst -> log.info("Started instance {}", inst));
            // update the cloud group
          } else {

            log.info("####################################");
            log.info("Requesting spot instance(s)");
            log.info("####################################");

            autoscaleRequest.setStatus(AutoscaleStatus.REQUESTING_CREATE_SPOT);
            autoscaleRequestRepository.save(autoscaleRequest);

            // create spot instances
            CloudSpotCreateRequest cloudSpotCreateRequest = new CloudSpotCreateRequest(req,
                spotPrice, 1, null, null, null, null, SpotInstanceType.OneTime, null);
            cloudSpotCreateRequest.setCloudGroup(cloudGroup.getId());
            queueManagerService.sendSpotCreateRequestMessage(cloudSpotCreateRequest,
                autoscaleRequest.getId());
          }
          // always release the cloud group
          this.releaseCloudGroup(cloudGroup);
        }

      }

    } catch (NotImplementedException | InstanceRequestException e) {
      log.error(e.getMessage(), e);
    }
  }


  /**
   * Consume a SpotCreateResponse message.
   *
   * @param message with the spot creation response
   */
  private void consumeSpotCreateResponseMessage(SpotCreateResponseMessage message) {
    log.info("Consuming consumeSpotCreateResponseMessage {}", message.getCloudSpotCreateResponse());
    try {
      if (message.getAutoscaleRequestId() == null) {
        log.info("####################################");
        log.info("SPOT CREATING WITHOUT REQUESTID");
        log.info("####################################");
        return;
      }

      // autoscaler request id
      AutoscaleRequest autoscaleRequest =
          autoscaleRequestRepository.findOne(message.getAutoscaleRequestId());

      if (autoscaleRequest != null) {

        log.info("####################################");
        log.info("SPOT CREATING WITH REQUESTID {}", autoscaleRequest.getId());
        log.info("SPOT CREATING RESPONSE {}", message.getCloudSpotCreateResponse());
        log.info("####################################");

        CloudCreateRequest req = autoscaleRequest.getCloudCreateRequest();
        //
        SpotBidderRequestStatus status = message.getCloudSpotCreateResponse().getStatus();
        if (status == SpotBidderRequestStatus.FULFILLED) {
          // fulfilled
          autoscaleRequest.setStatus(AutoscaleStatus.AUTOSCALING_WITH_SPOT);
          autoscaleRequestRepository.save(autoscaleRequest);

        } else if (status == SpotBidderRequestStatus.FAILED) {
          // failed
          autoscaleRequest.setStatus(AutoscaleStatus.AUTOSCALING_WITH_ON_DEMAND);
          autoscaleRequestRepository.save(autoscaleRequest);

          // create on demand
          List<?> current = cloudManagerService.createInstances(req);
          current.forEach(inst -> log.info("Started instance {}", inst));

        } else if (status == SpotBidderRequestStatus.INCOMPLETE
            || status == SpotBidderRequestStatus.PARTIALLY_FULFILLED) {
          // partially
          Integer createdInstances = message.getCloudSpotCreateResponse().getCreated();
          Integer notCreatedInstances = req.getCount() - createdInstances;

          autoscaleRequest.setStatus(AutoscaleStatus.AUTOSCALING_WITH_ON_DEMAND);
          autoscaleRequestRepository.save(autoscaleRequest);

          // create on demand
          req.setCount(notCreatedInstances);
          req.setMinCount(notCreatedInstances);
          List<?> current = cloudManagerService.createInstances(req);
          current.forEach(inst -> log.info("Started instance {}", inst));
        }

        CloudGroup cloudGroup =
            this.cloudGroupService.describeCloudGroup(autoscaleRequest.getCloudGroupId());

        log.info("####################################");
        log.info("Checking state for Cloud Group {}", cloudGroup);
        log.info("####################################");
        if (cloudGroup == null) {
          log.warn("Cloud Group {} does not exist", autoscaleRequest.getCloudGroupId());
        } else if (cloudGroup.getIsBlocked() != null && !cloudGroup.getIsBlocked().get()) {
          log.warn("Cloud group {} is not blocked.", cloudGroup);
        } else {
          // update the cloud group
          this.releaseCloudGroup(cloudGroup);
        }
      }
    } catch (NotImplementedException | InstanceRequestException e) {
      log.error(e.getMessage());
    }
  }


  /**
   * Count the active instances.
   *
   * @param provider to filter
   * @return number of instances
   */
  public Integer countInstances(Providers provider) {
    return getInstances(provider).size();
  }

  /**
   * Performs a downscale for the given cloud provider.
   *
   * @param request the autoscale request do downscale
   */
  @SuppressWarnings("unchecked")
  private void downscale(@NotNull AutoscaleRequest request) {
    CloudActionRequest cloudActionRequest = request.getActionRequest();
    log.info("Downscaling with request {}", cloudActionRequest);
    try {

      if (request.getCloudGroupId() == null) {
        // return - downscale not allowed without cloud group
        log.info("############################");
        log.info("DOWNSCALE WITHOUT CLOUDGROUP");
        log.info("############################");
        return;
      }

      CloudGroup cloudGroup = this.cloudGroupService.describeCloudGroup(request.getCloudGroupId());
      if (cloudGroup != null) {

        log.info("CLOUDGROUP: {}", cloudGroup);

        // number of instances in the group
        Integer count = this.instanceService.countInstancesByCloudGroupId(cloudGroup.getId());
        // number of instances to terminate
        int numberInstancesToTerminate = cloudActionRequest.getNames().size();
        // number of instances after downgrade
        int numberInstancesAfterDowngrade = count - numberInstancesToTerminate;

        //
        log.info("##########");
        log.info("VALIDATING: AD:{} vs MIN:{}", numberInstancesAfterDowngrade, cloudGroup.getMin());
        log.info("##########");

        // validate the min instances in the group
        if (numberInstancesAfterDowngrade >= cloudGroup.getMin()) {

          //
          log.info("############");
          log.info("DOWNSCALE {}", numberInstancesToTerminate);
          log.info("############");

          List<Instance> instances =
              (List<Instance>) cloudManagerService.terminateInstances(cloudActionRequest);
          if (instances != null) {
            instances.forEach(inst -> log.info("Terminated instance {}", inst));
            // obtain the instance ID of each instance
            List<String> instanceIds =
                instances.stream().map(Instance::getProviderId).collect(Collectors.toList());
            // 1. remove the instances from the cloud group.
            this.cloudGroupService.removeInstanceFromCloudGroup(request.getCloudGroupId(),
                instanceIds);
          }
        } else {
          //
          log.info("#########################################");
          log.info("DOWNSCALE NOT EXECUTED - COUNT: {} MIN: {}", count, numberInstancesToTerminate);
          log.info("#########################################");
        }
      } else {
        //
        // return - downscale not allowed without cloud group
        log.info("########################################################");
        log.info("DOWNSCALE  CLOUDGROUP NOT FOUND {}", request.getCloudGroupId());
        log.info("########################################################");
      }
    } catch (NotImplementedException | InvalidRequestException | InstanceRequestException e) {
      log.error(e.getMessage());
    }
  }

  /**
   * Performs an upscale for the given cloud provider.
   *
   * @param request the autoscale request
   */
  private void upscale(@NotNull AutoscaleRequest request) {
    CloudCreateRequest cloudCreateRequest = request.getCloudCreateRequest();
    log.info("Upscaling with request {}", cloudCreateRequest);

    if (request.getCloudGroupId() == null) {
      // return - upscale not allowed without cloud group
      log.info("############################");
      log.info("UPSCALE WITHOUT CLOUDGROUP");
      log.info("############################");
      return;
    }

    CloudGroup cloudGroup = this.cloudGroupService.describeCloudGroup(request.getCloudGroupId());
    if (cloudGroup != null) {

      log.info("CLOUDGROUP: {}", cloudGroup);

      // check if the cloud group is blocked
      // check if autoscale request is completed...
      if (cloudGroup.getIsBlocked() != null && cloudGroup.getIsBlocked().get()) {
        log.warn("Cloud Group {} ({}) is currently blocked. Skipping.", cloudGroup.getName(),
            request.getCloudGroupId());
        return;
      }

      // block the cloud group
      this.blockCloudGroup(cloudGroup);

      // number of instances in the group
      Integer count = this.instanceService.countInstancesByCloudGroupId(cloudGroup.getId());
      // number of instances to create
      int numberInstancesToCreate = cloudCreateRequest.getCount();
      // number of instances after upscale
      int numberInstancesAfterUpscale = count + numberInstancesToCreate;

      //
      log.info("##########");
      log.info("VALIDATING: AU:{} vs MAX:{}", numberInstancesAfterUpscale, cloudGroup.getMax());
      log.info("##########");

      // validate the min instances in the group
      if (numberInstancesAfterUpscale <= cloudGroup.getMax()) {

        //
        log.info("##########");
        log.info("UPSCALE {}", numberInstancesToCreate);
        log.info("##########");

        CloudSpotPricingRequest spotPricingRequest = CloudSpotPricingRequest.builder()
            .provider(cloudCreateRequest.getProvider()).region(cloudCreateRequest.getRegion())
            .availabilityZone(cloudCreateRequest.getZone()).startTime(new Date())
            // .endTime(new Date())
            .instanceTypes(Collections.singletonList(cloudCreateRequest.getMachineType()))
            .productDescription(
                Collections.singletonList(SpotProductDescription.AWS_LINUX_UNIX.toString()))
            .build();

        request.setStatus(AutoscaleStatus.REQUESTING_SPOT_PRICING);
        request = autoscaleRequestRepository.save(request);
        queueManagerService.sendSpotPricingPredictRequestMessage(spotPricingRequest,
            request.getId());
      } else {
        //
        log.info("#########################################");
        log.info("UPSCALE NOT EXECUTED - COUNT: {} MAX: {}", count, numberInstancesToCreate);
        log.info("#########################################");
        // always release the cloud group
        this.releaseCloudGroup(cloudGroup);
      }
    } else {
      // return - upscale not allowed without cloud group
      log.info("###########################################################");
      log.info("UPSCALE CLOUDGROUP NOT FOUND {}", request.getCloudGroupId());
      log.info("###########################################################");
    }
  }

  /**
   * Release the cloud group after the upscale be done.
   *
   * @param cloudGroup the cloud group in question
   * @return the unblocked and updated CloudGroup
   */
  private synchronized CloudGroup releaseCloudGroup(@NotNull CloudGroup cloudGroup) {
    log.info("+++++++++++++++++++++++++++++++++++++++++++++");
    log.info("RELEASING Cloud group {}. Number of Instances: {}", cloudGroup,
        instanceService.countInstancesByCloudGroupId(cloudGroup.getId()));
    log.info("+++++++++++++++++++++++++++++++++++++++++++++");
    // unblock the cloud group
    return this.cloudGroupService.setBlock(cloudGroup.getId(), false);
  }

  /**
   * Block the cloud group before the upscale be done.
   *
   * @param cloudGroup the cloud group in question
   * @return the blocked and updated CloudGroup
   */
  private synchronized CloudGroup blockCloudGroup(@NotNull CloudGroup cloudGroup) {
    log.info("+++++++++++++++++++++++++++++++++++++++++++++");
    log.info("BLOCKING Cloud group {}. Number of Instances: ", cloudGroup,
        instanceService.countInstancesByCloudGroupId(cloudGroup.getId()));
    log.info("+++++++++++++++++++++++++++++++++++++++++++++");
    // block the cloud group
    return this.cloudGroupService.setBlock(cloudGroup.getId(), true);
  }

  /**
   * Get all instances.
   *
   * @param provider to filter
   * @return List of Instances
   */
  private List<Instance> getInstances(Providers provider) {
    if (provider != null) {
      return instanceService.findAllActiveAndNotStoppedInstancesByProvider(provider);
    }
    return instanceService.findAllActiveAndNotStoppedInstances();
  }

  /**
   * Get the cost increase information.
   *
   * @param providers for filter
   * @return CostIncreaseResponse
   */
  public CostIncreaseResponse getCostIncrease(Providers providers) {
    LocalDate today = LocalDate.now();
    log.debug(today);

    // last month
    LocalDate firstDayOfLastMonth = today.minusMonths(1).with(TemporalAdjusters.firstDayOfMonth());
    LocalDate lastDayOfLastMonth = today.minusMonths(1).with(TemporalAdjusters.lastDayOfMonth());
    log.debug(firstDayOfLastMonth);
    log.debug(lastDayOfLastMonth);
    List<GenericBillingReport> lastMonthReports = this.getBillingReports(providers,
        valueOf(firstDayOfLastMonth), valueOf(lastDayOfLastMonth));
    Map<Providers, BigDecimal> collect = lastMonthReports.parallelStream()
        .filter(report -> report.getUsageCost().compareTo(BigDecimal.ZERO) > 0)
        .collect(Collectors.groupingBy(report -> report.getProvider(), Collectors
            .reducing(BigDecimal.ZERO, GenericBillingReport::getUsageCost, BigDecimal::add)));
    CostIncreaseResponse costIncreaseResponse = new CostIncreaseResponse();
    costIncreaseResponse.setLastMonth(collect);
    costIncreaseResponse.setLastMonthTotal(collect.values().parallelStream()
        .collect(Collectors.reducing(BigDecimal.ZERO, t -> t, BigDecimal::add)));

    // month-to-date - calculate from first day of the month to today
    LocalDate firstDayOfActualMonth = today.with(TemporalAdjusters.firstDayOfMonth());
    log.debug(firstDayOfActualMonth);
    log.debug(today);
    List<GenericBillingReport> monthToDateReports =
        this.getBillingReports(providers, valueOf(firstDayOfActualMonth), valueOf(today));
    Map<Providers, BigDecimal> collect2 = monthToDateReports.parallelStream()
        .filter(report -> report.getUsageCost().compareTo(BigDecimal.ZERO) > 0)
        .collect(Collectors.groupingBy(report -> report.getProvider(), Collectors
            .reducing(BigDecimal.ZERO, GenericBillingReport::getUsageCost, BigDecimal::add)));
    costIncreaseResponse.setMonthToDate(collect2);
    costIncreaseResponse.setMonthToDateTotal(collect2.values().parallelStream()
        .collect(Collectors.reducing(BigDecimal.ZERO, t -> t, BigDecimal::add)));

    // forecast - calculate from today to the last day of the month
    LocalDate lastDayOfActualMonth = today.with(TemporalAdjusters.lastDayOfMonth());
    log.debug(today);
    log.debug(lastDayOfActualMonth);
    Map<Providers, BigDecimal> forecastCost = this.getEstimateCost(providers,
        (lastDayOfActualMonth.getDayOfMonth() - today.getDayOfMonth()) + 1);
    costIncreaseResponse.setForecast(forecastCost);
    costIncreaseResponse.setForecastTotal(forecastCost.values().parallelStream()
        .collect(Collectors.reducing(BigDecimal.ZERO, t -> t, BigDecimal::add)));
    return costIncreaseResponse;
  }

  /**
   * Calculate the estimate cost for a provider.
   *
   * @param provider to calculate
   * @param days number of days
   * @return Map provider and cost amount
   */
  private Map<Providers, BigDecimal> getEstimateCost(Providers provider, int days) {
    return getInstances(provider).parallelStream().collect(Collectors.groupingBy(
        instance -> instance.getProvider(), Collectors.reducing(BigDecimal.ZERO, instance -> {
          try {
            PricingValue priceForInstance = pricingManagementService.getPriceForInstance(instance);
            if (priceForInstance != null) {
              BigDecimal value = new BigDecimal(priceForInstance.getValue());
              BigDecimal priceDays = value.multiply(new BigDecimal(days));
              log.info(priceForInstance);
              if (priceForInstance.getUnit().equals(PricingValueUnit.HRS)) {
                return priceDays.multiply(new BigDecimal(24));
              } else if (priceForInstance.getUnit().equals(PricingValueUnit.DAYS)) {
                return priceDays;
              } else {
                return priceDays;
              }
            }
          } catch (Exception e) {
            log.error("error getting estimate cost", e);
          }
          return BigDecimal.ZERO;
        }, BigDecimal::add)));
  }

  /**
   * Retrieves the billing reports from all implemented providers between a period.
   *
   * @param startDate the start date
   * @param endDate the end date
   */
  private List<GenericBillingReport> getBillingReports(Providers provider, Date startDate,
      Date endDate) {
    if (provider == null) {
      return genericBillingReportRepository.findByUsageStartTimeBetween(startDate, endDate);
    }
    return genericBillingReportRepository.findByProviderAndUsageStartTimeBetween(provider,
        startDate, endDate);
  }

  /**
   * Retrieve the costs of the last month till today, showing by services.
   *
   * @param providers to filter
   * @return Map service and cost
   */
  public Map<String, BigDecimal> getCostsByService(Providers providers) {
    LocalDate today = LocalDate.now();
    LocalDate firstDayOfLastMonth = today.minusMonths(3);
    LocalDate lastDayOfLastMonth = today;
    List<GenericBillingReport> billingReports = this.getBillingReports(providers,
        valueOf(firstDayOfLastMonth), valueOf(lastDayOfLastMonth));



    Map<String, BigDecimal> collect = billingReports.parallelStream()
        .filter(report -> report.getUsageCost().compareTo(BigDecimal.ZERO) > 0)
        .collect(Collectors.groupingBy(report -> getServiceByReport(report), Collectors
            .reducing(BigDecimal.ZERO, GenericBillingReport::getUsageCost, BigDecimal::add)));
    return collect.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
            (oldValue, newValue) -> oldValue, LinkedHashMap::new));
  }

  /**
   * Get the service name by report.
   *
   * @param report object
   * @return service name
   */
  private String getServiceByReport(GenericBillingReport report) {
    String productCode = report.getProductCode();
    if (!StringUtils.isBlank(productCode)) {
      if (report.getProvider() == Providers.GOOGLE_COMPUTE_ENGINE) {
        // com.google.cloud/services/compute-engine/NetworkInternetEgressApacCn
        productCode = productCode.replace("com.google.cloud/services/", "");
        if (productCode.length() > 0 && productCode.indexOf('/') != -1) {
          return productCode.substring(0, productCode.indexOf('/'));
        }
      }
      return productCode;
    }
    return String.format("No name - %s", report.getProvider().getShortName());
  }

}
