package cloudos.optimizer;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cloudos.Providers;
import cloudos.instances.InstanceService;
import cloudos.loadbalancer.LoadBalancerManagerService;
import cloudos.machinelearning.MLService;
import cloudos.machinelearning.models.CloudGraphCost;
import cloudos.models.CloudosLoadBalancer;
import cloudos.models.CloudosTargetGroup;
import cloudos.models.Instance;
import cloudos.models.cloudgroup.CloudGroup;
import cloudos.models.cloudgroup.CloudGroupFilterRequest;
import cloudos.models.cloudgroup.CloudGroupService;
import cloudos.optimizer.models.CloudEquivalenceRequest;
import cloudos.policies.CloudPolicy;
import cloudos.policies.CloudPolicyService;
import cloudos.security.WebSecurityConfig;
import cloudos.targetgroups.TargetGroupManagerService;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(value = OptimizerController.OPTIMIZER)
@Log4j2
public class OptimizerController {

  public static final String OPTIMIZER = WebSecurityConfig.API_PATH + "/optimizer";

  @Autowired
  private OptimizerService optimizerService;

  @Autowired
  private CloudGroupService cloudGroupService;

  @Autowired
  private MLService mlService;

  @Autowired
  private InstanceService instanceService;

  @Autowired
  private TargetGroupManagerService targetGroupManagerService;

  @Autowired
  private LoadBalancerManagerService loadBalancerManagerService;

  @Autowired
  private CloudPolicyService cloudPolicyService;

  /**
   * Count the number of active instances.
   * 
   * @return number of instances
   */
  @GetMapping("count/instances/")
  public ResponseEntity<?> getInstancesByProvider() {
    return this.getInstancesByProvider(null);
  }

  /**
   * Count the number of active instances for provider.
   * 
   * @param providers filter
   * @return number of instances
   */
  @GetMapping("count/instances/{providers}")
  public ResponseEntity<?> getInstancesByProvider(@PathVariable Providers providers) {
    return ResponseEntity.ok(optimizerService.countInstances(providers));
  }

  /**
   * Get the cost increase of the cloud.
   * 
   * @return costs
   */
  @GetMapping("costIncrease/")
  public ResponseEntity<?> getCosts() {
    return this.getCostIncrease(null);
  }

  /**
   * Get the costs increase of the cloud for provider.
   * 
   * @param providers filter
   * @return costs
   */
  @GetMapping("costIncrease/{providers}")
  public ResponseEntity<?> getCostIncrease(@PathVariable Providers providers) {
    return ResponseEntity.ok(optimizerService.getCostIncrease(providers));
  }

  /**
   * Get the costs by service of the cloud.
   * 
   * @return costs
   */
  @GetMapping("costsByService/")
  public ResponseEntity<?> getCostsByService() {
    return this.getCostsByService(null);
  }

  /**
   * Get the costs by service of the cloud for provider.
   * 
   * @param providers filter
   * @return costs
   */
  @GetMapping("costsByService/{providers}")
  public ResponseEntity<?> getCostsByService(@PathVariable Providers providers) {
    return ResponseEntity.ok(optimizerService.getCostsByService(providers));
  }

  /**
   * Create a new cloud group, if it already does not exist.
   *
   * @param cloudGroup the cloud group to be created
   * @return a response regarding the creation of the CloudGroup
   */
  @RequestMapping(value = "/cloud_group", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> createCloudGroup(@RequestBody CloudGroup cloudGroup) {
    CloudGroup group = this.cloudGroupService.createCloudGroup(cloudGroup);
    if (group != null) {
      return new ResponseEntity<>(group, HttpStatus.CREATED);
    }
    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * List all the cloud groups by provider. If the provider is null, then return all cloud groups.
   *
   * @return a response regarding the list of the CloudGroups
   */
  @RequestMapping(value = "/cloud_group", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> listCloudGroups() {
    List<CloudGroup> cloudGroups = this.cloudGroupService.getCloudGroups(null);
    return new ResponseEntity<>(cloudGroups, HttpStatus.OK);
  }

  /**
   * Describe a single cloud group, if it already does not exist.
   *
   * @param id the cloud group id to be described
   * @return a response regarding the description of the CloudGroup
   */
  @RequestMapping(value = "/cloud_group/{id}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> describeCloudGroup(@PathVariable String id) {
    CloudGroup group = this.cloudGroupService.describeCloudGroup(id);
    if (group != null) {
      return new ResponseEntity<>(group, HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  /**
   * Delete a single cloud group, if it already exists.
   *
   * @param id the cloud group id to be deleted
   * @return the status of the delete operation
   */
  @RequestMapping(value = "/cloud_group/{id}", method = RequestMethod.DELETE)
  @ResponseBody
  public ResponseEntity<?> deleteCloudGroup(@PathVariable String id) {
    boolean status = this.cloudGroupService.deleteCloudGroup(id);
    if (status) {
      return new ResponseEntity<>(HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }


  @RequestMapping(value = "/cloud_cost_comparison", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> cloudCostComparison(@RequestBody CloudEquivalenceRequest request) {
    CloudGraphCost graphCost = this.mlService.cloudCostComparison(request);
    if (graphCost != null) {
      return new ResponseEntity<>(graphCost, HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * Add instance(s) to cloudGroup.
   * 
   * @param groupId id
   * @param instances id list
   * @return cloudGroup
   */
  @RequestMapping(value = "/cloud_group/{groupId}/addInstance/", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> addInstanceToCloudGroup(@PathVariable String groupId,
      @RequestBody List<String> instances) {
    CloudGroup group = this.cloudGroupService.addInstanceToCloudGroup(groupId, instances);
    if (group != null) {
      return new ResponseEntity<>(group, HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * Remove instance from cloudGroup.
   * 
   * @param groupId id
   * @param instances ids
   * @return cloudgroup
   */
  @RequestMapping(value = "/cloud_group/{groupId}/removeInstance/", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> removeInstanceFromCloudGroup(@PathVariable String groupId,
      @RequestBody List<String> instances) {
    CloudGroup group = this.cloudGroupService.removeInstanceFromCloudGroup(groupId, instances);
    if (group != null) {
      return new ResponseEntity<>(group, HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * Retrieve instances associated to the cloud group.
   *
   * @param id the cloud group id
   * @return a list of instances
   */
  @RequestMapping(value = "/cloud_group/{id}/instances", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> retrieveCloudGroupInstances(@PathVariable String id) {
    List<Instance> instances = this.instanceService.findAllByCloudGroupId(id);
    if (instances != null) {
      return new ResponseEntity<>(instances, HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  /**
   * Retrieve instances associated to the cloud group.
   *
   * @param request the cloud group filter request
   * @return a list of instances that do not have a cloud group
   */
  @RequestMapping(value = "/cloud_group/getInstancesWithoutCloudGroup", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> getInstancesWithoutCloudGroup(
      @RequestBody CloudGroupFilterRequest request) {
    if (request.getProvider() == null) {
      log.warn("Provider is null in the request");
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    List<Instance> instances = this.instanceService.findAllWithoutCloudGroup(request);
    if (instances != null) {
      return new ResponseEntity<>(instances, HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * Create a new cloud policy, and append to an existing cloud group.
   *
   * @param groupId the cloud group ID
   * @param cloudPolicy the new cloud policy to be added
   * @return response
   */
  @RequestMapping(value = "/cloud_group/{groupId}/policies", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> createCloudPolicy(@PathVariable String groupId,
                                             @RequestBody CloudPolicy cloudPolicy) {
    CloudPolicy newCloudPolicy = this.cloudPolicyService.save(groupId, cloudPolicy);
    if (newCloudPolicy != null) {
      return new ResponseEntity<>(newCloudPolicy, HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * Describe an existing Cloud Policy, if it exists.
   *
   * @param groupId the Cloud Group id
   * @param id the Cloud Policy ID
   * @return response
   */
  @RequestMapping(value = "/cloud_group/{groupId}/policies/{id}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> describeCloudPolicy(@PathVariable String groupId,
                                               @PathVariable String id) {
    CloudPolicy cloudPolicy = this.cloudPolicyService.describe(groupId, id);
    if (cloudPolicy != null) {
      return new ResponseEntity<>(cloudPolicy, HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  /**
   * List all the cloud policies in a particular cloud group.
   *
   * @param groupId the cloud group ID
   * @return response
   */
  @RequestMapping(value = "/cloud_group/{groupId}/policies", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<?> listCloudPolicy(@PathVariable String groupId) {
    List<CloudPolicy> cloudPolicies = this.cloudPolicyService.list(groupId);
    if (cloudPolicies != null) {
      return new ResponseEntity<>(cloudPolicies, HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  /**
   * Update a cloud policy, if it exists.
   *
   * @param groupId the cloud group ID
   * @param id the cloud policy ID
   * @param cloudPolicy the cloud policy object with the updated information
   * @return response
   */
  @RequestMapping(value = "/cloud_group/{groupId}/policies/{id}", method = RequestMethod.PUT)
  @ResponseBody
  public ResponseEntity<?> updateCloudPolicy(@PathVariable String groupId,
                                             @PathVariable String id,
                                             @RequestBody CloudPolicy cloudPolicy) {
    CloudPolicy policy = this.cloudPolicyService.update(groupId, id, cloudPolicy);
    if (policy != null) {
      return new ResponseEntity<>(policy, HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * Delete a cloud policy from a cloud group, if the cloud policy exists.
   *
   * @param groupId the cloud group ID
   * @param id the cloud policy ID
   * @return response
   */
  @RequestMapping(value = "/cloud_group/{groupId}/policies/{id}", method = RequestMethod.DELETE)
  @ResponseBody
  public ResponseEntity<?> deleteCloudPolicy(@PathVariable String groupId,
                                             @PathVariable String id) {
    // find the cloud policy
    // remove it from the cloud group
    // update the cloud group
    CloudPolicy cloudPolicy = this.cloudPolicyService.describe(groupId, id);
    if (cloudPolicy == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    // delete the cloud policy from the repository
    if (!this.cloudPolicyService.delete(groupId, id)) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
    // remove it from the cloud group
    CloudGroup cloudGroup = this.cloudGroupService.describeCloudGroup(groupId);
    // find the matching policy
    return new ResponseEntity<>(HttpStatus.OK);
  }

  /**
   * Creates a new target group.
   * 
   * @param cloudosTargetGroup - The target group request with attributes.
   * @return The created target group.
   */
  @RequestMapping(value = "/target_group/create", method = RequestMethod.POST)
  @ResponseBody
  @SuppressWarnings("Duplicates")
  public ResponseEntity<?> createTargetGroup(
      @RequestBody @NotNull CloudosTargetGroup cloudosTargetGroup) {

    if (!targetGroupRequestIsValid(cloudosTargetGroup)) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    try {
      cloudosTargetGroup = targetGroupManagerService.create(cloudosTargetGroup);
      return new ResponseEntity<>(cloudosTargetGroup, HttpStatus.OK);
    } catch (Exception e) {
      log.error("Error", e);
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  /**
   * Retrieves a target group.
   *
   * @param cloudosTargetGroup - The target group request with attributes.
   * @return The updated target group.
   */
  @RequestMapping(value = "/target_group/describe", method = RequestMethod.POST)
  @ResponseBody
  @SuppressWarnings("Duplicates")
  public ResponseEntity<?> describeTargetGroup(
      @RequestBody @NotNull CloudosTargetGroup cloudosTargetGroup) {

    if (!targetGroupRequestIsValid(cloudosTargetGroup)) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    try {
      cloudosTargetGroup = targetGroupManagerService.describe(cloudosTargetGroup);
      return new ResponseEntity<>(cloudosTargetGroup, HttpStatus.OK);
    } catch (Exception e) {
      log.error("Error", e);
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  /**
   * Modifies the health checks used when evaluating the health state of the targets in the
   * specified target group.
   * 
   * @param cloudosTargetGroup - The target group request with attributes.
   * @return The updated target group.
   */
  @RequestMapping(value = "/target_group/modify", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> modifyTargetGroup(
      @RequestBody @NotNull CloudosTargetGroup cloudosTargetGroup) {

    if (!targetGroupRequestIsValid(cloudosTargetGroup)) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    try {
      cloudosTargetGroup = targetGroupManagerService.modify(cloudosTargetGroup);
      return new ResponseEntity<>(cloudosTargetGroup, HttpStatus.OK);
    } catch (Exception e) {
      log.error("Error", e);
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  /**
   * Deletes a target group.
   *
   * @param cloudosTargetGroup - The target group request with attributes.
   */
  @RequestMapping(value = "/target_group/delete", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> deleteTargetGroup(
      @RequestBody @NotNull CloudosTargetGroup cloudosTargetGroup) {

    if (!targetGroupRequestIsValid(cloudosTargetGroup)) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    try {
      targetGroupManagerService.delete(cloudosTargetGroup);
      return new ResponseEntity<>(cloudosTargetGroup, HttpStatus.OK);
    } catch (Exception e) {
      log.error("Error", e);
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  /**
   * Check if the target cloud is valid.
   *
   * @param cloudosTargetGroup the cloudos target group
   * @return true if is valid otherwise false when invalid
   */
  private boolean targetGroupRequestIsValid(CloudosTargetGroup cloudosTargetGroup) {
    if (StringUtils.isEmpty(cloudosTargetGroup.getRegion())) {
      log.error("Region is null in the request.");
      return false;
    }

    if (cloudosTargetGroup.getProvider() == null) {
      log.error("Provider is null in the request");
      return false;
    }

    return true;
  }

  /**
   * Creates a new load balancer.
   * 
   * @param cloudosLoadBalancer - The load balancer request with attributes.
   * @return The created load balancer.
   */
  @RequestMapping(value = "/load_balancer/create", method = RequestMethod.POST)
  @ResponseBody
  @SuppressWarnings("Duplicates")
  public ResponseEntity<?> createLoadBalancer(
      @RequestBody @NotNull CloudosLoadBalancer cloudosLoadBalancer) {

    if (!loadBalancerRequestIsValid(cloudosLoadBalancer)) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    try {
      cloudosLoadBalancer = loadBalancerManagerService.create(cloudosLoadBalancer);
      return new ResponseEntity<>(cloudosLoadBalancer, HttpStatus.OK);
    } catch (Exception e) {
      log.error("Error", e);
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  /**
   * Describes an existing load balancer.
   * 
   * @param cloudosLoadBalancer - The load balancer request with attributes.
   * @return The load balancer.
   */
  @RequestMapping(value = "/load_balancer/describe", method = RequestMethod.POST)
  @ResponseBody
  @SuppressWarnings("Duplicates")
  public ResponseEntity<?> describeLoadBalancer(
      @RequestBody @NotNull CloudosLoadBalancer cloudosLoadBalancer) {

    if (!loadBalancerRequestIsValid(cloudosLoadBalancer)) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    try {
      cloudosLoadBalancer = loadBalancerManagerService.describe(cloudosLoadBalancer);
      return new ResponseEntity<>(cloudosLoadBalancer, HttpStatus.OK);
    } catch (Exception e) {
      log.error("Error", e);
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  /**
   * Deletes an existing load balancer.
   * 
   * @param cloudosLoadBalancer - The load balancer request with attributes.
   */
  @RequestMapping(value = "/load_balancer/delete", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> deleteLoadBalancer(
      @RequestBody @NotNull CloudosLoadBalancer cloudosLoadBalancer) {

    if (!loadBalancerRequestIsValid(cloudosLoadBalancer)) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    try {
      loadBalancerManagerService.delete(cloudosLoadBalancer);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      log.error("Error", e);
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  /**
   * Sets the type of IP addresses used by the subnets of the specified Application Load Balancer or
   * Network Load Balancer.
   * 
   * @param cloudosLoadBalancer - The load balancer request with attributes.
   * @return the modified load balancer.
   */
  @RequestMapping(value = "/load_balancer/ip_address_type", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> setIpAddressType(
      @RequestBody @NotNull CloudosLoadBalancer cloudosLoadBalancer) {

    if (!loadBalancerRequestIsValid(cloudosLoadBalancer)) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    try {
      cloudosLoadBalancer = loadBalancerManagerService.setIpAddressType(cloudosLoadBalancer);
      return new ResponseEntity<>(cloudosLoadBalancer, HttpStatus.OK);
    } catch (Exception e) {
      log.error("Error", e);
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  /**
   * Associates the specified security groups with the specified Application Load Balancer.
   * 
   * @param cloudosLoadBalancer - The load balancer request with attributes.
   * @return the modified load balancer.
   */
  @RequestMapping(value = "/load_balancer/security_groups", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> setSecurityGroups(
      @RequestBody @NotNull CloudosLoadBalancer cloudosLoadBalancer) {

    if (!loadBalancerRequestIsValid(cloudosLoadBalancer)) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    try {
      cloudosLoadBalancer = loadBalancerManagerService.setSecurityGroups(cloudosLoadBalancer);
      return new ResponseEntity<>(cloudosLoadBalancer, HttpStatus.OK);
    } catch (Exception e) {
      log.error("Error", e);
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  /**
   * Enables the Availability Zone for the specified subnets for the specified Application Load
   * Balancer.
   * 
   * @param cloudosLoadBalancer - The load balancer request with attributes.
   * @return the modified load balancer.
   */
  @RequestMapping(value = "/load_balancer/subnets", method = RequestMethod.POST)
  @ResponseBody
  public ResponseEntity<?> setSubnets(
      @RequestBody @NotNull CloudosLoadBalancer cloudosLoadBalancer) {

    if (!loadBalancerRequestIsValid(cloudosLoadBalancer)) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    try {
      cloudosLoadBalancer = loadBalancerManagerService.setSubnets(cloudosLoadBalancer);
      return new ResponseEntity<>(cloudosLoadBalancer, HttpStatus.OK);
    } catch (Exception e) {
      log.error("Error", e);
      return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }



  /**
   * Validates a load balancer request.
   * 
   * @param cloudosLoadBalancer - The load balancer request be validated.
   * @return true if is valid otherwise false when invalid
   */
  private boolean loadBalancerRequestIsValid(CloudosLoadBalancer cloudosLoadBalancer) {

    if (StringUtils.isEmpty(cloudosLoadBalancer.getRegion())) {
      log.error("Region is null in the request.");
      return false;
    }

    if (cloudosLoadBalancer.getProvider() == null) {
      log.error("Provider is null in the request");
      return false;
    }

    return true;

  }

}
