package cloudos.optimizer;

import java.math.BigDecimal;
import java.util.Map;

import cloudos.Providers;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CostIncreaseResponse {

  BigDecimal lastMonthTotal;
  private Map<Providers, BigDecimal> lastMonth;
  BigDecimal monthToDateTotal;
  private Map<Providers, BigDecimal> monthToDate;
  BigDecimal forecastTotal;
  private Map<Providers, BigDecimal> forecast;

}
