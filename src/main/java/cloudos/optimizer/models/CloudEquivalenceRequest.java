package cloudos.optimizer.models;

import cloudos.Providers;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CloudEquivalenceRequest {
  
  private String region;
  private Providers fromProvider;
  private Providers toProvider;
}
