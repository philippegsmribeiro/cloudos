package cloudos.context;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.context.request.RequestContextHolder;

/**
 * PoolExecutor that uses the context aware callable allowing the child class to get access to the
 * requestcontext.
 *
 * @author flavio
 */
public class ContextAwarePoolExecutor extends SimpleAsyncTaskExecutor {
  private static final long serialVersionUID = 8548898124187571441L;

  @Override
  public <T> Future<T> submit(Callable<T> task) {
    return super.submit(
        new ContextAwareCallable<T>(task, RequestContextHolder.currentRequestAttributes()));
  }

  @Override
  public <T> ListenableFuture<T> submitListenable(Callable<T> task) {
    return super.submitListenable(
        new ContextAwareCallable<T>(task, RequestContextHolder.currentRequestAttributes()));
  }
}
