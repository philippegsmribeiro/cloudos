package cloudos.security;

import javax.servlet.http.HttpServletRequest;

import lombok.extern.log4j.Log4j2;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/** Request Scope Info Utility to get info from the request. ex: the username */
@Component
@Log4j2
public class RequestScope {

  /**
   * Get username.
   *
   * @return the username
   */
  public String getUserName() {
    try {
      return ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
          .getUsername();
    } catch (Exception e) {
      log.info("It wasn't possible to get the username from the request scope!");
    }
    return "";
  }

  /**
   * Get the request.
   *
   * @return HttpServletRequest
   */
  public HttpServletRequest getHttpServletRequest() {
    try {
      return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
          .getRequest();
    } catch (Exception e) {
      log.info("It wasn't possible to get the httpservletrequest from the request scope!");
    }
    return null;
  }
}
