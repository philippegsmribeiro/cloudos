package cloudos.security;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

import cloudos.SettingsController;
import cloudos.security.jwt.JwtAuthenticationEntryPoint;
import cloudos.security.jwt.JwtLoginAuthenticationFilter;
import cloudos.security.jwt.JwtLoginAuthenticationProvider;
import cloudos.security.jwt.JwtTokenAuthenticationFilter;
import cloudos.security.jwt.JwtTokenAuthenticationProvider;
import cloudos.security.jwt.JwtUtils;
import cloudos.tenant.TenantController;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  public static final String API_PATH = "/api";
  public static final String WS_PATH = "/ws";

  public static final String EVERYTHING = "/**";
  public static final String START = "/";
  public static final String ANGULAR = "/index.html";
  public static final String LOGIN_PATH = API_PATH + "/auth/login";
  public static final String TOKEN_REFRESH_PATH = API_PATH + "/auth/token";

  @Resource
  JwtUtils jwtUtils;

  @Autowired
  private JwtLoginAuthenticationProvider loginAuthenticationProvider;

  @Autowired
  private JwtTokenAuthenticationProvider tokenAuthenticationProvider;

  @Autowired
  private JwtAuthenticationEntryPoint authenticationEntryPoint;

  @Autowired
  private ObjectMapper objectMapper;

  @Bean
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) {
    auth.authenticationProvider(loginAuthenticationProvider);
    auth.authenticationProvider(tokenAuthenticationProvider);
  }

  @Override
  public void configure(WebSecurity web) throws Exception {
    super.configure(web);

    /* Configure resource that does not need authentication */
    web.ignoring()
        .antMatchers(HttpMethod.OPTIONS, "**")
        .antMatchers("/app/**")
        .antMatchers("/assets/**")
        .antMatchers("/bower_components/**")
        .antMatchers("/css/**")
        .antMatchers("/fonts/**")
        .antMatchers("/img/**")
        .antMatchers("/js/**")
        .antMatchers("/layout/**")
        .antMatchers("/pages/**")
        .antMatchers("/plugins/**")
        .antMatchers("/landing/**")
        .antMatchers("/#/**")
        .antMatchers("/index.html")
        .antMatchers(WS_PATH + "/**")
        .antMatchers(TenantController.TENANT + "/**")
        // add exception for accepting the invitation
        .antMatchers(SettingsController.SETTINGS  + "/user/invite/accept/**")
        .mvcMatchers(HttpMethod.POST, SettingsController.SETTINGS  + "/user")
        // for test
        .antMatchers("/mngt/***")
        .antMatchers(
            "/v2/api-docs",
            "/configuration/ui",
            "/swagger-resources/**",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**");
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.csrf()
        .disable() // We don't need CSRF for JWT based authentication
        .exceptionHandling()
        .authenticationEntryPoint(authenticationEntryPoint)
        .and()
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .authorizeRequests()
        .antMatchers(LOGIN_PATH)
        .permitAll() // Login end-point
        .antMatchers(TOKEN_REFRESH_PATH)
        .permitAll() // Token refresh end-point
        .and()
        .authorizeRequests()
        .antMatchers("/**")
        .authenticated() // Protected API
        // End-points
        // .antMatchers(API_PATH + "/**").authenticated() // Protected API End-points
        // login - filter
        .and()
        .addFilterBefore(getLoginFilter(), UsernamePasswordAuthenticationFilter.class)
        // token - filter
        .addFilterBefore(getJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
  }

  /**
   * Login Filter.
   *
   * @return JwtLoginAuthenticationFilter
   * @throws Exception if something goes wrong
   */
  @Bean
  protected JwtLoginAuthenticationFilter getLoginFilter() throws Exception {
    JwtLoginAuthenticationFilter filter = new JwtLoginAuthenticationFilter(LOGIN_PATH);
    filter.setAuthenticationManager(super.authenticationManagerBean());
    filter.setJwtUtils(jwtUtils);
    filter.setObjectMapper(objectMapper);
    return filter;
  }

  /**
   * JWT Filter.
   *
   * @return JwtTokenAuthenticationFilter
   * @throws Exception if something goes wrong
   */
  @Bean
  protected JwtTokenAuthenticationFilter getJwtTokenFilter() throws Exception {
    JwtTokenAuthenticationFilter filter =
        new JwtTokenAuthenticationFilter(API_PATH + "/**", LOGIN_PATH, TOKEN_REFRESH_PATH);
    filter.setAuthenticationManager(super.authenticationManagerBean());
    filter.setJwtUtils(jwtUtils);
    return filter;
  }

  /**
   * JWT Filter registration - set it not auto enabled - it will be included/enabled on the
   * configure method.
   *
   * @param filter to be registered
   * @return FilterRegistrationBean
   */
  @Bean
  public FilterRegistrationBean jwtTokenFilterRegistration(JwtTokenAuthenticationFilter filter) {
    FilterRegistrationBean registration = new FilterRegistrationBean(filter);
    registration.setEnabled(false);
    return registration;
  }

  /**
   * Login Filter registration - set it not auto enabled - it will be included/enabled on the
   * configure method.
   *
   * @param filter to be registered
   * @return FilterRegistrationBean
   */
  @Bean
  public FilterRegistrationBean loginFilterRegistration1(JwtLoginAuthenticationFilter filter) {
    FilterRegistrationBean registration = new FilterRegistrationBean(filter);
    registration.setEnabled(false);
    return registration;
  }

  @Bean
  public RequestScope requestScope() {
    return new RequestScope();
  }
}
