package cloudos.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.cors.CorsConfigurationSource;

/** Created by philipperibeiro on 1/2/17. */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CorsFilter extends org.springframework.web.filter.CorsFilter {

  public CorsFilter(CorsConfigurationSource configSource) {
    super(configSource);
  }

  @Override
  protected void doFilterInternal(
      HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {
    String requestUri = request.getRequestURI();
    if (requestUri.startsWith(WebSecurityConfig.API_PATH)
        || requestUri.startsWith(WebSecurityConfig.WS_PATH)) {
      // logger.info("#################CORS############### " + requestURI + " - " +
      // request.getMethod());
      // just for the api
      response.addHeader("Access-Control-Allow-Origin", "*");
    }
    if (request.getHeader("Access-Control-Request-Method") != null
        && "OPTIONS".equals(request.getMethod())) {
      // logger.info("#################CORS###############2 " + requestURI + " - " +
      // request.getMethod());
      // LOG.trace("Sending Header....");
      // CORS "pre-flight" request
      response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
      response.addHeader("Access-Control-Allow-Headers", "Authorization");
      response.addHeader("Access-Control-Allow-Headers", "Content-Type");
      // response.setHeader("Access-Control-Allow-Headers", "x-auth-token, x-requested-with");
      response.addHeader("Access-Control-Max-Age", "3600");
    } else {
      filterChain.doFilter(request, response);
    }
  }
}
