package cloudos.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.log4j.Log4j2;

import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

@Log4j2
public class JwtTokenAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

  public JwtTokenAuthenticationFilter(String urlMapping, String... ignoreUrls) {
    super(new SkipPathRequestMatcher(Arrays.asList(ignoreUrls), urlMapping));
  }

  protected JwtUtils jwtUtils;

  public void setJwtUtils(JwtUtils jwtUtils) {
    this.jwtUtils = jwtUtils;
  }

  @Override
  public Authentication attemptAuthentication(HttpServletRequest request,
      HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
    log.info("attemptAuthentication: " + request.getMethod() + " " + request.getRequestURI());
    if (jwtUtils.isSkipvalidation()) {
      // free
      return new JwtTokenSkip();
    }
    try {
      // validate token
      Jws<Claims> parseClaimsJws = jwtUtils.extractToken(request);
      // validate
      return this.getAuthenticationManager().authenticate(new JwtToken(parseClaimsJws));
    } catch (Exception e) {
      throw new InsufficientAuthenticationException(e.getLocalizedMessage());
    }
  }

  @Override
  protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
      FilterChain chain, Authentication authResult) throws IOException, ServletException {
    SecurityContext context = SecurityContextHolder.createEmptyContext();
    context.setAuthentication(authResult);
    SecurityContextHolder.setContext(context);
    chain.doFilter(request, response);
  }

  @Override
  protected void unsuccessfulAuthentication(HttpServletRequest request,
      HttpServletResponse response, AuthenticationException failed)
      throws IOException, ServletException {
    super.unsuccessfulAuthentication(request, response, failed);
  }
}
