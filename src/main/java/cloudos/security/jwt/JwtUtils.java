package cloudos.security.jwt;

import cloudos.user.UserService.MyUserDetails;
import com.google.api.client.util.Base64;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class JwtUtils {

  public static final String HEADER_FIELD = "Authorization";
  public static final String HEADER_PREFIX = "Bearer ";

  @Value("${jwt.refreshkey.expirationminutes}")
  public String refreshKeyExpirationMinutes;

  @Value("${jwt.key.expirationminutes}")
  public String keyExpirationMinutes;

  @Value("${jwt.key.secret}")
  public String keySecret;

  @Value("${jwt.key.issuer}")
  public String keyIssuer;

  @Value("${jwt.skipvalidation}")
  public Boolean skipvalidation;

  public JwtUtils() {}

  /**
   * Create the access token.
   *
   * @param userDetails user details
   * @return token
   */
  public String createAccessToken(UserDetails userDetails) {
    Claims claims =
        Jwts.claims()
            .setSubject(userDetails.getUsername())
            .setId(((MyUserDetails) userDetails).getUserId());
    claims.put(
        "scopes",
        userDetails.getAuthorities().stream().map(s -> s.toString()).collect(Collectors.toList()));
    return Jwts.builder()
        .setSubject(userDetails.getUsername())
        .setClaims(claims)
        .setIssuer(keyIssuer)
        .setIssuedAt(DateTime.now().toDate())
        .setExpiration(DateTime.now().plusMinutes(Integer.parseInt(keyExpirationMinutes)).toDate())
        .signWith(SignatureAlgorithm.HS512, Base64.encodeBase64String(keySecret.getBytes()))
        .compact();
  }

  /**
   * Create the refresh token.
   *
   * @param userDetails user details
   * @return refresh token
   */
  public String createRefreshToken(UserDetails userDetails) {
    Claims claims = Jwts.claims().setSubject(userDetails.getUsername());
    claims.put(
        "scopes",
        userDetails.getAuthorities().stream().map(s -> s.toString()).collect(Collectors.toList()));
    return Jwts.builder()
        .setClaims(claims)
        .setSubject(userDetails.getUsername())
        .setIssuer(keyIssuer)
        .setId(UUID.randomUUID().toString())
        .setIssuedAt(DateTime.now().toDate())
        .setExpiration(
            DateTime.now().plusMinutes(Integer.parseInt(refreshKeyExpirationMinutes)).toDate())
        .signWith(SignatureAlgorithm.HS512, Base64.encodeBase64String(keySecret.getBytes()))
        .compact();
  }

  /**
   * Extract the token info.
   *
   * @param request object
   * @return jws claims
   */
  public Jws<Claims> extractToken(HttpServletRequest request) {
    String header = request.getHeader(HEADER_FIELD);
    if (StringUtils.isBlank(header)) {
      throw new AuthenticationServiceException("Authorization header cannot be blank!");
    }
    if (header.length() < HEADER_PREFIX.length()) {
      throw new AuthenticationServiceException("Invalid authorization header size.");
    }
    String extract = header.substring(HEADER_PREFIX.length(), header.length());
    return parseToken(extract);
  }

  /**
   * Parse the token string.
   * @param token string
   * @return token claims
   */
  public Jws<Claims> parseToken(String token) {
    return Jwts.parser()
        .setSigningKey(Base64.encodeBase64String(keySecret.getBytes()))
        .parseClaimsJws(token);
  }

  /**
   * Create the map with the tokens.
   *
   * @param userDetails details
   * @return map of tokens
   */
  public Map<String, String> createMapTokens(UserDetails userDetails) {
    String accessToken = this.createAccessToken(userDetails);
    String refreshToken = this.createRefreshToken(userDetails);
    Map<String, String> tokenMap = new HashMap<String, String>();
    tokenMap.put("token", accessToken);
    tokenMap.put("refreshToken", refreshToken);
    return tokenMap;
  }

  public boolean isSkipvalidation() {
    return skipvalidation != null ? skipvalidation : false;
  }

  /**
   * For testing reasons.
   *
   * @param skip to ignore the validation
   */
  public void setSkipValidation(boolean skip) {
    skipvalidation = skip;
  }
}
