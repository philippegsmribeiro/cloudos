package cloudos.security.jwt;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
public class JwtLoginRequest {

  private String userName;
  private String password;

  @JsonCreator
  public JwtLoginRequest(
      @JsonProperty(JwtLoginAuthenticationFilter.USERNAME_JSON) String userName,
      @JsonProperty(JwtLoginAuthenticationFilter.PASSWORD_JSON) String password) {
    this.userName = userName;
    this.password = password;
  }

}
