package cloudos.security.jwt;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import cloudos.encryptionKeys.AmazonKMSService;
import cloudos.encryptionKeys.AmazonKMSServiceException;

@Component
public class JwtLoginAuthenticationProvider implements AuthenticationProvider {
  
  private final UserDetailsService userService;
  
  @Autowired
  private AmazonKMSService amazonKMSService;  

  @Autowired
  public JwtLoginAuthenticationProvider(final UserDetailsService userService) {
    this.userService = userService;
  }

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    String username = (String) authentication.getPrincipal();
    String password = (String) authentication.getCredentials();
    
    // validate on the mongo
    UserDetails user = userService.loadUserByUsername(username);
    if (user == null) {
      throw new UsernameNotFoundException("User not found: " + username);
    }
    
    password = encryptPassword(password);
    
    if (!password.equals(user.getPassword())) {
      throw new BadCredentialsException("Authentication Failed. Username or Password not valid.");
    }
    
    List<GrantedAuthority> authorities = new ArrayList<>();
    return new UsernamePasswordAuthenticationToken(user, null, authorities);
  }

  private String encryptPassword(String password) {
    try {
      password = amazonKMSService.encryptMessage(amazonKMSService.getUserEncryptedDataKey(), password);
    } catch (AmazonKMSServiceException e) {
      throw new BadCredentialsException("Authentication Failed. Error when encrypting data.");
    }
    return password;
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
  }
}
