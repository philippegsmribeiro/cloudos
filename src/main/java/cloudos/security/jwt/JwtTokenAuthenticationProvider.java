package cloudos.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class JwtTokenAuthenticationProvider implements AuthenticationProvider {

  @SuppressWarnings("unchecked")
  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    Jws<Claims> jwsClaims = (Jws<Claims>) authentication.getCredentials();
    String subject = jwsClaims.getBody().getSubject();
    List<String> scopes = jwsClaims.getBody().get("scopes", List.class);
    List<GrantedAuthority> authorities =
        scopes
            .stream()
            .map(authority -> new SimpleGrantedAuthority(authority))
            .collect(Collectors.toList());

    UserDetails userDetails = new User(subject, subject, authorities);
    return new JwtToken(userDetails, authorities);
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return (JwtToken.class.isAssignableFrom(authentication));
  }
}
