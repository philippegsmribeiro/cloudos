package cloudos.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import java.util.Collection;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class JwtToken extends AbstractAuthenticationToken {

  private static final long serialVersionUID = 2877954820905567501L;

  private Jws<Claims> rawAccessToken;
  UserDetails userDetails;

  /**
   * Constructor.
   *
   * @param parseClaimsJws claims
   */
  public JwtToken(Jws<Claims> parseClaimsJws) {
    super(null);
    this.rawAccessToken = parseClaimsJws;
    this.setAuthenticated(false);
  }

  /**
   * Constructor.
   *
   * @param userDetails details
   * @param authorities collection
   */
  public JwtToken(UserDetails userDetails, Collection<? extends GrantedAuthority> authorities) {
    super(authorities);
    this.eraseCredentials();
    this.userDetails = userDetails;
    super.setAuthenticated(true);
  }

  @Override
  public void setAuthenticated(boolean authenticated) {
    if (authenticated) {
      throw new IllegalArgumentException(
          "Cannot set this token to trusted - use constructor instead");
    }
    super.setAuthenticated(false);
  }

  @Override
  public Object getCredentials() {
    return rawAccessToken;
  }

  @Override
  public Object getPrincipal() {
    return this.userDetails;
  }

  @Override
  public void eraseCredentials() {
    super.eraseCredentials();
    this.rawAccessToken = null;
  }
}
