package cloudos.security.jwt;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.stereotype.Component;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

  public static final String DEFAULT_ENTRY_POINT = "/";
  public static final String DEFAULT_ENTRY_POINT_PAGE = "/index.html";

  private static final String XML_HTTP_REQUEST = "XMLHttpRequest";
  private static final String X_REQUESTED_WITH = "X-Requested-With";

  public static boolean isAjax(HttpServletRequest request) {
    return XML_HTTP_REQUEST.equals(request.getHeader(X_REQUESTED_WITH));
  }

  RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

  @Override
  public void commence(
      HttpServletRequest request,
      HttpServletResponse response,
      AuthenticationException authException)
      throws IOException, ServletException {
    if (!DEFAULT_ENTRY_POINT.equals(request.getRequestURI())) {
      if (isAjax(request)) {
        response.sendError(HttpStatus.UNAUTHORIZED.value(), "Unauthorized");
      } else {
        // redirect to / if normal request
        redirectStrategy.sendRedirect(request, response, DEFAULT_ENTRY_POINT);
        // request.getRequestDispatcher(DEFAULT_ENTRY_POINT).forward(request, response);
      }
    } else {
      // redirectStrategy.sendRedirect(request, response, DEFAULT_ENTRY_POINT_PAGE);
      request.getRequestDispatcher(DEFAULT_ENTRY_POINT_PAGE).forward(request, response);
    }
  }
}
