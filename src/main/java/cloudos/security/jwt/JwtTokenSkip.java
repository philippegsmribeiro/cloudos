package cloudos.security.jwt;

import java.util.Collections;
import org.springframework.security.authentication.AbstractAuthenticationToken;

public class JwtTokenSkip extends AbstractAuthenticationToken {
  private static final long serialVersionUID = 3893646730823646021L;

  @SuppressWarnings("unchecked")
  public JwtTokenSkip() {
    super(Collections.EMPTY_LIST);
  }

  @Override
  public boolean isAuthenticated() {
    return true;
  }

  @Override
  public Object getPrincipal() {
    return null;
  }

  @Override
  public Object getCredentials() {
    return null;
  }
}
