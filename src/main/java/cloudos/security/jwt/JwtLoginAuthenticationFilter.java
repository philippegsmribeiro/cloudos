package cloudos.security.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.log4j.Log4j2;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

/** Execute the login and generate the jwt tokens. */
@Log4j2
public class JwtLoginAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

  public static final String USERNAME_JSON = "username";
  public static final String PASSWORD_JSON = "password";

  public JwtLoginAuthenticationFilter(String urlMapping) {
    super(urlMapping);
  }

  protected JwtUtils jwtUtils;

  public void setJwtUtils(JwtUtils jwtUtils) {
    this.jwtUtils = jwtUtils;
  }

  protected ObjectMapper objectMapper;

  public void setObjectMapper(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  @Override
  public Authentication attemptAuthentication(
      HttpServletRequest request, HttpServletResponse response)
      throws AuthenticationException, IOException, ServletException {
    // validate login
    log.info("attemptAuthentication: " + request.getRequestURI());
    try {
      // get the fields
      String principal = "";
      String credentials = "";
      if (MediaType.APPLICATION_FORM_URLENCODED_VALUE.equals(request.getContentType())) {
        principal = request.getParameter(JwtLoginAuthenticationFilter.USERNAME_JSON);
        credentials = request.getParameter(JwtLoginAuthenticationFilter.PASSWORD_JSON);
      } else if (request.getContentType().contains(MediaType.APPLICATION_JSON_VALUE)) {
        JwtLoginRequest loginRequest =
            objectMapper.readValue(request.getReader(), JwtLoginRequest.class);
        principal = loginRequest.getUserName();
        credentials = loginRequest.getPassword();
      }
      // authenticate
      return this.getAuthenticationManager()
          .authenticate(new UsernamePasswordAuthenticationToken(principal, credentials));
    } catch (Exception e) {
      throw new BadCredentialsException(e.getMessage());
    }
  }

  @Override
  protected void successfulAuthentication(
      HttpServletRequest request,
      HttpServletResponse response,
      FilterChain chain,
      Authentication authResult)
      throws IOException, ServletException {
    UserDetails userDetails = (UserDetails) authResult.getPrincipal();
    // if successful - return the jwt info
    Map<String, String> tokenMap = jwtUtils.createMapTokens(userDetails);
    response.setStatus(HttpStatus.OK.value());
    response.setContentType(MediaType.APPLICATION_JSON_VALUE);
    response.getWriter().write(new Gson().toJson(tokenMap));
  }

  @Override
  protected void unsuccessfulAuthentication(
      HttpServletRequest request, HttpServletResponse response, AuthenticationException failed)
      throws IOException, ServletException {
    // if unsuccessful - return error
    super.unsuccessfulAuthentication(request, response, failed);
  }
}
