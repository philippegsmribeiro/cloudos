package cloudos;

import cloudos.healthchecker.HealthCheckerService;
import cloudos.healthchecker.ResourceChart;
import cloudos.healthchecker.ResourcesCost;
import cloudos.instances.InstanceService;
import cloudos.models.HealthcheckerResponse;
import cloudos.models.Instance;
import cloudos.security.WebSecurityConfig;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Implement the HealthChecker controller, so instances can register themselves and start sending
 * information from the instances in the cloud provider to the CloudOS ML modules.
 *
 * @author Philippe Ribeiro
 */
@RestController
@RequestMapping(value = HealthCheckerController.HEALTHCHECKER)
@Log4j2
public class HealthCheckerController {

    public static final String HEALTHCHECKER = WebSecurityConfig.API_PATH + "/healthchecker";

    @Autowired
    InstanceService instanceService;

    @Autowired
    HealthCheckerService healthCheckerService;

    public HealthCheckerController() {
    }

    /**
     * Constructor for the HealthCheckerController. It takes an InstanceRepository object so the
     * controller can be unit tested.
     *
     * @param instanceService: A instanceService object.
     */
    public HealthCheckerController(InstanceService instanceService,
                                   HealthCheckerService healthCheckerService) {
        this.instanceService = instanceService;
        this.healthCheckerService = healthCheckerService;
    }

    /**
     * Get a particular instance, based on its id.
     *
     * @param instance: An instance id
     * @return: the instance, if one exists.
     */
    @RequestMapping(value = "/{instance}", method = RequestMethod.GET)
    @ResponseBody
    public Instance getInstance(@PathVariable String instance) {
        return this.instanceService.findById(instance);
    }

    /**
     * Implement the create API for a new instance to register itself with the health checker
     * controller.
     *
     * @param instance: the new Instance object to be inserted into the MongoDB.
     * @return: the newly created Instance object.
     */
    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    public Instance create(@RequestBody Instance instance) {
        return this.instanceService.save(instance);
    }

    /**
     * Delete an existing instance, based on its ID.
     *
     * @param instance: The instance's ID.
     */
    @RequestMapping(value = "/{instance}", method = RequestMethod.DELETE)
    public void delete(@PathVariable String instance) {
        this.instanceService.delete(instance);
    }

    /**
     * Return the chart of the total cost for all the cloud providers, from the last 30 days.
     *
     * @return ResponseEntity: A HTTP Response.
     */
    @RequestMapping(value = "/get_resources_cost", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getResourcesCost() {
        ResourcesCost resourcesCost = this.healthCheckerService.getLast10DaysCost();
        log.info("Fetching Resource cost {}", resourcesCost);
        if (resourcesCost == null) {
            return new ResponseEntity<>(new ResourcesCost(), HttpStatus.OK);
        }
        return new ResponseEntity<>(resourcesCost, HttpStatus.OK);
    }

    /**
     * Return the resource cost chart for a resource, given its resource id.
     *
     * @param instance: The resource id
     * @return ResponseEntity: A HTTP Response.
     */
    @RequestMapping(value = "/get_resource_cost/{instance}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getResourceCost(@PathVariable String instance) {
        ResourceChart resourceChart = this.healthCheckerService.getResourceCost(instance);

        HealthcheckerResponse response = new HealthcheckerResponse();
        // set the response dependending on the resource chart response.
        if (resourceChart != null) {

            Map<Long, BigDecimal> sortedTenDaysChart = resourceChart.getChart().entrySet().stream()
                    .sorted(Map.Entry.<Long, BigDecimal>comparingByKey().reversed())
                    .limit(10)
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

            resourceChart.setChart(sortedTenDaysChart);

            response.setStatus(HttpStatus.OK);
            response.setMessage(String.format("Resource cost chart for %s successfully found", instance));
            response.setResourceChart(resourceChart);
        } else {
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setMessage(String.format("Resource cost chart for %s was not found", instance));
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
