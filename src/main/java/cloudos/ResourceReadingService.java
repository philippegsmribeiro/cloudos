package cloudos;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

/**
 * Auxiliary service to reading resources from the src/main/resources during jar execution.
 *
 * @author flavio
 */
@Service
@Log4j2
public class ResourceReadingService {

  @Autowired
  ApplicationContext appContext;

  /**
   * Get a file from the resources.
   *
   * @param resourcePath the resource path
   * @return a file if found, null otherwise
   * @throws IOException if the resource wasn't found in the given path
   */
  public File getFile(String resourcePath) throws IOException {
    if (!resourcePath.startsWith("classpath:")) {
      resourcePath = "classpath:" + resourcePath;
    }
    Resource resource = appContext.getResource(resourcePath);
    log.info("Loaded resource: {}", resource.toString());
    log.info("Resource exists: {}", resource.exists());
    log.info("Resource File: {}", resource.getFile().getAbsolutePath());
    return resource.getFile();
  }

  /**
   * Get a URL in the given resource path.
   *
   * @param resourcePath the resource path
   * @return a url if found, null otherwise
   * @throws IOException if the resource wasn't found in the given path
   */
  public URL getURL(String resourcePath) throws IOException {
    if (!resourcePath.startsWith("classpath:")) {
      resourcePath = "classpath:" + resourcePath;
    }
    Resource resource = appContext.getResource(resourcePath);
    log.info("Loaded resource: {}", resource.toString());
    log.info("Resource exists: {}", resource.exists());
    log.info("Resource File: {}", resource.getFile().getAbsolutePath());
    log.info("Resource URL: {}", resource.getURL());
    log.info("Resource File URL: {}", resource.getFile().toURI().toURL());
    return resource.getFile().toURI().toURL();
  }
}
