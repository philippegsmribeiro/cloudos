package cloudos;

import cloudos.encryptionKeys.AmazonKMSService;
import cloudos.security.WebSecurityConfig;
import cloudos.security.jwt.JwtUtils;
import cloudos.user.UserService;
import com.google.gson.Gson;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import java.io.IOException;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = AuthController.AUTH)
public class AuthController {

  public static final String AUTH = WebSecurityConfig.API_PATH + "/auth";

  @Autowired
  private JwtUtils jwtUtils;
  
  @Autowired
  private UserService userService;
  
  @Autowired
  private AmazonKMSService amazonKMSService;    

  @RequestMapping(value = "/login", method = RequestMethod.POST,
      headers = "content-type=application/x-www-form-urlencoded")
  public void login(String username, String password) {
    System.out.println();
  }

  /**
   * Update the auth token.
   *
   * @param request object
   * @param response object
   * @throws IOException if not able to send the error
   */
  @RequestMapping(value = "/token", method = RequestMethod.POST)
  public void token(HttpServletRequest request, HttpServletResponse response) throws IOException {
    try {
      String refreshToken = request.getParameter("refresh_token");
      Jws<Claims> parseClaimsJws = jwtUtils.parseToken(refreshToken);
      
      String subject = parseClaimsJws.getBody().getSubject();
      UserDetails user = userService.loadUserByUsername(subject);
      
      Map<String, String> tokenMap = jwtUtils.createMapTokens(user);
      tokenMap.put("userName", subject);
      response.setStatus(HttpStatus.OK.value());
      response.setContentType(MediaType.APPLICATION_JSON_VALUE);
      response.getWriter().write(new Gson().toJson(tokenMap));
    } catch (Exception e) {
      response.sendError(HttpStatus.UNAUTHORIZED.value(), "Unauthorized");
    }
  }
}
