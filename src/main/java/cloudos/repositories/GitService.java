package cloudos.repositories;

import com.jcraft.jsch.Session;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import lombok.extern.log4j.Log4j2;

import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.TransportConfigCallback;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.dircache.DirCache;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.RefUpdate;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.transport.FetchResult;
import org.eclipse.jgit.transport.JschConfigSessionFactory;
import org.eclipse.jgit.transport.OpenSshConfig;
import org.eclipse.jgit.transport.PushResult;
import org.eclipse.jgit.transport.RemoteRefUpdate;
import org.eclipse.jgit.transport.SshSessionFactory;
import org.eclipse.jgit.transport.SshTransport;
import org.eclipse.jgit.transport.TrackingRefUpdate;
import org.eclipse.jgit.transport.Transport;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.springframework.stereotype.Service;

/**
 * Created by philipperibeiro on 12/22/16.
 *
 * <p>
 *   GitHubService allows us perform any of the GitHub related operations.
 * </p>
 */
@Service
@Log4j2
public class GitService {


  /**
   * Clone a repository with username and password authentication.
   *
   * @param uri the URL repository to be cloned
   * @param directory the directory to where the git clone will take place
   * @param branches if we should clone all the submodules
   * @param username the username of the user which has access to the repository
   * @param password the password of the user which has access to the repository
   * @param bare if the repository should be a bare repo.
   * @return the newly cloned repository
   */
  public synchronized Git cloneRepository(
      String uri,
      String username,
      String password,
      String directory,
      boolean bare,
      boolean branches) {
    try {
      CloneCommand cloneCommand = Git.cloneRepository();
      cloneCommand.setBare(bare);
      cloneCommand.setCloneAllBranches(branches);
      cloneCommand.setDirectory(new File(directory));
      cloneCommand.setURI(uri);
      // set the credentials
      cloneCommand.setCredentialsProvider(
          new UsernamePasswordCredentialsProvider(username, password));
      return cloneCommand.call();
    } catch (GitAPIException gae) {
      log.error(gae.getMessage(), gae);
      return null;
    }
  }

  /**
   * Close a repository.
   *
   * @param uri the URL repository to be cloned
   * @param directory the directory to where the git clone will take place
   * @param branches if we should clone all the submodules
   * @return the newly cloned repository
   */
  public synchronized Git cloneRepository(
      String uri, String directory, boolean branches, boolean bare) {
    try {
      log.debug(String.format("###### Cloning Repository '%s' into directory '%s'", uri, directory));
      Git git =
          Git.cloneRepository()
              .setBare(bare)
              .setCloneSubmodules(true)
              .setCloneAllBranches(branches)
              .setURI(uri)
              .setDirectory(new File(directory))
              .call();
      return git;
    } catch (GitAPIException gae) {
      log.error(gae.getMessage(), gae);
      return null;
    }
  }

  /**
   * Use public key authentication to clone the repository. The keys must exist under ~/.ssh
   * directory.
   *
   * @param uri the URL repository to be cloned
   * @param directory the directory to where the git clone will take place
   * @param bare if the repository should be a bare repo.
   * @param branches if we should clone all the submodules
   * @return the newly cloned repository
   */
  public synchronized Git cloneRepositorySsh(
      String uri, String directory, boolean bare, boolean branches) {
    try {
      SshSessionFactory sshSessionFactory =
          new JschConfigSessionFactory() {
            @Override
            protected void configure(OpenSshConfig.Host host, Session session) {
              // do nothing
            }
          };
      CloneCommand cloneCommand = Git.cloneRepository();
      cloneCommand.setURI(uri);
      cloneCommand.setBare(bare);
      cloneCommand.setCloneAllBranches(branches);
      cloneCommand.setDirectory(new File(directory));
      cloneCommand.setURI(uri);
      cloneCommand.setTransportConfigCallback(
          new TransportConfigCallback() {
            @Override
            public void configure(Transport transport) {
              SshTransport sshTransport = (SshTransport) transport;
              sshTransport.setSshSessionFactory(sshSessionFactory);
            }
          });
      return cloneCommand.call();
    } catch (GitAPIException gae) {
      log.error(gae.getMessage(), gae);
      return null;
    }
  }

  /**
   * Add a file to a git repository.
   *
   * @param git a Git reference for the git repository
   * @param files a list of files to be committed
   * @return true if the files were successfully added
   */
  public synchronized boolean addFileToRepository(Git git, List<String> files) {
    try {
      for (String file : files) {
        log.debug(
            String.format("#### Committing file: '%s' to repository '%s'", file, git.describe()));
        DirCache index = git.add().addFilepattern(file).call();
        log.debug(String.format("Current repository index: '%s'", index));
      }
      return true;
    } catch (GitAPIException gae) {
      log.error(gae.getMessage(), gae);
      return false;
    }
  }

  /**
   * Commit changes to the repository.
   *
   * @param git a Git reference for the git repository
   * @param message the commit message
   * @return true if the files were successfully added.
   */
  public synchronized boolean commitRepository(Git git, String message) {
    try {
      RevCommit commit = git.commit().setMessage(message).call();
      log.debug(String.format("###### Commit review: '%s'", commit));
      if (commit != null) {
        return true;
      }
      return false;
    } catch (GitAPIException gae) {
      log.error(gae.getMessage(), gae);
      return false;
    }
  }

  /**
   * Add a file to a git repository.
   *
   * @param git a Git reference for the git repository
   * @param files a list of files to be committed.
   * @return true if the files were successfully added.
   */
  public synchronized boolean removeFileFromRepository(Git git, List<String> files) {
    try {
      for (String file : files) {
        log.debug(
            String.format("#### Committing file: '%s' to repository '%s'", file, git.describe()));
        DirCache index = git.rm().addFilepattern(file).call();
        log.debug(String.format("Current repository index: '%s'", index));
      }
      return true;
    } catch (GitAPIException gae) {
      log.error(gae.getMessage(), gae);
      return false;
    }
  }

  /**
   * Return the status of the repository.
   *
   * @param git a Git reference for the git repository
   * @return the repository status
   */
  public Status getStatus(Git git) {
    try {
      return git.status().call();
    } catch (GitAPIException e) {
      log.error(e.getMessage(), e);
    }
    return null;
  }

  /**
   * Create and initialize an empty Git repository.
   *
   * @param directory the directory where the empty Git repository will be located
   * @return a Git object
   */
  public synchronized Git createRepository(String directory) {
    try {
      return Git.init().setDirectory(new File(directory)).call();
    } catch (GitAPIException gae) {
      log.error(gae.getMessage(), gae);
      return null;
    }
  }

  /**
   * Specify all the branches to be cloned.
   *
   * @param uri the URL repository to be cloned
   * @param directory the directory to where the git clone will take place
   * @param branches a list of branches to be cloned
   * @return true if successful, false otherwise
   */
  public synchronized Git cloneBranches(String uri, String directory, List<String> branches) {
    try {
      Git git =
          Git.cloneRepository()
              .setURI(uri)
              .setDirectory(new File(directory))
              .setBranchesToClone(branches)
              .call();
      return git;
    } catch (GitAPIException gae) {
      log.error(gae.getMessage(), gae);
      return null;
    }
  }

  /**
   * Return the Status from a remote update push.
   *
   * @param git a git object
   * @param refName the remote name
   * @return the status of the commit
   */
  public synchronized RemoteRefUpdate.Status pushRemoteRepository(Git git, String refName) {
    try {
      Iterable<PushResult> iterable = git.push().call();
      PushResult pushResult = iterable.iterator().next();
      return pushResult.getRemoteUpdate(refName).getStatus();
    } catch (GitAPIException gae) {
      log.error(gae.getMessage(), gae);
      return null;
    }
  }

  /**
   * Execute a fetch command in a repository.
   *
   * @param git a git object
   * @param refName the remote name
   * @return the status of the fetch
   */
  public synchronized RefUpdate.Result fetchRepository(Git git, String refName) {
    try {
      FetchResult fetchResult = git.fetch().call();
      TrackingRefUpdate refUpdate = fetchResult.getTrackingRefUpdate(refName);
      return refUpdate.getResult();
    } catch (GitAPIException gae) {
      log.error(gae.getMessage(), gae);
      return null;
    }
  }

  /**
   * Get a collection of all the remove branches in the uri.
   *
   * @param uri the URL repository to be cloned
   * @return a collection of Ref objects
   */
  public synchronized Collection<Ref> listRemoteBranches(String uri) {
    Collection<Ref> branches = new ArrayList<>();
    try {
      branches = Git.lsRemoteRepository().setHeads(true).setRemote(uri).call();
    } catch (GitAPIException gae) {
      log.error(gae.getMessage(), gae);
    }
    return branches;
  }
}
