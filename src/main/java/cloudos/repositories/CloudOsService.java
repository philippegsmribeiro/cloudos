package cloudos.repositories;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/** Created by philipperibeiro on 12/24/16. */
@Service
@PropertySource(value = "classpath:application.properties")
public class CloudOsService {

  @Value("${cloudos.release.aws_access_key_id}")
  private String awsKeys;

  @Value("${cloudos.release.aws_secret_key}")
  private String awsSecret;

  @Value("${cloudos.release.repository_bucket}")
  private String repository;

  private static final String settings =
      System.getProperty("user.home") + File.separator + ".m2/settings.xml";

  public CloudOsService() {}
  
  /**
   * Configure a given maven repository.
   */
  public void configureMavenRepository() {
    DocumentBuilderFactory icFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder icBuilder;
    try {
      icBuilder = icFactory.newDocumentBuilder();
      Document doc = icBuilder.newDocument();
      Element mainRootElement =
          doc.createElementNS("http://maven.apache.org/SETTINGS/1.0.0", "settings");
      mainRootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
      mainRootElement.setAttribute(
          "xsi:schemaLocation",
          "http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd");
      doc.appendChild(mainRootElement);

      Element serversElement = doc.createElement("servers");
      mainRootElement.appendChild(serversElement);

      serversElement.appendChild(getServer(doc, "cloudos-release", this.awsKeys, this.awsSecret));
      serversElement.appendChild(getServer(doc, "cloudos-snapshot", this.awsKeys, this.awsSecret));

      // output DOM XML to console
      Transformer transformer = TransformerFactory.newInstance().newTransformer();
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
      DOMSource source = new DOMSource(doc);

      // StreamResult console = new StreamResult(System.out);
      StreamResult console = new StreamResult(new File(settings));
      transformer.transform(source, console);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Create a new server node.
   *
   * @param doc the document to be parsed
   * @param id the id of the document
   * @param username the username
   * @param password the password
   * @return a new document node
   */
  private static Node getServer(Document doc, String id, String username, String password) {
    Element company = doc.createElement("server");
    company.appendChild(getServerElements(doc, company, "id", id));
    company.appendChild(getServerElements(doc, company, "username", username));
    company.appendChild(getServerElements(doc, company, "password", password));
    return company;
  }

  /**
   * Get the element in a node.
   *
   * @param doc the document to be fetched
   * @param element the element to be found
   * @param name the name of the element
   * @param value the value of the element
   * @return the newly created node
   */
  private static Node getServerElements(Document doc, Element element, String name, String value) {
    Element node = doc.createElement(name);
    node.appendChild(doc.createTextNode(value));
    return node;
  }
}
