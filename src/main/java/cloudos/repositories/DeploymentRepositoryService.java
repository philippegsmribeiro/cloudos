package cloudos.repositories;

import cloudos.amazon.AmazonS3Service;
import cloudos.utils.FileUtils;
import java.io.File;
import java.io.IOException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Created by philipperibeiro on 12/25/16.
 * Perform repository related operations for the deployments. Whenever a deployment is created, the
 * uploaded content is uploaded to the deployment repository in S3.
 */
@Service
@Log4j2
public class DeploymentRepositoryService {

  @Autowired
  private AmazonS3Service s3Service;

  @Value("${cloudos.build.remote.repository}")
  private String repositoryBucket;

  @Value("${cloudos.build.s3.region}")
  private String region;

  @Value("${cloudos.build.directory}")
  private String deploymentDirectory;

  /**
   * Constructor for the DeploymentService. It will create the deployment directory if it does not
   * exists, and create the deployments bucket if it does not exist.
   */
  public DeploymentRepositoryService() {
    this.s3Service = new AmazonS3Service();

    // just to make it a directory
    String directory = this.deploymentDirectory + File.separator;
    // check if the deployment directory exists.
    FileUtils.create(directory);
  }

  /**
   * Constructor for the DeploymentService. It will create the deployment directory if it does not
   * exists, and create the deployment bucket if it does not exist. Used mostly for testing.
   *
   * @param repositoryBucket the name of the bucket to be used.
   * @param region the region the bucket repository will exist.
   * @param deploymentDirectory the deployment directory location.
   */
  public DeploymentRepositoryService(
      String repositoryBucket,
      String region,
      String deploymentDirectory,
      AmazonS3Service s3Service) {

    this.repositoryBucket = repositoryBucket;
    this.region = region;
    this.deploymentDirectory = deploymentDirectory;
    this.s3Service = s3Service;

    // just to make it a directory
    String directory = this.deploymentDirectory + File.separator;
    // check if the deployment directory exists.
    FileUtils.create(directory);
  }

  /**
   * Given a full file or folder path, it uploads the resource to the S3 bucket repository.
   *
   * @param resourcePath the file or folder full path.
   * @return true if successful uploaded, false otherwise.
   */
  public boolean uploadDeployment(String resourcePath) {
    try {
      File file = new File(resourcePath);
      if (!file.exists()) {
        throw new IOException(String.format("Resource '%s' does not exist.", resourcePath));
      }
      if (file.isFile()) {
        // the key will be the name of the file.
        return this.s3Service.uploadFile(
            this.repositoryBucket, this.region, file.getName(), resourcePath);
      }
      if (file.isDirectory()) {
        return this.s3Service.uploadFolder(this.repositoryBucket, this.region, resourcePath);
      }
    } catch (Exception ex) {
      log.error(ex.getMessage());
    }
    return false;
  }

  /**
   * Download a deployment from S3 repository to the deployment local repository.
   *
   * @param key the name of the file or folder in S3.
   * @param isFolder if the resource is a file or folder.
   * @return true if successfully downloaded and created, false otherwise.
   */
  public boolean downloadDeployment(String key, boolean isFolder) {
    try {
      String destination = this.deploymentDirectory + File.separator + key;
      if (isFolder) {
        return this.s3Service.downloadFolder(
            this.deploymentDirectory, this.region, key, destination);
      }
      return this.s3Service.downloadFile(this.repositoryBucket, this.region, key, destination, true,
          false);
    } catch (Exception ex) {
      log.error(ex.getMessage());
    }
    return false;
  }
}
