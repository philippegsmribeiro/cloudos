package cloudos.healthchecker;

import static cloudos.billings.BillingsConstants.MATH_CONTEXT;

import cloudos.Providers;
import cloudos.billings.BillingReportUtils;
import cloudos.models.Instance;
import cloudos.models.SparkJob;
import cloudos.models.costanalysis.GenericBillingReport;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import lombok.extern.log4j.Log4j2;

import org.apache.commons.collections.CollectionUtils;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

/**
 * Created by philipperibeiro on 6/27/17.
 */
@Log4j2
public class HealthcheckerJob extends SparkJob {

  /**
   * Default constructor the HealthcheckerJob class.
   */
  public HealthcheckerJob() {
    super();
  }

  /**
   * Assign the JavaSparkContext to the context.
   *
   * @param sc A JavaSparkContext context
   */
  public HealthcheckerJob(JavaSparkContext sc) {
    super(sc);
  }

  /**
   * Return the ResourceChart for the resource id.
   *
   * @param reports A list of Billing Reports.
   * @param resourceId The resource id.
   * @param provider The provider for this resource.
   * @return ResourceChart: ResourceChart if the reports exist, null otherwise.
   */
  public ResourceChart updateResourceCost(
      @NotNull List<GenericBillingReport> reports, @NotNull String resourceId, Providers provider) {

    // check if the reports exist.
    if (CollectionUtils.isEmpty(reports)) {
      return null;
    }
    // get a list of resources
    JavaRDD<GenericBillingReport> rdd = context.parallelize(reports);

    // now build a map between resourceId and usage amount, already sorted by the key (date)
    JavaPairRDD<Date, BigDecimal> result =
        rdd.mapToPair(
            (PairFunction<GenericBillingReport, Date, BigDecimal>) x -> new Tuple2<>(
                x.getUsageStartTime(), x.getUsageCost())).reduceByKey(
            (Function2<BigDecimal, BigDecimal, BigDecimal>) (x, y) -> x.add(y, MATH_CONTEXT))
            .sortByKey();

    // reduce over all the keys to return the total cost
    Map<Date, BigDecimal> chart = new TreeMap<>(result.collectAsMap());
    BigDecimal cost = result.values().fold(BigDecimal.ZERO,
        (Function2<BigDecimal, BigDecimal, BigDecimal>) (x, y) -> x.add(y, MATH_CONTEXT));

    Map<Long, BigDecimal> longChart = new HashMap<>();

    chart.forEach((date, value) -> longChart.put(date.getTime(), value));

    return new ResourceChart(null, resourceId, cost, longChart, new Date(), provider);
  }

  /**
   * Update the instances collection with the total cost so far.
   *
   * @param instances The list of instances for the user.
   * @param reports The list of billing reports.
   * @return {@code List<Instance>} Updated amount for the instances.
   */
  public List<Instance> updateResourcesEstCost(
      @NotNull List<Instance> instances, @NotNull List<GenericBillingReport> reports) {
    try {
      // nothing to do.
      if (CollectionUtils.isEmpty(instances) || CollectionUtils.isEmpty(reports)) {
        return new ArrayList<>();
      }

      JavaRDD<GenericBillingReport> rdd = context.parallelize(reports);
      // aggregate for all the resources.

      JavaPairRDD<String, BigDecimal> result = rdd
          .mapToPair(
              (PairFunction<GenericBillingReport, String, BigDecimal>) x -> new Tuple2<>(
                  x.getResourceId(),
                  x.getUsageCost()))
          .reduceByKey(
              (Function2<BigDecimal, BigDecimal, BigDecimal>) (x, y) -> x.add(y, MATH_CONTEXT));

      Set<String> keys = new HashSet<>(result.keys().collect());

      // update for every instance
      for (Instance instance : instances) {
        if (keys.contains(instance.getProviderId())) {
          List<BigDecimal> value = result.lookup(instance.getProviderId());
          BigDecimal cost = value.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
          // update the cost.
          instance.setCost(cost.doubleValue());
        }
      }
    } catch (Exception e) {
      // TODO: handle exception
      log.error("error", e);
    }
    return instances;
  }

  /**
   * Return the cost of operating the current instances for the last 10 days.
   *
   * @param instances A list of all the instances in the last 10 days.
   * @param reports A list of reports from the last 10 days.
   * @return A Map between the providers, and its cost for the last 10 days.
   */
  public LinkedList<ResourcesCostEntry> getResourcesCostLast10Days(
      @NotNull List<Instance> instances, @NotNull List<GenericBillingReport> reports, List<Providers> providers) {

    LinkedList<ResourcesCostEntry> entries = new LinkedList<>();

    // initialize with all the providers.
    for (Providers provider : providers) {
      entries.add(new ResourcesCostEntry(provider, new LinkedList<>()));
    }

    // Transforms the lists into RDDs
    JavaRDD<Instance> rdd = context.parallelize(instances);

    // group billing reports by Provider
    final Map<Providers, List<GenericBillingReport>> providersListMap =
        BillingReportUtils.groupBillingReportsByProvider(reports);

    providersListMap
        .entrySet()
        .forEach(
            (final Map.Entry<Providers, List<GenericBillingReport>>
                billingReportByProviderEntry) -> {
              try {
                final Providers thisProvider = billingReportByProviderEntry.getKey();
                final List<GenericBillingReport> reportsByProvider =
                    billingReportByProviderEntry.getValue();
                JavaRDD<GenericBillingReport> regionRdd = context.parallelize(reportsByProvider);

                //////////////////////////////////////////////////////////////////////////////
                // 1. map the instance -> instance id //
                // 2. map the BillingReport instance id -> (date, cost) //
                // 3. filter instance id -> (date, cost) by the instance id in instances. //
                // 4. reduce (instance id, (date, cost)) -> (date, sum(cost)) //
                // 5. return (provider, (date -> sum(cost)) //
                //////////////////////////////////////////////////////////////////////////////

                // map BillingReport Report to (id, date) -> cost)
                JavaPairRDD<Tuple2<String, Date>, BigDecimal> counts = regionRdd
                    .mapToPair(
                        (PairFunction<GenericBillingReport, Tuple2<String, Date>, BigDecimal>)
                      x -> {
                          Tuple2<String, Date> key = new Tuple2<>(x.getResourceId(),
                              x.getUsageStartTime());
                          return new Tuple2<>(key, x.getUsageCost());
                        }).reduceByKey((Function2<BigDecimal, BigDecimal, BigDecimal>) (x, y) -> x
                        .add(y, MATH_CONTEXT));

                // filter for only those instance-ids that appear in the instances map.
                // Now aggregate by the same day.
                JavaPairRDD<Date, BigDecimal> results = counts.mapToPair(
                    (PairFunction<Tuple2<Tuple2<String, Date>, BigDecimal>, Date, BigDecimal>) x ->
                        new Tuple2<>(
                        x._1()._2(), x._2)).reduceByKey(
                    (Function2<BigDecimal, BigDecimal, BigDecimal>) (x, y) -> x
                        .add(y, MATH_CONTEXT)).sortByKey();

                // guarantees the values are sorted by the date
                // @TODO: refactor ResourceCostPair to use BigDecimal instead of Double
                List<ResourceCostPair> pairs = results.collect().stream()
                    .map(x -> new ResourceCostPair(x._1(), x._2().doubleValue()))
                    .collect(Collectors.toList());

                ResourcesCostEntry costEntry =
                    new ResourcesCostEntry(thisProvider, new LinkedList<>(pairs));
                entries.add(costEntry);
              } catch (Exception e) {
                log.error("error", e);
              }
            });

    return entries;
  }
}
