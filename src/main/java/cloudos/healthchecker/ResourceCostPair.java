package cloudos.healthchecker;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResourceCostPair {

  public Date date = new Date();

  public Double cost = 0.0;

}
