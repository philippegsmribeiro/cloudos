package cloudos.healthchecker;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.LinkedList;

/**
 * Created by philipperibeiro on 6/27/17.
 */
@Document(collection = "healthchecker_resources_cost")
@Data
@Builder
@AllArgsConstructor
public class ResourcesCost {

    @Id
    @JsonSerialize(using = ToStringSerializer.class)
    private String id;

    private LinkedList<ResourcesCostEntry> resources;
    private Date timestamp;
    private Double cost;

    /**
     * Default constructor for the Resources Cost.
     */
    public ResourcesCost() {
        this.cost = 0.0;
        this.timestamp = new Date();
        this.resources = new LinkedList<>();
    }

    /**
     * @param resources
     */
    public ResourcesCost(@NotNull LinkedList<ResourcesCostEntry> resources, Double cost) {
        this.resources = resources;
        this.cost = cost;
        this.timestamp = new Date();
    }
}
