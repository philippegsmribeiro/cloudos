package cloudos.healthchecker;

import cloudos.instances.InstanceService;

import javax.annotation.Resource;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Created by gleimar on 26/03/2017. */
@Service
@Log4j2
public class HealthCheckerService {

  @Resource
  InstanceService instanceService;

  @Autowired
  ResourcesCostRepository costRepository;

  @Autowired
  ResourceChartRepository resourceChartRepository;

  /**
   * Constructor.
   *
   * @param instanceService service
   * @param costRepository repository
   */
  public HealthCheckerService(InstanceService instanceService,
      ResourcesCostRepository costRepository) {
    this.instanceService = instanceService;
    this.costRepository = costRepository;
  }

  /**
   * Get the resources cost for the last 10 days.
   *
   * @return the latest ResourcesCost entry in the collection.
   */
  public ResourcesCost getLast10DaysCost() {
    return this.costRepository.findFirstByOrderByTimestampDesc();
  }

  /**
   * Get the resource cost of the resourceId for the last 30 days.
   *
   * @param resourceId the Id of the Resource
   * @return the resource chart for the resource.
   */
  public ResourceChart getResourceCost(String resourceId) {
    return this.resourceChartRepository.findFirstByResourceIdOrderByTimestampDesc(resourceId);
  }
}
