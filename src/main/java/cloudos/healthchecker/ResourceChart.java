package cloudos.healthchecker;

import cloudos.Providers;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * Created by philipperibeiro on 7/3/17.
 * <p>
 * <p>Define the model for the Resources Cost Chart. The resource entry model will store the
 * resource's cost for the last 30 days, aggregate the total cost and make it available to be
 * displayed in the Resources page.
 */
@Data
@Builder
@ToString
@AllArgsConstructor
@Document(collection = "healthchecker_resource_entry")
public class ResourceChart {

    @Id
    @JsonSerialize(using = ToStringSerializer.class)
    private String id;
    private String resourceId;
    private BigDecimal cost;
    private Map<Long, BigDecimal> chart;
    private Date timestamp;
    private Providers provider;

    public ResourceChart() {
        this.timestamp = new Date();
    }
}
