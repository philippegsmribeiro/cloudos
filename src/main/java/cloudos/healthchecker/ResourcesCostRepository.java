package cloudos.healthchecker;

import org.springframework.data.mongodb.repository.MongoRepository;

/** Created by philipperibeiro on 6/27/17. */
public interface ResourcesCostRepository extends MongoRepository<ResourcesCost, String> {

  /* Used to return the very last one added */
  ResourcesCost findFirstByOrderByTimestampDesc();
}
