package cloudos.healthchecker;

import cloudos.Providers;
import org.springframework.data.mongodb.repository.MongoRepository;

/** Created by philipperibeiro on 7/3/17. */
public interface ResourceChartRepository extends MongoRepository<ResourceChart, String> {

  /* Used to return the very last one added by the Resource id */
  ResourceChart findFirstByResourceIdOrderByTimestampDesc(String resourceId);

  /* Used to return the very last one added */
  ResourceChart findFirstByOrderByTimestampDesc();

  /* Used to return the very last one added by the resource id and provider */
  ResourceChart findFirstByResourceIdAndProviderOrderByTimestampDesc(
      String resourceId, Providers provider);
}
