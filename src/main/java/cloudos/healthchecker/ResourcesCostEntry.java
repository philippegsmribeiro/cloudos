package cloudos.healthchecker;

import cloudos.Providers;
import java.util.LinkedList;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Inner class that holds the the resource cost entry between the provider and the list of costs.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResourcesCostEntry {

  public Providers providers;

  public LinkedList<ResourceCostPair> cost;

}
