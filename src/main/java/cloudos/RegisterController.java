package cloudos;

import cloudos.security.WebSecurityConfig;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = RegisterController.REGISTER)
public class RegisterController {

  public static final String REGISTER = WebSecurityConfig.API_PATH + "/register";
}
