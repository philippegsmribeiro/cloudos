package cloudos.utils;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import lombok.extern.log4j.Log4j2;

import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/** Created by philipperibeiro on 11/24/16. */
@Log4j2
public final class ChartUtils {

  /** Make the ChartUtils class static. */
  private ChartUtils() {}

  /**
   * Create a XYDataset from a file.
   *
   * @param filename The name of the file
   * @return: A XYDataset object
   * @throws IOException if something goes wrong
   */
  public static XYDataset createDatasetFromFile(String filename) throws IOException {
    File file = new File(filename);
    Scanner scanner = new Scanner(file);

    XYSeriesCollection dataset = new XYSeriesCollection();
    XYSeries series = new XYSeries("Real estate item");

    // Read the price and the living area
    while (scanner.hasNextLine()) {
      if (scanner.hasNextFloat()) {
        float livingArea = scanner.nextFloat();
        float price = scanner.nextFloat();
        series.add(livingArea, price);
      }
    }
    scanner.close();
    dataset.addSeries(series);

    return dataset;
  }
}
