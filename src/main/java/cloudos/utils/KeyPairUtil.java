package cloudos.utils;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.KeyPair;
import java.io.ByteArrayOutputStream;
import java.util.AbstractMap;
import java.util.Map;
import org.apache.log4j.Logger;

/** Created by gleimar on 01/05/2017. */
public class KeyPairUtil {

  private static final Logger logger = Logger.getLogger(KeyPairUtil.class);

  /**
   * Return a Entry where the key field is a private key and the value field is the public key.
   *
   * @param type one of the key type presents in KeyPair. Ex.: KeyPair.RSA
   * @param size the size of the key. Ex.: 2048
   * @param comment a comment to append in the key
   * @return A Map between the strings
   */
  public static Map.Entry<String, String> generateKeyPair(int type, int size, String comment) {

    Map.Entry<String, String> keyPair = null;

    JSch jsch = new JSch();

    try {
      KeyPair kpair = KeyPair.genKeyPair(jsch, type, 2048);

      // TODO: we should use passphrase?
      // kpair.setPassphrase(passphrase);

      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      kpair.writePrivateKey(baos);

      String privateKey = new String(baos.toByteArray(), "UTF-8");

      ByteArrayOutputStream baosp = new ByteArrayOutputStream();
      kpair.writePublicKey(baosp, comment);

      String publicKey = new String(baosp.toByteArray(), "UTF-8");

      keyPair = new AbstractMap.SimpleEntry<>(privateKey, publicKey);

      logger.info(String.format("Finger print: %s", kpair.getFingerPrint()));

      kpair.dispose();
    } catch (Exception e) {
      logger.error("error", e);
    }

    return keyPair;
  }
}
