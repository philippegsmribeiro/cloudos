package cloudos.utils;

import com.google.gson.Gson;

public class JsonResponse {
  private String status;
  private String errorMessage;

  public JsonResponse(String status, String errorMessage) {
    this.setStatus(status);
    this.setErrorMessage(errorMessage);
  }

  @Override
  public String toString() {
    Gson gson = new Gson();
    return gson.toJson(this);
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }
}
