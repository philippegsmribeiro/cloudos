package cloudos.utils;

import java.io.Serializable;

import lombok.extern.log4j.Log4j2;

import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaSparkContext;

/** Created by philipperibeiro on 6/23/17. */
@Log4j2
public final class JavaSparkContextBuilder implements Serializable {

  private static transient SparkContext context;

  private static transient JavaSparkContext jsc;

  private static final String DEFAULT_APP_NAME = "CloudTown";

  private static final String DEFAULT_SPARK_MASTER = "local";

  // define the input URI for mongodb
  private static final String SPARK_MONGO_INPUT_URI = "spark.mongodb.input.uri";

  // define the output URI for mongodb
  private static final String SPARK_MONGO_OUTPUT_URI = "spark.mongodb.output.uri";

  private static final String SPARK_ALLOW_MULTIPLE_CONTEXT = "spark.driver.allowMultipleContexts";

  /* Add information exclusive for MongoDB */
  private String host;

  private Integer port;

  private String database;

  private String username;

  private String password;

  private String master;

  private String appname;

  private String inputCollection;

  private String outputCollection;

  private String inputUri;

  private String outputUri;

  private JavaSparkContextBuilder() {
    // do nothing, this is a singleton
    context = getContext(DEFAULT_APP_NAME);
  }

  /**
   * Use the JavaSparkContext to set up a standard context builder.
   *
   * @return Create new instance of builder with all defaults set.
   */
  public static JavaSparkContextBuilder standard() {
    return new JavaSparkContextBuilder();
  }

  /**
   * Use the JavaSparkContext to set up a default context builder.
   *
   * @return Default JavaSparkContext
   */
  public static JavaSparkContext defaultContext() {
    return standard().build();
  }

  /**
   * Use the JavaSparkContext to set the connection to a Mongo database.
   *
   * @return JavaSparkContext with the set MongoDb connection
   */
  public static JavaSparkContext mongo(String inputUri, String outputUri) {
    return standard().withInputUri(inputUri).withOutputUri(outputUri).mongoBuilder();
  }

  protected static SparkContext getContext(final String appname) {
    if (context  == null || context.isStopped()) {
      // init the context
      SparkConf conf = new SparkConf()
          .setAppName(appname)
          .setMaster(DEFAULT_SPARK_MASTER)
          .set(SPARK_ALLOW_MULTIPLE_CONTEXT, "true");
      // set the context
      context = new SparkContext(conf);
    }
    return context;
  }


  /**
   * Create a new instance of the JavaSparkContext and returns it.
   *
   * @return A JavaSparkContext reference
   */
  protected JavaSparkContext build() {
    // check the status of the Spark context
    if (context == null || context.isStopped()) {
      context = getContext(DEFAULT_APP_NAME);
    }
    // check if the Java Spark Context is already running
    return JavaSparkContext.fromSparkContext(context);
  }

  /**
   * Create a new instance of the JavaSparkContext and returns it.
   *
   * @param appName the name of the all to be constructed.
   * @return A JavaSparkContext reference
   */
  protected JavaSparkContext build(final String appName) {
    // check if the Java Spark Context is already running
    if (context == null || context.isStopped()) {
      context = getContext(appName);
    }
    // check if the Java Spark Context is already running
    return JavaSparkContext.fromSparkContext(context);

  }

  /**
   * Create a JavaSparkContext that can connect to a Mongo Collection.
   *
   * @return A JavaSparkContext reference
   */
  protected JavaSparkContext mongoBuilder() {
    // check if the Java Spark Context is already running
    if (jsc != null) {
      jsc.stop();
    }

    log.info("----------------------------------------------------");
    log.info("Mongo Input URI: " + inputUri);
    log.info("Mongo Output URI: " + outputUri);
    log.info("----------------------------------------------------");

    jsc =
        new JavaSparkContext(
            new SparkConf()
                .setAppName(DEFAULT_APP_NAME)
                .setMaster(DEFAULT_SPARK_MASTER)
                .set(SPARK_MONGO_INPUT_URI, inputUri)
                .set(SPARK_MONGO_OUTPUT_URI, outputUri)
                .set(SPARK_ALLOW_MULTIPLE_CONTEXT, "true"));
    return jsc;
  }

  /**
   * Set the name of the host.
   *
   * @param host the name of the host
   * @return JavaSparkContextBuilder object
   */
  public JavaSparkContextBuilder withHost(String host) {
    this.host = host;
    return this;
  }

  /**
   * Set the Mongo port.
   *
   * @param port The port number
   * @return JavaSparkContextBuilder object
   */
  public JavaSparkContextBuilder withPort(Integer port) {
    this.port = port;
    return this;
  }

  /**
   * Set the name of the database.
   *
   * @param database The name of the database
   * @return JavaSparkContextBuilder object
   */
  public JavaSparkContextBuilder withDatabase(String database) {
    this.database = database;
    return this;
  }

  /**
   * Set the name of the username.
   *
   * @param username the name of the username
   * @return JavaSparkContextBuilder object
   */
  public JavaSparkContextBuilder withUsername(String username) {
    this.username = username;
    return this;
  }

  /**
   * Set the name of the password.
   *
   * @param password the name of the password
   * @return JavaSparkContextBuilder object
   */
  public JavaSparkContextBuilder withPassword(String password) {
    this.password = password;
    return this;
  }

  /**
   * Set the name of the master.
   *
   * @param master the name of the master
   * @return JavaSparkContextBuilder object
   */
  public JavaSparkContextBuilder withMaster(String master) {
    this.master = master;
    return this;
  }

  /**
   * Set the app name.
   *
   * @param appname The name of the app to be used.
   * @return JavaSparkContextBuilder object
   */
  public JavaSparkContextBuilder withAppname(String appname) {
    this.appname = appname;
    return this;
  }

  /**
   * Set the inputCollection.
   *
   * @param inputCollection the URI of the input connection
   * @return JavaSparkContextBuilder object
   */
  public JavaSparkContextBuilder withInputCollection(String inputCollection) {
    this.inputCollection = inputCollection;
    return this;
  }

  /**
   * Set the input URI.
   *
   * @param inputUri the specified input URI
   * @return JavaSparkContextBuilder object
   */
  public JavaSparkContextBuilder withInputUri(String inputUri) {
    this.inputUri = inputUri;
    return this;
  }

  /**
   * Set the output URI.
   *
   * @param outputUri the specified output URI
   * @return JavaSparkContextBuilder object
   */
  public JavaSparkContextBuilder withOutputUri(String outputUri) {
    this.outputUri = outputUri;
    return this;
  }

  /**
   * Set the output collection.
   *
   * @param outputCollection a specified output collection
   * @return JavaSparkContextBuilder object
   */
  public JavaSparkContextBuilder withOutputCollection(String outputCollection) {
    this.outputCollection = outputCollection;
    return this;
  }

  /**
   * Format a Mongo URI to be used when connecting to the Mongo Server.
   *
   * @param collection The name of the mongo collection to be used
   * @return A formated Mongo URI.
   */
  private String formatMongoUri(final String collection) {
    return String.format(
        "mongodb://%s:%s@%s:%s/%s.%s",
        this.username, this.password, this.host, this.port, this.database, collection);
  }

  /**
   * Gets the current input collection.
   *
   * @return the current input collection
   */
  public String getInputCollection() {
    return inputCollection;
  }

  /**
   * Gets the current output collection.
   *
   * @return the current output collection
   */
  public String getOutputCollection() {
    return outputCollection;
  }

  /**
   * Gets the current Spark master.
   *
   * @return The name of the current Spark master
   */
  public String getMaster() {
    return master;
  }

  /**
   * Sets the name of the Spark master.
   *
   * @param master The name of the Spark master
   */
  public void setMaster(String master) {
    this.master = master;
  }

  /**
   * Gets the name of the Spark app.
   *
   * @return the name of the Spark app
   */
  public String getAppname() {
    return appname;
  }
}
