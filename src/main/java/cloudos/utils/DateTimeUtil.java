package cloudos.utils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

/** Created by gleimar on 22/01/2017. */
public abstract class DateTimeUtil {

  private static final String UTC = "UTC";

  /**
   * Return the now value in unix format.
   *
   * @return now time
   */
  public static long getNowTimeUnixFormat(boolean isUtc) {
    LocalDateTime localDateTime = null;

    if (isUtc) {
      localDateTime = LocalDateTime.now(TimeZone.getTimeZone(UTC).toZoneId());
    } else {
      localDateTime = LocalDateTime.now();
    }

    long l = localDateTime.atZone(ZoneOffset.UTC).toInstant().toEpochMilli();

    return l;
  }

  /**
   * Convert a epoch value in milliseconds in a LocaDateTime object.
   *
   * @param epochMilli epoch in milliseconds
   * @return the local date time
   */
  public static LocalDateTime convertToDate(long epochMilli) {
    LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochMilli(epochMilli), ZoneOffset.UTC);

    return date;
  }

  /**
   * Convert a date in milliseconds.
   *
   * @param dateVaue the strings that represents a date value
   * @param dateFormat the string thar represent a format of the date string value
   * @return milliseconds
   */
  public static Long convertToMilliseconds(String dateVaue, String dateFormat) {

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);
    LocalDateTime localDateTime = LocalDateTime.parse(dateVaue, formatter);

    return localDateTime.atZone(ZoneOffset.UTC).toInstant().toEpochMilli();
  }
}
