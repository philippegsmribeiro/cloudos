package cloudos.utils;

import cloudos.exceptions.InvalidRequestException;

import java.util.HashSet;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/**
 * Service validates a generic object using LocalValidatorFactoryBean configured and transform
 * ConstraintViolation into InvalidRequestException.
 * 
 * @author Rogério Souza
 *
 */

@Service
public class BeanValidationUtil {

  @Autowired
  private LocalValidatorFactoryBean localValidatorFactoryBean;


  /**
   * validates a generic object using LocalValidatorFactoryBean configured and transform
   * ConstraintViolation into InvalidRequestException.
   * 
   * @param entity - the object/document/entity to be validated
   * @throws InvalidRequestException if something goes wrong
   */
  public void validate(Object entity) throws InvalidRequestException {
    Validator validator = localValidatorFactoryBean.getValidator();
    Set<ConstraintViolation<Object>> constraintViolations = validator.validate(entity);

    if (constraintViolations.size() > 0) {
      Set<String> violationMessages = new HashSet<String>();

      for (ConstraintViolation<? extends Object> constraintViolation : constraintViolations) {
        violationMessages
            .add(constraintViolation.getPropertyPath() + ": " + constraintViolation.getMessage());
      }

      throw new InvalidRequestException(StringUtils.join(violationMessages, "\n"));
    }
  }

}
