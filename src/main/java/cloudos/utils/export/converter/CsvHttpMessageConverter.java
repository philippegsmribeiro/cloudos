package cloudos.utils.export.converter;

import cloudos.utils.export.FileNameExport;
import cloudos.utils.export.ListExport;
import com.opencsv.CSVWriter;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

/**
 * Specifies a converter that can convert from and to HTTP requests and responses.
 * 
 * @author Alex Calagua
 *
 * @param <T> the object to write to the output message.
 * @param <L> List the object.
 */
@Log4j2
public class CsvHttpMessageConverter<T, L extends ListExport<T>>
    extends AbstractHttpMessageConverter<L> {

  public static final MediaType MEDIA_TYPE = new MediaType("text", "csv", Charset.forName("utf-8"));

  /**
   * Construct that indicating the supported media type.
   */
  public CsvHttpMessageConverter() {
    super(new MediaType("text", "csv"));
  }

  @Override
  protected boolean supports(Class<?> clazz) {
    return ListExport.class.isAssignableFrom(clazz);
  }

  @Override
  protected L readInternal(Class<? extends L> clazz, HttpInputMessage inputMessage)
      throws IOException, HttpMessageNotReadableException {

    HeaderColumnNameMappingStrategy<T> strategy = new HeaderColumnNameMappingStrategy<>();
    Class<T> beanType = toBeanType(clazz.getGenericSuperclass());
    strategy.setType(beanType);

    CsvToBean<T> csvToBean = new CsvToBeanBuilder<T>(new InputStreamReader(inputMessage.getBody()))
        .withMappingStrategy(strategy).build();
    List<T> beanList = csvToBean.parse();

    try {
      L bean = clazz.newInstance();
      bean.setList(beanList);
      return bean;
    } catch (Exception e) {
      log.error("Error reading the information to mount the CSV ", e);
      throw new RuntimeException(e);
    }
  }

  /**
   * Method that returns the annotation value FileNameExport that defines the name of the file to be
   * exported.
   * 
   * @param clazz Class where the annotation will be searched.
   * @param annotation Annotation to look for
   * @param attributeName Annotation attribute that will return the configured value
   * @return Returns the annotation value.
   */
  @SuppressWarnings({"unchecked", "rawtypes"})
  public String getClassAnnotationValue(Class clazz, Class annotation, String attributeName) {
    String value = null;

    Annotation annotationProcessed = clazz.getAnnotation(annotation);
    if (annotation != null) {
      try {
        value = (String) annotationProcessed.annotationType().getMethod(attributeName)
            .invoke(annotationProcessed);
      } catch (Exception e) {
        log.error("Error fetching annotation value", e);
      }
    }
    return value;
  }

  @Override
  protected void writeInternal(L bean, HttpOutputMessage output)
      throws IOException, HttpMessageNotWritableException {

    String fileName =
        getClassAnnotationValue(bean.getClass(), FileNameExport.class, "fileNameExport");

    DateFormat df = new SimpleDateFormat("yyyyMMddhhmmss");
    String fileNameComplete = fileName + df.format(new Date()) + "." + "csv";
    output.getHeaders().setContentType(MEDIA_TYPE);
    output.getHeaders().set("Content-Disposition",
        "attachment; filename=\"" + fileNameComplete + "\"");
    OutputStreamWriter outputStream = new OutputStreamWriter(output.getBody());
    HeaderColumnNameMappingStrategy<T> strategy = new HeaderColumnNameMappingStrategy<>();
    strategy.setType(toBeanType(bean.getClass().getGenericSuperclass()));

    StatefulBeanToCsv<T> beanToCsv = new StatefulBeanToCsvBuilder<T>(outputStream)
        .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER).withMappingStrategy(strategy).build();

    try {
      beanToCsv.write(bean.getList());
      outputStream.close();
    } catch (Exception e) {
      log.error("Error writing info to mount CSV ", e);
      throw new RuntimeException(e);
    }
  }

  @SuppressWarnings("unchecked")
  private Class<T> toBeanType(Type type) {
    return (Class<T>) ((ParameterizedType) type).getActualTypeArguments()[0];
  }
}
