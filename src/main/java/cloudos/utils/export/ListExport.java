package cloudos.utils.export;

import java.util.List;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Class to export a list of a particular entity.
 *
 * @author Alex Calagua
 *
 * @param <T> Type of entity to be exported.
 */
@Getter
@Setter
@EqualsAndHashCode
public class ListExport<T> {
  private List<T> list;
}
