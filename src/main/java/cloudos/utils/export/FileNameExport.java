package cloudos.utils.export;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to store the name of the file to be exported.
 * 
 * @author Alex Calagua
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface FileNameExport {
 
  /**
   * Returns the name of the file to be exported.
   *  
   * @return String
   */
  String fileNameExport();
}
