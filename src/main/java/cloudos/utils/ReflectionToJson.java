package cloudos.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.stream.JsonReader;
import java.io.Reader;
import java.lang.reflect.Type;

/** Created by philipperibeiro on 6/23/17. */
public final class ReflectionToJson {

  private ReflectionToJson() {
    // do nothing...
  }

  private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

  /**
   * Convert an object to a JSON string.
   *
   * @param src the input object
   * @return the JSON String representation
   */
  public static String toString(Object src) {
    return gson.toJson(src);
  }

  public static String toString(Object src, Type typeOfSrc) {
    return gson.toJson(src, typeOfSrc);
  }

  public static <T> T fromJson(String json, Type typeOfT) {
    return gson.fromJson(json, typeOfT);
  }

  public static <T> T fromJson(String json, Class<T> classOfT) {
    return gson.fromJson(json, classOfT);
  }

  public static <T> T fromJson(JsonElement json, Class<T> classOfT) {
    return gson.fromJson(json, classOfT);
  }

  public static <T> T fromJson(JsonElement json, Type typeOfT) {
    return gson.fromJson(json, typeOfT);
  }

  public static <T> T fromJson(JsonReader reader, Type typeOfT) {
    return gson.fromJson(reader, typeOfT);
  }

  public static <T> T fromJson(Reader reader, Class<T> classOfT) {
    return gson.fromJson(reader, classOfT);
  }
}
