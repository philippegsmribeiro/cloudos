package cloudos.utils.pricingupdate;

import com.mongodb.WriteResult;

import java.lang.reflect.ParameterizedType;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

/** Responsible for default operation when update pricing data of a repository. */
@Log4j2
public class GenericPricingUpdateRepository<T> implements CustomGenericPricingUpdateRepository {

  private final MongoOperations mongoOperations;

  @Autowired
  public GenericPricingUpdateRepository(MongoOperations mongoOperations) {
    this.mongoOperations = mongoOperations;
  }

  /**
   * Count the documents in the database where active is true.
   *
   * @return the number of active documents
   */
  private long countDocumentActive() {
    Query query = new Query();
    query.addCriteria(Criteria.where("active").is(Boolean.TRUE));

    return mongoOperations.count(query, getPersistentClass());
  }

  /**
   * Count the documents in the database where active is false.
   *
   * @return the number of inactive documents
   */
  private long countDocumentInactive() {
    Query query = new Query();
    query.addCriteria(Criteria.where("active").is(Boolean.FALSE));

    return mongoOperations.count(query, getPersistentClass());
  }

  /**
   * Remove the documents that the flag active is true.
   *
   * @return The amount of documents removed
   */
  private int removeDocumentsActive() {
    Query query = new Query();
    query.addCriteria(Criteria.where("active").is(Boolean.TRUE));

    WriteResult writeResult = mongoOperations.remove(query, getPersistentClass());

    return writeResult.getN();
  }

  /**
   * Remove the documents that the flag active is true.
   *
   * @return The amount of documents removed
   */
  private int removeInactiveDocument() {
    Query query = new Query();
    query.addCriteria(Criteria.where("active").is(Boolean.FALSE));

    WriteResult writeResult = mongoOperations.remove(query, getPersistentClass());

    return writeResult.getN();
  }

  @Override
  public int activateDataLoaded() {
    log.info(String.format("document:%s", getPersistentClass().getName()));

    log.info(String.format("actives %s=%s", "true", countDocumentActive()));
    log.info(String.format("actives %s=%s", "false", countDocumentInactive()));

    log.info(String.format("delete active=true =%s", removeDocumentsActive()));

    int activateDocumentInactive = activateDocumentInactive();

    log.info(String.format("update to active=true =%s", activateDocumentInactive));

    log.info(String.format("actives %s=%s", "true", countDocumentActive()));
    log.info(String.format("actives %s=%s", "false", countDocumentInactive()));

    return activateDocumentInactive;
  }

  /**
   * Activate the document where the flag active is false.
   *
   * @return the number of documents inactive
   */
  private int activateDocumentInactive() {
    Query query = new Query();
    query.addCriteria(Criteria.where("active").is(Boolean.FALSE));

    Update update = new Update();
    update.set("active", Boolean.TRUE);

    WriteResult updateMulti = mongoOperations.updateMulti(query, update, getPersistentClass());

    return updateMulti.getN();
  }

  @SuppressWarnings("unchecked")
  public Class<T> getPersistentClass() {
    return (Class<T>)
        ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
  }

  @Override
  public boolean hasDataLoaded() {
    return this.countDocumentInactive() > 0;
  }

  @Override
  public void cleanOldData() {
    this.removeInactiveDocument();
  }
}
