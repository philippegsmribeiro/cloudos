package cloudos.utils.pricingupdate;

public interface CustomGenericPricingUpdateRepository {
  /**
   * responsible for.<br>
   *
   * <p>1 - Remove de old documents, where active = true<br> 2 - To active the new documents, set
   * active = true where active = false
   *
   * @return number of active data loaded
   */
  int activateDataLoaded();

  /**
   * Indicates that exists data loaded into database to be updated.
   *
   * <p>When true, it will execute {@link #activateDataLoaded()}.
   *
   * @return if the data has been loaded
   */
  boolean hasDataLoaded();

  /**
   * Before load new pricing data, will clean the oldest data. It is usefull when a workload process
   * has not finished well.
   */
  void cleanOldData();
}
