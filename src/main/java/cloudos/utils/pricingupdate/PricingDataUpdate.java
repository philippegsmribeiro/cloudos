package cloudos.utils.pricingupdate;

import lombok.extern.log4j.Log4j2;

/**
 * Responsible for the behaviour of how the cloud providers update pricing data.
 *
 * <p>It is necessary implement all abstract methods. Then it will be executed by a thread, using
 * Runnable interface.
 *
 * @author gleimar
 */
@Log4j2
public abstract class PricingDataUpdate implements Runnable {

  /**
   * This method is responsible for: Delete active data loaded Update inactive loaded data to
   * active.
   *
   * @return the active value
   */
  public abstract int activeLoadedData();

  /** Fetch and store the fetched data into database. */
  public abstract void loadPricingData();

  /** Remove old data load. */
  public abstract void cleanOldPricingData();

  /**
   * Indicates that exists data loaded into database to be updated.
   *
   * <p>When true, it will execute {@link #activeLoadedData()}.
   *
   * @return if the pricing has been loaded
   */
  public abstract boolean hasPricingLoaded();

  /**
   * This method will:
   *
   * <p>1 - Clean oldest data {@link #cleanOldPricingData()}<br> 2 - Fetch and store new pricing
   * data {@link #loadPricingData()}<br> 3 - If has loaded data {@link #hasPricingLoaded()}, then
   * will be ativated {@link #activeLoadedData()}.<br>
   */
  public final void updatePrincingData() {
    log.info("clean old pricing data ...");
    this.cleanOldPricingData();

    log.info("load new pricing data ...");
    this.loadPricingData();

    log.info("checking has pricing loaded ...");
    if (this.hasPricingLoaded()) {
      log.info("update new pricing data ...");

      this.activeLoadedData();
    }
  }

  @Override
  public void run() {
    this.updatePrincingData();
  }
}
