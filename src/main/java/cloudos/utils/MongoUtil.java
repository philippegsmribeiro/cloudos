package cloudos.utils;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * Created by philipperibeiro on 12/7/16. Add a few utilities since we can't rely on MongoRepository
 * templates for everything
 */
public final class MongoUtil {

  @Autowired private static MongoTemplate mongoTemplate;

  private MongoUtil() {}

  @SuppressWarnings({"unchecked"})
  public static List<String> getInstanceTypes() {
    return mongoTemplate.getCollection("aws_instances").distinct("attributes.instanceType");
  }
}
