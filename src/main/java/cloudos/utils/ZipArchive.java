package cloudos.utils;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import lombok.extern.log4j.Log4j2;

/**
 * Utility to create zip and extract zip files.
 *
 * @author Philippe Ribeiro
 */
@Log4j2
public class ZipArchive {

  public ZipArchive() {}

  /**
   * Given a input file, read the file content and write it to a zip file.
   *
   * @param inputFile the name of the input file
   * @param directory the directory where the zip file will be located
   * @param zipFile the name of the zip file.
   */
  public void zipFile(final String inputFile, final String directory, final String zipFile)
      throws IOException {
    byte[] buffer = new byte[1024];
    FileOutputStream output = null;
    ZipOutputStream zipfile = null;
    FileInputStream input = null;
    try {
      log.info("Attempting to create zip file " + zipFile);
      output = new FileOutputStream(zipFile);
      zipfile = new ZipOutputStream(output);
      ZipEntry entry = new ZipEntry(inputFile);
      zipfile.putNextEntry(entry);
      input = new FileInputStream(directory + File.separator + zipFile);

      int len = 0;
      while ((len = input.read(buffer)) > 0) {
        zipfile.write(buffer, 0, len);
      }

      input.close();
      zipfile.closeEntry();
      log.info("Done");
    } catch (IOException ex) {
      log.error(ex.getMessage());
    } finally {
      // remember close them
      if (input != null) {
        input.close();
      }
      if (zipfile != null) {
        zipfile.close();
      }
      if (output != null) {
        output.close();
      }
    }
  }

  /**
   * Create a zip file from a folder and its sub-folders.
   *
   * @param zipFile the name of the file to be created.
   * @param sourceFolder the folder to be zipped.
   */
  public void zipFolder(final String zipFile, final String sourceFolder) throws IOException {
    List<String> filelist = new ArrayList<>();
    this.generateFileList(sourceFolder, new File(sourceFolder), filelist);

    // define the buffer to read from
    byte[] buffer = new byte[1024];
    FileOutputStream fos = null;
    ZipOutputStream zos = null;
    try {
      fos = new FileOutputStream(zipFile);
      zos = new ZipOutputStream(fos);

      log.info("Output to Zip: " + zipFile);

      for (String file : filelist) {
        log.info("File added: " + file);
        ZipEntry entry = new ZipEntry(file);
        zos.putNextEntry(entry);

        FileInputStream in = new FileInputStream(sourceFolder + File.separator + file);

        int len = 0;
        while ((len = in.read(buffer)) > 0) {
          zos.write(buffer, 0, len);
        }

        in.close();
      }
      zos.closeEntry();
      log.info("Done");
    } catch (IOException ex) {
      log.info(ex.getMessage());
    } finally {
      if (zos != null) {
        // remember close it
        zos.close();
      }
      if (fos != null) {
        fos.close();
      }
    }
  }

  /**
   * Traverse a directory and get all files, and add the file into fileList.
   *
   * @param node file or directory
   */
  public void generateFileList(String source, File node, List<String> filelist) {
    // add file only
    if (node.isFile()) {
      filelist.add(this.generateZipEntry(source, node.getAbsoluteFile().toString()));
    }
    // if Directory
    if (node.isDirectory()) {
      String[] subNote = node.list();
      for (String filename : subNote) {
        this.generateFileList(source, new File(node, filename), filelist);
      }
    }
  }

  /**
   * Format the file path for zip.
   *
   * @param file file path
   * @return Formatted file path
   */
  private String generateZipEntry(String source, String file) {
    return file.substring(source.length() + 1, file.length());
  }

  /**
   * Given a zip file, extract it in the given output folder.
   *
   * @param zipFile The input zip file to be extracted
   * @param outputFolder The file to where the extracted zip file will be placed
   */
  public void unzip(final String zipFile, final String outputFolder) throws IOException {
    byte[] buffer = new byte[1024];
    ZipInputStream input = null;
    try {
      if (!zipFile.endsWith(".zip")) {
        log.warn("File " + zipFile + " is not a zip file");
        return;
      }
      // Create output directory if is not exists
      File folder = new File(outputFolder);
      if (!folder.exists()) {
        log.info("Folder " + outputFolder + " does not exist. Creating...");
        folder.mkdir();
      }
      // get the zip file content
      log.info("Attempting to unzip file " + zipFile);
      input = new ZipInputStream(new FileInputStream(zipFile));
      // get the zipped file list entry
      ZipEntry entry = input.getNextEntry();
      while (entry != null) {
        String filename = entry.getName();
        File file = new File(outputFolder + File.separator + filename);
        log.info("File unzip {}", file.getAbsoluteFile());

        // create all non existing folders
        // else you will hit FileNotFoundException for compressed folder.
        new File(file.getParent()).mkdirs();

        FileOutputStream fos = new FileOutputStream(file);
        int len;
        while ((len = input.read(buffer)) > 0) {
          fos.write(buffer, 0, len);
        }
        fos.close();
        entry = input.getNextEntry();
      }
      input.closeEntry();
      input.close();
      log.info("Done unzipping file " + zipFile);
    } catch (IOException ex) {
      log.error(ex.getMessage());
    } finally {
      if (input != null) {
        input.close();
      }
    }
  }

  /**
   * Check if a file is a .zip file.
   *
   * @param file The candidate zip file
   * @return whether the file object is a zip file or not.
   * @throws IOException if failed to read the file
   */
  public boolean isZipFile(File file) throws IOException {
    if (file.isDirectory()) {
      return false;
    }
    if (!file.canRead()) {
      throw new IOException("Cannot read file " + file.getAbsolutePath());
    }
    if (file.length() < 4) {
      return false;
    }
    DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
    int test = in.readInt();
    in.close();
    return test == 0x504b0304;
  }

  /**
   * Check if a given file is a zip file.
   *
   * @param filename the file full path
   * @return whether the file is a zip file or not
   * @throws IOException if failed to read the file
   */
  public boolean isZipFile(String filename) throws IOException {
    File file = new File(filename);
    return this.isZipFile(file);
  }
}
