package cloudos.utils;

public class OsUtils {

  /**
   * Convert a unix path to a Windows path.
   *
   * @param path the string path in the unix format
   * @return a converted path to the windows standard
   */
  public static String convertWindowsPathToWindowsBashPath(String path) {
    if (path.contains(":")) {
      String driver = path.substring(0, path.indexOf(":"));
      driver = "/mnt/" + driver.toLowerCase();
      return driver + path.substring(path.indexOf("\\")).replace("\\", "/");
    }
    return path.replace("\\", "/");
  }
}
