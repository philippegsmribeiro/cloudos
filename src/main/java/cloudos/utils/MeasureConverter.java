package cloudos.utils;

public final class MeasureConverter {

  private static final double MEGABYTE_TO_GIGABYTE = 1000.0;

  private MeasureConverter() {}

  /**
   * Convert the capacity in Megabytes to Gigabytes.
   *
   * @return Capacity in Gigabytes
   */
  public static double convertMbToGb(double size) {
    assert (size >= 0);
    return size / MEGABYTE_TO_GIGABYTE;
  }
}
