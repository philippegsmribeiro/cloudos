package cloudos.utils;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

/**
 * Utils about profiles.
 *
 */
@Service
public class ProfileUtils {

  public static final String DEVELOPMENT = "development";
  public static final String TEST = "test";
  public static final String PRODUCTION = "production";
  public static final String DEPLOY = "deploy";
  public static final String DOCKER = "docker";

  @Autowired
  private Environment env;

  /**
   * Verify if profile test is included.
   * @return true if yes, false otherwise
   */
  public boolean isTest() {
    if (Arrays.asList(env.getActiveProfiles()).contains(TEST)) {
      return true;
    }
    return false;
  }

  /**
   * Verify if profile deploy is included.
   * @return true if yes, false otherwise
   */
  public boolean isDeploy() {
    if (Arrays.asList(env.getActiveProfiles()).contains(DEPLOY)) {
      return true;
    }
    return false;
  }
  
  /**
   * Returns the first and main profile.
   * @return main profile name
   */
  public String getMainProfile() {
    return env.getActiveProfiles()[0];
  }

  /**
   * Returns if profile docker is included.
   * @return true if yes, false otherwise
   */
  public boolean isDocker() {
    if (Arrays.asList(env.getActiveProfiles()).contains(DOCKER)) {
      return true;
    }
    return false;
  }

}
