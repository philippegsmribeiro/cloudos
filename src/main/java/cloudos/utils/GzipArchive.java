package cloudos.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import lombok.extern.log4j.Log4j2;

/** Created by philipperibeiro on 11/24/16. Utility class to create and extract gzip files. */
@Log4j2
public class GzipArchive {

  public GzipArchive() {}

  /**
   * Extract a file from a gzip file.
   *
   * @param input The input gzip file.
   * @param output The output file.
   */
  public void gunzipIt(final String input, final String output) {

    byte[] buffer = new byte[1024];

    try {
      GZIPInputStream gzis = new GZIPInputStream(new FileInputStream(input));
      FileOutputStream out = new FileOutputStream(output);

      int len;
      while ((len = gzis.read(buffer)) > 0) {
        out.write(buffer, 0, len);
      }
      /* Close the input files */

      gzis.close();
      out.close();

      log.info("Done zipping file {}", input);
    } catch (IOException ex) {
      log.error(ex.getMessage());
    }
  }

  /**
   * Checks if an input stream is gzipped.
   *
   * @param in the input stream
   * @return if the given input stream is a gz file
   */
  public boolean isGZipped(InputStream in) {
    if (!in.markSupported()) {
      in = new BufferedInputStream(in);
    }
    in.mark(2);
    int magic = 0;
    try {
      magic = in.read() & 0xff | ((in.read() << 8) & 0xff00);
      in.reset();
    } catch (IOException e) {
      log.error(e.getMessage());
      return false;
    }
    return magic == GZIPInputStream.GZIP_MAGIC;
  }

  /**
   * Checks if a file is gzipped.
   *
   * @param filename The name of the file to be checked
   * @return If the file is of type gz or not
   */
  public boolean isGZipped(String filename) {
    int magic = 0;
    try {
      File file = new File(filename);
      RandomAccessFile raf = new RandomAccessFile(file, "r");
      magic = raf.read() & 0xff | ((raf.read() << 8) & 0xff00);
      raf.close();
    } catch (Throwable e) {
      log.error(e.getMessage());
    }
    return magic == GZIPInputStream.GZIP_MAGIC;
  }

  /**
   * Create a new GZip file from the input file.
   *
   * @param input The full path of the file to be gzipped.
   * @param output The output gzip file.
   */
  public void gzipIt(final String input, final String output) {

    byte[] buffer = new byte[1024];
    try {
      GZIPOutputStream gzos = new GZIPOutputStream(new FileOutputStream(output));
      FileInputStream in = new FileInputStream(input);

      int len;
      while ((len = in.read(buffer)) > 0) {
        gzos.write(buffer, 0, len);
      }

      in.close();

      gzos.finish();
      gzos.close();

      log.info("completed");
    } catch (IOException ex) {
      log.error(ex.getMessage());
    }
  }
}
