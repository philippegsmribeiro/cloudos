package cloudos.utils;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

/**
 * Created by philipperibeiro on 7/6/17.
 * Util class that allows the creation of csv files.
 */
public final class CsvUtils {

  private static final char DEFAULT_SEPARATOR = ',';

  private CsvUtils() {
    // do nothing
  }

  /**
   * Write the values.
   *
   * @param w the buffer writer
   * @param values a list of values equivalent to a row in the csv file
   * @throws IOException if fails to write the given like to the file
   */
  public static void writeLine(Writer w, List<String> values) throws IOException {
    writeLine(w, values, DEFAULT_SEPARATOR, ' ');
  }

  /**
   * Write line of values stored in the list into the Writer.
   *
   * @param w the buffer writer
   * @param values a list of values equivalent to a row in the csv file
   * @param separators the separator for the csv file
   * @throws IOException if fails to write the given like to the file
   */
  public static void writeLine(Writer w, List<String> values, char separators) throws IOException {
    writeLine(w, values, separators, ' ');
  }

  /**
   * Write line of values stored in the list into the Writer.
   *
   * @param w the buffer writer
   * @param values a list of values equivalent to a row in the csv file
   * @param separators the separator for the csv file
   * @param customQuote a user defined custom quote
   * @throws IOException if fails to write the given like to the file
   */
  public static void writeLine(Writer w, List<String> values, char separators, char customQuote)
      throws IOException {

    boolean first = true;

    // default customQuote is empty

    if (separators == ' ') {
      separators = DEFAULT_SEPARATOR;
    }

    StringBuilder sb = new StringBuilder();
    for (String value : values) {
      if (!first) {
        sb.append(separators);
      }
      if (customQuote == ' ') {
        sb.append(followCsvformat(value));
      } else {
        sb.append(customQuote).append(followCsvformat(value)).append(customQuote);
      }

      first = false;
    }
    sb.append("\n");
    w.append(sb.toString());
  }

  /**
   * Format the value to the CSV default format.
   *
   * @param value a given string value
   * @return the formatted version of the value
   */
  // https://tools.ietf.org/html/rfc4180
  private static String followCsvformat(String value) {

    String result = value;
    if (result.contains("\"")) {
      result = result.replace("\"", "\"\"");
    }
    return result;
  }


}
