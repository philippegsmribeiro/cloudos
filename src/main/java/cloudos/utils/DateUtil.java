package cloudos.utils;

import cloudos.Application;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.WeekFields;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import org.springframework.data.util.Pair;

public final class DateUtil {

  /* The default string format for dates */
  private static final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
  
  public static final String ISO_DATE = "yyyy-MM-dd";

  private DateUtil() {
    // nothing to do
  }

  /**
   * Return the actual data and time formatted.
   *
   * @return the current date
   */
  public static String getNow() {
    return dateFormat.format(new Date());
  }

  /**
   * Print the current date in string format.
   *
   * @param date the date to be printed
   * @return the string format of the date
   */
  public static String printDate(Date date) {
    return dateFormat.format(date);
  }

  /**
   * Get the current day of the week.
   *
   * @return the string format of the date
   */
  public static String getDay() {
    Calendar currentDate = new GregorianCalendar();
    return currentDate
        .getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Application.LOCALE_DEFAULT);
  }

  /**
   * Get the current week of the month.
   *
   * @return the string format of the date
   */
  public static String getWeek() {
    WeekFields weekFields = WeekFields.of(Application.LOCALE_DEFAULT);
    return weekFields.toString();
  }

  /**
   * Get the current month of the year.
   *
   * @return the string format of the date
   */
  public static String getMonth() {
    // get the current month.
    Calendar cal = Calendar.getInstance();
    return cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Application.LOCALE_DEFAULT);
  }

  /**
   * Get the range between the first day of the month and the current day.
   *
   * @return a pair containing the range of the dates
   */
  public static Pair<Date, Date> getDateRange() {
    Date beginning;
    Date end;

    {
      Calendar calendar = getCalendarForNow();
      calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
      setTimeToBeginningOfDay(calendar);
      beginning = calendar.getTime();
    }

    {
      Calendar calendar = getCalendarForNow();
      calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
      setTimeToEndofDay(calendar);
      end = calendar.getTime();
    }

    return Pair.of(beginning, end);
  }

  /**
   * Get the current calendar in Gregorian format.
   *
   * @return the current calendar
   */
  public static Calendar getCalendarForNow() {
    Calendar calendar = GregorianCalendar.getInstance();
    calendar.setTime(new Date());
    return calendar;
  }

  /**
   * Set the beginning of the day at the 00:00:00:000 time frame.
   *
   * @param calendar a calendar
   */
  private static void setTimeToBeginningOfDay(Calendar calendar) {
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
  }

  /**
   * Set the end of the day at 23:59:49:999 time frame.
   *
   * @param calendar a calendar
   */
  private static void setTimeToEndofDay(Calendar calendar) {
    calendar.set(Calendar.HOUR_OF_DAY, 23);
    calendar.set(Calendar.MINUTE, 59);
    calendar.set(Calendar.SECOND, 59);
    calendar.set(Calendar.MILLISECOND, 999);
  }

  /**
   * Get the range between the first day of the previous month and the final day the previous month.
   *
   * @return a pair containing the range of the dates
   */
  public static Pair<Date, Date> getDateRangePreviousMonth() {

    Date beginning;
    Date end;

    {
      Calendar calendar = getCalendarForNow();
      calendar.add(Calendar.MONTH, -1);
      calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
      setTimeToBeginningOfDay(calendar);
      beginning = calendar.getTime();
    }

    {
      Calendar calendar = getCalendarForNow();
      calendar.add(Calendar.MONTH, -1);
      calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
      setTimeToEndofDay(calendar);
      end = calendar.getTime();
    }
    return Pair.of(beginning, end);
  }

  /**
   * Method that returns the number of the last day of the month.
   * 
   * @return int referring to the last day of the current month
   */
  public static int getLastDayCurrentMonth() {
    Calendar lastDayCalendar = GregorianCalendar.getInstance();
    lastDayCalendar.setTime(getDateRange().getSecond());
    return lastDayCalendar.get(Calendar.DAY_OF_MONTH);
  }

  /**
   * Method that returns a date referring to the end of the current day.
   * 
   * @return calendar referring end of the current day
   */
  public static Calendar getDateNowEndToDay() {
    Calendar calendar = getCalendarForNow();
    setTimeToEndofDay(calendar);
    return calendar;
  }
  
  /**
   * Parse date time with DateTimeFormatter.ISO_DATE_TIME
   * 
   * @param timestamp timestamp string
   * @return Date
   */
  public static Date getIsoDateTime(String timestamp) {

    TemporalAccessor temporalAccessor = DateTimeFormatter.ISO_DATE_TIME.parse(timestamp);
    Date date = Date.from(Instant.from(temporalAccessor));
    return date;
  }

  /**
   * Converts a {@link LocalDate} into {@link Date}.
   *
   * @return a {@link Date} object or @{code null} if a null argument is provided
   */
  public static Date toDate(LocalDate date) {
    if (date == null) {
      return null;
    }
    return Date.from(date.atStartOfDay(Application.ZONE_ID_DEFAULT).toInstant());
  }
  
  /**
   * Converts a {@link LocalDate} into {@link Date}.
   *
   * @return a {@link Date} object or @{code null} if a null argument is provided
   */
  public static Date toDate(LocalDateTime date) {
    if (date == null) {
      return null;
    }
    return Date.from(date.atZone(Application.ZONE_ID_DEFAULT).toInstant());
  }

  /**
   * Get date difference in milliseconds.
   */
  public static long getDifferenceInMillis(Date start, Date end) {
    return end.getTime() - start.getTime();
  }

  /**
   * Converts a Date object into a LocalDate instance.
   */
  public static LocalDate toLocalDate(Date input) {
    LocalDate date = input.toInstant().atZone(Application.ZONE_ID_DEFAULT).toLocalDate();
    return date;
  }
}
