package cloudos.utils;

import java.awt.Color;

import javax.swing.JTabbedPane;

import lombok.extern.log4j.Log4j2;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.function.Function2D;
import org.jfree.data.function.LineFunction2D;
import org.jfree.data.function.PowerFunction2D;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.data.statistics.Regression;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;

/** Created by philipperibeiro on 11/24/16. */
@Log4j2
public class RegressionChart extends ApplicationFrame {

  /* Dataset */
  private XYDataset dataset;

  /**
   * Creates a new instance of the demo application.
   *
   * @param title the frame title
   */
  public RegressionChart(String title, XYDataset dataset) {
    super(title);
    this.dataset = dataset;
    getContentPane().add(createContent());
  }

  /**
   * Constructor that takes all the parameters.
   *
   * @param title the title of the chart
   * @param seriesName the name of the series
   * @param x the x axis values
   * @param y the y axis values
   * @throws Exception any exception that may occur
   */
  public RegressionChart(String title, String seriesName, double[] x, double[] y) throws Exception {
    super(title);
    XYSeries series = new XYSeries(seriesName);
    if (x.length != y.length) {
      throw new Exception("The dimension of the vectors x and y do not match");
    }
    for (int i = 0; i < x.length; i++) {
      series.add(x[i], y[i]);
    }
    this.dataset = new XYSeriesCollection(series);
    getContentPane().add(createContent());
  }

  /**
   * Creates a tabbed pane for displaying sample charts.
   *
   * @return the tabbed pane.
   */
  private JTabbedPane createContent() {
    JTabbedPane tabs = new JTabbedPane();
    tabs.add("Linear", createChartPanel1());
    tabs.add("Power", createChartPanel2());
    return tabs;
  }

  /**
   * Creates a chart based on the first dataset, with a fitted linear regression line.
   *
   * @return the chart panel
   */
  private ChartPanel createChartPanel1() {

    // create plot...
    NumberAxis xAxis = new NumberAxis("X");
    xAxis.setAutoRangeIncludesZero(false);
    NumberAxis yAxis = new NumberAxis("Y");
    yAxis.setAutoRangeIncludesZero(false);

    XYLineAndShapeRenderer renderer1 = new XYLineAndShapeRenderer(false, true);
    XYPlot plot = new XYPlot(this.dataset, xAxis, yAxis, renderer1);

    // calculate the regression and create subplot 2...
    double[] coefficients = Regression.getOLSRegression(this.dataset, 0);
    Function2D curve = new LineFunction2D(coefficients[0], coefficients[1]);
    XYDataset regressionData =
        DatasetUtilities.sampleFunction2D(curve, 2.0, 11.0, 100, "Fitted Regression Line");

    plot.setDataset(1, regressionData);
    XYLineAndShapeRenderer renderer2 = new XYLineAndShapeRenderer(true, false);
    renderer2.setSeriesPaint(0, Color.blue);
    plot.setRenderer(1, renderer2);

    // create and return the chart panel...
    JFreeChart chart =
        new JFreeChart("Linear Regression", JFreeChart.DEFAULT_TITLE_FONT, plot, true);
    return new ChartPanel(chart, false);
  }

  /**
   * Creates a chart based on the second dataset, with a fitted power regression line.
   *
   * @return the chart panel
   */
  private ChartPanel createChartPanel2() {

    // create subplot 1...
    NumberAxis xVariableAxis = new NumberAxis("X");
    xVariableAxis.setAutoRangeIncludesZero(false);
    NumberAxis yAxis = new NumberAxis("Y");
    yAxis.setAutoRangeIncludesZero(false);

    XYLineAndShapeRenderer renderer1 = new XYLineAndShapeRenderer(false, true);
    XYPlot plot = new XYPlot(this.dataset, xVariableAxis, yAxis, renderer1);

    // calculate the regression and create subplot 2...
    double[] coefficients = Regression.getPowerRegression(this.dataset, 0);
    Function2D curve = new PowerFunction2D(coefficients[0], coefficients[1]);
    XYDataset regressionData =
        DatasetUtilities.sampleFunction2D(curve, 2.0, 11.0, 100, "Fitted Regression Line");

    XYLineAndShapeRenderer renderer2 = new XYLineAndShapeRenderer(true, false);
    renderer2.setSeriesPaint(0, Color.blue);
    plot.setDataset(1, regressionData);
    plot.setRenderer(1, renderer2);

    // create and return the chart panel...
    JFreeChart chart =
        new JFreeChart("Power Regression", JFreeChart.DEFAULT_TITLE_FONT, plot, true);
    ChartPanel chartPanel = new ChartPanel(chart, false);
    return chartPanel;
  }
}
