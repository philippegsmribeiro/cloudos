package cloudos.utils.datafactory;

import cloudos.Providers;
import cloudos.provider.ProviderMachineType;
import cloudos.provider.ProviderRegion;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;

/**
 * Utility class to help assemble data for use in unit tests.
 * 
 * @author Alex Calagua
 *
 */
public class DataFactory {
  private static Random original_random = new Random(93285);
  public static final String HYPHEN = "-";
  public static final String INSTANCE = "instance";
  public static final String DESCRIPTION = "description";
  private DataValues dataValues = new DataValues();
  private Random random = new Random();

  public DataFactory() {
    this(original_random);
  }

  private DataFactory(final Random random) {
    this.random = random;
  }

  /**
   * Backwards compatible constructor that creates a datafactory driven by the original instance of
   * random.
   *
   * @return DataFactory instance with a shared random
   */
  public static DataFactory createWithOriginalRandom() {
    return new DataFactory(original_random);
  }

  /**
   * Returns a random item from a list of items.
   *
   * @param <T> Item type in the list and to return
   * @param items List of items to choose from
   * @return Item from the list
   */
  public <T> T getItem(final List<T> items) {
    return getItem(items, 100, null);
  }

  /**
   * Returns a random item from a list of items or the null depending on the probability parameter.
   * The probability determines the chance (in %) of returning an item off the list versus null.
   *
   * @param <T> Item type in the list and to return
   * @param items List of items to choose from
   * @param probability chance (in %, 100 being guaranteed) of returning an item from the list
   * @return Item from the list or null if the probability test fails.
   */
  public <T> T getItem(final List<T> items, final int probability) {
    return getItem(items, probability, null);
  }

  /**
   * Returns a random item from a list of items or the defaultItem depending on the probability
   * parameter. The probability determines the chance (in %) of returning an item off the list
   * versus the default value.
   *
   * @param <T> Item type in the list and to return
   * @param items List of items to choose from
   * @param probability chance (in %, 100 being guaranteed) of returning an item from the list
   * @param defaultItem value to return if the probability test fails
   * @return Item from the list or the default value
   */
  public <T> T getItem(final List<T> items, final int probability, final T defaultItem) {
    if (items == null) {
      throw new IllegalArgumentException("Item list cannot be null");
    }
    if (items.isEmpty()) {
      throw new IllegalArgumentException("Item list cannot be empty");
    }

    return chance(probability) ? items.get(random.nextInt(items.size())) : defaultItem;
  }

  /**
   * Returns a random item from an array of items or the defaultItem depending on the probability
   * parameter. The probability determines the chance (in %) of returning an item from the array
   * versus the default value.
   *
   * @param <T> Array item type and the type to return
   * @param items Array of items to choose from
   * @param probability chance (in %, 100 being guaranteed) of returning an item from the array
   * @param defaultItem value to return if the probability test fails
   * @return Item from the array or the default value
   */
  public <T> T getItem(final T[] items, final int probability, final T defaultItem) {
    if (items == null) {
      throw new IllegalArgumentException("Item array cannot be null");
    }
    if (items.length == 0) {
      throw new IllegalArgumentException("Item array cannot be empty");
    }
    return chance(probability) ? items[random.nextInt(items.length)] : defaultItem;
  }

  /**
   * Returns a random item from an array of items.
   *
   * @param <T> Array item type and the type to return
   * @param items Array of items to choose from
   * @return Item from the array
   */
  public <T> T getItem(final T[] items) {
    return getItem(items, 100, null);
  }


  /**
   * Gives you a true/false based on a probability with a random number generator. Can be used to
   * optionally add elements.
   *
   * <pre>
   * if (DataFactory.chance(70)) {
   *   // 70% chance of this code being executed
   * }
   * </pre>
   *
   * @param chance % chance of returning true
   * @return true or false value based on the random number generated and the chance value passed in
   */
  public boolean chance(final int chance) {
    return random.nextInt(100) < chance;
  }

  /**
   * Returns a random int value.
   *
   * @return random number
   */
  public int getNumber() {
    return random.nextInt();
  }

  /**
   * Returns a random number between 0 and max.
   *
   * @param max Maximum value of result
   * @return random number no more than max
   */
  public int getNumberUpTo(final int max) {
    return getNumberBetween(0, max);
  }

  /**
   * Returns a number betwen min and max.
   *
   * @param min minimum value of result
   * @param max maximum value of result
   * @return Random number within range
   */
  public int getNumberBetween(final int min, final int max) {
    if (max < min) {
      throw new IllegalArgumentException(
          String.format("Minimum must be less than minimum (min=%d, max=%d)", min, max));
    }
    if (max == min) {
      return min;
    }
    return min + random.nextInt(max - min);
  }

  /**
   * Method that returns a decimal value that is between the min and max values.
   * 
   * @param min minimum value of result
   * @param max maximum value of result
   * @return Random double value.
   */
  public double getDoubleBetween(double min, double max) {
    if (max < min) {
      throw new IllegalArgumentException(
          String.format("Minimum must be less than minimum (min=%d, max=%d)", min, max));
    }

    if (max == min) {
      return min;
    }
    Random r = new Random();
    return r.doubles(min, (max + 1)).limit(1).findFirst().getAsDouble();
  }

  /**
   * Method that returns a random boolean value.
   * 
   * @return boolean
   */
  public boolean getBoolean() {
    return random.nextBoolean();
  }

  /**
   * Builds a date from the year, month, day values passed in.
   *
   * @param year The year of the final {@link Date} result
   * @param month The month of the final {@link Date} result (from 1-12)
   * @param day The day of the final {@link Date} result
   * @return Date representing the passed in values.
   */
  public Date getDate(final int year, final int month, final int day) {
    Calendar cal = Calendar.getInstance();
    cal.clear();
    cal.set(year, month - 1, day, 0, 0, 0);
    return cal.getTime();
  }

  /**
   * Returns a random date which is in the range <code>baseData</code> +
   * <code>minDaysFromData</code> to <code>baseData</code> + <code>maxDaysFromData</code>. This
   * method does not alter the time component and the time is set to the time value of the base
   * date.
   *
   * @param baseDate Date to start from
   * @param minDaysFromDate minimum number of days from the baseDate the result can be
   * @param maxDaysFromDate maximum number of days from the baseDate the result can be
   * @return A random date
   */
  public Date getDate(final Date baseDate, final int minDaysFromDate, final int maxDaysFromDate) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(baseDate);
    int diff = minDaysFromDate + (random.nextInt(maxDaysFromDate - minDaysFromDate));
    cal.add(Calendar.DATE, diff);
    return cal.getTime();
  }

  /**
   * Returns a random date between two dates. This method will alter the time component of the dates
   *
   * @param minDate Minimum date that can be returned
   * @param maxDate Maximum date that can be returned
   * @return random date between these two dates.
   */
  public Date getDateBetween(final Date minDate, final Date maxDate) {
    // this can break if seconds is an int
    long seconds = (maxDate.getTime() - minDate.getTime()) / 1000;
    seconds = (long) (random.nextDouble() * seconds);
    Date result = new Date();
    result.setTime(minDate.getTime() + (seconds * 1000));
    return result;
  }

  /**
   * Method that valid maximum and minimum length value.
   * 
   * @param minLength minimum length
   * @param maxLength maximum length
   */
  private void validateMinMaxParams(final int minLength, final int maxLength) {
    if (minLength < 0) {
      throw new IllegalArgumentException("Minimum length must be a non-negative number");
    }
    if (maxLength < 0) {
      throw new IllegalArgumentException("Maximum length must be a non-negative number");
    }
    if (maxLength < minLength) {
      throw new IllegalArgumentException(
          String.format("Minimum length must be less than maximum length (min=%d, max=%d)",
              minLength, maxLength));
    }
  }

  /**
   * Return a string containing between <code>length</code> random characters.
   *
   * @param minLength minimum number of characters to use in the string
   * @param maxLength maximum number of characters to use in the string
   *
   * @return A string containing <code>length</code> random characters
   */
  public String getRandomChars(final int minLength, final int maxLength) {
    validateMinMaxParams(minLength, maxLength);
    StringBuilder sb = new StringBuilder(maxLength);

    int length = minLength;
    if (maxLength != minLength) {
      length = length + random.nextInt(maxLength - minLength);
    }
    while (length > 0) {
      sb.append(getRandomChar());
      length--;
    }
    return sb.toString();
  }

  /**
   * Return a string containing between <code>length</code> random characters.
   * 
   * @return a random character.
   */
  public char getRandomChar() {
    return (char) (random.nextInt(26) + 'a');
  }


  /**
   * Method used to assemble words at random. You receive the prefix, suffix, separator, and group
   * of words.
   * 
   * @param prefix prefix of words.
   * @param suffix suffix of words.
   * @param charSeparator charSeparator of words.
   * @return array of word list.
   */
  private String[] mountWord(String prefix, String suffix, String charSeparator, String[] words) {
    String[] wordsFinal = new String[words.length];
    for (int i = 0; i < words.length; i++) {
      StringJoiner joiner = new StringJoiner(charSeparator);
      if (StringUtils.isNotBlank(prefix)) {
        joiner.add(prefix);
      }
      joiner.add(words[0]);
      if (StringUtils.isNotBlank(suffix)) {
        joiner.add(suffix);
      }
      wordsFinal[i] = joiner.toString();
    }
    return wordsFinal;
  }

  /**
   * Method that returns a random region.
   * 
   * @param provider used to filter
   * @param regions list the region
   * @return A random region
   */
  public String getRegion(Providers provider, List<ProviderRegion> regions) {
    List<ProviderRegion> regionsFilterByProvider =
        regions.stream().filter(p -> p.getProvider().equals(provider)).collect(Collectors.toList());
    if (!regionsFilterByProvider.isEmpty()) {
      return getItem(regionsFilterByProvider).getRegion();
    } else {
      return getItem(dataValues.getMachineTypes(provider));
    }
  }

  /**
   * Method that returns a random machine type.
   * 
   * @param provider used to filter
   * @param machineTypes list the machineType
   * @return A random machine type
   */
  public String getMachineType(Providers provider, List<ProviderMachineType> machineTypes) {
    List<ProviderMachineType> machineTypesFilterByProvider = machineTypes.stream()
        .filter(p -> p.getProvider().equals(provider)).collect(Collectors.toList());
    if (!machineTypesFilterByProvider.isEmpty()) {
      return getItem(machineTypesFilterByProvider).getRegion();
    } else {
      return getItem(dataValues.getMachineTypes(provider));
    }

  }

  /**
   * Method that returns a random alphanumeric.
   * 
   * @return A random alphanumeric
   */
  public String getAlphaNumericRandom() {
    return RandomStringUtils.randomAlphanumeric(10);
  }

  /**
   * Method that returns a random instance name.
   * 
   * @return A random instance name
   */
  public String getInstanceName(String prefix, String suffix, String charSeparator) {
    String[] wordsFinal = mountWord(prefix, suffix, charSeparator, dataValues.getServerTypes());
    return getItem(wordsFinal);
  }


}
