package cloudos.utils.datafactory;

import cloudos.Providers;

/**
 * Class that stores the default values ​​of the datafactory.
 * 
 * @author Alex Calagua
 *
 */
public class DataValues {

  public static String[] amazonRegions = {"us-east-1", "us-east-2", "us-west-1", "us-west-2",
      "eu-west-1a", "ap-south-1", "ca-central-1"};
  public static String[] googleRegions = {"us-east1-b", "us-east1-c", "us-east1-d", "us-east4-c",
      "us-east4-b", "us-east4-a", "us-central1-a", "us-central1-b", "us-central1-c",
      "us-central1-f", "asia-south1-c", "asia-south1-b", "asia-south1-a",
      "northamerica-northeast1-a", "northamerica-northeast1-b", "northamerica-northeast1-c"};
  public static String[] amazonMachineType =
      {"t2.nano", "t2.medium", "t2.xlarge", "m5.large", "m4.xlarge", "t2.micro"};
  public static String[] googleMachineType = {"n1-standard-1", "n1-standard-4", "n1-highmem-2",
      "n1-highmem-8", "n1-highcpu-2", "f1-micro"};
  public static String[] serversTypes =
      {"web", "server", "ldap", "virtual", "fileserver", "database", "message", "email", "proxy"};

  /**
   * Method that returns the regions by the provider.
   * 
   * @param provider used to filter
   * @return regions by provider
   */
  public String[] getRegions(Providers provider) {
    if (Providers.AMAZON_AWS.equals(provider)) {
      return amazonRegions;
    } else if (Providers.GOOGLE_COMPUTE_ENGINE.equals(provider)) {
      return googleRegions;
    } else {
      return new String[] {"default-region"};
    }
  }
  
  /**
   * Method that returns the machine types by the provider.
   * 
   * @param provider used to filter
   * @return machine types by provider
   */
  public String[] getMachineTypes(Providers provider) {
    if (Providers.AMAZON_AWS.equals(provider)) {
      return amazonMachineType;
    } else if (Providers.GOOGLE_COMPUTE_ENGINE.equals(provider)) {
      return googleMachineType;
    } else {
      return new String[] {"default-machine-type"};
    }
  }


  public String[] getServerTypes() {
    return serversTypes;
  }
}
