package cloudos.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/** Created by gleimar on 26/03/2017. */
public abstract class IpAddressUtils {

  /**
   * Return the external ip of the machine.
   * @return external ip
   */
  public static String getExternalIpAddress() {

    String ip = null;
    BufferedReader in = null;

    try {
      URL whatismyip = new URL("http://checkip.amazonaws.com");

      in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));
      ip = in.readLine();

    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (in != null) {
        try {
          in.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    return ip;
  }
}
