package cloudos.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import lombok.extern.log4j.Log4j2;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.io.IOUtils;

/**
 * <p>
 * Utility to create a Tar file, either from a file or a directory.
 *</p>
 *
 * @author Philippe Ribeiro
 */
@Log4j2
public class TarArchive {

  private static final String TAR_EXTENSION = ".tar";

  public TarArchive() {}

  /**
   * Create a tar archive from the source file in the directory 'directory'.
   *
   * @param source the source file to be tar-ed
   * @param directory the directory where there tar-ed file will be located.
   */
  public String createArchive(String source, String directory) throws IOException {
    TarArchiveOutputStream outputStream = null;
    try {
      File sourcefile = new File(source);

      // Returns the name of the file or directory denoted by this abstract pathname.
      String filename = sourcefile.getName();

      // Returns the pathname string of this abstract pathname's parent.
      log.info("--------------------------------------");
      String destpath = directory + File.separator + filename + TAR_EXTENSION;
      log.info("Archived location: " + destpath);
      log.info("Filename: " + filename);
      log.info("Dest Path: " + destpath);
      log.info("--------------------------------------");
      outputStream = new TarArchiveOutputStream(new FileOutputStream(new File(destpath)));

      // invoke the private createArchive method
      this.createArchive(sourcefile, outputStream, directory);

      // Flushes this output stream and forces any buffered output to be written out.
      outputStream.flush();

      return destpath;
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return null;
    } finally {
      if (outputStream != null) {
        // Closes the underlying OutputStream
        outputStream.close();
      }
    }
  }

  /**
   * Utility that can check whether File source is a file or directory.
   *
   * @param sourcefile the original file input
   * @param outputStream the Tar output stream
   * @param basepath the new file's base path
   * @throws Exception if if fails to create the archive
   */
  private void createArchive(File sourcefile, TarArchiveOutputStream outputStream, String basepath)
      throws Exception {
    if (sourcefile.isDirectory()) {
      // Archive Directory
      this.archiveDirectory(sourcefile, outputStream, basepath);
    } else {
      // Archive file
      this.archiveFile(sourcefile, outputStream, basepath);
    }
  }

  /**
   * Extract a tar file.
   *
   * @param filename the name of the tar file.
   */
  public void untarArchive(String filename) throws IOException {
    if (!filename.endsWith(".tar")) {
      log.warn(filename + " is not a .tar file. Nothing to do");
      return;
    }
    /* Read TAR file into TarArchiveInputStream */
    TarArchiveInputStream tarfile = null;
    try {
      tarfile = new TarArchiveInputStream(new FileInputStream(new File(filename)));
      /* To Read individual TAR file */
      TarArchiveEntry entry = null;
      String individualFiles = null;
      int offset = 0;
      FileOutputStream outputFile = null;
      /* Create a loop to read every single entry in TAR file */
      while ((entry = tarfile.getNextTarEntry()) != null) {
        /* Get the name of the file */
        individualFiles = entry.getName();
        /* Get Size of the file and create a byte array for the size */
        offset = 0;
        byte[] content = new byte[(int) entry.getSize()];

        /* Some SOP statements to check progress */
        log.info("File Name in TAR File is: " + entry.getName());
        log.info("Size of the File is: " + entry.getSize());
        log.info("Byte Array length: " + content.length);

        /* Read file from the archive into byte array */
        tarfile.read(content, offset, content.length - offset);
        /* Define OutputStream for writing the file */
        outputFile = new FileOutputStream(new File(individualFiles));
        /* Use IOUtiles to write content of byte array to physical file */
        IOUtils.write(content, outputFile);
        /* Close Output Stream */
        outputFile.close();
      }
      /* Close TarAchiveInputStream */
    } catch (FileNotFoundException e) {
      log.error(e.getMessage());
    } finally {
      if (tarfile != null) {
        tarfile.close();
      }
    }
  }

  /**
   * Create a tar file from a directory.
   *
   * @param directory the input source directory to be tar-ed
   * @param outputStream the Tar output stream
   * @param basepath the new file's base path
   * @throws Exception if if fails to create the archive
   */
  private void archiveDirectory(
      File directory, TarArchiveOutputStream outputStream, String basepath) throws Exception {

    // Returns an array of abstract pathnames denoting the files in the directory
    // denoted by this abstract pathname
    File[] files = directory.listFiles();
    String directoryPath = basepath + File.separator + directory.getName();
    if (files != null) {
      if (files.length < 1) {
        // Construct an entry with only a name. This allows the programmer to
        // construct the entry's header "by hand". File is set to null
        TarArchiveEntry entry = new TarArchiveEntry(directoryPath);

        // put an entry on the output stream
        outputStream.putArchiveEntry(entry);

        // Close an entry. this method MUST be called for all entries that contain data
        outputStream.closeArchiveEntry();
      }
    }

    // Repeat for all files
    for (File file : files) {
      this.archiveFile(file, outputStream, directoryPath);
    }
  }

  /**
   * Private auxiliary method to read the file input as a byte stream.
   *
   * @param file the file input
   * @param outputStream the output stream
   * @param directory the output directory
   */
  private void archiveFile(File file, TarArchiveOutputStream outputStream, String directory) {
    try {
      String filename = directory + File.separator + file.getName();
      TarArchiveEntry entry = new TarArchiveEntry(filename);

      // Set this entry's file size
      entry.setSize(file.length());

      outputStream.putArchiveEntry(entry);

      BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(file));
      int counter = -1;

      // 512: buffer size
      log.info("Writing file content");
      byte[] byteData = new byte[512];
      while ((counter = inputStream.read(byteData, 0, 512)) != -1) {
        outputStream.write(byteData, 0, counter);
      }

      inputStream.close();
      outputStream.closeArchiveEntry();
    } catch (IOException ex) {
      log.error(ex.getMessage());
    }
  }
}
