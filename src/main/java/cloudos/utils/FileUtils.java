package cloudos.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Scanner;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.core.io.ClassPathResource;

/** Created by philipperibeiro on 12/23/16. */
@Log4j2
public final class FileUtils {

  private static final String MOZILLA_5_0 = "Mozilla/5.0";

  private static final String USER_AGENT = "User-Agent";


  private static final int BUFFER_SIZE = 4096;

  private FileUtils() {}


  /**
   * Get the MD5 hash value of the file.
   *
   * @param filepath the path to the file
   * @return the string representation of the hash or null
   */
  public static String getMd5Hash(final String filepath) {
    FileInputStream fis = null;
    try {
      fis = new FileInputStream(new File(filepath));
      // get the md5 hash of the file.
      String md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(fis);
      fis.close();

      return md5;
    } catch (IOException e) {
      log.error(e.getMessage());
    }

    return null;
  }

  /**
   * Download a file in either plain, zip or tar formats, and write the extracted file to the
   * destination specified in path.
   *
   * @param url the file original url
   * @param archive the name of the archive to be downloaded
   * @param filename the name of the file after extraction
   * @param path the destination of the file
   * @throws Exception any exception related with extracting the file
   */
  public static void downloadData(String url, String archive, String filename, String path)
      throws Exception {
    // Create directory is required
    File directory = new File(path);
    if (!directory.exists()) {
      if (!directory.mkdirs()) {
        throw new IOException("Could not create the directory in the path " + path);
      }
    }

    // Download file:
    String archizePath = path + File.separator + archive;
    File archiveFile = new File(archizePath);
    String extractedPath = path + File.separator + filename;
    File extractedFile = new File(extractedPath);

    if (!archiveFile.exists()) {
      log.info("Starting data download (80MB)...");
      org.apache.commons.io.FileUtils.copyURLToFile(new URL(url), archiveFile);

      log.info("Data (.tar.gz file) downloaded to" + archiveFile.getAbsolutePath());
      // extract tar.gz file to output directory
      extractTarGz(archizePath, path);
    } else {
      // Assume if archive (.tar.gz) exists, then data has already been extracted
      log.info("Data (.tar.gz file) already exists at " + archiveFile.getAbsolutePath());
      if (!extractedFile.exists()) {
        // Extract tar.gz file to output directory
        extractTarGz(archizePath, path);
      } else {
        log.info("Data (extracted) already exists at " + extractedFile.getAbsolutePath());
      }
    }
  }

  /**
   * Check if the path exists. The path could be a folder or file.
   *
   * @param path the path to the resource
   * @return whether the resource exists or not.
   */
  public static boolean exits(String path) {
    if (path == null) {
      return false;
    }
    File file = new File(path);
    return file.exists();
  }

  /**
   * Extract a tar file to a given output path location.
   *
   * @param filePath the path to the source tar file
   * @param outputPath the destination of the extracted tar file
   * @throws IOException any exception related with extracting the file
   */
  private static void extractTarGz(String filePath, String outputPath) throws IOException {
    int fileCount = 0;
    int dirCount = 0;
    log.info("Extracting files...");
    try (TarArchiveInputStream tais = new TarArchiveInputStream(
        new GzipCompressorInputStream(new BufferedInputStream(new FileInputStream(filePath))))) {
      TarArchiveEntry entry;

      /** Read the tar entries using the getNextEntry method * */
      while ((entry = (TarArchiveEntry) tais.getNextEntry()) != null) {

        // Create directories as required
        if (entry.isDirectory()) {
          new File(outputPath + entry.getName()).mkdirs();
          dirCount++;
        } else {
          int count;
          byte[] data = new byte[BUFFER_SIZE];

          FileOutputStream fos = new FileOutputStream(outputPath + entry.getName());
          BufferedOutputStream dest = new BufferedOutputStream(fos, BUFFER_SIZE);
          while ((count = tais.read(data, 0, BUFFER_SIZE)) != -1) {
            dest.write(data, 0, count);
          }
          dest.close();
          fileCount++;
        }
        if (fileCount % 1000 == 0) {
          log.info(".");
        }
      }
    }

    log.info(
        "\n" + fileCount + " files and " + dirCount + " directories extracted to: " + outputPath);
  }

  /**
   * Get the key in a private or public key file.
   *
   * @param filename The name of the file
   * @return The key string content
   * @throws IOException Any IOException related issue.
   */
  public static String getKey(String filename) throws IOException {
    // Read key from file
    StringBuilder keyPem = new StringBuilder();
    BufferedReader br = new BufferedReader(new FileReader(filename));
    String line;
    while ((line = br.readLine()) != null) {
      keyPem.append(line + "\n");
    }
    br.close();
    return keyPem.toString();
  }

  /**
   * Utility to delete Files or directories.
   *
   * @param file A file object
   * @throws IOException IOException if it couldn't delete the file for whatever reason
   */
  public static void delete(File file) throws IOException {

    if (file.isDirectory()) {
      // directory is empty, then delete it
      if ((file.list() != null) && (file.list().length == 0)) {
        if (file.delete()) {
          log.info("Directory is deleted : " + file.getAbsolutePath());
        } else {
          throw new IOException("Could not delete directory " + file.getAbsolutePath());
        }
      } else {
        // list all the directory contents
        String[] files = file.list();
        for (String temp : files) {
          // construct the file structure
          File fileDelete = new File(file, temp);
          // recursive delete
          delete(fileDelete);
        }
        // check the directory again, if empty then delete it
        if ((file.list() != null) && (file.list().length == 0)) {
          if (file.delete()) {
            log.info("Directory is deleted : " + file.getAbsolutePath());
          } else {
            throw new IOException("Could not delete directory " + file.getAbsolutePath());
          }
        }
      }
    } else {
      if (file.delete()) {
        log.info("File is deleted : " + file.getAbsolutePath());
      } else {
        throw new IOException("Could not delete file " + file.getAbsolutePath());
      }
    }
  }

  /**
   * Create a new file.
   *
   * @param pathname The full path to the file
   */
  public static void create(String pathname) {
    try {
      File file = new File(pathname);
      if (file.isFile()) {
        if (file.createNewFile()) {
          log.info("File is created!");
        } else {
          log.info("File already exists.");
        }
      } else if (file.isDirectory()) {
        if (file.mkdirs()) {
          log.info("Multiple directories are created!");
        } else {
          log.info("Failed to create multiple directories!");
        }
      }
    } catch (IOException e) {
      log.error(e.getMessage());
    }
  }

  /**
   * Create a temp file with the content of a resource file.
   *
   * @param fileName the name of the file to be fetched
   * @param classLoader the current class loader
   * @return a new file that was read from the resource
   * @throws Exception any exception related with extracting the file
   */
  public static File getFileFromResourcesFile(String fileName, ClassLoader classLoader)
      throws Exception {
    File file = File.createTempFile("file", ".gen");
    Files.copy(new ClassPathResource(fileName).getInputStream(), file.toPath(),
        StandardCopyOption.REPLACE_EXISTING);
    return file;
  }

  /**
   * Returns whether a directory is empty or not.
   *
   * @param directory the path to the directory
   * @return true if the directory is empty
   */
  public static boolean isDirectoryEmpty(Path directory) throws IOException {
    if (directory == null) {
      throw new IllegalArgumentException("directory must not be null");
    }
    try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(directory)) {
      return !directoryStream.iterator().hasNext();
    }
  }
  
  /**
   * Read an url content to a Reader.
   * 
   * @param url to be read
   * @return InputStreamReader
   * @throws Exception if something goes wrong.
   */
  public static InputStreamReader readUrlContentToReader(String url) throws Exception {
    HttpClient client = HttpClientBuilder.create().build();
    HttpGet request = new HttpGet(url);
    // add request header
    request.addHeader(USER_AGENT, MOZILLA_5_0);
    HttpResponse response = client.execute(request);
    log.info("Sending 'GET' request to URL {} - Response Code: {}", url,
        response.getStatusLine().getStatusCode());
    InputStream content = response.getEntity().getContent();
    return  new InputStreamReader(content);
  }

  /**
   * Read an url content.
   *
   * @param url - url
   * @return String contents
   * @throws Exception if not possible to read the url
   */
  public static String readUrlContent(String url) throws Exception {
    HttpClient client = HttpClientBuilder.create().build();
    HttpGet request = new HttpGet(url);
    // add request header
    request.addHeader(USER_AGENT, MOZILLA_5_0);
    HttpResponse response = client.execute(request);
    log.info("Sending 'GET' request to URL : " + url);
    log.info("Response Code : " + response.getStatusLine().getStatusCode());
    StringBuffer result = new StringBuffer();
    Scanner sc = null;
    InputStream inputStream = null;
    try {
      inputStream = response.getEntity().getContent();
      sc = new Scanner(inputStream, "UTF-8");
      while (sc.hasNextLine()) {
        result.append(sc.nextLine());
      }
      // note that Scanner suppresses exceptions
      if (sc.ioException() != null) {
        throw sc.ioException();
      }
    } finally {
      if (inputStream != null) {
        inputStream.close();
      }
      if (sc != null) {
        sc.close();
      }
    }
    return result.toString();
  }

  /**
   * Read a url content until certain tag appear on the content.
   *
   * @param url - url
   * @return String line containing the tag
   * @throws Exception if not possible to read the url
   */
  public static String readPartialUrlContent(String url, String tag) throws Exception {
    HttpClient client = HttpClientBuilder.create().build();
    HttpGet request = new HttpGet(url);
    // add request header
    request.addHeader(USER_AGENT, MOZILLA_5_0);
    HttpResponse response = client.execute(request);
    log.info("Sending 'GET' request to URL : " + url);
    log.info("Response Code : " + response.getStatusLine().getStatusCode());
    Scanner sc = null;
    InputStream inputStream = null;
    try {
      inputStream = response.getEntity().getContent();
      sc = new Scanner(inputStream, "UTF-8");
      String line = "";
      while (sc.hasNextLine()) {
        line = sc.nextLine();
        if (line.contains(tag)) {
          return line;
        }
      }
      // note that Scanner suppresses exceptions
      if (sc.ioException() != null) {
        throw sc.ioException();
      }
    } finally {
      if (inputStream != null) {
        inputStream.close();
      }
      if (sc != null) {
        sc.close();
      }
    }
    return "";
  }
}
