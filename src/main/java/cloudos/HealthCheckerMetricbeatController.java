package cloudos;

import cloudos.elasticsearch.model.ChartParameters;
import cloudos.elasticsearch.model.DiskIODataPoint;
import cloudos.elasticsearch.model.KibanaSearchParametersTransformer;
import cloudos.elasticsearch.model.NetworkIODataPoint;
import cloudos.elasticsearch.model.ProcessDataPoint;
import cloudos.elasticsearch.model.ValueDataPoint;
import cloudos.elasticsearch.model.charts.CpuUsagePctChart;
import cloudos.elasticsearch.model.charts.MemoryUsageBytesChart;
import cloudos.elasticsearch.model.charts.NetworkTrafficBytesChart;
import cloudos.elasticsearch.model.charts.NetworkTrafficPacketsChart;
import cloudos.elasticsearch.model.charts.SystemLoadChart;
import cloudos.elasticsearch.services.MetricbeatService;
import cloudos.security.WebSecurityConfig;

import java.net.UnknownHostException;
import java.util.List;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Implement the HealthChecker methods from MetricbeatService.
 *
 * @author Rogerio Souza
 */
@Controller
@RestController
@RequestMapping(value = HealthCheckerMetricbeatController.HEALTHCHECKER)
@Log4j2
public class HealthCheckerMetricbeatController {

  public static final String HEALTHCHECKER =
      WebSecurityConfig.API_PATH + "/healthchecker/metricbeat";
  public static final Integer TOP_10 = 10;

  @Autowired
  private MetricbeatService metricbeatService;

  @Autowired
  private KibanaSearchParametersTransformer transformer;

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by METRICSET_RTT
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/metricset_rtt_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getMetricsetRttAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getMetricsetRttAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by SYSTEM_CPU_IDLE_PCT
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_cpu_idle_pct_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemCpuIdlePctAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemCpuIdlePctAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by SYSTEM_CPU_IOWAIT_PCT
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_cpu_iowait_pct_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemCpuIowaitPctAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemCpuIowaitPctAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by SYSTEM_CPU_IRQ_PCT
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_cpu_irq_pct_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemCpuIrqPctAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemCpuIrqPctAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by SYSTEM_CPU_NICE_PCT
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_cpu_nice_pct_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemCpuNicePctAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemCpuNicePctAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by
   * SYSTEM_CPU_SOFTIRQ_PCT metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_cpu_soft_irq_pct_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemCpuSoftirqPctAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemCpuSoftirqPctAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by SYSTEM_CPU_STEAL_PCT
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_cpu_steal_pct_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemCpuStealPctAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemCpuStealPctAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by SYSTEM_CPU_SYSTEM_PCT
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_cpu_system_pct_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemCpuSystemPctAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemCpuSystemPctAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by SYSTEM_CPU_USER_PCT
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_cpu_user_pct_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemCpuUserPctAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemCpuUserPctAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by
   * SYSTEM_FILESYSTEM_AVAILABLE metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_filesystem_available_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemFilesystemAvailableAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemFilesystemAvailableAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by
   * SYSTEM_FILESYSTEM_FILES metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_filesystem_files_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemFilesystemFilesAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemFilesystemFilesAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by
   * SYSTEM_FILESYSTEM_FREE metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_filesystem_free_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemFilesystemFreeAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemFilesystemFreeAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by
   * SYSTEM_FILESYSTEM_FREE_FILES metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_filesystem_free_files_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemFilesystemFreeFilesAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemFilesystemFreeFilesAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by
   * SYSTEM_FILESYSTEM_TOTAL metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_filesystem_total_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemFilesystemTotalAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemFilesystemTotalAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by
   * SYSTEM_FILESYSTEM_USED_BYTES metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_filesystem_used_bytes_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemFilesystemUsedBytesAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemFilesystemUsedBytesAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by
   * SYSTEM_FILESYSTEM_USED_PCT metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_filesystem_used_pct_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemFilesystemUsedPctAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemFilesystemUsedPctAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by SYSTEM_LOAD_1
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_load1_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemLoad1Avg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemLoad1Avg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by SYSTEM_LOAD_15
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_load15_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemLoad15Avg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemLoad15Avg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by SYSTEM_LOAD_5
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_load5_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemLoad5Avg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemLoad5Avg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by SYSTEM_LOAD_NORM_1
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_load_norm1_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemLoadNorm1Avg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemLoadNorm1Avg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by SYSTEM_LOAD_NORM_15
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_load_norm15_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemLoadNorm15Avg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemLoadNorm15Avg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by SYSTEM_LOAD_NORM_5
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_load_norm5_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemLoadNorm5Avg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemLoadNorm5Avg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by
   * SYSTEM_MEMORY_ACTUAL_FREE metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_memory_actual_free_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemMemoryActualFreeAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemMemoryActualFreeAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by
   * SYSTEM_MEMORY_ACTUAL_USED_BYTES metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_memory_actual_used_bytes_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemMemoryActualUsedBytesAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemMemoryActualUsedBytesAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by
   * SYSTEM_MEMORY_ACTUAL_USED_PCT metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_memory_actual_used_pct_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemMemoryActualUsedPctAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemMemoryActualUsedPctAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by SYSTEM_MEMORY_FREE
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_memory_free_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemMemoryFreeAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemMemoryFreeAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by
   * SYSTEM_MEMORY_SWAP_FREE metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_memory_swap_free_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemMemorySwapFreeAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemMemorySwapFreeAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by
   * SYSTEM_MEMORY_SWAP_TOTAL metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_memory_swap_total_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemMemorySwapTotalAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemMemorySwapTotalAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by
   * SYSTEM_MEMORY_SWAP_USED_BYTES metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_memory_swap_used_bytes_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemMemorySwapUsedBytesAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemMemorySwapUsedBytesAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by
   * SYSTEM_MEMORY_SWAP_USED_PCT metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_memory_swap_used_pct_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemMemorySwapUsedPctAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemMemorySwapUsedPctAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by SYSTEM_MEMORY_TOTAL
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_memory_total_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemMemoryTotalAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemMemoryTotalAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by
   * SYSTEM_MEMORY_USED_BYTES metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_memory_used_bytes_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemMemoryUsedBytesAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemMemoryUsedBytesAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by
   * SYSTEM_MEMORY_USED_PCT metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_memory_used_pct_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemMemoryUsedPctAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemMemoryUsedPctAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by
   * SYSTEM_NETWORK_IN_BYTES metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_network_in_bytes_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemNetworkInBytesAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemNetworkInBytesAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by
   * SYSTEM_NETWORK_IN_DROPPED metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_network_in_dropped_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemNetworkInDroppedAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemNetworkInDroppedAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by
   * SYSTEM_NETWORK_IN_ERRORS metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_network_in_errors_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemNetworkInErrorsAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemNetworkInErrorsAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by
   * SYSTEM_NETWORK_IN_PACKETS metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_network_in_packets_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemNetworkInPacketsAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemNetworkInPacketsAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by
   * SYSTEM_NETWORK_OUT_BYTES metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_network_out_bytes_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemNetworkOutBytesAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemNetworkOutBytesAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by
   * SYSTEM_NETWORK_OUT_DROPPED metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_network_out_dropped_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemNetworkOutDroppedAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemNetworkOutDroppedAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by
   * SYSTEM_NETWORK_OUT_ERRORS metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_network_out_errors_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemNetworkOutErrorsAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemNetworkOutErrorsAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by
   * SYSTEM_NETWORK_OUT_PACKETS metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_network_out_packets_avg", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemNetworkOutPacketsAvg(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemNetworkOutPacketsAvg(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and METRICSET_RTT metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/metricset_rtt", method = RequestMethod.POST)
  @ResponseBody
  public Long getMetricsetRtt(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getMetricsetRtt(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_CPU_IDLE_PCT metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_cpu_idle_pct", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemCpuIdlePct(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemCpuIdlePct(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_CPU_IDLE_PCT metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_cpu_total_usage_pct", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemCpuTotalUsagePct(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemCpuTotalUsagePct(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_CPU_IOWAIT_PCT metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_cpu_iowait_pct", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemCpuIowaitPct(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemCpuIowaitPct(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_CPU_IRQ_PCT metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_cpu_irq_pct", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemCpuIrqPct(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemCpuIrqPct(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_CPU_NICE_PCT metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_cpu_nice_pct", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemCpuNicePct(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemCpuNicePct(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_CPU_SOFTIRQ_PCT metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_cpu_soft_irq_pct", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemCpuSoftirqPct(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemCpuSoftirqPct(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_CPU_STEAL_PCT metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_cpu_steal_pct", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemCpuStealPct(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemCpuStealPct(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_CPU_SYSTEM_PCT metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_cpu_system_pct", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemCpuSystemPct(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemCpuSystemPct(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_CPU_USER_PCT metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_cpu_user_pct", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemCpuUserPct(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemCpuUserPct(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_FILESYSTEM_AVAILABLE
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_filesystem_available", method = RequestMethod.POST)
  @ResponseBody
  public Long getSystemFilesystemAvailable(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemFilesystemAvailable(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_FILESYSTEM_FILES metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_filesystem_files", method = RequestMethod.POST)
  @ResponseBody
  public Long getSystemFilesystemFiles(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemFilesystemFiles(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_FILESYSTEM_FREE metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_filesystem_free", method = RequestMethod.POST)
  @ResponseBody
  public Long getSystemFilesystemFree(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemFilesystemFree(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_FILESYSTEM_FREE_FILES
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_filesystem_free_files", method = RequestMethod.POST)
  @ResponseBody
  public Long getSystemFilesystemFreeFiles(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemFilesystemFreeFiles(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_FILESYSTEM_TOTAL metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_filesystem_total", method = RequestMethod.POST)
  @ResponseBody
  public Long getSystemFilesystemTotal(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemFilesystemTotal(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_FILESYSTEM_USED_BYTES
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_filesystem_used_bytes", method = RequestMethod.POST)
  @ResponseBody
  public Long getSystemFilesystemUsedBytes(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemFilesystemUsedBytes(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_FILESYSTEM_USED_PCT
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_filesystem_used_pct", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemFilesystemUsedPct(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemFilesystemUsedPct(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_LOAD_1 metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_load1", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemLoad1(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemLoad1(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_LOAD_15 metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_load15", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemLoad15(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemLoad15(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_LOAD_5 metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_load5", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemLoad5(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemLoad5(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_LOAD_NORM_1 metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_load_norm1", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemLoadNorm1(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemLoadNorm1(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_LOAD_NORM_15 metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_load_norm15", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemLoadNorm15(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemLoadNorm15(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_LOAD_NORM_5 metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_load_norm5", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemLoadNorm5(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemLoadNorm5(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_MEMORY_ACTUAL_FREE
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_memory_actual_free", method = RequestMethod.POST)
  @ResponseBody
  public Long getSystemMemoryActualFree(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemMemoryActualFree(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_MEMORY_ACTUAL_USED_BYTES
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_memory_actual_used_bytes", method = RequestMethod.POST)
  @ResponseBody
  public Long getSystemMemoryActualUsedBytes(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemMemoryActualUsedBytes(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_MEMORY_ACTUAL_USED_PCT
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_memory_actual_used_pct", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemMemoryActualUsedPct(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemMemoryActualUsedPct(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_MEMORY_FREE metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_memory_free", method = RequestMethod.POST)
  @ResponseBody
  public Long getSystemMemoryFree(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemMemoryFree(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_MEMORY_SWAP_FREE metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_memory_swap_free", method = RequestMethod.POST)
  @ResponseBody
  public Long getSystemMemorySwapFree(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemMemorySwapFree(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_MEMORY_SWAP_TOTAL
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_memory_swap_total", method = RequestMethod.POST)
  @ResponseBody
  public Long getSystemMemorySwapTotal(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemMemorySwapTotal(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_MEMORY_SWAP_USED_BYTES
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_memory_swap_used_bytes", method = RequestMethod.POST)
  @ResponseBody
  public Long getSystemMemorySwapUsedBytes(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemMemorySwapUsedBytes(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_MEMORY_SWAP_USED_PCT
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_memory_swap_used_pct", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemMemorySwapUsedPct(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemMemorySwapUsedPct(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_MEMORY_TOTAL metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_memory_total", method = RequestMethod.POST)
  @ResponseBody
  public Long getSystemMemoryTotal(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemMemoryTotal(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_MEMORY_USED_BYTES
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_memory_used_bytes", method = RequestMethod.POST)
  @ResponseBody
  public Long getSystemMemoryUsedBytes(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemMemoryUsedBytes(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_MEMORY_USED_PCT metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_memory_used_pct", method = RequestMethod.POST)
  @ResponseBody
  public Double getSystemMemoryUsedPct(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemMemoryUsedPct(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_NETWORK_IN_BYTES metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_network_in_bytes", method = RequestMethod.POST)
  @ResponseBody
  public Long getSystemNetworkInBytes(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemNetworkInBytes(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_NETWORK_IN_DROPPED
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_network_in_dropped", method = RequestMethod.POST)
  @ResponseBody
  public Long getSystemNetworkInDropped(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemNetworkInDropped(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_NETWORK_IN_ERRORS
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_network_in_errors", method = RequestMethod.POST)
  @ResponseBody
  public Long getSystemNetworkInErrors(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemNetworkInErrors(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_NETWORK_IN_PACKETS
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_network_in_packets", method = RequestMethod.POST)
  @ResponseBody
  public Long getSystemNetworkInPackets(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemNetworkInPackets(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_NETWORK_OUT_BYTES
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_network_out_bytes", method = RequestMethod.POST)
  @ResponseBody
  public Long getSystemNetworkOutBytes(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemNetworkOutBytes(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_NETWORK_OUT_DROPPED
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_network_out_dropped", method = RequestMethod.POST)
  @ResponseBody
  public Long getSystemNetworkOutDropped(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemNetworkOutDropped(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_NETWORK_OUT_ERRORS
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_network_out_errors", method = RequestMethod.POST)
  @ResponseBody
  public Long getSystemNetworkOutErrors(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemNetworkOutErrors(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_NETWORK_OUT_PACKETS
   * metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_network_out_packets", method = RequestMethod.POST)
  @ResponseBody
  public Long getSystemNetworkOutPackets(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemNetworkOutPackets(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hit field value filtered by HOSTNAME and SYSTEM_CPU_CORES metricbeats
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_cpu_cores", method = RequestMethod.POST)
  @ResponseBody
  public Long getSystemCpuCores(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemCpuCores(transformer.apply(chartParameters));
  }

  /**
   * Retrieves the last hour top 10 processes MetricbeatFields.SYSTEM_PROCESS_CPU_TOTAL_PCT
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/top_10_process_cpu_pct", method = RequestMethod.POST)
  @ResponseBody
  public List<ValueDataPoint> getTop10SystemProcessCpuTotalPct(
      @RequestBody ChartParameters chartParameters) throws UnknownHostException {
    return metricbeatService.getTopSystemProcessCpuTotalPct(transformer.apply(chartParameters),
        TOP_10);
  }

  /**
   * Retrieves the last hour top 10 processes memory usage percentage based on
   * MetricbeatFields.SYSTEM_PROCESS_MEMORY_SIZE / MetricbeatFields.SYSTEM_MEMORY_TOTAL
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/top_10_system_process_memory_rss_pct", method = RequestMethod.POST)
  @ResponseBody
  public List<ValueDataPoint> getTop10SystemProcessMemoryRssPct(
      @RequestBody ChartParameters chartParameters) throws UnknownHostException {
    return metricbeatService.getTopSystemProcessMemoryRssPct(transformer.apply(chartParameters),
        TOP_10);
  }

  /**
   * Retrieves most recent snapshot top 10 processes data (name, user, cpu %, memory % i/o %)
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/top_10_processes", method = RequestMethod.POST)
  @ResponseBody
  public List<ProcessDataPoint> getTop10SystemProcesses(
      @RequestBody ChartParameters chartParameters) throws UnknownHostException {
    return metricbeatService.getTopSystemProcesses(transformer.apply(chartParameters), TOP_10);
  }

  /**
   * Build the Memory Usage Bytes Chart
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/memory_usage_bytes_chart", method = RequestMethod.POST)
  @ResponseBody
  public MemoryUsageBytesChart getMemoryUsageBytesChartData(
      @RequestBody ChartParameters chartParameters) throws UnknownHostException {
    return metricbeatService.getMemoryUsageBytesChartData(transformer.apply(chartParameters));
  }

  /**
   * Build the Memory Usage Bytes Chart
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/cpu_usage_pct_chart", method = RequestMethod.POST)
  @ResponseBody
  public CpuUsagePctChart getCpuUsagePctChartData(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getCpuUsagePctChartData(transformer.apply(chartParameters));
  }

  /**
   * Build Network Traffic Packets Chart
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/network_traffic_packets_chart", method = RequestMethod.POST)
  @ResponseBody
  public NetworkTrafficPacketsChart getNetworkTrafficPacketsChart(
      @RequestBody ChartParameters chartParameters) throws UnknownHostException {
    return metricbeatService.getNetworkTrafficPacketsChart(transformer.apply(chartParameters));
  }

  /**
   * Builds the Memory Usage Bytes Chart
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/network_traffic_bytes_chart", method = RequestMethod.POST)
  @ResponseBody
  public NetworkTrafficBytesChart getNetworkTrafficBytesChart(
      @RequestBody ChartParameters chartParameters) throws UnknownHostException {
    return metricbeatService.getNetworkTrafficBytesChart(transformer.apply(chartParameters));
  }

  /**
   * Retrieves most recent snapshot top 10 hostnames disk io data (R/W bytes R/W millisedons)
   *
   * @param KibanaSearchParameters
   * @return
   */
  @RequestMapping(value = "/top_10_hostnames_disk_io", method = RequestMethod.POST)
  @ResponseBody
  public List<DiskIODataPoint> getTopHostnamesDiskIO(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getTopHostnamesDiskIO(transformer.apply(chartParameters), TOP_10);
  }

  /**
   * Retrieves most recent snapshot top 10 hostnames network io data(bytes IN/OUT packets IN/OUT)
   *
   * @param KibanaSearchParameters
   * @return
   */
  @RequestMapping(value = "/top_10_hostnames_network", method = RequestMethod.POST)
  @ResponseBody
  public List<NetworkIODataPoint> getTopHostnamesNetwork(
      @RequestBody ChartParameters chartParameters) throws UnknownHostException {
    return metricbeatService.getTopHostnamesNetwork(transformer.apply(chartParameters), TOP_10);
  }

  /**
   * Builds the System Load Chart
   *
   * @param ChartParameters
   * @return
   */
  @RequestMapping(value = "/system_load_chart", method = RequestMethod.POST)
  @ResponseBody
  public SystemLoadChart getSystemLoadChart(@RequestBody ChartParameters chartParameters)
      throws UnknownHostException {
    return metricbeatService.getSystemLoadChart(transformer.apply(chartParameters));
  }
}
