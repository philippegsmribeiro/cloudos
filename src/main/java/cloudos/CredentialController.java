package cloudos;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cloudos.credentials.CloudCredentialActionException;
import cloudos.credentials.CloudCredentialActionRequest;
import cloudos.credentials.CloudCredentialActionResponse;
import cloudos.encryptionKeys.AmazonKMSServiceException;
import cloudos.exceptions.NotImplementedException;
import cloudos.models.CloudosCredential;
import cloudos.security.WebSecurityConfig;

/**
 * Credential Controller.
 *
 * @author flavio
 *
 */
@RestController
@RequestMapping(value = CredentialController.CREDENTIAL)
public class CredentialController {

  public static final String CREDENTIAL = WebSecurityConfig.API_PATH + "/credential";

  @Autowired
  private CredentialManagerService credentialManagerService;

  /**
   * Save a cloud credential.
   *
   * @param request cloudcredentialActionRequest
   * @return CloudCredentialActionResponse
   */
  @PostMapping(value = "/save")
  @ResponseBody
  public ResponseEntity<?> createCloudCredential(
      @RequestBody CloudCredentialActionRequest request) {
    try {
      CloudosCredential cloudosCredential = this.credentialManagerService.save(request);
      if (cloudosCredential != null) {
        return ResponseEntity.ok(
            new CloudCredentialActionResponse(HttpStatus.OK, "Credential saved!",
                cloudosCredential));
      }
      return ResponseEntity.badRequest().body("Could not save credential");
    } catch (NotImplementedException e) {
      return new ResponseEntity<>(e, HttpStatus.NOT_IMPLEMENTED);
    } catch (CloudCredentialActionException e) {
      return ResponseEntity.badRequest().body(
          new CloudCredentialActionResponse(HttpStatus.BAD_REQUEST, e.getLocalizedMessage(),
              null));
    }
  }

  /**
   * Delete a cloud credential.
   *
   * @param request cloudcredentialActionRequest
   * @return CloudCredentialActionResponse
   */
  @PostMapping(value = "/delete")
  @ResponseBody
  public ResponseEntity<?> deleteCloudCredential(
      @RequestBody CloudCredentialActionRequest request) {
    try {
      this.credentialManagerService.delete(request);
      return ResponseEntity
          .ok(new CloudCredentialActionResponse(HttpStatus.OK, "Credential deleted!", null));
    } catch (NotImplementedException e) {
      return new ResponseEntity<>(e, HttpStatus.NOT_IMPLEMENTED);
    } catch (CloudCredentialActionException | AmazonKMSServiceException e) {
      return ResponseEntity.badRequest().body(
          new CloudCredentialActionResponse(HttpStatus.BAD_REQUEST, e.getLocalizedMessage(), null));
    }
  }

  /**
   * List the cloud credentials.
   *
   * @param providers Provider of the credentials
   * @return list of cloud credentials
   */
  @PostMapping(value = "/list")
  @ResponseBody
  public ResponseEntity<?> listCloudCredential(@RequestBody(required = false) Providers providers) {
    try {
      List<CloudosCredential> cloudosCredentials = this.credentialManagerService.list(providers);
      if (cloudosCredentials != null) {
        return ResponseEntity.ok(cloudosCredentials);
      }
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } catch (NotImplementedException e) {
      return new ResponseEntity<>(e, HttpStatus.NOT_IMPLEMENTED);
    }
  }
}
