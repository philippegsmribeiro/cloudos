package cloudos.data;

import cloudos.models.Requests;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import java.io.InputStream;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.http.entity.ContentType;

/** Created by philipperibeiro on 2/18/17. */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class LogRecordRequest extends Requests {

  // @TODO: Maybe I should make it more stateless?
  private String host;
  private String instance;
  private Integer port;
  private String filename;
  private Integer offset;

  private static String LOGS_URL = "logs";

  /* Define the buffer size */
  private static int BUFFER_SIZE = 4096;

  public HttpResponse<InputStream> getStream() throws UnirestException {
    String url = this.getLogsUrl();
    String json = this.toString();
    return this.getStream(url, json);
  }

  public HttpResponse<InputStream> getStream(String url, String json) throws UnirestException {

    return Unirest.post(url)
        .header("accept", String.valueOf(ContentType.APPLICATION_OCTET_STREAM))
        .header("content-type", String.valueOf(ContentType.APPLICATION_JSON))
        .body(json)
        .asBinary();
  }

  public String getLogsUrl() {
    return String.format("%s:%d/%s", this.host, this.port, LOGS_URL);
  }

}
