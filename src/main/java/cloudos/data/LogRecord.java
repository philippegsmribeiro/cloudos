package cloudos.data;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/** Created by philipperibeiro on 2/18/17. I will define the LogRecord as as a maximum 1MB object */
@Document(collection = "healthchecker_logrecord")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class LogRecord {

  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  private String id;

  private String instance;
  private String host;
  private String time;
  private String path;
  private String referer;
  private Integer offset;

}
