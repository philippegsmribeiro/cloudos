package cloudos.data;

public enum InstanceDataUnit {
  PERCENTUAL,
  BYTES
}
