package cloudos.data;

import static com.mongodb.client.model.Sorts.ascending;
import static com.mongodb.client.model.Sorts.orderBy;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.UpdateResult;

import lombok.extern.log4j.Log4j2;

import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.receiver.Receiver;
import org.bson.Document;
import org.spark_project.guava.util.concurrent.RateLimiter;

/**
 * A custom receiver to retrieve data from mongodb and send to spark aggregator. Created by gleimar
 * on 21/01/2017.
 */
@Log4j2
public class MongoCustomReceiver extends Receiver<Document> {

  //1 call heach 30 seconds
  private static final double CALL_PER_SECONDS = 1 / 30d;

  private String host;
  private Integer port;
  private String db;
  private String username;
  private String password;
  private String inputCollectionName;

  /**
   * @param host - The host of the mongodb
   * @param port - The port of the mongodb
   * @param db - The database name of the mongodb
   * @param username - The username of the mongodb
   * @param password - The password of the mongodb
   * @param inputCollectionName - The input collection name to retrieve the data
   */
  public MongoCustomReceiver(
      String host,
      Integer port,
      String db,
      String username,
      String password,
      String inputCollectionName) {
    super(StorageLevel.MEMORY_AND_DISK_2());
    this.host = host;
    this.port = port;
    this.db = db;
    this.password = password;
    this.username = username;
    this.inputCollectionName = inputCollectionName;
  }

  @Override
  public void onStart() {
    // Start the thread that receives data over a connection
    new Thread(() -> receive()).start();
  }

  private void receive() {
    RateLimiter limiter = RateLimiter.create(CALL_PER_SECONDS);

    MongoCollection mongoCollection = readCollection();

    try {

      // Until stopped or connection broken continue reading
      while (!isStopped()) {

        FindIterable<Document> findIterable =
            mongoCollection
                .find()
                .filter(Filters.ne("processed", 1))
                .sort(orderBy(ascending("now")));

        for (Document document : findIterable) {

          UpdateResult updateResult =
              mongoCollection.updateOne(
                  Filters.eq("_id", document.getObjectId("_id")),
                  new Document("$set", new Document("processed", 1)));

          log.debug(String.format("qtd register updated:%s", updateResult.getMatchedCount()));

          store(document);
        }

        limiter.acquire();
      }

      // Restart in an attempt to connect again when server is active again
      restart("Trying to connect again");

    } catch (Throwable t) {
      // restart if there is any other error
      restart("Error receiving data", t);
    }
  }

  /**
   * Calculates a offset of the input time.
   *
   * @param seconds
   * @param timeValue
   * @return
   */
  public long getNextOffsetTime(long seconds, long timeValue) {
    return timeValue + (seconds * 1000);
  }

  @Override
  public void onStop() {
    log.info("MongoCustomReceiver has stopped");
  }

  /**
   * Format a Mongo URI to be used when connecting to the Mongo Server
   *
   * @return String: A formated Mongo URI.
   */
  private String formatMongoURI() {
    return String.format(
        "mongodb://%s:%s@%s:%s/%s", this.username, this.password, this.host, this.port, this.db);
  }

  /**
   * Loads the mongoCollection of the inputCollectionname.
   *
   * @return the mongoCollection collection
   */
  private MongoCollection readCollection() {
    MongoClient mongoClient = new MongoClient(new MongoClientURI(this.formatMongoURI()));

    MongoDatabase db = mongoClient.getDatabase(this.db);

    return db.getCollection(this.inputCollectionName);
  }
}
