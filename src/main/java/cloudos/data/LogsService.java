package cloudos.data;

import java.util.List;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Created by philipperibeiro on 2/18/17. */
@Service
@Log4j2
public class LogsService {

  @Autowired
  private LogRecordRepository logRecordRepository;

  /** */
  public LogsService() {}

  /** @param logRecordRepository */
  public LogsService(LogRecordRepository logRecordRepository) {
    this.logRecordRepository = logRecordRepository;
  }

  /**
   * @param instance
   * @return
   */
  public List<LogRecord> fetchLogRecords(String instance) {
    return this.logRecordRepository.findByInstance(instance);
  }
}
