package cloudos.data;

public enum InstanceDataType {
  AVERAGE("Average");

  private String description;

  InstanceDataType(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
