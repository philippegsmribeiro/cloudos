package cloudos.data;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;

/** Created by philipperibeiro on 2/18/17. */
public interface LogRecordRepository extends MongoRepository<LogRecord, String> {

  List<LogRecord> findByInstance(@Param("instance") String instance);

  List<LogRecord> findByInstanceAndPath(
      @Param("instance") String instance, @Param("path") String path);
}
