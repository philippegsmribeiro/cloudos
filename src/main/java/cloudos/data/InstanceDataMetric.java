package cloudos.data;

public enum InstanceDataMetric {
  CPU_UTILIZATION,
  MEMORY_USAGE,
  DISK_USAGE,
  DISK_READ_BYTES,
  DISK_WRITE_BYTES,
  NETWORK_RECEIVED_BYTES,
  NETWORK_SENT_BYTES
}
