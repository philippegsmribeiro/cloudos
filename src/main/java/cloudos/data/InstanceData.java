package cloudos.data;

import com.google.gson.Gson;

import cloudos.Providers;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by philipperibeiro on 2/17/17.
 *
 * <p>Represent the information to be fetched from a Instance
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class InstanceData {

  /** Provider */
  private Providers provider;
  /** Metric */
  private InstanceDataMetric metric;
  /** Type */
  private InstanceDataType type;
  /** Time window in seconds */
  private Long timeWindowInSeconds;
  /** Time frame in seconds */
  private Integer timeFrameInSeconds;

  @Override
  public String toString() {
    Gson gson = new Gson();
    return gson.toJson(this);
  }

}
