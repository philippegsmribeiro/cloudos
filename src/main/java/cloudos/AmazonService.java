package cloudos;

import cloudos.amazon.AWSCredentialService;
import cloudos.amazon.AmazonEC2OnDemand;
import cloudos.amazon.Key;
import cloudos.models.AbstractInstance;
import cloudos.models.AwsCredential;
import cloudos.models.AwsKeyPair;
import cloudos.models.AwsKeyPairRepository;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.DeleteSecurityGroupRequest;
import com.amazonaws.services.ec2.model.DeleteSecurityGroupResult;
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsRequest;
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.InstanceStatus;
import com.amazonaws.services.ec2.model.SecurityGroup;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import javax.xml.bind.ValidationException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by philipperibeiro on 12/16/16. AmazonServices handles the logic behind all the AWS
 * Related operations, such as manipulating instances through the appropriate action.
 */
@Service
@Log4j2
public class AmazonService {

  // Add the keys repository
  @Autowired
  private AwsKeyPairRepository awsKeyPairRepository;
  @Autowired
  private AWSCredentialService awsCredentialService;

  private Key key;

  private static ConcurrentMap<String, AmazonEC2> map;
  private static ConcurrentHashMap<String, AmazonEC2OnDemand> onDemandMap;

  /** Default constructor. */
  public AmazonService() {
    this.key = new Key();
    map = new ConcurrentHashMap<>();
    onDemandMap = new ConcurrentHashMap<>();
  }

  /**
   * Constructor.
   *
   * @param awsKeyPairRepository A repository of KeyPair pem keys.
   */
  public AmazonService(AwsKeyPairRepository awsKeyPairRepository) {
    this();
    this.awsKeyPairRepository = awsKeyPairRepository;
  }

  /**
   * Return an AmazonEC2 client whenever needed. If it does not exist, it will then create one.
   * There must be only one client per region.
   *
   * @param region The region the client is being created
   * @return AmazonEC2: An AmazonEC2 client
   */
  public synchronized AmazonEC2 getClient(final String region) {
    // There should be only one AmazonEC2 client per region
    try {
      // There should be only one AmazonEC2 client per region
      if (map.containsKey(region)) {
        return map.get(region);
      } else {
        // Create a new AmazonEC2 client by region
        AmazonEC2 ec2Client = AmazonEC2ClientBuilder.standard().withRegion(region)
            .withCredentials(awsCredentialService.getAWSCredentials()).build();
        log.debug("AmazonEC2: {} for region: {}", ec2Client, region);
        map.put(region, ec2Client);
        return ec2Client;
      }
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return null;
    }
  }

  /**
   * Create the given number of instances, between minCount and maxCount.
   *
   * @param ami The AWS AMI being used
   * @param instanceType The type of the instance being used
   * @param minCount The min number of instances to be created
   * @param maxCount The max number of instances to be created
   * @param keyPairName The current keypair being used
   * @param securityGroup The groupname being used
   * @param cloudGroup The cloudGroupId that the instance will belong
   * @return List of Instance: list of created instances
   * @throws Exception if something goes wrong
   */
  public synchronized List<Instance> createInstances(String ami, String instanceType,
      Integer minCount, Integer maxCount, String keyPairName, String securityGroup, String region,
      String zone, String name, Map<String, String> tags, String cloudGroup) throws Exception {
    List<Instance> instances = new ArrayList<>();
    // invalid number of instances
    if (minCount <= 0 || maxCount <= 0 || (minCount > maxCount)) {
      return instances;
    }

    // import the passed key if doesnt exist
    // this.createKeyPair(region, keyPairName);

    log.debug(String.format(
        "#### AMI: '%s', Instance Type: '%s', Min Count: '%s', Max Count: '%s', "
            + "Key Pair Name: '%s', Security Group: '%s', Region: '%s'",
        ami, instanceType, minCount, maxCount, keyPairName, securityGroup, region));
    AmazonEC2OnDemand onDemand = this.findAmazonEC2OnDemand(region, securityGroup);
    if (onDemand != null) {
      log.debug(String.format("### Using OnDemand Object: '%s'", onDemand));
      instances = onDemand.createInstance(ami, instanceType, minCount, maxCount, keyPairName,
          securityGroup, zone, name, tags, cloudGroup);
    }
    return instances;
  }

  /**
   * Return all the instances in a particular region with a particular security group.
   *
   * @param region The name of the region being searched
   * @param securityGroup The name of the security group
   * @return List: A list of instances
   */
  public synchronized Set<Instance> listInstances(String region, String securityGroup) {
    Set<Instance> instances = new HashSet<>();
    try {
      AmazonEC2OnDemand onDemand = this.findAmazonEC2OnDemand(region, securityGroup);
      instances = onDemand.getInstances();
    } catch (AmazonServiceException ase) {
      log.error(ase);
    }
    return instances;
  }

  /**
   * Terminate all the instances, given the region and the security group
   *
   * @param instances A list of instance IDs
   * @param securityGroup The instance's security group.
   * @return boolean: true if successful, false otherwise.
   */
  public synchronized boolean terminateInstances(List<String> instances, String region,
      String securityGroup) {
    try {
      // @TODO: Remove all the terminated instances from Mongo
      AmazonEC2OnDemand onDemand = this.findAmazonEC2OnDemand(region, securityGroup);
      if (onDemand != null) {
        onDemand.terminate(instances);
        return true;
      }
      return false;
    } catch (AmazonServiceException ase) {
      return false;
    }
  }

  /**
   * Get the status of all the instances, given the region and the security group
   *
   * @param instances A list of instance IDs
   * @param securityGroup The instance's security group.
   * @return boolean: list of instancesStatus.
   */
  public synchronized List<InstanceStatus> instancesStatus(List<String> instances, String region,
      String securityGroup) throws AmazonServiceException {
    AmazonEC2OnDemand onDemand = this.findAmazonEC2OnDemand(region, securityGroup);
    if (onDemand != null) {
      return onDemand.getInstancesStatus(instances);
    }
    return null;
  }

  /**
   * Reboot all the instances, given the region and the security group
   *
   * @param instances A list of instance IDs
   * @param securityGroup The instance's security group.
   * @return boolean: true if successful, false otherwise.
   */
  public synchronized boolean rebootInstances(List<String> instances, String region,
      String securityGroup) {
    try {
      AmazonEC2OnDemand onDemand = this.findAmazonEC2OnDemand(region, securityGroup);
      if (onDemand != null) {
        onDemand.reboot(instances);
        return true;
      }
      return false;
    } catch (AmazonServiceException ase) {
      return false;
    }
  }

  /**
   * Stop all the instances, given the region and the security group
   *
   * @param instances A list of instance IDs
   * @param securityGroup The instance's security group.
   * @return boolean: true if successful, false otherwise.
   */
  public synchronized boolean stopInstances(List<String> instances, String region,
      String securityGroup) {
    try {
      AmazonEC2OnDemand onDemand = this.findAmazonEC2OnDemand(region, securityGroup);
      if (onDemand != null) {
        onDemand.stop(instances);
        return true;
      }
      return false;
    } catch (AmazonServiceException ase) {
      return false;
    }
  }

  /**
   * Start all the instances, given the region and the security group
   *
   * @param instances A list of instance IDs
   * @param securityGroup The instance's security group.
   * @return boolean: true if successful, false otherwise.
   */
  public synchronized boolean startInstances(List<String> instances, String region,
      String securityGroup) {
    try {
      AmazonEC2OnDemand onDemand = this.findAmazonEC2OnDemand(region, securityGroup);
      if (onDemand != null) {
        onDemand.start(instances);
        return true;
      }
      return false;
    } catch (AmazonServiceException ase) {
      return false;
    }
  }

  /**
   * Find or create a new AmazonEC2OnDemand object.
   *
   * @param region: The region the AmazonEC2 object belongs to.
   * @param securityGroup: The security group used to create instances.
   * @return AmazonEC2OnDemand: The AmazonEC2OnDemand object
   */
  private synchronized AmazonEC2OnDemand findAmazonEC2OnDemand(String region,
      String securityGroup) {
    AmazonEC2OnDemand onDemand;
    String key = String.format("%s#%s", securityGroup, region);
    if (!onDemandMap.containsKey(key)) {
      AmazonEC2 client = this.getClient(region);
      log.debug("# Created client '{}'", key);
      onDemand = this.createEC2OnDemand(securityGroup, client);
      if (onDemand == null) {
        return null;
      }
      onDemandMap.put(key, onDemand);
    } else {
      onDemand = onDemandMap.get(key);
    }
    return onDemand;
  }

  /**
   * When creating a keyPair, we will first check if the key already exist and it is valid 1. It
   * does not exist, then we can create the new keyPair freely. 2. If it does exist and it is
   * invalid, we will replace the old one by the new one. 3. If it does exist and it is valid, we
   * will send the appropriate status code.
   *
   * @param region The region the key is being created. There should be one keyPair in that name in
   *        this region
   * @param keyname The keyPair name
   * @return HttpStatus: The code which reflects the operation
   */
  public synchronized AwsKeyPair createKeyPair(String region, String keyname) {
    // use the read lock outside
    try {
      // attempt to find the key pair
      AwsKeyPair keyPair = this.getKeyPair(region, keyname);
      if ((keyPair == null) || (!keyPair.isValid())) {
        log.debug(String.format("#### KeyPair '%s' in region '%s' does not exist or is invalid",
            keyname, region));
        //
        if (keyPair != null) {
          // attempt to delete the old key, and create the new key
          if (!this.deleteKeyPair(region, keyname)) {
            return null;
          }
        }
        AmazonEC2 client = getClient(region);
        if (client == null) {
          throw new ValidationException(
              "Could not create a new AmazonEC2 client in region " + region);
        }
        String privateKey = this.key.getPrivateKey(keyname, client);
        keyPair = new AwsKeyPair(region, keyname, true, privateKey);
        // save the keypair
        this.awsKeyPairRepository.save(keyPair);
      }
      return keyPair;
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return null;
    }
  }

  /**
   * Return all the keys based on that region.
   *
   * @param region The aws region to be used
   * @return List of AwsKeyPair: A list of AwsKeyPair objects in that region
   */
  public List<AwsKeyPair> listKeyPairs(String region) {
    try {
      return this.awsKeyPairRepository.findByRegion(region);
    } catch (Exception ex) {
      log.error(ex.getMessage());
      // better than returning null
      return new ArrayList<>();
    }
  }

  /**
   * Finds and gets a keyPair variable given its name and region
   *
   * @param region The aws region to be used
   * @param keyname The name of the keyPair being searched.
   * @return AwsKeyPair: The keyPair if found, null otherwise
   */
  public AwsKeyPair getKeyPair(String region, String keyname) {
    // use a read lock, since we want to only read from the repository
    try {
      AwsKeyPair keyPair = this.awsKeyPairRepository.findByRegionAndKeyname(region, keyname);
      return keyPair;
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return null;
    }
  }

  /**
   * The deleteKeyPair method will first check if the key exists. If it does, then it will delete
   * the key pair, independently if it is valid or not
   *
   * @param region The region the keyPair belongs to
   * @param keyname The name of the keyPair
   * @return A HttpResponse: OK if successful
   */
  public synchronized boolean deleteKeyPair(String region, String keyname) {
    try {
      AwsKeyPair keyPair = this.getKeyPair(region, keyname);
      if (keyPair != null) {
        log.debug(String.format("###### Deleting keyPair '%s' ##########", keyPair));
        // delete from the mongo repository
        this.awsKeyPairRepository.delete(keyPair);

        AmazonEC2 client = getClient(region);
        if (client == null) {
          throw new ValidationException(
              "Could not create a new AmazonEC2 client in region " + region);
        }
        // attempt to delete it in AWS as well
        this.key.deleteKey(keyname, client);
        // return the status if everything went well
        return true;
      }
      return false;
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return false;
    }
  }

  /**
   * Delete a key pair.
   *
   * @param keyPair to be deleted
   * @return boolean
   */
  public synchronized boolean deleteKeyPair(AwsKeyPair keyPair) {
    if (keyPair == null) {
      return false;
    }
    return this.deleteKeyPair(keyPair.getRegion(), keyPair.getKeyname());
  }

  /**
   * Create a new Security Group in AWS.
   *
   * @param groupname name of the group
   * @param region of the group
   * @param securityGroup related to the group
   * @param description of the group
   * @return group id
   */
  public synchronized String createSecurityGroup(String groupname, String region,
      String securityGroup, String description) {
    String groupId = null;
    try {
      AmazonEC2OnDemand onDemand = this.findAmazonEC2OnDemand(region, securityGroup);
      if (onDemand != null) {
        groupId = onDemand.createSecurityGroup(securityGroup, description);
      }
    } catch (Exception ex) {
      log.error(ex.getMessage());
    }
    return groupId;
  }

  /**
   * Describe a particular security group, given it's region and group id.
   *
   * @param region the region the security group belongs to
   * @param groupId the security Id group id
   * @return the security group being described
   */
  public SecurityGroup describeSecurityGroup(String region, String groupId) {
    AmazonEC2 ec2 = this.getClient(region);
    DescribeSecurityGroupsRequest request = new DescribeSecurityGroupsRequest()
                                                  .withGroupIds(groupId);

    DescribeSecurityGroupsResult response = ec2.describeSecurityGroups(request);
    List<SecurityGroup> securityGroups = response.getSecurityGroups();
    if (securityGroups != null && securityGroups.size() > 0) {
      return securityGroups.get(0);
    }
    return null;
  }

  /**
   * List all the security groups in a particular region.
   *
   * @param region the region we want to see the security groups
   * @return a list of all the security groups
   */
  public List<SecurityGroup> listSecurityGroups(String region) {
    AmazonEC2 ec2 = this.getClient(region);
    DescribeSecurityGroupsRequest request = new DescribeSecurityGroupsRequest();

    DescribeSecurityGroupsResult result = ec2.describeSecurityGroups(request);
    return result.getSecurityGroups();
  }

  /**
   * Delete a particular security group based on its groupId
   *
   * @param region the region the group belongs to
   * @param groupId the security group id
   */
  public void deleteSecurityGroup(String region, String groupId) {
    AmazonEC2 ec2 = this.getClient(region);
    DeleteSecurityGroupRequest request = new DeleteSecurityGroupRequest()
        .withGroupId(groupId);

    ec2.deleteSecurityGroup(request);
  }

  /**
   * Delete an existing security group.
   *
   * @param region The region the security group beings too.
   * @param securityGroup The parent security group.
   * @param groupname The name of the security to be deleted.
   * @return boolean: true if successful, false otherwise.
   */
  public synchronized boolean deleteSecurityGroup(String region, String securityGroup,
      String groupname) {
    try {
      // @TODO: Remove the Security Group from MongoDB.
      AmazonEC2OnDemand onDemand = this.findAmazonEC2OnDemand(region, securityGroup);
      if (onDemand != null) {
        onDemand.deleteSecurityGroup(groupname);
        return true;
      }
      return false;
    } catch (Exception ex) {
      return false;
    }
  }

  /**
   * Return a list of all the existing security groups
   *
   * @param region The name of the region being searched
   * @param securityGroup The name of the security group for the region
   * @return List: A list of security groups.
   */
  public synchronized List<SecurityGroup> listSecurityGroups(String region, String securityGroup) {
    List<SecurityGroup> groups = new ArrayList<>();
    try {
      AmazonEC2OnDemand onDemand = this.findAmazonEC2OnDemand(region, securityGroup);
      if (onDemand != null) {
        groups = onDemand.listSecurityGroup();
      }
    } catch (Exception ex) {
      log.error(ex.getMessage());
    }
    return groups;
  }

  /**
   * Create a new AmazonEC2OnDemand instance, based on the Security Group.
   *
   * @param securityGroup
   * @param client
   * @return AmazonEC2OnDemand
   */
  private synchronized AmazonEC2OnDemand createEC2OnDemand(String securityGroup, AmazonEC2 client) {
    try {
      return new AmazonEC2OnDemand(securityGroup, client);
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return null;
    }
  }

  /**
   * Set the `valid` field in the keyPair object.
   *
   * @param region The region the keyPair belongs to
   * @param keyname The name of the keyPair
   * @param valid The value being set, true or false.
   * @return: A HttpResponse: OK if successful
   */
  public synchronized boolean setValidKeyPair(String region, String keyname, boolean valid) {
    try {
      AwsKeyPair keyPair = this.getKeyPair(region, keyname);
      if (keyPair != null) {
        log.debug(String.format("###### Setting keyPair '%s' valid field as '%s' ##########",
            keyPair, valid));
        // set the new value and update the repository
        keyPair.setValid(valid);
        this.awsKeyPairRepository.save(keyPair);

        return true;
      }
      return false;
    } catch (Exception ex) {
      log.error(ex.getMessage());
      return false;
    }
  }

  public void resetMaps() {
    map.clear();
    onDemandMap.clear();
  }

  /**
   * Validate a credential against the provider.
   *
   * @param awsCredentials to be validate
   * @return true if credentials are ok
   */
  public boolean validateCredential(AwsCredential awsCredentials) {
    try {

      BasicAWSCredentials credentials = new BasicAWSCredentials(awsCredentials.getAccessKeyId(),
          awsCredentials.getSecretAccessKey());
      AmazonEC2ClientBuilder.standard().withRegion(Regions.US_EAST_1.getName())
          .withCredentials(new AWSStaticCredentialsProvider(credentials)).build().describeRegions();
      return true;
    } catch (Exception e) {
      log.error("Validating credential: {}", awsCredentials.toString(), e);
      return false;
    }
  }

  /**
   * Enable monitoring for Instance.
   *
   * @param region - instance region
   * @param instance - instance
   */
  public synchronized void enableMonitoringForInstance(String region, Instance instance) {
    AmazonEC2OnDemand onDemand = this.findAmazonEC2OnDemand(region, null);
    if (onDemand != null) {
      log.debug(String.format("Enabling monitoring for Instance: '%s'", instance.getInstanceId()));
      onDemand.enableMonitoring(new String[] {instance.getInstanceId()});
    }
  }

  /**
   * Clone an instance.
   *
   * @param originalInstance to be cloned
   * @return new cloned instance
   */
  public Instance cloneInstance(AbstractInstance originalInstance) {
    try {
      String region = originalInstance.getInstance().getRegion();
      String securityGroup = originalInstance.getInstance().getSecurityGroup();
      AmazonEC2OnDemand onDemand = this.findAmazonEC2OnDemand(region, securityGroup);
      if (onDemand != null) {
        Instance instanceById =
            onDemand.getInstanceById(originalInstance.getInstance().getProviderId());
        String machineType = instanceById.getInstanceType();
        String instanceClonedName = originalInstance.getInstance().getName() + "cloned";
        Instance clonedInstance =
            onDemand.cloneInstance(instanceClonedName, instanceById, machineType);
        return clonedInstance;
      }
    } catch (Exception ase) {
      log.error("Erro cloning! ", ase);
    }
    return null;
  }
}
