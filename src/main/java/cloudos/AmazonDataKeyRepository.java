package cloudos;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Repository class for AmazonDataKey type.
 * 
 * @author Rogério Souza
 *
 */
public interface AmazonDataKeyRepository extends MongoRepository<AmazonDataKey, String> {

  public AmazonDataKey findByAlias(String alias);

}
