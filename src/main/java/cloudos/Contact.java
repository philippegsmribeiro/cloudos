package cloudos;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Define the Contact model. The Contact object will be added into the `contacts` collections in
 * Mongo's development database. Every time a user submits a message in the CloudOS main page
 * (index.html), if the contact form is valid, then it will insert the new contact object into the
 * database.
 *
 * @author Philippe Ribeiro
 */
@Document(collection = "contacts")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Contact {

  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  private String id;

  @NotNull
  @Size(min = 2)
  private String name;

  @NotNull
  @Size(min = 2)
  private String email;

  @NotNull private String date;

  @NotNull
  @Size(min = 2)
  private String message;

  /**
   * Constructor for the Contact object. A contact form contains a name, email and message.
   *
   * @param name: The name of the user
   * @param email: The user's email
   * @param message: The user's message
   */
  public Contact(String name, String email, String message) {
    this.id = new ObjectId().toHexString();
    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    this.date = dateFormat.format(new Date());
    this.name = name;
    this.email = email;
    this.message = message;
  }
}
