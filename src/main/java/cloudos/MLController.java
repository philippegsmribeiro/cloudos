package cloudos;

import cloudos.security.WebSecurityConfig;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = MLController.ML)
public class MLController {

  public static final String ML = WebSecurityConfig.API_PATH + "/ml";
}
