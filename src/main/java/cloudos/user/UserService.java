package cloudos.user;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import cloudos.encryptionKeys.AmazonKMSService;
import cloudos.encryptionKeys.AmazonKMSServiceException;
import cloudos.exceptions.InvalidRequestException;
import cloudos.utils.BeanValidationUtil;

/**
 * Responsible for user authentication.
 * Search the user in the database and returns to spring validate de password
 *
 * @author gleimar
 */
@Service
public class UserService implements UserDetailsService {

  private final Logger logger = LoggerFactory.getLogger(UserService.class);

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private BeanValidationUtil beanValidationUtil;

  @Autowired
  private AmazonKMSService amazonKMSService;

  /**
   * Saves a user in the repository after encrypting sensistive data.
   * 
   * @param user - The user to be saved.
   * @return
   * @throws AmazonKMSServiceException - When an error occurs when encrypting/decrypting
   * @throws InvalidRequestException - When validations raise an error.
   */
  public User save(User user) throws AmazonKMSServiceException, InvalidRequestException {
    beanValidationUtil.validate(user);
    encryptUserSensitiveData(user);
    userRepository.save(user);
    return decryptUserSensitiveData(user);
  }

  /**
   * Updates the given username password.
   * 
   * @param username - the username key
   * @param oldPassword - the old password that will be searched for
   * @param newPassword - the new password that will be updated
   *
   * @throws UsernameNotFoundException - When the user is not found
   * @throws InvalidRequestException - When any required parameter is null or empty
   * @throws AmazonKMSServiceException - When an error occurs when encrypting/decrypting
   */
  public void updatePassword(String username, String oldPassword, String newPassword)
      throws UsernameNotFoundException, InvalidRequestException, AmazonKMSServiceException {

    if (StringUtils.isBlank(username)) {
      throw new InvalidRequestException("The username must not be null.");
    }

    if (StringUtils.isBlank(oldPassword)) {
      throw new InvalidRequestException("The old password must not be null.");
    }

    if (StringUtils.isBlank(newPassword)) {
      throw new InvalidRequestException("The new password must not be null.");
    }

    String encryptedUsername =
        amazonKMSService.encryptMessage(amazonKMSService.getUserEncryptedDataKey(), username);
    String encryptedOldPassword =
        amazonKMSService.encryptMessage(amazonKMSService.getUserEncryptedDataKey(), oldPassword);
    String encryptedNewPassword =
        amazonKMSService.encryptMessage(amazonKMSService.getUserEncryptedDataKey(), newPassword);

    User user = userRepository.findByUsernameAndPassword(encryptedUsername, encryptedOldPassword);

    if (user == null) {
      throw new UsernameNotFoundException(username);
    }

    user.setPassword(encryptedNewPassword);
    userRepository.save(user);

  }

  /**
   * Updates the user profile and preferences.
   * 
   * @param username - the username.
   * @param userProfile - the user profile to be saved.
   * @throws InvalidRequestException - when user validation raises an error.
   * @throws UsernameNotFoundException in case the username was not found.
   * @throws AmazonKMSServiceException when an error occurs when encrypting/decrypting
   */
  public UserProfile updateUserProfile(String username, UserProfile userProfile)
      throws InvalidRequestException, AmazonKMSServiceException {

    String encryptedUsername =
        amazonKMSService.encryptMessage(amazonKMSService.getUserEncryptedDataKey(), username);

    User user = userRepository.findByUsername(encryptedUsername);
    if (user == null) {
      throw new UsernameNotFoundException(username);
    }

    beanValidationUtil.validate(userProfile);

    user = decryptUserSensitiveData(user);
    user.setProfile(userProfile);

    user = encryptUserSensitiveData(user);
    userRepository.save(user);

    return decryptUserSensitiveData(user).getProfile();
  }

  /**
   * Load the user based on his username.
   *
   * @param username the username
   * @return the user detail
   * @throws UsernameNotFoundException in case the username was not found
   */
  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

    String encryptedUsername = username;
    try {
      encryptedUsername =
          amazonKMSService.encryptMessage(amazonKMSService.getUserEncryptedDataKey(), username);
    } catch (AmazonKMSServiceException e) {
      logger.error(
          "Could not encrypt username to loadUserByUsername. Will try to search user by original username value.");
    }

    User user = userRepository.findByUsername(encryptedUsername);

    if (user == null) {

      throw new UsernameNotFoundException(username);

    } else {

      try {
        return new MyUserDetails(decryptUserSensitiveData(user));
      } catch (AmazonKMSServiceException e) {
        logger.error("Could not decrypt user sensitive data. Will return encrypted values.");
        return new MyUserDetails(user);
      }

    }

  }

  /**
   * Load the user profile based on his username.
   *
   * @param username the username
   * @return the user profile
   * @throws UsernameNotFoundException in case the username was not found
   */
  public UserProfile loadUserProfileByUsername(String username)
      throws UsernameNotFoundException, AmazonKMSServiceException {

    String encryptedUsername =
        amazonKMSService.encryptMessage(amazonKMSService.getUserEncryptedDataKey(), username);

    User user = userRepository.findByUsername(encryptedUsername);
    if (user == null) {
      throw new UsernameNotFoundException(username);
    } else {
      return decryptUserSensitiveData(user).getProfile();
    }
  }

  /**
   * Load the user based on his email.
   *
   * @param email the email
   * @return the user detail
   * @throws UsernameNotFoundException in case the username was not found
   */
  public UserDetails loadUserByEmail(String email)
      throws UsernameNotFoundException, AmazonKMSServiceException {

    String encryptedEmail =
        amazonKMSService.encryptMessage(amazonKMSService.getUserEncryptedDataKey(), email);

    User user = userRepository.findByEmail(encryptedEmail);
    if (user == null) {
      throw new UsernameNotFoundException(email);
    } else {
      return new MyUserDetails(decryptUserSensitiveData(user));
    }
  }

  /**
   * Retrieve the user from Mongo and checks if this is the first login or not.
   *
   * @param username the username of the cloudosTenant user object to be searched.
   * @return true or false.
   */
  public boolean isUserFirstAccess(String username)
      throws UsernameNotFoundException, InvalidRequestException, AmazonKMSServiceException {

    String encryptedUsername =
        amazonKMSService.encryptMessage(amazonKMSService.getUserEncryptedDataKey(), username);
    
    User user = userRepository.findByUsername(encryptedUsername);
    
    if (user.isFirstAccess()) {
      user.setFirstAccess(false);
      userRepository.save(user);
      return true;
    }

    return false;

  }

  /**
   * Encrypts user's sensitive data based on Amazon customer master key (CMK) using AES algorithm.
   *
   * @param user - The user to have sensitive data encrypted.
   * @return user - The User with encrypted sensitive data.
   * @throws AmazonKMSServiceException when an error occurs when encrypting/decrypting
   */
  public User encryptUserSensitiveData(User user) throws AmazonKMSServiceException {

    byte[] userDataKey = amazonKMSService.getUserEncryptedDataKey();

    if (!StringUtils.isBlank(user.getEmail())) {
      user.setEmail(amazonKMSService.encryptMessage(userDataKey, user.getEmail()));
    }

    if (!StringUtils.isBlank(user.getUsername())) {
      user.setUsername(amazonKMSService.encryptMessage(userDataKey, user.getUsername()));
    }

    if (!StringUtils.isBlank(user.getPassword())) {
      user.setPassword(amazonKMSService.encryptMessage(userDataKey, user.getPassword()));
    }

    if (!StringUtils.isBlank(user.getProfile().getFirstName())) {
      user.getProfile().setFirstName(
          amazonKMSService.encryptMessage(userDataKey, user.getProfile().getFirstName()));
    }

    if (!StringUtils.isBlank(user.getProfile().getLastName())) {
      user.getProfile().setLastName(
          amazonKMSService.encryptMessage(userDataKey, user.getProfile().getLastName()));
    }

    if (!StringUtils.isBlank(user.getProfile().getTelephone())) {
      user.getProfile().setTelephone(
          amazonKMSService.encryptMessage(userDataKey, user.getProfile().getTelephone()));
    }

    return user;

  }

  /**
   * Decrypts user's sensitive data based on Amazon customer master key (CMK) using AES algorithm.
   *
   * @param user - The user to have sensitive data decrypted.
   * @return user - The User with decrypted sensitive data.
   * @throws AmazonKMSServiceException when an error occurs when encrypting/decrypting
   */
  public User decryptUserSensitiveData(User user) throws AmazonKMSServiceException {

    byte[] userDataKey = amazonKMSService.getUserEncryptedDataKey();

    if (!StringUtils.isBlank(user.getEmail())) {
      user.setEmail(amazonKMSService.decryptMessage(userDataKey, user.getEmail()));
    }

    if (!StringUtils.isBlank(user.getUsername())) {
      user.setUsername(amazonKMSService.decryptMessage(userDataKey, user.getUsername()));
    }

    if (!StringUtils.isBlank(user.getProfile().getFirstName())) {
      user.getProfile().setFirstName(
          amazonKMSService.decryptMessage(userDataKey, user.getProfile().getFirstName()));
    }

    if (!StringUtils.isBlank(user.getProfile().getLastName())) {
      user.getProfile().setLastName(
          amazonKMSService.decryptMessage(userDataKey, user.getProfile().getLastName()));
    }

    if (!StringUtils.isBlank(user.getProfile().getTelephone())) {
      user.getProfile().setTelephone(
          amazonKMSService.decryptMessage(userDataKey, user.getProfile().getTelephone()));
    }


    return user;

  }
  
  /**
   * Delete an user.
   * @param user to be deleted
   */
  public void delete(User user) {
    userRepository.delete(user);
  }

  /**
   * Retrieve user by the username.
   * @param username of user
   * @return
   */
  public User retrieveByUsername(String username) {
    return userRepository.findByUsername(username);
  }
  
  /**
   * Retrieve the number of users.
   * @return number of user
   */
  public long retrieveNumberOfUsers() {
    return userRepository.count();
  }
  
  /**
   * List all users.
   * @return list of users
   */
  public List<User> retrieveAllUsers() {
    return userRepository.findAll();
  }
  
  /**
   * Find by id.
   * @param id of user.
   * @return User
   */
  public User findById(String id) {
    return userRepository.findOne(id);
  }

  /**
   * Attempt to find the user by the email.
   *
   * @param email - the user's email
   * @return An user, if found, null otherwise
   * @throws AmazonKMSServiceException in case we fail to load the key
   */
  public User findByEmail(String email) throws AmazonKMSServiceException {
    byte[] userDataKey = amazonKMSService.getUserEncryptedDataKey();
    return userRepository.findByEmail(amazonKMSService.encryptMessage(userDataKey, email));
  }

  public class MyUserDetails implements UserDetails {

    private static final long serialVersionUID = 1L;
    private User user;

    public MyUserDetails(User user) {
      super();
      this.user = user;
    }

    /**
     * Obtain the authority of the user.
     *
     * @return a list of the granted authorities of the user
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
      List<GrantedAuthority> ls = new ArrayList<GrantedAuthority>();
      ls.add(new SimpleGrantedAuthority("ADMIN"));
      return ls;
    }

    @Override
    public String getPassword() {
      return user.getPassword();
    }

    @Override
    public String getUsername() {
      return user.getUsername();
    }

    public String getUserId() {
      return user.getId();
    }

    @Override
    public boolean isAccountNonExpired() {
      return true;
    }

    @Override
    public boolean isAccountNonLocked() {
      return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
      return true;
    }

    @Override
    public boolean isEnabled() {
      return true;
    }
  }
}
