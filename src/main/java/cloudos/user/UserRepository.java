package cloudos.user;

import org.springframework.data.mongodb.repository.MongoRepository;

interface UserRepository extends MongoRepository<User, String> {

  User findByEmail(String email);

  User findByUsername(String username);
  
  User findByUsernameAndPassword(String username, String password);
}
