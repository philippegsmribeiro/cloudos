package cloudos.user;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 * Defines the model of the Invite that will be used to send invitation to new users
 * in the cloudtown platform.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "cloudos_invites")
public class Invite {

  @Id
  private String id;
  @NonNull
  private String email;

  private String guest;
  @NonNull
  private String tenant;
  private Boolean isPending;
  private Date invitationDate;
  private Date expirationDate;

}
