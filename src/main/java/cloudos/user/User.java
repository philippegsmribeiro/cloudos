package cloudos.user;

import javax.validation.constraints.NotNull;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by Philippe Ribeiro on 04/07/2018.
 */
@Data
@Document(collection = "cloudos_users")
@Log4j2
@NoArgsConstructor
@ToString
public class User {

  @Id
  private String id;

  @NotEmpty
  private String username;

  @NotEmpty
  private String password;

  @NotEmpty
  private String email;

  private UserRole role;

  @NotNull
  private UserProfile profile = new UserProfile();
  
  private boolean firstAccess = true;

  /**
   * Constructor with all the parameters.
   *
   * @param firstName the first name
   * @param lastName the last name
   * @param username the username
   * @param password the password
   * @param email the email
   */
  public User(String firstName, String lastName, String username, String password, String email) {
    setPassword(password);
    setUsername(username);
    setEmail(email);
    setFirstAccess(true);
    profile.setLastName(lastName);
    profile.setFirstName(firstName);
  }

  @Builder
  public User(String id, String username, String password, String email, UserRole role,
              UserProfile userProfile, boolean firstAccess) {
    this.setId(id);
    this.setUsername(username);
    this.setEmail(email);
    this.setPassword(password);
    this.setProfile(userProfile);
    this.setFirstAccess(firstAccess);
    this.setRole(role);
  }

  /**
   * Constructor with the user id already defined.
   *
   * @param id the user id
   */
  public User(String id) {
    this.id = id;
  }

}
