package cloudos.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserPreferences {

  @NotNull
  private String dateFormat;

  @NotNull
  private String timeFormat;

  @NotNull
  private boolean alertNotifications;

  @NotNull
  private boolean emailNotification;

  @NotNull
  private boolean use24HourFormat;

  @NotNull
  private boolean useSSL;

}
