package cloudos.user;

/**
 * Define the user role, if the user is admin or not for example.
 * 
 * @author Rogério Souza
 *
 */
public enum UserRole {
  ADMIN,
  USER
}
