package cloudos.user;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;

import cloudos.amazon.AWSCredentialService;
import cloudos.encryptionKeys.AmazonKMSService;
import cloudos.encryptionKeys.AmazonKMSServiceException;

/**
 * InviteService is responsible for managing invitations - whenever we want to add a new user
 * to cloudtown, we will send an invitation to that user.
 */
@Service
public class InviteService {

  @Autowired
  private UserService userService;

  private final Logger log = LoggerFactory.getLogger(UserService.class);

  @Autowired
  private InviteRepository inviteRepository;

  @Autowired
  private AmazonKMSService amazonKMSService;

  @Autowired
  private AWSCredentialService awsCredentialService;

  // Replace sender@example.com with your "From" address.
  // This address must be verified with Amazon SES.
  @Value("${cloudos.settings.invite.from_email}")
  private String FROM;

  // The subject line for the email.
  @Value("${cloudos.settings.invite.subject}")
  private String SUBJECT;

  // The HTML body for the email.
  @Value("${cloudos.settings.invite.html_body}")
  private String HTMLBODY;

  // The email body for recipients with non-HTML email clients.
  @Value("${cloudos.settings.invite.text_body}")
  private String TEXTBODY;

  /**
   * Create a new invitation to cloudos by sending an email to the user. The invitation will be
   * pending until the user registers as an cloudos user.
   *
   * @param inviteRequest the original invitation request
   * @return a newly created invitation, or null in case it failed
   * @throws AmazonKMSServiceException whenever there's a problem loading the key
   */
  public Invite create(@NotNull InviteRequest inviteRequest) throws AmazonKMSServiceException {

    // we will always encrypt the email for security reasons
    String encryptedEmail =
        amazonKMSService.encryptMessage(amazonKMSService.getUserEncryptedDataKey(),
            inviteRequest.getEmail());

    Calendar cal = Calendar.getInstance();
    Invite invite = null;

    // @TODO: Make user the user email is in the same domain as the tenant

    // check if the invite already exists
    Invite current = this.inviteRepository.findByEmailAndTenant(encryptedEmail,
                                                                inviteRequest.getTenant());

    if (current != null && (current.getIsPending() != null && current.getIsPending())) {
      log.warn("Invitation for user {} already exists.", inviteRequest.getEmail());
    }
    // check if within expiration date
    else if (current != null && cal.after(current.getExpirationDate())) {
      log.warn("Invitation is expired. Will not create a new one");
      this.inviteRepository.delete(current);
    }
    else if (userService.findByEmail(inviteRequest.getEmail()) != null) {
      log.warn("User {} already exists. Won't create new invitation", inviteRequest.getEmail());
    }
    else {
      Date now = new Date();
      cal.setTime(now);
      cal.add(Calendar.DATE, 7);
      Date expirationDate = cal.getTime();

      invite = Invite.builder()
                    .email(encryptedEmail)
                    .invitationDate(now)
                    .tenant(inviteRequest.getTenant())
                    .guest(inviteRequest.getGuest())
                    .isPending(true)
                    .expirationDate(expirationDate)
                    .build();

      // send the invitation email
      if (!this.sendInvitationEmail(inviteRequest.getEmail())) {
        log.warn("Failed to send the email to destination {}", inviteRequest.getEmail());
        return null;
      }
      // save the invitation in the collection
      invite = this.inviteRepository.save(invite);
    }
    return invite;
  }

  /**
   * Return a list of all the current invites. Expired invitations are not included.
   *
   * @return a list of invites
   */
  public List<Invite> list() {
    Date today = new Date();
    List<Invite> invites = this.inviteRepository.findAll();
    // remove all the invites that are expired
    return invites.stream()
                  .filter(invite -> today.before(invite.getExpirationDate()))
                  .collect(Collectors.toList());
  }

  /**
   * Delete an existing invitation.
   *
   * @param id the invitation id
   * @return true, if the invitation was found, false otherwise
   */
  public boolean delete(@NotNull String id) {
    if (this.inviteRepository.findOne(id) != null) {
      // delete the invitation and return
      this.inviteRepository.delete(id);
      return true;
    }
    return false;
  }

  /**
   * Attempt to describe if an invitation exists.
   *
   * @param id the invitation id
   * @return an Invitation is found, null otherwise
   */
  public Invite describe(@NotNull String id) {
    return this.inviteRepository.findOne(id);
  }

  /**
   * Update an existing invitation. We will let the user update only the email destination of the
   * invitation. The expiration date will be updated another 7 days.
   *
   * @param id the invitation id
   * @return a newly updated invitation
   */
  public Invite edit(@NotNull String id, @NotNull InviteRequest inviteRequest)
      throws AmazonKMSServiceException {

    Calendar cal = Calendar.getInstance();
    Date now = new Date();

    Invite invite = this.inviteRepository.findOne(id);
    // current invitation found... edit
    if (invite != null && now.before(invite.getExpirationDate()) && invite.getIsPending()) {
      // we will always encrypt the email for security reasons
      String encryptedEmail =
          amazonKMSService.encryptMessage(amazonKMSService.getUserEncryptedDataKey(),
              inviteRequest.getEmail());

      // send the invitation email
      if (!this.sendInvitationEmail(inviteRequest.getEmail())) {
        log.warn("Failed to send the email to destination {}", inviteRequest.getEmail());
        return null;
      }

      // update the expiration date
      cal.setTime(now);
      cal.add(Calendar.DATE, 7);
      Date expirationDate = cal.getTime();

      // update the email
      invite.setEmail(encryptedEmail);
      invite.setExpirationDate(expirationDate);
      return this.inviteRepository.save(invite);
    }
    return null;
  }

  /**
   * Send a new invitation email to a new cloudos user. The email body is standard and defined,
   * and the email destination is the new user's email.
   *
   * @param email the new user's email
   * @return true if successful, false otherwise
   */
  public boolean sendInvitationEmail(@NotNull String email) {
    try {
      AmazonSimpleEmailService client = AmazonSimpleEmailServiceClientBuilder.standard()
                                          .withRegion(Regions.US_EAST_1)
                                          .withCredentials(awsCredentialService.getAWSCredentials())
                                          .build();
      SendEmailRequest request = new SendEmailRequest()
              .withDestination(
                  new Destination().withToAddresses(email))
              .withMessage(new Message()
                  .withBody(new Body()
                    .withHtml(new Content()
                      .withCharset("UTF-8").withData(HTMLBODY))
                    .withText(new Content()
                      .withCharset("UTF-8").withData(TEXTBODY)))
                  .withSubject(new Content()
                      .withCharset("UTF-8").withData(SUBJECT)))
              .withSource(FROM);
      // send the email
      client.sendEmail(request);
      log.info("Email sent to {}", email);
      return true;
    } catch (Exception ex) {
      log.error("The email was not sent. Error message: {}", ex.getMessage());
    }
    return false;
  }

  /**
   * Verify if a current invitation is expired or invalid.
   *
   * @param invite an existing invitation.
   * @return true if expired, false otherwise.
   */
  public boolean isExpired(@NotNull Invite invite) {
    // check if the invitation is still pending.
    if (!invite.getIsPending()) {
      return true;
    }

    Date now = new Date();
    return now.after(invite.getExpirationDate());
  }
}
