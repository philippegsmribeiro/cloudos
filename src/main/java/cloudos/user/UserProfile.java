package cloudos.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.NotNull;

import lombok.Data;

import org.hibernate.validator.constraints.NotEmpty;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserProfile {

  @NotEmpty
  private String firstName;

  @NotEmpty
  private String lastName;

  @NotEmpty
  private String country;

  @NotEmpty
  private String state;

  @NotEmpty
  private String city;

  @NotEmpty
  private String telephone;

  @NotEmpty
  private String jobRole;

  private String picture;

  @NotNull
  private UserPreferences preferences;

}
