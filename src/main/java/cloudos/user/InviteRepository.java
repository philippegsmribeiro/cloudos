package cloudos.user;

import javax.validation.constraints.NotNull;

import org.springframework.data.mongodb.repository.MongoRepository;


public interface InviteRepository extends MongoRepository<Invite, String> {

  Invite findByEmail(@NotNull String email);

  Invite findByEmailAndTenant(@NotNull String email, @NotNull String tenant);
}
