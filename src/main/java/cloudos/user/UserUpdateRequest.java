package cloudos.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * Represents the request object to update a given username password
 * 
 * @author Rogério Souza
 *
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserUpdateRequest {

  private String username;
  private String newPassword;
  private String oldPassword;

}
