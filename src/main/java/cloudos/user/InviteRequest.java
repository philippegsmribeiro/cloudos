package cloudos.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Defines the invite request for creating a new invitation in cloudtown.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InviteRequest {

  private String email;
  private String guest;
  private String tenant;

}
