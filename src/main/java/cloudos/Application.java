package cloudos;

import cloudos.config.LoadOnTestCondition;
import cloudos.config.NotLoadOnTestCondition;
import cloudos.encryptionKeys.AmazonKMSServiceException;
import cloudos.exceptions.InvalidRequestException;
import cloudos.keys.cloudoskey.CloudosKeyService;
import cloudos.machinelearning.Models;
import cloudos.machinelearning.ModelsRepository;
import cloudos.monitoring.AbstractMonitor;
import cloudos.monitoring.AbstractProviderMonitor;
import cloudos.monitoring.RunAfterStartup;
import cloudos.repositories.CloudOsService;
import cloudos.storage.StorageService;
import cloudos.user.User;
import cloudos.user.UserService;
import cloudos.utils.ProfileUtils;
import com.github.ulisesbocchio.jar.resources.JarResourceLoader;
import java.io.File;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.session.data.mongo.config.annotation.web.http.EnableMongoHttpSession;
import org.springframework.web.client.RestTemplate;

/**
 * Define the main class for the CloudOS project. The Application class is responsible for starting
 * the Tomcat server, as well as configuring Spring.
 *
 * @author Philippe Ribeiro
 */
@SpringBootApplication
@EnableScheduling
@EnableMongoHttpSession
@EnableCaching
@Log4j2
public class Application extends SpringBootServletInitializer implements CommandLineRunner {

  public static final Locale LOCALE_DEFAULT = Locale.US;
  public static final ZoneId ZONE_ID_DEFAULT = ZoneId.systemDefault();
  public static AtomicBoolean STARTED = new AtomicBoolean(false);
  private static final String SCALER_MODEL = "scaler.zip";
  private static final String DECIDER_MODEL = "decider.zip";

  @Autowired
  CloudosKeyService cloudosKeyService;

  @Autowired
  CredentialManagerService credentialManagerService;
  /* Use the User collections */
  @Autowired
  private UserService userService;

  @Autowired
  private CloudOsService cloudOsService;

  @Autowired
  protected ModelsRepository modelsRepository;

  @Autowired
  ProfileUtils profileUtils;

  @Value("${cloudos.build.endpoint}")
  private String endpoint;

  @Value("${cloudos.build.endpoint.repository}")
  private String repositoryEndpoint;

  @Value("${cloudos.ml.autoscaler}")
  private String autoscaler;

  @Value("${admin.user.firstname}")
  private String adminUserFirstName;
  @Value("${admin.user.surname}")
  private String adminUserSurname;
  @Value("${admin.user.loginname}")
  private String adminUserLoginName;
  @Value("${admin.user.password}")
  private String adminUserPassword;
  @Value("${admin.user.email}")
  private String adminUserEmail;

  /**
   * Main application method.
   *
   * @param args arguments
   */
  public static void main(String[] args) {
    // set the security context to be visible on the child threads
    SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);
    new SpringApplicationBuilder().sources(Application.class)
        .resourceLoader(new JarResourceLoader()).run(args);
  }

  @Bean
  @Conditional(NotLoadOnTestCondition.class)
  CommandLineRunner init(StorageService storageService) {
    return (args) -> {
      storageService.deleteAll();
      storageService.init();
    };
  }

  /**
   * Rest Template.
   *
   * @return RestTemplate
   */
  @Bean
  public RestTemplate getRestTemplate() {
    final RestTemplate restTemplate = new RestTemplate();

    restTemplate.setMessageConverters(
        Arrays.asList(new FormHttpMessageConverter(), new StringHttpMessageConverter()));
    restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("admin", "admin"));

    return restTemplate;
  }

  @Bean
  public TestRestTemplate restTemplate() {
    return new TestRestTemplate();
  }

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    return application.sources(Application.class);
  }

  /**
   * Create the local installation point and repository.
   *
   * @return boolean: true if successful created, false otherwise
   */
  public boolean createLocalRepository() {
    // Due to root issues I will create it locally
    try {
      File endpoint = new File(this.endpoint);
      File repository = new File(this.repositoryEndpoint);
      // endpoint does not exist ... create
      if (!endpoint.exists()) {
        log.info("Endpoint '{}' for CloudOS does not exist... creating", this.endpoint);
        if (!endpoint.mkdirs()) {
          log.warn("Could not create endpoint");
          return false;
        }
      }
      if (!repository.exists()) {
        log.info("Repository endpoint '{}' for CloudOS does not exist... creating",
            this.repositoryEndpoint);
        if (!repository.mkdirs()) {
          log.warn("Could not create repository endpoint");
          return false;
        }
      }
      return true;
    } catch (Exception ex) {
      log.error(ex);
      return false;
    }
  }

  /**
   * Create the ML repository in the cloudos directory.
   *
   * @return boolean: If the repository was successfully created.
   */
  public boolean createMlRepository() {
    File file = new File(this.autoscaler);
    if (!file.exists()) {
      return file.mkdirs();
    }
    return true;
  }

  /** Check if the CloudOS maven repository is set. */
  public void configureCloudOsRepository() {
    this.cloudOsService.configureMavenRepository();
  }

  @Override
  public void run(String... args) throws Exception {
    // invoke the repository creation
    if (!this.createLocalRepository()) {
      log.warn("Failed to create local repository. Exiting...");
      return;
    }

    // create the ML repository
    if (!this.createMlRepository()) {
      log.warn("Failed to create local ML repository. Exiting ...");
      return;
    }

    // load the models
    // download the decider model
    this.modelsRepository.download(Models.AUTOSCALER, Models.Task.PRODUCTION, DECIDER_MODEL);

    // download the scaler model
    this.modelsRepository.download(Models.AUTOSCALER, Models.Task.PRODUCTION, SCALER_MODEL);
  }

  /**
   * Set up the application for running.
   *
   * @return runner
   */
  @Bean
  @Conditional(NotLoadOnTestCondition.class)
  CommandLineRunner setUp() {
    return (args) -> {
      log.info("Running application setup:");

      // invoke the cloudos repository
      this.configureCloudOsRepository();

      validateUsers();

      // setup default keys
      setUpDefaultKeys();

      if (!profileUtils.isDeploy()) {
        // set the gcloud credentials for testing
        // TODO: should be remove in the future
        credentialManagerService.setDefaultCredentials();
      }

      STARTED.set(true);

      // run monitor after start
      runMonitorsAfterStartingUp();
    };
  }

  /**
   * Start/Run once the monitors that are annotated with the RunAfterStartup.
   * For monitors that load/population some essential data for the application.
   */
  private void runMonitorsAfterStartingUp() {
    //
    Map<String, Object> beans = applicationContext.getBeansWithAnnotation(RunAfterStartup.class);
    for (Object bean : beans.values()) {
      if (bean instanceof AbstractMonitor) {
        AbstractMonitor monitor = (AbstractMonitor) bean;
        log.info("Starting monitor: {}", monitor.getClass().getSimpleName());
        monitor.run();
      }
      if (bean instanceof AbstractProviderMonitor) {
        AbstractProviderMonitor monitor = (AbstractProviderMonitor) bean;
        log.info("Starting monitor: {}", monitor.getClass().getSimpleName());
        monitor.run();
      }
    }
    log.info(beans.toString());

  }

  @Autowired
  ApplicationContext applicationContext;

  /**
   * Create the default keys.
   */
  private void setUpDefaultKeys() {
    // set the default keys if not exists
    cloudosKeyService.setUpDefaultKeys();
  }

  /**
   * Set the users if not exists - Save the admin user for first logon.
   * 
   * @throws AmazonKMSServiceException if kms generates an error
   * @throws InvalidRequestException if request is invalid
   */
  private void validateUsers() throws AmazonKMSServiceException, InvalidRequestException {
    log.info("# Validating users:");
    long count = this.userService.retrieveNumberOfUsers();

    if (count == 0) {

      this.userService.save(new User(adminUserFirstName, adminUserSurname, adminUserLoginName,
          adminUserPassword, adminUserEmail));
    }

    log.info(" - Users found with findAll():");
    for (User user : this.userService.retrieveAllUsers()) {
      log.info(" - {}", user);
    }
  }

  /**
   * Just set it as Started for tests.
   *
   * @return runner
   */
  @Bean
  @Conditional(LoadOnTestCondition.class)
  CommandLineRunner setUpTest() {
    return (args) -> {
      // set default credentials
      credentialManagerService.setDefaultCredentials();
      // setup default keys
      setUpDefaultKeys();
      STARTED.set(true);
    };
  }
}
