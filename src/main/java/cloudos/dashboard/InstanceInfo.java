package cloudos.dashboard;

import cloudos.Providers;
import cloudos.models.InstanceStatus;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Created by philipperibeiro on 2/3/17. */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InstanceInfo implements Serializable {

  private String type;
  private String name;
  private InstanceStatus status;
  private String resourceGroup;
  private String location;
  private Providers provider;
  private Double estimatedCost;

}
