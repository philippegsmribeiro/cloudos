package cloudos.dashboard;

import cloudos.Providers;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Created by philipperibeiro on 2/3/17. */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Provider implements Serializable {

  private static final long serialVersionUID = 1L;

  @JsonProperty(required = true)
  @ApiModelProperty(notes = "The provider's name", required = true)
  private Providers providers;

  @JsonProperty(required = true)
  @ApiModelProperty(notes = "The monthly amount", required = true)
  private Double amount;

  @JsonProperty(required = true)
  @ApiModelProperty(notes = "The number of resoruces", required = true)
  private Integer resources;

  /**
   * Define the merge operation for Provider objects.
   *
   * @param provider A provider object to be merged.
   * @return Provider A merged provider object.
   */
  public Provider merge(Provider provider) {
    // check if the two providers match.
    if (this.providers.equals(provider.getProviders())) {
      return new Provider(this.providers, this.amount + provider.getAmount(),
          this.resources + provider.getResources());
    }
    // they do not match, so nothing to do.
    return this;
  }

}
