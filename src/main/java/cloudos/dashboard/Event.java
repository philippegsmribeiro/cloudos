package cloudos.dashboard;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Created by philipperibeiro on 2/3/17. */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Event implements Serializable {

  private String content;

}
