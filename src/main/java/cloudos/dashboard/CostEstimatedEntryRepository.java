package cloudos.dashboard;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Custom spring data repository interface for {@link CostEstimatedEntry}. Created by Alex Calagua
 * on 10/26/17
 */
public interface CostEstimatedEntryRepository extends MongoRepository<CostEstimatedEntry, String> {

  /* Used to return the very last one added */
  CostEstimatedEntry findFirstByOrderByTimestampDesc();
}
