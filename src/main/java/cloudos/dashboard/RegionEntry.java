package cloudos.dashboard;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by philipperibeiro on 6/28/17.
 *
 * <p>Build a snapshot repository for the Regions feature in the Dashboard
 */
@Data
@Document(collection = "dashboard_regions_entry")
public class RegionEntry implements Serializable {

  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  private String id;

  private List<DashboardRegion> regions;
  private Date timestamp;

  public RegionEntry() {
    this.regions = new ArrayList<>();
    this.timestamp = new Date();
  }

  public RegionEntry(List<DashboardRegion> regions) {
    this.regions = regions;
    this.timestamp = new Date();
  }

}
