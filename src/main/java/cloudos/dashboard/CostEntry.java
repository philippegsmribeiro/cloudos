package cloudos.dashboard;

import cloudos.utils.DateUtil;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Created by philipperibeiro on 6/23/17. Define a model that holds the cost information. We will
 * insert in the appropriate Mongo collection so the user does not need to fetch the Spark job at
 * every request.
 */
@Data
@Builder
@AllArgsConstructor
@Document(collection = "dashboard_cost_entry")
public class CostEntry implements Serializable {

  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  private String id;

  private Cost totalDailyCost;
  private Cost totalMonthyCost;
  private Cost totalWeeklyCost;
  private List<BigDecimal> weeklyCost;

  @DateTimeFormat(pattern = "dd-MM-yyyy")
  private Date timestamp;

  /** Default constructor for the CostEntry. */
  public CostEntry() {
    this.timestamp = new Date();
    this.totalWeeklyCost = new Cost(0.0, DateUtil.getWeek());
    this.totalMonthyCost = new Cost(0.0, DateUtil.getMonth());
    this.totalDailyCost = new Cost(0.0, DateUtil.getDay());
    this.weeklyCost = new ArrayList<>();
  }

  /**
   * Constructor. Set all the paramters via the constructor
   *
   * @param totalDailyCost The total cost for the current day
   * @param totalMonthyCost The total cost for the current month
   * @param totalWeeklyCost The total cost for the current week
   * @param weeklyCost The total cost for the past week.
   */
  public CostEntry(
      @NotNull Cost totalDailyCost,
      @NotNull Cost totalMonthyCost,
      @NotNull Cost totalWeeklyCost,
      @NotNull List<BigDecimal> weeklyCost) {
    this.totalDailyCost = totalDailyCost;
    this.totalMonthyCost = totalMonthyCost;
    this.totalWeeklyCost = totalWeeklyCost;
    this.weeklyCost = weeklyCost;
    this.timestamp = new Date();
  }
}
