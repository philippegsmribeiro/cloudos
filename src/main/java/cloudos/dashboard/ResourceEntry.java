package cloudos.dashboard;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/** Created by philipperibeiro on 6/23/17. */
@Document(collection = "dashboard_resource_entry")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ResourceEntry implements Serializable {

  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  private String id;

  private String month;
  // assign a default value to the month.
  private Double total = 0.0;
  private List<Resource> resources;
  private Date timestamp;

}
