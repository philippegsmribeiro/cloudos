package cloudos.dashboard;

import cloudos.Providers;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by philipperibeiro on 2/3/17. Define a cloud resource model, which represents how much
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Resource implements Serializable {

  private String name;
  // @TODO: refactor to BigDecimal instead of Double
  private Double amount;
  private Providers provider;

}
