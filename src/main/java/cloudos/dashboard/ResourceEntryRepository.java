package cloudos.dashboard;

import org.springframework.data.mongodb.repository.MongoRepository;

/** Created by philipperibeiro on 6/23/17. */
public interface ResourceEntryRepository extends MongoRepository<ResourceEntry, String> {

  /* Used to return the very last one added */
  ResourceEntry findFirstByOrderByTimestampDesc();
}
