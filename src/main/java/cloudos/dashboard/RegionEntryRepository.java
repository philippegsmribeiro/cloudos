package cloudos.dashboard;

import org.springframework.data.mongodb.repository.MongoRepository;

/** Created by philipperibeiro on 6/28/17. */
public interface RegionEntryRepository extends MongoRepository<RegionEntry, String> {

  /* Used to return the very last one added */
  RegionEntry findFirstByOrderByTimestampDesc();
}
