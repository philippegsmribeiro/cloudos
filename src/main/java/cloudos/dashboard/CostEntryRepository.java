package cloudos.dashboard;

import org.springframework.data.mongodb.repository.MongoRepository;

/** Created by philipperibeiro on 6/23/17. */
public interface CostEntryRepository extends MongoRepository<CostEntry, String> {

  /* Used to return the very last one added */
  CostEntry findFirstByOrderByTimestampDesc();
}
