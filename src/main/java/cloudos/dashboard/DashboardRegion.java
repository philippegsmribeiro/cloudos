package cloudos.dashboard;

import cloudos.Providers;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.io.Serializable;
import java.util.List;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Define all the cloud regions in the major providers. It contains the the number of instances, the
 * list of availability zones and whether it's available. It also contains the iso code and
 * coordinates so the region can be displayed in a map.
 *
 * @author philipperibeiro
 */
@Data
@Document(collection = "cloudos_regions")
public class DashboardRegion implements Serializable {

  @JsonSerialize(using = ToStringSerializer.class)
  private String name;

  private String city;
  private String region;
  private List<String> availabilityZones;
  private Integer numberAvailabilityZones;
  private String iso;
  private Double latitude;
  private Double longitude;
  private Integer instances;
  private Boolean available;
  private Providers providers;
  private Integer runningInstances;
  private Integer stoppedInstances;

  /**
   * Define a new region object.
   *
   * @param name the name of the region
   * @param city the closest city where the region is located
   * @param region the name of the region, abbreviated
   * @param availabilityZones a list of all the availability zones
   * @param numberAvailabilityZones define now many availability zones there are in the region
   * @param instances the number of instances in that region
   * @param providers the name of the provider
   * @param available whether the instance is already available
   * @param iso the iso code for the country the region is located
   * @param latitude the city's latitude
   * @param longitude the city's longitude
   */
  public DashboardRegion(
      String name,
      String city,
      String region,
      Integer numberAvailabilityZones,
      List<String> availabilityZones,
      Integer instances,
      Boolean available,
      String iso,
      Double latitude,
      Double longitude,
      Providers providers) {

    this.name = name;
    this.instances = instances;
    this.providers = providers;
    this.available = available;
    this.city = city;
    this.availabilityZones = availabilityZones;
    this.region = region;
    this.numberAvailabilityZones = numberAvailabilityZones;
    this.iso = iso;
    this.latitude = latitude;
    this.longitude = longitude;
  }

}
