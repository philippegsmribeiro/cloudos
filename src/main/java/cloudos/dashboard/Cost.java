package cloudos.dashboard;

import java.io.Serializable;
import lombok.Data;
import lombok.NoArgsConstructor;

/** Created by philipperibeiro on 2/3/17. */
@Data
@NoArgsConstructor
public class Cost implements Serializable {

  private String month;
  private Double cost = 0.0;
  private Double percentage = 0.0;
  private String message;

  /**
   * Constructor for the Cost class.
   *
   * @param cost The current monthly cost
   * @param month The current month
   */
  public Cost(Double cost, String month) {
    this.cost = cost;
    this.month = month;
    this.percentage = 0.0;
    this.message = "";
  }

}
