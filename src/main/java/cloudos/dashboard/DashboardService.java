package cloudos.dashboard;

import cloudos.Application;
import cloudos.Providers;
import cloudos.amazon.CloudWatchDimensions;
import cloudos.amazon.CloudWatchMetrics;
import cloudos.amazon.CloudWatchService;
import cloudos.costanalysis.GenericBillingReportRepository;
import cloudos.costanalysis.costsummary.CostSummary;
import cloudos.costanalysis.costsummary.CostSummaryService;
import cloudos.data.InstanceDataMetric;
import cloudos.data.InstanceDataUnit;
import cloudos.google.integration.GoogleMetricsIntegration;
import cloudos.instances.InstanceService;
import cloudos.models.CloudosDatapointAggregated;
import cloudos.models.CloudosDatapointAggregatedRepository;
import cloudos.models.Instance;
import cloudos.models.InstanceStatus;
import cloudos.models.costanalysis.GenericBillingReport;
import cloudos.resources.ResourcesUtils;
import cloudos.security.LogFilter;
import cloudos.utils.DateUtil;
import com.amazonaws.services.cloudwatch.model.Datapoint;
import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.monitoring.v3.Aggregation;
import com.google.monitoring.v3.TimeSeries;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.logging.log4j.ThreadContext;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

/**
 * Created by philipperibeiro on 2/3/17.
 * Define the DashboardService, which is responsible for handling tasks and activities with
 * regarding the Dashboard service.
 */
@Service
@Log4j2
public class DashboardService {

  private static final String AWS_AVERAGE = "Average";

  private static final Long WINDOW_24_HOURS = (long) (1000 * 60 * 60 * 24);

  private static final Integer PERIOD = 300;

  private static final Integer IGNORED_TIME_WINDOW = 60 * 15;

  /* Define the daily period as 1 day */
  public static final int DAILY_PERIOD = 1;

  /* Define the weekly period as 7 days */
  public static final int WEEKLY_PERIOD = 7;

  /* Define the monthly period as 30 days */
  public static final int MONTHLY_PERIOD = 30;

  /* Define the yearly period as 365 days */
  public static final int YEARLY_PERIOD = 365;

  private DashboardJob dashboardJob;

  private static Gson gson = new Gson();

  @Value("classpath:cloudos/regions.json")
  private org.springframework.core.io.Resource regionsResource;

  @Autowired
  private InstanceService instanceService;

  @Autowired
  private CloudWatchService cloudWatchService;

  @Autowired
  private RegionEntryRepository regionEntryRepository;

  @Autowired
  private GoogleMetricsIntegration googleMetricsIntegration;

  @Autowired
  private AsyncTaskExecutor taskExecutor;

  @Autowired
  private ResourceEntryRepository entryRepository;

  @Autowired
  private CostEntryRepository costEntryRepository;

  @Autowired
  private CostEstimatedEntryRepository costEstimatedEntryRepository;
  
  @Autowired
  private CostSummaryService costSummaryService;

  @Autowired
  private CloudosDatapointAggregatedRepository cloudosDatapointsRepository;

  @Autowired
  private DashboardRegionRepository dashboardRegionRepository;

  @Autowired
  private ResourceEntryRepository resourceEntryRepository;

  @Autowired
  private GenericBillingReportRepository genericBillingReportRepository;

  /**
   * Default constructor.
   *
   * <p>
   * Initializes non spring managed dependencies.
   * </p>
   */
  public DashboardService() {
    super();
    this.dashboardJob = new DashboardJob();
  }

  /**
   * DashboardService constructor, takes all the autowired repositories, used for test purposes.
   *
   * @param entryRepository - The ResourceEntry repository
   * @param costEntryRepository - The CostEntry repository
   */
  public DashboardService(ResourceEntryRepository entryRepository,
      CostEntryRepository costEntryRepository) {
    this();
    this.entryRepository = entryRepository;
    this.costEntryRepository = costEntryRepository;
  }

  /**
   * Get the current monthly cost for all the cloud infrastructure.
   *
   * @return Cost: The currently monthly cost
   */
  public CostEntry getCost() {
    return this.costEntryRepository.findFirstByOrderByTimestampDesc();
  }

  /**
   * Get cost estimated for all the cloud infrastructure.
   *
   * @return Cost Estimated: The currently cost estimated
   */
  public CostEstimatedEntry getCostEstimated() {
    return this.costEstimatedEntryRepository.findFirstByOrderByTimestampDesc();
  }
  
  /**
   * Get cost summary for all the cloud infrastructure.
   *
   * @return Cost Summary: The currently cost summary
   */
  public Map<String, CostSummary> getCostSummary() {
    Map<String, CostSummary> mapCostSummary = new HashMap<>();
    mapCostSummary.put("", this.costSummaryService.retrieveUpdatedCostSummary(null));
    mapCostSummary.put(Providers.AMAZON_AWS.name(),
        this.costSummaryService.retrieveUpdatedCostSummary(Providers.AMAZON_AWS));
    mapCostSummary.put(Providers.GOOGLE_COMPUTE_ENGINE.name(),
        this.costSummaryService.retrieveUpdatedCostSummary(Providers.GOOGLE_COMPUTE_ENGINE));
    return mapCostSummary;
  }

  /**
   * Get a list of all the instances currently running.
   *
   * @return List: A List of all the instances current running
   */
  public List<InstanceInfo> getInstances() {
    List<Instance> instances = this.instanceService.findAllActiveInstances();
    return instances.stream().map(this::toInstanceInfo).collect(Collectors.toList());
  }

  /**
   * Convert a CloudOS instance into a InstanceInfo object.
   *
   * @param instance An instance object.
   * @return InstanceInfo: a new InstanceInfo
   */
  private InstanceInfo toInstanceInfo(Instance instance) {
    return new InstanceInfo(instance.getMachineType(), instance.getName(), instance.getStatus(),
        instance.getResourceGroup(), instance.getRegion(), instance.getProvider(), 0.00);
  }

  /**
   * Get a list of all resources currently allocated.
   *
   * @return List: A list of resources
   */
  public ResourceEntry getResources() {
    return this.entryRepository.findFirstByOrderByTimestampDesc();
  }

  /**
   * Get providers return a list of all the current providers.
   *
   * @return List: A list of providers
   */
  public List<Provider> getProviders() {
    // @TODO: Don't fetch the Instances at every function call.
    List<Instance> instances = this.instanceService.findAllActiveInstances();

    Map<Providers, Integer> result = new HashMap<>();
    for (Instance instance : instances) {
      if (result.containsKey(instance.getProvider())) {
        Integer value = result.get(instance.getProvider());
        result.put(instance.getProvider(), value + 1);
      } else {
        if (instance.getProvider() != null) {
          result.put(instance.getProvider(), 1);
        }
      }
    }
    return result.entrySet().stream().map(x -> new Provider(x.getKey(), 0.0, x.getValue()))
        .collect(Collectors.toList());
  }

  /**
   * Get a list of the latest events.
   *
   * @return List: A list of events
   */
  public List<Event> getEvents() {
    List<Event> events = Arrays.asList(new Event("[14:00] VM1 CPU increased by 10%"),
        new Event("[05:00] VM2 CPu exceed by 21%, VM stopped"));
    return events;
  }

  /**
   * Return a RegionEntry of all the regions the user currently has offerings.
   *
   * @return RegionEntry: A list of regions
   */
  public RegionEntry getRegions() {
    RegionEntry findFirstByOrderByTimestampDesc =
        this.regionEntryRepository.findFirstByOrderByTimestampDesc();
    if (findFirstByOrderByTimestampDesc == null) {
      // force load if not loaded yet
      updateRegions();
      findFirstByOrderByTimestampDesc =
          this.regionEntryRepository.findFirstByOrderByTimestampDesc();
    }
    return findFirstByOrderByTimestampDesc;
  }

  /**
   * Gets the Average CPU usage of all the cloud providers' instances.
   *
   * @param providers - filter for specific provider - null for all
   * @return A map between the provider and a list of datapoints
   */
  @SuppressWarnings("Duplicates")
  public Map<Providers, List<CloudosDatapointAggregated>> getCloudCpuUsage(Providers providers) {
    Map<Providers, List<CloudosDatapointAggregated>> map = new HashMap<>();

    List<Instance> instances = retrieveListOfInstances(providers);

    Map<Providers, List<Instance>> instancesTable = this.getInstanceIds(instances);

    // loop over the map and for each cloud provider add the data points for
    // that provider.
    instancesTable.forEach((provider, instanceIds) -> {
      List<CloudosDatapointAggregated> datapoints = new ArrayList<>();
      if (provider.equals(Providers.AMAZON_AWS)) {
        datapoints.addAll(getAwsPoints(instanceIds, InstanceDataMetric.CPU_UTILIZATION));
      } else if (provider.equals(Providers.GOOGLE_COMPUTE_ENGINE)) {
        datapoints.addAll(getGooglePoints(instanceIds, InstanceDataMetric.CPU_UTILIZATION));
      }
      // @TODO: Add for Microsoft Azure
      map.put(provider, datapoints);
    });

    verifyEmptyReturn(providers, map);
    return map;
  }

  /**
   * Get GCloud datapoints.
   *
   * @param instances list
   * @param googleMetric metric
   * @return list of datagpoints
   */
  private List<CloudosDatapointAggregated> getGooglePoints(List<Instance> instances,
      InstanceDataMetric googleMetric) {
    List<CloudosDatapointAggregated> result = new ArrayList<>();

    List<String> instanceIds = getInstanceNames(instances);
    String instanceNames = Arrays.toString(instanceIds.toArray());

    final DateTime now = DateTime.now();
    long timeWindow = WINDOW_24_HOURS;

    // try to find on mongo
    final List<CloudosDatapointAggregated> savedData = retrieveSavedData(now,
        Providers.GOOGLE_COMPUTE_ENGINE, Arrays.asList(instanceNames), googleMetric);

    // if has saved data
    if (CollectionUtils.isNotEmpty(savedData)) {
      // get the new timeWindow for search on the provider
      timeWindow = analyseAndGetNewTimeWindow(now, Providers.GOOGLE_COMPUTE_ENGINE, savedData,
          result, timeWindow, instanceNames);
    }

    // if timeWindow is valid
    if (timeWindow != 0) {
      // go to the provider
      String measure = (String) ResourcesUtils
          .convertMetricToMeasure(Providers.GOOGLE_COMPUTE_ENGINE, googleMetric);
      Iterable<TimeSeries> points = googleMetricsIntegration.retrieveMetricTimeSeriesForInstance(
          instanceIds, measure, (timeWindow / 1000), PERIOD, Aggregation.Aligner.ALIGN_MEAN,
          Aggregation.Reducer.REDUCE_MEAN);
      List<CloudosDatapointAggregated> newData =
          ResourcesUtils.convertToCloudosDatapointAggregated(instanceNames,
              Lists.newArrayList(points), Providers.GOOGLE_COMPUTE_ENGINE, googleMetric, "");
      // if has new data
      if (CollectionUtils.isNotEmpty(newData)) {
        // save in another thread for performance
        String threadName = ThreadContext.get(LogFilter.LOG_PATH);
        taskExecutor.execute(new Runnable() {

          @Override
          public void run() {
            ThreadContext.put(LogFilter.LOG_PATH, threadName);
            log.info("saving datapoints: " + newData.size());
            cloudosDatapointsRepository.save(newData);
            log.info("datapoints saved: " + newData.size());
            ThreadContext.remove(LogFilter.LOG_PATH);
          }
        });
        // add the new data to the savedData
        result.addAll(newData);
      }
    }
    return ResourcesUtils.sortDataPoints(result);
  }

  /**
   * Search for saved data.
   */
  private List<CloudosDatapointAggregated> retrieveSavedData(DateTime now, Providers provider,
      List<String> instanceNames, InstanceDataMetric metric) {
    log.info("Searching on mongo: {} - {}", provider, instanceNames);
    List<CloudosDatapointAggregated> savedPoints = null;
    if (instanceNames.size() == 1) {
      // search using equals
      savedPoints =
          cloudosDatapointsRepository.findByInstanceIdAndProviderAndMetricAndTimestampBetween(
              instanceNames.get(0), provider, metric, now.minusHours(24).toDate(), now.toDate());
    } else {
      // search using in
      savedPoints =
          cloudosDatapointsRepository.findByInstanceIdInAndProviderAndMetricAndTimestampBetween(
              instanceNames, provider, metric, now.minusHours(24).toDate(), now.toDate());
    }
    log.info("Found {} data", savedPoints.size());
    return savedPoints;
  }

  /**
   * Retrieve a timewindow for searching on the provider Uses the saved data point.
   */
  private long analyseAndGetNewTimeWindow(DateTime now, Providers provider,
      List<CloudosDatapointAggregated> savedData, List<CloudosDatapointAggregated> result,
      long timeWindow, String instanceNames) {
    if (CollectionUtils.isNotEmpty(savedData)) {
      // sort by date
      Collections.sort(savedData, new Comparator<CloudosDatapointAggregated>() {
        @Override
        public int compare(CloudosDatapointAggregated o1, CloudosDatapointAggregated o2) {
          return o1.getTimestamp().compareTo(o2.getTimestamp());
        }
      });
      // get the last
      CloudosDatapointAggregated last = savedData.get(savedData.size() - 1);
      log.info("Last saved datapoint for {} - : {}", instanceNames, last.getTimestamp());

      // get the difference for the searching data
      DateTime lastTime = new DateTime(last.getTimestamp().getTime());
      DateTime minus = now.minus(lastTime.getMillis());
      log.info("Millis: {} - {}", minus.getMillis(), WINDOW_24_HOURS);
      if (minus.getMillis() < timeWindow) {
        // if last time inside the time window - set the new timewindow
        timeWindow = minus.getMillis();
        // add points to the result
        result.addAll(savedData);
        // verify if is needed to get new data
        if (timeWindow / 1000 < IGNORED_TIME_WINDOW) {
          // don't search if the last datapoint is in the ignored window
          timeWindow = 0;
        }
      }
    }
    return timeWindow;
  }

  /**
   * Get AWS datapoints.
   *
   * @param instances list
   * @param awsMetric metric
   * @return list of Datapoints
   */
  @SuppressWarnings("unchecked")
  private List<CloudosDatapointAggregated> getAwsPoints(List<Instance> instances,
      InstanceDataMetric awsMetric) {
    List<CloudosDatapointAggregated> result = new ArrayList<>();

    List<CloudosDatapointAggregated> cloudosDataPoints = new ArrayList<>();

    Map<String, List<Instance>> instanceRegions = getInstanceRegions(instances);
    List<String> names = new ArrayList<>();
    instanceRegions.forEach((region, regionInstances) -> {
      List<String> instanceIds = getInstanceNames(regionInstances);
      String instanceNames = Arrays.toString(instanceIds.toArray());
      names.add(instanceNames);
    });

    final DateTime now = DateTime.now();

    // try to find on mongo
    final List<CloudosDatapointAggregated> savedData =
        retrieveSavedData(now, Providers.AMAZON_AWS, names, awsMetric);

    CloudWatchMetrics measure =
        (CloudWatchMetrics) ResourcesUtils.convertMetricToMeasure(Providers.AMAZON_AWS, awsMetric);
    instanceRegions.forEach((region, regionInstances) -> {
      List<String> instanceIds = getInstanceNames(regionInstances);
      String instanceNames = Arrays.toString(instanceIds.toArray());

      long timeWindow = WINDOW_24_HOURS;

      // if has saved data
      if (CollectionUtils.isNotEmpty(savedData)) {
        // get the new timeWindow for search on the provider
        timeWindow = analyseAndGetNewTimeWindow(now, Providers.AMAZON_AWS, savedData.stream()
            .filter(dp -> dp.getInstanceId().equals(instanceNames)).collect(Collectors.toList()),
            result, timeWindow, instanceNames);
      }

      if (timeWindow != 0) {
        List<List<String>> partition = ListUtils.partition(instanceIds, 10);
        for (List<String> instanceList : partition) {
          List<Datapoint> points = cloudWatchService.getClient(region).getDatapoints(instanceList,
              CloudWatchDimensions.INSTANCE_ID, measure, Arrays.asList(AWS_AVERAGE), PERIOD,
              timeWindow);
          cloudosDataPoints.addAll(ResourcesUtils.convertToCloudosDatapointAggregated(instanceNames,
              points, Providers.AMAZON_AWS, awsMetric, region));
        }
      }
    });
    result.addAll(cloudosDataPoints);
    if (CollectionUtils.isNotEmpty(cloudosDataPoints)) {
      String threadName = ThreadContext.get(LogFilter.LOG_PATH);
      taskExecutor.execute(() -> {
        // save in another thread for performance
        ThreadContext.put(LogFilter.LOG_PATH, threadName);
        log.info("saving datapoints: " + cloudosDataPoints.size());
        cloudosDatapointsRepository.save(cloudosDataPoints);
        log.info("datapoints saved: " + cloudosDataPoints.size());
        ThreadContext.remove(LogFilter.LOG_PATH);
      });
      return this.aggregateDatapoints(getInstanceNames(instances), result, Providers.AMAZON_AWS,
          awsMetric);
    } else if (CollectionUtils.isNotEmpty(result)) {
      return this.aggregateDatapoints(getInstanceNames(instances), result, Providers.AMAZON_AWS,
          awsMetric);
    }
    return Collections.EMPTY_LIST;
  }

  /**
   * Create a map between the cloud providers and all the instance ids currently running.
   *
   * @param instances list
   * @return map provider list
   */
  private Map<Providers, List<Instance>> getInstanceIds(List<Instance> instances) {
    Map<Providers, List<Instance>> table = new HashMap<>();
    instances.stream().filter(instance ->
    // only collect the data points for running instances.
    instance != null && instance.getProvider() != null && instance.getProviderId() != null
        && instance.getStatus() == InstanceStatus.RUNNING).forEach(instance -> {
          if (!table.containsKey(instance.getProvider())) {
            table.put(instance.getProvider(), new ArrayList<>());
          }
          table.get(instance.getProvider()).add(instance);
        });
    return table;
  }

  /**
   * Get instance for regions.
   *
   * @param instances list
   * @return map region list
   */
  private Map<String, List<Instance>> getInstanceRegions(List<Instance> instances) {
    Map<String, List<Instance>> table = new HashMap<>();
    instances.forEach(instance -> {
      if (!table.containsKey(instance.getRegion())) {
        table.put(instance.getRegion(), new ArrayList<>());
      }
      table.get(instance.getRegion()).add(instance);
    });
    return table;
  }

  /**
   * Get the instance names.
   *
   * @param instances list
   * @return list of names
   */
  private static List<String> getInstanceNames(List<Instance> instances) {
    return instances.stream().filter(Objects::nonNull).map(t -> t.getProviderId()).distinct()
        .sorted().collect(Collectors.toList());
  }

  /**
   * Obtain the average of all the CPU usage data points for AWS instances.
   *
   * @param instanceIds: A list containing all the instances id.
   * @return List: A averaged list of all the data points.
   */
  private List<CloudosDatapointAggregated> aggregateDatapoints(List<String> instanceIds,
      List<CloudosDatapointAggregated> datapoints, Providers provider, InstanceDataMetric metric) {
    log.info("Aggregating... {}", datapoints.size());
    List<CloudosDatapointAggregated> cloudosDatapoints = new ArrayList<>();
    /* assert both elements are greater than 0 */
    if (CollectionUtils.isNotEmpty(instanceIds) && CollectionUtils.isNotEmpty(datapoints)) {

      // TODO: verify if there is a optmized way to do that
      Map<Date, List<CloudosDatapointAggregated>> mapTimestamp = new HashMap<>();
      for (CloudosDatapointAggregated cloudosDatapoint : datapoints) {
        final Date key = cloudosDatapoint.getTimestamp();
        if (!mapTimestamp.containsKey(key)) {
          mapTimestamp.put(key, new ArrayList<>());
        }
        mapTimestamp.get(key).add(cloudosDatapoint);
      }

      final InstanceDataUnit dataUnit = ResourcesUtils.getDataUnit(metric);

      for (Entry<Date, List<CloudosDatapointAggregated>> entry : mapTimestamp.entrySet()) {
        final List<CloudosDatapointAggregated> value = entry.getValue();
        BigDecimal sum = BigDecimal.ZERO;
        int count = 0;
        String instanceNames = "";
        for (CloudosDatapointAggregated cloudosDatapoint : value) {
          sum = sum.add(new BigDecimal(cloudosDatapoint.getValue()));
          count += cloudosDatapoint.getInstanceId().split(",").length;
          instanceNames += cloudosDatapoint.getInstanceId();
        }
        BigDecimal average = sum.divide(new BigDecimal(count), RoundingMode.CEILING);
        average.setScale(3, RoundingMode.CEILING);
        CloudosDatapointAggregated current = new CloudosDatapointAggregated(provider, instanceNames,
            "", metric, entry.getKey(), average.doubleValue(), dataUnit);
        cloudosDatapoints.add(current);
      }
    }
    log.info("Finishing aggregating... {}", cloudosDatapoints.size());
    return ResourcesUtils.sortDataPoints(cloudosDatapoints);
  }

  /**
   * Gets the Average Disk usage of all the cloud providers' instances.
   *
   * @param providers - filter for specific provider - null for all
   * @return A map between the provider and a list of datapoints
   */
  @SuppressWarnings("Duplicates")
  public Map<Providers, List<CloudosDatapointAggregated>> getCloudDiskUsage(Providers providers) {
    Map<Providers, List<CloudosDatapointAggregated>> map = new HashMap<>();

    List<Instance> instances = retrieveListOfInstances(providers);

    Map<Providers, List<Instance>> instancesTable = this.getInstanceIds(instances);

    // loop over the map and for each cloud provider add the data points for
    // that provider.
    instancesTable.forEach((provider, instanceIds) -> {
      List<CloudosDatapointAggregated> datapoints = new ArrayList<>();
      if (provider.equals(Providers.AMAZON_AWS)) {
        datapoints.addAll(getAwsPoints(instanceIds, InstanceDataMetric.DISK_READ_BYTES));
        datapoints.addAll(getAwsPoints(instanceIds, InstanceDataMetric.DISK_WRITE_BYTES));
      } else if (provider.equals(Providers.GOOGLE_COMPUTE_ENGINE)) {
        datapoints.addAll(getGooglePoints(instanceIds, InstanceDataMetric.DISK_READ_BYTES));
        datapoints.addAll(getGooglePoints(instanceIds, InstanceDataMetric.DISK_WRITE_BYTES));
      }
      // @TODO: Add for Microsoft Azure
      map.put(provider, datapoints);
    });

    verifyEmptyReturn(providers, map);
    return map;
  }

  /**
   * Gets the Average Memory usage of all the cloud providers' instances.
   *
   * @param providers - filter for specific provider - null for all
   * @return A map between the provider and a lis of datapoints
   */
  public Map<Providers, List<CloudosDatapointAggregated>> getCloudMemoryUsage(Providers providers) {

    Map<Providers, List<CloudosDatapointAggregated>> map = new HashMap<>();

    retrieveListOfInstances(providers);

    verifyEmptyReturn(providers, map);
    return map;
  }

  /**
   * Retrieve a list of instances for using on the datapoints.
   *
   * @param providers filter
   * @return list of Instances
   */
  private List<Instance> retrieveListOfInstances(Providers providers) {
    List<Instance> instances = null;
    if (providers != null) {
      instances = this.instanceService.findAllActiveInstancesByProvider(providers);
    } else {
      instances = this.instanceService.findAllActiveInstances();
    }
    return instances;
  }

  /**
   * Gets the Average network usage of all the cloud providers' instances.
   *
   * @param providers - filter for specific provider - null for all
   * @return A map between the provider and a list of datapoints
   */
  @SuppressWarnings("Duplicates")
  public Map<Providers, List<CloudosDatapointAggregated>> getCloudNetworkUsage(
      Providers providers) {
    Map<Providers, List<CloudosDatapointAggregated>> map = new HashMap<>();

    List<Instance> instances = retrieveListOfInstances(providers);

    Map<Providers, List<Instance>> instancesTable = this.getInstanceIds(instances);

    // loop over the map and for each cloud provider add the data points for
    // that provider.
    instancesTable.forEach((provider, instanceIds) -> {
      List<CloudosDatapointAggregated> datapoints = new ArrayList<>();
      if (provider.equals(Providers.AMAZON_AWS)) {
        datapoints.addAll(getAwsPoints(instanceIds, InstanceDataMetric.NETWORK_RECEIVED_BYTES));
        datapoints.addAll(getAwsPoints(instanceIds, InstanceDataMetric.NETWORK_SENT_BYTES));
      } else if (provider.equals(Providers.GOOGLE_COMPUTE_ENGINE)) {
        datapoints.addAll(getGooglePoints(instanceIds, InstanceDataMetric.NETWORK_RECEIVED_BYTES));
        datapoints.addAll(getGooglePoints(instanceIds, InstanceDataMetric.NETWORK_SENT_BYTES));
      }
      // @TODO: Add for Microsoft Azure
      map.put(provider, datapoints);
    });

    verifyEmptyReturn(providers, map);
    return map;
  }

  /**
   * Verify if the return map and set a empty (zero metric) for the provider.
   * TODO: it is not really needed, it is just for produced a empty chart for now - it might be or
   * not removed later
   */
  private void verifyEmptyReturn(Providers providers,
      Map<Providers, List<CloudosDatapointAggregated>> map) {
    if (providers != null && !map.containsKey(providers)) {
      // for not returning null
      map.put(providers, Arrays.asList(
          new CloudosDatapointAggregated(providers, "", "none", null, new Date(), 0.0, null)));
    }
  }

  /**
   * Method that calculates the Cost Estimated week/month/year.
   *
   * @param costEntry the cost information.
   * @param totalYearlyCost total cost of the year.
   * @return CostEstimatedEntry The entity with estimated costs completed.
   */
  public CostEstimatedEntry calculateCostEstimatedEntry(CostEntry costEntry, Cost totalYearlyCost) {
    CostEstimatedEntry costEstimatedEntry = new CostEstimatedEntry();
    Double costEstimatedForDayYear = totalYearlyCost.getCost() / YEARLY_PERIOD;
    Double costEstimatedForDayMonth = costEntry.getTotalMonthyCost().getCost() / MONTHLY_PERIOD;
    Double costEstimatedForDayWeekly = costEntry.getTotalWeeklyCost().getCost() / WEEKLY_PERIOD;

    Double mediaCostEstimatedDay =
        (costEstimatedForDayYear + costEstimatedForDayMonth + costEstimatedForDayWeekly) / 3;

    log.info("Cost information per year");
    costEstimatedEntry.setYearlyCost(totalYearlyCost.getCost());
    costEstimatedEntry.setYearlyEstimatedCost(mediaCostEstimatedDay * YEARLY_PERIOD);

    log.info("Cost information per month");
    costEstimatedEntry.setMonthlyCost(costEntry.getTotalMonthyCost().getCost());
    costEstimatedEntry.setMonthyPercentage(costEntry.getTotalMonthyCost().getPercentage());
    costEstimatedEntry.setMonthlyEstimatedCost(mediaCostEstimatedDay * MONTHLY_PERIOD);
    costEstimatedEntry
        .setMonthlyEstimatedPercentage((costEstimatedEntry.getMonthlyEstimatedCost() * 100)
            / costEntry.getTotalMonthyCost().getCost());

    log.info("Cost information per week");
    costEstimatedEntry.setWeeklyCost(costEntry.getTotalWeeklyCost().getCost());
    costEstimatedEntry.setWeeklyPercentage(costEntry.getTotalWeeklyCost().getPercentage());
    costEstimatedEntry.setWeeklyEstimatedCost(mediaCostEstimatedDay * WEEKLY_PERIOD);
    costEstimatedEntry
        .setWeeklyEstimatedPercentage((costEstimatedEntry.getWeeklyEstimatedCost() * 100)
            / costEntry.getTotalWeeklyCost().getCost());

    return costEstimatedEntry;
  }

  /**
   * Updates the regions list.
   */
  public void updateRegions() {
    // only fetch the dashboardRegions which are available.
    List<DashboardRegion> dashboardRegions =
        this.dashboardRegionRepository.getRegionByAvailable(true);

    List<Instance> instances = this.instanceService.findAllActiveAndNotTerminatedInstances();

    if (org.apache.commons.collections4.CollectionUtils.isEmpty(dashboardRegions)) {
      dashboardRegions = loadRegions();
      log.debug("DashboardRegion: {}", dashboardRegions);
      if (!org.apache.commons.collections4.CollectionUtils.isEmpty(dashboardRegions)) {
        dashboardRegionRepository.save(dashboardRegions);
      }
    }

    if (org.apache.commons.collections4.CollectionUtils.isEmpty(instances)) {
      log.warn("There are no instances or regions to be processed");
      return;
    }

    // aggregate the instances by region.
    List<DashboardRegion> regions = dashboardJob.getInstancesByRegion(instances, dashboardRegions);
    // only return the regions that have instances
    List<DashboardRegion> filtered = regions.stream()
        .filter(region -> region != null && region.getInstances() > 0 && region.getAvailable())
        .collect(Collectors.toList());

    // store the new entry in the collection.
    RegionEntry regionEntry = new RegionEntry(filtered);
    log.info("Inserting the region entry {}", regionEntry);
    this.regionEntryRepository.save(regionEntry);
  }

  /**
   * Read the regions.json file and load the MongoDB's cloudos_regions collection with the each
   * region provided.
   *
   * @return List DashboardRegion A list of DashboardRegions.
   */
  private List<DashboardRegion> loadRegions() {

    final Type regionType = new TypeToken<List<DashboardRegion>>() {}.getType();
    List<DashboardRegion> dashboardRegions;
    try {
      // Create JsonReader from Json.
      JsonReader reader = new JsonReader(new FileReader(regionsResource.getFile()));
      dashboardRegions = gson.fromJson(reader, regionType);
      // save the regions to the repository
      return dashboardRegions;
    } catch (IOException e) {
      log.error(e.getMessage());
      return new ArrayList<>();
    }
  }

  /**
   * Update the resources table from the current date to the beginning of the month.
   */
  public void updateResources() {
    List<GenericBillingReport> reports = this.getReportsForTheCurrentMonth();
    List<Resource> resources = this.dashboardJob.getResources(reports);
    // no null resources allowed
    if (resources == null) {
      log.warn("No resources available, nothing to do.");
      return;
    }

    // sort the resource ...
    resources.sort(Comparator.comparing(Resource::getAmount).reversed());

    // get the current month.
    Calendar cal = Calendar.getInstance();
    String month = cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Application.LOCALE_DEFAULT);
    double total = resources.stream().mapToDouble(Resource::getAmount).sum();
    ResourceEntry resourceEntry = ResourceEntry.builder()
                                    .resources(resources)
                                    .month(month)
                                    .total(total)
                                    .build();
    log.info("Inserting the resource entry {}", resourceEntry.toString());
    this.resourceEntryRepository.save(resourceEntry);
  }

  /**
   * Get the all the billings reports for the current month.
   *
   * @return List GenericBillingReport A List of reports.
   */
  public List<GenericBillingReport> getReportsForTheCurrentMonth() {

    // get the range from today to the beginning of the month
    Pair<Date, Date> pair = DateUtil.getDateRange();

    // get all the billings reports between the start and current day of the month.
    // we are going to use the same reports to calculate different metrics.

    return genericBillingReportRepository
        .findByUsageStartTimeBetween(pair.getFirst(), pair.getSecond());
  }

  /**
   * Find regions (descriptions) by providers.
   * @param provider to filter 
   * @return list of DashboardRegion
   */
  public List<DashboardRegion> findRegionsByProviders(Providers provider) {
    List<DashboardRegion> findByProviders = dashboardRegionRepository.findByProviders(provider);
    if (CollectionUtils.isEmpty(findByProviders)) {
      updateRegions();
      findByProviders = dashboardRegionRepository.findByProviders(provider);
    }
    return findByProviders;
  }

  /**
   * Update (create a newest cost summary) based on the billing reports.
   * It should be called only on scheduling (monitors) as it takes a while to process.
   */
  public void updateCostSummary() {
    // all
    this.costSummaryService.updateCostSummary(null);
    // aws
    this.costSummaryService.updateCostSummary(Providers.AMAZON_AWS);
    // google
    this.costSummaryService.updateCostSummary(Providers.GOOGLE_COMPUTE_ENGINE);
  }

  
}
