package cloudos.dashboard;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Define a model that holds the cost estimated information. Created by Alex Calagua on 10/26/17
 */
@Document(collection = "dashboard_cost_estimated_entry")
@Data
@NoArgsConstructor
public class CostEstimatedEntry implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @JsonSerialize(using = ToStringSerializer.class)
  private String id;
  private Double weeklyPercentage = 0.0;
  private Double monthyPercentage = 0.0;
  private Double monthlyCost = 0.0;
  private Double weeklyCost = 0.0;
  private Double yearlyCost = 0.0;
  private Double weeklyEstimatedCost = 0.0;
  private Double monthlyEstimatedCost = 0.0;
  private Double yearlyEstimatedCost = 0.0;
  private Double weeklyEstimatedPercentage = 0.0;
  private Double monthlyEstimatedPercentage = 0.0;

  @DateTimeFormat(pattern = "dd-MM-yyyy")
  private Date timestamp;

}
