package cloudos.dashboard;

import static cloudos.billings.BillingsConstants.MATH_CONTEXT;

import cloudos.Providers;
import cloudos.billings.BillingReportUtils;
import cloudos.models.Instance;
import cloudos.models.InstanceStatus;
import cloudos.models.SparkJob;
import cloudos.models.costanalysis.GenericBillingReport;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import lombok.extern.log4j.Log4j2;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.Optional;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

/**
 * Created by philipperibeiro on 6/23/17.
 *
 * <p>The DashboardJob is designed to apply a Spark aggregator in order to calculate dashboard
 * related costs, such as total cost, average daily and weekly cost.
 */
@Log4j2
public class DashboardJob extends SparkJob {

  public static final String DAILY_COST = "DAILY COST";
  public static final String WEEKLY_COST = "WEEKLY COST";
  public static final String MONTHLY_COST = "MONTHLY COST";
  public static final String YEARLY_COST = "YEARLY COST";

  /**
   * Default constructor the DashboardJob class.
   */
  public DashboardJob() {
    super();
    log.info("Initializing dashboard job");
  }

  /**
   * Assign the JavaSparkContext to the context.
   *
   * @param context A JavaSparkContext context
   */
  public DashboardJob(JavaSparkContext context) {
    super(context);
    log.info("Initializing dashboard job using provided java spark context");
  }

  /**
   * Get the total monthly cost for the cloud operation, up to the current date.
   *
   * @param reports A List of Billing Reports
   * @param month The current month
   * @return The current monthly cost
   */
  public Cost getCost(@NotNull List<GenericBillingReport> reports, final String month) {
    // check if reports list is empty
    if (CollectionUtils.isEmpty(reports)) {
      return new Cost(BigDecimal.ZERO.doubleValue(), month);
    }

    List<BigDecimal> usageAmount =
        reports.stream().map(GenericBillingReport::getUsageCost).collect(Collectors.toList());
    // produce an RDD from the usage amount
    JavaRDD<BigDecimal> rdd = context.parallelize(usageAmount);
    // Reduce Function for cumulative sum
    Function2<BigDecimal, BigDecimal, BigDecimal> reduceSumFunc = (accum, n) -> accum
        .add(n, MATH_CONTEXT);

    BigDecimal cost = rdd.reduce(reduceSumFunc);

    return new Cost(cost.doubleValue(), month);
  }

  /**
   * Get today's cost.
   *
   * @param reports A List of Billing Reports
   * @return The current daily cost
   */
  public Cost getDailyCost(@NotNull List<GenericBillingReport> reports) {
    return this.getCost(reports, new Date().toString());
  }

  /**
   * Return the total cost of the past week.
   *
   * @param reports A List of Billing Reports
   * @return The past weekly cost
   */
  public Cost getTotalWeeklyCost(@NotNull List<GenericBillingReport> reports) {
    return this.getCost(reports, WEEKLY_COST);
  }

  /**
   * Return the total cost of the past week, by the day.
   *
   * @param reports A List of Billing Reports
   * @return The past weekly cost, by the day
   */
  public List<BigDecimal> getWeeklyCost(@NotNull List<GenericBillingReport> reports,
      @NotNull List<Date> week) {
    try {

      // first, build a map between the date and the cost, 0, for now
      if (CollectionUtils.isEmpty(reports)) {
        return Arrays.asList(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO,
            BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO);
      }

      // map billing -> (date, amount)
      // aggregate (date, amount) -> (date, amount)
      // get a list of resources
      JavaRDD<GenericBillingReport> rdd = context.parallelize(reports);

      // @TODO: Fix this map reduce ... we need to join in the week and by the day.
      // now build a map between resourceId and usage amount
      JavaPairRDD<Date, BigDecimal> result = rdd
          .mapToPair(
              (PairFunction<GenericBillingReport, Date, BigDecimal>) x -> new Tuple2<>(
                  x.getUsageStartTime(),
                  x.getUsageCost()))
          .reduceByKey(
              (Function2<BigDecimal, BigDecimal, BigDecimal>) (x, y) -> x.add(y, MATH_CONTEXT));

      Map<Date, BigDecimal> sortedMap = new TreeMap<>(result.collectAsMap());
      Map<Date, BigDecimal> weeklyCost = new TreeMap<>();
      Date current = null;
      // merge the dates in the same day.
      for (Map.Entry<Date, BigDecimal> entry : sortedMap.entrySet()) {
        if (current == null) {
          current = entry.getKey();
          weeklyCost.put(entry.getKey(), entry.getValue());
        } else if (DateUtils.isSameDay(current, entry.getKey())) {
          BigDecimal total = weeklyCost.get(current).add(entry.getValue());
          weeklyCost.put(current, total);
        } else {
          current = entry.getKey();
          weeklyCost.put(entry.getKey(), entry.getValue());
        }
      }
      // convert to a Resource type
      return new ArrayList<>(weeklyCost.values());
    } catch (Exception e) {
      // TODO: handle exception
      log.error("Spark error", e);
    }
    return Collections.EMPTY_LIST;
  }

  /**
   * Return the total cost of the past month.
   *
   * @param reports A List of Billing Reports
   * @return The past monthly cost
   */
  public Cost getTotalMonthlyCost(@NotNull List<GenericBillingReport> reports) {
    return this.getCost(reports, MONTHLY_COST);
  }

  /**
   * Return the total cost of the past year.
   *
   * @param reports A List of Billing Reports
   * @return The past yearly cost
   */
  public Cost getTotalYearlyCost(@NotNull List<GenericBillingReport> reports) {
    return this.getCost(reports, YEARLY_COST);
  }

  /**
   * Aggregate all the instances currently available by the region they belong to.
   *
   * @param instances A list of all the current existing instances.
   * @param dashboardRegions A list of all the regions available for all the cloud providers.
   * @return An aggregated DashboardRegion list.
   */
  public List<DashboardRegion> getInstancesByRegion(
      @NotNull List<Instance> instances, @NotNull List<DashboardRegion> dashboardRegions) {

    // check for empty lists
    if (CollectionUtils.isEmpty(instances) || CollectionUtils.isEmpty(dashboardRegions)) {
      return new ArrayList<>();
    }

    // build a map for the dashboard regions
    Map<Tuple2<String, String>, DashboardRegion> regionMap = new HashMap<>();
    for (DashboardRegion region : dashboardRegions) {
      // the key here is a simple pair
      Tuple2<String, String> pair = new Tuple2<>(region.getRegion(), region.getProviders().name());
      regionMap.put(pair, region);
    }

    // get a list of instances
    JavaRDD<Instance> rdd =
        context
            .parallelize(instances)
            .filter(
                (Function<Instance, Boolean>) instance -> instance != null
                    && instance.getRegion() != null && instance.getProvider() != null);
    JavaRDD<DashboardRegion> regionRdd = context.parallelize(dashboardRegions);

    // map the instances to a pair of <region, provider> -> count
    // reduce and sum the count
    // map the dashboard regions by <region, provider> -> count
    // reduce by the <region, provider> -> <runningCount, stoppedCount>
    // now build a map between resourceId and usage amount

    final List<InstanceStatus> runningStates = Arrays
        .asList(InstanceStatus.RUNNING, InstanceStatus.PROVISIONING);

    JavaPairRDD<Tuple2<String, String>, Tuple2<Integer, Integer>> left =
        rdd.mapToPair(
            (PairFunction<Instance, Tuple2<String, String>, Tuple2<Integer, Integer>>) x -> {
              return new Tuple2<>(
                  new Tuple2<>(x.getRegion(), x.getProvider().name()),
                  new Tuple2<>(
                      runningStates.contains(x.getStatus()) ? 1 : 0,
                      !runningStates.contains(x.getStatus()) ? 1 : 0
                  )
              );
            }
        ).reduceByKey((Function2<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>,
            Tuple2<Integer, Integer>>) (x, y) -> new Tuple2<>(
            x._1() + y._1(),
            x._2() + y._2()));

    // map the dashboard region to its amount
    JavaPairRDD<Tuple2<String, String>, DashboardRegion> right =
        regionRdd.mapToPair(
            (PairFunction<DashboardRegion, Tuple2<String, String>, DashboardRegion>) x ->
                new Tuple2<>(
                    new Tuple2<>(x.getRegion(), x.getProviders().name()), x));

    JavaPairRDD<Tuple2<String, String>, Tuple2<Tuple2<Integer, Integer>, Optional<DashboardRegion>>>
        result = left.leftOuterJoin(right);

    result
        .collect()
        .forEach(
            res -> {
              log.info("Key value {} has value {}", res._1(), res._2()._1());
              if (regionMap.containsKey(res._1())) {
                DashboardRegion region = regionMap.get(res._1());
                region.setInstances(res._2._1()._1() + res._2._1()._2());
                region.setRunningInstances(res._2._1()._1());
                region.setStoppedInstances(res._2._1()._2());
                regionMap.put(res._1(), region);
              }
            });

    return new ArrayList<>(regionMap.values())
        .stream()
        .filter(region -> region.getInstances() > 0)
        .collect(Collectors.toList());
  }

  /**
   * Return an aggregated list of resources
   *
   * @param reports A list of billings reports
   * @return A list of resources.
   */
  public List<Resource> getResources(@NotNull List<GenericBillingReport> reports) {
    // check for empty lists
    if (CollectionUtils.isEmpty(reports)) {
      log.info("No billing reports to process, returning empty result");
      return new ArrayList<>();
    }

    // group billing reports by Provider
    Map<Providers, List<GenericBillingReport>> groupedReports = BillingReportUtils
        .groupBillingReportsByProvider(reports);

    // iterate the reports in order to create Resource instances
    final Set<Map.Entry<Providers, List<GenericBillingReport>>> entries =
        new HashSet<>(groupedReports.entrySet());
    final List<Resource> resources = new ArrayList<>();

    entries.forEach((final Map.Entry<Providers, List<GenericBillingReport>> reportsByProvider) -> {
      try {
        final Providers thisProvider = reportsByProvider.getKey();

        // get a list of resources
        JavaRDD<GenericBillingReport> rdd = context.parallelize(reportsByProvider.getValue());

        // now build a map between resourceId and usage amount
        JavaPairRDD<String, BigDecimal> result = rdd
            .mapToPair((PairFunction<GenericBillingReport, String, BigDecimal>) x -> new Tuple2<>(
                x.getResourceId(), x.getUsageCost())).reduceByKey(
                (Function2<BigDecimal, BigDecimal, BigDecimal>) (x, y) -> x.add(y,
                    MATH_CONTEXT));

        // convert to a Resource type
        JavaRDD<Resource> map = result
            .map((Tuple2<String, BigDecimal> item) -> new Resource(item._1(),
                item._2().doubleValue(), thisProvider));

        resources.addAll(map.collect());
      } catch (Exception e) {
        log.error("error", e);
      }
    });

    return resources;
  }
}
