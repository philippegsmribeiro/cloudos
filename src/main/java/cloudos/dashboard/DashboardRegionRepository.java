package cloudos.dashboard;

import cloudos.Providers;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

/** Created by philipperibeiro on 4/22/17. */
interface DashboardRegionRepository extends MongoRepository<DashboardRegion, String> {

  /* Get all the regions based on the availability */
  List<DashboardRegion> getRegionByAvailable(boolean isAvailable);

  List<DashboardRegion> getRegionByAvailable(boolean isAvailable, Pageable pageable);

  List<DashboardRegion> getRegionByAvailableAndInstancesGreaterThan(
      boolean isAvailable, Integer instances);

  List<DashboardRegion> getRegionByName(String name);

  public List<DashboardRegion> findByProviders(Providers providers);

  List<DashboardRegion> getRegionByRegion(String region);
}
