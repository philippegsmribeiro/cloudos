package cloudos.instances.spotinstances;

import cloudos.instances.InstanceRequestException;
import cloudos.models.AbstractInstance;
import cloudos.models.CloudActionRequest;
import cloudos.models.CloudActionResponse;
import cloudos.models.spotinstances.CloudSpotCancelRequest;
import cloudos.models.spotinstances.CloudSpotCreateRequest;
import cloudos.models.spotinstances.CloudSpotDescribeRequest;
import cloudos.models.spotinstances.CloudSpotPricingRequest;

import com.amazonaws.services.ec2.model.Instance;

import java.util.List;

/**
 * Define the type that will manage spot instances between the major cloud providers.
 *
 * <p>
 * We define the major actions to be taken, such as managing instances, fetching the current price,
 * checking the status of requests, etc.
 * </p>
 *
 * @param <T> The type of the instance being created
 */
public interface SpotInstanceManager<T extends AbstractInstance> {

  /**
   * Create an instance on the provider and save it on mongo.
   *
   * @return mongo instance
   */
  List<T> createInstances(CloudSpotCreateRequest request) throws InstanceRequestException;

  /**
   * Fetch a list of instances on the provider and save it on mongo.
   *
   * @param request the CloudActionRequest
   * @return list of mongo instance
   */
  List<?> pricing(CloudSpotPricingRequest request);

  /**
   * Cancel an existing spot request, thus avoiding the request to be completed.
   *
   * @param request the request to cancel the spot request
   * @return whether the spot request has been canceled or not
   */
  boolean cancelSpotRequest(CloudSpotCancelRequest request);

  /**
   * Terminate an instance.
   *
   * @param request the action request
   * @return a response
   */
  List<CloudActionResponse<T>> terminate(CloudActionRequest request)
      throws InstanceRequestException;

  /**
   * Check the status of a spot request.
   *
   * @param request the CloudActionRequest
   */
  List<Instance> describeRequest(CloudSpotDescribeRequest request);
}
