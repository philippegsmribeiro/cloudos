package cloudos.instances.spotinstances;

import cloudos.Providers;
import cloudos.exceptions.NotImplementedException;
import cloudos.instances.InstanceRequestException;
import cloudos.models.AbstractInstance;
import cloudos.models.CloudActionRequest;
import cloudos.models.CloudActionResponse;
import cloudos.models.spotinstances.CloudSpotCancelRequest;
import cloudos.models.spotinstances.CloudSpotCreateRequest;
import cloudos.models.spotinstances.CloudSpotDescribeRequest;
import cloudos.models.spotinstances.CloudSpotPricingRequest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CloudosSpotInstanceManager {

  @Autowired
  private AmazonSpotInstanceManager amazonSpotInstanceManager;

  @Autowired
  private GoogleSpotInstanceManager googleSpotInstanceManager;

  /**
   * Create instances allow the user to submit spot requests and create spot instances.
   *
   * @param request A request to create spot instances
   * @return a list of instances created
   * @throws InstanceRequestException any exception when creating the instances
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  public List<? extends AbstractInstance> createInstances(CloudSpotCreateRequest request)
      throws InstanceRequestException, NotImplementedException {
    // get the SpotInstanceManager depending on the provider
    SpotInstanceManager<?> spotInstanceManager = getManagement(request.getProvider());
    return spotInstanceManager.createInstances(request);
  }

  /**
   * Send a cancel spot request, so the request is canceled and not fulfilled.
   *
   * @param request a request to cancel spot request
   * @return if the spot cancel request was successful
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  public boolean cancelSpotRequest(CloudSpotCancelRequest request) throws NotImplementedException {
    // get the SpotInstanceManager depending on the provider
    SpotInstanceManager<?> spotInstanceManager = getManagement(request.getProvider());
    return spotInstanceManager.cancelSpotRequest(request);
  }

  /**
   * Terminate spot instances.
   *
   * @param request a request with the action to terminate the instance
   * @return a list of instances terminated
   * @throws InstanceRequestException any exception when creating the instances
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  public List<? extends CloudActionResponse<?>> terminate(CloudActionRequest request)
      throws NotImplementedException, InstanceRequestException {
    // get the SpotInstanceManager depending on the provider
    SpotInstanceManager<?> spotInstanceManager = getManagement(request.getProvider());
    return spotInstanceManager.terminate(request);
  }

  /**
   * Describe the current status of the instances for a spot request.
   *
   * @param request the request to describe the spot request
   * @return a list of instances
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  public List<?> describeInstances(CloudSpotDescribeRequest request)
      throws NotImplementedException {
    // get the SpotInstanceManager depending on the provider
    SpotInstanceManager<?> spotInstanceManager = getManagement(request.getProvider());
    return spotInstanceManager.describeRequest(request);
  }

  /**
   * Get the pricing history (or the latest pricing) of a particular spot instance resource.
   *
   * @param request a spot pricing request
   * @return a list of spot prices
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  public List<?> pricing(CloudSpotPricingRequest request) throws NotImplementedException {
    // get the SpotInstanceManager depending on the provider
    SpotInstanceManager<?> spotInstanceManager = getManagement(request.getProvider());
    return spotInstanceManager.pricing(request);
  }

  /**
   * Get the autoscaler manager based on the provider.
   *
   * @param providers the name of the provider
   * @return a provider manager
   * @throws NotImplementedException if the manager hasn't been implement yet for the provider
   */
  @SuppressWarnings("Duplicates")
  private SpotInstanceManager getManagement(Providers providers) throws NotImplementedException {
    switch (providers) {
      case AMAZON_AWS:
        return amazonSpotInstanceManager;
      case GOOGLE_COMPUTE_ENGINE:
        return googleSpotInstanceManager;
      default:
        break;
    }
    throw new NotImplementedException();
  }
}
