package cloudos.instances.spotinstances;

import cloudos.google.GoogleInstance;
import cloudos.instances.GoogleInstanceManagement;
import cloudos.instances.InstanceRequestException;
import cloudos.models.CloudActionRequest;
import cloudos.models.CloudActionResponse;
import cloudos.models.spotinstances.CloudSpotCancelRequest;
import cloudos.models.spotinstances.CloudSpotCreateRequest;
import cloudos.models.spotinstances.CloudSpotDescribeRequest;
import cloudos.models.spotinstances.CloudSpotPricingRequest;
import cloudos.pricing.GooglePricingManagement;
import cloudos.pricing.PricingValue;

import com.amazonaws.services.ec2.model.Instance;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GoogleSpotInstanceManager implements SpotInstanceManager<GoogleInstance> {
  
  @Autowired
  GooglePricingManagement googlePricingManagement;
  @Autowired
  GoogleInstanceManagement googleInstanceManagement;
  
  @Override
  public List<GoogleInstance> createInstances(CloudSpotCreateRequest request)
      throws InstanceRequestException {
    return googleInstanceManagement.createInstances(request);
  }

  
  @Override
  public List<?> pricing(CloudSpotPricingRequest request) {
    List<String> instanceTypes = request.getInstanceTypes();
    List<PricingValue> pricing = new ArrayList<>();
    for (String string : instanceTypes) {
      PricingValue pricingForInstanceType =
          googlePricingManagement.getPricingForInstanceType(string, request.getRegion(), true,
              null);
      if (pricingForInstanceType != null) {
        pricing.add(pricingForInstanceType);
      }
    }
    return pricing;
  }
  
  @Override
  public boolean cancelSpotRequest(CloudSpotCancelRequest request) {
    return false;
  }
  
  @Override
  public List<CloudActionResponse<GoogleInstance>> terminate(CloudActionRequest request)
      throws InstanceRequestException {
    return googleInstanceManagement.terminate(request);
  }
  
  @Override
  public List<Instance> describeRequest(CloudSpotDescribeRequest request) {
    return null;
  }
}
