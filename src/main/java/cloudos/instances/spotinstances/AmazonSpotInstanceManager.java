package cloudos.instances.spotinstances;

import static org.apache.commons.collections.CollectionUtils.isNotEmpty;
import cloudos.AmazonService;
import cloudos.Providers;
import cloudos.amazon.AWSCredentialService;
import cloudos.amazon.AmazonInstance;
import cloudos.amazon.models.AmazonSpotInstance;
import cloudos.exceptions.NotImplementedException;
import cloudos.instances.AmazonInstanceManagement;
import cloudos.instances.InstanceRequestException;
import cloudos.models.AbstractInstance;
import cloudos.models.CloudActionRequest;
import cloudos.models.CloudActionResponse;
import cloudos.models.spotinstances.CloudSpotCancelRequest;
import cloudos.models.spotinstances.CloudSpotCreateRequest;
import cloudos.models.spotinstances.CloudSpotDescribeRequest;
import cloudos.models.spotinstances.CloudSpotFleetCancelRequest;
import cloudos.models.spotinstances.CloudSpotFleetCancelResponse;
import cloudos.models.spotinstances.CloudSpotFleetCreateRequest;
import cloudos.models.spotinstances.CloudSpotFleetCreateResponse;
import cloudos.models.spotinstances.CloudSpotFleetDescribeRequest;
import cloudos.models.spotinstances.CloudSpotFleetDescribeResponse;
import cloudos.models.spotinstances.CloudSpotFleetModifyRequest;
import cloudos.models.spotinstances.CloudSpotFleetModifyResponse;
import cloudos.models.spotinstances.CloudSpotPricingRequest;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.CancelSpotFleetRequestsRequest;
import com.amazonaws.services.ec2.model.CancelSpotFleetRequestsResult;
import com.amazonaws.services.ec2.model.CancelSpotInstanceRequestsRequest;
import com.amazonaws.services.ec2.model.CancelSpotInstanceRequestsResult;
import com.amazonaws.services.ec2.model.CancelledSpotInstanceRequest;
import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.DescribeSpotFleetInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeSpotFleetInstancesResult;
import com.amazonaws.services.ec2.model.DescribeSpotInstanceRequestsRequest;
import com.amazonaws.services.ec2.model.DescribeSpotInstanceRequestsResult;
import com.amazonaws.services.ec2.model.DescribeSpotPriceHistoryRequest;
import com.amazonaws.services.ec2.model.DescribeSpotPriceHistoryResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.LaunchSpecification;
import com.amazonaws.services.ec2.model.ModifySpotFleetRequestRequest;
import com.amazonaws.services.ec2.model.ModifySpotFleetRequestResult;
import com.amazonaws.services.ec2.model.RequestSpotFleetRequest;
import com.amazonaws.services.ec2.model.RequestSpotFleetResult;
import com.amazonaws.services.ec2.model.RequestSpotInstancesRequest;
import com.amazonaws.services.ec2.model.RequestSpotInstancesResult;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.SpotFleetRequestConfigData;
import com.amazonaws.services.ec2.model.SpotInstanceRequest;
import com.amazonaws.services.ec2.model.SpotPlacement;
import com.amazonaws.services.ec2.model.SpotPrice;
import com.amazonaws.services.ec2.model.Tag;
import com.amazonaws.util.CollectionUtils;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import javax.validation.constraints.NotNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Amazon Spot Instance Manager.
 */
@Service
@Log4j2
public class AmazonSpotInstanceManager implements SpotInstanceManager<AmazonInstance> {

  private static final String MARKED_FOR_TERMINATION = "marked-for-termination";
  private static Map<String, AmazonEC2> cache = new ConcurrentHashMap<>();
  private static final String STATUS_OPEN = "open";
  private static final int HOUR = 60;
  private static final long TIMEOUT_INTERVAL = 30;
  private static final long MAX_REQUEST_WAIT = 20 * TIMEOUT_INTERVAL;

  private static final int MAX_NUMBER_SPOT_FLEET_REQUESTS = 1000;
  private static final int MAX_SPOT_INSTANCES_PER_REGION = 3000;

  @Autowired
  AWSCredentialService awsCredentialService;

  @Autowired
  AmazonService amazonService;

  @Autowired
  AmazonInstanceManagement amazonInstanceManagement;

  /**
   * Create a set of spot instances based on the options given in the requests.
   *
   * @param request a request to create spot instances
   * @return a list of Amazon instances created
   * @throws InstanceRequestException whenever there's an error with the request
   */
  @Override
  public List<AmazonInstance> createInstances(CloudSpotCreateRequest request)
      throws InstanceRequestException {

    // Initializes a Spot Instance Request
    RequestSpotInstancesRequest spotRequest =
        new RequestSpotInstancesRequest().withSpotPrice(request.getSpotPrice().toString())
            .withInstanceCount(request.getCount()).withType(request.getSpotInstanceType());

    // Setup the specifications of the launch. This includes the
    // instance type (e.g. t1.micro) and the latest Amazon Linux
    // AMI id available. Note, you should always use the latest
    // Amazon Linux AMI id or another of your choosing.
    LaunchSpecification launchSpecification = new LaunchSpecification()
        .withImageId(request.getImage()).withInstanceType(request.getMachineType())
        .withSecurityGroups(request.getSecurityGroup()).withKeyName(request.getKeyName())
        .withMonitoringEnabled(true);

    // check if the request contains a validUntil field
    if (request.getValidFrom() != null) {
      spotRequest.setValidFrom(request.getValidFrom());
    }
    // check if the request contains a validUntil field
    if (request.getValidUntil() != null) {
      spotRequest.setValidUntil(request.getValidUntil());
    }
    // set the duration of the request. This value must be a multiple of 60
    // (60, 120, 180, 240, 300, or 360).
    if (request.getDuration() != null && request.getDuration() > 0) {
      if (request.getDuration() % HOUR == 0) {
        spotRequest.setBlockDurationMinutes(request.getDuration());
      }
    }
    // set the availability zone group
    if (StringUtils.isNotEmpty(request.getAvailabilityZoneGroup())) {
      spotRequest.setAvailabilityZoneGroup(request.getAvailabilityZoneGroup());
    }
    // set the launch group
    if (StringUtils.isNotEmpty(request.getLaunchGroup())) {
      spotRequest.setLaunchGroup(request.getLaunchGroup());
    }
    // set the availability zone to be used
    if (StringUtils.isNotEmpty(request.getZone())) {
      SpotPlacement placement = new SpotPlacement(request.getZone());
      launchSpecification.setPlacement(placement);
    }
    // check the placement name
    if (StringUtils.isNotEmpty(request.getPlacementName())) {
      SpotPlacement placement;
      // check if the placement already exists
      if (launchSpecification.getPlacement() != null) {
        placement = launchSpecification.getPlacement();
      } else {
        placement = new SpotPlacement();
      }
      placement.setGroupName(request.getPlacementName());
      launchSpecification.setPlacement(placement);
    }

    // Add the launch specifications to the request.
    spotRequest.setLaunchSpecification(launchSpecification);

    AmazonEC2 ec2 = this.createOrGetClient(request.getRegion());
    // check the state of the amazon client
    if (ec2 == null) {
      return null;
    }

    log.info("Launching spot request {}", spotRequest);
    // Call the RequestSpotInstance API.
    RequestSpotInstancesResult requestResult = ec2.requestSpotInstances(spotRequest);

    // get the list of requests ...
    List<SpotInstanceRequest> requests = requestResult.getSpotInstanceRequests();
    List<String> resourcesIds = this.getRequestIds(requests);

    // if tags are given, tag the request
    if (!CollectionUtils.isNullOrEmpty(request.getTags())) {
      this.tagResource(resourcesIds, request.getTags(), ec2);
    }

    List<String> instanceIds = this.checkRequests(resourcesIds, ec2);

    return this.getSpotInstances(instanceIds, ec2, request.getCloudGroup());
  }

  /**
   * Create a new spot fleet request, based on the parameters given in the fleet create request. The
   * response contains a list of Amazon spot instances that were created.
   *
   * @param createRequest the spot fleet request for creation
   * @return a list of Amazon spot instances that were created
   */
  public CloudSpotFleetCreateResponse createSpotFleet(
      @NotNull CloudSpotFleetCreateRequest createRequest) {
    SpotFleetRequestConfigData requestConfigData = new SpotFleetRequestConfigData()
        .withAllocationStrategy(createRequest.getAllocationStrategy())
        .withClientToken(createRequest.getClientToken())
        .withExcessCapacityTerminationPolicy(createRequest.getExcessCapacityTerminationPolicy())
        .withFulfilledCapacity(createRequest.getFulfilledCapacity())
        .withIamFleetRole(createRequest.getIamFleetRole())
        .withValidUntil(createRequest.getValidUntil()).withValidFrom(createRequest.getValidFrom())
        .withType(createRequest.getType())
        .withTerminateInstancesWithExpiration(createRequest.getTerminateInstancesWithExpiration())
        .withTargetCapacity(createRequest.getTargetCapacity())
        .withSpotPrice(createRequest.getSpotPrice())
        .withReplaceUnhealthyInstances(createRequest.getReplaceUnhealthyInstances())
        .withLaunchSpecifications(createRequest.getLaunchSpecifications())
        .withInstanceInterruptionBehavior(createRequest.getInstanceInterruptionBehavior());

    RequestSpotFleetRequest spotFleetRequest =
        new RequestSpotFleetRequest().withSpotFleetRequestConfig(requestConfigData);

    try {
      AmazonEC2 ec2 = this.createOrGetClient(createRequest.getRegion());
      // check the state of the amazon client
      if (ec2 == null) {
        return null;
      }
      log.info("Launching spot request {}", spotFleetRequest);
      RequestSpotFleetResult spotFleetResult = ec2.requestSpotFleet(spotFleetRequest);
      String fleetRequestId = spotFleetResult.getSpotFleetRequestId();
      // let's check the state of the spot request
      DescribeSpotInstanceRequestsRequest spotRequest =
          new DescribeSpotInstanceRequestsRequest().withSpotInstanceRequestIds(fleetRequestId);
      DescribeSpotInstanceRequestsResult spotResult = ec2.describeSpotInstanceRequests(spotRequest);
      List<SpotInstanceRequest> spotInstanceRequests = spotResult.getSpotInstanceRequests();

      // get the list of requests ...
      List<String> resourcesIds = this.getRequestIds(spotInstanceRequests);

      // if tags are given, tag the request
      if (!CollectionUtils.isNullOrEmpty(createRequest.getTags())) {
        this.tagResource(resourcesIds, createRequest.getTags(), ec2);
      }

      List<String> instanceIds = this.checkRequests(resourcesIds, ec2);
      List<AmazonInstance> instances =
          this.getSpotInstances(instanceIds, ec2, createRequest.getCloudGroupId());
      if (instances != null) {
        // return the new response
        return CloudSpotFleetCreateResponse.builder().fleetRequestId(fleetRequestId)
            .id(createRequest.getId()).provider(Providers.AMAZON_AWS)
            .region(createRequest.getRegion()).instances(instances).build();
      }
      // we need to list the instances from the instance id.
    } catch (AmazonServiceException ex) {
      log.error(ex.getMessage());
    }
    return null;
  }

  /**
   * Modify an existing spot fleet request.
   *
   * @param request the request with the modifications for the spot request
   * @return the response with the status of the request
   */
  public CloudSpotFleetModifyResponse modifySpotFleetRequest(
      @NotNull CloudSpotFleetModifyRequest request) {

    ModifySpotFleetRequestRequest spotFleetRequest =
        new ModifySpotFleetRequestRequest().withSpotFleetRequestId(request.getSpotFleetRequestId())
            .withExcessCapacityTerminationPolicy(request.getExcessCapacityTerminationPolicy())
            .withTargetCapacity(request.getTargetCapacity());

    try {
      AmazonEC2 ec2 = this.createOrGetClient(request.getRegion());
      // check the state of the amazon client
      if (ec2 == null) {
        return null;
      }

      ModifySpotFleetRequestResult spotFleetResult = ec2.modifySpotFleetRequest(spotFleetRequest);

      return CloudSpotFleetModifyResponse.builder().id(request.getId())
          .provider(Providers.AMAZON_AWS).region(request.getRegion())
          .returnValue(spotFleetResult.getReturn()).build();

    } catch (AmazonServiceException ex) {
      log.error(ex.getMessage());
    }
    return null;
  }

  /**
   * Run a describe call to the spot fleet requests.
   *
   * @param request the spot fleet describe request
   * @return a response for the cloud spot fleet describe
   */
  public CloudSpotFleetDescribeResponse describeSpotFleetRequests(
      @NotNull CloudSpotFleetDescribeRequest request) {

    DescribeSpotFleetInstancesRequest spotFleetRequest = new DescribeSpotFleetInstancesRequest()
        .withSpotFleetRequestId(request.getSpotFleetRequestId())
        .withMaxResults(request.getMaxResults()).withNextToken(request.getNextToken());

    try {
      AmazonEC2 ec2 = this.createOrGetClient(request.getRegion());
      // check the state of the amazon client
      if (ec2 == null) {
        return null;
      }

      DescribeSpotFleetInstancesResult spotFleetResult =
          ec2.describeSpotFleetInstances(spotFleetRequest);

      return CloudSpotFleetDescribeResponse.builder()
          .activeInstances(spotFleetResult.getActiveInstances()).id(request.getId())
          .provider(Providers.AMAZON_AWS).region(request.getRegion())
          .nextToken(spotFleetResult.getNextToken())
          .spotFleetRequestId(spotFleetResult.getSpotFleetRequestId()).build();

    } catch (AmazonServiceException ex) {
      log.error(ex.getMessage());
    }
    return null;
  }

  /**
   * Cancel an existing spot fleet request. The user can specify whether the instances can also be
   * terminated.
   *
   * @param request the request to cancel the spot fleet requests
   * @return a response of the cancel request, or null if it fails
   */
  public CloudSpotFleetCancelResponse cancelSpotFleetRequests(
      @NotNull CloudSpotFleetCancelRequest request) {

    // create the spot fleet cancel request
    CancelSpotFleetRequestsRequest fleetRequests = new CancelSpotFleetRequestsRequest()
        .withSpotFleetRequestIds(request.getSpotFleetRequestIds())
        .withTerminateInstances(request.getTerminateInstances());

    try {
      AmazonEC2 ec2 = this.createOrGetClient(request.getRegion());
      // check the state of the amazon client
      if (ec2 == null) {
        return null;
      }

      CancelSpotFleetRequestsResult fleetResult = ec2.cancelSpotFleetRequests(fleetRequests);
      return CloudSpotFleetCancelResponse.builder().id(request.getId())
          .provider(Providers.AMAZON_AWS).region(request.getRegion())
          .successfulFleetRequests(fleetResult.getSuccessfulFleetRequests())
          .unsuccessfulFleetRequests(fleetResult.getUnsuccessfulFleetRequests()).build();
    } catch (AmazonServiceException ex) {
      log.error(ex.getMessage());
    }
    return null;
  }

  /**
   * Fetch the pricing history for the spot offering for the given cloud provider.
   *
   * @param request the spot pricing request
   * @return a list containing the spot pricing
   */
  @Override
  public List<?> pricing(CloudSpotPricingRequest request) {

    // list instead of set because it can contain duplicates
    List<SpotPrice> priceSet = new LinkedList<>();
    AmazonEC2 ec2 = this.createOrGetClient(request.getRegion());
    // get the spot price history
    String nextToken = "";
    do {
      DescribeSpotPriceHistoryRequest historyRequest = new DescribeSpotPriceHistoryRequest()
          .withNextToken(nextToken).withAvailabilityZone(request.getAvailabilityZone())
          .withStartTime(request.getStartTime()).withEndTime(request.getEndTime())
          .withInstanceTypes(request.getInstanceTypes())
          .withProductDescriptions(request.getProductDescription());
      // Perform request
      DescribeSpotPriceHistoryResult result = ec2.describeSpotPriceHistory(historyRequest);
      priceSet.addAll(result.getSpotPriceHistory());
      // 'nextToken' is the string marking the next set of results returned (if any),
      // it will be empty if there are no more results to be returned.
      nextToken = result.getNextToken();
    } while (!nextToken.isEmpty());

    return priceSet;
  }


  /**
   * Tag a request or resource, depending on the type of the resource you are giving.
   *
   * @param resourcesIds a list of resource ids
   * @param tags a list of tags to be added
   * @param ec2 the AmazonEC2 client connection
   */
  private void tagResource(@NotNull List<String> resourcesIds, @NotNull List<Tag> tags,
      AmazonEC2 ec2) {

    // Create the tag request
    CreateTagsRequest createTagsRequest =
        new CreateTagsRequest().withResources(resourcesIds).withTags(tags);
    // tag the spot request
    try {
      // will do a best effor to tag, but no guarantees
      ec2.createTags(createTagsRequest);
    } catch (AmazonServiceException ex) {
      log.info("Error terminating instances");
      log.info("Caught Exception: {}", ex.getMessage());
      log.info("Reponse Status Code: {}", ex.getStatusCode());
      log.info("Error Code: {}", ex.getErrorCode());
      log.info("Request ID: {}", ex.getRequestId());
    }
  }

  /**
   * Get the request ids from the spot instance requests.
   *
   * @param requests a list of spot instance requests
   * @return a list of request ids
   */
  private List<String> getRequestIds(List<SpotInstanceRequest> requests) {
    return requests.stream().map(SpotInstanceRequest::getSpotInstanceRequestId)
        .collect(Collectors.toList());
  }

  /**
   * Check all the spot instance requests, ensuring all the requests are fulfilled before returning.
   *
   * @param requestIds a list of requests ids
   * @return a list of instance id of the spot instances created
   */
  private List<String> checkRequests(@NotNull List<String> requestIds, AmazonEC2 ec2) {

    boolean anyOpen; // tracks whether any requests are still open
    int counter = 1;

    // a list of instances to tag.
    List<String> instanceIds = new ArrayList<>();
    long waitingPeriod = 0;

    do {
      DescribeSpotInstanceRequestsRequest describeRequest =
          new DescribeSpotInstanceRequestsRequest();
      describeRequest.setSpotInstanceRequestIds(requestIds);

      anyOpen = false; // assume no requests are still open

      try {
        // Get the requests to monitor
        DescribeSpotInstanceRequestsResult describeResult =
            ec2.describeSpotInstanceRequests(describeRequest);

        List<SpotInstanceRequest> describeResponses = describeResult.getSpotInstanceRequests();

        // are any requests open?
        for (SpotInstanceRequest describeResponse : describeResponses) {
          if (describeResponse.getState().equals(STATUS_OPEN)) {
            log.info("Request is still open... waiting 30 seconds");
            anyOpen = true;
            break;
          }
          // get the corresponding instance ID of the spot request
          instanceIds.add(describeResponse.getInstanceId());
        }
      } catch (AmazonServiceException e) {
        // Don't break the loop due to an exception (it may be a temporary issue)
        anyOpen = true;
      }

      try {
        Thread.sleep(TIMEOUT_INTERVAL * 1000); // sleep 30s.
        waitingPeriod += TIMEOUT_INTERVAL;
      } catch (Exception e) {
        // Do nothing if the thread woke up early.
      }
      counter++;
    } while ((anyOpen && counter <= 10) && (waitingPeriod <= MAX_REQUEST_WAIT));

    // check if we timed out, so we can go ahead and cancel the request
    if (waitingPeriod > MAX_REQUEST_WAIT) {
      // TODO: Close the open requests
    }

    return instanceIds;
  }

  /**
   * Get all the spot instances that were created, either through normal or fleet requests.
   *
   * @param instanceIds a list of instance ids
   * @param ec2 the AmazonEC2 client
   * @param cloudGroupId that the instance will join
   * @return a list of amazon instances that were created
   */
  private List<AmazonInstance> getSpotInstances(@NotNull List<String> instanceIds,
      @NotNull AmazonEC2 ec2, String cloudGroupId) {

    List<AmazonInstance> amazonInstances = new ArrayList<>();

    // we need to list the instances from the instance id.
    List<Instance> instances = this.describe(instanceIds, ec2);

    // let's return an empty list to signal we failed
    if (instances == null) {
      return amazonInstances;
    }

    // let's convert it to AmazonInstances
    for (Instance instance : instances) {
      AmazonInstance amazonInstance =
          amazonInstanceManagement.createAmazonInstanceFromInstance(instance, true);
      // use copy constructor in the request
      amazonInstance.getInstance().setCloudGroupId(cloudGroupId);
      AmazonSpotInstance spotInstance =
          new AmazonSpotInstance(amazonInstance, instance.getSpotInstanceRequestId());
      amazonInstances.add(spotInstance);
    }

    return amazonInstances;
  }

  /**
   * List all the instances based on the instance ids.
   *
   * @param instanceIds a list of instance ids
   * @param client a AmazonEC2 client
   * @return a list of instances or null
   */
  private List<Instance> describe(List<String> instanceIds, AmazonEC2 client) {
    try {
      List<Instance> instances = new ArrayList<>();
      // create a describe request from the instance id
      DescribeInstancesRequest request =
          new DescribeInstancesRequest().withInstanceIds(instanceIds);
      DescribeInstancesResult result = client.describeInstances(request);
      List<Reservation> reservations = result.getReservations();

      // list all the instances from the reservations
      reservations.forEach(reservation -> {
        instances.addAll(reservation.getInstances());
      });

      return instances;
    } catch (AmazonServiceException ex) {
      return null;
    }
  }

  /**
   * Cancel an existing spot request.
   *
   * @param request the request to cancel the spot request
   * @return if the cancel spot request was successful or not
   */
  @Override
  @SuppressWarnings("Duplicates")
  public boolean cancelSpotRequest(CloudSpotCancelRequest request) {

    AmazonEC2 ec2 = this.createOrGetClient(request.getRegion());

    try {
      // Cancel requests.
      log.info("Cancelling the request {} ", request);
      CancelSpotInstanceRequestsRequest cancelRequest = new CancelSpotInstanceRequestsRequest()
          .withSpotInstanceRequestIds(request.getSpotInstanceRequestIds());

      CancelSpotInstanceRequestsResult result = ec2.cancelSpotInstanceRequests(cancelRequest);
      List<CancelledSpotInstanceRequest> requests = result.getCancelledSpotInstanceRequests();
      List<String> requestIds = requests.stream()
          .map(CancelledSpotInstanceRequest::getSpotInstanceRequestId).collect(Collectors.toList());
      this.checkRequests(requestIds, ec2);
      return true;
    } catch (AmazonServiceException e) {
      // Write out any exceptions that may have occurred.
      log.error("Error cancelling instances");
      log.error("Caught Exception: {}", e.getMessage());
      log.error("Reponse Status Code: {}", e.getStatusCode());
      log.error("Error Code: {}", e.getErrorCode());
      log.error("Request ID: {}", e.getRequestId());
    }

    return false;
  }

  /**
   * Terminate a list of spot instances.
   *
   * @param request the action request
   * @return a list containing the response of the amazon instances terminated
   * @throws InstanceRequestException whenever fails to execute the request
   */
  @Override
  @SuppressWarnings("Duplicates")
  public List<CloudActionResponse<AmazonInstance>> terminate(CloudActionRequest request)
      throws InstanceRequestException {

    return amazonInstanceManagement.terminate(request);
  }

  /**
   * Describe the status of the request of some spot instances.
   *
   * @param request the CloudActionRequest
   */
  @Override
  public List<Instance> describeRequest(CloudSpotDescribeRequest request) {
    AmazonEC2 ec2 = this.createOrGetClient(request.getRegion());
    return this.describe(request.getInstanceIds(), ec2);
  }

  /**
   * Create a new AmazonEC2 client based on the region given.
   *
   * @param region the region the client is located
   * @return a newly created AmazonEC2 client
   */
  private AmazonEC2 createOrGetClient(String region) {
    AmazonEC2 ec2;
    // check if the client exists in the cache
    if (!cache.containsKey(region)) {
      // add a new AmazonEC2 client to the cache.
      ec2 = AmazonEC2ClientBuilder.standard()
          .withCredentials(awsCredentialService.getAWSCredentials()).withRegion(region).build();
      cache.put(region, ec2);
    }

    ec2 = cache.get(region);
    return ec2;
  }

  /**
   * Check if the spot request was marked for termination.
   *
   * @param spotInstanceRequestId id
   * @param providerId instance id
   * @param region instance region
   * @return true if was marked-for-termination, false otherwise
   */
  public boolean isMarkedForTermination(String spotInstanceRequestId, String providerId,
      String region) {
    AmazonEC2 client = createOrGetClient(region);
    DescribeSpotInstanceRequestsRequest request = new DescribeSpotInstanceRequestsRequest();
    request.withSpotInstanceRequestIds(spotInstanceRequestId);
    DescribeSpotInstanceRequestsResult result = client.describeSpotInstanceRequests(request);
    if (result != null && isNotEmpty(result.getSpotInstanceRequests())) {
      for (SpotInstanceRequest spotInstanceRequest : result.getSpotInstanceRequests()) {

        if (spotInstanceRequest.getInstanceId().equals(providerId)) {
          // if it was marked-for-termination
          if (MARKED_FOR_TERMINATION.equals(spotInstanceRequest.getState())) {
            return true;
          }
        }
      }
    }
    return false;
  }

  /**
   * Clone an spot instance.
   * TODO: Implement it.
   * 
   * @param originalInstance to be cloned
   * @return the cloned instance
   * @throws NotImplementedException if not implemented yet
   */
  public Instance cloneInstance(AbstractInstance originalInstance) throws NotImplementedException {
    throw new NotImplementedException();
  }
}
