package cloudos.instances;

import cloudos.models.AbstractInstance;
import cloudos.models.ActionStatus;
import cloudos.models.CloudActionRequest;
import cloudos.models.CloudActionResponse;
import cloudos.models.CloudCreateRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Provider - instance management for operations with the instances, like create, stop, start, etc.
 */
public interface InstanceManagement<T extends AbstractInstance> {

  /**
   * Create an instance on the provider and save it on mongo.
   *
   * @return mongo instance
   */
  List<T> createInstances(CloudCreateRequest request) throws InstanceRequestException;

  /**
   * Fetch a list of instances on the provider and save it on mongo.
   *
   * @return list of mongo instance
   */
  List<T> fetch(CloudActionRequest request) throws InstanceRequestException;

  /**
   * Start an instance.
   *
   * @param request the action request
   * @return a response
   */
  List<CloudActionResponse<T>> start(CloudActionRequest request) throws InstanceRequestException;

  /**
   * Stop an instance.
   *
   * @param request the action request
   * @return a response
   */
  List<CloudActionResponse<T>> stop(CloudActionRequest request) throws InstanceRequestException;

  /**
   * Reboot an instance.
   *
   * @param request the action request
   * @return a response
   */
  List<CloudActionResponse<T>> reboot(CloudActionRequest request) throws InstanceRequestException;

  /**
   * Terminate an instance.
   *
   * @param request the action request
   * @return a response
   */
  List<CloudActionResponse<T>> terminate(CloudActionRequest request)
      throws InstanceRequestException;

  /**
   * Verify if instance is started.
   *
   * @param instance the instance in question
   * @return boolean
   */
  boolean isStarted(T instance);

  /**
   * Verify if instance is stopped.
   *
   * @param instance the instance in question
   * @return boolean
   */
  boolean isStopped(T instance);

  /**
   * Verify if instance is terminated.
   *
   * @param instance the instance in question
   * @return boolean
   */
  boolean isTerminated(T instance);

  /** Sync the cloudos instance with the provider instances. */
  void syncInstances();

  /**
   * Generate a list of responses expected from the request.
   *
   * @param request a request
   * @return a list of responses
   */
  default List<CloudActionResponse<T>> generateResponseListFromRequest(CloudActionRequest request) {
    List<CloudActionResponse<T>> response = new ArrayList<>();
    for (String name : request.getNames()) {
      response.add(
          new CloudActionResponse<T>(name, null, ActionStatus.NOT_EXECUTED, request.getType()));
    }
    return response;
  }

  /**
   * Get a response for an instance on the list.
   *
   * @param actionResponses a list of actions
   * @param name the name of the instance
   * @return a single response
   */
  default CloudActionResponse<T> getResponseByInstanceName(
      List<CloudActionResponse<T>> actionResponses, String name) {
    for (CloudActionResponse<T> cloudActionResponse : actionResponses) {
      if (cloudActionResponse.getName().equals(name)) {
        return cloudActionResponse;
      }
    }
    return null;
  }

  /** Discover the instances of the account. */
  void discovery();

  /**
   * Clone an instance.
   * @param originalInstance to be cloned
   * @return a new cloned instance
   * @throws InstanceRequestException if something goes wrong
   */
  T cloneInstance(AbstractInstance originalInstance) throws InstanceRequestException;
}
