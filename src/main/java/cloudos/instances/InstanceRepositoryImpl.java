package cloudos.instances;

import cloudos.Providers;
import cloudos.models.Instance;
import cloudos.models.costanalysis.InstanceTypeCost;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

/**
 * Custom repository implementation for {@link Instance}.
 */
class InstanceRepositoryImpl implements InstanceRepositoryCustom {

  private final MongoTemplate mongoTemplate;

  @Autowired
  public InstanceRepositoryImpl(MongoTemplate mongoTemplate) {
    this.mongoTemplate = mongoTemplate;
  }

  @Override
  public List<InstanceTypeCost> findTopInstanceTypesOrderByCostDesc(Integer limit) {

    List<AggregationOperation> operations = new ArrayList<>(
        Arrays.asList(
            Aggregation.match(new Criteria("machineType").ne(null)),
            Aggregation.group("machineType").sum("cost").as("cost"),
            Aggregation.sort(Direction.DESC, "cost"))
    );

    if (limit != null && limit > 0) {
      operations.add(Aggregation.limit(limit));
    }

    AggregationResults<InstanceTypeCost> result = mongoTemplate
        .aggregate(Aggregation.newAggregation(operations), Instance.class, InstanceTypeCost.class);

    return result.getMappedResults();
  }

  @Override
  @Cacheable("instancebyproviderandmachinetypeanddatebetween")
  public List<Instance> findByProviderAndMachineTypeAndDateBetween(Providers provider,
      String machineType, Date startIncl,
      Date endIncl) {

    if (provider == null) {
      throw new IllegalArgumentException("provider is mandatory");
    }

    if (StringUtils.isBlank(machineType)) {
      throw new IllegalArgumentException("machine type is mandatory");
    }

    Criteria deletedDateCriteria = new Criteria()
        .orOperator(Criteria.where(Instance.DELETED_DATE).exists(false),
            Criteria.where(Instance.DELETED_DATE).lte(startIncl));

    Query query = new Query()
        .addCriteria(Criteria.where(Instance.PROVIDER).is(provider))
        .addCriteria(Criteria.where(Instance.MACHINE_TYPE).is(machineType))
        .addCriteria(Criteria.where(Instance.CREATION_DATE).lte(endIncl))
        .addCriteria(deletedDateCriteria);

    return mongoTemplate.find(query, Instance.class);
  }

}
