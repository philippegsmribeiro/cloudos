package cloudos.instances;

/**
 * Exception when a action request on an instance fails.
 * 
 */
public class InstanceRequestException extends Exception {

  private static final long serialVersionUID = -6283521504310814784L;

  public InstanceRequestException() {
    super();
  }

  public InstanceRequestException(String message) {
    super(message);
  }
}
