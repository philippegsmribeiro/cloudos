package cloudos.instances;

import cloudos.AmazonService;
import cloudos.Providers;
import cloudos.amazon.AmazonEC2OnDemand;
import cloudos.amazon.AmazonInstance;
import cloudos.amazon.AmazonInstanceRepository;
import cloudos.exceptions.NotImplementedException;
import cloudos.instances.spotinstances.AmazonSpotInstanceManager;
import cloudos.keys.KeyManagerService;
import cloudos.keys.cloudoskey.CloudosKey;
import cloudos.models.AbstractInstance;
import cloudos.models.ActionStatus;
import cloudos.models.CloudActionRequest;
import cloudos.models.CloudActionResponse;
import cloudos.models.CloudCreateRequest;
import cloudos.provider.AmazonProviderManagement;
import cloudos.queue.QueueManagerService;
import cloudos.security.LogFilter;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.InstanceLifecycleType;
import com.amazonaws.services.ec2.model.InstanceStateName;
import com.amazonaws.services.ec2.model.InstanceStatus;
import com.amazonaws.services.ec2.model.MonitoringState;
import com.amazonaws.services.ec2.model.Tag;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class AmazonInstanceManagement implements InstanceManagement<AmazonInstance> {

  private static final String DEFAULT = "default";

  @Autowired
  AmazonService amazonService;
  @Autowired
  AmazonSpotInstanceManager amazonSpotInstanceManager;
  @Autowired
  AmazonInstanceRepository amazonInstanceRepository;
  @Autowired
  InstanceService instanceService;
  @Autowired
  AmazonProviderManagement amazonProviderManagement;
  @Autowired
  KeyManagerService keyManagerService;
  @Autowired
  AsyncTaskExecutor taskExecutor;
  @Autowired
  QueueManagerService queueManagerService;
  // timeout
  private static final int TIMEOUT = 1000 * 60 * 2;

  private static int PENDING = 0;
  private static int RUNNING = 16;
  // private static int SHUTTING_DOWN = 32;
  private static int TERMINATED = 48;
  private static int STOPPING = 64;
  private static int STOPPED = 80;

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.InstanceManagement#createInstances(cloudos.models.CloudCreateRequest)
   */
  @Override
  public List<AmazonInstance> createInstances(CloudCreateRequest request)
      throws InstanceRequestException {
    try {
      List<Instance> createInstances = amazonService.createInstances(request.getImage(),
          request.getMachineType(), request.getMinCount(), request.getCount(), request.getKeyName(),
          request.getSecurityGroup(), request.getRegion(), request.getZone(), request.getName(),
          request.getTag(), request.getCloudGroup());
      List<AmazonInstance> instances = new ArrayList<>();
      for (Instance instance : createInstances) {
        instances.add(createAmazonInstanceFromInstance(instance, true));
      }
      return instances;
    } catch (Exception e) {
      throw new InstanceRequestException(e.getLocalizedMessage());
    }
  }

  /**
   * Creates/updates the instance info on the mongo.
   *
   * @param instance aws object
   * @return AmazonInstance
   */
  public AmazonInstance createAmazonInstanceFromInstance(Instance instance, boolean notify) {
    AmazonInstance amazonInstance = null;
    // verify if already exists
    cloudos.models.Instance cloudOsInstance =
        instanceService.findByProviderAndProviderId(Providers.AMAZON_AWS, instance.getInstanceId());
    if (cloudOsInstance != null) {
      // get from mongo and update
      amazonInstance = amazonInstanceRepository.findByInstance(cloudOsInstance);
      if (amazonInstance == null) {
        // for incomplete data
        amazonInstance = new AmazonInstance();
        amazonInstance.setInstance(cloudOsInstance);
      }
    } else {
      // create the new register
      amazonInstance = new AmazonInstance();
      cloudOsInstance = new cloudos.models.Instance();
      cloudOsInstance.setIncludedDate(new Date());
      cloudOsInstance.setProvider(Providers.AMAZON_AWS);
      cloudOsInstance.setProviderId(instance.getInstanceId());
      amazonInstance.setInstance(cloudOsInstance);
    }
    // update info

    // region and zone
    if (StringUtils.isBlank(cloudOsInstance.getZone())
        || !cloudOsInstance.getZone().equals(instance.getPlacement().getAvailabilityZone())) {
      // just update if it is not set or if it is different
      cloudOsInstance.setZone(instance.getPlacement().getAvailabilityZone());
      cloudOsInstance.setRegion(
          amazonProviderManagement.getRegionFromZone(cloudOsInstance.getZone()).getRegion());
    }
    cloudOsInstance.setCreationDate(instance.getLaunchTime());
    cloudOsInstance.setImageType(instance.getImageId());
    cloudOsInstance.setMachineType(instance.getInstanceType());
    cloudOsInstance.setIpAddress(instance.getPublicIpAddress());
    List<Tag> cloudOsTags = instance.getTags().stream()
        .filter(t -> t.getKey().equals(cloudos.models.Instance.DEPLOYMENT_ID_TAG))
        .collect(Collectors.toList());
    if (CollectionUtils.isNotEmpty(cloudOsTags)) {
      cloudOsInstance.setDeploymentId(cloudOsTags.get(0).getValue());
    }
    // cloudGroup
    List<Tag> cloudGroupTags = instance.getTags().stream()
        .filter(t -> t.getKey().equals(cloudos.models.Instance.CLOUDGROUP_ID_TAG))
        .collect(Collectors.toList());
    if (CollectionUtils.isNotEmpty(cloudGroupTags)) {
      cloudOsInstance.setCloudGroupId(cloudGroupTags.get(0).getValue());
    }
    cloudos.models.InstanceStatus oldStatus = cloudOsInstance.getStatus();
    final cloudos.models.InstanceStatus newStatus = getStatus(instance);
    cloudOsInstance.setStatus(newStatus);
    cloudOsInstance.setDeleted(false);
    String instanceName = getInstanceName(instance);
    cloudOsInstance.setName(
        StringUtils.isEmpty(instanceName) ? cloudOsInstance.getProviderId() : instanceName);

    CloudosKey findKey = null;
    String keyName = instance.getKeyName();
    if (StringUtils.isBlank(cloudOsInstance.getKeyName())
        || !cloudOsInstance.getKeyName().equals(keyName)) {
      // just update if it is not set or if it is different
      if (StringUtils.isNotBlank(keyName)) {
        try {
          findKey =
              keyManagerService.findKey(keyName, Providers.AMAZON_AWS, cloudOsInstance.getRegion());
          if (findKey != null) {
            cloudOsInstance.setKeyName(keyName);
          }
        } catch (Exception e) {
          log.error("Error getting the key", e);
        }
      }
    }

    // securityGroup
    if (CollectionUtils.isNotEmpty(instance.getSecurityGroups())) {
      cloudOsInstance.setSecurityGroup(instance.getSecurityGroups().get(0).getGroupName());
    } else {
      cloudOsInstance.setSecurityGroup(DEFAULT);
    }

    // update custom info
    amazonInstance.setProductFamily(instance.getInstanceType());
    // amazonInstance.setJson(instance.toString());

    // verify monitoring is enabled
    if (MonitoringState.Disabled.name().equalsIgnoreCase(instance.getMonitoring().getState())) {
      // enable the monitoring
      amazonService.enableMonitoringForInstance(cloudOsInstance.getRegion(), instance);
    }

    boolean notifySpotTerminated = false;
    // verify if is spot
    if (instance.getInstanceLifecycle() != null
        && InstanceLifecycleType.Spot == InstanceLifecycleType
            .fromValue(instance.getInstanceLifecycle())) {
      // set as spot
      cloudOsInstance.setSpot(true);

      if (cloudOsInstance.getStatus() != cloudos.models.InstanceStatus.TERMINATED) {
        // if not terminated, check if it was preempted

        String spotInstanceRequestId = instance.getSpotInstanceRequestId();
        if (amazonSpotInstanceManager.isMarkedForTermination(spotInstanceRequestId,
            cloudOsInstance.getProviderId(), cloudOsInstance.getRegion())) {
          log.info("################################PREEMPTED####################################");
          log.info(instance.toString());
          log.info("################################PREEMPTED####################################");
          // send message to the manager
          notifySpotTerminated = true;
        }
      }
    }

    // save
    try {
      cloudOsInstance = instanceService.save(cloudOsInstance);
    } catch (DuplicateKeyException e) {
      log.error("Duplicate key! Handling.", e);
      // if instance is already save
      cloudos.models.Instance savedCloudOsInstance = instanceService
          .findByProviderAndProviderId(Providers.AMAZON_AWS, instance.getInstanceId());
      if (savedCloudOsInstance != null) {
        cloudOsInstance.setId(savedCloudOsInstance.getId());
        cloudOsInstance = instanceService.save(cloudOsInstance);
        oldStatus = cloudOsInstance.getStatus();
        AmazonInstance savedAmazonInstance =
            amazonInstanceRepository.findByInstance(cloudOsInstance);
        if (savedAmazonInstance != null) {
          amazonInstance.setId(savedAmazonInstance.getId());
        }
      }
    }
    amazonInstance.setInstance(cloudOsInstance);
    amazonInstanceRepository.save(amazonInstance);

    if (notify) {

      if (oldStatus == null) {
        // create notification of the new instance found on the monitoring service, etc.
        queueManagerService.sendInstanceCreatedMessage(amazonInstance);
      }
      if (oldStatus != null && !oldStatus.equals(newStatus)) {
        // create notification of status changed - Instance X had the status updated to Y
        queueManagerService.sendInstanceUpdatedMessage(oldStatus, amazonInstance);
      }
    }

    // if key not found or private key not imported yet.
    if (findKey == null || StringUtils.isBlank(findKey.getPrivateKeyContent())) {
      // create notification
      queueManagerService.sendInstanceKeyNotFound(keyName, cloudOsInstance);
    }

    if (notifySpotTerminated) {
      // notify spot instance manager
      queueManagerService.sendSpotInstanceTerminatedMessage(cloudOsInstance);
    }

    return amazonInstance;
  }

  /**
   * Get the instance name.
   *
   * @param instance to get name of
   * @return name
   */
  private String getInstanceName(Instance instance) {
    if (instance != null && CollectionUtils.isNotEmpty(instance.getTags())) {
      for (Tag tag : instance.getTags()) {
        if (tag.getKey().equals(AmazonEC2OnDemand.TAG_NAME)) {
          return tag.getValue();
        }
      }
    }
    return null;
  }

  /**
   * Map the provider status with the cloudos status.
   *
   * @param instance element
   * @return InstanceStatus
   */
  private cloudos.models.InstanceStatus getStatus(Instance instance) {
    if (instance.getState().getName().equalsIgnoreCase(InstanceStateName.Pending.name())) {
      return cloudos.models.InstanceStatus.PROVISIONING;
    }
    if (instance.getState().getName().equalsIgnoreCase(InstanceStateName.Running.name())) {
      return cloudos.models.InstanceStatus.RUNNING;
    }
    if (instance.getState().getName().equalsIgnoreCase(InstanceStateName.ShuttingDown.name())) {
      return cloudos.models.InstanceStatus.STOPPING;
    }
    if (instance.getState().getName().equalsIgnoreCase(InstanceStateName.Stopping.name())) {
      return cloudos.models.InstanceStatus.STOPPING;
    }
    if (instance.getState().getName().equalsIgnoreCase(InstanceStateName.Stopped.name())) {
      return cloudos.models.InstanceStatus.STOPPED;
    }
    if (instance.getState().getName().equalsIgnoreCase(InstanceStateName.Terminated.name())) {
      return cloudos.models.InstanceStatus.TERMINATED;
    }
    return null;
  }

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.InstanceManagement#fetch(cloudos.models.CloudActionRequest)
   */
  @Override
  public List<AmazonInstance> fetch(CloudActionRequest request) {
    List<AmazonInstance> instances = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(request.getNames())) {
      instances.addAll(fetchByName(request));
    } else {
      Set<Instance> listInstances =
          amazonService.listInstances(request.getRegion(), request.getSecurityGroup());
      for (Instance instance : listInstances) {
        instances.add(createAmazonInstanceFromInstance(instance, false));
      }
    }
    return instances;
  }

  /**
   * Fetch by Name.
   *
   * @param request action request
   * @return List of Amazon Instance
   */
  private List<AmazonInstance> fetchByName(CloudActionRequest request) {
    Set<Instance> listInstances =
        amazonService.listInstances(request.getRegion(), request.getSecurityGroup());
    List<AmazonInstance> instances = new ArrayList<>();
    for (Instance instance : listInstances) {
      if (request.getNames().contains(instance.getInstanceId())) {
        instances.add(createAmazonInstanceFromInstance(instance, false));
      }
    }
    return instances;
  }

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.InstanceManagement#start(cloudos.models.CloudActionRequest)
   */
  @Override
  public List<CloudActionResponse<AmazonInstance>> start(CloudActionRequest request) {
    List<CloudActionResponse<AmazonInstance>> responseList =
        this.generateResponseListFromRequest(request);
    List<String> names =
        classifyInstances(request, responseList, Arrays.asList(STOPPED, STOPPING, TERMINATED));
    if (CollectionUtils.isNotEmpty(names)) {
      amazonService.startInstances(names, request.getRegion(), request.getSecurityGroup());
      processRequestReturn(request, responseList, names, RUNNING);
    }
    return updateResponseWithInstances(request, responseList);
  }

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.InstanceManagement#stop(cloudos.models.CloudActionRequest)
   */
  @Override
  public List<CloudActionResponse<AmazonInstance>> stop(CloudActionRequest request) {
    List<CloudActionResponse<AmazonInstance>> responseList =
        this.generateResponseListFromRequest(request);
    List<String> names = classifyInstances(request, responseList, Arrays.asList(RUNNING));
    if (CollectionUtils.isNotEmpty(names)) {
      amazonService.stopInstances(names, request.getRegion(), request.getSecurityGroup());
      processRequestReturn(request, responseList, names, STOPPED);
    }
    return updateResponseWithInstances(request, responseList);
  }

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.InstanceManagement#reboot(cloudos.models.CloudActionRequest)
   */
  @Override
  public List<CloudActionResponse<AmazonInstance>> reboot(CloudActionRequest request) {
    List<CloudActionResponse<AmazonInstance>> response =
        this.generateResponseListFromRequest(request);
    List<String> names = classifyInstances(request, response, Arrays.asList(RUNNING));
    if (CollectionUtils.isNotEmpty(names)) {
      amazonService.rebootInstances(names, request.getRegion(), request.getSecurityGroup());
      processRequestReturn(request, response, names, RUNNING);
    }
    return updateResponseWithInstances(request, response);
  }

  @Override
  public List<CloudActionResponse<AmazonInstance>> terminate(CloudActionRequest request) {
    List<CloudActionResponse<AmazonInstance>> responseList =
        this.generateResponseListFromRequest(request);
    List<String> names = classifyInstances(request, responseList,
        Arrays.asList(PENDING, RUNNING, STOPPING, STOPPED));
    if (CollectionUtils.isNotEmpty(names)) {
      amazonService.terminateInstances(names, request.getRegion(), request.getSecurityGroup());
      processRequestReturn(request, responseList, names, TERMINATED);
    }
    return updateResponseWithInstances(request, responseList);
  }

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.InstanceManagement#isStarted(cloudos.models.AbstractInstance)
   */
  @Override
  public boolean isStarted(AmazonInstance instance) {
    String region = instance.getInstance().getRegion();
    return amazonService
        .instancesStatus(Arrays.asList(instance.getInstance().getProviderId()), region, null).get(0)
        .getInstanceState().getCode() == RUNNING;
  }

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.InstanceManagement#isStopped(cloudos.models.AbstractInstance)
   */
  @Override
  public boolean isStopped(AmazonInstance instance) {
    String region = instance.getInstance().getRegion();
    return amazonService
        .instancesStatus(Arrays.asList(instance.getInstance().getProviderId()), region, null).get(0)
        .getInstanceState().getCode() == STOPPED;
  }

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.InstanceManagement#isTerminated(cloudos.models.AbstractInstance)
   */
  @Override
  public boolean isTerminated(AmazonInstance instance) {
    String region = instance.getInstance().getRegion();
    return amazonService
        .instancesStatus(Arrays.asList(instance.getInstance().getProviderId()), region, null).get(0)
        .getInstanceState().getCode() == TERMINATED;
  }

  /**
   * Block the thread until the instances get to certain status.
   *
   * @param instanceIds names
   * @param region instances region
   * @param securityGroup groups
   * @param timeout limit
   * @param status to check
   * @return List of instance names
   */
  private List<String> blockUntilStatus(List<String> instanceIds, String region,
      String securityGroup, long timeout, int... status) {
    List<String> readyInstances = new ArrayList<>();
    long start = System.currentTimeMillis();
    final long pollInterval = 5 * 1000;
    do {
      try {
        Thread.sleep(pollInterval);
      } catch (Exception e) {
        // ignore it
      }
      List<InstanceStatus> instancesStatus =
          amazonService.instancesStatus(instanceIds, region, securityGroup);
      for (InstanceStatus instanceStatus : instancesStatus) {
        String instanceId = instanceStatus.getInstanceId();
        if (readyInstances.contains(instanceId)) {
          continue;
        }
        Integer code = instanceStatus.getInstanceState().getCode();
        log.info("blockUntilStatus - expected: " + Arrays.toString(status) + " - actual: " + code
            + " " + instanceId);
        if (ArrayUtils.contains(status, code)) {
          readyInstances.add(instanceId);
        }
      }
      long elapsed = System.currentTimeMillis() - start;
      if (elapsed >= timeout) {
        return readyInstances;
      }
    } while (readyInstances.size() != instanceIds.size());
    return readyInstances;
  }

  /**
   * Process the returned request - loads the responses.
   *
   * @param request object
   * @param response objects
   * @param names instance names
   * @param status received
   */
  private void processRequestReturn(CloudActionRequest request,
      List<CloudActionResponse<AmazonInstance>> response, List<String> names, int status) {
    if (request.isSynchronous()) {
      List<String> okInstances =
          blockUntilStatus(names, request.getRegion(), request.getSecurityGroup(), TIMEOUT, status);
      for (String string : okInstances) {
        getResponseByInstanceName(response, string).setStatus(ActionStatus.EXECUTED);
      }
    } else {
      for (String string : names) {
        getResponseByInstanceName(response, string).setStatus(ActionStatus.EXECUTED_ASYNCH);
      }
    }
  }

  /**
   * Update the responses.
   *
   * @param request related
   * @param response to be update
   * @return list of updated responses
   */
  private List<CloudActionResponse<AmazonInstance>> updateResponseWithInstances(
      CloudActionRequest request, List<CloudActionResponse<AmazonInstance>> response) {
    List<AmazonInstance> fetch = fetch(request);
    for (AmazonInstance instance : fetch) {
      CloudActionResponse<AmazonInstance> actionResponse =
          this.getResponseByInstanceName(response, instance.getInstance().getProviderId());
      actionResponse.setInstance(instance);
    }
    return response;
  }

  /**
   * Classify the instance by status.
   *
   * @param request action request
   * @param response list
   * @param codes validated
   * @return list of instance names
   */
  private List<String> classifyInstances(CloudActionRequest request,
      List<CloudActionResponse<AmazonInstance>> response, List<Integer> codes) {
    List<String> names = new ArrayList<>();
    try {
      List<InstanceStatus> instancesStatus = amazonService.instancesStatus(request.getNames(),
          request.getRegion(), request.getSecurityGroup());
      for (InstanceStatus instanceStatus : instancesStatus) {
        if (codes.contains(instanceStatus.getInstanceState().getCode())) {
          names.add(instanceStatus.getInstanceId());
        } else {
          CloudActionResponse<AmazonInstance> byName =
              getResponseByInstanceName(response, instanceStatus.getInstanceId());
          byName.setErrorMessage("Wrong state: " + instanceStatus.getInstanceState().getName());
          byName.setStatus(ActionStatus.ERROR);
        }
      }
    } catch (Exception e) {
      for (CloudActionResponse<AmazonInstance> r : response) {
        r.setErrorMessage(e.getLocalizedMessage());
      }
    }
    return names;
  }

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.InstanceManagement#syncInstances()
   */
  @Override
  public void syncInstances() {

    List<cloudos.provider.ProviderRegion> retrieveRegions =
        amazonProviderManagement.retrieveRegions();
    if (!CollectionUtils.isEmpty(retrieveRegions)) {
      // for all regions
      for (cloudos.provider.ProviderRegion region : retrieveRegions) {
        String threadName = ThreadContext.get(LogFilter.LOG_PATH);
        taskExecutor.execute(new Runnable() {

          @Override
          public void run() {
            ThreadContext.put(LogFilter.LOG_PATH, threadName);
            syncByRegion(region);
            ThreadContext.remove(LogFilter.LOG_PATH);
          }
        });
      }
    }
  }

  /**
   * Execute the sync by region.
   *
   * @param region filter
   */
  public void syncByRegion(cloudos.provider.ProviderRegion region) {
    try {
      log.info("syncByRegion: {}", region.getRegion());
      // get the actual instance list
      List<cloudos.models.Instance> dbInstances = instanceService
          .findAllActiveInstancesByProviderAndRegion(Providers.AMAZON_AWS, region.getRegion());
      // get the new instance list
      Set<Instance> listInstances = amazonService.listInstances(region.getRegion(), null);
      // create map by provider Id to verify deleted instances afterwards
      Map<String, Instance> mapByProviderId = new HashMap<>();
      for (Instance instance : listInstances) {
        log.info("syncing: {} - {}", instance.getInstanceId(), instance.getState().getName());
        createAmazonInstanceFromInstance(instance, true);
        // add to the map
        mapByProviderId.put(instance.getInstanceId(), instance);
      }
      // verify any deleted
      for (cloudos.models.Instance instance : dbInstances) {
        if (!mapByProviderId.containsKey(instance.getProviderId())) {
          // set as deleted
          log.info("delete: {}", instance.getProviderId());
          instance.setDeleted(true);
          instance.setStatus(cloudos.models.InstanceStatus.TERMINATED);
          instance.setDeletedDate(new Date());
          instanceService.save(instance);
          // notify
          queueManagerService.sendInstanceDeletedMessage(instance);
        }
      }
    } catch (Exception e) {
      log.error("error syncing: " + region.getDescription());
    }
  }

  @Override
  public void discovery() {
    syncInstances();
  }

  @Override
  public AmazonInstance cloneInstance(AbstractInstance originalInstance) {
    if (originalInstance.getInstance().isSpot()) {
      try {
        // try spot
        Instance instance = amazonSpotInstanceManager.cloneInstance(originalInstance);
        return this.createAmazonInstanceFromInstance(instance, false);
      } catch (NotImplementedException e) {
        log.error("Not implemented yet!!", e);
      }
    }
    // if not spot or if spot not implemented yet, clone as on demand
    Instance instance = amazonService.cloneInstance(originalInstance);
    return this.createAmazonInstanceFromInstance(instance, false);
  }
}
