package cloudos.instances;

import cloudos.Providers;
import cloudos.models.ClientStatus;
import cloudos.models.Instance;
import cloudos.models.InstanceStatus;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * Interface to access the cloudos_instances repository.
 */
interface InstanceRepository extends MongoRepository<Instance, String>,
    InstanceRepositoryCustom, QueryDslPredicateExecutor<Instance> {

  List<Instance> findByProvider(Providers provider);

  List<Instance> findByProviderAndProviderId(Providers provider, String providerid);

  List<Instance> findByProviderAndProviderIdIn(Providers provider, List<String> providerid);

  List<Instance> findByDeleted(Boolean deleted);

  List<Instance> findByDeletedAndStatusNotIn(Boolean deleted, Collection<InstanceStatus> statuses);

  List<Instance> findByProviderAndDeleted(Providers provider, Boolean deleted);

  List<Instance> findByProviderAndDeletedAndRegion(Providers provider, Boolean deleted,
      String region);

  List<Instance> findByCreationDateAfter(Date date);
  
  List<Instance> findByCreationDateBefore(Date date);

  List<Instance> findByStatus(InstanceStatus status);

  List<Instance> findByClientStatusInAndKeyNameAndDeleted(List<ClientStatus> clientStatus,
      String keyName, Boolean deleted);

  List<Instance> findByRegion(String region);

  List<Instance> findByRegionAndProvider(String region, Providers provider);

  Instance findByProviderId(String providerId);

  List<Instance> findByIdIn(List<String> ids);

  Instance findByName(String name);

  List<Instance> findByCloudGroupId(String id);

  Integer countByCloudGroupIdAndDeleted(String id, Boolean deleted);

  List<Instance> findByCloudGroupIdIn(List<String> ids);

  List<Instance> findByDeletedDateLessThan(Date deletedDate);

  List<Instance> findByCloudGroupIdIsNullAndProviderAndDeleted(Providers provider, Boolean deleted);

}
