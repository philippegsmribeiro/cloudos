package cloudos.instances;

import cloudos.Providers;
import cloudos.google.GoogleInstance;
import cloudos.google.GoogleInstanceRepository;
import cloudos.google.GoogleKeyGenerator;
import cloudos.google.integration.GoogleComputeIntegration;
import cloudos.keys.KeyManagerService;
import cloudos.keys.cloudoskey.CloudosKey;
import cloudos.models.AbstractInstance;
import cloudos.models.ActionStatus;
import cloudos.models.ClientStatus;
import cloudos.models.CloudActionRequest;
import cloudos.models.CloudActionResponse;
import cloudos.models.CloudCreateRequest;
import cloudos.models.InstanceStatus;
import cloudos.provider.GoogleProviderManagement;
import cloudos.provider.ProviderImageType;
import cloudos.queue.QueueManagerService;
import com.google.cloud.compute.AttachedDisk;
import com.google.cloud.compute.ImageId;
import com.google.cloud.compute.Instance;
import com.google.cloud.compute.InstanceInfo;
import com.google.cloud.compute.LicenseId;
import com.google.cloud.compute.NetworkInterface;
import com.google.cloud.compute.NetworkInterface.AccessConfig;
import com.google.cloud.compute.Operation;
import com.google.cloud.compute.Operation.OperationError;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class GoogleInstanceManagement implements InstanceManagement<GoogleInstance> {

  public static final String KEY_NAME = "keyName";
  public static final String SSH_KEYS = "ssh-keys";

  @Autowired
  GoogleComputeIntegration googleIntegration;
  @Autowired
  GoogleInstanceRepository googleInstanceRepository;
  @Autowired
  InstanceService instanceService;
  @Autowired
  GoogleKeyGenerator googleKeyGenerator;
  @Autowired
  GoogleProviderManagement googleProviderManagement;
  @Autowired
  KeyManagerService keyManagerService;
  @Autowired
  QueueManagerService queueManagerService;
  // timeout
  private static final int TIMEOUT = 1000 * 60 * 5;
  //
  Gson gson = new Gson();

  /**
   * Create a new Google Cloud instance.
   *
   * @param request the create request
   * @return a list of newly created google instances
   * @throws InstanceRequestException if failed to create the instances
   */
  @Override
  public List<GoogleInstance> createInstances(CloudCreateRequest request)
      throws InstanceRequestException {
    try {
      log.info("create {} instances, base name: {}", request.getCount(), request.getName());
      List<GoogleInstance> instances = new ArrayList<>();
      for (int i = 0; i < request.getCount(); i++) {
        GoogleInstance instance = createUniqueInstance(
            request.getName() + (request.getCount() == 1 ? "" : i + 1), request.getZone(),
            request.getMachineType(), ImageId.of(request.getImageProject(), request.getImage()),
            request.getKeyName(), request.getTag(), request.isSpot(), request.getCloudGroup());
        instances.add(instance);
      }
      return instances;
    } catch (Exception e) {
      throw new InstanceRequestException(e.getLocalizedMessage());
    }
  }

  /**
   * Create instance.
   *
   * @param name the name of the instance
   * @param zone the zone the instance is located
   * @param machineType the machine type
   * @param image the image type
   * @param keyName the instance key
   * @param tags the instance tags
   * @param spot if it will be preemptible
   * @param cloudGroup The cloudGroupId that the instance will belong
   * @return a newly created instance
   * @throws Exception if something happened
   */
  private GoogleInstance createUniqueInstance(String name, String zone, String machineType,
      ImageId image, String keyName, Map<String, String> tags, boolean spot, String cloudGroup)
      throws Exception {
    log.info("create " + name);
    Path generateTempKey = googleKeyGenerator.getSshPubKey();
    if (StringUtils.isNotBlank(keyName)) {
      try {
        CloudosKey findKey =
            keyManagerService.findKey(keyName, Providers.GOOGLE_COMPUTE_ENGINE, zone);
        if (findKey != null) {
          Entry<Path, Path> generateKeyPairFiles = keyManagerService.generateKeyPairFiles(findKey);
          generateTempKey = generateKeyPairFiles.getValue();
        }
      } catch (Exception e) {
        // TODO: handle exception
        log.error("error getting the key", e);
      }
    }
    Operation createInstanceOperation = googleIntegration.createInstance(zone, name, machineType,
        image, generateTempKey, keyName, tags, spot, cloudGroup);
    List<OperationError> error =
        googleIntegration.blockUntilComplete(createInstanceOperation, TIMEOUT);
    if (CollectionUtils.isNotEmpty(error)) {
      StringBuffer errors = new StringBuffer();
      for (OperationError operationError : error) {
        errors.append(String.format(
            "Error creating instance %s, CODE:[%s] LOCATION[%s] MESSAGE[%s]\n", name,
            operationError.getCode(), operationError.getLocation(), operationError.getMessage()));
      }
      throw new RuntimeException(errors.toString());
    }
    // retrieve the instance
    return createGInstanceFromInstance(googleIntegration.getInstance(zone, name), true, image);
  }

  @Override
  public List<GoogleInstance> fetch(CloudActionRequest fetchRequest) {
    log.info("fetch {}", fetchRequest.toString());
    List<GoogleInstance> list = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(fetchRequest.getNames())) {
      for (String name : fetchRequest.getNames()) {
        GoogleInstance fetchInstance = fetchInstance(name, fetchRequest.getZone());
        if (fetchInstance != null) {
          list.add(fetchInstance);
        }
      }
    } else {
      Iterable<Instance> listInstances = googleIntegration.listInstances();
      listInstances.forEach((item) -> list.add(createGInstanceFromInstance(item, false, null)));
    }
    return list;
  }

  /**
   * Fetch instance by name and region.
   *
   * @param name - instance name
   * @param zone - instance region
   * @return GoogleInstance
   */
  private GoogleInstance fetchInstance(String name, String zone) {
    // google
    Instance instance2 = googleIntegration.getInstance(zone, name);
    if (instance2 != null) {
      GoogleInstance instance = createGInstanceFromInstance(instance2, false, null);
      return instance;
    }
    return null;
  }

  @Override
  public List<CloudActionResponse<GoogleInstance>> start(CloudActionRequest request) {
    log.info("start " + request.getNames());
    List<CloudActionResponse<GoogleInstance>> responseList =
        this.generateResponseListFromRequest(request);
    List<GoogleInstance> fetch = fetch(request);
    List<Operation> operations = new ArrayList<>();
    for (GoogleInstance googleInstance : fetch) {
      Instance instance = googleIntegration.getInstance(googleInstance.getInstance().getZone(),
          googleInstance.getInstance().getProviderId());
      if (instance != null) {
        if (InstanceInfo.Status.TERMINATED.equals(instance.getStatus())
            || InstanceInfo.Status.STOPPING.equals(instance.getStatus())) {
          Operation operation = googleIntegration.startInstance(
              googleInstance.getInstance().getZone(), googleInstance.getInstance().getProviderId());
          operations.add(operation);
        } else {
          String name = instance.getInstanceId().getInstance();
          CloudActionResponse<GoogleInstance> response =
              getResponseByInstanceName(responseList, name);
          response.setStatus(ActionStatus.NOT_EXECUTED);
          response.setErrorMessage(String.format("Instance %s is a wrong status!", name));
        }
      }
    }
    return processOperations(request, responseList, operations);
  }

  @Override
  public List<CloudActionResponse<GoogleInstance>> stop(CloudActionRequest request) {
    log.info("stop " + request.getNames());
    List<CloudActionResponse<GoogleInstance>> responseList =
        this.generateResponseListFromRequest(request);
    List<GoogleInstance> fetch = fetch(request);
    List<Operation> operations = new ArrayList<>();
    for (GoogleInstance googleInstance : fetch) {
      Instance instance = googleIntegration.getInstance(googleInstance.getInstance().getZone(),
          googleInstance.getInstance().getProviderId());
      if (instance != null) {
        if (!(InstanceInfo.Status.STOPPING.equals(instance.getStatus())
            || InstanceInfo.Status.TERMINATED.equals(instance.getStatus()))) {
          Operation operation = googleIntegration.stopInstance(
              googleInstance.getInstance().getZone(), googleInstance.getInstance().getProviderId());
          operations.add(operation);
        } else {
          String name = instance.getInstanceId().getInstance();
          CloudActionResponse<GoogleInstance> response =
              getResponseByInstanceName(responseList, name);
          response.setStatus(ActionStatus.NOT_EXECUTED);
          response.setErrorMessage(String.format("Instance %s is a wrong status!", name));
        }
      }
    }
    return processOperations(request, responseList, operations);
  }

  @Override
  public List<CloudActionResponse<GoogleInstance>> reboot(CloudActionRequest request) {
    log.info("reboot " + request.getNames());
    List<CloudActionResponse<GoogleInstance>> responseList =
        this.generateResponseListFromRequest(request);
    List<GoogleInstance> fetch = fetch(request);
    List<Operation> operations = new ArrayList<>();
    for (GoogleInstance googleInstance : fetch) {
      Instance instance = googleIntegration.getInstance(googleInstance.getInstance().getZone(),
          googleInstance.getInstance().getProviderId());
      if (instance != null) {
        if (!(InstanceInfo.Status.TERMINATED.equals(instance.getStatus())
            || InstanceInfo.Status.PROVISIONING.equals(instance.getStatus())
            || InstanceInfo.Status.STAGING.equals(instance.getStatus()))) {
          Operation operation = googleIntegration.rebootInstance(
              googleInstance.getInstance().getZone(), googleInstance.getInstance().getProviderId());
          operations.add(operation);
        } else {
          String name = instance.getInstanceId().getInstance();
          CloudActionResponse<GoogleInstance> response =
              getResponseByInstanceName(responseList, name);
          response.setStatus(ActionStatus.NOT_EXECUTED);
          response.setErrorMessage(String.format("Instance %s is a wrong status!", name));
        }
      }
    }
    return processOperations(request, responseList, operations);
  }

  @Override
  public List<CloudActionResponse<GoogleInstance>> terminate(CloudActionRequest request) {
    List<String> instanceNames = request.getNames();
    log.info("terminate " + instanceNames);
    List<CloudActionResponse<GoogleInstance>> responseList =
        this.generateResponseListFromRequest(request);

    List<Operation> operations = new ArrayList<>();
    for (String name : instanceNames) {
      try {
        Instance instance = googleIntegration.getInstance(request.getZone(), name);
        if (instance != null) {
          // TODO: verify if it is needed - it was removed because it wasn't really deleting the
          // instance, just terminating
          Operation operation = googleIntegration.deleteInstance(request.getZone(), name);
          operations.add(operation);
        }
      } catch (Exception e) {
        getResponseByInstanceName(responseList, name)
            .setErrorMessage("Error getting instance! " + e.getLocalizedMessage());
      }
    }
    return processOperations(request, responseList, operations);
  }

  @Override
  public boolean isStarted(GoogleInstance googleInstance) {
    log.info("isStarted " + googleInstance.getInstance().getProviderId());
    Instance instance = googleIntegration.getInstance(googleInstance.getInstance().getZone(),
        googleInstance.getInstance().getProviderId());
    if (instance != null) {
      if (InstanceInfo.Status.RUNNING.equals(instance.getStatus())) {
        log.info("true");
        return true;
      }
    }
    log.info("false");
    return false;
  }

  @Override
  public boolean isStopped(GoogleInstance googleInstance) {
    log.info("isStopped " + googleInstance.getInstance().getProviderId());
    Instance instance = googleIntegration.getInstance(googleInstance.getInstance().getZone(),
        googleInstance.getInstance().getProviderId());
    if (instance != null) {
      if (InstanceInfo.Status.STOPPING.equals(instance.getStatus())
          || InstanceInfo.Status.TERMINATED.equals(instance.getStatus())) {
        log.info("true");
        return true;
      }
    }
    log.info("false");
    return false;
  }

  @Override
  public boolean isTerminated(GoogleInstance googleInstance) {
    log.info("isTerminated " + googleInstance.getInstance().getProviderId());
    Instance instance = googleIntegration.getInstance(googleInstance.getInstance().getZone(),
        googleInstance.getInstance().getProviderId());
    if (instance != null) {
      if (InstanceInfo.Status.TERMINATED.equals(instance.getStatus())) {
        log.info("true");
        return true;
      }
    }
    log.info("false");
    return false;
  }

  /**
   * Create a GoogleInstance from a instance.
   *
   * @param instance object
   * @param notify flag to fire notification
   * @param imageId instance imagetype
   * @return GoogleInstance
   */
  private GoogleInstance createGInstanceFromInstance(Instance instance, boolean notify,
      ImageId imageId) {
    GoogleInstance googleInstance = null;
    // verify if already exists
    cloudos.models.Instance cloudosInstance = instanceService.findByProviderAndProviderId(
        Providers.GOOGLE_COMPUTE_ENGINE, instance.getInstanceId().getInstance());
    if (cloudosInstance != null) {
      // get from mongo and update
      googleInstance = googleInstanceRepository.findByInstance(cloudosInstance);
      if (googleInstance == null) {
        // for incomplete data
        googleInstance = new GoogleInstance();
        googleInstance.setInstance(cloudosInstance);
      }
    } else {
      // create the new register
      googleInstance = new GoogleInstance();
      cloudosInstance = new cloudos.models.Instance();
      cloudosInstance.setIncludedDate(new Date());
      cloudosInstance.setProvider(Providers.GOOGLE_COMPUTE_ENGINE);
      cloudosInstance.setProviderId(instance.getInstanceId().getInstance());
      cloudosInstance.setClientStatus(ClientStatus.NOT_INSTALLED);
      googleInstance.setInstance(cloudosInstance);
    }
    // update info
    // region and zone
    if (StringUtils.isBlank(cloudosInstance.getZone())
        || !cloudosInstance.getZone().equals(instance.getInstanceId().getZone())) {
      // just update if it is not set or if it is different
      cloudosInstance.setRegion(googleProviderManagement
          .getRegionFromZone(instance.getInstanceId().getZone()).getRegion());
      cloudosInstance.setZone(instance.getInstanceId().getZone());
    }
    cloudosInstance.setCreationDate(new Date(instance.getCreationTimestamp()));
    cloudosInstance.setMachineType(instance.getMachineType().getType());
    cloudosInstance.setIpAddress(getIpAddress(instance));
    Map<String, String> values = instance.getMetadata().getValues();
    cloudosInstance.setDeploymentId(values.get(cloudos.models.Instance.DEPLOYMENT_ID_TAG));
    String cloudGroupId = values.get(cloudos.models.Instance.CLOUDGROUP_ID_TAG);
    if (StringUtils.isNotBlank(cloudGroupId)) {
      // cloudgroup
      cloudosInstance.setCloudGroupId(cloudGroupId);
    }

    // image
    if (!StringUtils.isNoneBlank(cloudosInstance.getImageType())) {
      // just set if is not set beforehand
      if (imageId == null) {
        imageId = getImageType(instance);
      }
      if (imageId != null) {
        cloudosInstance.setImageProject(imageId.getProject());
        cloudosInstance.setImageType(imageId.getImage());
      }
    }
    final InstanceStatus oldStatus = cloudosInstance.getStatus();
    final InstanceStatus newStatus = getStatus(instance);
    cloudosInstance.setStatus(newStatus);
    cloudosInstance.setDeleted(false);
    cloudosInstance.setName(instance.getInstanceId().getInstance());

    // keys
    String keyName = values.get(KEY_NAME);
    if (StringUtils.isBlank(cloudosInstance.getKeyName()) 
        || !cloudosInstance.getKeyName().equals(keyName)) {
      // just update if it is not set or if it is different
      if (StringUtils.isNotBlank(keyName)) {
        try {
          CloudosKey findKey = keyManagerService.findKey(keyName, Providers.GOOGLE_COMPUTE_ENGINE,
              cloudosInstance.getZone());
          if (findKey != null) {
            cloudosInstance.setKeyName(keyName);
          }
        } catch (Exception e) {
          log.error("Error getting the key", e);
        }
      }
    }

    String keys = getSshKey(instance);
    log.debug("keys: {}", keys);

    boolean notifySpotTerminated = false;
    // verify if is spot
    if (instance.getSchedulingOptions() != null
        && instance.getSchedulingOptions().isPreemptible()) {
      cloudosInstance.setSpot(true);

      if (cloudosInstance.getStatus() != InstanceStatus.TERMINATED) {
        // if not terminated, check if it was preempted

        if (googleIntegration.checkIfWasPreempted(instance)) {
          log.info("################################PREEMPTED####################################");
          log.info(instance.toString());
          log.info("################################PREEMPTED####################################");
          // send message to the manager
          notifySpotTerminated = true;
        }
      }
    }

    // update custom info
    googleInstance.setProject(instance.getInstanceId().getProject());
    Object fromJson = gson.fromJson(gson.toJson(instance), Object.class);
    ((LinkedTreeMap<?, ?>) fromJson).remove("options");
    googleInstance.setObject(fromJson);

    // save
    cloudosInstance = instanceService.save(cloudosInstance);
    googleInstance.setInstance(cloudosInstance);
    googleInstanceRepository.save(googleInstance);

    if (notify) {
      if (oldStatus == null) {
        // create notification of the new instance found on the monitoring service, etc.
        queueManagerService.sendInstanceCreatedMessage(googleInstance);
      }
      if (oldStatus != null && !oldStatus.equals(newStatus)) {
        // create notification of status changed - Instance X had the status updated to Y
        queueManagerService.sendInstanceUpdatedMessage(oldStatus, googleInstance);
      }
    }

    if (notifySpotTerminated) {
      // notify spot instance manager
      queueManagerService.sendSpotInstanceTerminatedMessage(cloudosInstance);
    }

    return googleInstance;
  }

  protected InstanceStatus getStatus(Instance instance) {
    switch (instance.getStatus()) {
      case RUNNING:
        return InstanceStatus.RUNNING;
      case PROVISIONING:
        return InstanceStatus.PROVISIONING;
      case STAGING:
        return InstanceStatus.PROVISIONING;
      case STOPPING:
        return InstanceStatus.STOPPING;
      case TERMINATED:
        // for google terminated = stopped
        return InstanceStatus.STOPPED;
      default:
        break;
    }
    return null;
  }

  /**
   * Get the external ip.
   *
   * @param instance object
   * @return String with the ip
   */
  private String getIpAddress(Instance instance) {
    if (instance != null && !CollectionUtils.isEmpty(instance.getNetworkInterfaces())) {
      NetworkInterface networkInterface = instance.getNetworkInterfaces().get(0);
      if (networkInterface != null && networkInterface.getAccessConfigurations() != null
          && networkInterface.getAccessConfigurations().size() > 0) {
        AccessConfig accessConfig = networkInterface.getAccessConfigurations().get(0);
        if (accessConfig != null) {
          return accessConfig.getNatIp();
        }
      }
    }
    return null;
  }

  /**
   * Get the image type.
   *
   * @param instance object
   * @return ImageId
   */
  private ImageId getImageType(Instance instance) {
    if (instance != null && !CollectionUtils.isEmpty(instance.getAttachedDisks())) {
      AttachedDisk disk = instance.getAttachedDisks().get(0);
      if (disk != null && !CollectionUtils.isEmpty(disk.getLicenses())) {
        LicenseId license = disk.getLicenses().get(0);
        if (license != null) {
          // find the complete image key - with the latest version
          ProviderImageType fullImageKey =
              googleProviderManagement.getFullImageKey(license.getProject(), license.getLicense());
          if (fullImageKey != null) {
            return ImageId.of(fullImageKey.getProject(), fullImageKey.getKey());
          }
          return ImageId.of(license.getProject(), license.getLicense());
        }
      }
    }
    return null;
  }

  /**
   * Get the ssh key.
   *
   * @param instance object
   * @return key
   */
  private String getSshKey(Instance instance) {
    if (instance != null && !MapUtils.isEmpty(instance.getMetadata().getValues())) {
      Map<String, String> map = instance.getMetadata().getValues();
      if (map.containsKey(SSH_KEYS)) {
        return map.get(SSH_KEYS);
      }
    }
    return null;
  }

  /**
   * Process the operations.
   *
   * @param request object
   * @param response list
   * @param operations list
   * @return updated response list
   */
  private List<CloudActionResponse<GoogleInstance>> processOperations(CloudActionRequest request,
      List<CloudActionResponse<GoogleInstance>> response, List<Operation> operations) {
    List<GoogleInstance> fetch;
    if (request.isSynchronous()) {
      try {
        for (Operation operation : operations) {
          List<OperationError> error = googleIntegration.blockUntilComplete(operation, TIMEOUT);
          if (error == null) {
            // success
            CloudActionResponse<GoogleInstance> actionResponse =
                this.getResponseByInstanceName(response, operation.getTargetLink()
                    .substring(operation.getTargetLink().lastIndexOf("/") + 1));
            actionResponse.setStatus(ActionStatus.EXECUTED);
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    fetch = fetch(request);
    if (CollectionUtils.isNotEmpty(fetch)) {
      for (GoogleInstance googleInstance : fetch) {
        CloudActionResponse<GoogleInstance> actionResponse =
            this.getResponseByInstanceName(response, googleInstance.getInstance().getProviderId());
        if (!request.isSynchronous() && StringUtils.isEmpty(actionResponse.getErrorMessage())) {
          actionResponse.setStatus(ActionStatus.EXECUTED_ASYNCH);
        }
        actionResponse.setInstance(googleInstance);
      }
    } else {
      // instances doesnt exists anymore
      List<cloudos.models.Instance> instancesOnDb = instanceService
          .findByProviderAndProviderIdIn(Providers.GOOGLE_COMPUTE_ENGINE, request.getNames());
      for (cloudos.models.Instance instance : instancesOnDb) {
        // set as deleted
        log.info("delete: {}", instance.getProviderId());
        instance.setStatus(InstanceStatus.TERMINATED);
        instance.setDeleted(true);
        instance.setDeletedDate(new Date());
        instanceService.save(instance);
        // notify
        queueManagerService.sendInstanceDeletedMessage(instance);
        //
        GoogleInstance googleInstance = googleInstanceRepository.findByInstance(instance);
        CloudActionResponse<GoogleInstance> actionResponse =
            this.getResponseByInstanceName(response, googleInstance.getInstance().getProviderId());
        actionResponse.setInstance(googleInstance);
      }
    }
    return response;
  }

  /*
   * (non-Javadoc)
   *
   * @see cloudos.provider.InstanceManagement#syncInstances()
   */
  @Override
  public void syncInstances() {
    // get the actual instance list
    List<cloudos.models.Instance> dbInstances =
        instanceService.findAllActiveInstancesByProvider(Providers.GOOGLE_COMPUTE_ENGINE);

    // get the new instance list
    log.debug("syncing google");
    Iterable<Instance> listInstances = googleIntegration.listInstances();

    // create map by provider Id to verify deleted instances afterwards
    Map<String, Instance> mapByProviderId = new HashMap<>();

    for (Instance instance : listInstances) {
      log.debug("syncing: {} - {}", instance.getInstanceId(), instance.getStatus().name());
      // update it
      createGInstanceFromInstance(instance, true, null);
      // add to the map
      mapByProviderId.put(instance.getInstanceId().getInstance(), instance);
    }

    // verify any deleted
    for (cloudos.models.Instance instance : dbInstances) {
      if (!mapByProviderId.containsKey(instance.getProviderId())) {
        // set as deleted
        log.info("delete: {}", instance.getProviderId());
        instance.setDeleted(true);
        instance.setDeletedDate(new Date());
        instanceService.save(instance);
        // notify
        queueManagerService.sendInstanceDeletedMessage(instance);
      }
    }
  }

  @Override
  public void discovery() {
    this.syncInstances();
  }

  @Override
  public GoogleInstance cloneInstance(AbstractInstance originalInstance)
      throws InstanceRequestException {

    String name = originalInstance.getInstance().getProviderId();
    String zone = originalInstance.getInstance().getZone();
    Instance instance = this.googleIntegration.getInstance(zone, name);
    if (instance != null) {

      ImageId imageType = this.getImageType(instance);
      String image = imageType.getImage();
      String imageProject = imageType.getProject();

      try {
        String instanceClonedName = originalInstance.getInstance().getName() + "cloned";
        boolean spot = originalInstance.getInstance().isSpot();
        Instance clonedInstance =
            googleIntegration.cloneInstance(instanceClonedName, instance, spot, null, TIMEOUT);
        // retrieve the instance
        return createGInstanceFromInstance(clonedInstance, true, ImageId.of(imageProject, image));
      } catch (Exception e) {
        log.error("Error cloning an instance!", e);
        throw new InstanceRequestException(
            "Error cloning an instance! ex: " + e.getLocalizedMessage());
      }
    }
    return null;
  }
}
