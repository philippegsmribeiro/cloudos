package cloudos.instances;

import cloudos.Providers;
import cloudos.exceptions.InvalidRequestException;
import cloudos.exceptions.NotImplementedException;
import cloudos.models.AbstractInstance;
import cloudos.models.CloudActionRequest;
import cloudos.models.CloudCreateRequest;
import cloudos.queue.QueueManagerService;
import cloudos.security.RequestScope;

import java.util.List;

import lombok.extern.log4j.Log4j2;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CloudManagerService {

  private static final String SYSTEM_USER = "SYSTEM";
  @Autowired
  private AmazonInstanceManagement amazonInstanceManagement;
  @Autowired
  private GoogleInstanceManagement googleInstanceManagement;
  @Autowired
  QueueManagerService queueManagerService;
  @Autowired
  private RequestScope requestScope;

  /**
   * Get management class.
   *
   * @param providers a cloud provider
   * @return a instance of the InstanceManagement
   * @throws NotImplementedException whenever the service is not implemented for the provider
   */
  @SuppressWarnings("Duplicates")
  private InstanceManagement<?> getManagement(Providers providers) throws NotImplementedException {
    switch (providers) {
      case AMAZON_AWS:
        return amazonInstanceManagement;
      case GOOGLE_COMPUTE_ENGINE:
        return googleInstanceManagement;
      default:
        break;
    }
    throw new NotImplementedException();
  }

  /**
   * The method createInstance is responsible for creating instances in the given cloud provider.
   *
   * @param request A CloudAction request
   * @throws NotImplementedException whenever the service is not implemented for the provider
   * @throws InstanceRequestException whenever there's an exception in creating the instance
   */
  public List<? extends AbstractInstance> createInstances(CloudCreateRequest request)
      throws NotImplementedException, InstanceRequestException {
    return getManagement(request.getProvider()).createInstances(request);
  }

  /**
   * The method stopInstances is responsible for listing all the instances specified in the request.
   *
   * @param request A CloudAction request
   * @throws NotImplementedException whenever the service is not implemented for the provider
   * @throws InstanceRequestException whenever there's an exception in creating the instance
   */
  public List<? extends AbstractInstance> listInstances(CloudActionRequest request)
      throws NotImplementedException, InstanceRequestException {
    return getManagement(request.getProvider()).fetch(request);
  }

  /**
   * The method stopInstances is responsible for terminating the instances specified in the request.
   *
   * @param request A CloudAction request
   * @throws NotImplementedException whenever the service is not implemented for the provider
   * @throws InstanceRequestException whenever there's an exception in creating the instance
   * @throws InvalidRequestException whenever the request is invalid
   */
  public List<?> terminateInstances(CloudActionRequest request)
      throws NotImplementedException, InvalidRequestException, InstanceRequestException {
    validateInstancesRequest(request);
    fireNotification(request);
    return getManagement(request.getProvider()).terminate(request);
  }

  /**
   * The method start is responsible for starting the instances specified in the request.
   *
   * @param request A CloudAction request
   * @throws NotImplementedException whenever the service is not implemented for the provider
   * @throws InstanceRequestException whenever there's an exception in creating the instance
   * @throws InvalidRequestException whenever the request is invalid
   */
  public List<?> startInstances(CloudActionRequest request)
      throws NotImplementedException, InvalidRequestException, InstanceRequestException {
    validateInstancesRequest(request);
    fireNotification(request);
    return getManagement(request.getProvider()).start(request);
  }

  /**
   * The method stopInstances is responsible for stopping the instances specified in the request.
   *
   * @param request A CloudAction request
   * @throws NotImplementedException whenever the service is not implemented for the provider
   * @throws InstanceRequestException whenever there's an exception in creating the instance
   * @throws InvalidRequestException whenever the request is invalid
   */
  public List<?> stopInstances(CloudActionRequest request)
      throws NotImplementedException, InvalidRequestException, InstanceRequestException {
    validateInstancesRequest(request);
    fireNotification(request);
    return getManagement(request.getProvider()).stop(request);
  }

  /**
   * The method rebootInstances is responsible for rebooting the instances specified in the request.
   *
   * @param request A CloudAction request
   * @throws NotImplementedException whenever the service is not implemented for the provider
   * @throws InstanceRequestException whenever there's an exception in creating the instance
   * @throws InvalidRequestException whenever the request is invalid
   */
  public List<?> rebootInstances(CloudActionRequest request)
      throws NotImplementedException, InvalidRequestException, InstanceRequestException {
    validateInstancesRequest(request);
    fireNotification(request);
    return getManagement(request.getProvider()).reboot(request);
  }

  /**
   * Validate the request.
   *
   * @param request A CloudAction request
   * @return whether the request is valid or not
   * @throws InvalidRequestException whenever the request is invalid
   */
  private boolean validateInstancesRequest(CloudActionRequest request)
      throws InvalidRequestException {
    if (CollectionUtils.isEmpty(request.getNames())) {
      throw new InvalidRequestException("Names required!");
    }
    return true;
  }

  /**
   * Fire a notification about an action.
   *
   * @param request A CloudAction request
   */
  private void fireNotification(CloudActionRequest request) {
    log.info("notification");
    String userName = requestScope.getUserName();
    if (StringUtils.isBlank(userName)) {
      userName = SYSTEM_USER;
    }
    queueManagerService.sendCloudActionRequestNotification(request, userName);
  }
}
