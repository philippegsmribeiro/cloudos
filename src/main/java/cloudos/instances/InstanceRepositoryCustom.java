package cloudos.instances;

import cloudos.Providers;
import cloudos.models.Instance;
import cloudos.models.costanalysis.InstanceTypeCost;
import java.util.Date;
import java.util.List;

/**
 * Custom spring data repository interface for {@link Instance}.
 */
interface InstanceRepositoryCustom {

  /**
   * Retrieves the top instance types ordered by cost in descending order.
   *
   * @param limit the query limit (optional)
   * @return the top instance types ordered by cost in descending order
   */
  List<InstanceTypeCost> findTopInstanceTypesOrderByCostDesc(Integer limit);

  /**
   * Retrieves the instances by a set of filters and valid between a date range.
   *
   * @param provider the cloud provider
   * @param machineType the machine type
   * @param startIncl optional, the start date (inclusive)
   * @param endIncl optional, the end date (inclusive)
   * @return a list of instances
   */
  List<Instance> findByProviderAndMachineTypeAndDateBetween(Providers provider, String machineType,
      Date startIncl, Date endIncl);
}
