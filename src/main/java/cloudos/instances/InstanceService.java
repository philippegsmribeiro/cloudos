package cloudos.instances;

import cloudos.Providers;
import cloudos.amazon.AmazonInstanceRepository;
import cloudos.elasticsearch.services.MetricbeatService;
import cloudos.google.GoogleInstanceRepository;
import cloudos.keys.cloudoskey.CloudosKey;
import cloudos.microsoft.AzureInstanceRepository;
import cloudos.models.AbstractInstance;
import cloudos.models.ClientStatus;
import cloudos.models.Instance;
import cloudos.models.InstanceStatus;
import cloudos.models.cloudgroup.CloudGroupFilterRequest;
import cloudos.models.costanalysis.InstanceTypeCost;
import cloudos.utils.DateUtil;
import com.amazonaws.util.StringUtils;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.constraints.NotNull;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class InstanceService {

  @Autowired
  private InstanceRepository instanceRepository;
  @Autowired
  private AmazonInstanceRepository amazonInstanceRepository;
  @Autowired
  private GoogleInstanceRepository googleInstanceRepository;
  @Autowired
  private AzureInstanceRepository azureInstanceRepository;
  @Autowired
  private MetricbeatService metricbeatService;

  /**
   * Retrieve a specific instance from a cloudos instance object.
   *
   * @param instance - cloudos instance
   * @return specific instance that extends AbstractInstance
   */
  public AbstractInstance getAbstractInstanceFromInstance(Instance instance) {
    Providers provider = instance.getProvider();
    AbstractInstance abstractInstance = null;
    switch (provider) {
      case AMAZON_AWS:
        abstractInstance = amazonInstanceRepository.findByInstance(instance);
        break;
      case GOOGLE_COMPUTE_ENGINE:
        abstractInstance = googleInstanceRepository.findByInstance(instance);
        break;
      case MICROSOFT_AZURE:
        abstractInstance = azureInstanceRepository.findByInstance(instance);
        break;
      default:
        break;
    }
    return abstractInstance;
  }


  /**
   * Find instances which has the specific key and cloudos client was not installed yet.
   *
   * @param key - instance key
   * @return List of Instances
   */
  public List<AbstractInstance> findNotInstalledClientInstancesByKey(CloudosKey key) {
    List<Instance> instances = instanceRepository.findByClientStatusInAndKeyNameAndDeleted(
        Arrays.asList(ClientStatus.ERROR, ClientStatus.NOT_INSTALLED), key.getKeyName(), true);
    List<AbstractInstance> instancesFound = new ArrayList<>();
    for (Instance instance : instances) {
      AbstractInstance abstractInstance = getAbstractInstanceFromInstance(instance);
      if (abstractInstance != null) {
        instancesFound.add(abstractInstance);
      }
    }
    return instancesFound;
  }


  /**
   * Updates the client status for a instance.
   *
   * @param instance - instance
   * @param status - new status
   */
  public void setClientStatus(Instance instance, ClientStatus status) {
    instance.setClientStatus(status);
    instance.setClientInstalledCheckDate(DateUtil.getNow());
    instanceRepository.save(instance);
  }

  /**
   * Find all active instances.
   *
   * @return list of Instances
   */
  public List<Instance> findAllActiveInstances() {
    return instanceRepository.findByDeleted(false);
  }

  /**
   * Find all instances.
   *
   * @return list of Instances
   */
  public List<Instance> findAllInstances() {
    return instanceRepository.findAll();
  }

  /**
   * Method that returns a Page of Instance.
   * @param filter request generic by search instance.
   * @param pageable paremeter for pagination and sort
   * @return Page the Instance
   */
  public Page<Instance> findInstancesByFilter(Predicate filter, Pageable pageable) {
    return instanceRepository.findAll(filter, pageable);
  }
  
  /**
   * Method that returns a list of Instance.
   * @param filter request generic by search instance.
   * @return List the Instance
   */
  public List<Instance> findInstancesByFilter(Predicate filter) {
    return (List<Instance>) instanceRepository.findAll(filter);
  }

  /**
   * Find all active instances of a provider.
   *
   * @param provider filter
   * @return list of Instances
   */
  public List<Instance> findAllActiveInstancesByProvider(Providers provider) {
    return instanceRepository.findByProviderAndDeleted(provider, false);
  }

  /**
   * Find all active and not stopped instances.
   *
   * @return list of Instances
   */
  public List<Instance> findAllActiveAndNotStoppedInstances() {
    List<InstanceStatus> stoppedStatus =
        Arrays.asList(InstanceStatus.STOPPED, InstanceStatus.STOPPING, InstanceStatus.TERMINATED);
    List<Instance> instances = this.findAllActiveInstances();
    return instances.stream().filter(i -> {
      return !stoppedStatus.contains(i.getStatus());
    }).collect(Collectors.toList());
  }

  /**
   * Find all active and not stopped instances of a provider.
   *
   * @param provider filter
   * @return list of Instances
   */
  public List<Instance> findAllActiveAndNotStoppedInstancesByProvider(Providers provider) {
    List<InstanceStatus> stoppedStatus =
        Arrays.asList(InstanceStatus.STOPPED, InstanceStatus.STOPPING, InstanceStatus.TERMINATED);
    List<Instance> instances = this.findAllActiveInstancesByProvider(provider);
    return instances.stream().filter(i -> {
      return !stoppedStatus.contains(i.getStatus());
    }).collect(Collectors.toList());
  }

  /**
   * Find all active and not stopped instances of a provider.
   *
   * @param provider filter
   * @return list of Instances
   */
  public List<Instance> findAllActiveAndNotStoppedInstancesByProviderAndRegion(Providers provider,
      String region) {
    List<InstanceStatus> stoppedStatus =
        Arrays.asList(InstanceStatus.STOPPED, InstanceStatus.STOPPING, InstanceStatus.TERMINATED);
    List<Instance> instances = this.findAllActiveInstancesByProviderAndRegion(provider, region);
    return instances.stream().filter(i -> !stoppedStatus.contains(i.getStatus()))
        .collect(Collectors.toList());
  }

  /**
   * Find all active instances of a provider and region.
   *
   * @param provider filter
   * @param region filter
   * @return list of Instances
   */
  public List<Instance> findAllActiveInstancesByProviderAndRegion(Providers provider,
      String region) {
    return instanceRepository.findByProviderAndDeletedAndRegion(provider, false, region);
  }

  /**
   * Find all instances (active or not) of a provider filtering by instance names.
   *
   * @param provider filter
   * @param names to filter
   * @return list of Instances
   */
  public List<Instance> findByProviderAndProviderIdIn(Providers provider, List<String> names) {
    return instanceRepository.findByProviderAndProviderIdIn(provider, names);
  }


  /**
   * Find by id.
   *
   * @param cloudosId identifier
   * @return Instance
   */
  public Instance findById(String cloudosId) {
    return instanceRepository.findOne(cloudosId);
  }


  /**
   * Find by provider and provider Id.
   *
   * @param provider - filter
   * @param providerid - identifier on the provider
   * @return Instance
   */
  public Instance findByProviderAndProviderId(Providers provider, String providerid) {
    List<Instance> instances = instanceRepository.findByProviderAndProviderId(provider, providerid);
    if (CollectionUtils.isNotEmpty(instances)) {
      if (instances.size() == 1) {
        // it should be unique
        return instances.get(0);
      }
      // if not unique - delete others
      List<Instance> instancesToBeDelete = new ArrayList<>();
      for (int i = 1; i < instances.size(); i++) {
        instancesToBeDelete.add(instances.get(i));
      }
      instanceRepository.delete(instancesToBeDelete);
      // return first
      return instances.get(0);
    }
    return null;
  }

  /**
   * Find the instance by the provided id.
   *
   * @param providerId the provider generated id
   * @return a instance if found, null otherwise
   */
  public Instance findByProviderId(String providerId) {
    return this.instanceRepository.findByProviderId(providerId);
  }

  /**
   * Save an instance.
   *
   * @param instance to be saved
   * @return saved instance
   */
  public Instance save(Instance instance) {
    try {
      return instanceRepository.save(instance);
    } catch (DuplicateKeyException e) {
      throw e;
    }
  }

  /**
   * Save a list of instances.
   *
   * @param instances to be saved
   * @return saved instances
   */
  public List<Instance> save(List<Instance> instances) {
    return instanceRepository.save(instances);
  }

  /**
   * Delete an instance.
   *
   * @param id instance identifier
   */
  public void delete(String id) {
    instanceRepository.delete(id);
  }

  /**
   * Find instances which was created after certain date.
   *
   * @param date filter
   * @return list of Instances
   */
  public List<Instance> findByCreationDateAfter(Date date) {
    return instanceRepository.findByCreationDateAfter(date);
  }
  
  /**
   * Find instances which was created before certain date.
   *
   * @param date filter
   * @return list of Instances
   */
  public List<Instance> findByCreationDateBefore(Date date) {
    return instanceRepository.findByCreationDateBefore(date);
  }
  
  /**
   * Find by instance ids.
   *
   * @param ids list
   * @return Instance list
   */
  public List<Instance> findAllById(List<String> ids) {
    return instanceRepository.findByIdIn(ids);
  }

  /**
   * Find by cloud group ids.
   *
   * @param ids list
   * @return Instance list
   */
  public List<Instance> findAllByCloudGroupId(List<String> ids) {
    return instanceRepository.findByCloudGroupIdIn(ids);
  }
  

  /**
   * Find by cloud group id.
   *
   * @param id cloud group
   * @return Instance list
   */
  public List<Instance> findAllByCloudGroupId(String id) {
    return instanceRepository.findByCloudGroupId(id);
  }

  /**
   * Find all the instances that do not have a cloud group. The instance should not have been
   * deleted.
   *
   * @param request the request to filter the instances that match those parameters
   * @return a list of instances that do not contain a cloud group
   */
  public List<Instance> findAllWithoutCloudGroup(@NotNull CloudGroupFilterRequest request) {

    List<Instance> instances = null;
    // check if the request's provider is valid.
    if (request.getProvider() != null) {
      instances = this.instanceRepository
          .findByCloudGroupIdIsNullAndProviderAndDeleted(request.getProvider(), false);

      // apply the other filters ...

      // filter by the region
      if (!StringUtils.isNullOrEmpty(request.getRegion())) {
        instances =
            instances.stream().filter(instance -> request.getRegion().equals(instance.getRegion()))
                .collect(Collectors.toList());
      }
      // filter by the availability zone
      if (!StringUtils.isNullOrEmpty(request.getAvailabilityZone())) {
        instances = instances.stream()
            .filter(instance -> request.getAvailabilityZone().equals(instance.getZone()))
            .collect(Collectors.toList());
      }
      // find by the instance type
      if (!StringUtils.isNullOrEmpty(request.getInstanceType())) {
        instances = instances.stream()
            .filter(instance -> request.getInstanceType().equals(instance.getMachineType()))
            .collect(Collectors.toList());
      }
      // find by security group
      if (!StringUtils.isNullOrEmpty(request.getSecurityGroup())) {
        instances = instances.stream()
            .filter(instance -> request.getSecurityGroup().equals(instance.getSecurityGroup()))
            .collect(Collectors.toList());
      }
      // find by key
      if (!StringUtils.isNullOrEmpty(request.getKey())) {
        instances = instances.stream()
            .filter(instance -> request.getKey().equals(instance.getKeyName()))
            .collect(Collectors.toList());
      }
      // find instance that are ACTIVE and STOPPED resources.
      instances = instances.stream()
                           .filter(instance -> InstanceStatus.RUNNING.equals(instance.getStatus()) 
                                            || InstanceStatus.STOPPED.equals(instance.getStatus()))
                           .collect(Collectors.toList());
    }
    return instances;
  }


  /**
   * Count the number of instances in the cloud group.
   * 
   * @param cloudGroupId identifier for the cloud group
   * @return number of instances
   */
  public Integer countInstancesByCloudGroupId(String cloudGroupId) {
    return instanceRepository.countByCloudGroupIdAndDeleted(cloudGroupId, false);
  }

  /**
   * Find by deleted and status not equals a collection of {@link InstanceStatus}.
   *
   * @return Instance list or an empty list
   */
  public List<Instance> findAllActiveAndNotTerminatedInstances() {
    return instanceRepository.findByDeletedAndStatusNotIn(Boolean.FALSE,
        Arrays.asList(InstanceStatus.TERMINATED));
  }

  /**
   * Remove all the ElasticSearch data and repository entries related to the instances deleted
   * before the given timewindow.
   *
   * @param deletedTimewindowInMinutes - the timewindow between now end the time to filter the
   *        deleted instances.
   */
  public void removeDeletedInstancesEntries(Integer deletedTimewindowInMinutes) {
    Date deletedDateTime = decreaseTimewindowFromCurrentDateTime(deletedTimewindowInMinutes);
    List<Instance> deletedInstances = instanceRepository.findByDeletedDateLessThan(deletedDateTime);

    for (Instance instance : deletedInstances) {
      metricbeatService.deleteRecordsByInstanceName(instance);
    }
  }


  /**
   * Decrease the timewindow value in minutes from the current datetime.
   *
   * @param deletedTimewindowInMinutes the timewindow between now end the time to filter the deleted
   *        instances.
   * @return the current datetime minus the minutes in the given timewindow parameter.
   */
  private Date decreaseTimewindowFromCurrentDateTime(Integer deletedTimewindowInMinutes) {
    ZoneId defaultZoneId = ZoneId.systemDefault();
    LocalDateTime localDateTime = new Date().toInstant().atZone(defaultZoneId).toLocalDateTime();
    localDateTime = localDateTime.minus(deletedTimewindowInMinutes, ChronoUnit.MINUTES);
    return Date.from(localDateTime.atZone(defaultZoneId).toInstant());
  }

  /**
   * Retrieves the instances by a set of filters.
   *
   * @param provider the cloud provider
   * @param machineType the machine type
   * @param startIncl optional, the start date (inclusive)
   * @param endIncl optional, the end date (inclusive)
   * @return a list of instances
   */
  public List<Instance> findByProviderAndMachineTypeAndDateBetween(@NotNull Providers provider,
      @NotNull String machineType, Date startIncl, Date endIncl) {
    List<Instance> result = instanceRepository.findByProviderAndMachineTypeAndDateBetween(provider,
        machineType, startIncl, endIncl);
    return result;
  }


  /**
   * Retrieve instance types order by cost desc.
   * @param limit number max of instances 
   * @return List of Instances
   */
  public List<InstanceTypeCost> findTopInstanceTypesOrderByCostDesc(int limit) {
    return instanceRepository.findTopInstanceTypesOrderByCostDesc(limit);
  }

  /**
   * Retrieve all using the builder.
   * @param builder filter
   * @return iterable of instances
   */
  public Iterable<Instance> findAll(BooleanBuilder builder) {
    return instanceRepository.findAll(builder);
  }

}
