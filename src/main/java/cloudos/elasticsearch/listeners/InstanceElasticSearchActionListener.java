package cloudos.elasticsearch.listeners;

import cloudos.instances.InstanceService;
import cloudos.models.Instance;

import lombok.extern.log4j.Log4j2;

import org.elasticsearch.action.ActionListener;
import org.elasticsearch.index.reindex.BulkByScrollResponse;

/**
 * Listener to handle ElasticSearch delete method with asynchronous mode.
 *
 * @author Rogério Souza
 *
 */
@Log4j2
public class InstanceElasticSearchActionListener implements ActionListener<BulkByScrollResponse> {

  private final Instance instance;
  private InstanceService instanceService;

  public InstanceElasticSearchActionListener(Instance instance, InstanceService instanceService) {
    this.instance = instance;
    this.instanceService = instanceService;
  }

  /**
   * After deleting records from ElasticSearch data the Listener will remove the instance entry from
   * its repository.
   *
   * @param response - BulkByScrollResponse delete response object.
   */
  @Override
  public void onResponse(BulkByScrollResponse response) {
    instanceService.delete(instance.getId());
    log.info("#### Instance ID:{} NAME:{} has been delete", instance.getId().toString(),
        instance.getName());
  }

  /**
   * On failure it will log the error and continue the flow without throwing exceptions, since that
   * error should not stop the deleting process.
   *
   * @param e - The Exception that has caused the failure.
   */
  @Override
  public void onFailure(Exception e) {
    log.error("#### Instance ID:{} NAME:{} entries haven't been deleted from ElasticSearch data",
        instance.getId().toString(), instance.getName(), e);
  }

}
