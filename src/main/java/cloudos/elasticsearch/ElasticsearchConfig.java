package cloudos.elasticsearch;

import java.net.InetAddress;
import java.net.UnknownHostException;

import lombok.extern.log4j.Log4j2;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

/**
 * Spring configuration that builds a TransportClient as a Spring bean to handle Elasticsearch API
 * connection and operations. Created by rogerio.souza on 05/14/17.
 */
@Configuration
@Log4j2
public class ElasticsearchConfig {

  private static final String CLUSTER_NAME_PROPERTY = "cluster.name";

  @Value("${cloudos.data.elasticsearch.server}")
  private String elasticsearchServer;

  @Value("${cloudos.data.elasticsearch.api.port}")
  private Integer elasticsearchAPIport;

  @Value("${cloudos.data.elasticsearch.clusterName}")
  private String clusterName;

  /**
   * Builds a TransportClient as a Spring bean to handle Elasticsearch API connection and
   * operations.
   *
   * @return
   * @throws UnknownHostException
   */
  @Bean(destroyMethod = "close")
  @Lazy
  public TransportClient transportClient() throws UnknownHostException {
    System.setProperty("es.set.netty.runtime.available.processors", "false");
    Settings settings = Settings.builder().put(CLUSTER_NAME_PROPERTY, clusterName).build();
    TransportClient transportClient = new TransportClientImpl(settings);
    transportClient.addTransportAddress(new InetSocketTransportAddress(
        InetAddress.getByName(elasticsearchServer), elasticsearchAPIport));
    return transportClient;
  }

  /** Just to be sure the close is being called */
  public class TransportClientImpl extends PreBuiltTransportClient {

    public TransportClientImpl(Settings settings) {
      super(settings);
    }

    @Override
    public void close() {
      super.close();
      log.info("Closing Transporter!");
    }
  }
}
