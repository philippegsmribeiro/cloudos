package cloudos.elasticsearch.services;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.ArrayUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.DeleteByQueryAction;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.MultiBucketsAggregation;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.histogram.ExtendedBounds;
import org.elasticsearch.search.aggregations.bucket.histogram.InternalDateHistogram;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.avg.Avg;
import org.elasticsearch.search.aggregations.metrics.avg.AvgAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.avg.InternalAvg;
import org.elasticsearch.search.aggregations.metrics.max.Max;
import org.elasticsearch.search.sort.SortOrder;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;

import cloudos.elasticsearch.fields.SharedBeatFields;
import cloudos.elasticsearch.listeners.InstanceElasticSearchActionListener;
import cloudos.elasticsearch.model.DateHistogramDataPoint;
import cloudos.elasticsearch.model.KibanaField;
import cloudos.elasticsearch.model.KibanaSearchParameters;
import cloudos.elasticsearch.model.ValueDataPoint;
import cloudos.instances.InstanceService;
import cloudos.models.Instance;
import cloudos.queue.QueueService;
import lombok.extern.log4j.Log4j2;

/**
 * Represent a Beat service that handles Elasticsearch search and client connection, implements
 * generic search operations and fetch all Beat data according its subclass beat index. Created by
 * rogerio.souza on 05/14/17.
 *
 */
@Log4j2
public abstract class AbstractElasticsearchService {


  protected static final String AVG_AGGREGATION = "avgAggregation";
  protected static final String MIN_AGGREGATION = "minAggregation";
  protected static final String MAX_AGGREGATION = "maxAggregation";
  protected static final String ROOT_AGGREGATION = "rootAggregation";
  protected static final String TOP_HITS_AGGREGATION = "topHitsAggregation";
  private static final String RANGE_UNIT = "@timestamp";
  private static final String RANGE_FORMAT = "epoch_millis";
  protected static final Integer MAX_AGGREGATION_SIZE = 999999999;
  private static final String DELETE_ALL_INDEXES = "_all";

  @Autowired
  protected QueueService queueService;

  @Autowired
  private TransportClient transportClient;

  @Autowired
  private InstanceService instanceService;

  /**
   * Abstract method to me implemented by its subclasses to set the index to be searched.
   *
   * @return
   */
  protected abstract String getIndex();

  /**
   * Build a DFS search request to be used from its subclasses in order to implement specific search
   * criteria based on index parameter.
   *
   * @return
   * @throws UnknownHostException
   */
  protected SearchRequestBuilder buildDFSQuerySearchRequest() throws UnknownHostException {
    return transportClient.prepareSearch(getIndex()).setSearchType(SearchType.DFS_QUERY_THEN_FETCH);
  }

  /**
   * Retrieve all hits and MAX aggregations based on criteria KibanaSearchParameters and implemented
   * index
   *
   * @param parameters
   * @return
   * @throws UnknownHostException
   */
  protected List<DateHistogramDataPoint> searchMaxDateHistogram(KibanaSearchParameters parameters)
      throws UnknownHostException {

    SearchRequestBuilder searchRequestBuilder = buildDFSQuerySearchRequest();

    searchRequestBuilder.setSize(0);
    applyMatchQueryAndTimestampRange(searchRequestBuilder, parameters);
    searchRequestBuilder.addAggregation(buildMaxDateHistogramAggregation(parameters));

    SearchResponse searchResponse = searchRequestBuilder.execute().actionGet();
    InternalDateHistogram agg = searchResponse.getAggregations().get(ROOT_AGGREGATION);

    List<DateHistogramDataPoint> dataPointList = new ArrayList<>();

    for (MultiBucketsAggregation.Bucket bucket : agg.getBuckets()) {
      Date timestamp = ((DateTime) bucket.getKey()).toDate();

      Max max = bucket.getAggregations().get(MAX_AGGREGATION);

      dataPointList.add(new DateHistogramDataPoint(timestamp, max.getValue()));
    }

    return dataPointList;
  }

  /**
   * Max DateHistogram aggregation - builds a DateHistogramAggregationBuilder based on
   * AggregationFieldName and AggregationInterval.
   *
   * @param parameters
   * @return String JSON
   */
  private DateHistogramAggregationBuilder buildMaxDateHistogramAggregation(
      KibanaSearchParameters parameters) {
    return AggregationBuilders.dateHistogram(ROOT_AGGREGATION).field(RANGE_UNIT)
        .dateHistogramInterval(parameters.getAggregationInterval()).minDocCount(0)
        .subAggregation(
            AggregationBuilders.max(MAX_AGGREGATION).field(parameters.getAggregationFieldName()))
        .extendedBounds(new ExtendedBounds(parameters.getRangeFrom(), parameters.getRangeTo()));
  }

  /**
   * Retrieve all hits and aggregations based on criteria KibanaSearchParameters and implemented
   * index.
   *
   * @param parameters
   * @return
   * @throws UnknownHostException
   */
  protected List<DateHistogramDataPoint> searchAvgDateHistogram(KibanaSearchParameters parameters)
      throws UnknownHostException {

    SearchRequestBuilder searchRequestBuilder = buildDFSQuerySearchRequest();

    searchRequestBuilder.setSize(0);
    applyMatchQueryAndTimestampRange(searchRequestBuilder, parameters);
    searchRequestBuilder.addAggregation(buildAvgDateHistogramAggregation(parameters));

    SearchResponse searchResponse = searchRequestBuilder.execute().actionGet();
    InternalDateHistogram agg = searchResponse.getAggregations().get(ROOT_AGGREGATION);

    List<DateHistogramDataPoint> dataPointList = new ArrayList<>();

    for (MultiBucketsAggregation.Bucket bucket : agg.getBuckets()) {
      Date timestamp = ((DateTime) bucket.getKey()).toDate();

      Avg avg = bucket.getAggregations().get(AVG_AGGREGATION);
      dataPointList.add(new DateHistogramDataPoint(timestamp, avg.getValue()));
    }

    return dataPointList;
  }

  /**
   * Retrieve all hits and avg aggregations based on criteria KibanaSearchParameters and implemented
   * index.
   *
   * @param parameters
   * @return
   * @throws UnknownHostException
   */
  protected Double searchFieldAvg(KibanaSearchParameters parameters) throws UnknownHostException {

    SearchRequestBuilder searchRequestBuilder = buildDFSQuerySearchRequest();

    searchRequestBuilder.setSize(0);
    applyMatchQueryAndTimestampRange(searchRequestBuilder, parameters);
    searchRequestBuilder.addAggregation(buildTotalAvgAggregation(parameters));

    SearchResponse searchResponse = searchRequestBuilder.execute().actionGet();
    Avg avg = searchResponse.getAggregations().get(AVG_AGGREGATION);
    return avg.getValue();
  }

  /**
   * Builds a queryJson based on kibana elasrticsearch parameters to filter and match by multiple
   * fields and multiple values with a timestamp range
   *
   * <p>
   * Frequently filtered by beat hostname/name for example
   *
   * <p>
   * https://www.elastic.co/blog/lost-in-translation-boolean-operations-and-filters-in-the-bool-query
   *
   * @param parameters
   */
  protected void applyMatchQueryAndTimestampRange(SearchRequestBuilder searchRequestBuilder,
      KibanaSearchParameters parameters) {
    StringBuilder json = new StringBuilder();
    json.append("{");
    json.append("\"bool\":{");
    json.append("	\"must\":[");
    json.append(buildJSONFilterFieldsAndTimestampRange(parameters));
    if (!parameters.getMatchFields().isEmpty()) {
      json.append(",");
      json.append(buildJSONMatchFields(parameters));
    }
    json.append("]}");
    json.append("}");
    searchRequestBuilder.setQuery(QueryBuilders.wrapperQuery(json.toString()));
  }

  /**
   * Builds a queryJson based on kibana elasrticsearch parameters to filter and match by multiple
   * fields and multiple values
   *
   * <p>
   * Frequently filtered by beat hostname/name for example
   *
   * <p>
   * https://www.elastic.co/blog/lost-in-translation-boolean-operations-and-filters-in-the-bool-query
   *
   * @param parameters
   */
  protected void applyMatchQuery(SearchRequestBuilder searchRequestBuilder,
      KibanaSearchParameters parameters) {
    StringBuilder json = new StringBuilder();
    json.append("{");
    json.append("\"bool\":{");
    json.append("	\"must\":[");
    json.append(buildJSONFilterFields(parameters));
    if (!parameters.getMatchFields().isEmpty()) {
      json.append(",");
      json.append(buildJSONMatchFields(parameters));
    }
    json.append("]}");
    json.append("}");
    searchRequestBuilder.setQuery(QueryBuilders.wrapperQuery(json.toString()));
  }

  /**
   * Builds a queryJson based on kibana elasrticsearch parameters to search for hit with existing
   * fields
   *
   * @param parameters
   */
  protected void applyExistingFields(SearchRequestBuilder searchRequestBuilder,
      KibanaSearchParameters parameters) {
    BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
    for (KibanaField field : parameters.getFilterFields()) {
      BoolQueryBuilder innerQueryBuilder = QueryBuilders.boolQuery();
      for (String value : field.getValues()) {
        innerQueryBuilder.should()
            .add(QueryBuilders.matchQuery(field.getName(), value).operator(Operator.AND));
      }
      queryBuilder.must(innerQueryBuilder);
    }
    queryBuilder.must(QueryBuilders.existsQuery(parameters.getAggregationFieldName()));
    searchRequestBuilder.setQuery(queryBuilder);
  }

  /**
   * Builds a queryJson based on kibana elasrticsearch fields in orther to allow filtering multiple
   * fields and multiple values with a timestamp range
   *
   * <p>
   * https://www.elastic.co/blog/lost-in-translation-boolean-operations-and-filters-in-the-bool-query
   *
   * @param parameters
   */
  private String buildJSONFilterFieldsAndTimestampRange(KibanaSearchParameters parameters) {
    StringBuilder json = new StringBuilder();
    json.append("{");
    json.append("\"bool\":{");
    json.append("	\"must\":[");
    json.append(buildTimestampRangeFilterJSON(parameters));
    for (KibanaField field : parameters.getFilterFields()) {
      json.append(",{");
      json.append("	\"bool\":{");
      json.append("		\"should\":[");
      for (String value : field.getValues()) {
        json.append("{			\"match\":{\"" + field.getName() + "\" : \"" + value + "\"}},");
      }
      json = new StringBuilder(json.substring(0, json.length() - 1));
      json.append("	]}");
      json.append("}");
    }
    json.append("]}");
    json.append("}");
    return json.toString();
  }

  /**
   * Builds a queryJson based on kibana elasrticsearch fields in orther to allow filtering multiple
   * fields and multiple values range
   *
   * <p>
   * https://www.elastic.co/blog/lost-in-translation-boolean-operations-and-filters-in-the-bool-query
   *
   * @param parameters
   */
  protected String buildJSONFilterFields(KibanaSearchParameters parameters) {
    StringBuilder json = new StringBuilder();
    json.append("{");
    json.append("\"bool\":{");
    json.append("	\"must\":[");
    for (KibanaField field : parameters.getFilterFields()) {
      json.append("{");
      json.append("	\"bool\":{");
      json.append("		\"should\":[");
      for (String value : field.getValues()) {
        json.append("{			\"match\":{\"" + field.getName() + "\" : \"" + value + "\"}},");
      }
      json = new StringBuilder(json.substring(0, json.length() - 1));
      json.append("	]}");
      json.append("},");
    }
    json = new StringBuilder(json.substring(0, json.length() - 1));
    json.append("]}");
    json.append("}");
    return json.toString();
  }

  /**
   * Builds a queryJson based on kibana elasrticsearch fields in orther to allow filtering by
   * timestamp range
   *
   * <p>
   * https://www.elastic.co/blog/lost-in-translation-boolean-operations-and-filters-in-the-bool-query
   *
   * @param parameters
   */
  private String buildTimestampRangeFilterJSON(KibanaSearchParameters parameters) {
    StringBuilder json = new StringBuilder();
    json.append("{");
    json.append("\"bool\":{");
    json.append("	\"must\":[{");
    json.append(String.format("\"range\": {\"%s\":{\"gte\":%s,\"lte\":%s,\"format\":\"%s\"}}",
        RANGE_UNIT, parameters.getRangeFrom(), parameters.getRangeTo(), RANGE_FORMAT));
    json.append("}]}");
    json.append("}");
    return json.toString();
  }

  /**
   * Builds a queryJson based on kibana elasrticsearch fields in orther to allow querying multiple
   * fields and multiple values
   *
   * <p>
   * https://www.elastic.co/blog/lost-in-translation-boolean-operations-and-filters-in-the-bool-query
   *
   * @param parameters
   */
  private String buildJSONMatchFields(KibanaSearchParameters parameters) {

    if (parameters.getMatchFields().isEmpty()) {
      return null;
    }

    StringBuilder json = new StringBuilder();
    json.append("{");
    json.append("\"bool\":{");
    json.append("	\"must\":[");
    for (KibanaField field : parameters.getMatchFields()) {
      json.append("{");
      json.append("	\"bool\":{");
      json.append("		\"should\":[");
      for (String value : field.getValues()) {
        json.append("{");
        json.append("			\"match\":{\"" + field.getName() + "\" : \"" + value + "\"}");
        // json.append(" , \"operator\" : \"and\"");
        json.append("},");
      }
      json = new StringBuilder(json.substring(0, json.length() - 1));
      json.append("	]}");
      json.append("},");
    }
    json = new StringBuilder(json.substring(0, json.length() - 1));
    json.append("]}");
    json.append("}");
    return json.toString();
  }

  /**
   * Retrieve all hits matching by fields based on criteria KibanaSearchParameters and implemented
   * index
   *
   * <p>
   * https://www.elastic.co/guide/en/elasticsearch/guide/1.x/_combining_queries_with_filters.html
   *
   * @param parameters
   * @return
   * @throws UnknownHostException
   */
  protected List<SearchHit> searchMatchingByFieldQuery(KibanaSearchParameters parameters)
      throws UnknownHostException {

    SearchRequestBuilder searchRequestBuilder = buildDFSQuerySearchRequest();

    searchRequestBuilder.setSize(parameters.getSize());
    applyMatchQueryAndTimestampRange(searchRequestBuilder, parameters);
    return Arrays.asList(searchRequestBuilder.execute().actionGet().getHits().getHits());
  }

  /**
   * Search and returns the last hit field value based on filter fields.
   *
   * <p>
   * Frequently used to retrieve the last field value saved by Elasticsearch
   *
   * @param parameters
   * @return
   * @throws UnknownHostException
   */
  protected Object getLastFieldValue(KibanaSearchParameters parameters)
      throws UnknownHostException {

    SearchRequestBuilder searchRequestBuilder = buildDFSQuerySearchRequest();
    applyExistingFields(searchRequestBuilder, parameters);
    searchRequestBuilder.setSize(1);
    searchRequestBuilder.addSort(RANGE_UNIT, SortOrder.DESC);
    searchRequestBuilder.setFetchSource(new String[] {parameters.getAggregationFieldName()}, null);

    if (!ArrayUtils.isEmpty(searchRequestBuilder.execute().actionGet().getHits().getHits())) {
      String responseJson =
          searchRequestBuilder.execute().actionGet().getHits().getHits()[0].getSourceAsString();
      return findFirstObjectValueFromGenericJson(responseJson);
    }
    return null;
  }

  /**
   * Search and returns the last hits(SIZE NUMBER) fields values based on filter fields.
   *
   * @param parameters
   * @return
   * @throws UnknownHostException
   */
  private SearchHit[] getLastHitsFieldsValues(KibanaSearchParameters parameters)
      throws UnknownHostException {

    SearchRequestBuilder searchRequestBuilder = buildDFSQuerySearchRequest();

    applyExistingFields(searchRequestBuilder, parameters);
    searchRequestBuilder.setSize(parameters.getSize());
    searchRequestBuilder.addSort(RANGE_UNIT, SortOrder.DESC);
    searchRequestBuilder.setFetchSource(
        new String[] {parameters.getAggregationFieldName(), SharedBeatFields.BEAT_NAME}, null);

    return searchRequestBuilder.execute().actionGet().getHits().getHits();
  }

  /**
   * Search the last 2 field values and returns the difference between then.
   *
   * <p>
   * Frequently used to calculate the last snapshot value for fields that are summed along the hits
   * as total like:
   *
   * <p>
   * All MetricbeatFields.SYSTEM_NETWORK fields
   *
   * @param parameters
   * @return
   * @throws UnknownHostException
   */
  protected Long getLast2ValuesDifference(KibanaSearchParameters parameters)
      throws UnknownHostException {

    parameters.setSize(2);

    SearchHit[] searchHits = getLastHitsFieldsValues(parameters);

    if (ArrayUtils.isEmpty(searchHits)) {
      return null;
    }

    Long lastValue = (Long) findFirstObjectValueFromGenericJson(searchHits[0].getSourceAsString());

    if (searchHits.length == 1) {
      return lastValue;
    }

    Long firstValue = (Long) findFirstObjectValueFromGenericJson(searchHits[1].getSourceAsString());

    return lastValue - firstValue;
  }

  /**
   * Retrieve the first object value present in a generic JSON
   *
   * @param json
   * @return
   */
  protected Object findFirstObjectValueFromGenericJson(String json) {

    try {
      HashMap<String, Object> map =
          new ObjectMapper().readValue(json, new TypeReference<HashMap<String, Object>>() {});

      while (map != null) {
        for (String key : map.keySet()) {
          Object nextObj = map.get(key);
          if (nextObj.getClass().equals(LinkedHashMap.class)) {
            map = (HashMap<String, Object>) nextObj;
            break;
          } else {
            if (nextObj.getClass().equals(Integer.class)) {
              return new Long((Integer) nextObj);
            } else if (nextObj.getClass().equals(Float.class)) {
              return new Double((Float) nextObj);
            } else {
              return nextObj;
            }
          }
        }
      }

    } catch (IOException e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Execute the Elasticsearch API and retrieve the top N average aggregations based on criteria
   * KibanaSearchParameters and implemented index
   *
   * <p>
   * https://www.elastic.co/guide/en/elasticsearch/client/java-api/master/_metrics_aggregations.html
   *
   * @param parameters
   * @return
   * @throws UnknownHostException
   */
  protected List<ValueDataPoint> searchTopAverage(KibanaSearchParameters parameters)
      throws UnknownHostException {

    SearchRequestBuilder searchRequestBuilder = buildDFSQuerySearchRequest();

    searchRequestBuilder.setSize(0);
    applyMatchQueryAndTimestampRange(searchRequestBuilder, parameters);

    searchRequestBuilder
        .addAggregation(buildTopAverageAggregation(parameters).size(parameters.getSize()));

    SearchResponse searchResponse = searchRequestBuilder.execute().actionGet();

    List<ValueDataPoint> dataPointList = new ArrayList<>();
    StringTerms result = searchResponse.getAggregations().get(ROOT_AGGREGATION);

    for (Terms.Bucket bucket : result.getBuckets()) {

      String description = (String) bucket.getKey();
      InternalAvg avg = (InternalAvg) bucket.getAggregations().asList().get(0);

      dataPointList.add(new ValueDataPoint(description, avg.getValue()));
    }

    return dataPointList;
  }

  /**
   * Average aggregation - builds a AvgAggregationBuilder based on AggregationFieldName and
   * AggregationInterval
   *
   * @param parameters
   * @return
   */
  private AvgAggregationBuilder buildTotalAvgAggregation(KibanaSearchParameters parameters) {
    return AggregationBuilders.avg(AVG_AGGREGATION).field(parameters.getAggregationFieldName());
  }

  /**
   * Average DateHistogram aggregation - builds a DateHistogramAggregationBuilder based on
   * AggregationFieldName and AggregationInterval
   *
   * @param parameters
   * @return String JSON
   */
  private DateHistogramAggregationBuilder buildAvgDateHistogramAggregation(
      KibanaSearchParameters parameters) {
    return AggregationBuilders.dateHistogram(ROOT_AGGREGATION).field(RANGE_UNIT)
        .dateHistogramInterval(parameters.getAggregationInterval()).minDocCount(0)
        .subAggregation(
            AggregationBuilders.avg(AVG_AGGREGATION).field(parameters.getAggregationFieldName()))
        .extendedBounds(new ExtendedBounds(parameters.getRangeFrom(), parameters.getRangeTo()));
  }

  /**
   * Top N Aggregation- builds a TermsAggregationBuilder based on AggregationDescriptionFieldName to
   * retrieve and average AggregationFieldName
   *
   * <p>
   * https://www.elastic.co/guide/en/watcher/current/watching-marvel-data.html#watching-cpu-usage
   *
   * @param parameters
   * @return
   */
  private TermsAggregationBuilder buildTopAverageAggregation(KibanaSearchParameters parameters) {
    return AggregationBuilders.terms(ROOT_AGGREGATION)
        .field(parameters.getAggregationDescriptionFieldName())
        .order(Terms.Order.aggregation(AVG_AGGREGATION, false)).subAggregation(
            AggregationBuilders.avg(AVG_AGGREGATION).field(parameters.getAggregationFieldName()));
  }

  /**
   * Build the KibanaSearchParameters 60 seconds time window definitions
   *
   * @param timewindowInSeconds
   * @return KibanaSearchParameters
   */
  protected KibanaSearchParameters buildShipDataParameters(Integer timewindowInSeconds,
      Integer size) {

    KibanaSearchParameters kibanaSearchParameters = new KibanaSearchParameters();

    kibanaSearchParameters.setRangeTo(new Date().getTime());
    kibanaSearchParameters.setRangeFrom(
        kibanaSearchParameters.getRangeTo() - TimeUnit.SECONDS.toMillis(timewindowInSeconds));
    kibanaSearchParameters.setSize(size);

    return kibanaSearchParameters;
  }

  /**
   * Applies 15 min time window range to reduce data sample and optimize the top hits search or any
   * other type of search that would be useful
   *
   * @param parameters
   */
  protected void apply10MinutesTimestampRange(KibanaSearchParameters parameters) {
    parameters.setRangeTo(new Date().getTime() - TimeUnit.MINUTES.toMillis(15));
    parameters.setRangeFrom(parameters.getRangeTo() - TimeUnit.MINUTES.toMillis(25));
  }


  /**
   * Remove all the entries related to the instance from the elastic search data.
   *
   * It's executed in asynchronous mode and an implemented listener to handle the response.
   *
   * After deleting the ElasticSearch entries the listener will remove the instance from its
   * repository.
   *
   * @param instance - the instance object.
   * @return - the quantity of entries deleted
   */
  public void deleteRecordsByInstance(Instance instance) {

    DeleteByQueryAction.INSTANCE.newRequestBuilder(this.transportClient).source(DELETE_ALL_INDEXES)
        .filter(QueryBuilders.matchQuery(SharedBeatFields.BEAT_NAME, instance.getName()))
        .execute(new InstanceElasticSearchActionListener(instance, instanceService));

  }

  /**
   * Retrieves the last hit value of BEAT.NAME .
   *
   * @return the beat name
   * @throws UnknownHostException if the host does not exist
   */
  public String getLastBeatName() throws UnknownHostException {
    KibanaSearchParameters kibanaSearchParameters = new KibanaSearchParameters();
    kibanaSearchParameters.setAggregationFieldName(SharedBeatFields.BEAT_NAME);
    return (String) getLastFieldValue(kibanaSearchParameters);
  }



}
