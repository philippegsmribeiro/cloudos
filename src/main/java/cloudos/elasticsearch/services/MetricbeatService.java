package cloudos.elasticsearch.services;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.avg.Avg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import cloudos.Providers;
import cloudos.elasticsearch.fields.MetricbeatFields;
import cloudos.elasticsearch.fields.SharedBeatFields;
import cloudos.elasticsearch.model.DateHistogramDataPoint;
import cloudos.elasticsearch.model.DiskIODataPoint;
import cloudos.elasticsearch.model.KibanaField;
import cloudos.elasticsearch.model.KibanaSearchParameters;
import cloudos.elasticsearch.model.NetworkIODataPoint;
import cloudos.elasticsearch.model.ProcessDataPoint;
import cloudos.elasticsearch.model.ValueDataPoint;
import cloudos.elasticsearch.model.charts.CpuUsagePctChart;
import cloudos.elasticsearch.model.charts.MemoryUsageBytesChart;
import cloudos.elasticsearch.model.charts.NetworkTrafficBytesChart;
import cloudos.elasticsearch.model.charts.NetworkTrafficPacketsChart;
import cloudos.elasticsearch.model.charts.SystemLoadChart;
import cloudos.instances.InstanceService;
import cloudos.models.Instance;
import cloudos.queue.QueueServiceException;
import cloudos.queue.message.beats.MetricbeatMessage;
import lombok.extern.log4j.Log4j2;

/**
 * Created by rogerio.souza on 05/14/17.
 *
 * <p>
 * Represent a Beat service that implements all Kibana search operations and fetch all Metricbeat
 * index data
 */
@Service
@Lazy
@Log4j2
public class MetricbeatService extends AbstractElasticsearchService {

  private static final String SYSTEM_PROCESS_CPU_TOTAL_PCT = "SYSTEM_PROCESS_CPU_TOTAL_PCT";
  private static final String SYSTEM_PROCESS_MEMORY_RSS_PCT = "SYSTEM_PROCESS_MEMORY_RSS_PCT";
  private static final String SYSTEM_PROCESS_USERNAME = "SYSTEM_PROCESS_USERNAME";

  private static final String SYSTEM_DISKIO_READ_BYTES = "SYSTEM_DISKIO_READ_BYTES";
  private static final String SYSTEM_DISKIO_READ_TIME = "SYSTEM_DISKIO_READ_TIME";
  private static final String SYSTEM_DISKIO_WRITE_BYTES = "SYSTEM_DISKIO_WRITE_BYTES";
  private static final String SYSTEM_DISKIO_WRITE_TIME = "SYSTEM_DISKIO_WRITE_TIME";

  private static final String SYSTEM_NETWORK_IN_BYTES = "SYSTEM_NETWORK_IN_BYTES";
  private static final String SYSTEM_NETWORK_IN_PACKETS = "SYSTEM_NETWORK_IN_PACKETS";
  private static final String SYSTEM_NETWORK_OUT_BYTES = "SYSTEM_NETWORK_OUT_BYTES";
  private static final String SYSTEM_NETWORK_OUT_PACKETS = "SYSTEM_NETWORK_OUT_PACKETS";

  @Autowired
  private InstanceService instanceService;

  @Value("${cloudos.data.service.metricbeat.index}")
  private String index;

  @Override
  protected String getIndex() {
    return index;
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by METRICSET_RTT
   * metricbeats.
   *
   * @param kibanaSearchParameters the index search parameter
   * @return the average of the metric set
   * @throws UnknownHostException if the host does not exist
   */
  public Double getMetricsetRttAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.METRICSET_RTT);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * METRICSET_RTT metricbeats.
   *
   * @param kibanaSearchParameters the index search parameter
   * @return a list of the histogram data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getMetricsetRttDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.METRICSET_RTT);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by SYSTEM_CPU_IDLE_PCT
   * metricbeats.
   *
   * @param kibanaSearchParameters the index search parameter
   * @return the average of CPU usage idle
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemCpuIdlePctAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_IDLE_PCT);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_CPU_IDLE_PCT metricbeats.
   *
   * @param kibanaSearchParameters the index search parameter
   * @return a list of the histogram data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemCpuIdlePctDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_IDLE_PCT);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_CPU_IOWAIT_PCT metricbeats.
   *
   * @param kibanaSearchParameters the index search parameter
   * @return the average of CPU IO wait
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemCpuIowaitPctAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_IOWAIT_PCT);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_CPU_IOWAIT_PCT metricbeats.
   *
   * @param kibanaSearchParameters the index search parameter
   * @return a list of the histogram data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemCpuIowaitPctDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_IOWAIT_PCT);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by SYSTEM_CPU_IRQ_PCT
   * metricbeats.
   *
   * @param kibanaSearchParameters the index search parameter
   * @return the average of the CPU irq
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemCpuIrqPctAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_IRQ_PCT);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_CPU_IRQ_PCT metricbeats.
   *
   * @param kibanaSearchParameters the index search parameter
   * @return a list of the histogram data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemCpuIrqPctDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_IRQ_PCT);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by SYSTEM_CPU_NICE_PCT
   * metricbeats.
   *
   * @param kibanaSearchParameters the index search parameter
   * @return the average for CPU nice
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemCpuNicePctAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_NICE_PCT);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_CPU_NICE_PCT metricbeats.
   *
   * @param kibanaSearchParameters the index search parameter
   * @return a list of the histogram data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemCpuNicePctDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_NICE_PCT);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by metricbeats.
   *
   * @param kibanaSearchParameters the index search parameter
   * @return the average of the CPU soft irq
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemCpuSoftirqPctAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_SOFTIRQ_PCT);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_CPU_SOFTIRQ_PCT metricbeats.
   *
   * @param kibanaSearchParameters the index search parameter
   * @return a list of the histogram data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemCpuSoftirqPctDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_SOFTIRQ_PCT);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by SYSTEM_CPU_STEAL_PCT
   * metricbeats.
   *
   * @param kibanaSearchParameters the index search parameter
   * @return the average of the system CPU steal
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemCpuStealPctAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_STEAL_PCT);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_CPU_STEAL_PCT metricbeats.
   *
   * @param kibanaSearchParameters the index search parameter
   * @return a list of the histogram data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemCpuStealPctDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_STEAL_PCT);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_CPU_SYSTEM_PCT metricbeats.
   *
   * @param kibanaSearchParameters the index search parameter
   * @return the average of the system CPU usage
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemCpuSystemPctAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_SYSTEM_PCT);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_CPU_SYSTEM_PCT metricbeats.
   *
   * @param kibanaSearchParameters the index search parameter
   * @return a list of the histogram data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemCpuSystemPctDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_SYSTEM_PCT);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by SYSTEM_CPU_USER_PCT
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return the average usage of CPU by user
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemCpuUserPctAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_USER_PCT);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_CPU_USER_PCT metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matchin data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemCpuUserPctDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_USER_PCT);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_FILESYSTEM_AVAILABLE metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return the number of files available
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemFilesystemAvailableAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_FILESYSTEM_AVAILABLE);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_FILESYSTEM_AVAILABLE metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemFilesystemAvailableDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_FILESYSTEM_AVAILABLE);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_FILESYSTEM_FILES metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return the average number of files in the system
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemFilesystemFilesAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_FILESYSTEM_FILES);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_FILESYSTEM_FILES metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemFilesystemFilesDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_FILESYSTEM_FILES);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_FILESYSTEM_FREE metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return the number of free files in the system
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemFilesystemFreeAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_FILESYSTEM_FREE);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_FILESYSTEM_FREE metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemFilesystemFreeDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_FILESYSTEM_FREE);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_FILESYSTEM_FREE_FILES metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return the average number of files in the system
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemFilesystemFreeFilesAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_FILESYSTEM_FREE_FILES);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_FILESYSTEM_FREE_FILES metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemFilesystemFreeFilesDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_FILESYSTEM_FREE_FILES);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_FILESYSTEM_TOTAL metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return the average of the number of files in the system
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemFilesystemTotalAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_FILESYSTEM_TOTAL);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_FILESYSTEM_TOTAL metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemFilesystemTotalDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_FILESYSTEM_TOTAL);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_FILESYSTEM_USED_BYTES metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemFilesystemUsedBytesAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_FILESYSTEM_USED_BYTES);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_FILESYSTEM_USED_BYTES metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemFilesystemUsedBytesDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_FILESYSTEM_USED_BYTES);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_FILESYSTEM_USED_PCT metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemFilesystemUsedPctAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_FILESYSTEM_USED_PCT);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_FILESYSTEM_USED_PCT metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemFilesystemUsedPctDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_FILESYSTEM_USED_PCT);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by SYSTEM_LOAD_1
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemLoad1Avg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_LOAD_1);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_LOAD_1 metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemLoad1DateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_LOAD_1);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by SYSTEM_LOAD_15
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemLoad15Avg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_LOAD_15);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_LOAD_15 metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemLoad15DateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_LOAD_15);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by SYSTEM_LOAD_5
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemLoad5Avg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_LOAD_5);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_LOAD_5 metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemLoad5DateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_LOAD_5);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by SYSTEM_LOAD_NORM_1
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemLoadNorm1Avg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_LOAD_NORM_1);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_LOAD_NORM_1 metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemLoadNorm1DateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_LOAD_NORM_1);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by SYSTEM_LOAD_NORM_15
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemLoadNorm15Avg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_LOAD_NORM_15);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_LOAD_NORM_15 metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemLoadNorm15DateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_LOAD_NORM_15);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by SYSTEM_LOAD_NORM_5
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemLoadNorm5Avg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_LOAD_NORM_5);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_LOAD_NORM_5 metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemLoadNorm5DateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_LOAD_NORM_5);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_MEMORY_ACTUAL_FREE metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemMemoryActualFreeAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_ACTUAL_FREE);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_MEMORY_ACTUAL_FREE metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemMemoryActualFreeDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_ACTUAL_FREE);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_MEMORY_ACTUAL_USED_BYTES metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemMemoryActualUsedBytesAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters
        .setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_ACTUAL_USED_BYTES);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_MEMORY_ACTUAL_USED_BYTES metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemMemoryActualUsedBytesDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {

    kibanaSearchParameters
        .setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_ACTUAL_USED_BYTES);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_MEMORY_ACTUAL_USED_PCT metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemMemoryActualUsedPctAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_ACTUAL_USED_PCT);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_MEMORY_ACTUAL_USED_PCT metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemMemoryActualUsedPctDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_ACTUAL_USED_PCT);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by SYSTEM_MEMORY_FREE
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemMemoryFreeAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_FREE);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_MEMORY_FREE metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemMemoryFreeDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_FREE);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_MEMORY_SWAP_FREE metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemMemorySwapFreeAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_SWAP_FREE);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_MEMORY_SWAP_FREE metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemMemorySwapFreeDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_SWAP_FREE);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_MEMORY_SWAP_TOTAL metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemMemorySwapTotalAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_SWAP_TOTAL);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_MEMORY_SWAP_TOTAL metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemMemorySwapTotalDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_SWAP_TOTAL);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_MEMORY_SWAP_USED_BYTES metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemMemorySwapUsedBytesAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_SWAP_USED_BYTES);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_MEMORY_SWAP_USED_BYTES metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemMemorySwapUsedBytesDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_SWAP_USED_BYTES);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_MEMORY_SWAP_USED_PCT metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemMemorySwapUsedPctAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_SWAP_USED_PCT);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_MEMORY_SWAP_USED_PCT metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemMemorySwapUsedPctDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_SWAP_USED_PCT);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by SYSTEM_MEMORY_TOTAL
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemMemoryTotalAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_TOTAL);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_MEMORY_USED_BYTES metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemMemoryUsedBytesAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_USED_BYTES);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_MEMORY_USED_BYTES metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemMemoryUsedBytesDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_USED_BYTES);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_MEMORY_USED_PCT metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemMemoryUsedPctAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_USED_PCT);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_MEMORY_USED_PCT metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemMemoryUsedPctDateHistogramAvg(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_USED_PCT);
    return searchAvgDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_NETWORK_IN_BYTES metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemNetworkInBytesAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_NETWORK_IN_BYTES);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_NETWORK_IN_BYTES metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemNetworkInBytesDateHistogramCountByMinute(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_NETWORK_IN_BYTES);
    return searchMaxDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_NETWORK_IN_DROPPED metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemNetworkInDroppedAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_NETWORK_IN_DROPPED);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_NETWORK_IN_DROPPED metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemNetworkInDroppedDateHistogramCountByMinute(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_NETWORK_IN_DROPPED);
    return searchMaxDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_NETWORK_IN_ERRORS metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemNetworkInErrorsAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_NETWORK_IN_ERRORS);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_NETWORK_IN_ERRORS metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemNetworkInErrorsDateHistogramCountByMinute(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_NETWORK_IN_ERRORS);
    return searchMaxDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_NETWORK_IN_PACKETS metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemNetworkInPacketsAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_NETWORK_IN_PACKETS);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_NETWORK_IN_PACKETS metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemNetworkInPacketsDateHistogramCountByMinute(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_NETWORK_IN_PACKETS);
    return searchMaxDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_NETWORK_OUT_BYTES metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemNetworkOutBytesAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_NETWORK_OUT_BYTES);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_NETWORK_OUT_BYTES metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemNetworkOutBytesDateHistogramCountByMinute(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_NETWORK_OUT_BYTES);
    return searchMaxDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_NETWORK_OUT_DROPPED metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemNetworkOutDroppedAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_NETWORK_OUT_DROPPED);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_NETWORK_OUT_DROPPED metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemNetworkOutDroppedDateHistogramCountByMinute(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_NETWORK_OUT_DROPPED);
    return searchMaxDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_NETWORK_OUT_ERRORS metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemNetworkOutErrorsAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_NETWORK_OUT_ERRORS);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_NETWORK_OUT_ERRORS metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemNetworkOutErrorsDateHistogramCountByMinute(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_NETWORK_OUT_ERRORS);
    return searchMaxDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by BEAT.NAME and grouped by
   * SYSTEM_NETWORK_OUT_PACKETS metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemNetworkOutPacketsAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_NETWORK_OUT_PACKETS);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour average date histogram values filtered by BEAT.NAME and grouped by
   * SYSTEM_NETWORK_OUT_PACKETS metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of matching data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<DateHistogramDataPoint> getSystemNetworkOutPacketsDateHistogramCountByMinute(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_NETWORK_OUT_PACKETS);
    return searchMaxDateHistogram(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and METRICSET_RTT metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Long getMetricsetRtt(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.METRICSET_RTT);
    return (Long) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_CPU_IDLE_PCT metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemCpuTotalUsagePct(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_IDLE_PCT);
    Double value = (Double) getLastFieldValue(kibanaSearchParameters);
    if (value != null) {
      return 1 - value;
    }
    return value;
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_CPU_IDLE_PCT metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemCpuIdlePct(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_IDLE_PCT);
    return (Double) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_CPU_IOWAIT_PCT metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemCpuIowaitPct(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_IOWAIT_PCT);
    return (Double) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_CPU_IRQ_PCT metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemCpuIrqPct(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_IRQ_PCT);
    return (Double) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_CPU_NICE_PCT metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemCpuNicePct(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {

    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_NICE_PCT);
    return (Double) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and ' metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemCpuSoftirqPct(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_SOFTIRQ_PCT);
    return (Double) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_CPU_STEAL_PCT metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemCpuStealPct(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_STEAL_PCT);
    return (Double) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_CPU_SYSTEM_PCT metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemCpuSystemPct(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_SYSTEM_PCT);
    return (Double) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_CPU_USER_PCT metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemCpuUserPct(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_USER_PCT);
    return (Double) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_FILESYSTEM_AVAILABLE
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Long getSystemFilesystemAvailable(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_FILESYSTEM_AVAILABLE);
    return (Long) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_FILESYSTEM_FILES
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Long getSystemFilesystemFiles(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_FILESYSTEM_FILES);
    return (Long) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_FILESYSTEM_FREE
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Long getSystemFilesystemFree(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_FILESYSTEM_FREE);
    return (Long) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_FILESYSTEM_FREE_FILES
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Long getSystemFilesystemFreeFiles(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_FILESYSTEM_FREE_FILES);
    return (Long) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_FILESYSTEM_TOTAL
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Long getSystemFilesystemTotal(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_FILESYSTEM_TOTAL);
    return (Long) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_FILESYSTEM_USED_BYTES
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Long getSystemFilesystemUsedBytes(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_FILESYSTEM_USED_BYTES);
    return (Long) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_FILESYSTEM_USED_PCT
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemFilesystemUsedPct(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_FILESYSTEM_USED_PCT);
    return (Double) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_LOAD_1 metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemLoad1(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_LOAD_1);
    return (Double) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_LOAD_15 metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemLoad15(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_LOAD_15);
    return (Double) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_LOAD_5 metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemLoad5(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_LOAD_5);
    return (Double) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_LOAD_NORM_1 metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemLoadNorm1(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_LOAD_NORM_1);
    return (Double) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_LOAD_NORM_15 metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemLoadNorm15(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_LOAD_NORM_15);
    return (Double) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_LOAD_NORM_5 metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemLoadNorm5(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_LOAD_NORM_5);
    return (Double) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_MEMORY_ACTUAL_FREE
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Long getSystemMemoryActualFree(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_ACTUAL_FREE);
    return (Long) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_MEMORY_ACTUAL_USED_BYTES
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Long getSystemMemoryActualUsedBytes(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters
        .setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_ACTUAL_USED_BYTES);
    return (Long) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_MEMORY_ACTUAL_USED_PCT
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemMemoryActualUsedPct(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_ACTUAL_USED_PCT);
    return (Double) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_MEMORY_FREE metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Long getSystemMemoryFree(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_FREE);
    return (Long) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_MEMORY_SWAP_FREE
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Long getSystemMemorySwapFree(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_SWAP_FREE);
    return (Long) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_MEMORY_SWAP_TOTAL
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Long getSystemMemorySwapTotal(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_SWAP_TOTAL);
    return (Long) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_MEMORY_SWAP_USED_BYTES
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Long getSystemMemorySwapUsedBytes(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_SWAP_USED_BYTES);
    return (Long) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_MEMORY_SWAP_USED_PCT
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemMemorySwapUsedPct(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_SWAP_USED_PCT);
    return (Double) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_MEMORY_TOTAL metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Long getSystemMemoryTotal(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_TOTAL);
    return (Long) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_MEMORY_USED_BYTES
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Long getSystemMemoryUsedBytes(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_USED_BYTES);
    return (Long) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_MEMORY_USED_PCT
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Double getSystemMemoryUsedPct(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_MEMORY_USED_PCT);
    return (Double) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_NETWORK_IN_BYTES
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Long getSystemNetworkInBytes(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_NETWORK_IN_BYTES);
    return getLoopBackAndEthernetTrafficValues(kibanaSearchParameters);
  }

  /**
   * Get the number of loop back and ethernet traffic.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return number of matchin values
   * @throws UnknownHostException if the host doesn ot exist
   */
  private Long getLoopBackAndEthernetTrafficValues(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    KibanaField ethernetTrafficField = new KibanaField(MetricbeatFields.SYSTEM_NETWORK_NAME,
        Arrays.asList(MetricbeatFields.SYSTEM_NETWORK_NAME_ETHERNET));
    kibanaSearchParameters.getFilterFields().add(ethernetTrafficField);
    Long ethernetValue = getLast2ValuesDifference(kibanaSearchParameters);
    kibanaSearchParameters.getFilterFields()
        .remove(kibanaSearchParameters.getFilterFields().size() - 1);

    if (ethernetValue == null) {
      ethernetValue = 0L;
      log.error("Traffic value for SYSTEM_NETWORK_NAME_ETHERNET not found on instance {}",
          kibanaSearchParameters.getFilterFields().get(0).getValues().get(0));
    }

    KibanaField loopBackTraffic = new KibanaField(MetricbeatFields.SYSTEM_NETWORK_NAME,
        Arrays.asList(MetricbeatFields.SYSTEM_NETWORK_NAME_LOOPBACK));
    kibanaSearchParameters.getFilterFields().add(loopBackTraffic);
    Long loopBackValue = getLast2ValuesDifference(kibanaSearchParameters);
    kibanaSearchParameters.getFilterFields()
        .remove(kibanaSearchParameters.getFilterFields().size() - 1);

    if (loopBackValue == null) {
      loopBackValue = 0L;
      log.error(String.format(
          "Traffic value for SYSTEM_NETWORK_NAME_LOOPBACK not found on" + " instance %s",
          kibanaSearchParameters.getFilterFields().get(0).getValues().get(0)));
    }

    return ethernetValue + loopBackValue;
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_NETWORK_IN_DROPPED
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Long getSystemNetworkInDropped(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_NETWORK_IN_DROPPED);
    return getLoopBackAndEthernetTrafficValues(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_NETWORK_IN_ERRORS
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Long getSystemNetworkInErrors(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_NETWORK_IN_ERRORS);
    return getLoopBackAndEthernetTrafficValues(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_NETWORK_IN_PACKETS
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public Long getSystemNetworkInPackets(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_NETWORK_IN_PACKETS);
    return getLoopBackAndEthernetTrafficValues(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_NETWORK_OUT_BYTES
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return the number of packages sent out, in bytes
   * @throws UnknownHostException if the host does not exist
   */
  public Long getSystemNetworkOutBytes(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_NETWORK_OUT_BYTES);
    return getLoopBackAndEthernetTrafficValues(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_NETWORK_OUT_DROPPED
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return the number of packets dropped in the network
   * @throws UnknownHostException if the host does not exist
   */
  public Long getSystemNetworkOutDropped(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_NETWORK_OUT_DROPPED);
    return getLoopBackAndEthernetTrafficValues(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_NETWORK_OUT_ERRORS
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return the number of packets that fail to be sent out
   * @throws UnknownHostException if the host does not exist
   */
  public Long getSystemNetworkOutErrors(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_NETWORK_OUT_ERRORS);
    return getLoopBackAndEthernetTrafficValues(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_NETWORK_OUT_PACKETS
   * metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return the number of network packets out of the system
   * @throws UnknownHostException if the host does not exist
   */
  public Long getSystemNetworkOutPackets(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_NETWORK_OUT_PACKETS);
    return getLoopBackAndEthernetTrafficValues(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and SYSTEM_CPU_CORES metricbeats.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return the number of CPU cores in the system
   * @throws UnknownHostException if the host does not exist
   */
  public Long getSystemCpuCores(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_CPU_CORES);
    return (Long) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Retrieves top SIZE most recent snapshot MetricbeatFields.SYSTEM_PROCESS_CPU_TOTAL_PCT.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @param size the number of data points
   * @return a list of value data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<ValueDataPoint> getTopSystemProcessCpuTotalPct(
      KibanaSearchParameters kibanaSearchParameters, Integer size) throws UnknownHostException {

    kibanaSearchParameters.setAggregationDescriptionFieldName(MetricbeatFields.SYSTEM_PROCESS_NAME);
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_PROCESS_CPU_TOTAL_PCT);
    kibanaSearchParameters.setSize(size);

    return searchTopAverage(kibanaSearchParameters);
  }

  /**
   * Retrieves the most recent snapshot top SIZE MetricbeatFields.SYSTEM_PROCESS_MEMORY_RSS_PCT.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @param size the number of data points
   * @return a list of value data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<ValueDataPoint> getTopSystemProcessMemoryRssPct(
      KibanaSearchParameters kibanaSearchParameters, Integer size) throws UnknownHostException {

    kibanaSearchParameters.setAggregationDescriptionFieldName(MetricbeatFields.SYSTEM_PROCESS_NAME);
    kibanaSearchParameters.setAggregationFieldName(MetricbeatFields.SYSTEM_PROCESS_MEMORY_RSS_PCT);
    kibanaSearchParameters.setSize(size);

    return searchTopAverage(kibanaSearchParameters);
  }

  /**
   * Retrieves most recent snapshot top 10 processes data (name, user, cpu %, memory % i/o %).
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @param size the number of data points
   * @return a list of process data points
   * @throws UnknownHostException if the host does not exist
   */
  public List<ProcessDataPoint> getTopSystemProcesses(KibanaSearchParameters kibanaSearchParameters,
      Integer size) throws UnknownHostException {

    kibanaSearchParameters.setSize(size);

    SearchRequestBuilder searchRequestBuilder = buildDFSQuerySearchRequest();
    searchRequestBuilder.setSize(0);
    applyMatchQueryAndTimestampRange(searchRequestBuilder, kibanaSearchParameters);

    searchRequestBuilder.addAggregation(buildProcessesDataAggregation(kibanaSearchParameters)
        .size(kibanaSearchParameters.getSize()));

    SearchResponse searchResponse = searchRequestBuilder.execute().actionGet();

    List<ProcessDataPoint> dataPointList = new ArrayList<>();
    StringTerms result = searchResponse.getAggregations().get(ROOT_AGGREGATION);

    for (Terms.Bucket bucket : result.getBuckets()) {

      String name = (String) bucket.getKey();

      StringTerms usernameResult = bucket.getAggregations().get(SYSTEM_PROCESS_USERNAME);
      String username = (String) usernameResult.getBuckets().get(0).getKey();

      Avg cpuPctAvg = bucket.getAggregations().get(SYSTEM_PROCESS_CPU_TOTAL_PCT);
      Avg memoryPctAvg = bucket.getAggregations().get(SYSTEM_PROCESS_MEMORY_RSS_PCT);

      ProcessDataPoint processDataPoint = new ProcessDataPoint();
      processDataPoint.setName(name);
      processDataPoint.setUser(username);
      processDataPoint.setCpuPct(cpuPctAvg.getValue());
      processDataPoint.setMemoryPct(memoryPctAvg.getValue());

      dataPointList.add(processDataPoint);
    }

    return dataPointList;
  }

  /**
   * Builds aggregations to retrieve processes data: name, user, cpu %, memory % i/o %.
   *
   * <p>
   * MetricbeatFields.SYSTEM_PROCESS_MEMORY_RSS_PCT
   *
   * <p>
   * MetricbeatFields.SYSTEM_PROCESS_NAME
   *
   * <p>
   * MetricbeatFields.SYSTEM_PROCESS_USERNAME
   *
   * <p>
   * MetricbeatFields.SYSTEM_PROCESS_CPU_TOTAL_PCT
   *
   * @param parameters a list of matching search parameters
   * @return a term aggregation builder
   */
  private TermsAggregationBuilder buildProcessesDataAggregation(KibanaSearchParameters parameters) {

    return AggregationBuilders.terms(ROOT_AGGREGATION).field(MetricbeatFields.SYSTEM_PROCESS_NAME)
        .order(Terms.Order.compound(Terms.Order.aggregation(SYSTEM_PROCESS_CPU_TOTAL_PCT, false),
            Terms.Order.aggregation(SYSTEM_PROCESS_MEMORY_RSS_PCT, false)))
        .subAggregation(AggregationBuilders.terms(SYSTEM_PROCESS_USERNAME)
            .field(MetricbeatFields.SYSTEM_PROCESS_USERNAME).size(1))
        .subAggregation(AggregationBuilders.avg(SYSTEM_PROCESS_CPU_TOTAL_PCT)
            .field(MetricbeatFields.SYSTEM_PROCESS_CPU_TOTAL_PCT))
        .subAggregation(AggregationBuilders.avg(SYSTEM_PROCESS_MEMORY_RSS_PCT)
            .field(MetricbeatFields.SYSTEM_PROCESS_MEMORY_RSS_PCT));
  }

  /**
   * Ship all data from elasticsearch beat index to a specific beat index queue - filtering by range
   * considering the time window definition.
   *
   * @param size the number of data points
   * @param timewindowInSeconds the size of the window in seconds
   * @throws UnknownHostException if the host does not exist
   * @throws QueueServiceException if the message is not sent to the queue
   */
  public List<SearchHit> shipData(Integer timewindowInSeconds, Integer size)
      throws UnknownHostException, QueueServiceException {

    KibanaSearchParameters kibanaSearchParameters =
        buildShipDataParameters(timewindowInSeconds, size);

    List<SearchHit> searchHitList = searchMatchingByFieldQuery(kibanaSearchParameters);

    for (SearchHit searchHit : searchHitList) {
      String hitJson = searchHit.getSourceAsString();
      MetricbeatMessage message = new MetricbeatMessage();
      message.setHitJson(hitJson);
      queueService.sendMessage(message);
    }
    return searchHitList;
  }

  /**
   * Retrieve all data from Elasticsearch to build the Memory Usage Bytes chart.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return the memory usage chart in bytes
   * @throws UnknownHostException if the host does not exist
   */
  public MemoryUsageBytesChart getMemoryUsageBytesChartData(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {

    MemoryUsageBytesChart memoryUsageBytesChart = new MemoryUsageBytesChart();

    // SharedBeatFields.SYSTEM_MEMORY_FREE
    memoryUsageBytesChart.setSystemMemoryFreeAvg(getSystemMemoryFreeAvg(kibanaSearchParameters));
    memoryUsageBytesChart.setSystemMemoryFreeDateHistogramAvg(
        getSystemMemoryFreeDateHistogramAvg(kibanaSearchParameters));

    // SharedBeatFields.SYSTEM_MEMORY_SWAP_USED_BYTES
    memoryUsageBytesChart
        .setSystemMemorySwapAvg(getSystemMemorySwapUsedBytesAvg(kibanaSearchParameters));
    memoryUsageBytesChart.setSystemMemorySwapDateHistogramAvg(
        getSystemMemorySwapUsedBytesDateHistogramAvg(kibanaSearchParameters));

    // SharedBeatFields.SYSTEM_MEMORY_USED_BYTES
    memoryUsageBytesChart
        .setSystemMemoryUsedAvg(getSystemMemoryUsedBytesAvg(kibanaSearchParameters));
    memoryUsageBytesChart.setSystemMemoryUsedDateHistogramAvg(
        getSystemMemoryUsedBytesDateHistogramAvg(kibanaSearchParameters));

    // SharedBeatFields.SYSTEM_MEMORY_TOTAL
    memoryUsageBytesChart.setSystemMemoryTotal(getSystemMemoryTotalAvg(kibanaSearchParameters));

    return memoryUsageBytesChart;
  }

  /**
   * Retrieve running instances by provider data from Elasticsearch to build the Memory Usage Bytes
   * chart.
   *
   * @param provider - The given provider.
   * @return the instances average memory usage chart in bytes
   * @throws UnknownHostException if the host does not exist
   */
  public MemoryUsageBytesChart getMemoryUsageBytesChartDataByProvider(Providers provider,
      Long rangeFrom, Long rangeTo) throws UnknownHostException {

    MemoryUsageBytesChart memoryUsageBytesChart = new MemoryUsageBytesChart();

    List<Instance> instances =
        instanceService.findAllActiveAndNotStoppedInstancesByProvider(provider);

    if (CollectionUtils.isEmpty(instances)) {
      return memoryUsageBytesChart;
    }

    List<String> instanceNames = new ArrayList<>();
    instances.stream().forEach(e -> instanceNames.add(e.getName()));

    KibanaField beatname = new KibanaField(SharedBeatFields.BEAT_NAME, instanceNames);

    KibanaSearchParameters kibanaSearchParameters = new KibanaSearchParameters();
    List<KibanaField> filterFields = new ArrayList<>();
    filterFields.add(beatname);
    kibanaSearchParameters.setFilterFields(filterFields);

    if (rangeFrom != null) {
      kibanaSearchParameters.setRangeFrom(rangeFrom);
    }

    if (rangeTo != null) {
      kibanaSearchParameters.setRangeTo(rangeTo);
    }

    return getMemoryUsageBytesChartData(kibanaSearchParameters);

  }

  /**
   * Retrieve all data from Elasticsearch to build the CPU Usage Pct Chart.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return the chart of CPU usage
   * @throws UnknownHostException if the host does not exist
   */
  public CpuUsagePctChart getCpuUsagePctChartData(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {

    CpuUsagePctChart cpuUsagePctChart = new CpuUsagePctChart();

    // SharedBeatFields.SYSTEM_CPU_IOWAIT_PCT
    cpuUsagePctChart.setSystemCpuIowaitPctAvg(getSystemCpuIowaitPctAvg(kibanaSearchParameters));
    cpuUsagePctChart.setSystemCpuIowaitPctDateHistogramAvg(
        getSystemCpuIowaitPctDateHistogramAvg(kibanaSearchParameters));

    // SharedBeatFields.SYSTEM_CPU_IRQ_PCT
    cpuUsagePctChart.setSystemCpuIrqPctAvg(getSystemCpuIrqPctAvg(kibanaSearchParameters));
    cpuUsagePctChart.setSystemCpuIrqPctDateHistogramAvg(
        getSystemCpuIrqPctDateHistogramAvg(kibanaSearchParameters));

    // SharedBeatFields.SYSTEM_CPU_NICE_PCT
    cpuUsagePctChart.setSystemCpuNicePctAvg(getSystemCpuNicePctAvg(kibanaSearchParameters));
    cpuUsagePctChart.setSystemCpuNicePctDateHistogramAvg(
        getSystemCpuNicePctDateHistogramAvg(kibanaSearchParameters));

    // SharedBeatFields.SYSTEM_CPU_SOFTIRQ_PCT
    cpuUsagePctChart.setSystemCpuSoftirqPctAvg(getSystemCpuSoftirqPctAvg(kibanaSearchParameters));
    cpuUsagePctChart.setSystemCpuSoftirqPctDateHistogramAvg(
        getSystemCpuSoftirqPctDateHistogramAvg(kibanaSearchParameters));

    // SharedBeatFields.SYSTEM_CPU_SYSTEM_PCT
    cpuUsagePctChart.setSystemCpuSystemPctAvg(getSystemCpuSystemPctAvg(kibanaSearchParameters));
    cpuUsagePctChart.setSystemCpuSystemPctDateHistogramAvg(
        getSystemCpuSystemPctDateHistogramAvg(kibanaSearchParameters));

    // SharedBeatFields.SYSTEM_CPU_USER_PCT
    cpuUsagePctChart.setSystemCpuUserPctAvg(getSystemCpuUserPctAvg(kibanaSearchParameters));
    cpuUsagePctChart.setSystemCpuUserPctDateHistogramAvg(
        getSystemCpuUserPctDateHistogramAvg(kibanaSearchParameters));

    return cpuUsagePctChart;
  }

  /**
   * Retrieve all data from Elasticsearch to build Network Traffic Packets chart.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return the chart of the network traffic in packets
   * @throws UnknownHostException if the host does not exist
   */
  public NetworkTrafficPacketsChart getNetworkTrafficPacketsChart(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {

    NetworkTrafficPacketsChart networkTrafficPacketsChart = new NetworkTrafficPacketsChart();

    // SharedBeatFields.SYSTEM_NETWORK_IN_PACKETS
    networkTrafficPacketsChart.setSystemNetworkInPacketsDateHistogramAvg(
        mergeSystemNetworkLoopBackAndEthernetInPacketsDateHistogram(kibanaSearchParameters));

    calculateTotalValueAggregationByTimeWindow(
        networkTrafficPacketsChart.getSystemNetworkInPacketsDateHistogramAvg());

    for (DateHistogramDataPoint dataPoint : networkTrafficPacketsChart
        .getSystemNetworkInPacketsDateHistogramAvg()) {
      dataPoint.setValue(dataPoint.getValue() * 100);
    }

    networkTrafficPacketsChart.setSystemNetworkInPacketsAvg(calculateAverageFromDateHistogram(
        networkTrafficPacketsChart.getSystemNetworkInPacketsDateHistogramAvg()));

    // SharedBeatFields.SYSTEM_NETWORK_OUT_PACKETS
    networkTrafficPacketsChart.setSystemNetworkOutPacketsDateHistogramAvg(
        mergeSystemNetworkLoopBackAndEthernetOutPacketsDateHistogram(kibanaSearchParameters));

    calculateTotalValueAggregationByTimeWindow(
        networkTrafficPacketsChart.getSystemNetworkOutPacketsDateHistogramAvg());

    for (DateHistogramDataPoint dataPoint : networkTrafficPacketsChart
        .getSystemNetworkOutPacketsDateHistogramAvg()) {
      dataPoint.setValue(dataPoint.getValue() * 100);
    }

    networkTrafficPacketsChart.setSystemNetworkOutPacketsAvg(calculateAverageFromDateHistogram(
        networkTrafficPacketsChart.getSystemNetworkOutPacketsDateHistogramAvg()) * -1);

    return networkTrafficPacketsChart;
  }

  /**
   * Retrieve all data from Elasticsearch to build Network Traffic Bytes Chart.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return the network traffic chart in bytes
   * @throws UnknownHostException if the host does not exist
   */
  public NetworkTrafficBytesChart getNetworkTrafficBytesChart(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {

    NetworkTrafficBytesChart networkTrafficBytesChart = new NetworkTrafficBytesChart();

    // SharedBeatFields.SYSTEM_NETWORK_IN_BYTES
    networkTrafficBytesChart.setSystemNetworkInBytesDateHistogramAvg(
        mergeSystemNetworkLoopBackAndEthernetInBytesDateHistogram(kibanaSearchParameters));

    calculateTotalValueAggregationByTimeWindow(
        networkTrafficBytesChart.getSystemNetworkInBytesDateHistogramAvg());

    networkTrafficBytesChart.setSystemNetworkInBytesAvg(calculateAverageFromDateHistogram(
        networkTrafficBytesChart.getSystemNetworkInBytesDateHistogramAvg()));

    // SharedBeatFields.SYSTEM_NETWORK_OUT_BYTES
    networkTrafficBytesChart.setSystemNetworkOutBytesDateHistogramAvg(
        mergeSystemNetworkLoopBackAndEthernetOutBytesDateHistogram(kibanaSearchParameters));

    calculateTotalValueAggregationByTimeWindow(
        networkTrafficBytesChart.getSystemNetworkOutBytesDateHistogramAvg());

    networkTrafficBytesChart.setSystemNetworkOutBytesAvg(calculateAverageFromDateHistogram(
        networkTrafficBytesChart.getSystemNetworkOutBytesDateHistogramAvg()) * -1);

    return networkTrafficBytesChart;
  }

  /**
   * Retrieves the ethernet packets and also the loopback packets for
   * SharedBeatFields.SYSTEM_NETWORK_IN_BYTES. Merging values from both list to return the sum of
   * each datatime entry.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of the merged data points
   * @throws UnknownHostException in case the host does not exist
   */
  private List<DateHistogramDataPoint> mergeSystemNetworkLoopBackAndEthernetInBytesDateHistogram(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    // SharedBeatFields.SYSTEM_NETWORK_IN_BYTES
    KibanaField ethernetTrafficField = new KibanaField(MetricbeatFields.SYSTEM_NETWORK_NAME,
        Arrays.asList(MetricbeatFields.SYSTEM_NETWORK_NAME_ETHERNET));
    kibanaSearchParameters.getFilterFields().add(ethernetTrafficField);

    List<DateHistogramDataPoint> ethernetDateHistogram =
        getSystemNetworkInBytesDateHistogramCountByMinute(kibanaSearchParameters);

    kibanaSearchParameters.getFilterFields()
        .remove(kibanaSearchParameters.getFilterFields().size() - 1);

    KibanaField loopBackTrafficField = new KibanaField(MetricbeatFields.SYSTEM_NETWORK_NAME,
        Arrays.asList(MetricbeatFields.SYSTEM_NETWORK_NAME_LOOPBACK));

    kibanaSearchParameters.getFilterFields().add(loopBackTrafficField);

    List<DateHistogramDataPoint> loopBackDateHistogram =
        getSystemNetworkInBytesDateHistogramCountByMinute(kibanaSearchParameters);

    kibanaSearchParameters.getFilterFields()
        .remove(kibanaSearchParameters.getFilterFields().size() - 1);

    return merge2DateHistogramValues(ethernetDateHistogram, loopBackDateHistogram);
  }

  /**
   * Retrieves the ethernet packets and also the loopback packets for
   * SharedBeatFields.SYSTEM_NETWORK_OUT_BYTES. Merging values from both list to return the sum of
   * each datatime entry.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of the merged data points
   * @throws UnknownHostException in case the host does not exist
   */
  private List<DateHistogramDataPoint> mergeSystemNetworkLoopBackAndEthernetOutBytesDateHistogram(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    // SharedBeatFields.SYSTEM_NETWORK_OUT_BYTES
    KibanaField ethernetTrafficField = new KibanaField(MetricbeatFields.SYSTEM_NETWORK_NAME,
        Arrays.asList(MetricbeatFields.SYSTEM_NETWORK_NAME_ETHERNET));
    kibanaSearchParameters.getFilterFields().add(ethernetTrafficField);

    List<DateHistogramDataPoint> ethernetDateHistogram =
        getSystemNetworkOutBytesDateHistogramCountByMinute(kibanaSearchParameters);

    kibanaSearchParameters.getFilterFields()
        .remove(kibanaSearchParameters.getFilterFields().size() - 1);

    KibanaField loopBackTrafficField = new KibanaField(MetricbeatFields.SYSTEM_NETWORK_NAME,
        Arrays.asList(MetricbeatFields.SYSTEM_NETWORK_NAME_LOOPBACK));

    kibanaSearchParameters.getFilterFields().add(loopBackTrafficField);

    List<DateHistogramDataPoint> loopBackDateHistogram =
        getSystemNetworkOutBytesDateHistogramCountByMinute(kibanaSearchParameters);

    kibanaSearchParameters.getFilterFields()
        .remove(kibanaSearchParameters.getFilterFields().size() - 1);

    return merge2DateHistogramValues(ethernetDateHistogram, loopBackDateHistogram);
  }

  /**
   * Retrieves the ethernet packets and also the loopback packets for
   * SharedBeatFields.SYSTEM_NETWORK_IN_PACKETS. Merging values from both list to return the sum of
   * each datatime entry.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of the merged data points
   * @throws UnknownHostException in case the host does not exist
   */
  private List<DateHistogramDataPoint> mergeSystemNetworkLoopBackAndEthernetInPacketsDateHistogram(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    // SharedBeatFields.SYSTEM_NETWORK_IN_PACKETS
    KibanaField ethernetTrafficField = new KibanaField(MetricbeatFields.SYSTEM_NETWORK_NAME,
        Arrays.asList(MetricbeatFields.SYSTEM_NETWORK_NAME_ETHERNET));
    kibanaSearchParameters.getFilterFields().add(ethernetTrafficField);

    List<DateHistogramDataPoint> ethernetDateHistogram =
        getSystemNetworkInPacketsDateHistogramCountByMinute(kibanaSearchParameters);

    kibanaSearchParameters.getFilterFields()
        .remove(kibanaSearchParameters.getFilterFields().size() - 1);

    KibanaField loopBackTrafficField = new KibanaField(MetricbeatFields.SYSTEM_NETWORK_NAME,
        Arrays.asList(MetricbeatFields.SYSTEM_NETWORK_NAME_LOOPBACK));

    kibanaSearchParameters.getFilterFields().add(loopBackTrafficField);

    List<DateHistogramDataPoint> loopBackDateHistogram =
        getSystemNetworkInPacketsDateHistogramCountByMinute(kibanaSearchParameters);

    kibanaSearchParameters.getFilterFields()
        .remove(kibanaSearchParameters.getFilterFields().size() - 1);

    return merge2DateHistogramValues(ethernetDateHistogram, loopBackDateHistogram);
  }

  /**
   * Retrieves the ethernet packets and also the loopback packets for
   * SharedBeatFields.SYSTEM_NETWORK_OUT_PACKETS. Merging values from both list to return the sum of
   * each datatime entry.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return a list of the merged data points
   * @throws UnknownHostException in case the host does not exist
   */
  private List<DateHistogramDataPoint> mergeSystemNetworkLoopBackAndEthernetOutPacketsDateHistogram(
      KibanaSearchParameters kibanaSearchParameters) throws UnknownHostException {
    // SharedBeatFields.SYSTEM_NETWORK_OUT_PACKETS
    KibanaField ethernetTrafficField = new KibanaField(MetricbeatFields.SYSTEM_NETWORK_NAME,
        Arrays.asList(MetricbeatFields.SYSTEM_NETWORK_NAME_ETHERNET));
    kibanaSearchParameters.getFilterFields().add(ethernetTrafficField);

    List<DateHistogramDataPoint> ethernetDateHistogram =
        getSystemNetworkOutPacketsDateHistogramCountByMinute(kibanaSearchParameters);

    kibanaSearchParameters.getFilterFields()
        .remove(kibanaSearchParameters.getFilterFields().size() - 1);

    KibanaField loopBackTrafficField = new KibanaField(MetricbeatFields.SYSTEM_NETWORK_NAME,
        Arrays.asList(MetricbeatFields.SYSTEM_NETWORK_NAME_LOOPBACK));

    kibanaSearchParameters.getFilterFields().add(loopBackTrafficField);

    List<DateHistogramDataPoint> loopBackDateHistogram =
        getSystemNetworkOutPacketsDateHistogramCountByMinute(kibanaSearchParameters);

    kibanaSearchParameters.getFilterFields()
        .remove(kibanaSearchParameters.getFilterFields().size() - 1);

    return merge2DateHistogramValues(ethernetDateHistogram, loopBackDateHistogram);
  }

  /**
   * Merges values from one DateHistogramDataPoint list to another DateHistogramDataPoint list
   * considering the same datetime for entries in each one.
   *
   * @param firstDateHistogram the first set of data points
   * @param secondDateHistogram the second set of data points
   * @return a list of the merged data points
   * @throws UnknownHostException in case the host does not exist
   */
  private List<DateHistogramDataPoint> merge2DateHistogramValues(
      List<DateHistogramDataPoint> firstDateHistogram,
      List<DateHistogramDataPoint> secondDateHistogram) throws UnknownHostException {

    List<DateHistogramDataPoint> totalBytesInDateHistogram = new ArrayList<>();
    for (int i = 0; i < firstDateHistogram.size(); i++) {

      DateHistogramDataPoint ethernetDataPoint = firstDateHistogram.get(i);
      DateHistogramDataPoint loopBackDataPoint = secondDateHistogram.get(i);

      if (ethernetDataPoint.getValue().isNaN() || ethernetDataPoint.getValue().isInfinite()) {
        ethernetDataPoint.setValue(0D);
      }

      if (loopBackDataPoint.getValue().isNaN() || loopBackDataPoint.getValue().isInfinite()) {
        ethernetDataPoint.setValue(0D);
      }

      Double totalValue = ethernetDataPoint.getValue() + loopBackDataPoint.getValue();

      if (totalValue.isNaN() || totalValue.isInfinite()) {
        totalValue = 0D;
      }

      DateHistogramDataPoint totalDataPoint =
          new DateHistogramDataPoint(ethernetDataPoint.getTimestamp(), totalValue);
      totalBytesInDateHistogram.add(totalDataPoint);
    }

    return totalBytesInDateHistogram;
  }

  /**
   * Calculate the average based date histogram values for fields like SYSTEM_NETWORK_IN_PACKETS,
   * SYSTEM_NETWORK_OUT_PACKETS, SYSTEM_NETWORK_IN_BYTES, SYSTEM_NETWORK_IN_BYTES.
   *
   * @param dateHistogramList a list of histogram data points
   * @return the average of the data points
   */
  private Double calculateAverageFromDateHistogram(List<DateHistogramDataPoint> dateHistogramList) {

    Double total = 0D;
    for (DateHistogramDataPoint dateHistogramDataPoint : dateHistogramList) {
      if (!dateHistogramDataPoint.getValue().isNaN()
          && !dateHistogramDataPoint.getValue().isInfinite()) {
        total = total + dateHistogramDataPoint.getValue();
      }
    }

    return (total / dateHistogramList.size());
  }

  /**
   * Calculate the aggregated value based on the time window.
   *
   * @param dateHistogramList a list of histogram data points
   */
  private void calculateTotalValueAggregationByTimeWindow(
      List<DateHistogramDataPoint> dateHistogramList) {

    boolean isFirst = true;
    Double lastValue = 0D;

    for (DateHistogramDataPoint dataPoint : dateHistogramList) {

      if (dataPoint.getValue().isNaN() || dataPoint.getValue().isInfinite()
          || dataPoint.getValue().equals(0D)) {
        dataPoint.setValue(0D);
        continue;
      }

      if (isFirst) {
        lastValue = dataPoint.getValue();
        dataPoint.setValue(0D);
      } else {
        Double timeWindowValue = dataPoint.getValue() - lastValue;
        lastValue = dataPoint.getValue();
        dataPoint.setValue(timeWindowValue);
      }
      isFirst = false;
    }
  }

  /**
   * Retrieves most recent snapshot top SIZE hostnames disk io data.
   *
   * <p>
   * MetricbeatFields.SYSTEM_DISKIO_READ_BYTES
   *
   * <p>
   * MetricbeatFields.SYSTEM_DISKIO_READ_TIME
   *
   * <p>
   * MetricbeatFields.SYSTEM_DISKIO_WRITE_BYTES
   *
   * <p>
   * MetricbeatFields.SYSTEM_DISKIO_WRITE_TIME
   *
   * @param kibanaSearchParameters the index search parameter
   * @param size the number of elements to be fetched
   * @return a list of disk IO that matches the query
   * @throws UnknownHostException if the host does not exist
   */
  public List<DiskIODataPoint> getTopHostnamesDiskIO(KibanaSearchParameters kibanaSearchParameters,
      Integer size) throws UnknownHostException {

    kibanaSearchParameters.setSize(size);

    SearchRequestBuilder searchRequestBuilder = buildDFSQuerySearchRequest();
    searchRequestBuilder.setSize(0);
    applyMatchQueryAndTimestampRange(searchRequestBuilder, kibanaSearchParameters);

    searchRequestBuilder.addAggregation(
        buildDiskIODataAggregation(kibanaSearchParameters).size(kibanaSearchParameters.getSize()));

    SearchResponse searchResponse = searchRequestBuilder.execute().actionGet();

    List<DiskIODataPoint> dataPointList = new ArrayList<>();
    StringTerms result = searchResponse.getAggregations().get(ROOT_AGGREGATION);

    for (Terms.Bucket bucket : result.getBuckets()) {

      String hostname = (String) bucket.getKey();

      Avg readBytes = bucket.getAggregations().get(SYSTEM_DISKIO_READ_BYTES);
      Avg readTime = bucket.getAggregations().get(SYSTEM_DISKIO_READ_TIME);
      Avg writeBytes = bucket.getAggregations().get(SYSTEM_DISKIO_WRITE_BYTES);
      Avg writeTime = bucket.getAggregations().get(SYSTEM_DISKIO_WRITE_TIME);

      DiskIODataPoint processDataPoint = new DiskIODataPoint();
      processDataPoint.setHostname(hostname);
      processDataPoint.setReadBytes(readBytes.getValue());
      processDataPoint.setReadTime(readTime.getValue());
      processDataPoint.setWriteBytes(writeBytes.getValue());
      processDataPoint.setWriteTime(writeTime.getValue());

      dataPointList.add(processDataPoint);
    }

    return dataPointList;
  }

  /**
   * Retrieves most recent snapshot top SIZE hostnames network io data:
   *
   * <p>
   * MetricbeatFields.SYSTEM_NETWORK_IN_BYTES
   *
   * <p>
   * MetricbeatFields.SYSTEM_NETWORK_IN_PACKETS
   *
   * <p>
   * MetricbeatFields.SYSTEM_NETWORK_OUT_BYTES
   *
   * <p>
   * MetricbeatFields.SYSTEM_NETWORK_OUT_PACKETS
   *
   * @param kibanaSearchParameters the index search parameter
   * @param size the number of elements to be fetched
   * @return a list of the network IO data points that match the query
   * @throws UnknownHostException if the host does not exist
   */
  public List<NetworkIODataPoint> getTopHostnamesNetwork(
      KibanaSearchParameters kibanaSearchParameters, Integer size) throws UnknownHostException {

    kibanaSearchParameters.setSize(size);

    SearchRequestBuilder searchRequestBuilder = buildDFSQuerySearchRequest();
    searchRequestBuilder.setSize(0);
    applyMatchQueryAndTimestampRange(searchRequestBuilder, kibanaSearchParameters);

    searchRequestBuilder.addAggregation(buildNetworkIODataAggregation(kibanaSearchParameters)
        .size(kibanaSearchParameters.getSize()));

    SearchResponse searchResponse = searchRequestBuilder.execute().actionGet();

    List<NetworkIODataPoint> dataPointList = new ArrayList<>();
    StringTerms result = searchResponse.getAggregations().get(ROOT_AGGREGATION);

    for (Terms.Bucket bucket : result.getBuckets()) {

      String hostname = (String) bucket.getKey();

      Avg inBytes = bucket.getAggregations().get(SYSTEM_NETWORK_IN_BYTES);
      Avg inPackets = bucket.getAggregations().get(SYSTEM_NETWORK_IN_PACKETS);
      Avg outByes = bucket.getAggregations().get(SYSTEM_NETWORK_OUT_BYTES);
      Avg outPackets = bucket.getAggregations().get(SYSTEM_NETWORK_OUT_PACKETS);

      NetworkIODataPoint processDataPoint = new NetworkIODataPoint();
      processDataPoint.setHostname(hostname);
      processDataPoint.setInBytes(inBytes.getValue());
      processDataPoint.setOutByes(outByes.getValue());
      processDataPoint.setInPackets(inPackets.getValue());
      processDataPoint.setOutPackets(outPackets.getValue());
      dataPointList.add(processDataPoint);
    }

    return dataPointList;
  }

  /**
   * Builds aggregations to retrieve system disk io data by BEAT.NAME.
   *
   * <p>
   * MetricbeatFields.SYSTEM_DISKIO_READ_BYTES
   *
   * <p>
   * MetricbeatFields.SYSTEM_DISKIO_READ_TIME
   *
   * <p>
   * MetricbeatFields.SYSTEM_DISKIO_WRITE_BYTES
   *
   * <p>
   * MetricbeatFields.SYSTEM_DISKIO_WRITE_TIME
   *
   * @param parameters the index search parameter
   * @return the terms aggregation builder
   */
  private TermsAggregationBuilder buildDiskIODataAggregation(KibanaSearchParameters parameters) {
    return AggregationBuilders.terms(ROOT_AGGREGATION).field(SharedBeatFields.BEAT_NAME)
        .order(Terms.Order.aggregation(SYSTEM_DISKIO_READ_BYTES, false))
        .subAggregation(AggregationBuilders.avg(SYSTEM_DISKIO_READ_BYTES)
            .field(MetricbeatFields.SYSTEM_DISKIO_READ_BYTES))
        .subAggregation(AggregationBuilders.avg(SYSTEM_DISKIO_READ_TIME)
            .field(MetricbeatFields.SYSTEM_DISKIO_READ_TIME))
        .subAggregation(AggregationBuilders.avg(SYSTEM_DISKIO_WRITE_BYTES)
            .field(MetricbeatFields.SYSTEM_DISKIO_WRITE_BYTES))
        .subAggregation(AggregationBuilders.avg(SYSTEM_DISKIO_WRITE_TIME)
            .field(MetricbeatFields.SYSTEM_DISKIO_WRITE_TIME));
  }

  /**
   * Builds aggregations to retrieve system network io data by BEAT.NAME.
   *
   * <p>
   * MetricbeatFields.SYSTEM_NETWORK_IN_BYTES
   *
   * <p>
   * MetricbeatFields.SYSTEM_NETWORK_IN_PACKETS
   *
   * <p>
   * MetricbeatFields.SYSTEM_NETWORK_OUT_BYTES
   *
   * <p>
   * MetricbeatFields.SYSTEM_NETWORK_OUT_PACKETS
   *
   * @param parameters the index search parameter
   * @return the terms aggregation builder
   */
  private TermsAggregationBuilder buildNetworkIODataAggregation(KibanaSearchParameters parameters) {

    return AggregationBuilders.terms(ROOT_AGGREGATION).field(SharedBeatFields.BEAT_NAME)
        .order(Terms.Order.aggregation(SYSTEM_NETWORK_IN_BYTES, false))
        .subAggregation(AggregationBuilders.avg(SYSTEM_NETWORK_IN_BYTES)
            .field(MetricbeatFields.SYSTEM_NETWORK_IN_BYTES))
        .subAggregation(AggregationBuilders.avg(SYSTEM_NETWORK_IN_PACKETS)
            .field(MetricbeatFields.SYSTEM_NETWORK_IN_PACKETS))
        .subAggregation(AggregationBuilders.avg(SYSTEM_NETWORK_OUT_BYTES)
            .field(MetricbeatFields.SYSTEM_NETWORK_OUT_BYTES))
        .subAggregation(AggregationBuilders.avg(SYSTEM_NETWORK_OUT_PACKETS)
            .field(MetricbeatFields.SYSTEM_NETWORK_OUT_PACKETS));
  }

  /**
   * Retrieve all data from Elasticsearch to build System Load Chart.
   *
   * @param kibanaSearchParameters the index search parameter
   * @return the system load chart
   * @throws UnknownHostException if the host does not exist
   */
  public SystemLoadChart getSystemLoadChart(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {

    SystemLoadChart systemLoadChart = new SystemLoadChart();

    // MetricbeatFields.SYSTEM_LOAD_1
    systemLoadChart.setSystemLoad1Avg(getSystemLoad1Avg(kibanaSearchParameters));
    systemLoadChart
        .setSystemLoad1DateHistogramAvg(getSystemLoad1DateHistogramAvg(kibanaSearchParameters));

    // MetricbeatFields.SYSTEM_LOAD_5
    systemLoadChart.setSystemLoad5Avg(getSystemLoad5Avg(kibanaSearchParameters));
    systemLoadChart
        .setSystemLoad5DateHistogramAvg(getSystemLoad5DateHistogramAvg(kibanaSearchParameters));

    // MetricbeatFields.SYSTEM_LOAD_15
    systemLoadChart.setSystemLoad15Avg(getSystemLoad15Avg(kibanaSearchParameters));
    systemLoadChart
        .setSystemLoad15DateHistogramAvg(getSystemLoad15DateHistogramAvg(kibanaSearchParameters));

    return systemLoadChart;
  }

  /**
   * Remove all the entries related to the instance name from the elastic search data.
   *
   * @param name - the name of the instance.
   */
  public void deleteRecordsByInstanceName(Instance instance) {
    super.deleteRecordsByInstance(instance);
  }

}
