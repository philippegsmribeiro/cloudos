package cloudos.elasticsearch.services;


import cloudos.elasticsearch.fields.FilebeatFields;
import cloudos.elasticsearch.fields.SharedBeatFields;
import cloudos.elasticsearch.model.KibanaSearchParameters;
import cloudos.elasticsearch.model.LogHit;
import cloudos.queue.QueueServiceException;
import cloudos.queue.message.beats.FilebeatMessage;

import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import lombok.extern.log4j.Log4j2;

import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * Created by rogerio.souza on 05/14/17.
 *
 * <p>
 * Represent a Beat service that implements all Kibana search operations and fetch all Filebeat
 * index data
 */
@Service
@Lazy
@Log4j2
public class FilebeatService extends AbstractElasticsearchService {

  @Value("${cloudos.data.service.filebeat.index}")
  private String index;

  private static final String TIMESTAMP_FORMAT = "yyyy-MM-dd'T'hh:mm:ss.SSS";
  private SimpleDateFormat simpleDateFormat = new SimpleDateFormat(TIMESTAMP_FORMAT);

  /**
   * Retrieves the hits matching the query word against message field.
   *
   * @param kibanaSearchParameters: represents the parameters to query and match the word/phrase.
   * @return
   */
  @SuppressWarnings("unchecked")
  public List<LogHit> getHitsByMessageQueryWord(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {

    List<LogHit> logHitList = new ArrayList<>();
    List<SearchHit> hitList = searchMatchingByFieldQuery(kibanaSearchParameters);
    for (SearchHit searchHit : hitList) {
      LogHit logHit = buildLogHitFromSourceFields(searchHit);
      logHitList.add(logHit);
    }
    return logHitList;
  }

  /**
   * Builds a LogHit model object based on a generic source Map<String, Object> containing the
   * common beat fields and specific filebeat fields.
   *
   * @param SearchHit searchHit: ElasticSearch API single search hit returned from match query.
   * @return
   */
  private LogHit buildLogHitFromSourceFields(SearchHit searchHit) {
    Map<String, Object> fields = searchHit.getSource();
    Map<String, Object> beat = (Map<String, Object>) fields.get(SharedBeatFields.BEAT);
    LogHit logHit = new LogHit();
    logHit.setHostname((String) beat.get(SharedBeatFields.HOSTNAME));
    logHit.setName((String) beat.get(SharedBeatFields.NAME));
    logHit.setMessage((String) fields.get(FilebeatFields.MESSAGE));
    logHit.setSource((String) fields.get(FilebeatFields.SOURCE));
    logHit.setType((String) fields.get(FilebeatFields.TYPE));

    String stringDatetime = (String) fields.get(SharedBeatFields.TIMESTAMP);
    try {
      stringDatetime = stringDatetime.replaceAll("Z", "");
      logHit.setTimestamp(simpleDateFormat.parse(stringDatetime));
    } catch (ParseException e) {
      log.error(
          "Error when converting beat timestamp {} to Date using the yyyy-MM-DD hh:mm:ss.SSS format",
          stringDatetime);
      log.error(e);
    }
    return logHit;
  }


  /**
   * Ship all data from elasticsearch beat index to a specific beat index queue - filtering by range
   * considering the time window definition.
   *
   * @param size the size of the batch to be shipped
   * @param timewindowInSeconds the size of the time window, in seconds
   * @throws UnknownHostException in case the host is unknown or does not exist
   * @throws QueueServiceException whenever the hit cannot be sent to the queue
   */
  public List<SearchHit> shipData(Integer timewindowInSeconds, Integer size)
      throws UnknownHostException, QueueServiceException {

    KibanaSearchParameters kibanaSearchParameters =
        buildShipDataParameters(timewindowInSeconds, size);

    List<SearchHit> searchHitList = searchMatchingByFieldQuery(kibanaSearchParameters);

    for (SearchHit searchHit : searchHitList) {
      String hitJson = searchHit.getSourceAsString();
      FilebeatMessage message = new FilebeatMessage();
      message.setHitJson(hitJson);
      queueService.sendMessage(message);
    }

    return searchHitList;
  }

  /**
   * Retrieves the last hit field value filtered by BEAT.NAME and FilebeatFields.MESSAGE.
   *
   * @param kibanaSearchParameters a list of matching search parameters
   * @return average number
   * @throws UnknownHostException if the host does not exist
   */
  public String getLastErrorMessage(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(FilebeatFields.MESSAGE);
    return (String) getLastFieldValue(kibanaSearchParameters);
  }

  /**
   * Return the index of the filebeat.
   *
   * @return the name of the index
   */
  @Override
  protected String getIndex() {
    return index;
  }
}
