package cloudos.elasticsearch.services;

import cloudos.elasticsearch.fields.HeartbeatFields;
import cloudos.elasticsearch.model.KibanaSearchParameters;
import cloudos.queue.QueueServiceException;
import cloudos.queue.message.beats.HeartbeatMessage;

import java.net.UnknownHostException;
import java.util.List;

import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * Created by rogerio.souza on 05/14/17.
 *
 * <p>
 * Represent a Beat service that implements all Kibana search operations and fetch all Heartbeat
 * index data
 */
@Service
@Lazy
public class HeartbeatService extends AbstractElasticsearchService {

  @Value("${cloudos.data.service.heartbeat.index}")
  private String index;

  @Override
  protected String getIndex() {
    return index;
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by DURATION_US
   * heartbeats
   *
   * @param kibanaSearchParameters
   * @return
   */
  public Double getDurationUsAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(HeartbeatFields.DURATION_US);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by RESOLVE_RTT_US
   * heartbeats
   *
   * @param kibanaSearchParameters
   * @return
   */
  public Double getResolveRttUsAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(HeartbeatFields.RESOLVE_RTT_US);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by ICMP_RTT_US
   * heartbeats
   *
   * @param kibanaSearchParameters
   * @return
   */
  public Double getIcmpRttUsAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(HeartbeatFields.ICMP_RTT_US);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by TCP_CONNECT_RTT_US
   * heartbeats
   *
   * @param kibanaSearchParameters
   * @return
   */
  public Double getTcpConnectRttUsAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(HeartbeatFields.TCP_CONNECT_RTT_US);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by SOCKS5_CONNECT_RTT_US
   * heartbeats
   *
   * @param kibanaSearchParameters
   * @return
   */
  public Double getSocks5ConnectRttUsAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(HeartbeatFields.SOCKS5_CONNECT_RTT_US);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by TLS_HANDSHAKE_RTT_US
   * heartbeats
   *
   * @param kibanaSearchParameters
   * @return
   */
  public Double getTlsHandshakeRttUsAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(HeartbeatFields.TLS_HANDSHAKE_RTT_US);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by HTTP_RTT_US
   * heartbeats
   *
   * @param kibanaSearchParameters
   * @return
   */
  public Double getHttpRttUsAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(HeartbeatFields.HTTP_RTT_US);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by VALIDATE_RTT_US
   * heartbeats
   *
   * @param kibanaSearchParameters
   * @return
   */
  public Double getValidateRttUsAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(HeartbeatFields.VALIDATE_RTT_US);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by RESPONSE_STATUS
   * heartbeats
   *
   * @param kibanaSearchParameters
   * @return
   */
  public Double getResponseStatusAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(HeartbeatFields.RESPONSE_STATUS);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by UP heartbeats
   *
   * @param kibanaSearchParameters
   * @return
   */
  public Double getUpAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(HeartbeatFields.UP);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Ship all data from elasticsearch beat index to a specific beat index queue - filtering by range
   * considering the time window definition
   *
   * @param size
   * @param timewindowInSeconds
   * @throws UnknownHostException
   * @throws QueueServiceException
   */
  public List<SearchHit> shipData(Integer timewindowInSeconds, Integer size)
      throws UnknownHostException, QueueServiceException {

    KibanaSearchParameters kibanaSearchParameters =
        buildShipDataParameters(timewindowInSeconds, size);

    List<SearchHit> searchHitList = searchMatchingByFieldQuery(kibanaSearchParameters);

    for (SearchHit searchHit : searchHitList) {
      String hitJson = searchHit.getSourceAsString();
      HeartbeatMessage message = new HeartbeatMessage();
      message.setHitJson(hitJson);
      queueService.sendMessage(message);
    }

    return searchHitList;
  }
}
