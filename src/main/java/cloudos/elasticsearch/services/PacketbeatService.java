package cloudos.elasticsearch.services;

import cloudos.elasticsearch.fields.PacketbeatFields;
import cloudos.elasticsearch.model.KibanaSearchParameters;
import cloudos.queue.QueueServiceException;
import cloudos.queue.message.beats.PacketbeatMessage;

import java.net.UnknownHostException;
import java.util.List;

import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * Created by rogerio.souza on 05/14/17.
 *
 * <p>
 * Represent a Beat service that implements all Kibana search operations and fetch all Packetbeat
 * index data
 */
@Service
@Lazy
public class PacketbeatService extends AbstractElasticsearchService {

  @Value("${cloudos.data.service.packetbeat.index}")
  private String index;

  @Override
  protected String getIndex() {
    return index;
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by BYTES_IN packetbeats
   *
   * @param String
   * @return
   */
  public Double getBytesInAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(PacketbeatFields.BYTES_IN);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by BYTES_OUT packetbeats
   *
   * @param String
   * @return
   */
  public Double getBytesOutAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(PacketbeatFields.BYTES_OUT);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by COUNT packetbeats
   *
   * @param String
   * @return
   */
  public Double getCountAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(PacketbeatFields.COUNT);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by HTTP_CONTENT_LENGTH
   * packetbeats
   *
   * @param String
   * @return
   */
  public Double getHttpContentLengthAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(PacketbeatFields.HTTP_CONTENT_LENGTH);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by RESPONSETIME
   * packetbeats
   *
   * @param String
   * @return
   */
  public Double getResponsetimeAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(PacketbeatFields.RESPONSETIME);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Retrieves the last hour total average filtered by HOSTNAME and grouped by SHIPPER packetbeats
   *
   * @param String
   * @return
   */
  public Double getShipperAvg(KibanaSearchParameters kibanaSearchParameters)
      throws UnknownHostException {
    kibanaSearchParameters.setAggregationFieldName(PacketbeatFields.SHIPPER);
    return searchFieldAvg(kibanaSearchParameters);
  }

  /**
   * Ship all data from elasticsearch beat index to a specific beat index queue - filtering by range
   * considering the time window definition
   *
   * @param beatService
   * @param timewindowInSeconds
   * @throws UnknownHostException
   * @throws QueueServiceException
   */
  public List<SearchHit> shipData(Integer timewindowInSeconds, Integer size)
      throws UnknownHostException, QueueServiceException {

    KibanaSearchParameters kibanaSearchParameters =
        buildShipDataParameters(timewindowInSeconds, size);

    List<SearchHit> searchHitList = searchMatchingByFieldQuery(kibanaSearchParameters);

    for (SearchHit searchHit : searchHitList) {
      String hitJson = searchHit.getSourceAsString();
      PacketbeatMessage message = new PacketbeatMessage();
      message.setHitJson(hitJson);
      queueService.sendMessage(message);
    }

    return searchHitList;
  }
}
