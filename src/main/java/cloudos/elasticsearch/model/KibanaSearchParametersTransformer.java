package cloudos.elasticsearch.model;

import cloudos.elasticsearch.fields.FilebeatFields;
import cloudos.elasticsearch.fields.SharedBeatFields;

import com.amazonaws.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

/**
 * Class transforms the endpoint elasticsearch ChartParameters into KibanaSearchParameters
 *
 * @author Rogério Souza
 */
@Component
public class KibanaSearchParametersTransformer {

  public KibanaSearchParameters apply(ChartParameters chartParameters) {
    KibanaSearchParameters kibanaSearchParameters = new KibanaSearchParameters();

    if (chartParameters.getTimestampRangeFrom() != null) {
      kibanaSearchParameters.setRangeFrom(chartParameters.getTimestampRangeFrom());
    }

    if (chartParameters.getTimestampRangeTo() != null) {
      kibanaSearchParameters.setRangeTo(chartParameters.getTimestampRangeTo());
    }

    if (!StringUtils.isNullOrEmpty(chartParameters.getName())) {

      KibanaField beatname =
          new KibanaField(SharedBeatFields.BEAT_NAME, Arrays.asList(chartParameters.getName()));

      List<KibanaField> filterFields = new ArrayList<>();
      filterFields.add(beatname);
      kibanaSearchParameters.setFilterFields(filterFields);
    }

    return kibanaSearchParameters;
  }

  public KibanaSearchParameters apply(LogSearchParameters logSearchParameters) {
    KibanaSearchParameters kibanaSearchParameters = new KibanaSearchParameters();

    if (logSearchParameters.getTimestampRangeFrom() != null) {
      kibanaSearchParameters.setRangeFrom(logSearchParameters.getTimestampRangeFrom());
    }

    if (logSearchParameters.getTimestampRangeTo() != null) {
      kibanaSearchParameters.setRangeTo(logSearchParameters.getTimestampRangeTo());
    }

    if (logSearchParameters.getSize() != null) {
      kibanaSearchParameters.setSize(logSearchParameters.getSize());
    }

    if (!StringUtils.isNullOrEmpty(logSearchParameters.getName())) {

      KibanaField beatname =
          new KibanaField(SharedBeatFields.BEAT_NAME, Arrays.asList(logSearchParameters.getName()));

      List<KibanaField> filterFields = new ArrayList<>();
      filterFields.add(beatname);
      kibanaSearchParameters.setFilterFields(filterFields);
    }

    if (!StringUtils.isNullOrEmpty(logSearchParameters.getQuery())) {

      KibanaField beatname =
          new KibanaField(FilebeatFields.MESSAGE, Arrays.asList(logSearchParameters.getQuery()));

      List<KibanaField> filterFields = new ArrayList<>();
      filterFields.add(beatname);
      kibanaSearchParameters.setMatchFields(filterFields);
    }

    return kibanaSearchParameters;
  }
}
