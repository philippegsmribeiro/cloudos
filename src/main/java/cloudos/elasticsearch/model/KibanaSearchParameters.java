package cloudos.elasticsearch.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval;

import lombok.Data;

/**
 * Class represents the parameters to be used on back end when querying elastiscsearch data through
 * services
 *
 * @author Rogério Souza
 */
@Data
public class KibanaSearchParameters {

  private Long rangeTo = new Date().getTime() - TimeUnit.MINUTES.toMillis(15);
  private Long rangeFrom = rangeTo - TimeUnit.MINUTES.toMillis(60);
  private Integer size = 100;
  private String aggregationDescriptionFieldName = null;
  private String aggregationFieldName = null;
  private DateHistogramInterval aggregationInterval = DateHistogramInterval.MINUTE;
  private List<KibanaField> filterFields = new ArrayList<>();
  private List<KibanaField> matchFields = new ArrayList<>();

}
