package cloudos.elasticsearch.model;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Class represents the fields parameters to be used on back end when querying elastiscsearch data
 * through services
 *
 * @author Rogério Souza
 */
@Data
@AllArgsConstructor
public class KibanaField {

  private String name = null;
  private List<String> values = new ArrayList<>();

}
