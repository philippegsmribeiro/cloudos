package cloudos.elasticsearch.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Class represents the model for Date Histogram data point
 *
 * @author Rogério Souza
 */
@Data
@AllArgsConstructor
public class DateHistogramDataPoint {

  private Date timestamp;
  private Double value;

}
