package cloudos.elasticsearch.model;

import java.util.Date;

import lombok.Data;

/**
 * Class represents the model for Filebeat fields data point
 * 
 * <p>
 * SharedBeatFields.BEAT_NAME
 *
 * <p>
 * SharedBeatFields.TIMESTAMP
 *
 * <p>
 * SharedBeatFields.BEAT_HOSTNAME
 *
 * <p>
 * FilebeatFields.SOURCE
 *
 * <p>
 * FilebeatFields.MESSAGE
 *
 * @author Rogério Souza
 */
@Data
public class LogHit {

  private Date timestamp;
  private String name;
  private String hostname;
  private String source;
  private String message;
  private String type;

}
