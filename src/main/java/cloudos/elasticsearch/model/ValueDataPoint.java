package cloudos.elasticsearch.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Class represents the model for Description and Double Value data point
 *
 * @author Rogério Souza
 */
@Data
@AllArgsConstructor
public class ValueDataPoint {

  private String description;
  private Double value;

}
