package cloudos.elasticsearch.model;

import lombok.Data;

/**
 * Class represents the model for Metricbeat system disk io fields data point
 *
 * <p>
 * MetricbeatFields.SYSTEM_DISKIO_READ_BYTES
 *
 * <p>
 * MetricbeatFields.SYSTEM_DISKIO_READ_TIME
 *
 * <p>
 * MetricbeatFields.SYSTEM_DISKIO_WRITE_BYTES
 *
 * <p>
 * MetricbeatFields.SYSTEM_DISKIO_WRITE_TIME
 *
 * @author Rogério Souza
 */
@Data
public class DiskIODataPoint {

  private String hostname;
  private Double readBytes;
  private Double readTime;
  private Double writeBytes;
  private Double writeTime;

}
