package cloudos.elasticsearch.model;

import lombok.Data;

/**
 * Class represents the model for Metricbeat process fields data point
 *
 * <p>
 * MetricbeatFields.SYSTEM_PROCESS_MEMORY_RSS_PCT
 *
 * <p>
 * MetricbeatFields.SYSTEM_PROCESS_NAME
 *
 * <p>
 * MetricbeatFields.SYSTEM_PROCESS_USERNAME
 *
 * <p>
 * MetricbeatFields.SYSTEM_PROCESS_CPU_TOTAL_PCT
 *
 * @author Rogério Souza
 */
@Data
public class ProcessDataPoint {

  private String name;
  private String user;
  private Double cpuPct;
  private Double memoryPct;

}
