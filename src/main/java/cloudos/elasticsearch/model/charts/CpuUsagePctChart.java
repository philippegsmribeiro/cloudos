package cloudos.elasticsearch.model.charts;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

import cloudos.elasticsearch.model.DateHistogramDataPoint;
import lombok.Data;

/**
 * Class represents the model for Cpu Usage Pct Chart storing up the total average and date
 * histogram for each below Metricbeat field:
 *
 * <p>
 * MetricbeatFields.SYSTEM_CPU_IOWAIT_PCT
 *
 * <p>
 * MetricbeatFields.SYSTEM_CPU_IRQ_PCT
 *
 * <p>
 * MetricbeatFields.SYSTEM_CPU_NICE_PCT
 *
 * <p>
 * MetricbeatFields.SYSTEM_CPU_SOFTIRQ_PCT
 *
 * <p>
 * MetricbeatFields.SYSTEM_CPU_SYSTEM_PC
 *
 * <p>
 * MetricbeatFields.SYSTEM_CPU_USER_PCT
 *
 * @author Rogério Souza
 */
@Data
public class CpuUsagePctChart {

  private Double systemCpuIowaitPctAvg;
  private Double systemCpuIrqPctAvg;
  private Double systemCpuNicePctAvg;
  private Double systemCpuSoftirqPctAvg;
  private Double systemCpuSystemPctAvg;
  private Double systemCpuUserPctAvg;

  private List<DateHistogramDataPoint> systemCpuIowaitPctDateHistogramAvg = new ArrayList<>();
  private List<DateHistogramDataPoint> systemCpuIrqPctDateHistogramAvg = new ArrayList<>();
  private List<DateHistogramDataPoint> systemCpuNicePctDateHistogramAvg = new ArrayList<>();
  private List<DateHistogramDataPoint> systemCpuSoftirqPctDateHistogramAvg = new ArrayList<>();
  private List<DateHistogramDataPoint> systemCpuSystemPctDateHistogramAvg = new ArrayList<>();
  private List<DateHistogramDataPoint> systemCpuUserPctDateHistogramAvg = new ArrayList<>();

}
