package cloudos.elasticsearch.model.charts;

import java.util.ArrayList;
import java.util.List;

import cloudos.elasticsearch.model.DateHistogramDataPoint;
import lombok.Data;

/**
 * Class represents the model for Memory Usage Bytes Chart storing up the total average and date
 * histogram for each below Metricbeat field:
 *
 * <p>
 * MetricbeatFields.SYSTEM_MEMORY_FREE
 *
 * <p>
 * MetricbeatFields.SYSTEM_MEMORY_SWAP_USED_BYTES
 *
 * <p>
 * MetricbeatFields.SYSTEM_MEMORY_TOTAL
 *
 * <p>
 * MetricbeatFields.SYSTEM_MEMORY_USED_BYTES
 *
 * @author Rogério Souza
 */
@Data
public class MemoryUsageBytesChart {

  private Double systemMemoryFreeAvg;
  private Double systemMemorySwapAvg;
  private Double systemMemoryUsedAvg;
  private Double systemMemoryTotal;

  private List<DateHistogramDataPoint> systemMemoryFreeDateHistogramAvg = new ArrayList<>();
  private List<DateHistogramDataPoint> systemMemorySwapDateHistogramAvg = new ArrayList<>();
  private List<DateHistogramDataPoint> systemMemoryUsedDateHistogramAvg = new ArrayList<>();

}
