package cloudos.elasticsearch.model.charts;

import java.util.ArrayList;
import java.util.List;

import cloudos.elasticsearch.model.DateHistogramDataPoint;
import lombok.Data;

/**
 * Class represents the model for Network Traffic Bytes Chart storing up the total average and date
 * histogram for each below Metricbeat field:
 *
 * <p>
 * MetricbeatFields.SYSTEM_NETWORK_IN_BYTES
 *
 * <p>
 * MetricbeatFields.SYSTEM_NETWORK_OUT_BYTES
 *
 * @author Rogério Souza
 */
@Data
public class NetworkTrafficBytesChart {

  private Double systemNetworkInBytesAvg;
  private Double systemNetworkOutBytesAvg;

  private List<DateHistogramDataPoint> systemNetworkInBytesDateHistogramAvg = new ArrayList<>();
  private List<DateHistogramDataPoint> systemNetworkOutBytesDateHistogramAvg = new ArrayList<>();

}
