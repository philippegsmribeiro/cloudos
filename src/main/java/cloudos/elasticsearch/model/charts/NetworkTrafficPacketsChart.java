package cloudos.elasticsearch.model.charts;

import java.util.ArrayList;
import java.util.List;

import cloudos.elasticsearch.model.DateHistogramDataPoint;
import lombok.Data;

/**
 * Class represents the model for Network Traffic Packets Chart storing up the total average and
 * date histogram for each below Metricbeat field:
 *
 * <p>
 * MetricbeatFields.SYSTEM_NETWORK_IN_PACKETS
 *
 * <p>
 * MetricbeatFields.SYSTEM_NETWORK_OUT_PACKETS
 *
 * @author Rogério Souza
 */
@Data
public class NetworkTrafficPacketsChart {

  private Double systemNetworkInPacketsAvg;
  private Double systemNetworkOutPacketsAvg;

  private List<DateHistogramDataPoint> systemNetworkInPacketsDateHistogramAvg = new ArrayList<>();
  private List<DateHistogramDataPoint> systemNetworkOutPacketsDateHistogramAvg = new ArrayList<>();

}
