package cloudos.elasticsearch.model.charts;

import java.util.ArrayList;
import java.util.List;

import cloudos.elasticsearch.model.DateHistogramDataPoint;
import lombok.Data;

/**
 * Class represents the model for System Load Chart storing up the total average and date histogram
 * for each below Metricbeat field:
 *
 * <p>MetricbeatFields.SYSTEM_LOAD_1
 *
 * <p>MetricbeatFields.SYSTEM_LOAD_5
 *
 * <p>MetricbeatFields.SYSTEM_LOAD_15
 *
 * @author Rogério Souza
 */
@Data
public class SystemLoadChart {

  private Double systemLoad1Avg;
  private Double systemLoad5Avg;
  private Double systemLoad15Avg;

  private List<DateHistogramDataPoint> systemLoad1DateHistogramAvg = new ArrayList<>();
  private List<DateHistogramDataPoint> systemLoad5DateHistogramAvg = new ArrayList<>();
  private List<DateHistogramDataPoint> systemLoad15DateHistogramAvg = new ArrayList<>();

}
