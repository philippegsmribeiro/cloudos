package cloudos.elasticsearch.model;

import lombok.Data;

/**
 * Class represents the model for Metricbeat network fields data point
 *
 * <p>
 * MetricbeatFields.SYSTEM_NETWORK_IN_BYTES
 *
 * <p>
 * MetricbeatFields.SYSTEM_NETWORK_IN_PACKETS
 *
 * <p>
 * MetricbeatFields.SYSTEM_NETWORK_OUT_BYTES
 *
 * <p>
 * MetricbeatFields.SYSTEM_NETWORK_OUT_PACKETS
 *
 * @author Rogério Souza
 */
@Data
public class NetworkIODataPoint {

  private String hostname;
  private Double inBytes;
  private Double outByes;
  private Double inPackets;
  private Double outPackets;

}
