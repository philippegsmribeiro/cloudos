package cloudos.elasticsearch.model;

import lombok.Data;

/**
 * Class represents the parameters to be used on front end when querying elastiscsearch data through
 * REST endpoints
 *
 * @author Rogério Souza
 */
@Data
public class ChartParameters {

  private Long timestampRangeFrom;
  private Long timestampRangeTo;
  private String name;

}
