package cloudos.elasticsearch.fields;

/**
 * Constants representing filebeat exported fields:
 * https://www.elastic.co/guide/en/beats/filebeat/current/exported-fields.html
 *
 * @author Rogerio Souza
 */
public class FilebeatFields {

  public static final String SOURCE = "source";
  public static final String MESSAGE = "message";
  public static final String OFFSET = "offset";
  public static final String TYPE = "type";
  public static final String INPUT_TYPE = "input_type";
  public static final String ERROR = "error";
  public static final String READ_TIMESTAMP = "read_timestamp";
  public static final String FILESET_MODULE = "fileset.module";
  public static final String FILESET_NAME = "fileset.name";

}
