package cloudos.elasticsearch.fields;

/**
 * Constants representing beat shared exported fields:
 * https://www.elastic.co/guide/en/beats/filebeat/current/exported-fields.html
 * https://www.elastic.co/guide/en/beats/packetbeat/current/exported-fields.html
 * https://www.elastic.co/guide/en/beats/metricbeat/current/exported-fields.html
 * https://www.elastic.co/guide/en/beats/heartbeat/current/exported-fields.html
 *
 * @author Rogerio Souza
 */
public class SharedBeatFields {

  public static final String HOSTNAME = "hostname";
  public static final String NAME = "name";
  
  public static final String BEAT = "beat";
  public static final String TIMESTAMP = "@timestamp";
  public static final String BEAT_HOSTNAME = "beat.hostname";
  public static final String BEAT_NAME = "beat.name";
  public static final String BEAT_VERSION = "beat.version";
  
  public static final String META_CLOUD_PROVIDER = "meta.cloud.provider";
  public static final String META_CLOUD_INSTANCE_ID = "meta.cloud.instance_id";
  public static final String META_CLOUD_MACHINE_TYPE = "meta.cloud.machine_type";
  public static final String META_CLOUD_AVAILABILITY_ZONE = "meta.cloud.availability_zone";
  public static final String META_CLOUD_PROJECT_ID = "meta.cloud.project_id";
  public static final String META_CLOUD_REGION = "meta.cloud.region";
}
