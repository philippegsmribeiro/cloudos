package cloudos.elasticsearch.fields;

/**
 * Constants representing packetbeat exported fields:
 * https://www.elastic.co/guide/en/beats/packetbeat/current/exported-fields.html
 *
 * @author Rogerio Souza
 */
public class PacketbeatFields {

  public static final String BYTES_IN = "bytes_in";
  public static final String BYTES_OUT = "bytes_out";
  public static final String CLIENT_IP = "client_ip";
  public static final String CLIENT_LOCATION = "client_location";
  public static final String CLIENT_PORT = "client_port";
  public static final String CLIENT_PROC = "client_proc";
  public static final String CLIENT_SERVER = "client_server";
  public static final String COUNT = "count";
  public static final String HTTP_CODE = "http.code";
  public static final String HTTP_CONTENT_LENGTH = "http.content_length";
  public static final String HTTP_PHRASE = "http.phrase";
  public static final String HTTP_REQUEST_HEADERS_HOST = "http.request_headers.host";
  public static final String HTTP_RESPONSE_HEADERS_CONTENT_TYPE =
      "http.response_headers.content_type";
  public static final String IP = "ip";
  public static final String METHOD = "method";
  public static final String MONGODB_CURSORID = "mongodb.cursorId";
  public static final String MONGODB_ERROR = "mongodb.error";
  public static final String TMONGODB_FULLCOLLECTIONNAME = "tmongodb.fullCollectionName";
  public static final String MONGODB_NUMBERRETURNED = "mongodb.numberReturned";
  public static final String MONGODB_NUMBERTORETURN = "mongodb.numberToReturn";
  public static final String MONGODB_NUMBERTOSKIP = "mongodb.numberToSkip";
  public static final String MONGODB_STARTINGFROM = "mongodb.startingFrom";
  public static final String MYSQL_AFFECTED_ROWS = "mysql.affected_rows";
  public static final String MYSQL_ERROR_CODE = "mysql.error_code";
  public static final String MYSQL_ERROR_MESSAGE = "mysql.error_message";
  public static final String MYSQL_INSERT_ID = "mysql.insert_id";
  public static final String MYSQL_ISERROR = "mysql.iserror";
  public static final String MYSQL_NUM_FIELDS = "mysql.num_fields";
  public static final String MYSQL_NUM_ROWS = "mysql.num_rows";
  public static final String PATH = "path";
  public static final String PGSQL_ERROR_CODE = "pgsql.error_code";
  public static final String PGSQL_ERROR_MESSAGE = "pgsql.error_message";
  public static final String PGSQL_ISERROR = "pgsql.iserror";
  public static final String PGSQL_NUM_FIELDS = "pgsql.num_fields";
  public static final String PGSQL_NUM_ROWS = "pgsql.num_rows";
  public static final String PORT = "port";
  public static final String PROC = "proc";
  public static final String QUERY = "query";
  public static final String REDIS_RETURN_VALUE = "redis.return_value";
  public static final String REQUEST = "request";
  public static final String RESOURCE = "resource";
  public static final String RESPONSE = "response";
  public static final String RESPONSETIME = "responsetime";
  public static final String SERVER = "server";
  public static final String SHIPPER = "shipper";
  public static final String STATUS = "status";
  public static final String THRIFT_EXCEPTIONS = "thrift.exceptions";
  public static final String THRIFT_PARAMS = "thrift.params";
  public static final String THRIFT_RETURN_VALUE = "thrift.return_value";
  public static final String TIMESTAMP = "timestamp";
  public static final String TYPE = "type";
}
