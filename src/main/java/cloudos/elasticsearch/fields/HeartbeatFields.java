package cloudos.elasticsearch.fields;

/**
 * Constants representing filebeat exported_FIELDS:
 * https://www.elastic.co/guide/en/beats/heartbeat/current/exported-fields.html
 *
 * @author Rogerio Souza
 */
public class HeartbeatFields {

  public static final String TYPE = "type";
  public static final String MONITOR = "monitor";
  public static final String SCHEME = "scheme";
  public static final String HOST = "host";
  public static final String PORT = "port";
  public static final String URL = "url";
  public static final String IP = "ip";
  public static final String DURATION_US = "duration.us";
  public static final String RESOLVE_RTT_US = "resolve_rtt.us";
  public static final String ICMP_RTT_US = "icmp_rtt.us";
  public static final String TCP_CONNECT_RTT_US = "tcp_connect_rtt.us";
  public static final String SOCKS5_CONNECT_RTT_US = "socks5_connect_rtt.us";
  public static final String TLS_HANDSHAKE_RTT_US = "tls_handshake_rtt.us";
  public static final String HTTP_RTT_US = "http_rtt.us";
  public static final String VALIDATE_RTT_US = "validate_rtt.us";
  public static final String RESPONSE_STATUS = "response.status";
  public static final String UP = "up";
  public static final String ERROR_TYPE = "error.type";
  public static final String ERROR_MESSAGE = "error.message";
}
