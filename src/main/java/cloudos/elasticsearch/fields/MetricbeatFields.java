package cloudos.elasticsearch.fields;

/**
 * Constants representing metricbeat exported fields:
 * https://www.elastic.co/guide/en/beats/metricbeat/current/exported-fields.html
 *
 * @author Rogerio Souza
 */
public class MetricbeatFields {

  public static final String METRICSET_MODULE = "tmetricset.module";
  public static final String METRICSET_NAME = "tmetricset.name";
  public static final String METRICSET_RTT = "metricset.rtt";
  public static final String SYSTEM_CPU_CORES = "system.cpu.cores";
  public static final String SYSTEM_CPU_IDLE_PCT = "system.cpu.idle.pct";
  public static final String SYSTEM_CPU_IOWAIT_PCT = "system.cpu.iowait.pct";
  public static final String SYSTEM_CPU_IRQ_PCT = "system.cpu.irq.pct";
  public static final String SYSTEM_CPU_NICE_PCT = "system.cpu.nice.pct";
  public static final String SYSTEM_CPU_SOFTIRQ_PCT = "system.cpu.softirq.pct";
  public static final String SYSTEM_CPU_STEAL_PCT = "system.cpu.steal.pct";
  public static final String SYSTEM_CPU_SYSTEM_PCT = "system.cpu.system.pct";
  public static final String SYSTEM_CPU_USER_PCT = "system.cpu.user.pct";
  public static final String SYSTEM_DISKIO_READ_BYTES = "system.diskio.read.bytes";
  public static final String SYSTEM_DISKIO_WRITE_BYTES = "system.diskio.write.bytes";
  public static final String SYSTEM_DISKIO_READ_TIME = "system.diskio.read.time";
  public static final String SYSTEM_DISKIO_WRITE_TIME = "system.diskio.write.time";
  public static final String SYSTEM_FILESYSTEM_AVAILABLE = "system.filesystem.available";
  public static final String SYSTEM_FILESYSTEM_DEVICE_NAME = "system.filesystem.device_name";
  public static final String SYSTEM_FILESYSTEM_FILES = "system.filesystem.files";
  public static final String SYSTEM_FILESYSTEM_FREE = "system.filesystem.free";
  public static final String SYSTEM_FILESYSTEM_FREE_FILES = "system.filesystem.free_files";
  public static final String SYSTEM_FILESYSTEM_MOUNT_POINT = "system.filesystem.mount_point";
  public static final String SYSTEM_FILESYSTEM_TOTAL = "system.filesystem.total";
  public static final String SYSTEM_FILESYSTEM_USED_BYTES = "system.filesystem.used.bytes";
  public static final String SYSTEM_FILESYSTEM_USED_PCT = "system.filesystem.used.pct";
  public static final String SYSTEM_LOAD_1 = "system.load.1";
  public static final String SYSTEM_LOAD_15 = "system.load.15";
  public static final String SYSTEM_LOAD_5 = "system.load.5";
  public static final String SYSTEM_LOAD_NORM_1 = "system.load.norm.1";
  public static final String SYSTEM_LOAD_NORM_15 = "system.load.norm.15";
  public static final String SYSTEM_LOAD_NORM_5 = "system.load.norm.5";
  public static final String SYSTEM_MEMORY_ACTUAL_FREE = "system.memory.actual.free";
  public static final String SYSTEM_MEMORY_ACTUAL_USED_BYTES = "system.memory.actual.used.bytes";
  public static final String SYSTEM_MEMORY_ACTUAL_USED_PCT = "system.memory.actual.used.pct";
  public static final String SYSTEM_MEMORY_FREE = "system.memory.free";
  public static final String SYSTEM_MEMORY_SWAP_FREE = "system.memory.swap.free";
  public static final String SYSTEM_MEMORY_SWAP_TOTAL = "system.memory.swap.total";
  public static final String SYSTEM_MEMORY_SWAP_USED_BYTES = "system.memory.swap.used.bytes";
  public static final String SYSTEM_MEMORY_SWAP_USED_PCT = "system.memory.swap.used.pct";
  public static final String SYSTEM_MEMORY_TOTAL = "system.memory.total";
  public static final String SYSTEM_MEMORY_USED_BYTES = "system.memory.used.bytes";
  public static final String SYSTEM_MEMORY_USED_PCT = "system.memory.used.pct";
  public static final String SYSTEM_NETWORK_IN_BYTES = "system.network.in.bytes";
  public static final String SYSTEM_NETWORK_IN_DROPPED = "system.network.in.dropped";
  public static final String SYSTEM_NETWORK_IN_ERRORS = "system.network.in.errors";
  public static final String SYSTEM_NETWORK_IN_PACKETS = "system.network.in.packets";
  public static final String SYSTEM_NETWORK_NAME = "system.network.name";
  public static final String SYSTEM_NETWORK_OUT_BYTES = "system.network.out.bytes";
  public static final String SYSTEM_NETWORK_OUT_DROPPED = "system.network.out.dropped";
  public static final String SYSTEM_NETWORK_OUT_ERRORS = "system.network.out.errors";
  public static final String SYSTEM_NETWORK_OUT_PACKETS = "system.network.out.packets";
  public static final String SYSTEM_PROCESS_CMDLINE = "system.process.cmdline";
  public static final String SYSTEM_PROCESS_CPU_START_TIME = "system.process.cpu.start_time";
  public static final String SYSTEM_PROCESS_CPU_TOTAL_PCT = "system.process.cpu.total.pct";
  public static final String SYSTEM_PROCESS_FD_LIMIT_HARD = "system.process.fd.limit.hard";
  public static final String SYSTEM_PROCESS_FD_LIMIT_SOFT = "system.process.fd.limit.soft";
  public static final String SYSTEM_PROCESS_FD_OPEN = "system.process.fd.open";
  public static final String SYSTEM_PROCESS_MEMORY_RSS_BYTES = "system.process.memory.rss.bytes";
  public static final String SYSTEM_PROCESS_MEMORY_RSS_PCT = "system.process.memory.rss.pct";
  public static final String SYSTEM_PROCESS_MEMORY_SHARE = "system.process.memory.share";
  public static final String SYSTEM_PROCESS_MEMORY_SIZE = "system.process.memory.size";
  public static final String SYSTEM_PROCESS_NAME = "system.process.name";
  public static final String SYSTEM_PROCESS_PGID = "system.process.pgid";
  public static final String SYSTEM_PROCESS_PID = "system.process.pid";
  public static final String SYSTEM_PROCESS_PPID = "system.process.ppid";
  public static final String SYSTEM_PROCESS_STATE = "system.process.state";
  public static final String SYSTEM_PROCESS_USERNAME = "system.process.username";
  public static final String TYPE = "type";

  public static final String SYSTEM_NETWORK_NAME_ETHERNET = "eth0";
  public static final String SYSTEM_NETWORK_NAME_LOOPBACK = "lo";
}
