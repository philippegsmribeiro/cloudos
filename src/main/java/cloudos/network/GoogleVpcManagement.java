package cloudos.network;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.api.services.compute.Compute;
import com.google.api.services.compute.model.Network;
import com.google.api.services.compute.model.NetworkList;

import cloudos.Providers;
import cloudos.exceptions.NotImplementedException;
import cloudos.google.integration.GoogleComputeIntegration;
import cloudos.models.network.CloudosSubnet;
import cloudos.models.network.CloudosVpc;
import cloudos.models.network.CloudosVpcRequest;
import cloudos.models.network.GoogleSubnet;
import cloudos.models.network.GoogleVpc;
import cloudos.models.network.VpcUpdateRequest;
import cloudos.utils.DateUtil;
import lombok.extern.log4j.Log4j2;

/**
 * @author gerson.lima@cloudtownai.com
 *
 */
@Service
@Log4j2
public class GoogleVpcManagement implements VpcManagement<GoogleVpc> {

  @Autowired
  private GoogleComputeIntegration googleIntegration;

  @Autowired
  private GoogleSubnetManagement googleSubnetManagement;

  /**
   * Create a new GoogleVpc based on the request.
   *
   * @param cloudosVpcRequest the request to create a new VPC
   * @return a newly created CloudosVpc or null
   * @throws Exception any exception that may occur
   */
  @Override
  public CloudosVpc create(CloudosVpcRequest cloudosVpcRequest) {

    try {
      Network network = new Network();
      network.setName(cloudosVpcRequest.getName());
      network.setDescription(cloudosVpcRequest.getRegion());
      network.setAutoCreateSubnetworks(false);

      Network createdNetwork = googleIntegration.insertVpc(network);

      return GoogleVpc.builder() //
          .vpcId(createdNetwork.getId().toString()) //
          .region(cloudosVpcRequest.getRegion()) //
          .provider(cloudosVpcRequest.getProvider()) //
          .name(cloudosVpcRequest.getName()) //
          .url(createdNetwork.getSelfLink())//
          .build();

    } catch (Exception e) {
      log.error("Error creating VPC for Google Cloud {}", cloudosVpcRequest.getName(), e);
      throw new CloudosVpcException("Error creating VPC for Google Cloud", e);
    }

  }

  @Override
  public CloudosVpc update(VpcUpdateRequest request) throws NotImplementedException {
    // Not implement
    throw new NotImplementedException();
  }

  /**
   * Delete an existing VPC.
   *
   * @param cloudosVpc - GoogleVpc to delete
   */
  @Override
  public void delete(GoogleVpc cloudosVpc) {

    try {
      // delete subnets before delete VPC
      List<? extends CloudosSubnet> subnets = googleSubnetManagement.list(cloudosVpc);
      for (CloudosSubnet subnet : subnets) {
        googleSubnetManagement.delete((GoogleSubnet) subnet, cloudosVpc.getRegion());
      }

      // delete VPC
      googleIntegration.deleteVpc(cloudosVpc.getName());
    } catch (Exception e) {
      log.error("Error to delete Google VPC {} ", cloudosVpc.getName(), e);
      throw new CloudosVpcException("Error to delete Google VPC", e);
    }
  }


  /**
   * List VPC by region.
   *
   * @param region - region to list
   * @return List with VPCs
   */
  @Override
  public List<? extends CloudosVpc> list(@NotNull String region) {

    try {
      Compute.Networks.List request = googleIntegration.listVpc();

      List<GoogleVpc> vpcs = new ArrayList<>();
      NetworkList response;

      do {
        response = request.execute();
        if (response.getItems() != null) {
          for (Network network : response.getItems()) {
            vpcs.add(parseGoogleVpc(region, network));
          }
          request.setPageToken(response.getNextPageToken());
        }
      } while (response.getNextPageToken() != null);

      return vpcs;

    } catch (IOException e) {
      log.error("List VPC for region {}", region, e);
      throw new CloudosVpcException("List VPC for region " + region, e);
    }
  }

  /**
   * Describe a given Google VPC.
   *
   * @param region - the region where the VPC resides
   * @param name - the name of the VPC
   * @return a Google VPC if it exists, null otherwise
   */
  @Override
  public CloudosVpc describe(String region, String name) {

    try {
      Network network = googleIntegration.getVpc(name);
      return parseGoogleVpc(region, network);
    } catch (IOException e) {
      log.error("Failed to describe VPC name {}", name, e);
      throw new CloudosVpcException("Failed to describe VPC name " + name, e);
    }
  }

  private GoogleVpc parseGoogleVpc(String region, Network network) {

    GoogleVpc googleVpc = GoogleVpc.builder() //
        .creation(DateUtil.getIsoDateTime(network.getCreationTimestamp())) //
        .name(network.getName()) //
        .provider(Providers.GOOGLE_COMPUTE_ENGINE) //
        .region(region) //
        .vpcId(network.getId().toString()) //
        .url(network.getSelfLink()) //
        .build();

    if (CollectionUtils.isNotEmpty(network.getSubnetworks())) {
      googleVpc.setSubnets(new HashSet<>(network.getSubnetworks()));
    }

    return googleVpc;
  }

}
