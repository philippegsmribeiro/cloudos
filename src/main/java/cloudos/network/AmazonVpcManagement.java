package cloudos.network;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.AssociateVpcCidrBlockRequest;
import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.Subnet;
import com.amazonaws.services.ec2.model.Tag;
import com.amazonaws.services.ec2.model.Vpc;

import cloudos.AmazonService;
import cloudos.Providers;
import cloudos.amazon.VPC;
import cloudos.models.network.AwsVpc;
import cloudos.models.network.CloudosVpc;
import cloudos.models.network.CloudosVpcRequest;
import cloudos.models.network.VpcUpdateRequest;
import cloudos.securitygroup.CloudSecurityGroupException;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class AmazonVpcManagement implements VpcManagement<AwsVpc> {

  @Autowired
  private CloudosVpcCache cloudosVpcCache;

  @Autowired
  private AmazonService amazonService;

  /**
   * Returns the aws client.
   *
   * @param region of client
   * @return AmazonEC2 client
   * @throws CloudSecurityGroupException if something goes wrong creating the client
   */
  private AmazonEC2 getAmazonClient(String region) throws CloudSecurityGroupException {
    AmazonEC2 awsClient = amazonService.getClient(region);
    if (awsClient == null) {
      throw new CloudSecurityGroupException(
          String.format("No Amazon Client obtained by the region informed. Region: %s", region));
    }
    return awsClient;
  }

  /**
   * Create a new AWS VPC. The AwsVpc model holds the information needed in order to create
   * the vpc.
   *
   * @param cloudosVpcRequest the Request to create a new CloudosVpc
   * @return CloudosVpc the newly created VPC object. If the VPC already exists, then return null
   * @throws Exception any exception that may occur
   */
  @Override
  public CloudosVpc create(CloudosVpcRequest cloudosVpcRequest) {
    // Vpc already exists
    VPC vpc = this.cloudosVpcCache.getVpc(cloudosVpcRequest.getRegion());
    Vpc created;

    // bug right here ...
    if (StringUtils.isNotEmpty(cloudosVpcRequest.getName())) {
      if (cloudosVpcRequest.getTags() != null) {
        created = vpc.createVpc(cloudosVpcRequest.getCidrBlock(),
            cloudosVpcRequest.getInstanceTenancy(),
            cloudosVpcRequest.getIpv6CidrBlock(),
            cloudosVpcRequest.getName(),
            cloudosVpcRequest.getTags());
      }
      else {
        created = vpc.createVpc(cloudosVpcRequest.getCidrBlock(), cloudosVpcRequest.getInstanceTenancy());
      }

    } else {
      created = vpc.createVpc(cloudosVpcRequest.getCidrBlock());
    }

    return AwsVpc.builder()
        .state(created.getState())
        .cidrBlock(created.getCidrBlock())
        .isDefault(created.isDefault())
        .instanceTenancy(created.getInstanceTenancy())
        .tags(created.getTags())
        .provider(Providers.AMAZON_AWS)
        .vpcId(created.getVpcId())
        .tags(created.getTags())
        .cidrBlock(created.getCidrBlock())
        .instanceTenancy(created.getInstanceTenancy())
        .state(created.getState())
        .isDefault(created.getIsDefault())
        .dhcpOptionsId(created.getDhcpOptionsId())
        .ipv6CidrBlockAssociationSet(created.getIpv6CidrBlockAssociationSet())
        .name(cloudosVpcRequest.getName())
        .region(cloudosVpcRequest.getRegion())
        .subnets(new HashSet<>())
        .build();
  }

  @Override
  public CloudosVpc update(VpcUpdateRequest request) {
    CloudosVpc current = this.describe(request.getRegion(), request.getVpcId());
    // obtained a valid VPC
    if (current != null) {
      AmazonEC2 ec2 = this.amazonService.getClient(request.getRegion());

      // Vpc already exists
      AssociateVpcCidrBlockRequest blockRequest = new AssociateVpcCidrBlockRequest()
                  .withCidrBlock(request.getIpv4Cidr())
                  .withAmazonProvidedIpv6CidrBlock(request.getIsIpv6Cidr())
                  .withVpcId(request.getVpcId());

      ec2.associateVpcCidrBlock(blockRequest);

      if (request.getName() != null) {
        List<Tag> tags = new ArrayList<>();
        Tag newTag = new Tag();
        newTag.setKey("Name");
        newTag.setValue(request.getName());
        tags.add(newTag);
        CreateTagsRequest createTagsRequest = new CreateTagsRequest();
        createTagsRequest.setTags(tags);
        createTagsRequest.withResources(request.getVpcId());
        ec2.createTags(createTagsRequest);
      }

      return this.describe(request.getRegion(), request.getVpcId());

    }
    return null;
  }

  /**
   * Delete an existing VPC.
   *
   * @param cloudosVpc an existing VPC. If the VPC does not exist in AWS, then nothing will be done
   */
  @Override
  public void delete(AwsVpc cloudosVpc) {
    if (cloudosVpc != null &&
        this.describe(cloudosVpc.getRegion(), cloudosVpc.getVpcId()) != null) {
      // delete the VPC
      // Vpc already exists
      VPC vpc = this.cloudosVpcCache.getVpc(cloudosVpc.getRegion());
      // delete the VPC
      vpc.deleteVpc(cloudosVpc.getVpcId());
    }
  }

  /**
   * Given a region, return all the VPCs in that particular region.
   *
   * @param region a valid AWS region
   * @return return a list of all the VPCs in that region as AwsVpc
   */
  @Override
  public List<? extends CloudosVpc> list(@NotNull String region) {
    // get a VPC connector
    VPC vpc = this.cloudosVpcCache.getVpc(region);
    List<Vpc> vpcList = vpc.listVpcs();
    List<Subnet> subnets = vpc.listSubnets();
    // return the converted list.
    return convertToCloudosVpc(vpcList, region, subnets);
  }

  /**
   * Describe the state of a given AwsVpc.
   *
   * @param region the region where the AWS VPC resides
   * @param id the vpc id
   * @return a single AWS VPC if found, null otherwise
   */
  @Override
  public CloudosVpc describe(String region, String id) {
    VPC vpc = this.cloudosVpcCache.getVpc(region);
    List<Vpc> vpcList = vpc.describeVpc(id);
    List<Subnet> subnets = vpc.listSubnets();
    List<? extends CloudosVpc> cloudosVpcs = convertToCloudosVpc(vpcList, region, subnets);
    if (cloudosVpcs.size() > 0) {
      return cloudosVpcs.get(0);
    }
    return null;
  }

  /**
   * Given a list of the AWS Vpc model, convert the Vpc model into a AwsVpc model.
   *
   * @param vpcList a list of Vpc models
   * @param region region of the vpcs
   * @return a list of AwsVpc models
   */
  private List<? extends CloudosVpc> convertToCloudosVpc(List<Vpc> vpcList, String region, List<Subnet> subnets) {
    List<AwsVpc> awsVpcs = new ArrayList<>();
    // convert Vpc to AwsVpc
    for (Vpc vpcItem : vpcList) {
      AwsVpc awsVpc = AwsVpc.builder()
          .state(vpcItem.getState())
          .cidrBlock(vpcItem.getCidrBlock())
          .isDefault(vpcItem.isDefault())
          .instanceTenancy(vpcItem.getInstanceTenancy())
          .tags(vpcItem.getTags())
          .provider(Providers.AMAZON_AWS)
          .region(region)
          .vpcId(vpcItem.getVpcId())
          .tags(vpcItem.getTags())
          .cidrBlock(vpcItem.getCidrBlock())
          .instanceTenancy(vpcItem.getInstanceTenancy())
          .state(vpcItem.getState())
          .isDefault(vpcItem.getIsDefault())
          .dhcpOptionsId(vpcItem.getDhcpOptionsId())
          .ipv6CidrBlockAssociationSet(vpcItem.getIpv6CidrBlockAssociationSet())
          .subnets(new HashSet<>())
          .build();

      if (vpcItem.getTags() != null) {
        // find the name tag
        for (Tag tag : vpcItem.getTags()) {
          if (tag.getKey().equals("Name")) {
            awsVpc.setName(tag.getValue());
          }
        }
      }

      if (subnets != null) {
        // now get the Subnets in the VPC
        for (Subnet subnet : subnets) {
          if (vpcItem.getVpcId() != null && subnet.getVpcId().equals(vpcItem.getVpcId())) {
            awsVpc.getSubnets().add(subnet.getSubnetId());
          }
        }
      }

      // we need to add the subnets and VPC name
      awsVpcs.add(awsVpc);

    }
    return awsVpcs;
  }
}
