package cloudos.network;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.api.services.compute.Compute;
import com.google.api.services.compute.model.Subnetwork;
import com.google.api.services.compute.model.SubnetworkList;

import cloudos.google.integration.GoogleComputeIntegration;
import cloudos.models.network.CloudosSubnet;
import cloudos.models.network.CloudosSubnetRequest;
import cloudos.models.network.CloudosVpc;
import cloudos.models.network.GoogleSubnet;
import cloudos.models.network.GoogleVpc;
import cloudos.models.network.SubnetUpdateRequest;
import cloudos.utils.DateUtil;
import lombok.extern.log4j.Log4j2;

/**
 * @author gerson.lima@cloudtownai.com
 *
 */
@Service
@Log4j2
public class GoogleSubnetManagement implements SubnetManagement<GoogleSubnet> {

  public static final Logger logger = LogManager.getLogger(GoogleSubnetManagement.class);

  @Autowired
  private GoogleComputeIntegration googleIntegration;


  /**
   * Create a new Google Subnetwork to the specified VPC based on the request.
   *
   * @param cloudosSubnetRequest - the request to create a new Subnetwork
   * @param cloudosVpc - network on which the subnetwork belongs
   * @return CloudosSubnet - a newly created CloudosSubnet or null
   * @throws Exception - any exception that may occur
   */
  @Override
  public CloudosSubnet create(CloudosSubnetRequest cloudosSubnetRequest, CloudosVpc cloudosVpc)
      throws Exception {

    try {
      GoogleVpc googleVpc = ((GoogleVpc) cloudosVpc);

      Subnetwork subnetwork = new Subnetwork();
      subnetwork.setIpCidrRange(cloudosSubnetRequest.getCidr());
      subnetwork.setName(cloudosSubnetRequest.getName());
      subnetwork.setNetwork(googleVpc.getUrl());
      subnetwork.setRegion(googleVpc.getRegion());

      Subnetwork request = googleIntegration.insertSubnetwork(cloudosVpc.getRegion(), subnetwork);

      return GoogleSubnet.builder()
          .availabilityZone(request.getRegion())
          .cidr(cloudosSubnetRequest.getCidr())
          .name(cloudosSubnetRequest.getName())
          .isPrivateSubnet(cloudosSubnetRequest.getIsPrivateSubnet())
          .subnetId(request.getId().toString())
          .vpc(cloudosVpc.getVpcId())
          .build();

    } catch (IOException e) {
      log.error("Error creating Subnet for Google Cloud {}", cloudosSubnetRequest.getName(), e);
      throw new CloudosSubnetException("Error creating Subnet for Google Cloud", e);
    }
  }

  @Override
  public CloudosSubnet update(SubnetUpdateRequest request) {
    // Not implement
    throw new NotImplementedException();
  }

  /**
   * Delete an existing subnetwork.
   *
   * @param cloudosSubnet - subnetwork to delete
   * @param region - Name of the region scoping this request
   */
  @Override
  public void delete(GoogleSubnet cloudosSubnet, String region) {

    try {
      googleIntegration.deleteSubnetwork(region, cloudosSubnet.getName());
    } catch (IOException e) {
      log.error("Error to delete Google Subnet {} ", cloudosSubnet.getName(), e);
      throw new CloudosSubnetException("Error to delete Google Subnet " + cloudosSubnet.getName(),
          e);
    }
  }

  /**
   * List subnetworks by VPC and region.
   *
   * @param cloudosVpc - network on which the subnetwork belongs
   * @return List with subnetworks
   */
  @Override
  public List<? extends CloudosSubnet> list(CloudosVpc cloudosVpc) {
    try {
      GoogleVpc googleVpc = (GoogleVpc) cloudosVpc;
      Compute.Subnetworks.List request = googleIntegration.listSubnetwork(googleVpc.getRegion())
          .setFilter("network eq " + googleVpc.getUrl());

      List<GoogleSubnet> subnets = new ArrayList<>();
      SubnetworkList response;

      do {
        response = request.execute();
        if (response.getItems() != null) {
          for (Subnetwork subnetwork : response.getItems()) {
            subnets.add(parseSubnetwork(subnetwork, googleVpc.getVpcId()));
          }
          request.setPageToken(response.getNextPageToken());
        }
      } while (response.getNextPageToken() != null);

      return subnets;

    } catch (IOException e) {
      log.error("List Subnetworks for region {}", cloudosVpc.getRegion(), e);
      throw new CloudosSubnetException("List Subnetworks for region {}" + cloudosVpc.getRegion(),
          e);
    }
  }

  /**
   * Describe a given Google Subnetwork.
   *
   * @param region - the subnetwork to describe
   * @param vpcId - the VPC ID the subnet belongs to
   * @param name - the name of the subnet
   * @return the specified subnetwork
   */
  @Override
  public CloudosSubnet describe(String region, String vpcId, String name) {

    try {
      Subnetwork subnetwork = googleIntegration.getSubnetwork(region, name);
      return parseSubnetwork(subnetwork, vpcId);
    } catch (IOException e) {
      log.error("Failed to describe Subnetwork name {}", name, e);
      throw new CloudosSubnetException(
          "Failed to describe Subnetwork name " + name, e);
    }
  }

  /**
   * Create an GoogleSubnet object with the parameters.
   *
   * @param subnetwork - Subnetwork from google API
   *
   * @param vpcId - Id of the network on which the subnetwork belongs
   *
   * @return GoogleSubnet - Cloudos object parsed
   */
  private GoogleSubnet parseSubnetwork(Subnetwork subnetwork, String vpcId) {
    return GoogleSubnet.builder() //
        .cidr(subnetwork.getIpCidrRange()) //
        .name(subnetwork.getName()) //
        .subnetId(subnetwork.getId().toString()) //
        .vpc(vpcId) //
        .creation(DateUtil.getIsoDateTime(subnetwork.getCreationTimestamp())) //
        .build();
  }

}
