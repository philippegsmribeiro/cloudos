package cloudos.network;

import java.util.List;

import javax.validation.constraints.NotNull;

import cloudos.models.network.CloudosSubnet;
import cloudos.models.network.CloudosSubnetRequest;
import cloudos.models.network.CloudosVpc;
import cloudos.models.network.SubnetUpdateRequest;

/**
 * The SubnetManagement is responsible for creating a common interface of methods responsible for
 * managing the state of subnets at any one of the major cloud providers.
 *
 * @param <T> Any subtype of CloudosSubnet.
 */
public interface SubnetManagement<T extends CloudosSubnet> {

  CloudosSubnet create(CloudosSubnetRequest cloudosSubnetRequest, CloudosVpc cloudosVpc)
      throws Exception;

  CloudosSubnet update(SubnetUpdateRequest request);

  void delete(T cloudosSubnet, String region);

  List<? extends CloudosSubnet> list(@NotNull CloudosVpc cloudosVpc);

  CloudosSubnet describe(String region, String vpcId, String subnetId);
}
