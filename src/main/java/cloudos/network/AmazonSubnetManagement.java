package cloudos.network;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.ec2.model.Subnet;

import cloudos.amazon.VPC;
import cloudos.models.network.AmazonSubnet;
import cloudos.models.network.CloudosSubnet;
import cloudos.models.network.CloudosSubnetRequest;
import cloudos.models.network.CloudosVpc;
import cloudos.models.network.SubnetUpdateRequest;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class AmazonSubnetManagement implements SubnetManagement<AmazonSubnet> {

  @Autowired
  private CloudosVpcCache cloudosVpcCache;

  /**
   * Create a new subnet based on the request information.
   *
   * @param cloudosSubnetRequest the request to create a new subnet
   * @return a newly created subnet
   */
  @Override
  public CloudosSubnet create(CloudosSubnetRequest cloudosSubnetRequest, CloudosVpc cloudosVpc) {

    // get or create the VPC object
    VPC vpc = this.cloudosVpcCache.getVpc(cloudosVpc.getRegion());
    if (vpc != null) {
      // create the new Subnet
      Subnet subnet = vpc.createSubnet(cloudosVpc.getVpcId(),
          cloudosSubnetRequest.getCidr(),
          cloudosSubnetRequest.getAvailabilityZone(),
          cloudosSubnetRequest.getIsIpv6(),
          cloudosSubnetRequest.getIsPrivateSubnet(),
          cloudosSubnetRequest.getName());

      // return the newly created Amazon Subnet
      return new AmazonSubnet(subnet);
    }
    return null;
  }


  @Override
  public CloudosSubnet update(SubnetUpdateRequest request) {
    // @TODO: IMPLEMENT ME
    return null;
  }

  /**
   * Delete a subnet, if it exists.
   *
   * @param cloudosSubnet the subnet to be deleted in Amazon AWS
   * @param region the name of region the subnet belongs to
   */
  @Override
  public void delete(AmazonSubnet cloudosSubnet, String region) {
    VPC vpc = this.cloudosVpcCache.getVpc(region);
    vpc.deleteSubnet(cloudosSubnet.getSubnetId());
  }

  /**
   * Lists all the subnets that belongs to the given vpcId and region.
   *
   * @param cloudosVpc - vpc
   * @return a list of subnets for the given vpc
   */
  @Override
  public List<? extends CloudosSubnet> list(CloudosVpc cloudosVpc) {
    List<AmazonSubnet> amazonSubnets = new ArrayList<>();
    // get or create the VPC object
    VPC vpc = this.cloudosVpcCache.getVpc(cloudosVpc.getRegion());
    List<Subnet> subnets = vpc.listSubnets();
    if (subnets != null) {
      subnets.forEach(subnet -> {
        // filter the subnet associated with that VPC
        if (subnet != null && subnet.getVpcId().equals(cloudosVpc.getVpcId())) {
          amazonSubnets.add(new AmazonSubnet(subnet));
        }
      });
    }
    return amazonSubnets;
  }

  /**
   * Describe a single cloudos subnet object, given its region.
   *
   * @param region - the subnetwork to describe
   * @param vpcId - the VPC the subnet belongs to
   * @param subnetId - the subnet ID
   * @return the subnet if found, null otherwise
   */
  @Override
  public CloudosSubnet describe(String region, String vpcId, String subnetId) {
    List<AmazonSubnet> amazonSubnets = new ArrayList<>();
    VPC vpc = this.cloudosVpcCache.getVpc(region);
    if (vpc != null) {
      List<Subnet> subnets = vpc.describeSubnets(subnetId);
      subnets.forEach(subnet -> {
        if (subnet.getVpcId().equals(vpcId)) {
          amazonSubnets.add(new AmazonSubnet(subnet));
        }
      });
      return amazonSubnets.get(0);
    }
    // refactor this
    return null;
  }

}
