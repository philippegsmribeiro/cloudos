package cloudos.network;

public class CloudosVpcException extends RuntimeException {

  private static final long serialVersionUID = 7138157422617377827L;

  public CloudosVpcException(String message, Throwable throwable) {
    super(message, throwable);
  }

}
