package cloudos.network;

import java.util.List;

import javax.validation.constraints.NotNull;

import cloudos.exceptions.NotImplementedException;
import cloudos.models.network.CloudosVpc;
import cloudos.models.network.CloudosVpcRequest;
import cloudos.models.network.VpcUpdateRequest;

/**
 * The VpcManagement is responsible for creating a common interface of methods responsible for
 * managing the state of VPC (Virtual Private Cloud) at any one of the major cloud providers.
 *
 * @param <T> Any subtype of CloudosVpc.
 */
public interface VpcManagement<T extends CloudosVpc> {

  /**
   * Create a new CloudosVpc based on the request.
   *
   * @param cloudosVpcRequest the request to create a new VPC
   * @return a newly created CloudosVpc or null
   * @throws Exception any exception that may occur
   */
  CloudosVpc create(CloudosVpcRequest cloudosVpcRequest) throws Exception;

  CloudosVpc update(VpcUpdateRequest request) throws NotImplementedException;

  void delete(T cloudosVpc);

  List<? extends CloudosVpc> list(@NotNull String region);

  CloudosVpc describe(String region, String id);
}
