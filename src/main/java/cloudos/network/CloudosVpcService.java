package cloudos.network;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cloudos.Providers;
import cloudos.exceptions.NotImplementedException;
import cloudos.models.network.CloudosVpc;
import cloudos.models.network.CloudosVpcRequest;
import cloudos.models.network.VpcUpdateRequest;
import lombok.extern.log4j.Log4j2;

/**
 * The CloudosVpcService is responsible for managing cloud VPCs.
 */
@Service
@Log4j2
public class CloudosVpcService {

  @Autowired
  private AmazonVpcManagement amazonVpcManagement;

  @Autowired
  private GoogleVpcManagement googleVpcManagement;

  /**
   * Obtain the VpcManagement service for the appropriate cloud provider.
   *
   * @param providers the cloud provider
   * @return a VpcManagement service
   * @throws NotImplementedException if the manager has not been implemented yet
   */
  @SuppressWarnings("Duplicates")
  private VpcManagement<?> getManagement(Providers providers) throws NotImplementedException {
    switch (providers) {
      case AMAZON_AWS:
        return amazonVpcManagement;
      case GOOGLE_COMPUTE_ENGINE:
        return googleVpcManagement;
      default:
        break;
    }
    throw new NotImplementedException();
  }

  /**
   * Create a new CloudosVpc. Multiplex between the supported providers while attempting to create a
   * new VPC.
   *
   * @param cloudosVpcRequest the original request
   * @param <T> the result from the create operation. It must be a subtype of CloudosVpc
   * @return a subtype of CloudosVpc
   * @throws Exception any error that may occur
   */
  @SuppressWarnings("unchecked")
  public <T extends CloudosVpc> CloudosVpc create(CloudosVpcRequest cloudosVpcRequest)
      throws Exception {

    VpcManagement<T> vpcManagement =
        (VpcManagement<T>) this.getManagement(cloudosVpcRequest.getProvider());

    return vpcManagement.create(cloudosVpcRequest);
  }

  /**
   * Updates an existing CloudosVpc. It may be adding tags or subnets that are being added to the
   * VPC.
   *
   * @param request the VpcUpdate request
   * @return the newly updated CloudosVpc
   */
  @SuppressWarnings("unchecked")
  public CloudosVpc update(@NotNull VpcUpdateRequest request) throws NotImplementedException {

    VpcManagement vpcManagement = this.getManagement(request.getProvider());

    return vpcManagement.update(request);
  }

  /**
   * Given an existing VPC, delete the VPC from the repository and from the provider.
   *
   * @param cloudosVpc the given VPC to be removed
   * @param <T> a subtype of CloudosVpc
   * @throws NotImplementedException if the manager has not been implement for a particular provider
   */
  @SuppressWarnings("unchecked")
  public <T extends CloudosVpc> void delete(T cloudosVpc) throws NotImplementedException {
    // If the CloudOS VPC is not null, then we want to remove it from the repository
    // and then remove it from the provider
    if (cloudosVpc != null) {
      // check that the VPC exists
      log.info("VPC {} found. Will remove it from the repository and the provider", cloudosVpc);

      // obtain the correct VpcManagement
      VpcManagement<T> vpcManagement =
          (VpcManagement<T>) this.getManagement(cloudosVpc.getProvider());
      // delete it from the provider now
      vpcManagement.delete(cloudosVpc);
    }
  }

  /**
   * Get all the VPCs managed by CloudOS. If the user specifies the provider, then only the VPCs
   * related to that provider will be returned.
   *
   * @param provider the supported provider
   * @param region the region where the VCP is located
   * @return a list of VPCs
   * @throws NotImplementedException if the manager has not been implement for a particular provider
   */
  @SuppressWarnings("unchecked")
  public <T extends CloudosVpc> List<CloudosVpc> list(Providers provider, String region)
      throws NotImplementedException {

    VpcManagement<T> vpcManagement = (VpcManagement<T>) this.getManagement(provider);
    List<? extends CloudosVpc> vpcList = vpcManagement.list(region);
    return new ArrayList<>(vpcList);
  }

  /**
   * Find a particular CloudosVpc given its id.
   *
   * @param id the CloudosVpc id
   * @return the CloudosVpc object itself
   */
  @SuppressWarnings("unchecked")
  public <T extends CloudosVpc> CloudosVpc describe(Providers provider, String region, String id)
      throws NotImplementedException {
    VpcManagement<T> vpcManagement = (VpcManagement<T>) this.getManagement(provider);
    return vpcManagement.describe(region, id);
  }
}
