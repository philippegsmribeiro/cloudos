package cloudos.network;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cloudos.amazon.AWSCredentialService;
import cloudos.amazon.VPC;

@Service
public class CloudosVpcCache {

  // build a table that holds the region and the VPC client
  private static ConcurrentMap<String, VPC> table = new ConcurrentHashMap<>();

  @Autowired
  AWSCredentialService awsCredentialService;

  /**
   * Helper method. Given the region, obtain the VCP connector to AWS. If the connector wasn't
   * previously created, then a new one is created and stored in the table.
   *
   * @param region an existing AWS region.
   * @return VPC an Amazon VPC connector.
   */
  public VPC getVpc(@NotNull String region) {
    // VPC connection exists, fetch it
    if (table.containsKey(region)) {
      return table.get(region);
    }
    // create a new VPC client, store in the map
    VPC vpc = new VPC(region, awsCredentialService.getAWSCredentials());
    table.put(region, vpc);

    // finally, return the VPC
    return vpc;
  }
}
