package cloudos.network;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cloudos.Providers;
import cloudos.exceptions.NotImplementedException;
import cloudos.models.network.CloudosSubnet;
import cloudos.models.network.CloudosSubnetRequest;
import cloudos.models.network.CloudosVpc;
import cloudos.models.network.SubnetUpdateRequest;
import lombok.extern.log4j.Log4j2;

/**
 * Define the CloudosSubnetManagement, which is responsible for managing all the subnets across all
 * the providers.
 *
 * <p>
 * The CloudosSubnetManagement receives the request from the controller, and multiplexes between the
 * cloud provider in order to find the appropriate manager, if supported.
 * </p>
 */
@Service
@Log4j2
public class CloudosSubnetManagement {

  @Autowired
  private AmazonSubnetManagement amazonSubnetManagement;

  @Autowired
  private GoogleSubnetManagement googleSubnetManagement;

  /**
   * Obtain the VpcManagement service for the appropriate cloud provider.
   *
   * @param providers the cloud provider
   * @return a VpcManagement service
   * @throws NotImplementedException if the manager has not been implemented yet
   */
  @SuppressWarnings("Duplicates")
  private SubnetManagement<?> getManagement(@NotNull Providers providers)
      throws NotImplementedException {

    switch (providers) {
      case AMAZON_AWS:
        return amazonSubnetManagement;
      case GOOGLE_COMPUTE_ENGINE:
        return googleSubnetManagement;
      default:
        break;
    }
    // thrown when not implemented
    throw new NotImplementedException();
  }

  /**
   * Create a new subnet based on the request information.
   *
   * @param cloudosSubnetRequest the request to create a new subnet
   * @param <T> a subtype of CloudosSubnet
   * @return a newly created subnet
   * @throws Exception any exception that may occur
   */
  @SuppressWarnings("unchecked")
  public <T extends CloudosSubnet> CloudosSubnet create(CloudosSubnetRequest cloudosSubnetRequest,
      CloudosVpc cloudosVpc) throws Exception {

    SubnetManagement<T> subnetManagement =
        (SubnetManagement<T>) this.getManagement(cloudosSubnetRequest.getProvider());

    return subnetManagement.create(cloudosSubnetRequest, cloudosVpc);
  }

  /**
   * Update an existing subnet.
   *
   * @param cloudosSubnet the CloudosSubnet object to be deleted
   * @param <T> a subtype of CloudosSubnet
   */
  @SuppressWarnings("unchecked")
  public CloudosSubnet update(SubnetUpdateRequest request) throws Exception {
    // TODO: Implement whenever subnets are added.
    SubnetManagement subnetManagement = this.getManagement(request.getProvider());
    return null;
  }

  /**
   * Delete a subnet, if it exists.
   *
   * @param cloudosSubnet the CloudosSubnet object to be deleted
   * @param <T> a subtype of CloudosSubnet
   * @throws NotImplementedException in case the provider is not implemented
   */
  @SuppressWarnings("unchecked")
  public <T extends CloudosSubnet> void delete(T cloudosSubnet, String region) throws NotImplementedException {
    SubnetManagement<T> subnetManagement =
        (SubnetManagement<T>) this.getManagement(cloudosSubnet.getProvider());
    // delete from the provider and delete from the repository
    subnetManagement.delete(cloudosSubnet, region);
  }

  /**
   * List all the subnets in a given VPC.
   *
   * @param <T> a subtype of CloudosSubnet
   * @return a list of subnets
   */
  @SuppressWarnings("unchecked")
  public <T extends CloudosSubnet> List<CloudosSubnet> list(@NotNull CloudosVpc cloudosVpc)
      throws Exception {

    List<CloudosSubnet> subnets = new ArrayList<>();
    SubnetManagement<T> subnetManagement =
        (SubnetManagement<T>) this.getManagement(cloudosVpc.getProvider());
    List<? extends CloudosSubnet> current = subnetManagement.list(cloudosVpc);
    if (current != null) {
      subnets = new ArrayList<>(current);
    }
    return subnets;
  }

  /**
   * Find a particular CloudosSubnet given its id.
   *
   * @param provider - the cloud provider
   * @param region - the subnet to describe
   * @param vpcId - the VPC the subnet belongs to
   * @param subnetId - the subnet ID
   * @return the CloudosSubnet object itself
   */
  @SuppressWarnings("unchecked")
  public <T extends CloudosSubnet> CloudosSubnet describe(Providers provider, String region,
      String vpcId, String subnetId) throws NotImplementedException {
    SubnetManagement<T> subnetManagement =
        (SubnetManagement<T>) this.getManagement(provider);
    return subnetManagement.describe(region, vpcId, subnetId);
  }

}
