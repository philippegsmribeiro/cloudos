package cloudos.network;

public class CloudosSubnetException extends RuntimeException {

  private static final long serialVersionUID = -2882088051605197267L;

  public CloudosSubnetException(String message, Throwable throwable) {
    super(message, throwable);
  }

}
