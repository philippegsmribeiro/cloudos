package cloudos;

import cloudos.security.WebSecurityConfig;

import lombok.extern.log4j.Log4j2;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = SchedulerController.SCHEDULER)
@Log4j2
public class SchedulerController {

  public static final String SCHEDULER = WebSecurityConfig.API_PATH + "/scheduler";

}
