# We're using the official Maven 3 image from the Docker Hub (https://hub.docker.com/_/maven/).
# Take a look at the available versions so you can specify the Java version you want to use.
FROM maven:3.5.3-jdk-8

# INSTALL any further tools you need here so they are cached in the docker build
ENV CODECOV_TOKEN="541479cc-ca60-4ce6-a809-bff60ad0beb3"

WORKDIR /app

# Copy the pom.xml into the image to install all dependencies
COPY pom.xml ./

# Copy the settings.xml into the image
COPY settings.xml /root/.m2/

RUN apt-get update && apt-get install -y \
    sshpass curl python python-setuptools python-pip

RUN pip install coverage awscli

# Run install task so all necessary dependencies are downloaded and cached in
# the Docker image. We're running through the whole process but disable
# testing and make sure the command doesn't fail.
RUN export MAVEN_OPTS="-XX:+TieredCompilation -XX:TieredStopAtLevel=1"; mvn dependency:go-offline dependency:resolve-plugins dependency:resolve -U;

COPY src/test/java/cloudos/AppContextTest.java ./src/test/java/cloudos/AppContextTest.java

RUN export MAVEN_OPTS="-XX:+TieredCompilation -XX:TieredStopAtLevel=1"; mvn -D test="cloudos.AppContextTest" test clean --fail-never -B -D checkstyle.skip=true;

# Copy the whole repository into the image
COPY . ./
