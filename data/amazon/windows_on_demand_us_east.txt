	vCPU	ECU	Memory (GiB)	Instance Storage (GB)	Windows Usage
General Purpose - Current Generation
t2.nano	1	Variable	0.5	EBS Only	$0.0088 per Hour
t2.micro	1	Variable	1	EBS Only	$0.018 per Hour
t2.small	1	Variable	2	EBS Only	$0.036 per Hour
t2.medium	2	Variable	4	EBS Only	$0.072 per Hour
t2.large	2	Variable	8	EBS Only	$0.134 per Hour
m4.large	2	6.5	8	EBS Only	$0.252 per Hour
m4.xlarge	4	13	16	EBS Only	$0.504 per Hour
m4.2xlarge	8	26	32	EBS Only	$1.008 per Hour
m4.4xlarge	16	53.5	64	EBS Only	$2.016 per Hour
m4.10xlarge	40	124.5	160	EBS Only	$5.04 per Hour
m3.medium	1	3	3.75	1 x 4 SSD	$0.13 per Hour
m3.large	2	6.5	7.5	1 x 32 SSD	$0.259 per Hour
m3.xlarge	4	13	15	2 x 40 SSD	$0.518 per Hour
m3.2xlarge	8	26	30	2 x 80 SSD	$1.036 per Hour
Compute Optimized - Current Generation
c4.large	2	8	3.75	EBS Only	$0.193 per Hour
c4.xlarge	4	16	7.5	EBS Only	$0.386 per Hour
c4.2xlarge	8	31	15	EBS Only	$0.773 per Hour
c4.4xlarge	16	62	30	EBS Only	$1.546 per Hour
c4.8xlarge	36	132	60	EBS Only	$3.091 per Hour
c3.large	2	7	3.75	2 x 16 SSD	$0.188 per Hour
c3.xlarge	4	14	7.5	2 x 40 SSD	$0.376 per Hour
c3.2xlarge	8	28	15	2 x 80 SSD	$0.752 per Hour
c3.4xlarge	16	55	30	2 x 160 SSD	$1.504 per Hour
c3.8xlarge	32	108	60	2 x 320 SSD	$3.008 per Hour
GPU Instances - Current Generation
g2.2xlarge	8	26	15	60 SSD	$0.767 per Hour
g2.8xlarge	32	104	60	2 x 120 SSD	$2.878 per Hour
Memory Optimized - Current Generation
r3.large	2	6.5	15	1 x 32 SSD	$0.3 per Hour
r3.xlarge	4	13	30.5	1 x 80 SSD	$0.6 per Hour
r3.2xlarge	8	26	61	1 x 160 SSD	$1.08 per Hour
r3.4xlarge	16	52	122	1 x 320 SSD	$1.944 per Hour
r3.8xlarge	32	104	244	2 x 320 SSD	$3.5 per Hour
Storage Optimized - Current Generation
i2.xlarge	4	14	30.5	1 x 800 SSD	$0.973 per Hour
i2.2xlarge	8	27	61	2 x 800 SSD	$1.946 per Hour
i2.4xlarge	16	53	122	4 x 800 SSD	$3.891 per Hour
i2.8xlarge	32	104	244	8 x 800 SSD	$7.782 per Hour
d2.xlarge	4	14	30.5	3 x 2000 HDD	$0.821 per Hour
d2.2xlarge	8	28	61	6 x 2000 HDD	$1.601 per Hour
d2.4xlarge	16	56	122	12 x 2000 HDD	$3.062 per Hour
d2.8xlarge	36	116	244	24 x 2000 HDD	$6.198 per Hour